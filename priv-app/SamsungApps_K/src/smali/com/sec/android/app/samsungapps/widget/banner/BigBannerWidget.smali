.class public Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final AUTO_ROTATION:I = 0x4

.field public static final AUTO_ROTATION_TIME:I = 0x1388

.field public static final MIN_BANNER_NUMBER:I = 0x2


# instance fields
.field a:Z

.field b:Z

.field d:Landroid/os/Handler;

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

.field private g:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

.field private h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

.field private i:Landroid/widget/LinearLayout;

.field private j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

.field private k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->a:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b:Z

    .line 344
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/banner/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/banner/g;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    .line 57
    const v0, 0x7f040060

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->initView(Landroid/content/Context;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->a:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b:Z

    .line 344
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/banner/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/banner/g;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    .line 62
    const v0, 0x7f040060

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->initView(Landroid/content/Context;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->a:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b:Z

    .line 344
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/banner/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/banner/g;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    .line 67
    const v0, 0x7f040060

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->initView(Landroid/content/Context;I)V

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    return-object v0
.end method


# virtual methods
.method public initLayoutItems()V
    .locals 3

    .prologue
    .line 82
    const v0, 0x7f0c018c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->i:Landroid/widget/LinearLayout;

    .line 84
    const v0, 0x7f0c018d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f080110

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setFocusableInTouchMode(Z)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/c;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/d;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setOnTouchObserver(Lcom/sec/android/app/samsungapps/widget/banner/a;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/e;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 149
    return-void
.end method

.method public initView(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->initView(Landroid/content/Context;I)V

    .line 73
    return-void
.end method

.method public loadWidget()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    if-eqz v0, :cond_0

    .line 202
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->a:Z

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/f;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V

    invoke-interface {v0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 217
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->stopAutoRotation()V

    .line 311
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->refreshWidget()V

    .line 313
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onFinishInflate()V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->initLayoutItems()V

    .line 79
    return-void
.end method

.method public playNext()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->getCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->getSelectedItemPosition()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    const/16 v1, 0x15

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 318
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    if-eqz v1, :cond_1

    .line 321
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadCount()I

    move-result v2

    move v1, v0

    .line 322
    :goto_0
    if-ge v1, v2, :cond_1

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadView(I)Landroid/view/View;

    move-result-object v0

    .line 324
    if-eqz v0, :cond_0

    .line 325
    const v3, 0x7f0c0193

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;

    .line 326
    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->refreshWidget()V

    .line 322
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->startAutoRotation()V

    .line 342
    :cond_2
    :goto_1
    return-void

    .line 334
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 335
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setVisibleNodata(I)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 338
    :catch_0
    move-exception v0

    .line 340
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BigBannerWidget::refreshWidget()::OutOfMemoryError::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->release()V

    .line 180
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    .line 182
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    .line 183
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    .line 184
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 185
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->i:Landroid/widget/LinearLayout;

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 190
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 194
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    .line 196
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->removeAllViews()V

    .line 197
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->setBannerWidgetData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->init()V

    .line 294
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_2

    .line 296
    const/4 v0, 0x0

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 299
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 303
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->k:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    const v2, 0x3fffffff    # 1.9999999f

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->setSelection(I)V

    .line 306
    :cond_2
    return-void
.end method

.method public setBannerData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 169
    return-void
.end method

.method public setVisibleNodata(I)Z
    .locals 2

    .prologue
    .line 268
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setVisibleNodata(I)Z

    .line 270
    if-nez p1, :cond_1

    .line 271
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    if-eqz v0, :cond_0

    .line 273
    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 276
    :cond_0
    const v0, 0x7f0c0081

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_1

    .line 278
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 281
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    .line 165
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 173
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    .line 174
    return-void
.end method

.method public startAutoRotation()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 366
    :cond_0
    return-void
.end method

.method public stopAutoRotation()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 372
    :cond_0
    return-void
.end method

.method public updateWidget()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    if-nez v0, :cond_1

    .line 264
    :goto_0
    return-void

    .line 229
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBannerCount()I

    move-result v2

    move v0, v1

    .line 231
    :goto_1
    if-ge v0, v2, :cond_2

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBanner(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 235
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBannerCount()I

    move-result v0

    if-le v0, v5, :cond_6

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    if-nez v0, :cond_3

    .line 239
    new-instance v0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f040064

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    .line 241
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V

    .line 251
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->i:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 255
    :cond_5
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setVisibleLoading(I)Z

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->startAutoRotation()V

    .line 263
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b:Z

    goto :goto_0

    .line 243
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBannerCount()I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    if-nez v0, :cond_7

    .line 246
    new-instance v0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f040065

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    .line 248
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->j:Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V

    goto :goto_2

    .line 258
    :cond_8
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setVisibleNodata(I)Z

    goto/16 :goto_0
.end method
