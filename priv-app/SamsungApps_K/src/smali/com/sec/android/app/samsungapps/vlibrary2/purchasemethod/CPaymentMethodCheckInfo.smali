.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;


# instance fields
.field private _Context:Landroid/content/Context;

.field private _Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Context:Landroid/content/Context;

    .line 16
    return-void
.end method

.method private static isAlipayInstalled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 57
    const-string v1, "com.alipay.android.app"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isAlipayPurchaseSupported()Z
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isAlipayPurchaseSupported()Z

    move-result v0

    return v0
.end method

.method public isCreditCardPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isCreditCardPurchaseAvailable()Z

    move-result v0

    return v0
.end method

.method public isCyberCashSupported()Z
    .locals 1

    .prologue
    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isCyberCashSupported()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 67
    :goto_0
    return v0

    .line 64
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 67
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDirectBillingPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isDirectBillingPurchaseAvailable()Z

    move-result v0

    return v0
.end method

.method public isPPCSupported()Z
    .locals 1

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPPCSupported()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 78
    :goto_0
    return v0

    .line 75
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 78
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPSMSPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPSMSPurchaseAvailable()Z

    move-result v0

    return v0
.end method

.method public isPhoneBillPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPhoneBillPurchaseAvailable()Z

    move-result v0

    return v0
.end method

.method public isTestPSMSPurchaseSupported()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/CPaymentMethodCheckInfo;->_Detail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isTestPSMSPurchaseSupported()Z

    move-result v0

    return v0
.end method
