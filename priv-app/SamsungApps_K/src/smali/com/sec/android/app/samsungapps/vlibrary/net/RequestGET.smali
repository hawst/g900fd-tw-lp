.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/Request;
.source "ProGuard"


# instance fields
.field bSending:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->bSending:Z

    return-void
.end method


# virtual methods
.method public abstract getHttpGet()Lorg/apache/http/client/methods/HttpGet;
.end method

.method public isPOST()Z
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    return v0
.end method

.method protected isSending()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->bSending:Z

    return v0
.end method

.method protected onFail()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public abstract onResult(Lorg/apache/http/HttpResponse;)V
.end method

.method public send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->bSending:Z

    .line 26
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->getHttpGet()Lorg/apache/http/client/methods/HttpGet;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->getURI()Ljava/net/URI;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_1

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    .line 58
    :goto_0
    return-void

    .line 31
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->isCanceled()Z
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    goto :goto_0

    .line 35
    :cond_2
    :try_start_2
    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;->execute(Lorg/apache/http/client/methods/HttpGet;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 36
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onResult(Lorg/apache/http/HttpResponse;)V
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 56
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->bSending:Z

    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    goto :goto_1

    .line 41
    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    goto :goto_1

    .line 44
    :catch_2
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    goto :goto_1

    .line 46
    :catch_3
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    goto :goto_1

    :catchall_0
    move-exception v0

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->onFail()V

    throw v0
.end method
