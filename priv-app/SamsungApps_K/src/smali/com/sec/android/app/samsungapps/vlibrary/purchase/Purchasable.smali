.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/purchase/Purchasable;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getChinaPrepaidCardInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
.end method

.method public abstract getCurrencyUnit()Ljava/lang/String;
.end method

.method public abstract getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
.end method

.method public abstract getDownloadableContent()Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;
.end method

.method public abstract getGUID()Ljava/lang/String;
.end method

.method public abstract getNormalPrice()D
.end method

.method public abstract getPaymentPrice()D
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getProductImageURL()Ljava/lang/String;
.end method

.method public abstract getProductName()Ljava/lang/String;
.end method

.method public abstract purchaseSuccess()V
.end method
