.class public Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final INPUT_METHOD_FROM_FOCUSABLE:I = 0x0

.field public static final INPUT_METHOD_NEEDED:I = 0x1

.field public static final INPUT_METHOD_NOT_NEEDED:I = 0x2

.field private static final K:[I


# instance fields
.field private A:Landroid/graphics/Rect;

.field private B:Landroid/graphics/drawable/Drawable;

.field private C:Landroid/graphics/drawable/Drawable;

.field private D:Landroid/graphics/drawable/Drawable;

.field private E:Z

.field private F:I

.field private G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

.field private H:Z

.field private I:I

.field private J:Z

.field private L:Ljava/lang/ref/WeakReference;

.field private M:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field private N:I

.field private O:I

.field a:Lcom/sec/android/app/samsungapps/widget/am;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/WindowManager;

.field private d:Z

.field private e:Z

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Z

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/view/View$OnTouchListener;

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:[I

.field private z:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 131
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100aa

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->K:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 235
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/view/View;II)V

    .line 236
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/view/View;II)V

    .line 263
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 166
    const v0, 0x1010076

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 167
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i:I

    .line 91
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->k:Z

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->l:Z

    .line 93
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->m:Z

    .line 109
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    .line 110
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->z:[I

    .line 111
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->A:Landroid/graphics/Rect;

    .line 118
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->F:I

    .line 121
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->H:Z

    .line 123
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    .line 129
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/q;-><init>(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->M:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 175
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    .line 176
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    .line 178
    sget-object v0, Lcom/android/internal/R$styleable;->PopupWindow:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 182
    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    .line 184
    invoke-virtual {v3, v4, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 185
    const v4, 0x10301f8

    if-ne v0, v4, :cond_0

    move v0, v1

    :cond_0
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    .line 201
    sget-object v4, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->K:[I

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawableIndex([I)I

    move-result v4

    .line 205
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    move-result v5

    .line 207
    :goto_0
    if-ge v2, v5, :cond_4

    .line 208
    if-eq v2, v4, :cond_2

    .line 216
    :goto_1
    if-eq v4, v1, :cond_3

    if-eq v2, v1, :cond_3

    .line 217
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->C:Landroid/graphics/drawable/Drawable;

    .line 218
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->D:Landroid/graphics/drawable/Drawable;

    .line 225
    :cond_1
    :goto_2
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 226
    return-void

    .line 207
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 220
    :cond_3
    iput-object v6, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->D:Landroid/graphics/drawable/Drawable;

    .line 221
    iput-object v6, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->C:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 248
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/view/View;II)V

    .line 249
    return-void
.end method

.method public constructor <init>(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 279
    return-void
.end method

.method public constructor <init>(Landroid/view/View;IIZ)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i:I

    .line 91
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->k:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->l:Z

    .line 93
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->m:Z

    .line 109
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    .line 110
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->z:[I

    .line 111
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->A:Landroid/graphics/Rect;

    .line 118
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->F:I

    .line 121
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->H:Z

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    .line 129
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/q;-><init>(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->M:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 294
    if-eqz p1, :cond_0

    .line 295
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    .line 298
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setContentView(Landroid/view/View;)V

    .line 299
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setWidth(I)V

    .line 300
    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setHeight(I)V

    .line 301
    invoke-virtual {p0, p4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setFocusable(Z)V

    .line 302
    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    const/high16 v3, 0x20000

    .line 987
    const v0, -0x868219

    and-int/2addr v0, p1

    .line 995
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->H:Z

    if-eqz v1, :cond_0

    .line 996
    const v1, 0x8000

    or-int/2addr v0, v1

    .line 998
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->h:Z

    if-nez v1, :cond_7

    .line 999
    or-int/lit8 v0, v0, 0x8

    .line 1000
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1001
    or-int/2addr v0, v3

    .line 1006
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->k:Z

    if-nez v1, :cond_2

    .line 1007
    or-int/lit8 v0, v0, 0x10

    .line 1009
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->l:Z

    if-eqz v1, :cond_3

    .line 1010
    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    .line 1012
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->m:Z

    if-nez v1, :cond_4

    .line 1013
    or-int/lit16 v0, v0, 0x200

    .line 1015
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->n:Z

    if-eqz v1, :cond_5

    .line 1016
    const/high16 v1, 0x800000

    or-int/2addr v0, v1

    .line 1018
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->o:Z

    if-eqz v1, :cond_6

    .line 1019
    or-int/lit16 v0, v0, 0x100

    .line 1021
    :cond_6
    return v0

    .line 1003
    :cond_7
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1004
    or-int/2addr v0, v3

    goto :goto_0
.end method

.method private a(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;
    .locals 3

    .prologue
    .line 964
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 969
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 970
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->r:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->s:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 971
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->u:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->v:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 972
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 973
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 977
    :goto_0
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 978
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->F:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 979
    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 980
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->j:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 981
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PopupWindow:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 983
    return-object v0

    .line 975
    :cond_0
    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->L:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 1457
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c()V

    .line 1459
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->L:Ljava/lang/ref/WeakReference;

    .line 1460
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1461
    if-eqz v0, :cond_0

    .line 1462
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->M:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1465
    :cond_0
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->N:I

    .line 1466
    iput p3, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->O:I

    .line 1467
    return-void
.end method

.method private a(Landroid/view/View;ZIIII)V
    .locals 8

    .prologue
    const/4 v1, -0x1

    .line 1396
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1431
    :cond_0
    :goto_0
    return-void

    .line 1400
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->L:Ljava/lang/ref/WeakReference;

    .line 1401
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2

    if-eqz p2, :cond_3

    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->N:I

    if-ne v0, p3, :cond_2

    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->O:I

    if-eq v0, p4, :cond_3

    .line 1403
    :cond_2
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;II)V

    .line 1406
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1408
    if-ne p5, v1, :cond_5

    .line 1410
    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->w:I

    .line 1414
    :goto_1
    if-ne p6, v1, :cond_6

    .line 1415
    iget v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->x:I

    .line 1421
    :goto_2
    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1422
    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1424
    if-eqz p2, :cond_7

    .line 1425
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Z)V

    .line 1430
    :goto_3
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v7, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    if-ne v5, v7, :cond_4

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    if-eq v6, v0, :cond_8

    :cond_4
    const/4 v5, 0x1

    :goto_4
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->update(IIIIZ)V

    goto :goto_0

    .line 1412
    :cond_5
    iput p5, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->w:I

    move v3, p5

    goto :goto_1

    .line 1417
    :cond_6
    iput p6, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->x:I

    move v4, p6

    goto :goto_2

    .line 1427
    :cond_7
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->N:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->O:I

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Z)V

    goto :goto_3

    .line 1430
    :cond_8
    const/4 v5, 0x0

    goto :goto_4
.end method

.method private a(Landroid/view/WindowManager$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v0, -0x2

    .line 911
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    if-nez v2, :cond_1

    .line 912
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You must specify a valid content view by calling setContentView() before attempting to show the popup."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    .line 917
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 919
    if-eqz v2, :cond_3

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v2, v0, :cond_3

    .line 926
    :goto_0
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/r;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/samsungapps/widget/r;-><init>(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;Landroid/content/Context;)V

    .line 927
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 930
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/r;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 931
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/samsungapps/widget/r;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 933
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    .line 937
    :goto_1
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->w:I

    .line 938
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->x:I

    .line 939
    return-void

    .line 935
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 867
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->E:Z

    if-eq p1, v0, :cond_0

    .line 868
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->E:Z

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->C:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 875
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->E:Z

    if-eqz v0, :cond_1

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->C:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 885
    :cond_0
    :goto_0
    return-void

    .line 878
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->D:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 881
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1053
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1054
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    aget v2, v2, v0

    add-int/2addr v2, p3

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1055
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    aget v2, v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v2, p4

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1059
    const/16 v2, 0x33

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1061
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->z:[I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1062
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1063
    invoke-virtual {p1, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1065
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    .line 1066
    iget v4, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->x:I

    add-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    if-gt v4, v5, :cond_0

    iget v4, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->w:I

    add-int/2addr v4, v5

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    if-lez v4, :cond_2

    .line 1070
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v4

    .line 1071
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v5

    .line 1072
    new-instance v6, Landroid/graphics/Rect;

    iget v7, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->w:I

    add-int/2addr v7, v4

    add-int/2addr v7, p3

    iget v8, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->x:I

    add-int/2addr v8, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    add-int/2addr v8, p4

    invoke-direct {v6, v4, v5, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1074
    invoke-virtual {p1, v6, v1}, Landroid/view/View;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    .line 1078
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1079
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    aget v4, v4, v0

    add-int/2addr v4, p3

    iput v4, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1080
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    aget v4, v4, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v4, p4

    iput v4, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1083
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->z:[I

    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1085
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->z:[I

    aget v5, v5, v1

    sub-int/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, p4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->z:[I

    aget v5, v5, v1

    sub-int/2addr v5, p4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v5, v2

    if-ge v4, v2, :cond_1

    move v0, v1

    .line 1087
    :cond_1
    if-eqz v0, :cond_3

    .line 1088
    const/16 v2, 0x53

    iput v2, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1089
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    aget v1, v3, v1

    sub-int v1, v2, v1

    add-int/2addr v1, p4

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1095
    :cond_2
    :goto_0
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1097
    return v0

    .line 1091
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    aget v1, v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, p4

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a()[I
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->K:[I

    return-object v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 1025
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 1026
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->e:Z

    if-eqz v0, :cond_1

    .line 1027
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->E:Z

    if-eqz v0, :cond_0

    const v0, 0x10301f2

    .line 1033
    :goto_0
    return v0

    .line 1027
    :cond_0
    const v0, 0x10301f1

    goto :goto_0

    .line 1031
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1033
    :cond_2
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    .prologue
    .line 950
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 952
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->N:I

    return v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->L:Ljava/lang/ref/WeakReference;

    .line 1446
    if-eqz v0, :cond_1

    .line 1447
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1449
    :goto_0
    if-eqz v0, :cond_0

    .line 1450
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1451
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->M:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1453
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->L:Ljava/lang/ref/WeakReference;

    .line 1454
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->O:I

    return v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->E:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->p:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    .line 1178
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1179
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c()V

    .line 1182
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1196
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1199
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    .line 1203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    if-eqz v0, :cond_1

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;->onDismiss()V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1213
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a:Lcom/sec/android/app/samsungapps/widget/am;

    if-eqz v0, :cond_2

    .line 1214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a:Lcom/sec/android/app/samsungapps/widget/am;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/am;->a()V

    .line 1217
    :cond_2
    return-void

    .line 1209
    :catch_0
    move-exception v0

    const-string v0, "OptionMenuPopupWindow::dismiss::IndexOutOfBoundsException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1184
    :catch_1
    move-exception v0

    .line 1186
    :try_start_2
    const-string v1, "OptionMenuPopupWindow::dismiss::IllegalStateException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 1187
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1196
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1199
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    .line 1203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    if-eqz v0, :cond_1

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;->onDismiss()V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1209
    :catch_2
    move-exception v0

    const-string v0, "OptionMenuPopupWindow::dismiss::IndexOutOfBoundsException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1189
    :catch_3
    move-exception v0

    .line 1191
    :try_start_4
    const-string v1, "OptionMenuPopupWindow::dismiss::IllegalArgumentException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 1192
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1196
    :try_start_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1199
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    .line 1203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    if-eqz v0, :cond_1

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;->onDismiss()V
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 1209
    :catch_4
    move-exception v0

    const-string v0, "OptionMenuPopupWindow::dismiss::IndexOutOfBoundsException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1195
    :catchall_0
    move-exception v0

    move-object v1, v0

    .line 1196
    :try_start_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eq v0, v2, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_5

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1199
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    .line 1203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->J:Z

    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    if-eqz v0, :cond_6

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;->onDismiss()V
    :try_end_6
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1210
    :cond_6
    :goto_1
    throw v1

    .line 1209
    :catch_5
    move-exception v0

    const-string v0, "OptionMenuPopupWindow::dismiss::IndexOutOfBoundsException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getAnimationStyle()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    return v0
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 694
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->u:I

    return v0
.end method

.method public getInputMethodMode()I
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i:I

    return v0
.end method

.method public getMaxAvailableHeight(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1111
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v0

    return v0
.end method

.method public getMaxAvailableHeight(Landroid/view/View;I)I
    .locals 1

    .prologue
    .line 1126
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->getMaxAvailableHeight(Landroid/view/View;IZ)I

    move-result v0

    return v0
.end method

.method public getMaxAvailableHeight(Landroid/view/View;IZ)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1147
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1148
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1150
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->y:[I

    .line 1151
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1153
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1154
    if-eqz p3, :cond_0

    .line 1155
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1157
    :cond_0
    aget v3, v2, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v0, v3

    sub-int/2addr v0, p2

    .line 1158
    aget v2, v2, v5

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    add-int/2addr v1, p2

    .line 1161
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1162
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 1163
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->A:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1164
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->A:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->A:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1167
    :cond_1
    return v0
.end method

.method public getSoftInputMode()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->j:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 720
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->r:I

    return v0
.end method

.method public getWindowLayoutType()I
    .locals 1

    .prologue
    .line 656
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->F:I

    return v0
.end method

.method public isAboveAnchor()Z
    .locals 1

    .prologue
    .line 899
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->E:Z

    return v0
.end method

.method public isClippingEnabled()Z
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->m:Z

    return v0
.end method

.method public isFocusable()Z
    .locals 1

    .prologue
    .line 422
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->h:Z

    return v0
.end method

.method public isLayoutInScreenEnabled()Z
    .locals 1

    .prologue
    .line 625
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->o:Z

    return v0
.end method

.method public isOutsideTouchable()Z
    .locals 1

    .prologue
    .line 534
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->l:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 744
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    return v0
.end method

.method public isSplitTouchEnabled()Z
    .locals 1

    .prologue
    .line 596
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->n:Z

    return v0
.end method

.method public isTouchable()Z
    .locals 1

    .prologue
    .line 503
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->k:Z

    return v0
.end method

.method public setAnimationStyle(I)V
    .locals 0

    .prologue
    .line 362
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->I:I

    .line 363
    return-void
.end method

.method public setAtLocation(Landroid/view/View;III)V
    .locals 3

    .prologue
    .line 785
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 786
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 787
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 788
    iput p3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 789
    iput p4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    if-eqz v1, :cond_0

    .line 792
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 808
    :cond_0
    :goto_0
    return-void

    .line 794
    :catch_0
    move-exception v0

    .line 796
    const-string v1, "OptionMenuPopupWindow::setAtLocation::IllegalStateException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 797
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 798
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    goto :goto_0

    .line 800
    :catch_1
    move-exception v0

    .line 802
    const-string v1, "OptionMenuPopupWindow::setAtLocation::IllegalArgumentException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 803
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 804
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    goto :goto_0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->B:Landroid/graphics/drawable/Drawable;

    .line 321
    return-void
.end method

.method public setClippingEnabled(Z)V
    .locals 0

    .prologue
    .line 584
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->m:Z

    .line 585
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_2

    .line 391
    :cond_0
    const-string v0, "isShowing is false or contentView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 404
    :cond_1
    :goto_0
    return-void

    .line 395
    :cond_2
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    if-nez v0, :cond_3

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    .line 401
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    if-nez v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 0

    .prologue
    .line 442
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->h:Z

    .line 443
    return-void
.end method

.method public setHeight(I)V
    .locals 0

    .prologue
    .line 709
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->u:I

    .line 710
    return-void
.end method

.method public setIgnoreCheekPress()V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->H:Z

    .line 345
    return-void
.end method

.method public setInputMethodMode(I)V
    .locals 0

    .prologue
    .line 468
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i:I

    .line 469
    return-void
.end method

.method public setLayoutInScreenEnabled(Z)V
    .locals 0

    .prologue
    .line 637
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->o:Z

    .line 638
    return-void
.end method

.method public setOnDismissListener(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;)V
    .locals 0

    .prologue
    .line 1225
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->G:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow$OnDismissListener;

    .line 1226
    return-void
.end method

.method public setOutsideTouchable(Z)V
    .locals 0

    .prologue
    .line 555
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->l:Z

    .line 556
    return-void
.end method

.method public setRemoveSensorManagerListener(Lcom/sec/android/app/samsungapps/widget/am;)V
    .locals 0

    .prologue
    .line 1582
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a:Lcom/sec/android/app/samsungapps/widget/am;

    .line 1583
    return-void
.end method

.method public setSoftInputMode(I)V
    .locals 0

    .prologue
    .line 482
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->j:I

    .line 483
    return-void
.end method

.method public setSplitTouchEnabled(Z)V
    .locals 0

    .prologue
    .line 614
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->n:Z

    .line 615
    return-void
.end method

.method public setTouchInterceptor(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->p:Landroid/view/View$OnTouchListener;

    .line 412
    return-void
.end method

.method public setTouchable(Z)V
    .locals 0

    .prologue
    .line 522
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->k:Z

    .line 523
    return-void
.end method

.method public setWidth(I)V
    .locals 0

    .prologue
    .line 735
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->r:I

    .line 736
    return-void
.end method

.method public setWindowLayoutMode(II)V
    .locals 0

    .prologue
    .line 682
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->q:I

    .line 683
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->t:I

    .line 684
    return-void
.end method

.method public setWindowLayoutType(I)V
    .locals 0

    .prologue
    .line 648
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->F:I

    .line 649
    return-void
.end method

.method public showAsDropDown(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 822
    invoke-virtual {p0, p1, v0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 823
    return-void
.end method

.method public showAsDropDown(Landroid/view/View;II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 840
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 844
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;II)V

    .line 846
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    .line 847
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->e:Z

    .line 849
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 850
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/WindowManager$LayoutParams;)V

    .line 852
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Z)V

    .line 854
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->t:I

    if-gez v1, :cond_2

    .line 855
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->t:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->v:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 857
    :cond_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->q:I

    if-gez v1, :cond_3

    .line 858
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->q:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->s:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 861
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 863
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0
.end method

.method public showAtLocation(Landroid/view/View;III)V
    .locals 2

    .prologue
    .line 762
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c()V

    .line 768
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->d:Z

    .line 769
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->e:Z

    .line 771
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 772
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 774
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/WindowManager$LayoutParams;)V

    .line 775
    if-nez p2, :cond_2

    .line 776
    const/16 p2, 0x33

    .line 778
    :cond_2
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 779
    iput p3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 780
    iput p4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 781
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0
.end method

.method public update()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1236
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1260
    :cond_0
    :goto_0
    return-void

    .line 1240
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1243
    const/4 v1, 0x0

    .line 1245
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b()I

    move-result v3

    .line 1246
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    if-eq v3, v4, :cond_2

    .line 1247
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    move v1, v2

    .line 1251
    :cond_2
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(I)I

    move-result v3

    .line 1252
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    if-eq v3, v4, :cond_3

    .line 1253
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1257
    :goto_1
    if-eqz v2, :cond_0

    .line 1258
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method public update(II)V
    .locals 6

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1273
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->update(IIIIZ)V

    .line 1274
    return-void
.end method

.method public update(IIII)V
    .locals 6

    .prologue
    .line 1288
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->update(IIIIZ)V

    .line 1289
    return-void
.end method

.method public update(IIIIZ)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    .line 1305
    if-eq p3, v4, :cond_0

    .line 1306
    iput p3, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->s:I

    .line 1307
    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setWidth(I)V

    .line 1310
    :cond_0
    if-eq p4, v4, :cond_1

    .line 1311
    iput p4, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->v:I

    .line 1312
    invoke-virtual {p0, p4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setHeight(I)V

    .line 1315
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f:Landroid/view/View;

    if-nez v0, :cond_3

    .line 1360
    :cond_2
    :goto_0
    return-void

    .line 1319
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1321
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->q:I

    if-gez v2, :cond_9

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->q:I

    .line 1324
    :goto_1
    if-eq p3, v4, :cond_4

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    if-eq v3, v2, :cond_4

    .line 1325
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->s:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    move p5, v1

    .line 1329
    :cond_4
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->t:I

    if-gez v2, :cond_a

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->t:I

    .line 1330
    :goto_2
    if-eq p4, v4, :cond_5

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    if-eq v3, v2, :cond_5

    .line 1331
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->v:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    move p5, v1

    .line 1335
    :cond_5
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    if-eq v2, p1, :cond_6

    .line 1336
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    move p5, v1

    .line 1340
    :cond_6
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    if-eq v2, p2, :cond_7

    .line 1341
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move p5, v1

    .line 1345
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->b()I

    move-result v2

    .line 1346
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    if-eq v2, v3, :cond_8

    .line 1347
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    move p5, v1

    .line 1351
    :cond_8
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(I)I

    move-result v2

    .line 1352
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    if-eq v2, v3, :cond_b

    .line 1353
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1357
    :goto_3
    if-eqz v1, :cond_2

    .line 1358
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->c:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1321
    :cond_9
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->s:I

    goto :goto_1

    .line 1329
    :cond_a
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->v:I

    goto :goto_2

    :cond_b
    move v1, p5

    goto :goto_3
.end method

.method public update(Landroid/view/View;II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1372
    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, v2

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;ZIIII)V

    .line 1373
    return-void
.end method

.method public update(Landroid/view/View;IIII)V
    .locals 7

    .prologue
    .line 1390
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a(Landroid/view/View;ZIIII)V

    .line 1391
    return-void
.end method
