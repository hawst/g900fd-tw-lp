.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field private static l:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;


# instance fields
.field protected _silnceInstallErrCode:Ljava/lang/String;

.field a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

.field b:Landroid/os/Handler;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private f:Z

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;

.field private k:Z

.field private m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->l:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;ZZLcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    .line 32
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->_silnceInstallErrCode:Ljava/lang/String;

    .line 180
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    .line 232
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/s;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/s;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->b:Landroid/os/Handler;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 46
    iput-boolean p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->f:Z

    .line 47
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    .line 48
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->i:Ljava/lang/String;

    .line 49
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->k:Z

    .line 50
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/p;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/p;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 68
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 147
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->f:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isKnoxMode()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 157
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 156
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->k:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V
    .locals 3

    .prologue
    .line 26
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->setInstallerPackageName(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public install()V
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 73
    return-void
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 87
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/v;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 89
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->makeFileReadable()V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/installer/r;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/r;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->install()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onForegroundInstalling()V

    goto :goto_0

    .line 93
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->makeFileReadable()V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->getAbsoluteFilePath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/installer/t;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/t;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->installPackage(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 96
    :pswitch_2
    :try_start_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->setOnInstalledPackaged(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/util/MultiUserUtil;->isMultiUserSupport(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->i:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/util/MultiUserUtil;->isInstalledForOtherUser(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->installExistingPackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InstallCommand::silenceInstall::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto/16 :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->makeFileReadable()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->getAbsoluteFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->installPackage(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 99
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->_silnceInstallErrCode:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallSuccess()V

    goto/16 :goto_0

    .line 106
    :pswitch_5
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->deleteFile()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 107
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 109
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto/16 :goto_0

    .line 114
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed()V

    goto/16 :goto_0

    .line 117
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;->execute()V

    goto/16 :goto_0

    .line 120
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;->prepare()V

    goto/16 :goto_0

    .line 124
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;->userCancel()V

    goto/16 :goto_0

    .line 129
    :pswitch_a
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_KNOX_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSlienceInstallSupported()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->k:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto/16 :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto/16 :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->k:Z

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto/16 :goto_0

    .line 132
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->l:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->dequeue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V

    goto/16 :goto_0

    .line 135
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->l:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->enqueue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V

    goto/16 :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;)V

    return-void
.end method

.method public onRun(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;)V
    .locals 1

    .prologue
    .line 341
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->RECEIVED_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 342
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    .line 57
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    .line 78
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;)V

    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 315
    return-void
.end method
