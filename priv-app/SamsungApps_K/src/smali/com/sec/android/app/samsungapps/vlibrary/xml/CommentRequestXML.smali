.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 0

    .prologue
    .line 9
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    return-void
.end method

.method private static cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    :try_start_0
    const-string v0, "[^^\t\r\n -\ud7ff\ue000-\ufffd]"

    .line 37
    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    .line 38
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 40
    const-string v0, ""

    goto :goto_0
.end method

.method public static toAdd(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    const-string v2, "commentRegister"

    const-string v3, "4050"

    move-object v1, p0

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 25
    const-string v1, "productID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v1, "rating"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;->getRatingValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;->getCommentText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    const-string v2, "comment"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return-object v0
.end method

.method public static toDel(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    const-string v2, "commentDelete"

    const-string v3, "4040"

    move-object v1, p0

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 16
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v1, "commentID"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    return-object v0
.end method

.method public static toModify(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    const-string v2, "commentModify"

    const-string v3, "4060"

    move-object v1, p0

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 48
    const-string v1, "productID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "commentID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;->getCommentID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;->getCommentText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    const-string v2, "comment"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v1, "ratingValue"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;->getRatingValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-object v0
.end method
