.class public Lcom/sec/android/app/samsungapps/UncListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

.field private c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    .line 24
    const-string v0, "UncListActivity"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->d:Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f04005f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/UncListActivity;->setMainView(I)V

    .line 36
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v4, v3, [I

    const/4 v0, 0x0

    const v5, 0xa0008

    aput v5, v4, v0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/UncListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/lo;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lo;-><init>(Lcom/sec/android/app/samsungapps/UncListActivity;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 47
    return-void
.end method

.method public onDataLoadCompleted()V
    .locals 2

    .prologue
    .line 101
    const-string v0, "UncListActivity"

    const-string v1, "onDataLoadCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void
.end method

.method public onDataLoadingMore()V
    .locals 2

    .prologue
    .line 107
    const-string v0, "UncListActivity"

    const-string v1, "onDataLoadingMore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->release()V

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->release()V

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    .line 60
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 61
    return-void
.end method

.method public onInitCompleted()V
    .locals 3

    .prologue
    .line 112
    const v0, 0x7f0c018a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/UncListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    const/16 v2, 0xf

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->loadWidget()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UncListActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/UncListActivity;->initialized()V

    .line 116
    return-void
.end method

.method public onInitFailed()V
    .locals 0

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/UncListActivity;->finish()V

    .line 121
    return-void
.end method

.method public onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 96
    :goto_0
    return-void

    .line 91
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/AboutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 93
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/UncListActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x7f0c000e
        :pswitch_0
    .end packed-switch
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 80
    const v1, 0x7f0c000e

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/UncListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f080116

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c3

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 81
    const/4 v0, 0x1

    return v0
.end method
