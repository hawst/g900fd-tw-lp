.class public interface abstract Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract onDownloadCanceled()V
.end method

.method public abstract onDownloadFailed()V
.end method

.method public abstract onDownloadSuccess()V
.end method

.method public abstract onInstallFailed(Ljava/lang/String;)V
.end method

.method public abstract onProgress(JJ)V
.end method
