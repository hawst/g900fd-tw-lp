.class public Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field protected _Context:Landroid/content/Context;

.field private _IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

.field private _ProductDetailResultMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Context:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    .line 41
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 42
    return-void
.end method


# virtual methods
.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getContentSize()I
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;)V

    return-object v0
.end method

.method public getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 192
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;)V

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)V

    return-object v0
.end method

.method public getProductDetailResultMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_ProductDetailResultMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWishListID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->wishListId:Ljava/lang/String;

    return-object v0
.end method

.method public hasLoadType()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOrderID()Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    return v0
.end method

.method public hasProductID()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 175
    :cond_0
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isAddedWishItem()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->wishListYn:Z

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInstalledItem()Z
    .locals 2

    .prologue
    .line 159
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->getGUID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    .line 163
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrePostType()Z
    .locals 2

    .prologue
    .line 108
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "3"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurchasedContentType()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    return v0
.end method

.method public isPurchasedDetailType()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseDetail;

    return v0
.end method

.method public isStoreType()Z
    .locals 2

    .prologue
    .line 103
    const-string v0, "2"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public requestGUIDProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V

    return-object v0
.end method

.method public requestGUIDProductDetailExCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V

    return-object v0
.end method

.method public requestGUIDProductDetailMainCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailMainCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailMainCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V

    return-object v0
.end method

.method public requestProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V

    return-object v0
.end method

.method public setAddedWishItem()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->wishListYn:Z

    .line 139
    return-void
.end method

.method public setDeletedWishItem()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->wishListYn:Z

    .line 145
    return-void
.end method

.method public setLikeState(ZI)V
    .locals 1

    .prologue
    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iput-boolean p1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->userLikeYn:Z

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iput p2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->likeCount:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    return-void

    .line 205
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProductDetailResultMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_ProductDetailResultMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 214
    return-void
.end method

.method public updateDetailAndAskAlreadyPurchased()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    return-object v0
.end method

.method public updateDetailViewAfterGetDetail()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;)V

    return-object v0
.end method

.method public validateCompatibleOS()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    return-object v0
.end method

.method public validateDetail()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->getProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    return-object v0
.end method
