.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

.field private b:Landroid/content/Context;

.field private c:Landroid/os/Handler;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->c:Landroid/os/Handler;

    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    .line 176
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    .line 194
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    .line 48
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 49
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "com.sec.android.app.samsungapps"

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->c:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/k;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 60
    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "Samsung Apps"

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;->onSelfUpdateResult(Z)V

    .line 258
    :cond_0
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    .line 64
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;)V
    .locals 9

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/o;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 80
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->c()V

    goto :goto_0

    .line 83
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;->onSelfUpdateResult(Z)V

    goto :goto_0

    .line 86
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/m;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->downloadEx2(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 89
    :pswitch_3
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    const-string v1, "update_self_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->NOT_AVAILABLE_UPDATE_BECAUSEOF_NETWORKSTATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    goto :goto_0

    :cond_1
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    invoke-direct {v6, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    const-string v2, "Samsung Apps"

    const-string v3, "com.sec.android.app.samsungapps"

    const-string v4, "0.1"

    const-string v5, "odc9820938409234.apk"

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->getApkInfo()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v8

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    invoke-direct {v6, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    const-string v2, "Samsung Apps"

    const-string v3, "com.sec.android.app.samsungapps"

    const-string v4, "0.1"

    const-string v5, "odc9820938409234.apk"

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->getVersionCodeList()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)V

    invoke-virtual {v8, v7, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->updateCheck(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto/16 :goto_0

    .line 92
    :pswitch_4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    const-string v3, "odc9820938409234Self.apk"

    const-string v4, "com.sec.android.app.samsungapps"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    iget-object v5, v5, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->execute()V

    goto/16 :goto_0

    .line 95
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;->onSelfUpdated()V

    goto/16 :goto_0

    .line 98
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;->setForeground()V

    goto/16 :goto_0

    .line 101
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;->unsetForeground()V

    goto/16 :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;

    .line 262
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    .line 69
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;)V

    return-void
.end method

.method public userCancel()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;->UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    if-ne v0, v1, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->userCancel()V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;->REQ_DOWNLOADEX:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    if-ne v0, v1, :cond_0

    .line 128
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$State;

    .line 129
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->c()V

    goto :goto_0
.end method
