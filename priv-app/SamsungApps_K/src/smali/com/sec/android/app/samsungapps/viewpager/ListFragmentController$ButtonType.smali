.class public final enum Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum BUTTON_ALL:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

.field public static final enum BUTTON_FREE:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

.field public static final enum BUTTON_NEW:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

.field public static final enum BUTTON_PAID:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

.field private static final synthetic b:[Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    const-string v1, "BUTTON_ALL"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_ALL:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    const-string v1, "BUTTON_PAID"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_PAID:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    const-string v1, "BUTTON_FREE"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_FREE:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    const-string v1, "BUTTON_NEW"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_NEW:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_ALL:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_PAID:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_FREE:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_NEW:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->b:[Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->a:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->b:[Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    return-object v0
.end method


# virtual methods
.method public final value()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->a:I

    return v0
.end method
