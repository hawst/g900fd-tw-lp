.class public Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:I

.field b:Ljava/util/ArrayList;

.field c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->a:I

    .line 26
    iput p2, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->a:I

    .line 27
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->b:Ljava/util/ArrayList;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->c:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 34
    if-eqz p2, :cond_0

    .line 37
    check-cast p2, Landroid/widget/LinearLayout;

    .line 43
    :goto_0
    const v0, 0x7f0c025a

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 44
    const v1, 0x7f0c0258

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 45
    const v2, 0x7f0c0259

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 46
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 50
    return-object p2

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 40
    iget v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;->c:Landroid/content/Context;

    .line 56
    return-void
.end method
