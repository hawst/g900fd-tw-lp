.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CONFIRM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

.field public static final enum CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

.field public static final enum INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    const-string v1, "INVOKE_COMPLETED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    const-string v1, "CONFIRM_DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->CONFIRM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    const-string v1, "CONFIRM_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->CONFIRM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    return-object v0
.end method
