.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private _Context:Landroid/content/Context;

.field private _ICancellableLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

.field private _IInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

.field private _Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

.field private mbNeedRetry:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->mbNeedRetry:Z

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Context:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_IInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_ICancellableLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;Z)Z
    .locals 0

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->mbNeedRetry:Z

    return p1
.end method

.method private isURLValid()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 108
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->needUpdate()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 110
    :cond_0
    return v0
.end method

.method private requestInit()V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_IInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;->createInitializer(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;->execute()V

    .line 97
    return-void
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)V
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)Z

    .line 102
    return-void
.end method


# virtual methods
.method public check(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;)V
    .locals 2

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;

    .line 117
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->isURLValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->CHECK_URL_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)Z

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->CHECK_URL_NOT_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)Z

    goto :goto_0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;)V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 47
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->requestInit()V

    goto :goto_0

    .line 50
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->mbNeedRetry:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;->onDeeplinkInitFailed(Z)V

    goto :goto_0

    .line 56
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;->onDeeplinkInitSuccess()V

    goto :goto_0

    .line 62
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_ICancellableLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_ICancellableLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;->start(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;)V

    goto :goto_0

    .line 74
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_ICancellableLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_ICancellableLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;->end()V

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    .line 35
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;)V

    return-void
.end method
