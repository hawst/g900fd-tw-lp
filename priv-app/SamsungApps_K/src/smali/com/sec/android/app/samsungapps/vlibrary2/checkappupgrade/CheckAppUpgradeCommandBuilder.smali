.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;


# instance fields
.field private _DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

.field private _OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-nez v0, :cond_0

    .line 25
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 31
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 32
    return-void

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->load(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract askUserUpdateODC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
.end method

.method protected abstract getODCUpdateViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getOldFreeStoreType()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFreeStoreClsf()I

    move-result v0

    return v0
.end method

.method public getSavedClientType()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getClientType()Ljava/lang/String;

    move-result-object v0

    .line 68
    const-string v1, "odc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ODC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    .line 74
    :goto_0
    return-object v0

    .line 71
    :cond_0
    const-string v1, "unc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract notifyChangedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract notifyCheckAppUpgradeFinished()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract notifyLimitedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->getODCUpdateViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    return-object v0
.end method

.method public requestCheckAppUpgradeCommand(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V

    return-object v0
.end method

.method public saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    return-void

    .line 82
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    const-string v1, "odc"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeClientType(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    const-string v1, "unc"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeClientType(Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public saveStoreType(I)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->_DB:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeFreeStore(I)V

    .line 107
    return-void
.end method

.method public unaUpdateCommand(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->getODCUpdateViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;->getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    return-object v0
.end method

.method public validateCheckAppUpgrade()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;)V

    return-object v0
.end method

.method public abstract validateDisclaimerCommand(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
