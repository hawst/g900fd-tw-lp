.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;
.source "ProGuard"


# instance fields
.field private _IAlipayUpdateCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;

.field loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 16
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->setNeedAutoDownload(Z)V

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->_IAlipayUpdateCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;

    .line 18
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->_IAlipayUpdateCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 26
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 27
    return-void
.end method

.method protected onFinalResult(Z)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onFinalResult(Z)V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 33
    return-void
.end method
