.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# static fields
.field private static final SWALLET_PACKAGENAME:Ljava/lang/String; = "com.sec.android.wallet"

.field private static final SWALLET_RECVACTIVITYNAME:Ljava/lang/String; = "com.sec.android.wallet.receive.ReceiveActivity"


# instance fields
.field private _ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

.field private receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->onFinalResult(Z)V

    return-void
.end method

.method private startWallet()V
    .locals 7

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->registerBroadcastReceiver()V

    .line 28
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 29
    const-string v0, "com.sec.android.wallet"

    const-string v1, "com.sec.android.wallet.receive.ReceiveActivity"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    const/high16 v0, 0x34000000

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getWK()Ljava/lang/String;

    move-result-object v4

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wallet"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 36
    const-string v2, ""

    .line 37
    const-string v0, ""

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getPrice()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SWGAESEncryption;->encryptionAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 40
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getTransactionID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SWGAESEncryption;->encryptionAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 45
    :goto_0
    const-string v2, "type"

    const-string v5, "mobile.instore"

    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    const-string v2, "action"

    const-string v5, "payment"

    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v2, "serviceId"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const-string v2, "price"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v1, "trid"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v0, "pType"

    const-string v1, "ALL"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v0, "cType"

    const-string v1, "ALL"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v0, "timeout"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getTimeout()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v0, " returnParams"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getReturnParams()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 56
    return-void

    .line 41
    :catch_0
    move-exception v1

    move-object v6, v1

    move-object v1, v2

    move-object v2, v6

    :goto_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->startWallet()V

    .line 129
    return-void
.end method

.method protected registerBroadcastReceiver()V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    .line 61
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "com.sec.android.wallet.PAYMENT_COMPLETED_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getWK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 65
    return-void
.end method

.method protected unregisterBroadcastReceiver()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 73
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->receiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;

    .line 74
    return-void
.end method
