.class public Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field _CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 39
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->clear()V

    .line 44
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 28
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->add(Ljava/lang/Object;)Z

    .line 30
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 31
    return-void
.end method

.method public findCouponBySeq(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 52
    if-nez p1, :cond_0

    move-object v0, v1

    .line 61
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 57
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 61
    goto :goto_0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 21
    :cond_0
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 68
    return-void
.end method
