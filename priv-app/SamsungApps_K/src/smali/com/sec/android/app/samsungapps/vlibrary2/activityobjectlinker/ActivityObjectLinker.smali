.class public Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static _ObjectList:Landroid/util/SparseArray;

.field private static iKey:I

.field static final sSyncMonitor:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->sSyncMonitor:Ljava/lang/Integer;

    .line 15
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    .line 36
    sput v1, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->iKey:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static clearAgedObjects()V
    .locals 8

    .prologue
    .line 52
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->sSyncMonitor:Ljava/lang/Integer;

    monitor-enter v2

    .line 54
    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 55
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 57
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/a;

    .line 58
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/a;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x2328

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 55
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static popObject(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 24
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->sSyncMonitor:Ljava/lang/Integer;

    monitor-enter v1

    .line 26
    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/a;

    .line 27
    if-eqz v0, :cond_0

    .line 29
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    invoke-virtual {v2, p0}, Landroid/util/SparseArray;->remove(I)V

    .line 30
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/a;->b()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static pushObject(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->clearAgedObjects()V

    .line 41
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->sSyncMonitor:Ljava/lang/Integer;

    monitor-enter v1

    .line 43
    :try_start_0
    sget v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->iKey:I

    add-int/lit8 v2, v0, 0x1

    sput v2, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->iKey:I

    .line 45
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->_ObjectList:Landroid/util/SparseArray;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/a;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 46
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static readObject(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 122
    const-string v0, "objectid"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 123
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->popObject(I)Ljava/lang/Object;

    move-result-object v0

    .line 124
    return-object v0
.end method

.method public static readObjectForFragment(Landroid/app/Fragment;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 139
    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    .line 143
    :cond_0
    const-string v1, "objectid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 144
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->popObject(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static registerObjectForFragment(Landroid/app/Fragment;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 129
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->pushObject(Ljava/lang/Object;)I

    move-result v0

    .line 131
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 132
    const-string v2, "objectid"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    invoke-virtual {p0, v1}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 134
    return-void
.end method

.method public static startActivityForResultWithObject(Landroid/app/Activity;Ljava/lang/Class;Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 114
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->pushObject(Ljava/lang/Object;)I

    move-result v0

    .line 115
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    const-string v2, "objectid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, v1, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 118
    return-void
.end method

.method public static startActivityWithObject(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 77
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->pushObject(Ljava/lang/Object;)I

    move-result v0

    .line 78
    const-string v1, "objectid"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 79
    instance-of v0, p0, Landroid/app/Service;

    if-eqz v0, :cond_0

    .line 81
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 83
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 84
    return-void
.end method

.method public static startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 89
    return-void
.end method

.method public static startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/high16 v3, 0x10000000

    .line 93
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->pushObject(Ljava/lang/Object;)I

    move-result v0

    .line 94
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    instance-of v2, p0, Landroid/app/Service;

    if-eqz v2, :cond_0

    .line 97
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 99
    :cond_0
    const-string v2, "objectid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 100
    instance-of v0, p0, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 102
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    :cond_1
    if-eqz p3, :cond_2

    .line 106
    invoke-virtual {v1, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 109
    :cond_2
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 110
    return-void
.end method
