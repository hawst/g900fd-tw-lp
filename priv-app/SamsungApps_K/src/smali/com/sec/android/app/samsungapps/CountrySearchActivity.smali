.class public Lcom/sec/android/app/samsungapps/CountrySearchActivity;
.super Lcom/sec/android/app/samsungapps/OrientationActivity;
.source "ProGuard"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

.field private b:Ljava/lang/String;

.field private c:Landroid/widget/EditText;

.field private d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

.field private e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

.field private f:Ljava/util/ArrayList;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/OrientationActivity;-><init>()V

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const v9, 0x7f0c008e

    const v8, 0x7f0400e0

    const/4 v7, -0x1

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 225
    const v0, 0x7f080336

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v4, v6, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 226
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 227
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setMainViewAndEmptyView(I)V

    .line 231
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconNextFocusDownid(I)V

    .line 232
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    const-string v4, "   "

    invoke-direct {v2, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0201b0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getTextSize()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v8, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v4, v8

    double-to-int v4, v4

    invoke-virtual {v0, v6, v6, v4, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x2

    const/16 v5, 0x21

    invoke-virtual {v2, v4, v3, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    const/16 v1, 0x4001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v3, :cond_6

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    :goto_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a(Z)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 264
    :cond_0
    :goto_2
    const v0, 0x7f0c008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 265
    new-instance v1, Lcom/sec/android/app/samsungapps/ca;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ca;-><init>(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    if-nez v0, :cond_1

    .line 273
    new-instance v0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    const v1, 0x7f0400e4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/CountryIndexer;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/view/CountryIndexer;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->setIndexer(Landroid/widget/SectionIndexer;)V

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    if-eqz v0, :cond_7

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->getPosition()I

    move-result v0

    move v1, v0

    .line 290
    :goto_3
    const v0, 0x7f0c02f7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    new-instance v2, Lcom/sec/android/app/samsungapps/cb;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/cb;-><init>(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->setListOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 304
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b()V

    .line 310
    if-eq v1, v7, :cond_3

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->setPostion(I)V

    .line 314
    :cond_3
    return-void

    .line 229
    :cond_4
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setMainView(I)V

    goto/16 :goto_0

    :cond_5
    move v3, v6

    .line 246
    goto/16 :goto_1

    .line 257
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_2

    :cond_7
    move v1, v7

    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CountrySearchActivity;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->getTitle()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v8, :cond_2

    :cond_0
    const-string v0, "CountrySearchActivity::getCountry::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :cond_1
    :goto_0
    move-object v0, v2

    :goto_1
    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->cancel()V

    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->finish()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->getCountryListMap()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "CountrySearchActivity::getCountry::countryList is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->findCountryNameByMCC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eq v5, v8, :cond_5

    const-string v5, "%s (%s)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    aput-object v1, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_4

    goto :goto_1

    :cond_5
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->selectCountry(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_2
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 533
    const v0, 0x7f0c008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 534
    if-eqz v1, :cond_0

    .line 536
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 538
    :cond_0
    return-void

    .line 536
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)Lcom/sec/android/app/samsungapps/widget/SectionListView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 467
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setVisibleLoading(I)Z

    .line 468
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setVisibleNodata(I)Z

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->showSectionScroller(Z)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->setVisibility(I)V

    .line 495
    :cond_1
    :goto_0
    return-void

    .line 482
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setVisibleLoading(I)Z

    .line 483
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setVisibleNodata(I)Z

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->showSectionScroller(Z)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)Lcom/sec/android/app/samsungapps/view/CountryListAdapter;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 395
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    if-nez v0, :cond_1

    .line 397
    :cond_0
    const-string v0, "CountrySearchActivity::afterTextChanged::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 426
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->b:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/cc;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/cc;-><init>(Lcom/sec/android/app/samsungapps/CountrySearchActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->g:Z

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->g:Z

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->cancel()V

    .line 103
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/OrientationActivity;->onBackPressed()V

    .line 104
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/OrientationActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a()V

    .line 111
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 59
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/OrientationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0, v5, v2}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->finish()V

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setVisibleLoading(I)Z

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setVisibleNodata(I)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->getCountryListMap()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;->size()I

    move-result v1

    if-ne v1, v6, :cond_4

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->cancel()V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->finish()V

    .line 70
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a()V

    goto :goto_0

    .line 69
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->selectCountry(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->findCountryNameByMCC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    if-eq v2, v3, :cond_5

    const-string v3, "%s (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->f:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->d:Lcom/sec/android/app/samsungapps/view/CountryListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->clear()V

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->e:Lcom/sec/android/app/samsungapps/widget/SectionListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->release()V

    .line 91
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/OrientationActivity;->onDestroy()V

    .line 92
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 375
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-ne v0, v1, :cond_0

    .line 377
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 379
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 380
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 381
    const/4 v0, 0x1

    .line 385
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 364
    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    .line 366
    const/4 v0, 0x0

    .line 369
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/OrientationActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 437
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->a(Z)V

    .line 439
    return-void

    .line 437
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setVisibleNodata(I)Z
    .locals 4

    .prologue
    .line 505
    const/4 v1, 0x1

    .line 507
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/OrientationActivity;->setVisibleNodata(I)Z

    .line 509
    if-nez p1, :cond_1

    .line 511
    const v0, 0x7f0c0081

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 512
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CountrySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 513
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 518
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 520
    const-string v2, "Search for Country or Region"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 528
    :goto_0
    return v0

    .line 524
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
