.class final Lcom/sec/android/app/samsungapps/commands/ac;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->b(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->b:Z

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Z)Z

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801a6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    new-instance v1, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080351

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0802ef

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/ad;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/ad;-><init>(Lcom/sec/android/app/samsungapps/commands/ac;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ac;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f08023d

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/ae;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/ae;-><init>(Lcom/sec/android/app/samsungapps/commands/ac;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 257
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 259
    :cond_0
    return-void
.end method
