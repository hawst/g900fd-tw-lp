.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/s;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/s;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 235
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 243
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 244
    return-void

    .line 237
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/s;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto :goto_0

    .line 240
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/s;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->KNOX_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
