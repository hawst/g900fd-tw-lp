.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;->_Content:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/b;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;->_Content:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->easybuyPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 41
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 42
    return-void
.end method
