.class public Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;


# instance fields
.field private final INICIS_CANCEL_RESULT_SCHEME:Ljava/lang/String;

.field private final INICIS_RESULT_SCHEME:Ljava/lang/String;

.field bSuccess:Z

.field bUserCanceled:Z

.field message:Ljava/lang/String;

.field noti:Ljava/lang/String;

.field reqUrl:Ljava/lang/String;

.field status:Ljava/lang/String;

.field tid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, "samsungappsinicisresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->INICIS_RESULT_SCHEME:Ljava/lang/String;

    .line 10
    const-string v0, "samsungappsiniciscancelresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->INICIS_CANCEL_RESULT_SCHEME:Ljava/lang/String;

    .line 18
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bUserCanceled:Z

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bSuccess:Z

    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->inicisMobilePurchaseResult(Landroid/net/Uri;)V

    .line 36
    return-void
.end method


# virtual methods
.method public getConfirmURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->reqUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getV3dToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->tid:Ljava/lang/String;

    return-object v0
.end method

.method protected inicisMobilePurchaseResult(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 40
    :try_start_0
    const-string v0, "P_STATUS"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->status:Ljava/lang/String;

    .line 41
    const-string v0, "P_RMESG1"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->message:Ljava/lang/String;

    .line 42
    const-string v0, "P_TID"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->tid:Ljava/lang/String;

    .line 43
    const-string v0, "P_REQ_URL"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->reqUrl:Ljava/lang/String;

    .line 44
    const-string v0, "P_NOTI"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->noti:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :goto_0
    const-string v0, "samsungappsinicisresult://"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bSuccess:Z

    .line 70
    :goto_1
    return-void

    .line 49
    :catchall_0
    move-exception v0

    throw v0

    .line 59
    :cond_0
    const-string v0, "samsungappsiniciscancelresult://"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bUserCanceled:Z

    .line 61
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bSuccess:Z

    goto :goto_1

    .line 65
    :cond_1
    const-string v0, "00"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 66
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bSuccess:Z

    goto :goto_1

    .line 69
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bSuccess:Z

    goto :goto_1

    .line 51
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bSuccess:Z

    return v0
.end method

.method public userCanceled()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;->bUserCanceled:Z

    return v0
.end method
