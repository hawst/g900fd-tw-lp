.class public Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field public addGiftCardnCouponURL:Ljava/lang/String;

.field public baseString:Ljava/lang/String;

.field public countryCode:Ljava/lang/String;

.field public currency:Ljava/lang/String;

.field public exceptionPaymentMethods:Ljava/lang/String;

.field public getGiftCardnCouponURL:Ljava/lang/String;

.field public notiPaymentResultURL:Ljava/lang/String;

.field public offerType:Ljava/lang/String;

.field public paymentMethods:Ljava/lang/String;

.field public paymentType:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public requestOrderURL:Ljava/lang/String;

.field public retrieveTaxURL:Ljava/lang/String;

.field public serviceType:Ljava/lang/String;

.field public signature:Ljava/lang/String;

.field public storeReqeustID:Ljava/lang/String;

.field public tax:Ljava/lang/String;

.field public timeStamp:Ljava/lang/String;

.field public userEmail:Ljava/lang/String;

.field public userId:Ljava/lang/String;

.field public userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 46
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->clear()V

    .line 50
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 42
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method
