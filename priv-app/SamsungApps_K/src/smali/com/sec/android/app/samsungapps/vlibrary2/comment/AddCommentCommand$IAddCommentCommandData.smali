.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract checkRatingAuthorityCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getAddCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getProhibitWords()[Ljava/lang/String;
.end method

.method public abstract getRatingValue()I
.end method

.method public abstract isMyReviewDuplicated()Z
.end method

.method public abstract setDuplicatedMyReview(Z)V
.end method

.method public abstract setProhibitWords([Ljava/lang/String;)V
.end method

.method public abstract validateLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
