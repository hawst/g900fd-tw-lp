.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field private _IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static invokeActivity(Landroid/content/Context;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;)V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;-><init>()V

    .line 48
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 49
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 14
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;-><init>()V

    .line 16
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->_IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->_IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    if-nez v0, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->finish()V

    .line 23
    :goto_0
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->_IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;->onAttached(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->_IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;->onDestroy()V

    .line 36
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 37
    return-void
.end method

.method public final onResult(Z)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->_IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ResponseActivity;->_IActivityInteractor:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IActivityInteractor;->onResult(Z)V

    .line 31
    :cond_0
    return-void
.end method
