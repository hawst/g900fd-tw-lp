.class public Lcom/sec/android/app/samsungapps/base/BaseHandle;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final EXTRA_ENTRY_POINT:Ljava/lang/String; = "ENTRY_POINT"

.field public static final EXTRA_PREVIOUS_PAGE:Ljava/lang/String; = "PREVIOUS_PAGE"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field private d:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field private e:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->c:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->d:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->e:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 23
    if-eqz p1, :cond_0

    :try_start_0
    const-string v0, "fakeModel"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->a:Ljava/lang/String;

    const-string v0, "GOSVERSION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    const-string v0, "PREVIOUS_PAGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->fromName(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->c:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 25
    :cond_1
    if-eqz p1, :cond_2

    const-string v0, "ENTRY_POINT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->fromName(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->e:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 26
    :cond_2
    return-void

    .line 23
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BaseHandle::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public copyToIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 49
    if-eqz p1, :cond_3

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "fakeModel"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 54
    const-string v0, "PREVIOUS_PAGE"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 57
    const-string v0, "ENTRY_POINT"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 61
    const-string v0, "GOSVERSION"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    :cond_3
    return-void
.end method

.method public getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->d:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    return-object v0
.end method

.method public getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->e:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    return-object v0
.end method

.method public getFakeModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getGearOSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->c:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    return-object v0
.end method

.method public setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->d:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 85
    return-void
.end method

.method public setEntryPoint(Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/base/BaseHandle;->e:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 93
    return-void
.end method
