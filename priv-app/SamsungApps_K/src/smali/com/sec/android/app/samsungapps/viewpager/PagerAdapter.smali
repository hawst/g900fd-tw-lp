.class public Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "ProGuard"


# instance fields
.field a:Landroid/support/v4/app/Fragment;

.field b:Landroid/support/v4/app/Fragment;

.field c:Landroid/support/v4/app/Fragment;

.field d:Landroid/content/Context;

.field private e:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 11
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->a:Landroid/support/v4/app/Fragment;

    .line 12
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->b:Landroid/support/v4/app/Fragment;

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->c:Landroid/support/v4/app/Fragment;

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->d:Landroid/content/Context;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->d:Landroid/content/Context;

    .line 23
    if-nez p3, :cond_1

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->a:Landroid/support/v4/app/Fragment;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->b:Landroid/support/v4/app/Fragment;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->c:Landroid/support/v4/app/Fragment;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    if-ne p3, v1, :cond_2

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->b:Landroid/support/v4/app/Fragment;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->c:Landroid/support/v4/app/Fragment;

    .line 35
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    goto :goto_0

    .line 37
    :cond_2
    if-ne p3, v2, :cond_0

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->c:Landroid/support/v4/app/Fragment;

    .line 39
    new-array v0, v1, [I

    const/4 v1, 0x0

    const v2, 0x7f0802f5

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    goto :goto_0

    .line 28
    :array_0
    .array-data 4
        0x7f080027
        0x7f0801f7
        0x7f0802f5
    .end array-data

    .line 35
    :array_1
    .array-data 4
        0x7f080027
        0x7f0802f5
    .end array-data
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    array-length v1, v1

    if-ge p1, v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    aget v1, v1, p1

    .line 53
    const v2, 0x7f080027

    if-ne v1, v2, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->b:Landroid/support/v4/app/Fragment;

    .line 61
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    const v2, 0x7f0801f7

    if-ne v1, v2, :cond_2

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->a:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 57
    :cond_2
    const v2, 0x7f0802f5

    if-ne v1, v2, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->c:Landroid/support/v4/app/Fragment;

    goto :goto_0
.end method

.method public getMainViewFragment()Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->b:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->e:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
