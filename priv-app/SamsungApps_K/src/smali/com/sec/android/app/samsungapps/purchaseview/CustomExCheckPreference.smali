.class public abstract Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;
.super Landroid/preference/Preference;
.source "ProGuard"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field protected mCategory:Landroid/preference/PreferenceCategory;

.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/PreferenceCategory;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->mView:Landroid/view/View;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->mCategory:Landroid/preference/PreferenceCategory;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->mCategory:Landroid/preference/PreferenceCategory;

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->init()V

    .line 44
    return-void
.end method


# virtual methods
.method protected abstract customGetView(Landroid/view/View;)V
.end method

.method protected abstract getLayoutResouceID()I
.end method

.method public init()V
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->getLayoutResouceID()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->setLayoutResource(I)V

    .line 49
    invoke-virtual {p0, p0}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 50
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->mView:Landroid/view/View;

    .line 61
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->customGetView(Landroid/view/View;)V

    .line 62
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v3

    move v1, v2

    .line 75
    :goto_0
    if-ge v1, v3, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->mCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;

    .line 78
    if-eq v0, p1, :cond_0

    .line 80
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->setSel(Z)V

    .line 75
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 87
    :cond_1
    check-cast p1, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;

    .line 88
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/purchaseview/CustomExCheckPreference;->setSel(Z)V

    .line 90
    return v4
.end method

.method protected abstract setSel(Z)V
.end method
