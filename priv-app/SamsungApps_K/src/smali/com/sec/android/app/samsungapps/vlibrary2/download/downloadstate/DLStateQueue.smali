.class public Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;


# instance fields
.field map:Ljava/util/HashMap;

.field observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->map:Ljava/util/HashMap;

    .line 15
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    .line 23
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    return-object v0
.end method

.method private setDLFinishState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 130
    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    .line 134
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)V

    .line 135
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->removeAllObserver()V

    .line 136
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 137
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setInstance(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;)V
    .locals 0

    .prologue
    .line 59
    sput-object p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    .line 60
    return-void
.end method


# virtual methods
.method public addDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)V

    .line 146
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;

    .line 66
    if-ne v0, p1, :cond_0

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 2

    .prologue
    .line 31
    monitor-enter p0

    .line 33
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->map:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->notifyDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 35
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    .locals 1

    .prologue
    .line 48
    if-nez p1, :cond_0

    .line 50
    const/4 v0, 0x0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    goto :goto_0
.end method

.method protected notifyDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;

    .line 83
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;->onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method

.method protected notifyDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;

    .line 91
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;->onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method public removeDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 154
    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 159
    :goto_0
    return v0

    .line 158
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)V

    .line 159
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method public removeStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 2

    .prologue
    .line 39
    monitor-enter p0

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->map:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->notifyDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 43
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDownloadCanceled(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDLFinishState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    move-result v0

    return v0
.end method

.method public setDownloadFailed(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADINGFAILED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDLFinishState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    move-result v0

    return v0
.end method

.method public setDownloadFinished(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDLFinishState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    move-result v0

    return v0
.end method

.method public setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;II)V
    .locals 2

    .prologue
    .line 164
    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    if-eq v0, v1, :cond_0

    .line 168
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-virtual {p1, v0, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;II)V

    .line 171
    :cond_0
    return-void
.end method

.method public setDownloadState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 104
    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    .line 108
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)V

    .line 109
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->map:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method
