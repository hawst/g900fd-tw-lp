.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract doesSupportPhoneFeature()Z
.end method

.method public abstract getConnectedNetworkType()I
.end method

.method public abstract getDeiviceWidth()I
.end method

.method public abstract getDeviceDensity()F
.end method

.method public abstract getDeviceHeight()I
.end method

.method public abstract getDeviceLCDSize()I
.end method

.method public abstract getExtraPhoneType()I
.end method

.method public abstract getIMEI()Ljava/lang/String;
.end method

.method public abstract getIMSI()Ljava/lang/String;
.end method

.method public abstract getMSISDN()Ljava/lang/String;
.end method

.method public abstract getModelName()Ljava/lang/String;
.end method

.method public abstract getNetwrokType()I
.end method

.method public abstract getOpenApiVersion()Ljava/lang/String;
.end method

.method public abstract isAirplaneMode()Z
.end method

.method public abstract isBlackTheme()Z
.end method

.method public abstract isConnectedDataNetwork()Z
.end method

.method public abstract isPhoneNumberReadable()Z
.end method

.method public abstract isViaNetwork()Z
.end method

.method public abstract isWIFIConnected()Z
.end method

.method public abstract isWibroConnected()Z
.end method

.method public abstract loadODCVersion()Ljava/lang/String;
.end method

.method public abstract loadODCVersionCode()I
.end method

.method public abstract readCSC()Ljava/lang/String;
.end method

.method public abstract readMCC()Ljava/lang/String;
.end method

.method public abstract readMNC()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;
.end method
