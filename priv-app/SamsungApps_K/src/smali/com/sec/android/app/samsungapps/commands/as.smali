.class final Lcom/sec/android/app/samsungapps/commands/as;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/as;->a:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/as;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/as;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/as;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/as;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected final impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 5

    .prologue
    .line 102
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/as;->_Context:Landroid/content/Context;

    const v3, 0x7f08015c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/as;->_Context:Landroid/content/Context;

    const v4, 0x7f080179

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/as;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/at;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/at;-><init>(Lcom/sec/android/app/samsungapps/commands/as;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/as;->_Context:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/au;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/au;-><init>(Lcom/sec/android/app/samsungapps/commands/as;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 126
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 127
    return-void
.end method
