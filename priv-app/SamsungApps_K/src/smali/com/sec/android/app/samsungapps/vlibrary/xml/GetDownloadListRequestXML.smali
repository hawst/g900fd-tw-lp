.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# instance fields
.field protected mPostloadCount:I

.field protected mPostloadapp:Ljava/lang/String;

.field protected mPreloadApp:Ljava/lang/String;

.field protected mPreloadCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 20
    const-string v2, "getDownloadList"

    const-string v3, "2309"

    move-object v0, p0

    move-object v1, p2

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 21
    const-string v0, "startNum"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v0, "endNum"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const-string v0, "imei"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 26
    const-string v0, "contentType"

    invoke-interface {p3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    invoke-interface {p3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;->getSortOrder()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 29
    const-string v0, "alignOrder"

    invoke-interface {p3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;->getSortOrder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addPrePostAppListParams(Landroid/content/Context;)V

    .line 37
    return-void

    .line 33
    :cond_0
    const-string v0, "alignOrder"

    const-string v1, "updated"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private addPrePostAppListParams(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;-><init>(Landroid/content/Context;)V

    .line 42
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPrePostPackageInfo()Ljava/util/Vector;

    move-result-object v1

    .line 44
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPreloadCount:I

    .line 45
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPostloadCount:I

    .line 46
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPreloadApp:Ljava/lang/String;

    .line 47
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPostloadapp:Ljava/lang/String;

    .line 49
    const-string v0, "preloadCount"

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPreloadCount:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v0, "postloadCount"

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPostloadCount:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v0, "preloadApp"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPreloadApp:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v0, "postloadApp"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->mPostloadapp:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method
