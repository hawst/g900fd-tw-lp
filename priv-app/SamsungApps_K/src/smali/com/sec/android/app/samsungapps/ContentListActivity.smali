.class public Lcom/sec/android/app/samsungapps/ContentListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;


# static fields
.field protected static final STATUS_ALL:I = 0x0

.field protected static final STATUS_FREE:I = 0x1

.field protected static final STATUS_NEW:I = 0x3

.field protected static final STATUS_PAID:I = 0x2


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/ListViewController;

.field private b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

.field private c:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field i:Ljava/lang/Boolean;

.field protected mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field protected mIsFromFeatured:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 40
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->i:Ljava/lang/Boolean;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mIsFromFeatured:Z

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/CustomDialogBuilder;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V
    .locals 2

    .prologue
    .line 335
    const v0, 0x7f08023d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/bw;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/bw;-><init>(Lcom/sec/android/app/samsungapps/ContentListActivity;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 341
    const v0, 0x7f0802ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/bx;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/bx;-><init>(Lcom/sec/android/app/samsungapps/ContentListActivity;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 347
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 348
    return-void
.end method


# virtual methods
.method protected getContentArrayAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    return-object v0
.end method

.method public getCurrButtonPosition()I
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    if-nez v0, :cond_0

    .line 375
    const/4 v0, 0x0

    .line 378
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->getCurrButtonPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getQueryType()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DownloaderStateChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onDownloaderStateChanged()V

    .line 278
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_1

    .line 279
    sget-object v0, Lcom/sec/android/app/samsungapps/by;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getUpdateAppEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->getUpdateAppEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 288
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_3

    .line 289
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedIn:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_3

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_3

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    .line 296
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    return v0

    .line 282
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 259
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 260
    sget-object v0, Lcom/sec/android/app/samsungapps/by;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getMyTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;->getMyTabEventType()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 262
    :pswitch_0
    const-string v0, "HideUpdateAllButton"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 265
    :pswitch_1
    const-string v0, "ShowUpdateAllButton"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleYesOrNoEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const v2, 0xff40

    .line 325
    sget-object v0, Lcom/sec/android/app/samsungapps/by;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->getYesOrNoEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 331
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 328
    :pswitch_0
    if-nez p2, :cond_0

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->a(Lcom/sec/android/app/samsungapps/CustomDialogBuilder;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    .line 329
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 328
    :cond_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->a(Lcom/sec/android/app/samsungapps/CustomDialogBuilder;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    goto :goto_1

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected manageListButtons()V
    .locals 5

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->c:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->getQueryType()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->manageButtonStates(Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;Landroid/view/View;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 255
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->release()V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->release()V

    .line 116
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 121
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    .line 122
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 123
    return-void
.end method

.method protected onDownloaderStateChanged()V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 306
    packed-switch p1, :pswitch_data_0

    .line 313
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNavigationActionBar(ILandroid/view/View;)V

    .line 314
    :goto_0
    return-void

    .line 309
    :pswitch_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0xa0001
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 104
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onPause()V

    .line 105
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 92
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 93
    return-void
.end method

.method public setEmptyText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0083

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 239
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    :cond_0
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 242
    if-eqz v0, :cond_1

    .line 243
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 245
    :cond_1
    return-void
.end method

.method public setListViewButtonState(Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->c:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 249
    return-void
.end method

.method public setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const v1, 0x7f0c0186

    const v2, 0x7f0c0078

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v3, 0x7f0c0078

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    if-nez v3, :cond_2

    new-instance v3, Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const v2, 0x7f0c016a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setMoreView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->isMoreLoadingViewShown()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->showMoreLoadingView(Z)V

    new-instance v2, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    new-instance v3, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    new-instance v4, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    new-instance v5, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v5}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v6, 0x7f0c0083

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v7, 0x7f0c007e

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v7, 0x7f0c0080

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v6, 0x7f0c007f

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v6, 0x7f0c0084

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    const v6, 0x7f0c0086

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    new-instance v6, Lcom/sec/android/app/samsungapps/bv;

    invoke-direct {v6, p0}, Lcom/sec/android/app/samsungapps/bv;-><init>(Lcom/sec/android/app/samsungapps/ContentListActivity;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setLoadingEmptyView()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setArrayAdapter(Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    if-nez v0, :cond_1

    new-instance v2, Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    move-object v0, v1

    check-cast v0, Landroid/widget/AbsListView;

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/AbsListView;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    .line 75
    :cond_1
    return-void

    .line 74
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setListView(Landroid/widget/AbsListView;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->i:Ljava/lang/Boolean;

    move-object v2, v3

    move-object v8, v0

    move v0, v1

    move-object v1, v8

    :goto_2
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_3

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    if-nez v1, :cond_5

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ListViewController;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    move-object v1, v2

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    move-object v2, v3

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setListView(Landroid/widget/AbsListView;)V

    move-object v1, v2

    goto/16 :goto_1
.end method

.method public setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->i:Ljava/lang/Boolean;

    .line 67
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 68
    return-void
.end method

.method protected setMainView(I)V
    .locals 0

    .prologue
    .line 317
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->setMainViewAndEmptyView(I)V

    .line 318
    return-void
.end method

.method protected setMainView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 321
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->setMainViewAndEmptyView(Landroid/view/View;)V

    .line 322
    return-void
.end method

.method protected setTabTextOnConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentListActivity;->b:Lcom/sec/android/app/samsungapps/view/ListButtonManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->setTabTextSize(Landroid/content/res/Configuration;)V

    .line 132
    return-void
.end method

.method public setVisibleLoading(I)Z
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 228
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 230
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 231
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 233
    const/4 v0, 0x0

    return v0
.end method
