.class final Lcom/sec/android/app/samsungapps/view/y;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/x;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/x;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfirmNameAgeResult(Z)V
    .locals 3

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    .line 131
    const-string v0, "kkangna"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "result 1 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-boolean v2, v2, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->requestAdultCheck()V

    .line 139
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    .line 137
    const-string v0, "kkangna"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "result 2 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/y;->a:Lcom/sec/android/app/samsungapps/view/x;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/view/x;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-boolean v2, v2, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
