.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public ForceUpdate:I

.field public _25gLimit:I

.field public _2gLimit:I

.field public _3gLimit:I

.field public _4gLimit:I

.field public _wifiLimit:I

.field public addMobClsf:Z

.field private bLoadSuccess:Z

.field public bdSize:I

.field public bdUrl:Ljava/lang/String;

.field public categoryTitle:Ljava/lang/String;

.field public chartTitle:Ljava/lang/String;

.field public clientType:Ljava/lang/String;

.field public contentSizeLimitation:I

.field public currencyUnitDivision:Z

.field public disableBaidu:Z

.field public disclaimerVer:Ljava/lang/String;

.field public enableTencent:I

.field public flexibleTabClsf:I

.field public flexibleTabName:Ljava/lang/String;

.field public freeStoreClsf:I

.field public freeTabClsf:I

.field public minPrice:Ljava/lang/String;

.field public notificationType:I

.field objToReceive:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

.field public odcConfigurationId:Ljava/lang/String;

.field public odcSize:I

.field public odcUrl:Ljava/lang/String;

.field public offset:I

.field private oldFlexibleTabClsf:Z

.field public secureTime:Ljava/lang/String;

.field public serverLoadLevel:I

.field public snsVal:I

.field public staffPicksTitle:Ljava/lang/String;

.field public upgrade:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcUrl:Ljava/lang/String;

    .line 14
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcSize:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_2gLimit:I

    .line 37
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_25gLimit:I

    .line 38
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_3gLimit:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_4gLimit:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_wifiLimit:I

    .line 42
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disableBaidu:Z

    .line 43
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->ForceUpdate:I

    .line 44
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->enableTencent:I

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->bLoadSuccess:Z

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->objToReceive:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 56
    return-void
.end method

.method private notifyFlextibleTabChanged()V
    .locals 1

    .prologue
    .line 184
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->flexibleTabChanged()V

    .line 185
    return-void
.end method


# virtual methods
.method protected checkNewDisclaimer()Z
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->isDisclaimerAlreadyAcceptedByUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    .line 192
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->isDiclamerUpdated()Z

    move-result v0

    goto :goto_0
.end method

.method public getCategoryTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->categoryTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getChartTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->chartTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getClientType()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->clientType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->clientType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 248
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    .line 266
    :goto_0
    return-object v0

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->clientType:Ljava/lang/String;

    const-string v1, "unc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    goto :goto_0

    .line 254
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->clientType:Ljava/lang/String;

    const-string v1, "una"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    goto :goto_0

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->clientType:Ljava/lang/String;

    const-string v1, "odc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ODC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    goto :goto_0

    .line 262
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->clientType:Ljava/lang/String;

    const-string v1, "open"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->Open:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    goto :goto_0

    .line 266
    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    goto :goto_0
.end method

.method public getFlexibleTabName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 231
    const-string v0, ""

    .line 233
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getForceUpdate()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->ForceUpdate:I

    packed-switch v0, :pswitch_data_0

    .line 281
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->AllowNotUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    :goto_0
    return-object v0

    .line 273
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->PossibleToUseWithoutUpdateAndDoesNotShowInDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    goto :goto_0

    .line 275
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ForceUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    goto :goto_0

    .line 277
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ForceUpdateAndDeepLinkUpdateVisible:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    goto :goto_0

    .line 279
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->AllowNotUpdate:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getPoweredBy()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->enableTencent:I

    packed-switch v0, :pswitch_data_0

    .line 307
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->SAMSUNG:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    :goto_0
    return-object v0

    .line 303
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->SINA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    goto :goto_0

    .line 305
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->TENCENT:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    goto :goto_0

    .line 300
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getServerDisclaimerVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disclaimerVer:Ljava/lang/String;

    return-object v0
.end method

.method public getStaffPicksTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->staffPicksTitle:Ljava/lang/String;

    return-object v0
.end method

.method protected isDiclamerUpdated()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;-><init>()V

    .line 208
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->load(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V

    .line 209
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/l;->a:[I

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disclaimerVer:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->compareVersion(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 220
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 214
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 218
    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected isDisclaimerAlreadyAcceptedByUser()Z
    .locals 2

    .prologue
    .line 200
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;-><init>()V

    .line 201
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->load(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V

    .line 202
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->isAgreed()Z

    move-result v0

    return v0
.end method

.method public isLoadSuccess()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->bLoadSuccess:Z

    return v0
.end method

.method public load(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V
    .locals 2

    .prologue
    .line 66
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFreeStoreClsf()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeStoreClsf:I

    .line 67
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getDisclaimerVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disclaimerVer:Ljava/lang/String;

    .line 68
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFreeTabClsf()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeTabClsf:I

    .line 69
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getSNSVal()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->snsVal:I

    .line 73
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFlexibleTabClsf()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabClsf:I

    .line 76
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFlexibleTabName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabName:Ljava/lang/String;

    .line 84
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getUnitDivision()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->currencyUnitDivision:Z

    .line 85
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->get2GLimit()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_2gLimit:I

    .line 86
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->get25GLimit()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_25gLimit:I

    .line 87
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->get3GLimit()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_3gLimit:I

    .line 88
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->get4GLimit()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_4gLimit:I

    .line 89
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getWifiLimit()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_wifiLimit:I

    .line 91
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->readServerLoadLevel()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->serverLoadLevel:I

    .line 92
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->serverLoadLevel:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->bLoadSuccess:Z

    .line 101
    :goto_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getEnableTencent()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->enableTencent:I

    .line 102
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->serverLoadLevel:I

    goto :goto_0
.end method

.method public odcNeedsUpdating()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 128
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->upgrade:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->upgrade:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public save()V
    .locals 2

    .prologue
    .line 106
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disclaimerVer:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setDisclaimerVersion(Ljava/lang/String;)V

    .line 110
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeTabClsf:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setFreeTabClsf(I)V

    .line 111
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->snsVal:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeSNSVal(I)V

    .line 112
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabClsf:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeFlexibleTabClsf(I)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeFlexibleTabName(Ljava/lang/String;)V

    .line 114
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->currencyUnitDivision:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeUnitDivision(Z)V

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->minPrice:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeMinPrice(Ljava/lang/String;)V

    .line 117
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_2gLimit:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->write2GLimit(I)V

    .line 118
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_25gLimit:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->write25GLimit(I)V

    .line 119
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_3gLimit:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->write3GLimit(I)V

    .line 120
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_4gLimit:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->write4GLimit(I)V

    .line 121
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_wifiLimit:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeWifiLimit(I)V

    .line 122
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->serverLoadLevel:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeServerLoadLeverl(I)V

    .line 123
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->enableTencent:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeEnableTencent(I)V

    .line 124
    return-void
.end method

.method public supportFlexibleTab()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 225
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabClsf:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unaNeedsUpdating()Z
    .locals 2

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->upgrade:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->upgrade:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
