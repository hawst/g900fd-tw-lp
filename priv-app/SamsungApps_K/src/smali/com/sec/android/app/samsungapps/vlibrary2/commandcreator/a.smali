.class final Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final downloadEnded(Z)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public final getDownloadURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    return-object v0
.end method

.method public final getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 3

    .prologue
    .line 69
    const-wide/16 v0, 0x0

    .line 71
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 75
    :goto_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    return-object v2

    .line 72
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final getSaveFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getConvertedFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final updateDownloadedSize(J)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method
