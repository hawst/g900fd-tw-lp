.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 26
    const-string v2, "CyberCashPurchase"

    const-string v3, "3110"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 27
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "couponIssuedSEQ"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getCouponIssuedSEQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 30
    const-string v0, "imei"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 34
    :goto_0
    const-string v0, "guid"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getProductGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v0, "paymentAmountPrice"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getPaymentPrice()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v0, "paymentMethodID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getPaymentMethodID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 37
    const-string v0, "cashID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getCashID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 38
    const-string v0, "cashPW"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;->getCashPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 39
    const-string v0, "rentalTerm"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    return-void

    .line 32
    :cond_0
    const-string v0, "imei"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method
