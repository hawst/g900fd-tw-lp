.class final Lcom/sec/android/app/samsungapps/initializer/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInitializeFailed(Z)V
    .locals 3

    .prologue
    .line 169
    if-eqz p1, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->startInitializing()V

    .line 187
    :goto_0
    return-void

    .line 178
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->g(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;->onInitFailed()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->b(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    goto :goto_0

    .line 180
    :catch_0
    move-exception v0

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppsInitProcess::onCommandResult::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onInitializeSuccess()V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->c(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->d(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->e(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->clearBadgeCount(Landroid/content/Context;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->clearUnaSetting(Landroid/content/Context;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->setInitialized(Landroid/content/Context;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getClientType()Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string v1, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCommandResult>>>>>>>> client Type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 146
    const-string v0, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->f(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->a(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->b(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    .line 164
    :goto_0
    return-void

    .line 156
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/d;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->g(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;->onInitCompleted()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppsInitProcess::onCommandResult::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
