.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentLikeUnlike;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field likeCount:I

.field userlikeYn:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    if-nez p1, :cond_1

    .line 39
    const-string v0, "ContentLIkeUnlike:: key is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->w(Ljava/lang/String;)V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 44
    const-string v1, "userlikeYn"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 46
    invoke-static {p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->strToBool(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;->userlikeYn:Z

    .line 48
    :cond_2
    const-string v1, "likeCount"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 50
    invoke-static {p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->strToInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;->likeCount:I

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLikeCount()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;->likeCount:I

    return v0
.end method

.method public getUserLikeYn()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;->userlikeYn:Z

    return v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method
