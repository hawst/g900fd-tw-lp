.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final TYPE_NETWORK_ERROR_DATA_REACHED_TO_LIMIT:I = 0x3

.field public static final TYPE_NETWORK_ERROR_DATA_ROAMING_OFF:I = 0x2

.field public static final TYPE_NETWORK_ERROR_FLIGHT_MODE:I = 0x0

.field public static final TYPE_NETWORK_ERROR_MOBILE_DATA_OFF:I = 0x1

.field public static final TYPE_NETWORK_ERROR_NO_SIGNAL:I = 0x4


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private _getServiceState()I
    .locals 5

    .prologue
    .line 157
    const/4 v1, -0x1

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 164
    :try_start_0
    const-class v2, Landroid/telephony/TelephonyManager;

    const-string v3, "getServiceState"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 165
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 168
    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4

    move-result v0

    .line 184
    :goto_0
    return v0

    .line 171
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "getServiceState::IllegalArgumentException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 176
    goto :goto_0

    .line 173
    :catch_1
    move-exception v0

    const-string v0, "getServiceState::IllegalAccessException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 176
    goto :goto_0

    .line 175
    :catch_2
    move-exception v0

    const-string v0, "getServiceState::InvocationTargetException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move v0, v1

    .line 182
    goto :goto_0

    .line 179
    :catch_3
    move-exception v0

    const-string v0, "getServiceState::SecurityException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 182
    goto :goto_0

    .line 181
    :catch_4
    move-exception v0

    const-string v0, "getServiceState::NoSuchMethodException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method private getMobileDataServiceState()I
    .locals 5

    .prologue
    .line 124
    const/4 v1, -0x1

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 130
    :try_start_0
    const-class v2, Landroid/telephony/TelephonyManager;

    const-string v3, "getDataServiceState"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 131
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 135
    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4

    move-result v0

    .line 152
    :goto_0
    return v0

    .line 138
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "getMobileDataServiceState::IllegalArgumentException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 143
    goto :goto_0

    .line 140
    :catch_1
    move-exception v0

    const-string v0, "getMobileDataServiceState::IllegalAccessException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 143
    goto :goto_0

    .line 142
    :catch_2
    move-exception v0

    const-string v0, "getMobileDataServiceState::InvocationTargetException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move v0, v1

    .line 151
    goto :goto_0

    .line 146
    :catch_3
    move-exception v0

    const-string v0, "getMobileDataServiceState::SecurityException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 151
    goto :goto_0

    .line 148
    :catch_4
    move-exception v0

    const-string v0, "getMobileDataServiceState::NoSuchMethodException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->_getServiceState()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getNetworkErrorType()I
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 266
    .line 268
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v2

    .line 269
    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getConnectedNetworkType()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 271
    const-string v0, "NetworkError::getNetworkErrorType::network is connected"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 303
    :goto_0
    return v1

    .line 275
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isFlightModeOn()Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 277
    const-string v0, "NetworkError::getNetworkErrorType::isFlightModeOn is true"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    :goto_1
    move v1, v0

    .line 303
    goto :goto_0

    .line 280
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isMobileDataOn()Z

    move-result v2

    if-nez v2, :cond_2

    .line 282
    const-string v1, "NetworkError::getNetworkErrorType::isMobileDataOn is false"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 285
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isRoamingNetwork()Z

    move-result v2

    if-ne v2, v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isRoamingDataOn()Z

    move-result v2

    if-nez v2, :cond_3

    .line 288
    const-string v0, "NetworkError::getNetworkErrorType::isRoamingDataOn is false"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 289
    const/4 v0, 0x2

    goto :goto_1

    .line 291
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isLimitedMobileData()Z

    move-result v2

    if-ne v2, v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isReachedToLimitMobileData()Z

    move-result v2

    if-nez v2, :cond_4

    .line 294
    const-string v0, "NetworkError::getNetworkErrorType::isLimitedMobileData&isReachedToLimitMobileData"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 295
    const/4 v0, 0x3

    goto :goto_1

    .line 297
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->isNoSignalMobileData()Z

    move-result v2

    if-ne v2, v0, :cond_5

    .line 299
    const-string v0, "NetworkError::getNetworkErrorType::isNoSignalMobileData"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 300
    const/4 v0, 0x4

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public isFlightModeOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 37
    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 42
    if-eqz v1, :cond_0

    .line 44
    const/4 v0, 0x1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 46
    :catch_0
    move-exception v1

    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isFlightModeOn:: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isLimitedMobileData()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 189
    .line 190
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->getMobileDataServiceState()I

    move-result v1

    .line 191
    packed-switch v1, :pswitch_data_0

    .line 202
    :goto_0
    :pswitch_0
    return v0

    .line 200
    :pswitch_1
    const-string v0, "NetworkError::isLimitedMobileData::STATE_EMERGENCY_ONLY"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x1

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isMobileDataOn()Z
    .locals 4

    .prologue
    .line 55
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 60
    :try_start_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 69
    :goto_0
    return v0

    .line 63
    :catch_0
    move-exception v0

    const-string v0, "isMobileDataOn::SecurityException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 67
    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isMobileDataOn::Exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public isNoSignalMobileData()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 211
    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->getMobileDataServiceState()I

    move-result v1

    .line 213
    packed-switch v1, :pswitch_data_0

    .line 224
    :goto_0
    :pswitch_0
    return v0

    .line 219
    :pswitch_1
    const-string v0, "NetworkError::isNoSignalMobileData::STATE_OUT_OF_SERVICE"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 220
    const/4 v0, 0x1

    .line 221
    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isReachedToLimitMobileData()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 234
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 240
    :try_start_0
    const-class v2, Landroid/net/ConnectivityManager;

    const-string v3, "isMobilePolicyDataEnable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 241
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 245
    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4

    move-result v0

    .line 261
    :goto_0
    return v0

    .line 248
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "isReachedToLimitMobileData::IllegalArgumentException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 253
    goto :goto_0

    .line 250
    :catch_1
    move-exception v0

    const-string v0, "isReachedToLimitMobileData::IllegalAccessException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 253
    goto :goto_0

    .line 252
    :catch_2
    move-exception v0

    const-string v0, "isReachedToLimitMobileData::InvocationTargetException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move v0, v1

    .line 259
    goto :goto_0

    .line 256
    :catch_3
    move-exception v0

    const-string v0, "isReachedToLimitMobileData::SecurityException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 259
    goto :goto_0

    .line 258
    :catch_4
    move-exception v0

    const-string v0, "isReachedToLimitMobileData::NoSuchMethodException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public isRoamingDataOn()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 82
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 87
    :try_start_0
    const-class v2, Landroid/telephony/TelephonyManager;

    const-string v3, "getDataRoamingEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 88
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 92
    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4

    move-result v0

    .line 107
    :goto_0
    return v0

    .line 95
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "isRoamingDataOn::IllegalArgumentException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 100
    goto :goto_0

    .line 97
    :catch_1
    move-exception v0

    const-string v0, "isRoamingDataOn::IllegalAccessException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 100
    goto :goto_0

    .line 99
    :catch_2
    move-exception v0

    const-string v0, "isRoamingDataOn::InvocationTargetException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    move v0, v1

    .line 106
    goto :goto_0

    .line 103
    :catch_3
    move-exception v0

    const-string v0, "isRoamingDataOn::SecurityException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 106
    goto :goto_0

    .line 105
    :catch_4
    move-exception v0

    const-string v0, "isRoamingDataOn::NoSuchMethodException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public isRoamingNetwork()Z
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 76
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    .line 77
    return v0
.end method

.method public setDataRoamingEnabled(Z)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 113
    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->setDataRoamingEnabled(Z)V

    .line 114
    return-void
.end method
