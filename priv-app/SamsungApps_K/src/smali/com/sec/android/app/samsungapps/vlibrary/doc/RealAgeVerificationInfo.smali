.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mFirstName:Ljava/lang/String;

.field mLastName:Ljava/lang/String;

.field mSSN:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mFirstName:Ljava/lang/String;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mLastName:Ljava/lang/String;

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mSSN:Ljava/lang/String;

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mFirstName:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mLastName:Ljava/lang/String;

    .line 11
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mSSN:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getSSN()Ljava/lang/String;
    .locals 7

    .prologue
    .line 26
    const-string v1, ""

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;->mSSN:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 30
    array-length v3, v2

    .line 31
    const/4 v0, 0x0

    move v6, v0

    move-object v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v3, :cond_1

    .line 33
    aget-char v4, v2, v1

    const/16 v5, 0x30

    if-lt v4, v5, :cond_0

    aget-char v4, v2, v1

    const/16 v5, 0x39

    if-gt v4, v5, :cond_0

    .line 35
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-char v4, v2, v1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 31
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    :cond_1
    return-object v0
.end method
