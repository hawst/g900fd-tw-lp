.class public Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculatorCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createFreePrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)V

    return-object v0
.end method

.method public static createPaidPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)V

    return-object v0
.end method
