.class public Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;
.super Lcom/sec/android/app/samsungapps/ContentListActivity;
.source "ProGuard"


# static fields
.field public static final EXTRA_SELLERID:Ljava/lang/String; = "_sellerID"

.field public static final EXTRA_SELLERNAME:Ljava/lang/String; = "_sellerName"

.field public static final EXTRA_TITLETEXT:Ljava/lang/String; = "_titleText"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->f:Z

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const v5, 0xb0001

    const v7, 0xa0005

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 101
    const v0, 0x7f04005a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setMainView(I)V

    .line 103
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->f:Z

    if-eqz v0, :cond_1

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->e:Ljava/lang/String;

    new-array v4, v3, [I

    aput v7, v4, v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 110
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getContentArrayAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getContentArrayAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08015d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setEmptyText(Ljava/lang/String;)V

    .line 114
    return-void

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    new-array v4, v3, [I

    aput v7, v4, v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 158
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    new-instance v2, Lcom/sec/android/app/samsungapps/hf;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/hf;-><init>(Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->sellerProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 165
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 166
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->getSellerDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->getSellerDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;->sellerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->e:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v4, v3, [I

    const/4 v0, 0x0

    const v5, 0xa0005

    aput v5, v4, v0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 192
    sget-object v0, Lcom/sec/android/app/samsungapps/hh;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 195
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public invalidateRelatedDetailUpdate()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/sec/android/app/samsungapps/hd;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hd;-><init>(Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->getProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/he;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/he;-><init>(Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 138
    :cond_0
    return-void
.end method

.method protected onCompleteSellerDetail(Z)V
    .locals 1

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->createSellerProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 187
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a()V

    .line 122
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 42
    const-string v1, "_sellerID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->c:Ljava/lang/String;

    .line 44
    const-string v1, "_sellerName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "_titleText"

    const v3, 0x7f0801eb

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isDeepLink"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->f:Z

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a()V

    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->f:Z

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 58
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b()V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->reqeustSellerDetail()V

    .line 82
    :cond_1
    :goto_0
    return-void

    .line 66
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cdcontainer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-nez v0, :cond_3

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->finish()V

    goto :goto_0

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 72
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b()V

    goto :goto_0

    .line 74
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 75
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->b()V

    goto :goto_0

    .line 78
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->invalidateRelatedDetailUpdate()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onDestroy()V

    .line 205
    return-void
.end method

.method protected reqeustSellerDetail()V
    .locals 3

    .prologue
    .line 170
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;

    new-instance v2, Lcom/sec/android/app/samsungapps/hg;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/hg;-><init>(Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->sellerDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 179
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 180
    return-void
.end method
