.class public Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field protected static final INIT_FAILED:I = 0x1

.field protected static final LOGIN_FAILED:I = 0x2

.field protected static final REQUEST_FAILED:I = 0x3


# instance fields
.field a:Landroid/os/Handler;

.field b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;

.field private h:I

.field private i:I

.field private j:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    .line 114
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->a:Landroid/os/Handler;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->e:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    .line 37
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->j:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;I)I
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->i:I

    return p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/c;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 161
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)V

    .line 48
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;)V

    goto :goto_0

    .line 68
    :pswitch_1
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->h:I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;->getResult()Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->h:I
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->CALC_UPDATE_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)V

    goto :goto_0

    .line 71
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->execute()V

    goto :goto_0

    .line 74
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->i:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;->onUpdateCheckFailed(I)V

    goto :goto_0

    .line 77
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->j:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->e:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;->createChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;->check(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 80
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->h:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;->onUpdateCheckSuccess(I)V

    goto :goto_0

    .line 83
    :pswitch_6
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->i:I

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    .line 68
    :catch_1
    move-exception v0

    goto :goto_1

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;

    .line 43
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    .line 53
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;)V

    return-void
.end method
