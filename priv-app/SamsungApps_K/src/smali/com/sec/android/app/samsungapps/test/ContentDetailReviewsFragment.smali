.class public Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->setWidget(Lcom/sec/android/app/samsungapps/widget/CommonWidget;)V

    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreate(Landroid/os/Bundle;)V

    .line 20
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    return-object v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->release()V

    .line 48
    return-void
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public setResizeInMultiWindow()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method
