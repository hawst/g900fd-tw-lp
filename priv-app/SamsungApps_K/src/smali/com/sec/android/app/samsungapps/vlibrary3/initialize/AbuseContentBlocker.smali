.class public Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->b:Landroid/content/Context;

    .line 29
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)Z

    .line 41
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;)V
    .locals 3

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 71
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRestrictedAge()I

    move-result v0

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->KOREA_AND_RESTRICTED_CONTENT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->DOESNT_MATCH_CONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0

    .line 74
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->NEED_TO_IDENTIFY_USER:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getRealAge()I

    move-result v0

    const/16 v1, 0x13

    if-lt v0, v1, :cond_3

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGEDIN_AND_OVER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGEDIN_AND_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->NOT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0

    .line 77
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->closeWindowByRestrictedContent()V

    goto :goto_0

    .line 80
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->showDetailnotRestrictedContent()V

    goto :goto_0

    .line 83
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->onShowNeedToLoginToCheckRestrictedApp()V

    goto/16 :goto_0

    .line 86
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->b:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto/16 :goto_0

    .line 89
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->onAskPopupForValidateUserAge()V

    goto/16 :goto_0

    .line 92
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->execute()V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->VERIFICATION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto/16 :goto_0

    .line 95
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->closeWindowByRestrictedContentUnder19()V

    goto/16 :goto_0

    .line 98
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->closeWindowByLogedOut()V

    goto/16 :goto_0

    .line 101
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;->closeWindowWithoutNotice()V

    goto/16 :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;)V

    return-void
.end method

.method public onContentDetailLoaded(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 50
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 53
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    .line 59
    :cond_0
    return-void
.end method

.method public onLogedOut()V
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    .line 155
    return-void
.end method

.method public onUserAgree(Z)V
    .locals 1

    .prologue
    .line 256
    if-eqz p1, :cond_0

    .line 258
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)V

    goto :goto_0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->d:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;

    .line 36
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    .line 46
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;)V

    return-void
.end method
