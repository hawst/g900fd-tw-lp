.class public Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

.field private _ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;->startLoading()V

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getCreditCardInfo(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 47
    return-void
.end method
