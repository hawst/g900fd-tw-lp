.class public Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _Observer:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;

.field protected mItemArray:Ljava/util/ArrayList;

.field protected mItemMap:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    .line 114
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;)Z
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->_Observer:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->_Observer:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;->onItemAdded(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;)V

    .line 26
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public empty()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findContentByPackageName(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    .line 78
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 83
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findContentByProductID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    return-object v0
.end method

.method public get(I)Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    return-object v0
.end method

.method public getFirst()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 55
    :goto_0
    return-object v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    goto :goto_0
.end method

.method public remove(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->_Observer:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->_Observer:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;->onItemRemoved(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;)V

    .line 36
    :cond_0
    return-void
.end method

.method public remove(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    .line 99
    if-nez v0, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 106
    :goto_0
    return v0

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 106
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeFirst()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->empty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    :goto_0
    return v0

    .line 65
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->remove(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;)V

    .line 66
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->_Observer:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue$QueueObserver;

    .line 112
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectProductIDQueue;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
