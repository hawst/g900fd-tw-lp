.class final Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 35
    if-eqz p2, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->setAddedWishItem()V

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;Z)V

    .line 40
    return-void
.end method
