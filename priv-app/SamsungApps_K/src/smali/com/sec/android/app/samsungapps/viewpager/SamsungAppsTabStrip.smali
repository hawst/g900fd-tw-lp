.class public Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;
.super Landroid/widget/LinearLayout;
.source "ProGuard"


# instance fields
.field private a:Landroid/widget/LinearLayout$LayoutParams;

.field private final b:Lcom/sec/android/app/samsungapps/viewpager/y;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/support/v4/view/ViewPager;

.field public delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:J

.field private m:I

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/y;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/y;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;B)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b:Lcom/sec/android/app/samsungapps/viewpager/y;

    .line 41
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->f:I

    .line 42
    const v0, -0xf94627

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->g:I

    .line 43
    const v0, 0x6b9d9

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->h:I

    .line 45
    const/16 v0, 0x2c

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->i:I

    .line 46
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->j:I

    .line 47
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->k:I

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->l:J

    .line 51
    const v0, 0x7f02009a

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->m:I

    .line 343
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->n:I

    .line 344
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->o:I

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/y;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/y;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;B)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b:Lcom/sec/android/app/samsungapps/viewpager/y;

    .line 41
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->f:I

    .line 42
    const v0, -0xf94627

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->g:I

    .line 43
    const v0, 0x6b9d9

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->h:I

    .line 45
    const/16 v0, 0x2c

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->i:I

    .line 46
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->j:I

    .line 47
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->k:I

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->l:J

    .line 51
    const v0, 0x7f02009a

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->m:I

    .line 343
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->n:I

    .line 344
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->o:I

    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;I)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 194
    move v1, v2

    :goto_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e:I

    if-ge v1, v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 196
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(I)Landroid/widget/TextView;

    move-result-object v0

    .line 198
    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 199
    check-cast v0, Landroid/widget/TextView;

    .line 201
    if-ne p1, v1, :cond_1

    .line 202
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    .line 194
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 204
    :cond_1
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 209
    :cond_2
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 69
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setWillNotDraw(Z)V

    .line 71
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->addView(Landroid/view/View;)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 78
    iget v1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->i:I

    int-to-float v1, v1

    invoke-static {v3, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->i:I

    .line 79
    iget v1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->j:I

    int-to-float v1, v1

    invoke-static {v3, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->j:I

    .line 81
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a:Landroid/widget/LinearLayout$LayoutParams;

    .line 83
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;IIFZ)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const v5, 0x3f4ccccd    # 0.8f

    const v4, 0x3e4ccccd    # 0.2f

    const/4 v3, 0x0

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(I)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eq p1, p2, :cond_2

    add-int/lit8 v2, p2, 0x1

    if-eq p1, v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->g:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-static {v1, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    :cond_2
    if-eqz p4, :cond_4

    if-ne p1, p2, :cond_3

    cmpg-float v0, p3, v5

    if-ltz v0, :cond_7

    invoke-static {v1, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    :cond_3
    cmpl-float v0, p3, v5

    if-gez v0, :cond_7

    invoke-static {v1, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    :cond_4
    if-ne p1, p2, :cond_6

    cmpl-float v0, p3, v4

    if-lez v0, :cond_5

    invoke-static {v1, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    :cond_5
    invoke-static {v1, v6}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    :cond_6
    cmpg-float v0, p3, v4

    if-gtz v0, :cond_7

    invoke-static {v1, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    :cond_7
    invoke-static {v1, v6}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Z
    .locals 4

    .prologue
    .line 26
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->l:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->l:J

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private b(I)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 378
    const/4 v1, 0x0

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 381
    instance-of v2, v0, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_0

    .line 382
    check-cast v0, Landroid/widget/FrameLayout;

    .line 383
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 384
    instance-of v2, v0, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 385
    check-cast v0, Landroid/widget/TextView;

    .line 390
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(I)V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->h:I

    return v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e:I

    return v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->f:I

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->g:I

    return v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCurrentTabDescription(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 404
    const/4 v0, 0x0

    .line 405
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(I)Landroid/widget/TextView;

    move-result-object v1

    .line 406
    if-eqz v1, :cond_0

    .line 407
    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 409
    :cond_0
    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e:I

    move v0, v1

    .line 107
    :goto_0
    iget v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e:I

    if-ge v0, v2, :cond_4

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    if-ne v2, v0, :cond_1

    move v2, v3

    :goto_1
    new-instance v5, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v6

    if-ne v0, v6, :cond_2

    iget v6, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->f:I

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    :goto_2
    new-instance v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    const/16 v4, 0x11

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setGravity(I)V

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const v4, 0x401d70a4    # 2.46f

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b008c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v4, v10, v10, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v4, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->m:I

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget v4, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->k:I

    int-to-float v4, v4

    invoke-virtual {v6, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0800de

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    add-int/lit8 v9, v0, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v1

    iget v9, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-lt v4, v7, :cond_0

    const-string v4, "sans-serif-light"

    invoke-static {v4, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    if-eqz v2, :cond_3

    invoke-static {v6, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    :goto_3
    iget v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->j:I

    iget v4, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->j:I

    invoke-virtual {v6, v2, v1, v4, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    new-instance v2, Lcom/sec/android/app/samsungapps/viewpager/x;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/x;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;I)V

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_1
    move v2, v1

    .line 108
    goto/16 :goto_1

    :cond_2
    iget v6, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->g:I

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto/16 :goto_2

    :cond_3
    invoke-static {v6, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_3

    .line 111
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_5

    if-eqz v0, :cond_5

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v11, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->n:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->o:I

    .line 114
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(I)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/w;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/w;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 130
    return-void

    .line 113
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->o:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->n:I

    goto :goto_4
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 361
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(I)V

    .line 365
    :cond_0
    return-void
.end method

.method public setCurrentTabSelect(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 420
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e:I

    if-ge v0, v2, :cond_0

    .line 421
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(I)Landroid/widget/TextView;

    move-result-object v2

    .line 422
    if-nez v2, :cond_1

    .line 434
    :cond_0
    return-void

    .line 425
    :cond_1
    if-ne v0, p1, :cond_2

    .line 426
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 427
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 420
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 429
    :cond_2
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 430
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 99
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d:Landroid/support/v4/view/ViewPager;

    .line 88
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b:Lcom/sec/android/app/samsungapps/viewpager/y;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->notifyDataSetChanged()V

    .line 95
    return-void
.end method
