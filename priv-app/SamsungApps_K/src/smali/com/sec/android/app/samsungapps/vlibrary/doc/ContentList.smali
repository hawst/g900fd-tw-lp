.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;


# static fields
.field private static final serialVersionUID:J = 0x5a7d54ed2563d434L


# instance fields
.field private _bCompleted:Z

.field _bNeverLoaded:Z

.field private _firstItemsIndex:I

.field private _itemCountPerPage:I

.field private _lastItemsIndex:I

.field private _oldX:I

.field private _oldY:I

.field bLocked:Z

.field private mTotalCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bCompleted:Z

    .line 20
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bNeverLoaded:Z

    .line 21
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->bLocked:Z

    .line 22
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_lastItemsIndex:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_firstItemsIndex:I

    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getItemCountPerPage()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_itemCountPerPage:I

    .line 127
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->mTotalCount:I

    .line 128
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_oldX:I

    return-void
.end method

.method private static close(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 256
    if-eqz p0, :cond_0

    .line 258
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 260
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static load(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 217
    .line 221
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/InvalidClassException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 222
    :try_start_1
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/InvalidClassException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 223
    :try_start_2
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;-><init>()V
    :try_end_2
    .catch Ljava/io/InvalidClassException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 224
    :try_start_3
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 225
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    .line 227
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v6, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Ljava/io/ObjectInputStream;)V

    .line 228
    invoke-virtual {v3, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/InvalidClassException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    .line 248
    :goto_1
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->close(Ljava/io/InputStream;)V

    .line 249
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->close(Ljava/io/InputStream;)V

    .line 250
    :goto_2
    return-object v2

    .line 231
    :catch_0
    move-exception v0

    move-object v3, v0

    move-object v1, v4

    move-object v2, v4

    move-object v0, v4

    :goto_3
    invoke-virtual {v3}, Ljava/io/InvalidClassException;->printStackTrace()V

    goto :goto_1

    .line 235
    :catch_1
    move-exception v0

    move-object v3, v0

    move-object v1, v4

    move-object v2, v4

    move-object v0, v4

    :goto_4
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 238
    :catch_2
    move-exception v0

    move-object v3, v0

    move-object v1, v4

    move-object v2, v4

    move-object v0, v4

    :goto_5
    invoke-virtual {v3}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 241
    :catch_3
    move-exception v0

    move-object v1, v4

    move-object v2, v4

    :goto_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 244
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->close(Ljava/io/InputStream;)V

    .line 245
    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->close(Ljava/io/InputStream;)V

    move-object v2, v4

    .line 246
    goto :goto_2

    .line 241
    :catch_4
    move-exception v0

    move-object v1, v4

    goto :goto_6

    :catch_5
    move-exception v0

    goto :goto_6

    .line 238
    :catch_6
    move-exception v0

    move-object v3, v0

    move-object v1, v2

    move-object v0, v4

    move-object v2, v4

    goto :goto_5

    :catch_7
    move-exception v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v4

    goto :goto_5

    :catch_8
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v7

    goto :goto_5

    .line 235
    :catch_9
    move-exception v0

    move-object v3, v0

    move-object v1, v2

    move-object v0, v4

    move-object v2, v4

    goto :goto_4

    :catch_a
    move-exception v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v4

    goto :goto_4

    :catch_b
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v7

    goto :goto_4

    .line 231
    :catch_c
    move-exception v0

    move-object v3, v0

    move-object v1, v2

    move-object v0, v4

    move-object v2, v4

    goto :goto_3

    :catch_d
    move-exception v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v4

    goto :goto_3

    :catch_e
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v7

    goto :goto_3
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0

    .prologue
    .line 268
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 269
    return-void
.end method

.method public static save(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V
    .locals 4

    .prologue
    .line 188
    const/4 v2, 0x0

    .line 190
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 191
    :try_start_1
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 192
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 193
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 194
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 196
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->writeObjectOutStream(Ljava/io/ObjectOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 205
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    .line 208
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 213
    :cond_1
    :goto_3
    return-void

    .line 201
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 209
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 201
    :catch_3
    move-exception v0

    goto :goto_4

    .line 198
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bNeverLoaded:Z

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bCompleted:Z

    .line 119
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 120
    return-void
.end method

.method public bridge synthetic get(I)Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;
    .locals 1

    .prologue
    .line 14
    invoke-super {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;

    return-object v0
.end method

.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const-string v0, "all"

    return-object v0
.end method

.method public final getContentsGettingCount()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_itemCountPerPage:I

    return v0
.end method

.method public getIndexOfFirstItem()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_firstItemsIndex:I

    return v0
.end method

.method public getIndexOfLastItem()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_lastItemsIndex:I

    return v0
.end method

.method public getOldX()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_oldX:I

    return v0
.end method

.method public getOldY()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_oldY:I

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->mTotalCount:I

    return v0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bCompleted:Z

    return v0
.end method

.method public justClearList()V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 125
    return-void
.end method

.method public loadOldY()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_oldY:I

    return v0
.end method

.method public lock()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public neverLoaded()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bNeverLoaded:Z

    return v0
.end method

.method public onUpdateListItem(IIILjava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 147
    monitor-enter p0

    .line 149
    if-nez p1, :cond_0

    .line 150
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->clear()V

    .line 152
    :cond_0
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 154
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 156
    :cond_1
    :try_start_1
    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setTotalCount(I)V

    .line 157
    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setIndexOfLastItem(I)V

    .line 158
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public saveOldX(I)V
    .locals 0

    .prologue
    .line 163
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_oldX:I

    .line 164
    return-void
.end method

.method public saveOldY(I)V
    .locals 0

    .prologue
    .line 173
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_oldY:I

    .line 174
    return-void
.end method

.method public setCompleted(Z)V
    .locals 1

    .prologue
    .line 85
    if-eqz p1, :cond_0

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bNeverLoaded:Z

    .line 88
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bCompleted:Z

    .line 89
    return-void
.end method

.method public setIndexOfFirstItem(I)V
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_firstItemsIndex:I

    .line 76
    return-void
.end method

.method public setIndexOfLastItem(I)V
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_lastItemsIndex:I

    .line 56
    return-void
.end method

.method public setNeverLoaded(Z)V
    .locals 0

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->_bNeverLoaded:Z

    .line 113
    return-void
.end method

.method public setTotalCount(I)V
    .locals 0

    .prologue
    .line 131
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->mTotalCount:I

    .line 132
    return-void
.end method

.method public unlock()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
