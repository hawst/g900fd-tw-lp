.class public final enum Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum HideUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowChangeCreditCardView:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowCreditCardRegister:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowGiftcards:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowMyTabDownload:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowPushPreference:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowRemoveSnsAccountMenu:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowUnifiedBillingCreditCard:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowVoucherList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowVouchers:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum ShowWishList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field public static final enum UpdateAll:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowWishList"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowWishList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowMyTabDownload"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowMyTabDownload:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowVouchers"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowVouchers:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowGiftcards"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowGiftcards:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowVoucherList"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowVoucherList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowRemoveSnsAccountMenu"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowRemoveSnsAccountMenu:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowCreditCardRegister"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowCreditCardRegister:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowChangeCreditCardView"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowChangeCreditCardView:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowPushPreference"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowPushPreference:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "UpdateAll"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->UpdateAll:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowUpdateAllButton"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "HideUpdateAllButton"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->HideUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    const-string v1, "ShowUnifiedBillingCreditCard"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowUnifiedBillingCreditCard:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    .line 4
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowWishList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowMyTabDownload:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowVouchers:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowGiftcards:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowVoucherList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowRemoveSnsAccountMenu:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowCreditCardRegister:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowChangeCreditCardView:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowPushPreference:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->UpdateAll:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->HideUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowUnifiedBillingCreditCard:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    return-object v0
.end method
