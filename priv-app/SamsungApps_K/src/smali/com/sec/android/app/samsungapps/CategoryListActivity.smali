.class public Lcom/sec/android/app/samsungapps/CategoryListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 30
    const-string v0, "CategoryListActivity"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;->b:Ljava/lang/String;

    .line 122
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CategoryListActivity;)Lcom/sec/android/app/samsungapps/view/CategoryItemListView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v1, 0x7f040050

    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->setMainViewAndEmptyView(I)V

    .line 46
    :goto_0
    const v1, 0x7f0801fa

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x2

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 52
    const v0, 0x7f0c0161

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 53
    new-instance v1, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->setData(Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->load()V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->initialized()V

    .line 58
    return-void

    .line 40
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->setMainView(I)V

    goto :goto_0

    .line 46
    nop

    :array_0
    .array-data 4
        0xa0005
        0xa0008
    .end array-data
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->release()V

    .line 64
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 65
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 77
    return-void
.end method

.method public retry()V
    .locals 4

    .prologue
    const v3, 0x7f0c007e

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 89
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/o;-><init>(Lcom/sec/android/app/samsungapps/CategoryListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategoryListActivity;->setVisibleEmpty(Z)Z

    .line 83
    return-void
.end method
