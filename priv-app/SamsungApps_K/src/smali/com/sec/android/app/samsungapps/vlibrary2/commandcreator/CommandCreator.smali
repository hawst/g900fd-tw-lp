.class public Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static NullReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/c;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/c;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->NullReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method


# virtual methods
.method protected createCardValidator(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;)V

    return-object v0
.end method

.method public createChinaCyberCash(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public createChinaPPC(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public createDanalPayment(Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestCommunicator;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestCommunicator;-><init>()V

    invoke-direct {v0, p2, p1, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public createGlobalCreditPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 6

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createCardValidator(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CGCreditCardReqComm;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CGCreditCardReqComm;-><init>()V

    move-object v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public createInicis(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 94
    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 95
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;-><init>()V

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_CREDITCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    invoke-direct {v1, p3, v7, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;)V

    invoke-direct {v6, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;)V

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v8, p4

    move v9, v5

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;ZLcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Z)V

    return-object v0
.end method

.method public createInicisPhoneBill(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 10

    .prologue
    .line 104
    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 105
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;-><init>()V

    const/4 v5, 0x0

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_PHONEBILL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    invoke-direct {v1, p3, v7, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;)V

    invoke-direct {v6, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;)V

    const/4 v9, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;ZLcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Z)V

    return-object v0
.end method

.method public createPurchaseCommand(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 12

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProductToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p2, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createPurchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;)V

    return-object v0
.end method

.method public createPurchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;Ljava/lang/String;)V

    return-object v0
.end method

.method public createUPoint(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 10

    .prologue
    .line 114
    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 115
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;-><init>()V

    const/4 v5, 0x1

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    invoke-direct {v1, p3, v7, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;)V

    invoke-direct {v6, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;)V

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;ZLcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Z)V

    return-object v0
.end method

.method public createUnifiedBillingPaymentCommand(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedPaymentPurchaseCommand;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedPaymentPurchaseCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;)V

    return-object v0
.end method

.method public createUpdateUtilCommand(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    return-object v0
.end method
