.class public Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;


# static fields
.field protected static final REQUEST_CODE_SAC_FIRST_CHECK:I = 0x2002

.field protected static final REQUEST_CODE_SAC_SIGN_IN:I = 0x2001

.field protected static final RESULT_CANCELED:I = 0x0

.field protected static final RESULT_FAILED:I = 0x1

.field protected static final RESULT_INCORRECT_CLIENT_SECRET:I = 0x5

.field protected static final RESULT_INVALID_CLIENT_ID:I = 0x4

.field protected static final RESULT_INVALID_COUNTRYCODE:I = 0x6

.field protected static final RESULT_NETWORK_ERROR:I = 0x3

.field protected static final RESULT_OK:I = -0x1

.field protected static final RESULT_SIM_NOT_READY:I = 0x2

.field protected static final RESULT_SKIP:I = 0x7

.field public static final SAC_INIT_VERSION_CODE:I = 0x2ee1

.field public static final SAC_PKGNAME:Ljava/lang/String; = "com.osp.app.signin"

.field static c:Lcom/sec/android/app/samsungapps/LoadingDialog;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

.field private d:Landroid/app/Activity;

.field private e:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->setmContext(Landroid/content/Context;)V

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->setmContext(Landroid/content/Context;)V

    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    .line 56
    return-void
.end method

.method public static toByte(Ljava/lang/String;)[B
    .locals 5

    .prologue
    .line 277
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    .line 278
    new-array v2, v1, [B

    .line 279
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 280
    mul-int/lit8 v3, v0, 0x2

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->byteValue()B

    move-result v3

    aput-byte v3, v2, v0

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 282
    :cond_0
    return-object v2
.end method


# virtual methods
.method public checkInitialSaccount()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 82
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->isExistSamsungAccount()Z

    move-result v1

    if-nez v1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 92
    :cond_1
    const-string v1, "Y"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v3, "DF_SKIP_SACCOUNT"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    .line 104
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public checkSamsungAccount()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public editSamsungAccountActivity()V
    .locals 4

    .prologue
    .line 157
    const-string v0, ""

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 159
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    .line 161
    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.osp.app.signin"

    invoke-direct {v2, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v1, "account"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 165
    :cond_0
    return-void
.end method

.method public getSamsungAccount()Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    const-string v0, ""

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 145
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 147
    array-length v2, v1

    if-lez v2, :cond_0

    .line 149
    const/4 v0, 0x0

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 152
    :cond_0
    return-object v0
.end method

.method public getmContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->e:Landroid/content/Context;

    return-object v0
.end method

.method public intentCheck(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 198
    if-eqz p1, :cond_0

    .line 234
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setPass(Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public isExistSamsungAccount()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public samsungAccountActivity()V
    .locals 3

    .prologue
    .line 132
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 133
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v1, "client_secret"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string v1, "service_name"

    const-string v2, "SamsungApps"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const/16 v1, 0x2001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->startAccountActivity(Landroid/content/Intent;I)V

    .line 139
    return-void
.end method

.method public setmContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->e:Landroid/content/Context;

    .line 61
    return-void
.end method

.method protected startAccountActivity(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    .line 125
    sput-object v0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->d:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 128
    return-void
.end method

.method public startSamsungAccountActivity()V
    .locals 3

    .prologue
    .line 109
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 110
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "client_secret"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v1, "service_name"

    const-string v2, "SamsungApps"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const/16 v1, 0x2002

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->startAccountActivity(Landroid/content/Intent;I)V

    .line 116
    return-void
.end method
