.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public noticeDate:Ljava/lang/String;

.field public noticeId:Ljava/lang/String;

.field public noticeTitle:Ljava/lang/String;

.field public noticeType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeId:Ljava/lang/String;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeType:I

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeTitle:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeDate:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeId:Ljava/lang/String;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeType:I

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeTitle:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeDate:Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeId:Ljava/lang/String;

    .line 25
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeType:I

    .line 26
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeTitle:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeDate:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getNoticeDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeDate:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeId:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeType()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeType:I

    return v0
.end method

.method public setNoticeDate(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeDate:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setNoticeID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeId:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setNoticeTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeTitle:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setNoticeType(I)V
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->noticeType:I

    .line 47
    return-void
.end method
