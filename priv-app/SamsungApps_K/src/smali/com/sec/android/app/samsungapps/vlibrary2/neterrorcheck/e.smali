.class final Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailed()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->notifyFailed()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V

    .line 68
    return-void
.end method

.method public final onFailedNeedRetry()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->bNeedRetry:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->access$102(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Z)Z

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->notifyFailed()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V

    .line 63
    return-void
.end method

.method public final onSuccess()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;->a:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->checkNetDisconnection(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Landroid/content/Context;)V

    .line 57
    return-void
.end method
