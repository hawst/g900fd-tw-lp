.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract getConfirmResult()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
.end method

.method public abstract getOrderID()Ljava/lang/String;
.end method

.method public abstract getPaymentID()Ljava/lang/String;
.end method

.method public abstract getResponseTime()J
.end method

.method public abstract getRetryCount()I
.end method

.method public abstract isFinalMapSuccess()Z
.end method
