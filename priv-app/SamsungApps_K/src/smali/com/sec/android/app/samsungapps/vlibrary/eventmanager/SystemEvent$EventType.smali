.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum AlreadyInstalledBatchApp:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CategoryEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CheckAppUpgradeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CommentChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum ContentDetailUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CountryDecided:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CouponOrGiftCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CouponOrGiftCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CreditCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum DispLimitedStore:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum DownloaderStateChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum EndLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum FlexibleTabLoaded:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum GeneralErrorEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum InicisPaymentEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum InitializeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum MigrationCompleted:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum NoAvailablePurchaseEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum NotEnoughPPCardBalance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum NotifyEnterPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum NotifyFailedPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum OpenContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum OpenWishList:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum REAL_AGE_NAME_VERIFIED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum SearchGesturePad:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum SignUpEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum StartLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum StoreTypeChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum SuggestAllShareContent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field public static final enum YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "MigrationCompleted"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->MigrationCompleted:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CreditCardRegistered"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CreditCardRemoved"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CouponOrGiftCardRegistered"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CouponOrGiftCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CouponOrGiftCardRemoved"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CouponOrGiftCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "AccountEvent"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CheckAppUpgradeEvent"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CheckAppUpgradeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "ExitSamsungApps"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "UpdateAppEvent"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "GeneralErrorEvent"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->GeneralErrorEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "InicisPaymentEvent"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->InicisPaymentEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "StartLoadingDialog"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->StartLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "EndLoadingDialog"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->EndLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "InitializeEvent"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->InitializeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "SignUpEvent"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SignUpEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CategoryEvent"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CategoryEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "RequestScreenShot"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "NotEnoughPPCardBalance"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotEnoughPPCardBalance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "OpenContentDetail"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->OpenContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "OpenWishList"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->OpenWishList:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "YesOrNoEvent"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "NotifyEnterPreCheckingForDownload"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotifyEnterPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "NotifyFailedPreCheckingForDownload"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotifyFailedPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "DownloaderStateChanged"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DownloaderStateChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "AlreadyInstalledBatchApp"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AlreadyInstalledBatchApp:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "NoAvailablePurchaseEvent"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NoAvailablePurchaseEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "SuggestAllShareContent"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SuggestAllShareContent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CommentChanged"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CommentChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "WishListChanged"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "ContentDetailUpdated"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ContentDetailUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "CountryDecided"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CountryDecided:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "StoreTypeChanged"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->StoreTypeChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "DispLimitedStore"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DispLimitedStore:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "FlexibleTabLoaded"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->FlexibleTabLoaded:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "SearchGesturePad"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SearchGesturePad:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    const-string v1, "REAL_AGE_NAME_VERIFIED"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->REAL_AGE_NAME_VERIFIED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 7
    const/16 v0, 0x24

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->MigrationCompleted:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CouponOrGiftCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CouponOrGiftCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CheckAppUpgradeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->GeneralErrorEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->InicisPaymentEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->StartLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->EndLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->InitializeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SignUpEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CategoryEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotEnoughPPCardBalance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->OpenContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->OpenWishList:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotifyEnterPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotifyFailedPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DownloaderStateChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AlreadyInstalledBatchApp:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NoAvailablePurchaseEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SuggestAllShareContent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CommentChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ContentDetailUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CountryDecided:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->StoreTypeChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DispLimitedStore:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->FlexibleTabLoaded:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SearchGesturePad:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->REAL_AGE_NAME_VERIFIED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    return-object v0
.end method
