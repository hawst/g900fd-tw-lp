.class final Lcom/sec/android/app/samsungapps/view/be;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/WebImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/WebImageView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/be;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 54
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 56
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/be;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget-boolean v1, v1, Lcom/sec/android/app/samsungapps/view/WebImageView;->mOverlay:Z

    if-eqz v1, :cond_1

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/be;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/be;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget v3, v3, Lcom/sec/android/app/samsungapps/view/WebImageView;->mResourceID:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/view/WebImageView;->overlayBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/be;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->applyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 68
    return-void

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/be;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->applyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_0
.end method
