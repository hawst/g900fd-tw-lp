.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
.end method

.method public abstract getAppliedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
.end method

.method public abstract getCVC()Ljava/lang/String;
.end method

.method public abstract getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
.end method

.method public abstract getSellingPrice()D
.end method

.method public abstract hasCVC()Z
.end method
