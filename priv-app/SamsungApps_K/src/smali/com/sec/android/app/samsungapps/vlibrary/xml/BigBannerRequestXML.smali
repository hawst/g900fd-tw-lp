.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 10
    const-string v2, "bigBannerList"

    const-string v3, "2430"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->BigBanner:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->BigBanner:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    return-void
.end method
