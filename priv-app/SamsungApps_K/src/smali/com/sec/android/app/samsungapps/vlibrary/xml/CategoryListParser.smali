.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private _IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

.field mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

.field mParentCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

.field templist:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mParentCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->_IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mParentCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->templist:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mParentCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->_IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->templist:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 19
    return-void
.end method


# virtual methods
.method public getCategoryList()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    return-object v0
.end method

.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 30
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->isKNOXCategory()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->_IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->templist:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Z

    .line 38
    :cond_1
    return-void
.end method

.method public onEndParse()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->templist:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->setRoot(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mParentCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->mParentCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;->templist:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->setChild(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V

    goto :goto_0
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method
