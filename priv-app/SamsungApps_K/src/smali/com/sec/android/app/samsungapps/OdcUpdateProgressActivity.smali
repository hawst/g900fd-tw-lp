.class public Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;
.super Lcom/sec/android/app/samsungapps/CommonActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;


# static fields
.field public static SAMSUNGAPPS_UPDATING:Z


# instance fields
.field private a:Landroid/widget/ProgressBar;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->SAMSUNGAPPS_UPDATING:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;-><init>()V

    .line 35
    iput v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    .line 36
    iput v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 190
    const/4 v0, -0x4

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 194
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 195
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finishODCUpdate()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 152
    return-void
.end method

.method public notifyInstallCompleted()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->finish()V

    .line 186
    return-void
.end method

.method public notifyInstallFailed(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x4

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 222
    const v0, 0x7f080276

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 224
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    const v0, 0x7f080168

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 235
    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 236
    const v0, 0x7f0802ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/fo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/fo;-><init>(Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 237
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 238
    return-void

    .line 231
    :cond_1
    const v0, 0x7f0802c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public notifyInstalling()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->d:Landroid/widget/TextView;

    const v1, 0x7f08027f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 179
    return-void
.end method

.method public notifyProgress(II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 156
    iput p1, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    .line 157
    iput p2, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->d:Landroid/widget/TextView;

    const v1, 0x7f080274

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    int-to-long v1, p1

    const-wide/16 v3, 0x64

    mul-long/2addr v1, v3

    int-to-long v3, p2

    div-long/2addr v1, v3

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->b:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->c:Landroid/widget/TextView;

    const-string v1, " / %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->e:Landroid/widget/TextView;

    const-string v1, "%d%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    mul-int/lit8 v4, p1, 0x64

    int-to-long v4, v4

    iget v6, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    int-to-long v6, v6

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ArithmeticException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    if-ge v0, v1, :cond_1

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->startHomeActivity()V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 109
    const v0, 0x7f040073

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->setContentView(I)V

    .line 111
    const v0, 0x7f0c0055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 112
    if-eqz v0, :cond_0

    .line 113
    const-string v1, "isa_samsungapps_icon"

    const-string v2, "drawable"

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    :cond_0
    const v0, 0x7f0c0038

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 117
    const v1, 0x7f0801ea

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->setProgressVisible()V

    .line 120
    iget v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    if-ne v0, v1, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->notifyInstalling()V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->g:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->h:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->notifyProgress(II)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f040073

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->setContentView(I)V

    .line 43
    const v0, 0x7f0c0055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 44
    if-eqz v0, :cond_0

    .line 45
    const-string v1, "isa_samsungapps_icon"

    const-string v2, "drawable"

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 48
    :cond_0
    const v0, 0x7f0c0038

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 49
    const v1, 0x7f0c002b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 51
    sget-object v2, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    sget-object v1, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f0801ea

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    const v0, 0x7f0c01b0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->b:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f0c01b1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->c:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f0c01b4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->d:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f0c01b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->e:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0c01b3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->finish()V

    .line 83
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getStorage()Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;->getAvailableInternalMemorySize()J

    move-result-wide v0

    const-wide/32 v2, 0x2800000

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 76
    const v0, 0x7f0802dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    const v1, 0x7f0802ef

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/fp;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/fp;-><init>(Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto :goto_0

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;)V

    .line 82
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->SAMSUNGAPPS_UPDATING:Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->SAMSUNGAPPS_UPDATING:Z

    .line 89
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onDestroy()V

    .line 90
    return-void
.end method

.method public setProgressVisible()V
    .locals 3

    .prologue
    .line 132
    const v0, 0x7f0c0038

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 133
    sget-object v1, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f0801ea

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    const v0, 0x7f0c01b0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->b:Landroid/widget/TextView;

    .line 136
    const v0, 0x7f0c01b1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->c:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f0c01b4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->d:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f0c01b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->e:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f0c01b3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 143
    return-void
.end method

.method public startODCUpdate()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 148
    return-void
.end method
