.class public Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;
.super Lcom/sec/android/app/samsungapps/view/EntryView;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;


# static fields
.field public static viewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# instance fields
.field protected final AMEXNAME:Ljava/lang/String;

.field protected final DISCOVERNAME:Ljava/lang/String;

.field protected final JCBNAME:Ljava/lang/String;

.field protected final MASTERNAME:Ljava/lang/String;

.field protected final VISANAME:Ljava/lang/String;

.field a:I

.field protected address:Landroid/widget/EditText;

.field b:I

.field c:I

.field protected cardCvcNum:Landroid/widget/EditText;

.field protected cardExpiryMonth:Landroid/widget/EditText;

.field protected cardExpiryYear:Landroid/widget/EditText;

.field protected cardNumView:Landroid/widget/EditText;

.field protected card_img_1:Landroid/widget/ImageView;

.field protected card_img_2:Landroid/widget/ImageView;

.field protected card_img_3:Landroid/widget/ImageView;

.field protected card_img_4:Landroid/widget/ImageView;

.field protected card_img_5:Landroid/widget/ImageView;

.field protected city:Landroid/widget/EditText;

.field d:[Landroid/text/InputFilter;

.field e:[Landroid/text/InputFilter;

.field f:Ljava/util/regex/Pattern;

.field protected filterMonth:Landroid/text/InputFilter;

.field protected firstName:Landroid/widget/EditText;

.field g:Ljava/util/regex/Pattern;

.field h:Ljava/util/regex/Pattern;

.field i:Ljava/util/regex/Pattern;

.field j:Ljava/util/regex/Pattern;

.field k:Landroid/os/Handler;

.field l:Landroid/text/TextWatcher;

.field protected lastName:Landroid/widget/EditText;

.field protected lengthFilter:Landroid/text/InputFilter;

.field protected lengthFilterPostCode:Landroid/text/InputFilter;

.field private m:Ljava/lang/String;

.field protected mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

.field protected mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

.field private n:Ljava/lang/String;

.field protected negative:Landroid/widget/Button;

.field protected positive:Landroid/widget/Button;

.field protected postCode:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593
    new-instance v0, Lcom/sec/android/app/samsungapps/gv;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/gv;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->viewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/EntryView;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 57
    const-string v0, "VISA"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->VISANAME:Ljava/lang/String;

    .line 58
    const-string v0, "MasterCard"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->MASTERNAME:Ljava/lang/String;

    .line 59
    const-string v0, "American Express"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->AMEXNAME:Ljava/lang/String;

    .line 60
    const-string v0, "Discover"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->DISCOVERNAME:Ljava/lang/String;

    .line 61
    const-string v0, "JCB"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->JCBNAME:Ljava/lang/String;

    .line 82
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a:I

    .line 83
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b:I

    .line 84
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c:I

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->d:[Landroid/text/InputFilter;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->e:[Landroid/text/InputFilter;

    .line 89
    const-string v0, "^4.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->f:Ljava/util/regex/Pattern;

    .line 90
    const-string v0, "^5[1-5].*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->g:Ljava/util/regex/Pattern;

    .line 91
    const-string v0, "^3[4|7].*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->h:Ljava/util/regex/Pattern;

    .line 92
    const-string v0, "^6011.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->i:Ljava/util/regex/Pattern;

    .line 93
    const-string v0, "^35(28|89).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->j:Ljava/util/regex/Pattern;

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->m:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->n:Ljava/lang/String;

    .line 716
    new-instance v0, Lcom/sec/android/app/samsungapps/gx;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/gx;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->k:Landroid/os/Handler;

    .line 730
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lengthFilter:Landroid/text/InputFilter;

    .line 731
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    iget v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c:I

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lengthFilterPostCode:Landroid/text/InputFilter;

    .line 732
    new-instance v0, Lcom/sec/android/app/samsungapps/gn;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/gn;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->filterMonth:Landroid/text/InputFilter;

    .line 749
    new-instance v0, Lcom/sec/android/app/samsungapps/go;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/go;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 517
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->m:Ljava/lang/String;

    return-object p1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->size()I

    move-result v1

    .line 126
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    move-result-object v2

    .line 129
    iget-object v3, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    iget-object v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompanyCode:Ljava/lang/String;

    .line 134
    :goto_1
    return-object v0

    .line 126
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getCardType()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xe

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->checkValidData(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    new-instance v1, Lcom/sec/android/app/samsungapps/gr;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gr;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->register(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getCardNumber()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->checkValidData(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0c02c3

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->checkValidData(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0c02c5

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->checkValidData(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getCVS()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->checkValidData(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_1
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->isRegisterMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->n:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 346
    const v0, 0x7f0c02ca

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 347
    const v2, 0x7f0c02c7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 348
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->isUSA()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 350
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    const v3, 0x7f0c02cb

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 354
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c()Landroid/widget/Spinner;

    move-result-object v0

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x7f04008e

    invoke-direct {v3, p0, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v4, 0x7f04008f

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_0

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setFocusable(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setFocusableInTouchMode(Z)V

    new-instance v3, Lcom/sec/android/app/samsungapps/gp;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/gp;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Landroid/widget/Spinner;)V

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0}, Landroid/widget/Spinner;->requestFocus()Z

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getStateInfoList()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->size()I

    move-result v3

    move v0, v1

    .line 357
    :goto_0
    if-ge v0, v3, :cond_2

    .line 359
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getStateInfoList()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 357
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 364
    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    const v3, 0x7f0c02c6

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 367
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->size()I

    move-result v2

    move v0, v1

    .line 370
    :goto_1
    if-ge v0, v2, :cond_8

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    const-string v4, "VISA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 374
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_1:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 376
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    const-string v4, "MasterCard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 378
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_2:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 380
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    const-string v4, "American Express"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 382
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_3:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 384
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    const-string v4, "Discover"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 386
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_4:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 388
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;->get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CardSort;->cardCompany:Ljava/lang/String;

    const-string v4, "JCB"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 390
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_5:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 397
    :cond_8
    const v0, 0x7f0c02c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 398
    if-eqz v0, :cond_9

    .line 400
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 401
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 402
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 403
    new-instance v1, Lcom/sec/android/app/samsungapps/gs;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/gs;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Landroid/widget/EditText;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 412
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->setTextChangedListene()V

    .line 414
    const v0, 0x7f0c02c9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 415
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f08021b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    new-instance v1, Lcom/sec/android/app/samsungapps/gt;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gt;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 435
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0801db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f080232

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->needDisplayCardDetail()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private c()Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 522
    const v0, 0x7f0c02cf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "VISA"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "MasterCard"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "American Express"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "Discover"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "JCB"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c()Landroid/widget/Spinner;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public checkButton()V
    .locals 5

    .prologue
    const/16 v1, 0xc

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->isUSA()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardNumView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryMonth:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lt v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->firstName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lastName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->address:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->city:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->postCode:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 632
    :goto_0
    return-void

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardNumView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryMonth:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lt v0, v4, :cond_2

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 628
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method

.method public getAuthNID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 578
    const-string v0, ""

    return-object v0
.end method

.method public getAuthPIN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 587
    const-string v0, ""

    return-object v0
.end method

.method public getCVS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 569
    const v0, 0x7f0c02c6

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    const v0, 0x7f0c02c2

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 819
    const v0, 0x7f0c02ce

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpirationMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 558
    const v0, 0x7f0c02c3

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpirationYear()Ljava/lang/String;
    .locals 2

    .prologue
    .line 552
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "20"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v1, 0x7f0c02c5

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 837
    const v0, 0x7f0c02cb

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 843
    const v0, 0x7f0c02cc

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 807
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c()Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 808
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 810
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mStateInfoSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfo;->stateCode:Ljava/lang/String;

    .line 813
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getStreet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 825
    const v0, 0x7f0c02cd

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getZip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 831
    const v0, 0x7f0c02d0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    .line 528
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 530
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const v1, 0x7f08027d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0802bb

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 531
    const/4 v0, 0x1

    .line 534
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/EntryView;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->userCancel()V

    .line 237
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/EntryView;->onBackPressed()V

    .line 238
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f080277

    const v6, 0x7f08023d

    const v4, 0xb0003

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 140
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->onCreate(Landroid/os/Bundle;)V

    .line 142
    new-array v0, v3, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->d:[Landroid/text/InputFilter;

    .line 143
    new-array v0, v3, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->e:[Landroid/text/InputFilter;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->d:[Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v2, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a:I

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->e:[Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v2, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b:I

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->finish()V

    .line 222
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->invokeCompleted(Z)V

    .line 157
    const v0, 0x7f0400d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->setMainView(I)V

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    const v0, 0x7f08026e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    .line 162
    const v0, 0x7f080235

    invoke-static {p0, v0, v6}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    .line 170
    :goto_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setFocusable(Z)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->positive:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/gm;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gm;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->negative:Landroid/widget/Button;

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->negative:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/gq;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gq;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    const v0, 0x7f0c02bd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_1:Landroid/widget/ImageView;

    .line 193
    const v0, 0x7f0c02be

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_2:Landroid/widget/ImageView;

    .line 194
    const v0, 0x7f0c02bf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_3:Landroid/widget/ImageView;

    .line 195
    const v0, 0x7f0c02c0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_4:Landroid/widget/ImageView;

    .line 196
    const v0, 0x7f0c02c1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_5:Landroid/widget/ImageView;

    .line 198
    const v0, 0x7f0c02c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardNumView:Landroid/widget/EditText;

    .line 199
    const v0, 0x7f0c02c6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    .line 200
    const v0, 0x7f0c02c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryMonth:Landroid/widget/EditText;

    .line 201
    const v0, 0x7f0c02c5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    .line 203
    const v0, 0x7f0c02cb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->firstName:Landroid/widget/EditText;

    .line 204
    const v0, 0x7f0c02cc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lastName:Landroid/widget/EditText;

    .line 205
    const v0, 0x7f0c02cd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->address:Landroid/widget/EditText;

    .line 206
    const v0, 0x7f0c02ce

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->city:Landroid/widget/EditText;

    .line 207
    const v0, 0x7f0c02d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->postCode:Landroid/widget/EditText;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryMonth:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0802a8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0802b2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08028f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f08014e

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 213
    const v0, 0x7f0c02bb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 214
    if-eqz v0, :cond_1

    .line 216
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    .line 217
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b()V

    goto/16 :goto_0

    .line 166
    :cond_2
    const v0, 0x7f0802ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    .line 167
    const v0, 0x7f0802e7

    invoke-static {p0, v0, v6}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->userCancel()V

    .line 250
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 252
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/EntryView;->onDestroy()V

    .line 253
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 440
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/EntryView;->onResume()V

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c()Landroid/widget/Spinner;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/gu;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gu;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 452
    :cond_0
    return-void
.end method

.method public setEnabledCardAll()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_1:Landroid/widget/ImageView;

    const v1, 0x7f02013d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_2:Landroid/widget/ImageView;

    const v1, 0x7f02013a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_3:Landroid/widget/ImageView;

    const v1, 0x7f020130

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_4:Landroid/widget/ImageView;

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_5:Landroid/widget/ImageView;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 460
    return-void
.end method

.method public setTextChangedListene()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 636
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardNumView:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/samsungapps/gw;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gw;-><init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryMonth:Landroid/widget/EditText;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->filterMonth:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryMonth:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardExpiryYear:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->firstName:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lastName:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 708
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->address:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->city:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->mRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->isUSA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->postCode:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->lengthFilterPostCode:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->postCode:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 714
    return-void
.end method
