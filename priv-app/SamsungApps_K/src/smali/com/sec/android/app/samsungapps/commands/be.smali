.class final Lcom/sec/android/app/samsungapps/commands/be;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field final synthetic b:Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)V
    .locals 1

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/be;->b:Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method


# virtual methods
.method public final endLoading()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 199
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 200
    return-void
.end method

.method public final startLoading()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/be;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 191
    return-void
.end method
