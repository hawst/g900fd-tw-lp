.class public Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->a:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    .line 18
    return-void
.end method


# virtual methods
.method public getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;
    .locals 0

    .prologue
    .line 32
    check-cast p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    return-object p0
.end method

.method public getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->a:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    return-object v0
.end method

.method public getFeatureTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;
    .locals 0

    .prologue
    .line 27
    check-cast p0, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;

    return-object p0
.end method

.method public getMainTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent;
    .locals 0

    .prologue
    .line 46
    check-cast p0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent;

    return-object p0
.end method

.method public getMyTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;
    .locals 0

    .prologue
    .line 37
    check-cast p0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    return-object p0
.end method

.method public getSuppotTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;
    .locals 0

    .prologue
    .line 42
    check-cast p0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;

    return-object p0
.end method
