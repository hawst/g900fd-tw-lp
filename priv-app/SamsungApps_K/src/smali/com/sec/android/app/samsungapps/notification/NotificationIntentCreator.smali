.class public Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V
    .locals 7

    .prologue
    .line 40
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    .line 30
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->c:Ljava/lang/String;

    .line 33
    iput p6, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->d:I

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->f:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private a(Z)Landroid/app/PendingIntent;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 90
    .line 91
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b(Z)I

    move-result v4

    .line 94
    const-string v1, "odc9820938409234.apk"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 98
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b(Z)I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    if-eq v4, v1, :cond_2

    const/4 v1, 0x3

    if-ne v4, v1, :cond_5

    :cond_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 101
    :cond_3
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/samsungapps/Main;

    invoke-direct {v1, v2, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    :goto_1
    if-eqz v1, :cond_0

    .line 114
    const-string v0, "GUID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string v0, "productid"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 117
    const-string v0, "loadtype"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    :cond_4
    const-string v0, "action"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    const-string v0, "notificationid"

    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "actionstring"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    invoke-static {v0, v3, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 104
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, v2, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "0"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "3"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_6
    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_7

    const-string v2, "productID"

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b:Ljava/lang/String;

    invoke-virtual {v5, v2, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->f:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "fakeModel"

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_8
    const-string v2, "GUID"

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    invoke-virtual {v5, v2, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 106
    const-string v5, "cdcontainer"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_9
    move v2, v3

    .line 105
    goto :goto_2

    :cond_a
    move-object v1, v0

    goto/16 :goto_1
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    const-string v2, "com.sec.spp.push"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    const/4 v0, 0x1

    .line 62
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 162
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 168
    const-string v2, "UNAService"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    const-string v2, "Samsung Apps"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    const-string v2, "Samsung Installer Utility"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    const-string v2, "Samsung Ad Utility"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    const-string v2, "Samsung Push Service"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    :cond_0
    move v0, v1

    .line 187
    :cond_1
    :goto_0
    return v0

    .line 177
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    .line 178
    goto :goto_0

    .line 180
    :cond_4
    if-eqz p1, :cond_1

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a:Ljava/lang/String;

    const-string v1, "KNOX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 182
    const/4 v0, 0x3

    goto :goto_0

    .line 184
    :cond_5
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    const-string v2, "com.adpreference.adp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const/4 v0, 0x1

    .line 73
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public getCancelIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 78
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 79
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/Main;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80
    const-string v1, "CANCELDOWNLOAD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    const-string v1, "PACKAGENAME"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->g:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDetailPageIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a(Z)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public getExecuteItent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->a(Z)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
