.class final Lcom/sec/android/app/samsungapps/view/downloadable/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->c(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->d(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)I

    move-result v1

    invoke-interface {v0, v4, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;->itemClick(Landroid/view/View;I)V

    .line 324
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->f(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->e(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isKNOXApp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->j(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->h(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->i(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->k(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->l(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->m(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->p(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->o(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->n(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 299
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->q(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->r(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->launchApp(Ljava/lang/String;Z)Z

    goto/16 :goto_0

    .line 306
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->s(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->t(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)I

    move-result v1

    invoke-interface {v0, v4, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;->itemClick(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 314
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->u(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->v(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    goto/16 :goto_0

    .line 317
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->x(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->w(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->y(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->A(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/e;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->z(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801e4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
