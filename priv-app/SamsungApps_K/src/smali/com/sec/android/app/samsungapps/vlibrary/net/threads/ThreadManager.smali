.class public Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;


# instance fields
.field _CurThreadCount:I

.field _MaximumThreadCount:I

.field private _NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

.field _RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    .line 10
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_MaximumThreadCount:I

    .line 11
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_CurThreadCount:I

    .line 16
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_MaximumThreadCount:I

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 18
    return-void
.end method


# virtual methods
.method public addRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    .locals 3

    .prologue
    .line 22
    monitor-enter p0

    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->addQueue(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 25
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_CurThreadCount:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_MaximumThreadCount:I

    if-ge v0, v1, :cond_0

    .line 27
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_CurThreadCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_CurThreadCount:I

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 29
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->start()V

    .line 31
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onThreadRunnerDestroyed()V
    .locals 1

    .prologue
    .line 44
    monitor-enter p0

    .line 46
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_CurThreadCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_CurThreadCount:I

    .line 47
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public takeRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;
    .locals 1

    .prologue
    .line 36
    monitor-enter p0

    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->_RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->take()Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
