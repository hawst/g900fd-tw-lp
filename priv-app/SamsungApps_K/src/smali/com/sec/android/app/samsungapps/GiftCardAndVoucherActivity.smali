.class public Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# static fields
.field public static mUpdateAppCount:I


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/dx;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getMyTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;->getMyTabEventType()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0x21

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x22

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/app/samsungapps/dx;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 77
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_1

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->finish()V

    .line 79
    const/4 v0, 0x0

    goto :goto_1

    .line 83
    :cond_1
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_1

    .line 86
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->reArrangeMenuItem()V

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 4

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 101
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/dw;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/dw;-><init>(Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 117
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_1

    .line 118
    sget-object v0, Lcom/sec/android/app/samsungapps/dx;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 123
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 52
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 54
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 57
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->a:Landroid/content/Context;

    .line 58
    const v1, 0x7f080226

    const/4 v2, 0x0

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0003

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 63
    const v0, 0x7f04008b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->setMainView(I)V

    .line 65
    const v0, 0x7f0c0205

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->setType(I)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->loadWidget()V

    .line 68
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 31
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 32
    return-void
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 38
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 39
    const v0, 0x7f0c0003

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 41
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
