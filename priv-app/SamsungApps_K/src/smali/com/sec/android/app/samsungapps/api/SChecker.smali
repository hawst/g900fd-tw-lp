.class public Lcom/sec/android/app/samsungapps/api/SChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final b:[I


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/samsungapps/api/SChecker;->b:[I

    return-void

    :array_0
    .array-data 4
        -0x68315e8
        0x7a7eaf4b
        -0x344b7c83    # -2.3660282E7f
        -0x4fa6f3d7
        0x623717ff
        -0x4c8716a4
        -0x4b5224d7
        -0x4c8716a4
        -0x4b5224d7
        -0x3649ab5
        -0x3649ab5
        -0x3649ab5
        -0x3998891c
        -0x2e28d493
        -0x724c0c96
        0x5509b3ab
        0x3d5b1e8e
        -0x3998891c
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/api/SChecker;->a:Landroid/content/Context;

    .line 22
    return-void
.end method

.method private a([I)Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 45
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/SChecker;->a:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, v2, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    move-object v4, v0

    .line 46
    :goto_0
    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_5

    aget-object v0, v4, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 48
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/api/SChecker;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v6, 0x40

    invoke-virtual {v3, v0, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v6, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 49
    array-length v7, v6

    move v3, v1

    :goto_2
    if-ge v3, v7, :cond_4

    aget-object v8, v6, v3

    array-length v9, p1

    move v0, v1

    :goto_3
    if-ge v0, v9, :cond_3

    aget v10, p1, v0

    invoke-virtual {v8}, Landroid/content/pm/Signature;->hashCode()I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v11

    if-ne v11, v10, :cond_2

    const/4 v0, 0x1

    .line 58
    :goto_4
    return v0

    .line 45
    :cond_1
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_0

    .line 49
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_4

    .line 52
    :catch_0
    move-exception v0

    .line 53
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "SChecker::NameNotFoundException::"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 46
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 56
    goto :goto_4

    .line 58
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_4
.end method


# virtual methods
.method public checkSignatureForBind()Z
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/samsungapps/api/SChecker;->b:[I

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/api/SChecker;->a([I)Z

    move-result v0

    return v0
.end method
