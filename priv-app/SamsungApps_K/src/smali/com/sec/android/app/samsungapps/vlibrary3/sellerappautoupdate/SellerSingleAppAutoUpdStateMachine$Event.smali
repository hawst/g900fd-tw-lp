.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOES_NOT_MEET_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

.field public static final enum DOWNLOAD_FAILED_OR_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

.field public static final enum DOWNLOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

.field public static final enum MEET_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    const-string v1, "DOES_NOT_MEET_CONDITION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOES_NOT_MEET_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    const-string v1, "MEET_CONDITION"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->MEET_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    const-string v1, "DOWNLOAD_FAILED_OR_CANCELED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_FAILED_OR_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    const-string v1, "DOWNLOAD_SUCCESS"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    const-string v1, "USER_CANCEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    .line 26
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOES_NOT_MEET_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->MEET_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_FAILED_OR_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    return-object v0
.end method
