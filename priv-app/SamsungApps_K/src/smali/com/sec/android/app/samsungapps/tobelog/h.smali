.class final Lcom/sec/android/app/samsungapps/tobelog/h;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/tobelog/h;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

.field private d:I

.field private e:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static a()Lcom/sec/android/app/samsungapps/tobelog/h;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/h;->a:Lcom/sec/android/app/samsungapps/tobelog/h;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/h;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/tobelog/h;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/h;->a:Lcom/sec/android/app/samsungapps/tobelog/h;

    .line 28
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/h;->a:Lcom/sec/android/app/samsungapps/tobelog/h;

    return-object v0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x6

    .line 61
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 64
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 65
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 67
    if-eq v1, v0, :cond_0

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/h;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/h;->f()V

    .line 91
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    .line 92
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/h;->e()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 45
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->b:Landroid/content/Context;

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    const-string v1, "log_traffic"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    const-string v1, "traffic_update_time"

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    .line 51
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_1

    .line 52
    iput v2, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    goto :goto_0

    .line 56
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/h;->f()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 74
    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/h;->a:Lcom/sec/android/app/samsungapps/tobelog/h;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/h;->d()V

    .line 78
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->b:Landroid/content/Context;

    .line 79
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    .line 81
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    .line 82
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/h;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    :cond_0
    return v1
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    const-string v1, "log_traffic"

    iget v2, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->putInt(Ljava/lang/String;I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->c:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    const-string v1, "traffic_update_time"

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/tobelog/h;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->putLong(Ljava/lang/String;J)V

    .line 131
    :cond_0
    return-void
.end method
