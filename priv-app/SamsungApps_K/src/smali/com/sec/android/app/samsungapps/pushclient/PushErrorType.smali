.class public final enum Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ADMIN_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum ADMIN_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum SPP_CONNECTION_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum SPP_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum WEB_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum WEB_DEREG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum WEB_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum WEB_REG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field public static final enum WEB_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "SPP_CONNECTION_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_CONNECTION_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "SPP_NOT_AVAILABLE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "SPP_REG_FAIL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "SPP_DEREG_FAIL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "WEB_SIGN_ERROR"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "WEB_REG_FAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "WEB_REG_SIGN_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_REG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "WEB_DEREG_FAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "WEB_DEREG_SIGN_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_DEREG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "ADMIN_DEREG_FAIL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->ADMIN_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "ADMIN_REG_FAIL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->ADMIN_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    .line 3
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_CONNECTION_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_REG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_DEREG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->ADMIN_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->ADMIN_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->a:[Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->a:[Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    return-object v0
.end method
