.class public Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/NotiDialogObserver;
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field a:Landroid/content/Context;

.field b:Z

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

.field f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

.field private g:Lcom/sec/android/app/samsungapps/LoadingDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V
    .locals 11

    .prologue
    const v3, 0x7f0c0144

    const v2, 0x7f0c0140

    const v1, 0x7f040082

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->setMainViewAndEmptyView(I)V

    :goto_0
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0149

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->b:Z

    if-nez v0, :cond_0

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0149

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/ci;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ci;-><init>(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f0c01d3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0146

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->d:Landroid/widget/Button;

    if-eqz v0, :cond_2

    if-nez v4, :cond_5

    :cond_2
    :goto_2
    return-void

    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->setMainView(I)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0142

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    :cond_5
    const v0, 0x7f0c01d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c01d8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c01d9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v3, 0x7f0c01de

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v3, 0x7f0c01dc

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v6, 0x7f0c01da

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v7

    if-eqz v7, :cond_9

    const v3, 0x7f0c01d6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v6, 0x7f0c01d1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0c01d4

    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v3, :cond_6

    if-eqz v6, :cond_6

    if-eqz v7, :cond_6

    const v8, 0x7f020081

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    invoke-virtual {v3, v9}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->d:Landroid/widget/Button;

    invoke-virtual {v3, v9}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    if-eqz v2, :cond_7

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    const v1, 0x7f0802a1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->d:Landroid/widget/Button;

    const v1, 0x7f080235

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->isUSA()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    if-eqz v0, :cond_8

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c01df

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->street:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->city:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->state:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->zip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " USA"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/cj;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cj;-><init>(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->d:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/ck;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ck;-><init>(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_9
    if-eqz v0, :cond_b

    if-eqz v1, :cond_b

    if-eqz v2, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v7, v7, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireYear:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v7, v7, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireYear:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->getCreditCardLast4Digit()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v7, v7, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireMonth:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v7, v7, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireYear:Ljava/lang/String;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->getCreditCardType()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_5
    :pswitch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v7, "MM"

    invoke-direct {v1, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v7, v7, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireYear:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-lt v7, v8, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v7, v7, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireYear:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ne v7, v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireMonth:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ge v0, v1, :cond_c

    :cond_a
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    :goto_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    const v2, 0x7f0801b8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    :pswitch_1
    const v0, 0x7f02012f

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_5

    :pswitch_2
    const v0, 0x7f020135

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_5

    :pswitch_3
    const v0, 0x7f020137

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    :pswitch_4
    const v0, 0x7f020139

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    :pswitch_5
    const v0, 0x7f02013c

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->validYN:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->validYN:Ljava/lang/String;

    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :cond_d
    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->d:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getMyTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;->getMyTabEventType()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;

    invoke-direct {v0, v2, v2}, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;->changeIranCreditCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    :goto_1
    new-instance v1, Lcom/sec/android/app/samsungapps/cm;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cm;-><init>(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createChangeCreditCardCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    new-instance v1, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->loadCreditCardInfo()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ch;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ch;-><init>(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 82
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    const v1, 0xff2b

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/NotiDialog;->questionDlg(Lcom/sec/android/app/samsungapps/NotiDialogObserver;IIZ)V

    .line 406
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    .line 410
    return-void
.end method

.method final b()V
    .locals 4

    .prologue
    .line 414
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    const v1, 0xff46

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/NotiDialog;->questionDlg(Lcom/sec/android/app/samsungapps/NotiDialogObserver;IIZ)V

    .line 415
    return-void
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 292
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 308
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    move-object v0, p1

    .line 295
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 296
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v2, :cond_0

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->finish()V

    move v0, v1

    .line 298
    goto :goto_0

    .line 300
    :cond_0
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_0

    .line 302
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 305
    :cond_1
    const v0, 0xff2a

    invoke-static {p0, v0, v1, v1}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Lcom/sec/android/app/samsungapps/NotiDialogObserver;IZZ)V

    .line 306
    const/4 v0, 0x1

    goto :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 4

    .prologue
    .line 314
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 316
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cl;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/cl;-><init>(Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 332
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_1

    .line 333
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 339
    :cond_1
    :goto_0
    return-void

    .line 338
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 333
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public isUSA()Z
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 53
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->a:Landroid/content/Context;

    .line 54
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f080270

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0xb0003

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    .line 57
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 59
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 283
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 284
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 286
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 287
    return-void
.end method

.method public onNotiDialogReceive(Lcom/sec/android/app/samsungapps/CommonDialogInterface;I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 420
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->getNotiType()I

    move-result v0

    .line 421
    sparse-switch v0, :sswitch_data_0

    .line 453
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 424
    :sswitch_0
    if-ne p2, v1, :cond_0

    .line 426
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->removeCard()Z

    .line 427
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    goto :goto_0

    .line 434
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->onBackPressed()V

    goto :goto_0

    .line 438
    :sswitch_2
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 444
    :sswitch_3
    if-ne p2, v1, :cond_0

    .line 446
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showChangeCreditCardView()V

    goto :goto_0

    .line 421
    :sswitch_data_0
    .sparse-switch
        0xff04 -> :sswitch_2
        0xff2a -> :sswitch_1
        0xff2b -> :sswitch_0
        0xff46 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CreditCardInfoActivity;->c()V

    .line 65
    return-void
.end method
