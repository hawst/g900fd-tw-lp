.class final Lcom/sec/android/app/samsungapps/view/bf;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/WebImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/WebImageView;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    if-eqz p2, :cond_1

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iput v2, v0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/view/WebImageView;->c:Z

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/WebImageView;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 176
    iput v1, v0, Landroid/os/Message;->what:I

    .line 177
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 178
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 179
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/WebImageView;->g:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 187
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->a(Lcom/sec/android/app/samsungapps/view/WebImageView;)Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->a(Lcom/sec/android/app/samsungapps/view/WebImageView;)Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;->downloadedDone(Z)V

    .line 190
    :cond_0
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget v0, v0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->a(Lcom/sec/android/app/samsungapps/view/WebImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/bf;->a:Lcom/sec/android/app/samsungapps/view/WebImageView;

    iput v2, v0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    goto :goto_0
.end method
