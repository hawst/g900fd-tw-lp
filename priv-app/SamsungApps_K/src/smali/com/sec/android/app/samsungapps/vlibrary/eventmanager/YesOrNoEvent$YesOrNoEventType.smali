.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

.field public static final enum NotifyTrafficChargeCanBeOccur:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    const-string v1, "NotifyTrafficChargeCanBeOccur"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->NotifyTrafficChargeCanBeOccur:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    .line 4
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->NotifyTrafficChargeCanBeOccur:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    return-object v0
.end method
