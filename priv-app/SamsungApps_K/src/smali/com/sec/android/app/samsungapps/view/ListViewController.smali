.class public Lcom/sec/android/app/samsungapps/view/ListViewController;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/InfLoadingState;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

.field private b:Landroid/widget/AbsListView;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private e:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private f:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private g:Lcom/sec/android/app/samsungapps/view/ViewList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 233
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/ListViewController;)Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 242
    :cond_1
    return-void
.end method


# virtual methods
.method public clearAdapterItem()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->clear()V

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setLoadingEmptyView()V

    .line 201
    :cond_0
    return-void
.end method

.method protected empty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-nez v1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setIndexOfFirstItem(I)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->release()V

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 100
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 101
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    .line 102
    return-void
.end method

.method public setArrayAdapter(Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V
    .locals 2

    .prologue
    .line 62
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setInfLoadingState(Lcom/sec/android/app/samsungapps/view/InfLoadingState;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ak;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/ak;-><init>(Lcom/sec/android/app/samsungapps/view/ListViewController;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setDisplayHandler(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getIndexOfFirstItem()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/al;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/al;-><init>(Lcom/sec/android/app/samsungapps/view/ListViewController;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/am;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/am;-><init>(Lcom/sec/android/app/samsungapps/view/ListViewController;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/an;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/an;-><init>(Lcom/sec/android/app/samsungapps/view/ListViewController;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/CustomGridView;->setOnScrollMoreCheckListener(Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;)V

    .line 76
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSearchList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isCompleteEmptyList()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->setNoDataEmptyView()V

    goto :goto_0

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method public setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 211
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 212
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 213
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 215
    return-void
.end method

.method public setListView(Landroid/widget/AbsListView;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    .line 42
    return-void
.end method

.method public setLoadingEmptyView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 332
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b()V

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->empty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 337
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setNoDataEmptyView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a()V

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b()V

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->empty()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSearchList()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isCompleteEmptyList()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    .line 281
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    goto :goto_0

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public setNoDataEmptyView(Z)V
    .locals 8

    .prologue
    const v7, 0x7f0801c9

    const v6, 0x7f0801c8

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    .line 293
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a()V

    .line 294
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b()V

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->empty()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSearchList()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isCompleteEmptyList()Z

    move-result v0

    if-ne v0, v1, :cond_6

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isPaidTabFirst()Z

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    instance-of v3, v1, Landroid/widget/TextView;

    if-nez v3, :cond_3

    .line 304
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    .line 321
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    goto :goto_0

    .line 303
    :cond_3
    instance-of v3, v0, Lcom/sec/android/app/samsungapps/WishListActivity;

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    instance-of v3, v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->getCurrButtonPosition()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LVC isPaid: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Pos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0801d0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_0
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0801ca

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 309
    :cond_6
    if-nez p1, :cond_2

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSelection(I)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/aj;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/view/aj;-><init>(Lcom/sec/android/app/samsungapps/view/ListViewController;I)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    .line 56
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListViewController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setVisibility(I)V

    .line 207
    :cond_0
    return-void
.end method
