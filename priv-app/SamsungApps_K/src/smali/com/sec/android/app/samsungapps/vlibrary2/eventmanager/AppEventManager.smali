.class public Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;


# instance fields
.field _observerList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->_observerList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 12
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    .line 20
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    return-object v0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;)V
    .locals 1

    .prologue
    .line 25
    monitor-enter p0

    .line 27
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->_observerList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clearObserver()V
    .locals 1

    .prologue
    .line 41
    monitor-enter p0

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->_observerList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clear()V

    .line 44
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public notifyEvent(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;)V
    .locals 2

    .prologue
    .line 49
    monitor-enter p0

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->_observerList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;

    .line 53
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;->onAppEvent(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;)Z
    .locals 1

    .prologue
    .line 33
    monitor-enter p0

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->_observerList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
