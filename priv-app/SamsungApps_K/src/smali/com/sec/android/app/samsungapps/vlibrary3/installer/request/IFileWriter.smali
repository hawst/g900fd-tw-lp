.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract close()V
.end method

.method public abstract deleteFile()V
.end method

.method public abstract download(Ljava/io/InputStream;)Z
.end method

.method public abstract getFileSize()J
.end method

.method public abstract open()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
.end method

.method public abstract setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;)V
.end method
