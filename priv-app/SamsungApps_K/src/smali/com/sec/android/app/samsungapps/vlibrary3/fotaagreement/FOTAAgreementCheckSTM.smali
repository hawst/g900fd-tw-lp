.class public Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 69
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 76
    :goto_0
    return-void

    .line 72
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;->CHECK_WHETHER_TNC_AGREED:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;->NOTIFY_AGREED:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;)Z
    .locals 2

    .prologue
    .line 42
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 64
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 45
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 48
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 56
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->DONE:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->NOT_AGREED:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 45
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 53
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    .line 91
    return-void
.end method
