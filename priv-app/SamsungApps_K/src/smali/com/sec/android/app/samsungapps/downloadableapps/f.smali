.class final Lcom/sec/android/app/samsungapps/downloadableapps/f;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAllTabSelected()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->checkButtonState()V

    .line 377
    return-void
.end method

.method public final onClickTab()V
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->updateWidget()V

    goto :goto_0
.end method

.method public final onDataLoadCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    move-result-object v0

    if-nez v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->k(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    goto :goto_0
.end method

.method public final onDataLoadingMore()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    move-result-object v0

    if-nez v0, :cond_0

    .line 339
    :goto_0
    return-void

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->j(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    goto :goto_0

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    goto :goto_0
.end method

.method public final onDownloadTabSelected()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->checkButtonState()V

    .line 371
    return-void
.end method

.method public final onUpdate()V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/f;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->checkButtonState()V

    .line 366
    return-void
.end method
