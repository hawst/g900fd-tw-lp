.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/BaseEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFirstEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
    .locals 3

    .prologue
    .line 8
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getFirstResponseRequiredEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 11
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/BaseEventManager;->getMyEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 16
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getMyEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
.end method

.method protected notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 24
    return-void
.end method

.method protected notifyResponseRequiredEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyResponseRequiredEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 29
    return-void
.end method
