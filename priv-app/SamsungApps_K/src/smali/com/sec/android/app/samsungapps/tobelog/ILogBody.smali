.class public interface abstract Lcom/sec/android/app/samsungapps/tobelog/ILogBody;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract getPageTimeDuration()J
.end method

.method public abstract increaseEventCount()I
.end method

.method public abstract sumPageTimeDuration(J)J
.end method

.method public abstract toString(Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;)Ljava/lang/String;
.end method
