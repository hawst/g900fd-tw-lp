.class public final enum Lcom/sec/android/app/samsungapps/tobelog/LogEvent;
.super Ljava/lang/Enum;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;


# static fields
.field public static final enum APP_FINISH:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum APP_START:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_ANNOUNCEMENT_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_APP_IN_SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_BIG_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_BOTTOM_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_BUY_DOWNLOAD_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_DISLIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_HELP_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_LIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MIDDLE_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_CURATED_SET:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_FROM_THIS_DEVELOPER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_REVIEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_TOP_FREE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_TOP_GAMES:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_TOP_NEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_MORE_TOP_PAID:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_OVERVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_PAYMENT_METHOD_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_PURCHASED_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_PURCHASED_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_PUSH_MESSAGE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_PUSH_SETTING_ON_OFF:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_RELATED_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_REVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_SEARCH_WITH_KEYWORD:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_SETTINGS_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_SHARE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_WISH_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum CLICK_WISH_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum PAGE_PV_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field public static final enum PURCHASE_COMPLETE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

.field private static final synthetic b:[Lcom/sec/android/app/samsungapps/tobelog/LogEvent;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "APP_START"

    const-string v2, "0100"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_START:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "APP_FINISH"

    const-string v2, "0101"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_FINISH:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "PAGE_PV_PATH_TIME"

    const-string v2, "1100"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "PAGE_PV_TIME"

    const-string v2, "1101"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_BUY_DOWNLOAD_BUTTON"

    const-string v2, "1200"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BUY_DOWNLOAD_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "PURCHASE_COMPLETE"

    const/4 v2, 0x5

    const-string v3, "1201"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PURCHASE_COMPLETE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_PURCHASED_LIST_BUTTON"

    const/4 v2, 0x6

    const-string v3, "2100"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_WISH_LIST_MENU"

    const/4 v2, 0x7

    const-string v3, "2101"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_PURCHASED_LIST_MENU"

    const/16 v2, 0x8

    const-string v3, "2102"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_PAYMENT_METHOD_MENU"

    const/16 v2, 0x9

    const-string v3, "2103"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PAYMENT_METHOD_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_ANNOUNCEMENT_MENU"

    const/16 v2, 0xa

    const-string v3, "2104"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_ANNOUNCEMENT_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_SETTINGS_MENU"

    const/16 v2, 0xb

    const-string v3, "2105"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SETTINGS_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_HELP_MENU"

    const/16 v2, 0xc

    const-string v3, "2106"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_HELP_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_TOP_GAMES"

    const/16 v2, 0xd

    const-string v3, "2111"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_GAMES:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_TOP_PAID"

    const/16 v2, 0xe

    const-string v3, "2112"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_PAID:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_TOP_FREE"

    const/16 v2, 0xf

    const-string v3, "2113"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_FREE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_TOP_NEW"

    const/16 v2, 0x10

    const-string v3, "2114"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_NEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "MOVE_OUT_FROM_MAIN_WITHOUT_ACTION"

    const/16 v2, 0x11

    const-string v3, "2130"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_CURATED_SET"

    const/16 v2, 0x12

    const-string v3, "2131"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_CURATED_SET:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_BIG_BANNER"

    const/16 v2, 0x13

    const-string v3, "2200"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BIG_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MIDDLE_BANNER"

    const/16 v2, 0x14

    const-string v3, "2201"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MIDDLE_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_BOTTOM_BANNER"

    const/16 v2, 0x15

    const-string v3, "2202"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BOTTOM_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_SHARE_BUTTON"

    const/16 v2, 0x16

    const-string v3, "2210"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SHARE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_WISH_LIST_BUTTON"

    const/16 v2, 0x17

    const-string v3, "2211"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_LIKE_BUTTON"

    const/16 v2, 0x18

    const-string v3, "2212"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_LIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_DISLIKE_BUTTON"

    const/16 v2, 0x19

    const-string v3, "2213"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_DISLIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_OVERVIEW_TAB"

    const/16 v2, 0x1a

    const-string v3, "2220"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_OVERVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_REVIEW_TAB"

    const/16 v2, 0x1b

    const-string v3, "2221"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_REVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_RELATED_TAB"

    const/16 v2, 0x1c

    const-string v3, "2222"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_RELATED_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_REVIEW"

    const/16 v2, 0x1d

    const-string v3, "2223"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_REVIEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_MORE_FROM_THIS_DEVELOPER"

    const/16 v2, 0x1e

    const-string v3, "2224"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_FROM_THIS_DEVELOPER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_PUSH_SETTING_ON_OFF"

    const/16 v2, 0x1f

    const-string v3, "2230"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_SETTING_ON_OFF:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_APP_IN_SEARCH_RESULT"

    const/16 v2, 0x20

    const-string v3, "2300"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_APP_IN_SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_SEARCH_WITH_KEYWORD"

    const/16 v2, 0x21

    const-string v3, "3100"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SEARCH_WITH_KEYWORD:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    const-string v1, "CLICK_PUSH_MESSAGE"

    const/16 v2, 0x22

    const-string v3, "2140"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_MESSAGE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 4
    const/16 v0, 0x23

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_START:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_FINISH:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BUY_DOWNLOAD_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PURCHASE_COMPLETE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PAYMENT_METHOD_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_ANNOUNCEMENT_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SETTINGS_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_HELP_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_GAMES:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_PAID:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_FREE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_NEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_CURATED_SET:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BIG_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MIDDLE_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BOTTOM_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SHARE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_LIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_DISLIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_OVERVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_REVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_RELATED_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_REVIEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_FROM_THIS_DEVELOPER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_SETTING_ON_OFF:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_APP_IN_SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SEARCH_WITH_KEYWORD:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_MESSAGE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->b:[Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->a:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogEvent;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/LogEvent;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->b:[Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    return-object v0
.end method


# virtual methods
.method public final value()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->a:Ljava/lang/String;

    return-object v0
.end method
