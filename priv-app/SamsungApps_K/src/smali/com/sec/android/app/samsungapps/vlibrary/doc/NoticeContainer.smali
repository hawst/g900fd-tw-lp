.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public mNoticeArrayList:Ljava/util/ArrayList;

.field private mNoticeDetailQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

.field private mNoticeList:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeList:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeDetailQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeArrayList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getNoticeArray()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNoticeDetailQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeDetailQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    return-object v0
.end method

.method public getNoticeList()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeList:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    return-object v0
.end method
