.class final Lcom/sec/android/app/samsungapps/ep;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/webkit/DownloadListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 384
    const-string v0, ".apk"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "InicisWebViewClient::shouldOverrideUrlLoading::file="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Lcom/sec/android/app/samsungapps/eq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Lcom/sec/android/app/samsungapps/eq;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/eq;->cancel(Z)Z

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Lcom/sec/android/app/samsungapps/eq;)Lcom/sec/android/app/samsungapps/eq;

    .line 395
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    new-instance v0, Lcom/sec/android/app/samsungapps/eq;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    move-object v2, p1

    move-wide v3, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/eq;-><init>(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Ljava/lang/String;JB)V

    invoke-static {v6, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Lcom/sec/android/app/samsungapps/eq;)Lcom/sec/android/app/samsungapps/eq;

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ep;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Lcom/sec/android/app/samsungapps/eq;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/eq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 399
    :cond_1
    return-void
.end method
