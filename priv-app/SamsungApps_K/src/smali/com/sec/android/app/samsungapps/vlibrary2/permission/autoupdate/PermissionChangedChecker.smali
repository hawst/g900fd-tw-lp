.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private _APKPermission:[Ljava/lang/String;

.field private _AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private _CPermissionRequestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/b;

.field private _Context:Landroid/content/Context;

.field private _GUID:Ljava/lang/String;

.field private _IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

.field private _ServerResult:Ljava/util/HashMap;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    .line 30
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_GUID:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_Context:Landroid/content/Context;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/b;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_Context:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_CPermissionRequestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/b;

    .line 35
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_ServerResult:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V

    return-void
.end method

.method private comparePermission()V
    .locals 5

    .prologue
    .line 103
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_ServerResult:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 107
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    .line 109
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionID()Ljava/lang/String;

    move-result-object v0

    const-string v4, "exist"

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_APKPermission:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 115
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117
    :cond_2
    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V

    .line 125
    :goto_2
    return-void

    .line 123
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V

    goto :goto_2
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)Z

    .line 45
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V

    .line 40
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;)V
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 69
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->comparePermission()V

    goto :goto_0

    .line 72
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackagePermission(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_APKPermission:[Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_APKPermission:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 75
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)V

    goto :goto_0

    .line 79
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_CPermissionRequestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/b;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/b;->request()V

    goto :goto_0

    .line 82
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;->onResultPermissionFailed()V

    goto :goto_0

    .line 88
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;->onResultPermissionChanged()V

    goto :goto_0

    .line 94
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_IPermissionChangedResult:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker$IPermissionChangedResult;->onResultPermissionSame()V

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    .line 57
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedChecker;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;)V

    return-void
.end method
