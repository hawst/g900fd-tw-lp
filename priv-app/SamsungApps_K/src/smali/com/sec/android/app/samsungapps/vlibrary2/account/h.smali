.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/h;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->loginODCWithOldAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/h;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;Z)V

    goto :goto_0
.end method
