.class public interface abstract Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getCurrentVersion()Ljava/lang/String;
.end method

.method public abstract getHelpText()Ljava/lang/String;
.end method

.method public abstract getLatestVersion()Ljava/lang/String;
.end method

.method public abstract isUpdateAvailable()Z
.end method

.method public abstract sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
.end method
