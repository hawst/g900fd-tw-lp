.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field curCount:I

.field maxCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    if-nez p1, :cond_1

    .line 21
    :cond_0
    :goto_0
    return-void

    .line 11
    :cond_1
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 16
    array-length v1, v0

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 19
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    .line 20
    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->maxCount:I

    goto :goto_0
.end method


# virtual methods
.method public getCurCount()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    return v0
.end method

.method public getMaxCount()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->maxCount:I

    return v0
.end method

.method public isDownloadCountReachedMax()Z
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->maxCount:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->maxCount:I

    if-lt v0, v1, :cond_0

    .line 44
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDownloadCountRemained()Z
    .locals 2

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->maxCount:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    if-lt v0, v1, :cond_1

    .line 36
    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNeverDownloaded()Z
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;->curCount:I

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
