.class final Lcom/sec/android/app/samsungapps/br;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

.field final synthetic b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;)V
    .locals 0

    .prologue
    .line 944
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/br;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 948
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-void

    .line 952
    :cond_1
    if-eqz p1, :cond_2

    .line 953
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetData(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->loadWidget(I)V

    .line 961
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->onWidgetSetViewState()V

    goto :goto_0

    .line 959
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/br;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetState(I)V

    goto :goto_1
.end method
