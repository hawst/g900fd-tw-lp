.class public Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkValidString(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 55
    if-nez p0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getEditText(Landroid/app/Activity;I)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method public static getEditText(Landroid/view/View;I)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method public static getString(Landroid/app/Activity;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getEditText(Landroid/app/Activity;I)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getString(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getString(Landroid/view/View;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getEditText(Landroid/view/View;I)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getString(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getString(Landroid/widget/EditText;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    if-nez p0, :cond_1

    .line 12
    const-string v0, ""

    .line 20
    :cond_0
    :goto_0
    return-object v0

    .line 15
    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 16
    if-nez v0, :cond_0

    .line 20
    const-string v0, ""

    goto :goto_0
.end method

.method public static setString(Landroid/app/Activity;ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 77
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getEditText(Landroid/app/Activity;I)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->setString(Landroid/widget/EditText;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static setString(Landroid/view/View;ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getEditText(Landroid/view/View;I)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->setString(Landroid/widget/EditText;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static setString(Landroid/widget/EditText;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 67
    if-nez p0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 71
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const/4 v0, 0x1

    goto :goto_0
.end method
