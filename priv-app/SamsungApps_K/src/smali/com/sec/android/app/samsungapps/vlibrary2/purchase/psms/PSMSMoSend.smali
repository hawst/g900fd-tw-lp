.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IPSMSMoSendData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_IPSMSMoSendData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_IPSMSMoSendData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_IPSMSMoSendData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/m;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/n;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->requestMOSend(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 47
    return-void
.end method

.method protected requestMOSend(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mODeliveryResult(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 54
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 55
    return-void
.end method
