.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private cvcRequired:Z

.field private mDiscountPrice:D

.field private mNormalPrice:D

.field private mPaymentPrice:D

.field private mPurchaseType:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

.field private mSelectedCoupon:Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

.field private mbShowAllCoupon:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;DDZ)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->cvcRequired:Z

    .line 22
    iput-wide p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mNormalPrice:D

    .line 23
    iput-wide p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mDiscountPrice:D

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mPurchaseType:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    .line 25
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mbShowAllCoupon:Z

    .line 26
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mDiscountPrice:D

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mPaymentPrice:D

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->selectCoupon(Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getDiscountPrice()D
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mDiscountPrice:D

    return-wide v0
.end method

.method protected getFinalPrice()D
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mSelectedCoupon:Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mSelectedCoupon:Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mPaymentPrice:D

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->calcPaymentPrice(D)D

    move-result-wide v0

    .line 83
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mPaymentPrice:D

    goto :goto_0
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mNormalPrice:D

    return-wide v0
.end method

.method public getPaymentPrice()D
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mPaymentPrice:D

    return-wide v0
.end method

.method public getPurchaseType()Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mPurchaseType:Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec$PurchaseType;

    return-object v0
.end method

.method public getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mSelectedCoupon:Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    return-object v0
.end method

.method public getShowAllCoupon()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mbShowAllCoupon:Z

    return v0
.end method

.method public isCVCRequired()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->cvcRequired:Z

    return v0
.end method

.method public selectCoupon(Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->mSelectedCoupon:Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    .line 65
    return-void
.end method

.method public setCVCRequired()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseMethodSpec;->cvcRequired:Z

    .line 32
    return-void
.end method
