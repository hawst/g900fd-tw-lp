.class public Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/notipopup/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    .line 119
    return-void
.end method

.method public static showDlg(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;)V
    .locals 2

    .prologue
    .line 94
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 96
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 98
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/samsungapps/notipopup/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;)Lcom/sec/android/app/samsungapps/notipopup/d;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/notipopup/d;->b(Landroid/content/Intent;)V

    .line 100
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public static showDlgWithoutCheckbox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;)V
    .locals 2

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 106
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 108
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/samsungapps/notipopup/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;)Lcom/sec/android/app/samsungapps/notipopup/d;

    move-result-object v1

    .line 109
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/notipopup/d;->b(Landroid/content/Intent;)V

    .line 110
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 111
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 25
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->requestWindowFeature(I)Z

    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/notipopup/d;->a(Landroid/content/Intent;)Lcom/sec/android/app/samsungapps/notipopup/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->c:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    new-instance v1, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->b:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    const v2, 0x7f0802ef

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/notipopup/b;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/app/samsungapps/notipopup/b;-><init>(Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;Lcom/sec/android/app/samsungapps/notipopup/d;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    const v2, 0x7f08023d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/notipopup/c;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/notipopup/c;-><init>(Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;Lcom/sec/android/app/samsungapps/notipopup/d;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->show()Z

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08023d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->a:Ljava/lang/String;

    new-instance v5, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    invoke-direct {v5, v4, v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/notipopup/a;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/notipopup/a;-><init>(Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;Lcom/sec/android/app/samsungapps/notipopup/d;)V

    invoke-virtual {v5, p0, v1}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->finish()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 35
    :catch_1
    move-exception v0

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ActivityPopupDlg::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->finish()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->a:Lcom/sec/android/app/samsungapps/notipopup/d;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/notipopup/d;->d:Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;->onCancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->finish()V

    .line 137
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 135
    :catch_1
    move-exception v0

    goto :goto_0
.end method
