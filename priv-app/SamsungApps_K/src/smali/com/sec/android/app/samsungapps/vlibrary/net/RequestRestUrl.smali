.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;
.source "ProGuard"


# static fields
.field public static final FNISH_WAITING:I = 0x2

.field public static final NOTIFY_RESPONSE:I = 0x0

.field public static final START_WAITING:I = 0x1


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mHandler:Landroid/os/Handler;

.field protected mHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field protected mIsMobileData:Z

.field protected mRetryCount:I

.field protected mURL:Ljava/lang/String;

.field protected mWaitingDialog:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;-><init>()V

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mIsMobileData:Z

    .line 28
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mRetryCount:I

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mURL:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mContext:Landroid/content/Context;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHandler:Landroid/os/Handler;

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mWaitingDialog:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mURL:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mContext:Landroid/content/Context;

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHandler:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->notifyResultToUI(Z)V

    return-void
.end method

.method public static extractAddressFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 165
    const-string v0, ""

    .line 167
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 169
    :cond_0
    const-string v1, "RequestRestUrl::extractAddressFromUrl::URL is empty"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    move-object p0, v0

    .line 216
    :cond_1
    :goto_0
    return-object p0

    .line 176
    :cond_2
    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 177
    if-lez v0, :cond_3

    .line 179
    add-int/lit8 v0, v0, 0x3

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 190
    :cond_3
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 191
    if-ltz v0, :cond_4

    .line 193
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 200
    :cond_4
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 201
    if-ltz v0, :cond_5

    .line 203
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 210
    :cond_5
    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 211
    if-ltz v0, :cond_1

    .line 213
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private notifyResultToUI(Z)V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    invoke-virtual {p0, p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 253
    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x0

    return v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;)V

    return-object v0
.end method

.method public getHttpGet()Lorg/apache/http/client/methods/HttpGet;
    .locals 2

    .prologue
    .line 337
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mURL:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method

.method public getThreadNo()I
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x3

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mURL:Ljava/lang/String;

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x0

    return v0
.end method

.method public isSucceed()Z
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x1

    return v0
.end method

.method protected lookupHost(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 226
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 238
    const/4 v1, 0x3

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/4 v2, 0x1

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/4 v2, 0x0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 243
    :goto_0
    return v0

    .line 234
    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public needRetry()Z
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    return v0
.end method

.method protected notifyFinishSending()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 333
    return-void
.end method

.method protected notifyStartSending()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 325
    return-void
.end method

.method public onResult(Lorg/apache/http/HttpResponse;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 275
    .line 277
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 278
    const/16 v3, 0xc8

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 293
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mIsMobileData:Z

    if-ne v3, v1, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->stopUsingMobileNetwork()I

    .line 296
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mIsMobileData:Z

    .line 302
    :cond_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 303
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 304
    iput v2, v1, Landroid/os/Message;->what:I

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 307
    return-void

    .line 287
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RequestRestUrl::onResult::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->w(Ljava/lang/String;)V

    move v0, v2

    goto :goto_0
.end method

.method public send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
    .locals 2

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->notifyStartSending()V

    .line 264
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mIsMobileData:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->startUsingMobileNetwork()Z

    .line 269
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V

    .line 270
    return-void
.end method

.method public setMobileData(Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mIsMobileData:Z

    .line 87
    return-void
.end method

.method public setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mWaitingDialog:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    .line 317
    return-void
.end method

.method public startUsingMobileNetwork()Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 98
    const-string v2, "enableHIPRI"

    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v2

    .line 99
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "RequestRestUrl::startUsingMobileNetwork::setFeature="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    move v0, v1

    .line 137
    :goto_0
    return v0

    .line 108
    :cond_0
    :goto_1
    const/16 v2, 0x1e

    if-ge v1, v2, :cond_1

    .line 110
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    .line 111
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v2, v3}, Landroid/net/NetworkInfo$State;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mURL:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->extractAddressFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->lookupHost(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v0

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestRestUrl::startUsingMobileNetwork::request="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method public stopUsingMobileNetwork()I
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 151
    const/4 v1, 0x0

    const-string v2, "enableHIPRI"

    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    move-result v0

    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestRestUrl::stopUsingMobileNetwork::setFeature="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    .line 155
    return v0
.end method
