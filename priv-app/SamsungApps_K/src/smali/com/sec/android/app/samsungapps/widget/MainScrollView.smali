.class public Lcom/sec/android/app/samsungapps/widget/MainScrollView;
.super Landroid/widget/ScrollView;
.source "ProGuard"


# instance fields
.field private a:F

.field private b:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 11
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->a:F

    .line 12
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->b:F

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->a:F

    .line 12
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->b:F

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->a:F

    .line 12
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->b:F

    .line 22
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 33
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 36
    if-nez v0, :cond_0

    .line 37
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->a:F

    .line 38
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->b:F

    .line 41
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->a:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/MainScrollView;->b:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 42
    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
