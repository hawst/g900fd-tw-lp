.class public Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;->a:Landroid/content/Context;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 15
    return-void
.end method


# virtual methods
.method public getAppManager()Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;->a:Landroid/content/Context;

    .line 26
    return-void
.end method
