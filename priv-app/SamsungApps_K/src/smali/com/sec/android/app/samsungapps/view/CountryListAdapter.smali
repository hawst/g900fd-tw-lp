.class public final Lcom/sec/android/app/samsungapps/view/CountryListAdapter;
.super Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Z)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Z)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 32
    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    if-eqz p3, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->mOrgItems:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 35
    :cond_0
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 39
    if-nez p2, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->mLayoutId:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 44
    :cond_0
    const v0, 0x7f0c004d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 49
    if-eqz v0, :cond_2

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    .line 51
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->mFilter:Landroid/widget/Filter;

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CountryListAdapter;->mFilter:Landroid/widget/Filter;

    check-cast v2, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->getCurrentWord()Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->textToHighlight(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :cond_2
    return-object p2
.end method
