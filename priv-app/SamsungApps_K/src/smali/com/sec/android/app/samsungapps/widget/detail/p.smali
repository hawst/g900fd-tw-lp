.class final Lcom/sec/android/app/samsungapps/widget/detail/p;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVerticalScrollBarEnabled(Z)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setScrollContainer(Z)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHeight(I)V

    .line 121
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->requestDisallowInterceptTouchEvent(Z)V

    .line 123
    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_0
    return v2

    .line 126
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 130
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/p;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
