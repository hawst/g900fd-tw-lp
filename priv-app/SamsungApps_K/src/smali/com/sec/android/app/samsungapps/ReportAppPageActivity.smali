.class public Lcom/sec/android/app/samsungapps/ReportAppPageActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Landroid/widget/ListView;

.field b:Ljava/lang/String;

.field c:Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;

.field private d:Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->a:Landroid/widget/ListView;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->b:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->d:Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->e:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->c:Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ReportAppPageActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->e:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 5

    .prologue
    .line 63
    sparse-switch p1, :sswitch_data_0

    .line 93
    :goto_0
    return-void

    .line 66
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->finish()V

    goto :goto_0

    .line 71
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 72
    const-string v1, "productID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    const-string v2, "sellerName"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;

    const-string v3, "."

    invoke-direct {v2, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->setType(Ljava/lang/String;)V

    .line 77
    new-instance v3, Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->e:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->d:Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->d:Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;

    invoke-virtual {v3, v1, v0, v2}, Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;->createReportProblem(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->e:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/hi;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/hi;-><init>(Lcom/sec/android/app/samsungapps/ReportAppPageActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 63
    :sswitch_data_0
    .sparse-switch
        0xa000c -> :sswitch_0
        0xa0014 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 33
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f04003a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->setMainView(I)V

    .line 36
    new-array v4, v7, [I

    fill-array-data v4, :array_0

    const v5, 0xb0001

    move-object v0, p0

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 40
    const v0, 0xa0014

    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->enableActionItem(IZ)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showNavi(Z)V

    .line 45
    :cond_0
    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const v0, 0x7f0801ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    const v0, 0x7f0801af

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    const v0, 0x7f080197

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v7

    const/4 v0, 0x3

    const v2, 0x7f080184

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    const v2, 0x7f080185

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    const v2, 0x7f080186

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 52
    const v0, 0x7f0c010e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->a:Landroid/widget/ListView;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->a:Landroid/widget/ListView;

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v4, 0x7f04001e

    invoke-direct {v2, p0, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 58
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->e:Landroid/content/Context;

    .line 59
    return-void

    .line 36
    nop

    :array_0
    .array-data 4
        0xa000c
        0xa0014
    .end array-data
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "00"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, p3, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    const v1, 0xa0014

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->enableActionItem(IZ)V

    .line 100
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;->b:Ljava/lang/String;

    .line 102
    return-void
.end method
