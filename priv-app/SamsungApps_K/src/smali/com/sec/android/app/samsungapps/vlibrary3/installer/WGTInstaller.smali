.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

.field private c:Ljava/io/File;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a:Landroid/os/Handler;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->c:Ljava/io/File;

    .line 24
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 115
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 111
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;)V
    .locals 3

    .prologue
    .line 14
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "gear2Install"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->c:Ljava/io/File;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/File;->setReadable(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->getAPI()Lcom/samsung/android/aidl/ICheckAppInstallState;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ah;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ah;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;)V

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/aidl/ICheckAppInstallState;->installWGT(Ljava/lang/String;Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WO:REMOTEERR"

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "WO:REMOTEERR"

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 129
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "notifyFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a()V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WO:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed(Ljava/lang/String;)V

    .line 135
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;)V
    .locals 2

    .prologue
    .line 14
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "notifySuccess"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallSuccess()V

    :cond_0
    return-void
.end method


# virtual methods
.method public install()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "install"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->setConnectionObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager$IGearAPIConnectionStateObserver;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->connect()V

    .line 50
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    .line 147
    return-void
.end method

.method public userCancel()V
    .locals 2

    .prologue
    .line 139
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "userCancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    return-void
.end method
