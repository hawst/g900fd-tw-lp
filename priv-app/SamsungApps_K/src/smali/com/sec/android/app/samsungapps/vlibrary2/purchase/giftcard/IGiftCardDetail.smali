.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardDetail;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getAvailableServices()Ljava/lang/String;
.end method

.method public abstract getBalanceAmount()D
.end method

.method public abstract getCurrencyCode()Ljava/lang/String;
.end method

.method public abstract getCurrencyUnit()Ljava/lang/String;
.end method

.method public abstract getGiftCardCode()Ljava/lang/String;
.end method

.method public abstract getGiftCardDescription()Ljava/lang/String;
.end method

.method public abstract getGiftCardImagePath()Ljava/lang/String;
.end method

.method public abstract getGiftCardName()Ljava/lang/String;
.end method

.method public abstract getGiftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;
.end method

.method public abstract getLastUsageDate()Ljava/lang/String;
.end method

.method public abstract getUserExpireDate()Ljava/lang/String;
.end method

.method public abstract getUserRegisterDate()Ljava/lang/String;
.end method

.method public abstract giftCardAmount()D
.end method
