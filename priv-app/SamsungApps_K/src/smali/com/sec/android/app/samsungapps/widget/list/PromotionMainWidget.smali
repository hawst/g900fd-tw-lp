.class public Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

.field private e:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    .line 31
    const v0, 0x7f040072

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->initView(Landroid/content/Context;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    .line 36
    const v0, 0x7f040072

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->initView(Landroid/content/Context;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    .line 41
    const v0, 0x7f040072

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->initView(Landroid/content/Context;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public loadWidget()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setObserver(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->load()V

    .line 72
    return-void
.end method

.method public onLoading()V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public onLoadingCompleted()V
    .locals 3

    .prologue
    .line 150
    const-string v0, "kimss"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadingCompleted : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->updateWidget()V

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->setVisibleNodata(I)Z

    goto :goto_0
.end method

.method public onLoadingFailed()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->setVisibleRetry(I)Z

    .line 146
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->updateWidget()V

    .line 76
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;->clear()V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;

    .line 52
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->release()V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    if-eqz v0, :cond_4

    .line 63
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    .line 65
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 66
    return-void
.end method

.method public setVisibleNodata(I)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 129
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 131
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 132
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 135
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    const v1, 0x7f0c0081

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 138
    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 124
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 125
    return-void
.end method

.method public updateWidget()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 86
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;

    .line 88
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 99
    :cond_1
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    .line 101
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v2, -0x2

    invoke-direct {v1, v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 104
    const v0, 0x7f0c01af

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    .line 107
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;-><init>(Landroid/content/Context;)V

    .line 108
    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 111
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->loadWidget()V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 90
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->a:Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->setVisibleLoading(I)Z

    .line 120
    :goto_3
    return-void

    .line 118
    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->setVisibleNodata(I)Z

    goto :goto_3
.end method
