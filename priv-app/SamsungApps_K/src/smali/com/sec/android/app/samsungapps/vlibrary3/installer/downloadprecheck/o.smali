.class final synthetic Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->REMEMBER_FREEPAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_1
    :try_start_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_TRUKEY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_3
    :try_start_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_DETAIL_INFO_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_NEEDTOSHOW_ALREADYPURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_5
    :try_start_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->STORAGE_SPACE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_6
    :try_start_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_7
    :try_start_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->NET_DOWNLOAD_SIZE_LIMIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_8
    :try_start_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_MULTIPLE_DOWNLOADCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_9
    :try_start_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->P2CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_a
    :try_start_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_b
    :try_start_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->VALIDATE_COMPATABILE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_c
    :try_start_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_d
    :try_start_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_e
    :try_start_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    .line 45
    :goto_f
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    :try_start_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_10
    :try_start_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->LOGIN_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_11
    :try_start_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->LOGIN_CHECK_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_12
    :try_start_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->TURKEY_CONDITION_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_13
    :try_start_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->TURKEY_CONDITION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_14
    :try_start_15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->DETAIL_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_15
    :try_start_16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_16
    :try_start_17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_17
    :try_start_18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_18
    :try_start_19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->FREE_STORAGE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_19
    :try_start_1a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->FREE_STORAGE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1a
    :try_start_1b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->NET_SIZE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1b
    :try_start_1c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->NET_SIZE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1c
    :try_start_1d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->MULTIPLE_DOWNLOADCOUNT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1d
    :try_start_1e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->MULTIPLE_DOWNLOADCOUNT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1e
    :try_start_1f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_1f
    :try_start_20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_20
    :try_start_21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_AGE_LIMIT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_21
    :try_start_22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_AGE_LIMIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_22
    :try_start_23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->VALIDATE_COMPATIBLE_OS_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_23
    :try_start_24
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->VALIDATE_COMPATIBLE_OS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_24
    :try_start_25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_REAL_NAME_AGE_NEED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_25
    :try_start_26
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_REAL_NAME_AGE_NEED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_26
    :try_start_27
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->PERMISSION_CHECK_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_27
    :try_start_28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/o;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->PERMISSION_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_28
    return-void

    :catch_0
    move-exception v0

    goto :goto_28

    :catch_1
    move-exception v0

    goto :goto_27

    :catch_2
    move-exception v0

    goto :goto_26

    :catch_3
    move-exception v0

    goto :goto_25

    :catch_4
    move-exception v0

    goto :goto_24

    :catch_5
    move-exception v0

    goto :goto_23

    :catch_6
    move-exception v0

    goto :goto_22

    :catch_7
    move-exception v0

    goto :goto_21

    :catch_8
    move-exception v0

    goto :goto_20

    :catch_9
    move-exception v0

    goto :goto_1f

    :catch_a
    move-exception v0

    goto/16 :goto_1e

    :catch_b
    move-exception v0

    goto/16 :goto_1d

    :catch_c
    move-exception v0

    goto/16 :goto_1c

    :catch_d
    move-exception v0

    goto/16 :goto_1b

    :catch_e
    move-exception v0

    goto/16 :goto_1a

    :catch_f
    move-exception v0

    goto/16 :goto_19

    :catch_10
    move-exception v0

    goto/16 :goto_18

    :catch_11
    move-exception v0

    goto/16 :goto_17

    :catch_12
    move-exception v0

    goto/16 :goto_16

    :catch_13
    move-exception v0

    goto/16 :goto_15

    :catch_14
    move-exception v0

    goto/16 :goto_14

    :catch_15
    move-exception v0

    goto/16 :goto_13

    :catch_16
    move-exception v0

    goto/16 :goto_12

    :catch_17
    move-exception v0

    goto/16 :goto_11

    :catch_18
    move-exception v0

    goto/16 :goto_10

    :catch_19
    move-exception v0

    goto/16 :goto_f

    :catch_1a
    move-exception v0

    goto/16 :goto_e

    :catch_1b
    move-exception v0

    goto/16 :goto_d

    :catch_1c
    move-exception v0

    goto/16 :goto_c

    :catch_1d
    move-exception v0

    goto/16 :goto_b

    :catch_1e
    move-exception v0

    goto/16 :goto_a

    :catch_1f
    move-exception v0

    goto/16 :goto_9

    :catch_20
    move-exception v0

    goto/16 :goto_8

    :catch_21
    move-exception v0

    goto/16 :goto_7

    :catch_22
    move-exception v0

    goto/16 :goto_6

    :catch_23
    move-exception v0

    goto/16 :goto_5

    :catch_24
    move-exception v0

    goto/16 :goto_4

    :catch_25
    move-exception v0

    goto/16 :goto_3

    :catch_26
    move-exception v0

    goto/16 :goto_2

    :catch_27
    move-exception v0

    goto/16 :goto_1

    :catch_28
    move-exception v0

    goto/16 :goto_0
.end method
