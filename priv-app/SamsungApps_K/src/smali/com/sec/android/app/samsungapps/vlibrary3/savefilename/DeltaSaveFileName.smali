.class public Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/DeltaSaveFileName;
.super Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;)V

    .line 9
    return-void
.end method

.method public static fromURLResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/DeltaSaveFileName;
    .locals 2

    .prologue
    .line 13
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->isSupportingDeltaDownload()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)V

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/DeltaSaveFileName;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/DeltaSaveFileName;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;)V

    .line 34
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected fileExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, ".delta"

    return-object v0
.end method
