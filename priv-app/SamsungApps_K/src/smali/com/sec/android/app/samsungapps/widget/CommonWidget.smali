.class public abstract Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.super Landroid/widget/FrameLayout;
.source "ProGuard"


# static fields
.field public static final WIDGET_STATE_EMPTY:I = 0x2

.field public static final WIDGET_STATE_LOADING:I = 0x1

.field public static final WIDGET_STATE_NONE:I = -0x1

.field public static final WIDGET_STATE_RETRY:I = 0x3

.field public static final WIDGET_STATE_VISIBLE:I


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:Landroid/view/View;

.field c:Z

.field protected configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:I

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->a:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->c:Z

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    .line 36
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->e:Z

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->a:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->c:Z

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    .line 36
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->e:Z

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->a:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->c:Z

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    .line 36
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->e:Z

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    .line 55
    return-void
.end method


# virtual methods
.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    return-object v0
.end method

.method public getNeedLoad()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->e:Z

    return v0
.end method

.method public getWidgetState()I
    .locals 1

    .prologue
    .line 363
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    return v0
.end method

.method public initView(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->mContext:Landroid/content/Context;

    .line 59
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0c0082

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const-string v2, "isa_samsungapps_icon_dummy"

    const-string v3, "drawable"

    invoke-static {p1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->addView(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public isLoaded()Z
    .locals 2

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract loadWidget()V
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 109
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {p2, p3, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getChildMeasureSpec(III)I

    move-result v1

    .line 110
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-static {p4, p5, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getChildMeasureSpec(III)I

    move-result v0

    .line 111
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 112
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getChildCount()I

    move-result v1

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getPaddingLeft()I

    move-result v2

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getPaddingTop()I

    move-result v3

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->c:Z

    .line 96
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 97
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 98
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    .line 99
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 100
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 101
    add-int/2addr v5, v2

    add-int/2addr v6, v3

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 96
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 117
    return-void
.end method

.method public onWidgetSetViewState()V
    .locals 2

    .prologue
    .line 348
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onWidgetSetViewState(IZ)V

    .line 349
    return-void
.end method

.method public onWidgetSetViewState(I)V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onWidgetSetViewState(IZ)V

    .line 345
    return-void
.end method

.method public onWidgetSetViewState(IZ)V
    .locals 7

    .prologue
    const v6, 0x7f0c0084

    const v5, 0x7f0c0080

    const v4, 0x7f0c007f

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 302
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    if-nez p1, :cond_2

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 311
    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 317
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    const v1, 0x7f0c007e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 322
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 324
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 314
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 327
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 330
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    const v1, 0x7f0c0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 332
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/m;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/m;-><init>(Lcom/sec/android/app/samsungapps/widget/CommonWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onWidgetViewState()V
    .locals 2

    .prologue
    .line 257
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onWidgetViewState(IZ)V

    .line 258
    return-void
.end method

.method public onWidgetViewState(I)V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onWidgetViewState(IZ)V

    .line 254
    return-void
.end method

.method public onWidgetViewState(IZ)V
    .locals 3

    .prologue
    const v1, 0x7f0c0077

    const/4 v2, 0x0

    .line 223
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 225
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 229
    :cond_0
    if-nez p1, :cond_1

    .line 230
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setVisibleGoneAll()V

    goto :goto_0

    .line 233
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 234
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238
    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 240
    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setVisibleLoading(I)Z

    goto :goto_0

    .line 236
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 243
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setVisibleNodata(I)Z

    goto :goto_0

    .line 246
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setVisibleRetry(I)Z

    goto :goto_0

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract refreshWidget()V
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->mContext:Landroid/content/Context;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 74
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    .line 76
    return-void
.end method

.method public setConfChangedCompleteListener(Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    .line 383
    return-void
.end method

.method public setEmptyRes(I)V
    .locals 1

    .prologue
    .line 270
    const v0, 0x7f0c0082

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 271
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 272
    return-void
.end method

.method public setEmptyText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 261
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 262
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 264
    if-eqz v0, :cond_0

    .line 265
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 267
    :cond_0
    return-void
.end method

.method public setEmptyTextSize(I)V
    .locals 3

    .prologue
    .line 275
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 276
    const/4 v1, 0x1

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 277
    return-void
.end method

.method public setEmptyView(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->b:Landroid/view/View;

    .line 289
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->d:Landroid/view/View;

    .line 290
    return-void
.end method

.method public setNeedLoad(Z)V
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 368
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 370
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->e:Z

    .line 371
    return-void
.end method

.method protected setVisibleGoneAll()V
    .locals 6

    .prologue
    const v5, 0x7f0c0084

    const v4, 0x7f0c0080

    const v3, 0x7f0c007f

    const v2, 0x7f0c007e

    const/16 v1, 0x8

    .line 199
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 204
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 207
    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 208
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 211
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 212
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 214
    :cond_3
    return-void
.end method

.method public setVisibleLoading(I)Z
    .locals 6

    .prologue
    const v5, 0x7f0c0084

    const v4, 0x7f0c0080

    const v3, 0x7f0c007f

    const v2, 0x7f0c007e

    const/16 v1, 0x8

    .line 125
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 134
    :cond_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 135
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 138
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 139
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 141
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public setVisibleNodata(I)Z
    .locals 6

    .prologue
    const v5, 0x7f0c0084

    const v4, 0x7f0c0080

    const v3, 0x7f0c007f

    const v2, 0x7f0c007e

    const/16 v1, 0x8

    .line 146
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 151
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    :cond_1
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 156
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 160
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public setVisibleRetry(I)Z
    .locals 6

    .prologue
    const v5, 0x7f0c0084

    const v4, 0x7f0c0080

    const v3, 0x7f0c007f

    const v2, 0x7f0c007e

    const/16 v1, 0x8

    .line 167
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 173
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 177
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 181
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 184
    :cond_3
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 185
    if-eqz v0, :cond_4

    .line 186
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/l;-><init>(Lcom/sec/android/app/samsungapps/widget/CommonWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    :cond_4
    const/4 v0, 0x0

    return v0
.end method

.method public abstract setWidgetData(Ljava/lang/Object;)V
.end method

.method public setWidgetState(I)V
    .locals 0

    .prologue
    .line 359
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->f:I

    .line 360
    return-void
.end method

.method public abstract updateWidget()V
.end method
