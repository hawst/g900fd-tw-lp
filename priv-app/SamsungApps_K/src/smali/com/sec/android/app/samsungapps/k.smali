.class final Lcom/sec/android/app/samsungapps/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInitializeFailed(Z)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->finish()V

    .line 130
    return-void
.end method

.method public final onInitializeSuccess()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->manageListButtons()V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/k;->a:Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->initialized()V

    .line 125
    return-void
.end method
