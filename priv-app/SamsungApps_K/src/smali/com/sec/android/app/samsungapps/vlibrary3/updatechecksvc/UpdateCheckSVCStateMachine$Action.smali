.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CALC_UPDATE_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field public static final enum CLEAR_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field public static final enum INIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field public static final enum LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field public static final enum REQ_GET_DOWNLOADLIST:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "INIT_CHECK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->INIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "LOGIN_CHECK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "REQ_GET_DOWNLOADLIST"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->REQ_GET_DOWNLOADLIST:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "CALC_UPDATE_COUNT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->CALC_UPDATE_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    const-string v1, "CLEAR_FAIL_CODE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->CLEAR_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    .line 21
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->INIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->REQ_GET_DOWNLOADLIST:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->CALC_UPDATE_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->CLEAR_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    return-object v0
.end method
