.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _VersionProvider:Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;

.field mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

.field mDevice:Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

.field mNetInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;

.field mSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mDevice:Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mNetInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    .line 16
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->_VersionProvider:Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;

    .line 17
    return-void
.end method


# virtual methods
.method public getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    return-object v0
.end method

.method public getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mDevice:Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    return-object v0
.end method

.method public getNetInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mNetInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;

    return-object v0
.end method

.method public getPlatformKeyVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->_VersionProvider:Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;->getPlatformSignedKeyVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    return-object v0
.end method

.method public selectCountry(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->clearUpdateField()V

    .line 48
    return-void
.end method

.method public setCurCountry(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    .line 42
    return-void
.end method
