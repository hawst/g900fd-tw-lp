.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

.field private _LoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

    .line 15
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_LoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->pay()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->onFinalResult(Z)V

    return-void
.end method

.method private pay()V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_LoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_LoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;->easyBuyCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 53
    return-void
.end method

.method private requestPin2()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;->getPin2Command()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 71
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->payment()V

    .line 20
    return-void
.end method

.method protected onRequestRegisterCard()V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;->registerIranCreditCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 89
    return-void
.end method

.method protected payment()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->_IIranPayment:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;->hasCreditCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->requestPin2()V

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;->onRequestRegisterCard()V

    goto :goto_0
.end method
