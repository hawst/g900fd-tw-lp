.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract askUserUpdateODC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;
.end method

.method public abstract odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestCheckAppUpgradeCommand(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract unaUpdateCommand(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract validateCheckAppUpgrade()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract validateDisclaimerCommand(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
