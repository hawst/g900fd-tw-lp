.class public Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;
.super Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;
.source "ProGuard"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;->a:I

    .line 15
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;->b:I

    .line 19
    return-void
.end method


# virtual methods
.method protected getPreferenceDefaultValue()I
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getSettingsProviderCreator(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    move-result-object v0

    .line 27
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;->createAutoUpdateNotification()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;->isOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;->a:I

    .line 36
    :goto_0
    return v0

    .line 33
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;->b:I

    goto :goto_0
.end method
