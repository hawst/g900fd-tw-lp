.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
.end method

.method public abstract getBuyPrice()D
.end method

.method public abstract getFinalMapContainer()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
.end method

.method public abstract getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
.end method
