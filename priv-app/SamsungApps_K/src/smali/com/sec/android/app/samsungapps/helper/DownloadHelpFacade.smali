.class public Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->a:Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->a:Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    return-object v0
.end method


# virtual methods
.method public createDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;
    .locals 3

    .prologue
    .line 19
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;-><init>()V

    new-instance v2, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;-><init>()V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    return-object v1
.end method

.method public createNoAccountBasedDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;
    .locals 3

    .prologue
    .line 29
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLoginFactory;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLoginFactory;-><init>()V

    new-instance v2, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;-><init>()V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactory;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactory;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    return-object v1
.end method

.method public createNoAccountBasedDownloadHelperFactoryForDemo(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;
    .locals 3

    .prologue
    .line 39
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLoginFactory;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLoginFactory;-><init>()V

    new-instance v2, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;-><init>()V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactoryForDemo;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactoryForDemo;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    return-object v1
.end method
