.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private _CouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

.field private _TempListCoupon:Ljava/util/ArrayList;

.field private _bEnd:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_CouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_TempListCoupon:Ljava/util/ArrayList;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_bEnd:Z

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_CouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    .line 15
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_TempListCoupon:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    return-void
.end method

.method public onEndParse()V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onEndParse()V

    .line 35
    monitor-enter p0

    .line 37
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_CouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->clear()V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_TempListCoupon:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    .line 40
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_CouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 42
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_bEnd:Z

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_CouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->setCompleted(Z)V

    .line 45
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 28
    const-string v0, "endOfList"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;->_bEnd:Z

    .line 31
    :cond_0
    return-void
.end method
