.class public Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;
.super Lcom/sec/android/app/samsungapps/ContentListActivity;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

.field private b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 55
    const v0, 0x7f0400e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->setMainView(I)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x3

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 64
    return-void

    .line 57
    :array_0
    .array-data 4
        0xa0005
        0xa0007
        0xa0008
    .end array-data
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/sec/android/app/samsungapps/in;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    .line 82
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getYesOrNoEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->handleYesOrNoEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 84
    :pswitch_1
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 69
    sget-object v0, Lcom/sec/android/app/samsungapps/in;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 72
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->a()V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 52
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 34
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    .line 35
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getContentList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->a()V

    .line 39
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->createSeriesList(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SeriesContentListViewActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 42
    return-void
.end method
