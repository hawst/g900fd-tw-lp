.class public Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;
.super Lcom/sec/android/app/samsungapps/view/CommonAdapter;
.source "ProGuard"


# instance fields
.field e:Landroid/content/Context;

.field f:Landroid/view/View$OnFocusChangeListener;

.field private g:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x63

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;-><init>(Landroid/content/Context;II)V

    .line 211
    new-instance v0, Lcom/sec/android/app/samsungapps/view/av;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/av;-><init>(Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->f:Landroid/view/View$OnFocusChangeListener;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->e:Landroid/content/Context;

    .line 34
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;)Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    return-object v0
.end method


# virtual methods
.method public cancelAll()V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v0

    .line 64
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 65
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 66
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 64
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method public checkUpdateCount()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 102
    :goto_0
    if-ge v2, v3, :cond_0

    .line 103
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 104
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 105
    add-int/lit8 v0, v1, 0x1

    .line 102
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 109
    :cond_0
    if-lez v1, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;->hideUpdateAll()V

    .line 114
    :goto_2
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;->showUpdateAll()V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 11

    .prologue
    .line 147
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->e:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->mListType:I

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    iget-object v10, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-object v3, p1

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    return-object v0
.end method

.method public deletingCount()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 73
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 75
    :goto_0
    if-ge v2, v3, :cond_0

    .line 76
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    add-int/lit8 v0, v1, 0x1

    .line 75
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 81
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 163
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 164
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v4

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->mAppMgr:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "1"

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getLoadType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 167
    :goto_0
    if-eqz v3, :cond_1

    .line 168
    const v5, 0x7f0c0056

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 169
    if-eqz v5, :cond_1

    .line 170
    if-eqz v0, :cond_3

    .line 171
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 183
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->f:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 184
    invoke-virtual {v3, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v6

    if-nez v6, :cond_4

    .line 186
    invoke-virtual {v5, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 187
    invoke-virtual {v5, v1}, Landroid/view/View;->setSelected(Z)V

    .line 208
    :cond_1
    :goto_2
    return-object v3

    :cond_2
    move v0, v1

    .line 165
    goto :goto_0

    .line 173
    :cond_3
    new-instance v6, Lcom/sec/android/app/samsungapps/view/au;

    invoke-direct {v6, p0, p1}, Lcom/sec/android/app/samsungapps/view/au;-><init>(Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_1

    .line 190
    :cond_4
    if-eqz v4, :cond_1

    .line 191
    if-eqz v0, :cond_5

    .line 192
    invoke-virtual {v3, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 193
    invoke-virtual {v5, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 194
    invoke-virtual {v5, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2

    .line 196
    :cond_5
    invoke-virtual {v5, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 197
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    if-nez v0, :cond_6

    .line 198
    invoke-virtual {v5, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2

    .line 200
    :cond_6
    invoke-virtual {v5, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2
.end method

.method public isAllCancellable()Z
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v0

    .line 50
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 51
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 52
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isCancellable(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 59
    :goto_1
    return v0

    .line 50
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 59
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isEnableSelected()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v3

    move v2, v1

    .line 119
    :goto_0
    if-ge v2, v3, :cond_1

    .line 120
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 127
    :goto_1
    return v0

    .line 119
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 127
    goto :goto_1
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    return v0
.end method

.method public onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->checkUpdateCount()V

    .line 133
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 134
    return-void
.end method

.method public onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->checkUpdateCount()V

    .line 139
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 140
    return-void
.end method

.method public updateAll()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v3

    move v1, v2

    .line 39
    :goto_0
    if-ge v1, v3, :cond_1

    .line 40
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 41
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->e:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->e:Landroid/content/Context;

    invoke-interface {v4, v5, v0, v2}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createUpdateCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    new-instance v4, Lcom/sec/android/app/samsungapps/view/at;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/view/at;-><init>(Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->execute()V

    .line 39
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45
    :cond_1
    return-void
.end method
