.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract dontOpenDetailPage()Z
.end method

.method public abstract getDownloadProgress()I
.end method

.method public abstract getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
.end method

.method public abstract getFakeModel()Ljava/lang/String;
.end method

.method public abstract getGUID()Ljava/lang/String;
.end method

.method public abstract getLoadType()Ljava/lang/String;
.end method

.method public abstract getNotificationID()I
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getProductName()Ljava/lang/String;
.end method

.method public abstract getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
.end method

.method public abstract isAD()Z
.end method

.method public abstract isCancellable()Z
.end method

.method public abstract isSPP()Z
.end method

.method public abstract setDownloadProgress(I)V
.end method
