.class final Lcom/sec/android/app/samsungapps/hx;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SearchResultActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 1189
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 1192
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1193
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    .line 1195
    if-eqz v0, :cond_2

    .line 1196
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 1197
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    .line 1199
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1200
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1201
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 1204
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_1

    .line 1207
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->isUserSearchHistory:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 1208
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->hstr:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    .line 1212
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->srchClickURL:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setSearchClickURL(Ljava/lang/String;)V

    .line 1215
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    .line 1219
    :cond_2
    return-void

    .line 1210
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hx;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->ac:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    goto :goto_0
.end method
