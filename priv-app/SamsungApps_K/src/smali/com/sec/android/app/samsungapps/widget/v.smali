.class final Lcom/sec/android/app/samsungapps/widget/v;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/v;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 381
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/v;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/v;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setFHAnimation(Landroid/content/Context;Landroid/view/View;Z)V

    .line 383
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/v;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->isHoverPopupPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/v;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHovered(Z)V

    .line 391
    :cond_0
    :goto_0
    return v2

    .line 387
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/v;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHovered(Z)V

    goto :goto_0
.end method
