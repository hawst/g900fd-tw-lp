.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;


# static fields
.field private static h:Landroid/os/Handler;


# instance fields
.field protected _ServiceConnectionManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

.field a:Ljava/lang/String;

.field b:Lcom/samsung/android/aidl/ICheckAppInstallState;

.field c:Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;

.field d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

.field private f:Landroid/content/Context;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 333
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->h:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    .line 329
    const-string v0, "installtestapp"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->g:Ljava/lang/String;

    .line 335
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/f;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->c:Lcom/samsung/android/aidl/ICheckAppInstallStateCallback;

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->f:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a:Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/a;

    const-string v1, "com.samsung.android.app.watchmanager.INSTALL_APP"

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->_ServiceConnectionManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V
    .locals 3

    .prologue
    .line 74
    :goto_0
    const-string v0, "BDeviceInstallManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exit: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    .line 76
    const-string v0, "BDeviceInstallManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "entry: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 77
    :cond_0
    :goto_1
    :pswitch_0
    return-void

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->_ServiceConnectionManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->checkServiceConnection(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->f()V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->f()V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->g()V

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->install()V

    goto :goto_1

    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->f()V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->g()V

    goto :goto_1

    :pswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->f()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;->onInstallSuccess()V

    goto :goto_1

    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->g()V

    sget-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;->onPrepareSuccess()V

    goto :goto_1

    :pswitch_9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->c()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method

.method static synthetic b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->b:Lcom/samsung/android/aidl/ICheckAppInstallState;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/aidl/ICheckAppInstallState;->cancelInstall(Ljava/lang/String;Lcom/samsung/android/aidl/ICancelInstallCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private d()V
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    .line 178
    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 244
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 255
    :goto_0
    :pswitch_0
    return-void

    .line 254
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e()V

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->g:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->g:Ljava/lang/String;

    const-string v1, "releaseService() unbound."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->_ServiceConnectionManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->release()Z

    .line 361
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;->onInstallFailed()V

    .line 386
    :cond_0
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 311
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 320
    :goto_0
    return-void

    .line 314
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 317
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 311
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public execute()V
    .locals 2

    .prologue
    .line 61
    const-string v0, "BDeviceInstallManager"

    const-string v1, "execute"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 71
    :goto_0
    :pswitch_0
    return-void

    .line 68
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected install()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->_ServiceConnectionManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->checkServiceConnection(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;)V

    .line 308
    return-void
.end method

.method protected onInstallSuccess()V
    .locals 2

    .prologue
    .line 364
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 373
    :goto_0
    return-void

    .line 367
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 370
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 364
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public prepare()V
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 56
    :goto_0
    return-void

    .line 53
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;

    .line 45
    return-void
.end method

.method public userCancel()V
    .locals 2

    .prologue
    .line 263
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 288
    :goto_0
    :pswitch_0
    return-void

    .line 272
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/i;)V

    goto :goto_0

    .line 285
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->c()V

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
