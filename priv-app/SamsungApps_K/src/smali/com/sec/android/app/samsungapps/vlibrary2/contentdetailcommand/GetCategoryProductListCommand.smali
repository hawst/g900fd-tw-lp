.class public Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field protected _IGetCategoryProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;

.field c:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->_IGetCategoryProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->_IGetCategoryProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->onFinalResult(Z)V

    .line 54
    :goto_0
    return-void

    .line 31
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->_IGetCategoryProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCategoryID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->_IGetCategoryProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCategoryName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;)V

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestDataGet(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    goto :goto_0
.end method
