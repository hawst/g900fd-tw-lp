.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;
.super Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;
.source "ProGuard"


# instance fields
.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;)V

    .line 16
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 17
    return-void
.end method


# virtual methods
.method protected notifyAlreadyPurchased()V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/u;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/u;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetailAndAskAlreadyPurchased;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->showAlreadyPurchasedContent(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    .line 34
    return-void
.end method
