.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

.field private mPurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

.field xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mContext:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mPurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->request()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->onFinalResult(Z)V

    return-void
.end method

.method private removeInstalledItem()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 93
    :cond_0
    return-void

    .line 84
    :cond_1
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 85
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 88
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->remove(I)Ljava/lang/Object;

    .line 85
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private request()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 42
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->onFinalResult(Z)V

    .line 75
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getOrderID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;->purchaseHistHide(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->purchaseHistHide(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 73
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mPurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getDeleteItemList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->mDeleteList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->removeInstalledItem()V

    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->request()V

    .line 36
    return-void
.end method
