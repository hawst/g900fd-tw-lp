.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract addPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract killView()V
.end method

.method public abstract onCouponGetCompleted(Z)V
.end method

.method public abstract onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract onPurchaseCompleted(Z)V
.end method

.method public abstract onPurchaseMethodSelected(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
.end method

.method public abstract onRequestCouponList()V
.end method

.method public abstract updateView()V
.end method
