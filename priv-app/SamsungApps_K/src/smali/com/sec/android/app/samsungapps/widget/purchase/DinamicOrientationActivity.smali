.class public abstract Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;


# instance fields
.field protected scrollY:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected changeHeaderTailView()V
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getTailView()Landroid/view/View;

    move-result-object v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getaddingResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getremovingResourceId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 38
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getFocus()V

    .line 40
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 41
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 42
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->requestFocus()V

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getHeaderView()Landroid/view/View;

    move-result-object v2

    .line 48
    if-eqz v2, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getaddingResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getremovingResourceId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 52
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 54
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 55
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 58
    :cond_1
    return-void
.end method

.method public configurationChangedComplete()V
    .locals 3

    .prologue
    .line 84
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->scrollY:I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/purchase/a;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/a;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 85
    :cond_0
    return-void
.end method

.method protected abstract getFocus()V
.end method

.method protected abstract getHeaderView()Landroid/view/View;
.end method

.method protected abstract getScrollView()Landroid/widget/ScrollView;
.end method

.method protected abstract getTailView()Landroid/view/View;
.end method

.method protected abstract getaddingResourceId()I
.end method

.method protected abstract getremovingResourceId()I
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->scrollY:I

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->changeHeaderTailView()V

    .line 30
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract requestFocus()V
.end method
