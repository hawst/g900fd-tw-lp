.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;


# instance fields
.field private _WishListArray:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

.field private _iCountPerPage:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->_iCountPerPage:I

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->_iCountPerPage:I

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->_WishListArray:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    .line 16
    return-void
.end method


# virtual methods
.method public cancelDeletion()V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->_WishListArray:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 42
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 43
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method

.method protected abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public deleteSelectedItemCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method

.method public getCountPerPage()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->_iCountPerPage:I

    return v0
.end method

.method public getWishListArray()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishListCommandBuilder;->_WishListArray:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    return-object v0
.end method

.method public requestListCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RequestWishListCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RequestWishListCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;)V

    return-object v0
.end method
