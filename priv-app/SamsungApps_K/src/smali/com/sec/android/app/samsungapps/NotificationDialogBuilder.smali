.class public Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;
.super Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
.source "ProGuard"


# instance fields
.field protected _Context:Landroid/content/Context;

.field protected helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field protected mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->populateUI()V

    .line 34
    return-void
.end method


# virtual methods
.method protected onClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/app/samsungapps/fn;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/fn;-><init>(Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;)V

    return-object v0
.end method

.method protected onOK()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c01ce

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getCheckBoxValue(I)Z

    move-result v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->setDontDisplayAgain()V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v0

    .line 124
    if-ne v0, v4, :cond_2

    .line 126
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 127
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    .line 191
    :cond_1
    :goto_0
    return-void

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->yesokEvent:Ljava/lang/String;

    .line 143
    const-string v1, "01"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v4, :cond_1

    .line 149
    const-string v1, "02"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v4, :cond_1

    .line 155
    const-string v1, "03"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v4, :cond_3

    .line 157
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v1, :cond_3

    .line 159
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 165
    :cond_3
    const-string v1, "04"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v4, :cond_4

    .line 167
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 168
    const-string v2, "productID"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->productSetId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 169
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 171
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showContentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 175
    :cond_4
    const-string v1, "05"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 180
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->productSetId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showBannerContentList(Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_5
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 185
    const-string v1, "productID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->productSetId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 188
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showContentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    goto :goto_0
.end method

.method public populateUI()V
    .locals 7

    .prologue
    const v3, 0x7f0c01ce

    const v6, 0x7f0802ef

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 40
    const v1, 0x7f040081

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 46
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->onClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 50
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v2, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v2, v4

    float-to-int v2, v2

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 56
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->eventType:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/fk;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/fk;-><init>(Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 92
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationHeaderVal:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 94
    const v0, 0x7f0c01cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationVal:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionImgUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionImgUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    const v0, 0x7f0c01cc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionImgUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 102
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    .line 104
    :cond_2
    return-void

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->eventType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/fl;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/fl;-><init>(Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/fm;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/fm;-><init>(Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    goto/16 :goto_0
.end method
