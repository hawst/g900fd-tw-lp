.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;


# instance fields
.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field info:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 12
    return-void
.end method

.method private getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getChinaPrepaidCardInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCardOperatorCount()I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getCount()I

    move-result v0

    return v0
.end method

.method public getCardOperatorName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardOperatorSeq(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getSeqNumberString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardOperatorTNCURL(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardPaymentMethodID(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getPaymentMethodID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
