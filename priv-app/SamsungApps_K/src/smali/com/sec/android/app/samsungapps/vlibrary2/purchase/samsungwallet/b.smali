.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCouponSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    .line 64
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getGiftCardCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public final isMobileWallet()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public final isPhoneBill()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public final isUPoint()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method
