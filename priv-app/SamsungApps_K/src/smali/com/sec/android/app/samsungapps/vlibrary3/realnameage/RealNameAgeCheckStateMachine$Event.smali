.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum AGE_CHECK_IS_ALREADY_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum AGE_CHECK_IS_NOT_NEEDED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum AGE_CHECK_IS_REQUIRED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum AGE_RESTRICTED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum CHECK_CONTENT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum CONFRIM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field public static final enum USER_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "AGE_CHECK_IS_ALREADY_DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_ALREADY_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "AGE_CHECK_IS_REQUIRED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_REQUIRED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "AGE_CHECK_IS_NOT_NEEDED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_NOT_NEEDED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "CONFRIM_DONE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CONFRIM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "CONFIRM_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "CHECK_CONTENT_OK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CHECK_CONTENT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "AGE_RESTRICTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_RESTRICTED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    const-string v1, "USER_OK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->USER_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    .line 9
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_ALREADY_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_REQUIRED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_NOT_NEEDED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CONFRIM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CHECK_CONTENT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_RESTRICTED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->USER_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    return-object v0
.end method
