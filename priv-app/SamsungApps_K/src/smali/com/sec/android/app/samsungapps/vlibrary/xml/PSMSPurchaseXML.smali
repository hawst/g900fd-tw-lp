.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 16
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 18
    return-void
.end method

.method public static confirm(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;ZLcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;
    .locals 4

    .prologue
    .line 72
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    const-string v0, "ConfirmSMSPurchaseNS"

    const-string v2, "3030"

    invoke-direct {v1, p2, v0, v2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 73
    const-string v0, "orderID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->orderID:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v0, "paymentID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->paymentID:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 76
    const-string v2, "lastReqYn"

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-object v1

    .line 76
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public static confirm(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;
    .locals 4

    .prologue
    .line 82
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    const-string v0, "ConfirmSMSPurchaseNS"

    const-string v2, "3030"

    invoke-direct {v1, p1, v0, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 83
    const-string v0, "orderID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;->getOrderID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "paymentID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;->getPaymentID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 86
    const-string v2, "lastReqYn"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;->isLastReq()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-object v1

    .line 86
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public static init(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;
    .locals 7

    .prologue
    .line 39
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    const-string v0, "InitiatePSMSPurchaseNS"

    const-string v1, "3020"

    invoke-direct {v2, p1, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v3

    .line 41
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v5

    .line 44
    const-string v0, ""

    .line 45
    if-eqz v3, :cond_0

    .line 47
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v0

    .line 50
    :cond_0
    const-string v1, ""

    .line 51
    if-eqz v3, :cond_1

    .line 53
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMSI()Ljava/lang/String;

    move-result-object v1

    .line 56
    :cond_1
    const-string v3, "productID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v3, "imei"

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 58
    const-string v3, "guid"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v3, "mcc"

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v3, "mnc"

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v3, "reserved01"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v0, "reserved02"

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v0, "reserved03"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v0, "reserved04"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v0, "reserved05"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "rentalTerm"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-object v2
.end method

.method public static modelivery(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;ZLcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;
    .locals 4

    .prologue
    .line 92
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    const-string v0, "MODeliveryResult"

    const-string v2, "3021"

    invoke-direct {v1, p2, v0, v2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    const-string v0, "paymentID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->paymentID:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 94
    const-string v2, "result"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-object v1

    .line 94
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public static modelivery(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;
    .locals 4

    .prologue
    .line 100
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    const-string v0, "MODeliveryResult"

    const-string v2, "3021"

    invoke-direct {v1, p1, v0, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    const-string v0, "paymentID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;->getPaymentID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 102
    const-string v2, "result"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;->getSendSMSSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-object v1

    .line 102
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method
