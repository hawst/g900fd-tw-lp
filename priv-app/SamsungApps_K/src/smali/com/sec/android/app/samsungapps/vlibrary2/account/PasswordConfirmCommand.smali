.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/IPasswordConfirm;


# instance fields
.field private _IPasswordConfirmCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;

.field private _View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_IPasswordConfirmCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;

    .line 17
    return-void
.end method

.method private invokeUI(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_IPasswordConfirmCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;->getPasswordConfirmViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 27
    return-void
.end method


# virtual methods
.method public cancelConfirm()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->onFinalResult(Z)V

    .line 60
    return-void
.end method

.method public confirmPassword(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->confirmPassword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_IPasswordConfirmCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;->setPasswordConfirmedTime()V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;->onPasswordConfirmed(Z)V

    .line 48
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->onFinalResult(Z)V

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;->onPasswordConfirmed(Z)V

    goto :goto_0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->invokeUI(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    .line 33
    return-void
.end method

.method protected onFinalResult(Z)V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onFinalResult(Z)V

    .line 39
    return-void
.end method
