.class public Lcom/sec/android/app/samsungapps/commands/NotiCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->a:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->b:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->c:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->d:Ljava/lang/String;

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/NotiCommand;)V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/NotiCommand;)V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->a:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 41
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->b:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/bl;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/bl;-><init>(Lcom/sec/android/app/samsungapps/commands/NotiCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->c:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/bm;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/bm;-><init>(Lcom/sec/android/app/samsungapps/commands/NotiCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 58
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 59
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    goto :goto_0
.end method
