.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getBillingServerType()Ljava/lang/String;
.end method

.method public abstract getCSC()Ljava/lang/String;
.end method

.method public abstract getDisclaimerVersion()Ljava/lang/String;
.end method

.method public abstract getHeaderHost()Ljava/lang/String;
.end method

.method public abstract getHubHost()Ljava/lang/String;
.end method

.method public abstract getHubUrl()Ljava/lang/String;
.end method

.method public abstract getIMEI()Ljava/lang/String;
.end method

.method public abstract getInfinityVersion()Ljava/lang/String;
.end method

.method public abstract getKnox2HomeType()Ljava/lang/String;
.end method

.method public abstract getMCC()Ljava/lang/String;
.end method

.method public abstract getMNC()Ljava/lang/String;
.end method

.method public abstract getModelName()Ljava/lang/String;
.end method

.method public abstract getODCVersion()Ljava/lang/String;
.end method

.method public abstract getODCVersionCode()Ljava/lang/String;
.end method

.method public abstract getOpenAPIVersion()Ljava/lang/String;
.end method

.method public abstract getPlatformKey()Ljava/lang/String;
.end method

.method public abstract getSizeLimitation()Ljava/lang/String;
.end method

.method public abstract getStageDataHostURL()Ljava/lang/String;
.end method

.method public abstract getStagingAppHostUrl()Ljava/lang/String;
.end method

.method public abstract getStagingImgHostUrl()Ljava/lang/String;
.end method

.method public abstract getUpdateInterval()I
.end method

.method public abstract isExistSaconfig()Z
.end method

.method public abstract isKnox2Mode()Ljava/lang/String;
.end method

.method public abstract isPSMSMoNeededSkipping()Z
.end method

.method public abstract isTestPurchaseSupported()Z
.end method

.method public abstract isUsingAPKVersionBilling()Z
.end method

.method public abstract isUsingStageURL()Z
.end method
