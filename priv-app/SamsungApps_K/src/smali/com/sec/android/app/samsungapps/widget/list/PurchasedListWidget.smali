.class public Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;
.super Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;


# instance fields
.field public allRemoved:Z

.field b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

.field private f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

.field private g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

.field private h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

.field private i:I

.field private j:I

.field private k:Landroid/view/View;

.field private l:I

.field private m:Z

.field private final n:Ljava/lang/String;

.field public refreshList:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    .line 46
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->i:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->j:I

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    .line 54
    const-string v0, "PurchasedListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->n:Ljava/lang/String;

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->a(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    .line 46
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->i:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->j:I

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    .line 54
    const-string v0, "PurchasedListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->n:Ljava/lang/String;

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->a(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    .line 46
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->i:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->j:I

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    .line 54
    const-string v0, "PurchasedListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->n:Ljava/lang/String;

    .line 69
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->a(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    return p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;)Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    .line 547
    if-eqz v0, :cond_0

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 549
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 551
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isAllDeselected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 552
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_0

    .line 554
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    .line 76
    const v0, 0x7f040012

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->initView(Landroid/content/Context;I)V

    .line 77
    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->mListView:Landroid/widget/AbsListView;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFocusableInTouchMode(Z)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->mListView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/ag;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/ag;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 95
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 97
    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 98
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/ah;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/ah;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method


# virtual methods
.method public bridge synthetic getAdapter()Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    return-object v0
.end method

.method public getContentList()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->m:Z

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 354
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v3, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->m:Z

    goto :goto_0

    .line 360
    :cond_1
    return-object v1
.end method

.method public getCountNotInstalledItem()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 382
    :cond_1
    return v2

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 377
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, v0

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v1, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 379
    add-int/lit8 v2, v2, 0x1

    move v0, v2

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v1, v3

    .line 377
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public getSelectedCount()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 505
    .line 506
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-nez v1, :cond_0

    .line 514
    :goto_0
    return v0

    .line 509
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 510
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 511
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 514
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public getUpdatableList()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 364
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getUpdatableItemList(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 366
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 368
    :cond_0
    return-object v1
.end method

.method public hideUpdateAll()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-nez v0, :cond_1

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getUpdatableListCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const v1, 0xa0012

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionBar(I)V

    goto :goto_0

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const v1, 0xa0011

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionBar(I)V

    goto :goto_0
.end method

.method public isShowDeleteIcon()Z
    .locals 1

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getContentList()Ljava/util/ArrayList;

    .line 586
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->m:Z

    return v0
.end method

.method public itemClick(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isEmpty()Z

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-nez v0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 439
    const v1, 0x7f0c0293

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 440
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getLoadType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v4, :cond_4

    .line 442
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->deSelectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_0

    .line 446
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v2

    .line 448
    if-eqz v2, :cond_6

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->deSelectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    .line 450
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 456
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    .line 458
    if-eqz v1, :cond_5

    .line 459
    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 462
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->a()V

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getSelectedCount()I

    move-result v0

    .line 465
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setBeforeSelectedItem(Landroid/view/View;I)V

    .line 466
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->updatePopupButtonText(II)V

    goto :goto_0

    .line 452
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    .line 453
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1

    .line 469
    :cond_7
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v2, 0x7

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 471
    :catch_0
    move-exception v0

    const-string v0, "IndexOutOfBoundsExceptionPurchasedListWidget"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public itemLongClick(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 484
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 502
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, "1"

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getLoadType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 490
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->deSelectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_0

    .line 493
    :cond_4
    const/16 v2, 0x14

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setDeletionMode(I)V

    .line 494
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->displayDeleteScreen()V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getSelectedCount()I

    move-result v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->updatePopupButtonText(II)V

    .line 497
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    .line 498
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    .line 499
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->a()V

    goto :goto_0
.end method

.method public loadMoreComplete()V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-eqz v1, :cond_0

    .line 270
    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->isSelectAll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->selectAll(Landroid/content/Context;)Z

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->notifyDataSetChanged()V

    .line 277
    :cond_0
    return-void
.end method

.method public loadWidget()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->requestMore()V

    .line 166
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->loadWidget()V

    .line 167
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->start()V

    goto :goto_0
.end method

.method public loadingMore()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onDataLoadingMore()V

    .line 284
    :cond_0
    return-void
.end method

.method public refreshActionBar(Z)V
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-eqz v0, :cond_0

    .line 593
    if-eqz p1, :cond_1

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->refreshActionBar(Z)V

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onDataLoadCompleted()V

    goto :goto_0
.end method

.method public refreshArrayAdapter()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->notifyDataSetChanged()V

    .line 294
    :cond_0
    return-void
.end method

.method public refreshBeforeSelectedItem()V
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    if-ltz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 566
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-nez v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    if-eqz v0, :cond_4

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->clear()V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getContentList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getUpdatableList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setListEOF(Z)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getUpdatableListCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080208

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 245
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->checkUpdateCount()V

    .line 252
    :goto_2
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    goto :goto_0

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->updatesListViewEOF()V

    .line 243
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    goto :goto_1

    .line 247
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->updateWidget()V

    goto :goto_2

    .line 250
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->notifyDataSetChanged()V

    goto :goto_2
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->clear()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->release()V

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->release()V

    .line 147
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 148
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 149
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 150
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 152
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    .line 153
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    .line 154
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->release()V

    .line 155
    return-void
.end method

.method public requestList()V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->start()V

    .line 393
    return-void
.end method

.method public requestListMore()V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->requestMore()V

    .line 388
    return-void
.end method

.method public saveOldY(I)V
    .locals 2

    .prologue
    .line 257
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->saveOldY(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 260
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->j:I

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->i:I

    goto :goto_0
.end method

.method public setBeforeSelectedItem(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->k:Landroid/view/View;

    .line 577
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->l:I

    .line 578
    return-void
.end method

.method public setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 113
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 300
    sget-object v1, Lcom/sec/android/app/samsungapps/widget/list/ai;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 302
    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 306
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getScrollState()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->loadMoreComplete()V

    .line 309
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetMoreLoading(Z)V

    .line 312
    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->setTopButtonLayoutVisible(Z)V

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->updateWidget()V

    goto :goto_0

    .line 320
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->loadingMore()V

    .line 321
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetMoreLoading(Z)V

    goto :goto_0

    .line 325
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getScrollState()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 326
    const v0, 0x7f0c007b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 327
    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onDataLoadCompleted()V

    goto :goto_0

    .line 330
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 335
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->clear()V

    .line 336
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    .line 337
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v1, v5}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->setPurchasedMode(I)V

    .line 338
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0

    .line 342
    :pswitch_5
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->updateWidget()V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0, v5}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->setPurchasedMode(I)V

    goto :goto_0

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 117
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->createPurchasedList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setListRequest(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/CommonAdapter;)V

    .line 121
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 125
    check-cast p2, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 127
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->createPurchasedList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setListRequest(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/CommonAdapter;)V

    .line 131
    return-void
.end method

.method public showUpdateAll()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-nez v0, :cond_1

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getUpdatableListCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const v1, 0xa0012

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionBar(I)V

    goto :goto_0

    .line 406
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const v1, 0xa000f

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionBar(I)V

    goto :goto_0
.end method

.method public updateList(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-nez v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshList:Z

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 529
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->clear()V

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getContentList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getUpdatableList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setListEOF(Z)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getUpdatableListCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 535
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080208

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 536
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->updatesListViewEOF()V

    .line 539
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    goto :goto_0
.end method

.method public updateWidget()V
    .locals 6

    .prologue
    const v5, 0x7f0801c7

    const v4, 0x7f0801c6

    const/4 v3, 0x2

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-nez v0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->clear()V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getContentList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getUpdatableList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->setListEOF(Z)V

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PurchasedListWidget :: size() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PurchasedListWidget :: isEOF() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 183
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyTextSize(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->updatesListViewEOF()V

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getUpdatableList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 187
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    if-gtz v0, :cond_4

    .line 190
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    if-eqz v0, :cond_3

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 199
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->checkUpdateCount()V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->mListView:Landroid/widget/AbsListView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 219
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onDataLoadCompleted()V

    goto/16 :goto_0

    .line 193
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    goto :goto_1

    .line 196
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080208

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    goto :goto_1

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-nez v0, :cond_6

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const v1, 0xa0010

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionBar(I)V

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->f:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->updateViewEOF()V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    if-gtz v0, :cond_7

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const v1, 0xa0012

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionBar(I)V

    .line 209
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 210
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->allRemoved:Z

    if-eqz v0, :cond_8

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 216
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->mListView:Landroid/widget/AbsListView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_2

    .line 213
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setEmptyText(Ljava/lang/String;)V

    goto :goto_3
.end method
