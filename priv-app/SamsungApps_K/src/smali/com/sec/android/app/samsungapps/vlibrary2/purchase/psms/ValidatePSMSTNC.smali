.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

.field private _View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method


# virtual methods
.method protected existTNC()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->existTNC()Z

    move-result v0

    return v0
.end method

.method public getFormattedSellingPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getFormattedSellingPrice()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTNCMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getTNCDisplayMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->existTNC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getTNCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->onFinalResult(Z)V

    goto :goto_0
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;

    .line 39
    return-void
.end method

.method protected onSendTNCSMSResult(Z)V
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->onFinalResult(Z)V

    .line 72
    if-nez p1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getNotiTNCSendFailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_Context:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->Nothing:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 76
    :cond_0
    return-void
.end method

.method public onTNCAgreedByUser(Z)V
    .locals 0

    .prologue
    .line 43
    if-nez p1, :cond_0

    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->onFinalResult(Z)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->sendTNCSMS()V

    goto :goto_0
.end method

.method protected sendTNCSMS()V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getTNCSMSNo()Ljava/lang/String;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->getTNCSMSMessage()Ljava/lang/String;

    move-result-object v1

    .line 58
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->_IValidatePSMSNTNCData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;->createSMSSender()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/q;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/q;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;)V

    invoke-interface {v2, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;->sendSMS(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 67
    return-void
.end method
