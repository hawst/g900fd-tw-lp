.class public Lcom/sec/android/app/samsungapps/tobelog/Preferences;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final PREF_LOG_TRAFFIC:Ljava/lang/String; = "log_traffic"

.field public static final PREF_REGISTERED:Ljava/lang/String; = "registered"

.field public static final PREF_TRAFFIC_UPDATE_TIME:Ljava/lang/String; = "traffic_update_time"


# instance fields
.field private a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "log_pref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    .line 19
    return-void
.end method


# virtual methods
.method protected getBoolean(Ljava/lang/String;Z)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getInt(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected getLong(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getSharedPref()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method protected getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected putBoolean(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 24
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 25
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 27
    return-void
.end method

.method protected putInt(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 66
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 67
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 69
    return-void
.end method

.method protected putLong(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 38
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 39
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 41
    return-void
.end method

.method protected putString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 52
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 53
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 55
    return-void
.end method
