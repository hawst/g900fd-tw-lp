.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

.field _CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

.field _ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

.field protected _ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

.field _bCouponResult:Z

.field _bExpiredGiftCardResult:Z

.field _bGiftCardResult:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 20
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 22
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bCouponResult:Z

    .line 23
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bGiftCardResult:Z

    .line 24
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bExpiredGiftCardResult:Z

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    .line 29
    return-void
.end method

.method private checkResult()V
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->iscompleteReceive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;->complete()V

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->onFinalResult(Z)V

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;->clear()V

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->requestActiveGiftCardList()V

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->requestCouponList()V

    .line 37
    return-void
.end method

.method protected iscompleteReceive()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bCouponResult:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bGiftCardResult:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onReceiveActiveGiftCardList(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 74
    if-eqz p1, :cond_1

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bGiftCardResult:Z

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;->add(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V

    goto :goto_0

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->checkResult()V

    .line 88
    :goto_1
    return-void

    .line 85
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bGiftCardResult:Z

    .line 86
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->onFinalResult(Z)V

    goto :goto_1
.end method

.method protected onReceiveCouponList(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-eqz p1, :cond_1

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bCouponResult:Z

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;->add(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->checkResult()V

    .line 69
    :goto_1
    return-void

    .line 66
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bCouponResult:Z

    .line 67
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->onFinalResult(Z)V

    goto :goto_1
.end method

.method protected onReceiveExpiredGiftCardList(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 92
    if-eqz p1, :cond_1

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bExpiredGiftCardResult:Z

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;->add(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V

    goto :goto_0

    .line 99
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->checkResult()V

    .line 106
    :goto_1
    return-void

    .line 103
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_bExpiredGiftCardResult:Z

    .line 104
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->onFinalResult(Z)V

    goto :goto_1
.end method

.method protected requestActiveGiftCardList()V
    .locals 4

    .prologue
    .line 110
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const-string v1, "A"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/c;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->giftCardList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 117
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 118
    return-void
.end method

.method protected requestCouponList()V
    .locals 3

    .prologue
    .line 134
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->customerCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 141
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 142
    return-void
.end method

.method protected requestExpiredGiftCardList()V
    .locals 4

    .prologue
    .line 122
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const-string v1, "E"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;->_ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/d;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->giftCardList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 129
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 130
    return-void
.end method
