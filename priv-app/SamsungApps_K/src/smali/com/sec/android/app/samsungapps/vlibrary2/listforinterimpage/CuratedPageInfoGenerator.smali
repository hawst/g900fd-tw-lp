.class public Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

.field mMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    .line 14
    return-void
.end method

.method private isValid(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 28
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    const-string v0, "listTitle"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listTitle:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listTitle:Ljava/lang/String;

    .line 55
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 56
    return-void

    .line 37
    :cond_1
    const-string v0, "listDescription"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listDescription:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listDescription:Ljava/lang/String;

    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "listBGImgUrl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGImgUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGImgUrl:Ljava/lang/String;

    goto :goto_0

    .line 45
    :cond_3
    const-string v0, "listContentSetID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listContentSetID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listContentSetID:Ljava/lang/String;

    goto :goto_0

    .line 49
    :cond_4
    const-string v0, "listBGLandscapeImgUrl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGLandscapeImgUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iput-object p2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGLandscapeImgUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->clearData()V

    .line 19
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mCuratedPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;->mMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 24
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method
