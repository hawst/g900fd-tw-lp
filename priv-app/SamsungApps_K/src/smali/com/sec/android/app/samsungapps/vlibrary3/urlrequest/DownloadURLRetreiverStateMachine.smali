.class public Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 21
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;

    .line 35
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 99
    const-string v0, "URLRequestorStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/l;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    :pswitch_0
    return-void

    .line 104
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->NOTIFY_NEED_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 105
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 108
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->REQ_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->REQ_EASYBUY:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->REQ_DOWNLOADEX:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 117
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 118
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 41
    const-string v0, "URLRequestorStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/l;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 94
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 44
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/l;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 47
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->REQ_FREEAPP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->REQ_PREPOSTAPP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->REQ_BOUGHT_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->NEED_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/l;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 64
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/l;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 75
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/l;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 86
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_b
    .end packed-switch

    .line 44
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 61
    :pswitch_data_2
    .packed-switch 0x5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 72
    :pswitch_data_3
    .packed-switch 0x5
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 83
    :pswitch_data_4
    .packed-switch 0x5
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 129
    const-string v0, "URLRequestorStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 130
    return-void
.end method
