.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;


# instance fields
.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->mName:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->mId:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public getReqID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getReqName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public setFakeModel(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public setGearOSVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public toMap()Ljava/util/HashMap;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 25
    const-string v1, "name"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const-string v1, "transactionId"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-object v0
.end method
