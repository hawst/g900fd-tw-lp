.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field bValid:Z

.field private mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

.field mAge:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->bValid:Z

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 15
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->mAge:I

    .line 16
    return-void
.end method


# virtual methods
.method public getAge()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->mAge:I

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->bValid:Z

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 56
    return-void
.end method

.method public requestNameAuth(Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ae;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ae;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->nameAuthSetting(Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 51
    return-void
.end method

.method public setAge(I)V
    .locals 1

    .prologue
    .line 20
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->bValid:Z

    .line 25
    :cond_0
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->mAge:I

    .line 26
    return-void
.end method
