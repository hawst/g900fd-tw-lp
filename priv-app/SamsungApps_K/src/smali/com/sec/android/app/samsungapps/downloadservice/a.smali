.class final Lcom/sec/android/app/samsungapps/downloadservice/a;
.super Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadService$Stub;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final downloadByPackageName(Ljava/lang/String;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadservice/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/samsungapps/downloadservice/c;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/a;Ljava/lang/String;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    goto :goto_0
.end method

.method public final downloadByProductId(Ljava/lang/String;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadservice/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/samsungapps/downloadservice/b;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/a;Ljava/lang/String;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/a;->a:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    goto :goto_0
.end method
