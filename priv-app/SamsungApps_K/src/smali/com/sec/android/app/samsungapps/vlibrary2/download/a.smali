.class final Lcom/sec/android/app/samsungapps/vlibrary2/download/a;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 37
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 47
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 48
    return-void

    .line 40
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    int-to-long v2, v0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterListener:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_TotalSize:J
    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;->notifyProgress(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;JJ)V

    goto :goto_0

    .line 44
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterListener:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;
    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v0, :cond_0

    :goto_1
    invoke-interface {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;->notifyProgressCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
