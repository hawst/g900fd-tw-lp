.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;


# instance fields
.field private cmdBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

.field private mCheckAppUpgradeObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;

.field private network:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;->cmdBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;->network:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;

    .line 17
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;->mCheckAppUpgradeObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;

    .line 18
    return-void
.end method


# virtual methods
.method public createInitializer(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;->cmdBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;->network:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;->mCheckAppUpgradeObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;)V

    .line 23
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)V

    .line 24
    return-object v0
.end method
