.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum CHECK_WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum CLOSE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum CREATE_FILE_WRITER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum CREATE_NORAML_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum CREATE_RESUME_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum ENQUEUE_IN_WAIT_NETWORACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum HTTP_ABORT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum INC_RETRY_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum NOTIFY_3G_CONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum NOTIFY_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum NOTIFY_SIG_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum REMEMBER_NETCONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum SET_CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field public static final enum STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CREATE_FILE_WRITER"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_FILE_WRITER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "NOTIFY_COMPLETE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CREATE_NORAML_HTTPGET"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_NORAML_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CREATE_RESUME_HTTPGET"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_RESUME_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "SEND_REQUEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CLOSE_FILE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CLOSE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "DELETE_FILE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "DOWNLOADING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "SET_CANCELING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SET_CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "NOTIFY_SIG_CANCELED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_SIG_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CLEAR_RETRYCOUNT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CHECK_RETRY_CONDITION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CHECK_RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "START_TIMER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "STOP_TIMER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "INC_RETRY_COUNT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->INC_RETRY_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "REQUEST_URL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "ENQUEUE_IN_WAIT_NETWORACTIVATE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->ENQUEUE_IN_WAIT_NETWORACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "CHECK_WIFI_TO_3G_HANDOVER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CHECK_WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "REMEMBER_NETCONNECTION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->REMEMBER_NETCONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "NOTIFY_3G_CONNECTION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_3G_CONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    const-string v1, "HTTP_ABORT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->HTTP_ABORT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    .line 16
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_FILE_WRITER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_NORAML_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_RESUME_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CLOSE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SET_CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_SIG_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CHECK_RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->INC_RETRY_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->ENQUEUE_IN_WAIT_NETWORACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CHECK_WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->REMEMBER_NETCONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_3G_CONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->HTTP_ABORT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    return-object v0
.end method
