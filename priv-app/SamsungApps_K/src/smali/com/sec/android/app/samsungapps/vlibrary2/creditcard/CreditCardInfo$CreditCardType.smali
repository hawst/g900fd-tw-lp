.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum AmericanExpress:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum Discover:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum Etc:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum Invalid:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum JCB:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum MasterCard:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

.field public static final enum VISA:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "Invalid"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Invalid:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "VISA"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->VISA:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "MasterCard"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->MasterCard:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "AmericanExpress"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->AmericanExpress:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "Discover"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Discover:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "JCB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->JCB:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    const-string v1, "Etc"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Etc:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 28
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Invalid:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->VISA:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->MasterCard:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->AmericanExpress:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Discover:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->JCB:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Etc:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    return-object v0
.end method
