.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 9
    const-string v2, "creditCardRegister"

    const-string v3, "5090"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 13
    const-string v0, "cardType"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->cardType:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 14
    const-string v0, "cardNum"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->cardNum:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 15
    const-string v0, "expirationYear"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->expirationYear:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 16
    const-string v0, "expirationMonth"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->expirationMonth:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 17
    const-string v0, "cvs"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->cvs:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 18
    const-string v0, "authNID"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->authNID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 19
    const-string v0, "authPIN"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;->authPIN:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 23
    const-string v2, "creditCardRegister"

    const-string v3, "5090"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 25
    const-string v0, "cardType"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getCardType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 26
    const-string v0, "cardNum"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getCardNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 27
    const-string v0, "expirationYear"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getExpirationYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 28
    const-string v0, "expirationMonth"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getExpirationMonth()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 29
    const-string v0, "cvs"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getCVS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 30
    const-string v0, "authNID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getAuthNID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 31
    const-string v0, "authPIN"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;->getAuthPIN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 32
    return-void
.end method
