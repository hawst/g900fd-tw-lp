.class final Lcom/sec/android/app/samsungapps/hr;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 44
    const-string v0, "info2.db"

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 45
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 396
    const-string v0, "INSERT INTO SamsungApps (_id, SamsungAppsMode, LatestMCC, AndromedaMCC, CountryURL, DisclaimerSkip, DisclaimerVersion, TwoGLimitSize, TwofiveGLimitSize, ThreeGLimitSize, FourGLimitSize, WifiLimitSize, FakeCSCCheck, userID, skipSignIn, NotiPopup, emailID, reserved, Encryption, CHubUrl, CIsStaging, CStagingImgHostUrl, CStagingAppHostUrl, CStagingDataHostUrl, CImgFolder, CAppFolder, CDataFolder, CTempFolder, CFakeImei, CFakeMcc, CFakeMnc, CFakeModel, CFakeCsc, CFakeOdcVer, CFakeOpenApiVer, CFakeHeaderHost, CLastCountryFreeStore, CHeaderInfinityVersion, CLastBannerProductId, CLastBannerImgUrl, CLastBannerType, CLastMcc, CLastMnc, CLastCsc, CDisclaimerVer, CAutoLogin, CUncMode, CProxyAddress, CCacheSize, CFreeDataUrl, CUncDataUrl, CDebugMode, CCurrencyUnitPrecedes, CLastCountryCode, CCurrencyUnitHasPenny, CLicenseUrl, CPlatformKey, CTransferConfig, CFreeTabPrecedes, CSnsCapaMask, CCurrencyUnitDivision, CSamsungNotiShow, CPsmsTestMode, CNotificationPopupShowSet, CConfigId, PhoneNumber, NetworkOperator, Birthday, Info, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, CLastModel, ServerLoadLevel, Option_12, Option_13, Option_14, Option_15, Option_16, Option_17, Option_18, Option_19, Option_20, Option_21, Option_22, Option_23, Option_24, Option_25, Option_26, Option_27, Option_28, Option_29, Option_30, Option_31, Option_32) VALUES "

    .line 502
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(\'0\',\'\',\'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'1\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'\', \'\', \'\', \'\', \'\');"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 605
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(\'1\',\'\',\'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'1\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'\', \'\', \'\', \'\', \'\');"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 706
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 51
    :try_start_0
    const-string v0, "CREATE TABLE SamsungApps (_id INTEGER PRIMARY KEY,SamsungAppsMode TEXT,LatestMCC TEXT,AndromedaMCC TEXT,CountryURL TEXT,DisclaimerSkip TEXT,DisclaimerVersion TEXT,TwoGLimitSize TEXT,TwofiveGLimitSize TEXT,ThreeGLimitSize TEXT,FourGLimitSize TEXT,WifiLimitSize TEXT,FakeCSCCheck TEXT,userID TEXT,skipSignIn TEXT,NotiPopup TEXT,emailID TEXT,reserved TEXT,Encryption TEXT,CHubUrl TEXT,CIsStaging TEXT,CStagingImgHostUrl TEXT,CStagingAppHostUrl TEXT,CStagingDataHostUrl TEXT,CImgFolder TEXT,CAppFolder TEXT,CDataFolder TEXT,CTempFolder TEXT,CFakeImei TEXT,CFakeMcc TEXT,CFakeMnc TEXT,CFakeModel TEXT,CFakeCsc TEXT,CFakeOdcVer TEXT,CFakeOpenApiVer TEXT,CFakeHeaderHost TEXT,CLastCountryFreeStore TEXT,CHeaderInfinityVersion TEXT,CLastBannerProductId TEXT,CLastBannerImgUrl TEXT,CLastBannerType TEXT,CLastMcc TEXT,CLastMnc TEXT,CLastCsc TEXT,CDisclaimerVer TEXT,CAutoLogin TEXT,CUncMode TEXT,CProxyAddress TEXT,CCacheSize TEXT,CFreeDataUrl TEXT,CUncDataUrl TEXT,CDebugMode TEXT,CCurrencyUnitPrecedes TEXT,CLastCountryCode TEXT,CCurrencyUnitHasPenny TEXT,CLicenseUrl TEXT,CPlatformKey TEXT,CTransferConfig TEXT,CFreeTabPrecedes TEXT,CSnsCapaMask TEXT,CCurrencyUnitDivision TEXT,CSamsungNotiShow TEXT,CPsmsTestMode TEXT,CNotificationPopupShowSet TEXT,CConfigId TEXT,PhoneNumber TEXT,NetworkOperator TEXT,Birthday TEXT,Info TEXT,push_service_onoff TEXT,SecureTime TEXT,push_service_reg_id TEXT,una_setting TEXT,FlexibleTabShow TEXT,FlexibleTabName TEXT,ProtocolCachingTime TEXT,IranShetabCardUrl TEXT,PushNotiRegistrationSuccess TEXT,CLastModel TEXT,ServerLoadLevel TEXT,Option_12 TEXT,Option_13 TEXT,Option_14 TEXT,Option_15 TEXT,Option_16 TEXT,Option_17 TEXT,Option_18 TEXT,Option_19 TEXT,Option_20 TEXT,Option_21 TEXT,Option_22 TEXT,Option_23 TEXT,Option_24 TEXT,Option_25 TEXT,Option_26 TEXT,Option_27 TEXT,Option_28 TEXT,Option_29 TEXT,Option_30 TEXT,Option_31 TEXT,Option_32 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/sec/android/app/samsungapps/SaProvider;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 161
    :goto_0
    return-void

    .line 155
    :pswitch_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/hr;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaProvider e="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :pswitch_1
    :try_start_1
    const-string v1, "_id, SamsungAppsMode, LatestMCC, AndromedaMCC, CountryURL, DisclaimerSkip, DisclaimerVersion, TwoGLimitSize, TwofiveGLimitSize, ThreeGLimitSize, FourGLimitSize, WifiLimitSize, FakeCSCCheck, userID, skipSignIn, NotiPopup, emailID, Encryption, CHubUrl, CIsStaging, CStagingImgHostUrl, CStagingAppHostUrl, CStagingDataHostUrl, CImgFolder, CAppFolder, CDataFolder, CTempFolder, CFakeImei, CFakeMcc, CFakeMnc, CFakeModel, CFakeCsc, CFakeOdcVer, CFakeOpenApiVer, CLastCountryFreeStore, CLastBannerProductId, CLastBannerImgUrl, CLastBannerType, CLastMcc, CLastMnc, CLastCsc, CDisclaimerVer, CAutoLogin, CUncMode, CProxyAddress, CCacheSize, CFreeDataUrl, CUncDataUrl, CDebugMode, CCurrencyUnitPrecedes, CLastCountryCode, CCurrencyUnitHasPenny, CLicenseUrl, CTransferConfig, CFreeTabPrecedes, CSnsCapaMask, CCurrencyUnitDivision, CSamsungNotiShow, CPsmsTestMode, CNotificationPopupShowSet, CConfigId, PhoneNumber, NetworkOperator, Birthday, Info, Option_12, Option_13, Option_14, Option_15, Option_16, Option_17, Option_18, Option_19, Option_20, Option_21, Option_22, Option_23, Option_24, Option_25, Option_26, Option_27, Option_28, Option_29, Option_30, Option_31, Option_32, "
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const-string v0, "reserved, CFakeHeaderHost, CHeaderInfinityVersion, CPlatformKey, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, CLastModel, ServerLoadLevel"

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "INSERT INTO SamsungApps ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "reserved, CFakeHeaderHost, CHeaderInfinityVersion, CPlatformKey, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, CLastModel, ServerLoadLevel) SELECT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "FROM SamsungApps_TMP WHERE _id=0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "INSERT INTO SamsungApps ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "reserved, CFakeHeaderHost, CHeaderInfinityVersion, CPlatformKey, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, CLastModel, ServerLoadLevel) SELECT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "FROM SamsungApps_TMP WHERE _id=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SaProvider;->a(I)I

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/hr;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :pswitch_2
    :try_start_4
    const-string v0, "password, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, Option_01, Option_02, Option_03, Option_04, Option_05, Option_06, Option_07, Option_08, Option_09, Option_10, Option_11 "

    goto :goto_1

    :pswitch_3
    const-string v0, "password, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, push_service_onoff, Option_02, Option_03, Option_04, Option_05, Option_06, Option_07, Option_08, Option_09, Option_10, Option_11 "

    goto :goto_1

    :pswitch_4
    const-string v0, "password, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, Option_04, Option_05, Option_06, Option_07, Option_08, Option_09, Option_10, Option_11 "

    goto :goto_1

    :pswitch_5
    const-string v0, "password, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, Option_07, Option_08, Option_09, Option_10, Option_11 "

    goto :goto_1

    :pswitch_6
    const-string v0, "password, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, Option_09, Option_10, Option_11 "

    goto :goto_1

    :pswitch_7
    const-string v0, "password, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, Option_10, Option_11 "

    goto :goto_1

    :pswitch_8
    const-string v0, "reserved, CCachedHostUrl, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, Option_10, Option_11 "

    goto :goto_1

    :pswitch_9
    const-string v0, "reserved, CFakeHeaderHost, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, Option_10, Option_11 "

    goto :goto_1

    :pswitch_a
    const-string v0, "reserved, CFakeHeaderHost, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, CLastModel, Option_11 "

    goto :goto_1

    :pswitch_b
    const-string v0, "reserved, CFakeHeaderHost, CSizeLimitation, CLastBannerTitle, push_service_onoff, SecureTime, push_service_reg_id, una_setting, FlexibleTabShow, FlexibleTabName, ProtocolCachingTime, IranShetabCardUrl, PushNotiRegistrationSuccess, CLastModel, ServerLoadLevel "
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upgrading SamsungApps database from version "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", which will destroy all old data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 168
    if-ge p2, p3, :cond_0

    .line 170
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SaProvider;->a(I)I

    .line 173
    :try_start_0
    const-string v0, "ALTER TABLE SamsungApps RENAME TO SamsungApps_TMP"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/hr;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 175
    const-string v0, "DROP TABLE IF EXISTS SamsungApps_TMP"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 176
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "new DB Version : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_0
    const-string v0, "DROP TABLE IF EXISTS SamsungApps"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/hr;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method
