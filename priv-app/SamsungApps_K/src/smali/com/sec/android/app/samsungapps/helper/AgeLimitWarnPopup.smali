.class public Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 19
    return-void
.end method


# virtual methods
.method protected matchCondition()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 36
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->restrictedAge:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->restrictedAge:Ljava/lang/String;

    const-string v3, "18+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 40
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 36
    goto :goto_0

    :cond_1
    move v0, v1

    .line 40
    goto :goto_1
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    const v1, 0xff2f

    invoke-static {p1, v1}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f08023d

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/a;-><init>(Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;->invokeCompleted()V

    .line 32
    return-void
.end method
