.class public final Lcom/sec/android/app/samsungapps/R$id;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final AdPreferenceMessage:I = 0x7f0c02a7

.field public static final AdPreferenceTitle:I = 0x7f0c02a6

.field public static final AutoUpdateChoice:I = 0x7f0c02a9

.field public static final AutoUpdateTitle:I = 0x7f0c02a8

.field public static final ChartBannerCustomView:I = 0x7f0c0199

.field public static final IVThumbnail:I = 0x7f0c00c4

.field public static final NotifyAppUpdatesMessage:I = 0x7f0c02ab

.field public static final NotifyAppUpdatesTitle:I = 0x7f0c02aa

.field public static final NotifyStoreActivitiesMessage:I = 0x7f0c02ad

.field public static final NotifyStoreActivitiesTitle:I = 0x7f0c02ac

.field public static final OptionSubTitle:I = 0x7f0c0093

.field public static final OptionTitle:I = 0x7f0c0092

.field public static final PurchaseProtectionMessage:I = 0x7f0c02af

.field public static final PurchaseProtectionTitle:I = 0x7f0c02ae

.field public static final RegiState:I = 0x7f0c001b

.field public static final RegiText:I = 0x7f0c001a

.field public static final Report_App_Reason_list:I = 0x7f0c010e

.field public static final TVNormalPrice:I = 0x7f0c0254

.field public static final TVProductName:I = 0x7f0c0251

.field public static final TVSellPrice:I = 0x7f0c0255

.field public static final TVSellerName:I = 0x7f0c0252

.field public static final UpdateNotificationChoice:I = 0x7f0c02e8

.field public static final UpdateNotificationTitle:I = 0x7f0c02e7

.field public static final about:I = 0x7f0c000e

.field public static final account:I = 0x7f0c01d1

.field public static final account_info:I = 0x7f0c01d3

.field public static final action_bar:I = 0x7f0c0039

.field public static final action_bar_layout:I = 0x7f0c0036

.field public static final action_item_layout:I = 0x7f0c029c

.field public static final action_layout:I = 0x7f0c029b

.field public static final action_settings:I = 0x7f0c030b

.field public static final ad_learn_more_text:I = 0x7f0c003c

.field public static final ad_setting_popup_body_text:I = 0x7f0c003b

.field public static final add_credit_card:I = 0x7f0c0204

.field public static final adult_icon:I = 0x7f0c005b

.field public static final alerts:I = 0x7f0c0004

.field public static final all_selector:I = 0x7f0c006b

.field public static final alphabetical_a_to_z:I = 0x7f0c0015

.field public static final alphabetical_z_to_a:I = 0x7f0c0016

.field public static final amex_credit_card_img:I = 0x7f0c02bf

.field public static final app_icon:I = 0x7f0c02f5

.field public static final app_name:I = 0x7f0c02f6

.field public static final app_snippet:I = 0x7f0c02f4

.field public static final application_icon_image_view:I = 0x7f0c01c7

.field public static final application_name_text_view:I = 0x7f0c01c8

.field public static final apps_for_banner:I = 0x7f0c0175

.field public static final appsfor_galaxy_description:I = 0x7f0c0170

.field public static final auth_name_help_msg:I = 0x7f0c0229

.field public static final auto_update_layout:I = 0x7f0c02a5

.field public static final available_app_layout:I = 0x7f0c0152

.field public static final backarea:I = 0x7f0c016e

.field public static final banner_item_main_body:I = 0x7f0c018e

.field public static final banner_text:I = 0x7f0c0176

.field public static final best01_category_name:I = 0x7f0c0197

.field public static final best01_pimg:I = 0x7f0c0195

.field public static final best01_product_name:I = 0x7f0c0196

.field public static final best01_rating:I = 0x7f0c0198

.field public static final best_selling:I = 0x7f0c0011

.field public static final big_banner:I = 0x7f0c01b6

.field public static final big_banner_item_widget:I = 0x7f0c0193

.field public static final bigbanner_gallery:I = 0x7f0c018d

.field public static final black_content_list:I = 0x7f0c0186

.field public static final body:I = 0x7f0c0050

.field public static final body_giftcard_coupon:I = 0x7f0c023e

.field public static final body_layout:I = 0x7f0c004f

.field public static final body_title:I = 0x7f0c0276

.field public static final bottom_layout:I = 0x7f0c0051

.field public static final bottom_line:I = 0x7f0c008a

.field public static final bt_detail_tag_item:I = 0x7f0c0102

.field public static final btnDeregi:I = 0x7f0c0023

.field public static final btnRegi:I = 0x7f0c0022

.field public static final btn_add_giftcardcoupon:I = 0x7f0c023d

.field public static final btn_common_more_down_arrow:I = 0x7f0c007a

.field public static final btn_detail_main_get:I = 0x7f0c00b4

.field public static final btn_detail_main_like:I = 0x7f0c00af

.field public static final btn_detail_main_sub_tab_overview:I = 0x7f0c00bb

.field public static final btn_detail_main_sub_tab_related:I = 0x7f0c00c1

.field public static final btn_detail_main_sub_tab_reveiw:I = 0x7f0c00be

.field public static final btn_detail_main_uninstall:I = 0x7f0c00b2

.field public static final btn_download_all:I = 0x7f0c0172

.field public static final btn_progress_cancel:I = 0x7f0c0044

.field public static final btn_review_delete:I = 0x7f0c012a

.field public static final btn_review_edit:I = 0x7f0c0129

.field public static final btn_starting:I = 0x7f0c02f3

.field public static final btn_updown_giftcardcoupon:I = 0x7f0c0241

.field public static final buttons_area:I = 0x7f0c0168

.field public static final card_frame:I = 0x7f0c01d9

.field public static final card_info:I = 0x7f0c01df

.field public static final card_layout:I = 0x7f0c01d5

.field public static final card_number:I = 0x7f0c01d7

.field public static final category_list:I = 0x7f0c0161

.field public static final category_list_name:I = 0x7f0c0163

.field public static final category_list_new:I = 0x7f0c0164

.field public static final category_list_subicon:I = 0x7f0c0165

.field public static final category_view:I = 0x7f0c0097

.field public static final chart_banner:I = 0x7f0c0048

.field public static final chart_banner_layout:I = 0x7f0c019f

.field public static final chart_bottom_padding:I = 0x7f0c019c

.field public static final chart_view_fragment_scrollview:I = 0x7f0c0047

.field public static final check:I = 0x7f0c0089

.field public static final check_talkback:I = 0x7f0c0155

.field public static final checkbox:I = 0x7f0c009f

.field public static final checkbox_layout:I = 0x7f0c009e

.field public static final ck_detail_overview_auto_update:I = 0x7f0c00d6

.field public static final code1:I = 0x7f0c0277

.field public static final code2:I = 0x7f0c0278

.field public static final code3:I = 0x7f0c0279

.field public static final code4:I = 0x7f0c027a

.field public static final confirm_edit_randomkey:I = 0x7f0c0268

.field public static final confirm_text_msg:I = 0x7f0c026a

.field public static final confirm_text_notice:I = 0x7f0c026c

.field public static final confirm_text_randomkey:I = 0x7f0c0269

.field public static final confirm_text_summary:I = 0x7f0c026b

.field public static final content_list:I = 0x7f0c0078

.field public static final content_list_top_free:I = 0x7f0c004a

.field public static final content_list_top_new:I = 0x7f0c004b

.field public static final content_list_top_paid:I = 0x7f0c0049

.field public static final corrected_text:I = 0x7f0c008c

.field public static final corrected_title:I = 0x7f0c008b

.field public static final countryFlag:I = 0x7f0c01a8

.field public static final country_list:I = 0x7f0c02f7

.field public static final credit_card_expired_layout:I = 0x7f0c01da

.field public static final credit_card_img:I = 0x7f0c0200

.field public static final credit_card_invalid_card_layout:I = 0x7f0c01dc

.field public static final curtain:I = 0x7f0c018f

.field public static final cvc_information:I = 0x7f0c027e

.field public static final danal_micro_btn_auth_num:I = 0x7f0c0236

.field public static final danal_micro_btn_disclaimer:I = 0x7f0c0238

.field public static final danal_micro_check_disclaimer:I = 0x7f0c023a

.field public static final danal_micro_edit_auth_num:I = 0x7f0c0237

.field public static final danal_micro_edit_ssn_left:I = 0x7f0c022f

.field public static final danal_micro_edit_ssn_right:I = 0x7f0c0232

.field public static final danal_micro_layout_auth:I = 0x7f0c0235

.field public static final danal_micro_layout_disclaimer:I = 0x7f0c0239

.field public static final danal_micro_manually_layout:I = 0x7f0c022b

.field public static final danal_micro_phone_num:I = 0x7f0c022a

.field public static final danal_micro_phone_number:I = 0x7f0c022e

.field public static final danal_micro_phone_number_auto:I = 0x7f0c0230

.field public static final danal_micro_phone_number_manual:I = 0x7f0c022c

.field public static final danal_micro_spinner_method:I = 0x7f0c022d

.field public static final danal_micro_text_auth_num:I = 0x7f0c0234

.field public static final danal_micro_text_ssn:I = 0x7f0c0231

.field public static final danal_micro_text_ssn_center:I = 0x7f0c0233

.field public static final delete_checkbox_gap:I = 0x7f0c0294

.field public static final delete_checkbox_layout:I = 0x7f0c0293

.field public static final depth_btn:I = 0x7f0c008d

.field public static final description_expand_button_layout:I = 0x7f0c02fc

.field public static final description_layout:I = 0x7f0c02fa

.field public static final description_text:I = 0x7f0c02fb

.field public static final description_text_view:I = 0x7f0c00ab

.field public static final detail_empty:I = 0x7f0c0211

.field public static final detail_loading:I = 0x7f0c0210

.field public static final detail_main_sub_tab_overview_selector:I = 0x7f0c00bc

.field public static final detail_main_sub_tab_related_selector:I = 0x7f0c00c2

.field public static final detail_main_sub_tab_reveiw_selector:I = 0x7f0c00bf

.field public static final detail_seller_page_info_ceo:I = 0x7f0c00f2

.field public static final detail_seller_page_info_contact:I = 0x7f0c00f5

.field public static final detail_seller_page_info_email:I = 0x7f0c00f6

.field public static final detail_seller_page_info_location:I = 0x7f0c00f7

.field public static final detail_seller_page_info_name:I = 0x7f0c00f1

.field public static final detail_seller_page_info_permit_number:I = 0x7f0c00f3

.field public static final detail_seller_page_info_ps:I = 0x7f0c00f8

.field public static final detail_seller_page_info_sales_number:I = 0x7f0c00f4

.field public static final discount_price:I = 0x7f0c0274

.field public static final discover_credit_card_img:I = 0x7f0c02c0

.field public static final divider:I = 0x7f0c029e

.field public static final downloadText:I = 0x7f0c003e

.field public static final downloadable_list:I = 0x7f0c0174

.field public static final downloadable_list_top_button:I = 0x7f0c0173

.field public static final downloadble_all_selector:I = 0x7f0c015a

.field public static final downloadble_list_withbtn_all:I = 0x7f0c015b

.field public static final downloadble_list_withbtn_update:I = 0x7f0c0158

.field public static final downloadble_update_selector:I = 0x7f0c0157

.field public static final downloadble_withbtn_all_layout:I = 0x7f0c0159

.field public static final downloadble_withbtn_update_layout:I = 0x7f0c0156

.field public static final dummy:I = 0x7f0c029d

.field public static final editAccount:I = 0x7f0c000a

.field public static final editText1:I = 0x7f0c0021

.field public static final edit_button:I = 0x7f0c0203

.field public static final email_edit:I = 0x7f0c009b

.field public static final email_text:I = 0x7f0c009a

.field public static final empty_data:I = 0x7f0c0080

.field public static final empty_icon:I = 0x7f0c0082

.field public static final empty_icon_layout:I = 0x7f0c0081

.field public static final empty_loading:I = 0x7f0c007f

.field public static final empty_manually:I = 0x7f0c007e

.field public static final empty_pb:I = 0x7f0c02a2

.field public static final empty_text:I = 0x7f0c0083

.field public static final enter_pin2:I = 0x7f0c0282

.field public static final everglaze_banner:I = 0x7f0c014c

.field public static final everglaze_frame:I = 0x7f0c014b

.field public static final expand_button:I = 0x7f0c02fd

.field public static final expiration_date:I = 0x7f0c01d8

.field public static final expired:I = 0x7f0c0202

.field public static final expired_gift_cards_list:I = 0x7f0c01e0

.field public static final expired_layout:I = 0x7f0c01e5

.field public static final expiry_mm:I = 0x7f0c02c3

.field public static final expiry_yy:I = 0x7f0c02c5

.field public static final explain_text:I = 0x7f0c0209

.field public static final fastscroll_layout:I = 0x7f0c02f9

.field public static final fastscroll_scroll:I = 0x7f0c02f8

.field public static final find_password:I = 0x7f0c00a0

.field public static final first_best_layout:I = 0x7f0c01a0

.field public static final first_num:I = 0x7f0c0194

.field public static final for_hover:I = 0x7f0c0190

.field public static final galleryLayout:I = 0x7f0c0138

.field public static final gift_card_name:I = 0x7f0c01e1

.field public static final giftcard_layout:I = 0x7f0c01e3

.field public static final giftcard_name:I = 0x7f0c01f3

.field public static final giftcard_scroll_view:I = 0x7f0c01e4

.field public static final google_tencent_icon:I = 0x7f0c005d

.field public static final gridarea:I = 0x7f0c01aa

.field public static final gridview:I = 0x7f0c00ee

.field public static final gridview_item_divider:I = 0x7f0c015f

.field public static final help:I = 0x7f0c0005

.field public static final home:I = 0x7f0c0008

.field public static final icon:I = 0x7f0c0055

.field public static final icon_layout:I = 0x7f0c0090

.field public static final icon_padding:I = 0x7f0c0037

.field public static final icon_padding_left:I = 0x7f0c029a

.field public static final image:I = 0x7f0c0088

.field public static final image_knox_title:I = 0x7f0c02f2

.field public static final image_padding:I = 0x7f0c013b

.field public static final img_card:I = 0x7f0c01d6

.field public static final img_expired:I = 0x7f0c01db

.field public static final img_invalid:I = 0x7f0c01dd

.field public static final img_security_card:I = 0x7f0c01be

.field public static final inicis_purchase_progress_bodyly_bar:I = 0x7f0c024b

.field public static final inicis_purchase_progress_ly:I = 0x7f0c0249

.field public static final inicis_purchase_progress_name:I = 0x7f0c024a

.field public static final inicis_purchase_progress_size:I = 0x7f0c024c

.field public static final inicis_purchase_result_text:I = 0x7f0c024e

.field public static final inicis_webview:I = 0x7f0c024d

.field public static final inputPwd:I = 0x7f0c0291

.field public static final install_confirm_question:I = 0x7f0c0305

.field public static final installed_text_view:I = 0x7f0c0066

.field public static final installed_time_view:I = 0x7f0c01c9

.field public static final isa_screenshot_pager:I = 0x7f0c02a0

.field public static final item_Balance:I = 0x7f0c020d

.field public static final item_amount:I = 0x7f0c01e6

.field public static final item_amount_value:I = 0x7f0c01e7

.field public static final item_available_services:I = 0x7f0c01ed

.field public static final item_balance:I = 0x7f0c01e8

.field public static final item_balance_value:I = 0x7f0c01e9

.field public static final item_check:I = 0x7f0c0244

.field public static final item_code:I = 0x7f0c0214

.field public static final item_date:I = 0x7f0c01f8

.field public static final item_description:I = 0x7f0c01ee

.field public static final item_disc_name:I = 0x7f0c0248

.field public static final item_empty_icon:I = 0x7f0c024f

.field public static final item_expiry:I = 0x7f0c01f7

.field public static final item_expiry_date:I = 0x7f0c0247

.field public static final item_issue_date:I = 0x7f0c0216

.field public static final item_last_used:I = 0x7f0c01eb

.field public static final item_layout:I = 0x7f0c01f5

.field public static final item_name:I = 0x7f0c020c

.field public static final item_name_with_code:I = 0x7f0c0208

.field public static final item_not_apply:I = 0x7f0c0245

.field public static final item_period:I = 0x7f0c01ea

.field public static final item_price:I = 0x7f0c01f4

.field public static final item_state:I = 0x7f0c01f6

.field public static final item_status:I = 0x7f0c01ec

.field public static final item_sub_name:I = 0x7f0c0246

.field public static final iv_detail_agegrade:I = 0x7f0c00de

.field public static final iv_detail_main_button_google_icon:I = 0x7f0c00b8

.field public static final iv_detail_main_widget_badge:I = 0x7f0c00c5

.field public static final iv_google_bg:I = 0x7f0c00cc

.field public static final iv_review_item_seller_icon:I = 0x7f0c012c

.field public static final iv_samsungapps_bg:I = 0x7f0c0250

.field public static final jcb_credit_card_img:I = 0x7f0c02c1

.field public static final layout_about_body:I = 0x7f0c0034

.field public static final layout_about_current_version:I = 0x7f0c002c

.field public static final layout_about_download_button:I = 0x7f0c002f

.field public static final layout_about_lastest_version:I = 0x7f0c002d

.field public static final layout_about_line:I = 0x7f0c0035

.field public static final layout_about_logo:I = 0x7f0c002a

.field public static final layout_about_logo_text:I = 0x7f0c002b

.field public static final layout_about_open_source:I = 0x7f0c0033

.field public static final layout_about_pref_text_item:I = 0x7f0c02a4

.field public static final layout_about_privacy_policy:I = 0x7f0c0031

.field public static final layout_about_terms_and_conditions:I = 0x7f0c0030

.field public static final layout_about_youth_privacy_policy:I = 0x7f0c0032

.field public static final layout_account_pref_text_item:I = 0x7f0c02b6

.field public static final layout_add_giftcardcoupon:I = 0x7f0c023c

.field public static final layout_address:I = 0x7f0c02cd

.field public static final layout_billing_address:I = 0x7f0c01de

.field public static final layout_btn_detail_overview_description_down_arrow:I = 0x7f0c00d1

.field public static final layout_btn_detail_overview_whatsnew_down_arrow:I = 0x7f0c00e6

.field public static final layout_btn_download_all:I = 0x7f0c0171

.field public static final layout_button:I = 0x7f0c0292

.field public static final layout_button_tabs:I = 0x7f0c0069

.field public static final layout_city:I = 0x7f0c02ce

.field public static final layout_customer_support_pref_text_item:I = 0x7f0c0307

.field public static final layout_detail:I = 0x7f0c00a1

.field public static final layout_detail_agegrade:I = 0x7f0c00dd

.field public static final layout_detail_body:I = 0x7f0c014a

.field public static final layout_detail_empty:I = 0x7f0c00a2

.field public static final layout_detail_expert_review:I = 0x7f0c011d

.field public static final layout_detail_expert_review_app_title:I = 0x7f0c011e

.field public static final layout_detail_expert_review_more:I = 0x7f0c0120

.field public static final layout_detail_fragments:I = 0x7f0c00a6

.field public static final layout_detail_main_Price:I = 0x7f0c0253

.field public static final layout_detail_main_google_app:I = 0x7f0c00b7

.field public static final layout_detail_main_like:I = 0x7f0c00ae

.field public static final layout_detail_main_midle:I = 0x7f0c00ad

.field public static final layout_detail_main_rating:I = 0x7f0c00c9

.field public static final layout_detail_main_sub_tab:I = 0x7f0c00ba

.field public static final layout_detail_main_top:I = 0x7f0c00cd

.field public static final layout_detail_main_view:I = 0x7f0c00a5

.field public static final layout_detail_overveiw_description_info_container:I = 0x7f0c00ce

.field public static final layout_detail_overveiw_description_whatsnew_container:I = 0x7f0c00e3

.field public static final layout_detail_overview_auto_update:I = 0x7f0c00d4

.field public static final layout_detail_overview_auto_update_area:I = 0x7f0c00d5

.field public static final layout_detail_overview_container:I = 0x7f0c00e8

.field public static final layout_detail_overview_description_container:I = 0x7f0c00d7

.field public static final layout_detail_overview_fragment:I = 0x7f0c00a7

.field public static final layout_detail_overview_info:I = 0x7f0c00d3

.field public static final layout_detail_overview_screenshot_container:I = 0x7f0c00e0

.field public static final layout_detail_overview_screenshot_view:I = 0x7f0c00e2

.field public static final layout_detail_overview_seller_info_widget:I = 0x7f0c00e9

.field public static final layout_detail_overview_whatsnew_container:I = 0x7f0c00d8

.field public static final layout_detail_productlist_app_title:I = 0x7f0c00eb

.field public static final layout_detail_related_developer_info:I = 0x7f0c00f9

.field public static final layout_detail_related_developer_info_email_addr:I = 0x7f0c00fd

.field public static final layout_detail_related_developer_info_webpage:I = 0x7f0c00fb

.field public static final layout_detail_related_fragment:I = 0x7f0c00a8

.field public static final layout_detail_related_more:I = 0x7f0c00ed

.field public static final layout_detail_related_product_widget:I = 0x7f0c00ea

.field public static final layout_detail_related_productlist_widget1:I = 0x7f0c010a

.field public static final layout_detail_related_productlist_widget2:I = 0x7f0c010b

.field public static final layout_detail_related_seller_info:I = 0x7f0c00ef

.field public static final layout_detail_related_tag:I = 0x7f0c0103

.field public static final layout_detail_related_tag_btn_list:I = 0x7f0c0107

.field public static final layout_detail_related_tag_container:I = 0x7f0c0104

.field public static final layout_detail_related_tag_list_container:I = 0x7f0c0106

.field public static final layout_detail_related_tag_more_btn:I = 0x7f0c0109

.field public static final layout_detail_related_tag_widget:I = 0x7f0c010c

.field public static final layout_detail_review_expert_item1:I = 0x7f0c0121

.field public static final layout_detail_review_expert_item2:I = 0x7f0c0123

.field public static final layout_detail_review_expert_item_link:I = 0x7f0c0113

.field public static final layout_detail_review_expert_item_text:I = 0x7f0c010f

.field public static final layout_detail_review_expert_widget:I = 0x7f0c0136

.field public static final layout_detail_review_user_item_area:I = 0x7f0c0131

.field public static final layout_detail_review_user_item_divider:I = 0x7f0c0122

.field public static final layout_detail_review_user_item_nodata:I = 0x7f0c0134

.field public static final layout_detail_review_user_widget:I = 0x7f0c0137

.field public static final layout_detail_reviews_fragment:I = 0x7f0c00a9

.field public static final layout_detail_screenshot_land:I = 0x7f0c013a

.field public static final layout_detail_screenshot_land_border:I = 0x7f0c0139

.field public static final layout_detail_screenshot_port:I = 0x7f0c013d

.field public static final layout_detail_screenshot_port_border:I = 0x7f0c013c

.field public static final layout_detail_screenshot_youtube:I = 0x7f0c013f

.field public static final layout_detail_screenshot_youtube_border:I = 0x7f0c013e

.field public static final layout_detail_user_review_more:I = 0x7f0c0133

.field public static final layout_detail_view:I = 0x7f0c00a3

.field public static final layout_detail_widget_empty:I = 0x7f0c00aa

.field public static final layout_disclaimer_privacy:I = 0x7f0c0053

.field public static final layout_disclaimer_terms_and_conditions:I = 0x7f0c0052

.field public static final layout_download_all:I = 0x7f0c016f

.field public static final layout_expert_link_area:I = 0x7f0c0116

.field public static final layout_expert_main_area:I = 0x7f0c0117

.field public static final layout_find_email:I = 0x7f0c02d4

.field public static final layout_first_name:I = 0x7f0c02cb

.field public static final layout_gap_overview_description:I = 0x7f0c00d2

.field public static final layout_gap_overview_whatsnew:I = 0x7f0c00e7

.field public static final layout_global_info:I = 0x7f0c02ca

.field public static final layout_gridview_item:I = 0x7f0c015e

.field public static final layout_hovering:I = 0x7f0c01a5

.field public static final layout_hovering_category_name:I = 0x7f0c01a3

.field public static final layout_hovering_rating:I = 0x7f0c01a4

.field public static final layout_hovering_title:I = 0x7f0c01a2

.field public static final layout_item_foreground:I = 0x7f0c014d

.field public static final layout_itemly_edit_email:I = 0x7f0c01f1

.field public static final layout_itemly_edit_password:I = 0x7f0c02d3

.field public static final layout_itemly_left_password:I = 0x7f0c02d2

.field public static final layout_itemly_left_top_email:I = 0x7f0c02d1

.field public static final layout_last_name:I = 0x7f0c02cc

.field public static final layout_list_item_rating_area:I = 0x7f0c0153

.field public static final layout_list_itemly:I = 0x7f0c0056

.field public static final layout_list_itemly_app_category_name:I = 0x7f0c005f

.field public static final layout_list_itemly_app_description:I = 0x7f0c014f

.field public static final layout_list_itemly_app_purchased_date:I = 0x7f0c014e

.field public static final layout_list_itemly_app_size:I = 0x7f0c0150

.field public static final layout_list_itemly_banner_tablet:I = 0x7f0c02ff

.field public static final layout_list_itemly_centerly:I = 0x7f0c005c

.field public static final layout_list_itemly_centerly_bottomly_leftly:I = 0x7f0c0151

.field public static final layout_list_itemly_centerly_bottomly_rating:I = 0x7f0c0064

.field public static final layout_list_itemly_centerly_oname:I = 0x7f0c0160

.field public static final layout_list_itemly_centerly_pname:I = 0x7f0c005e

.field public static final layout_list_itemly_check:I = 0x7f0c017d

.field public static final layout_list_itemly_discprice:I = 0x7f0c0062

.field public static final layout_list_itemly_hovering_text_main:I = 0x7f0c019b

.field public static final layout_list_itemly_hovering_text_title:I = 0x7f0c019a

.field public static final layout_list_itemly_imgly:I = 0x7f0c0057

.field public static final layout_list_itemly_imgly_backimg:I = 0x7f0c0191

.field public static final layout_list_itemly_imgly_hovering:I = 0x7f0c0192

.field public static final layout_list_itemly_imgly_pimg:I = 0x7f0c0059

.field public static final layout_list_itemly_imgly_ptype:I = 0x7f0c005a

.field public static final layout_list_itemly_page_name:I = 0x7f0c0300

.field public static final layout_list_itemly_price:I = 0x7f0c0063

.field public static final layout_list_itemly_pricely:I = 0x7f0c0061

.field public static final layout_list_itemly_product_count:I = 0x7f0c01a1

.field public static final layout_list_itemly_rightly:I = 0x7f0c0065

.field public static final layout_list_itemly_rightly_button:I = 0x7f0c0154

.field public static final layout_list_itemly_select_image:I = 0x7f0c0301

.field public static final layout_list_listly:I = 0x7f0c017b

.field public static final layout_list_not_available_app:I = 0x7f0c0060

.field public static final layout_list_top_layout:I = 0x7f0c0162

.field public static final layout_list_user_keyword_icon:I = 0x7f0c0187

.field public static final layout_list_withbtn_all_main:I = 0x7f0c006a

.field public static final layout_list_withbtn_left:I = 0x7f0c006d

.field public static final layout_list_withbtn_new:I = 0x7f0c0073

.field public static final layout_list_withbtn_right:I = 0x7f0c0070

.field public static final layout_list_withbtnly_all:I = 0x7f0c006c

.field public static final layout_list_withbtnly_left:I = 0x7f0c006f

.field public static final layout_list_withbtnly_new:I = 0x7f0c0075

.field public static final layout_list_withbtnly_right:I = 0x7f0c0072

.field public static final layout_more_loading:I = 0x7f0c007b

.field public static final layout_more_view_contaner:I = 0x7f0c0076

.field public static final layout_noti_progress_bodyly_bar:I = 0x7f0c01b3

.field public static final layout_noti_progress_bodyly_bar_percent:I = 0x7f0c01cf

.field public static final layout_noti_progress_name:I = 0x7f0c01d0

.field public static final layout_noti_progress_percent:I = 0x7f0c01b2

.field public static final layout_noti_progress_size:I = 0x7f0c01b0

.field public static final layout_noti_progress_state:I = 0x7f0c01b4

.field public static final layout_noti_progress_total_size:I = 0x7f0c01b1

.field public static final layout_payment_method:I = 0x7f0c01ff

.field public static final layout_payment_method_item:I = 0x7f0c0201

.field public static final layout_payment_method_title:I = 0x7f0c01fc

.field public static final layout_payment_method_view:I = 0x7f0c01fe

.field public static final layout_post_code:I = 0x7f0c02d0

.field public static final layout_pressed:I = 0x7f0c016d

.field public static final layout_purchase_cybercash_edit_id:I = 0x7f0c0224

.field public static final layout_purchase_cybercash_edit_password:I = 0x7f0c0226

.field public static final layout_purchase_cybercash_help:I = 0x7f0c0227

.field public static final layout_purchase_cybercash_spinner_method:I = 0x7f0c0222

.field public static final layout_purchase_cybercash_text_id:I = 0x7f0c0223

.field public static final layout_purchase_cybercash_text_method:I = 0x7f0c0221

.field public static final layout_purchase_cybercash_text_password:I = 0x7f0c0225

.field public static final layout_purchase_prepaid_button_terms:I = 0x7f0c0266

.field public static final layout_purchase_prepaid_check_terms:I = 0x7f0c0267

.field public static final layout_purchase_prepaid_edit_cardnum:I = 0x7f0c0263

.field public static final layout_purchase_prepaid_edit_face:I = 0x7f0c0261

.field public static final layout_purchase_prepaid_edit_password:I = 0x7f0c0265

.field public static final layout_purchase_prepaid_spinner_method:I = 0x7f0c025e

.field public static final layout_purchase_prepaid_text_cardnum:I = 0x7f0c0262

.field public static final layout_purchase_prepaid_text_face:I = 0x7f0c0260

.field public static final layout_purchase_prepaid_text_method:I = 0x7f0c025d

.field public static final layout_purchase_prepaid_text_notice:I = 0x7f0c025c

.field public static final layout_purchase_prepaid_text_password:I = 0x7f0c0264

.field public static final layout_purchase_prepaid_text_script:I = 0x7f0c025f

.field public static final layout_pwd:I = 0x7f0c028f

.field public static final layout_report:I = 0x7f0c0100

.field public static final layout_report_contanier:I = 0x7f0c00ff

.field public static final layout_reset_locale_pref_text_item:I = 0x7f0c02b4

.field public static final layout_retry_btn:I = 0x7f0c007c

.field public static final layout_review_list_item:I = 0x7f0c0124

.field public static final layout_review_list_item_btn:I = 0x7f0c0128

.field public static final layout_review_list_item_seller_review:I = 0x7f0c012b

.field public static final layout_scroll_entire:I = 0x7f0c0228

.field public static final layout_scroll_entire_signup:I = 0x7f0c0220

.field public static final layout_securitycode:I = 0x7f0c02c7

.field public static final layout_sign_text_item:I = 0x7f0c017c

.field public static final layout_sign_text_not_account:I = 0x7f0c02d5

.field public static final layout_signup_date_birthday:I = 0x7f0c02dd

.field public static final layout_signup_edit_cardnumber:I = 0x7f0c02c2

.field public static final layout_signup_edit_email:I = 0x7f0c02d7

.field public static final layout_signup_edit_password:I = 0x7f0c02d9

.field public static final layout_signup_edit_password_confirm:I = 0x7f0c02db

.field public static final layout_signup_edit_securitycode:I = 0x7f0c02c6

.field public static final layout_signup_edit_securitycode_help:I = 0x7f0c02c9

.field public static final layout_signup_text_account:I = 0x7f0c02ba

.field public static final layout_signup_text_account_email:I = 0x7f0c02bb

.field public static final layout_signup_text_account_title:I = 0x7f0c01d2

.field public static final layout_signup_text_birthday:I = 0x7f0c02dc

.field public static final layout_signup_text_cardtype:I = 0x7f0c02bc

.field public static final layout_signup_text_email:I = 0x7f0c02d6

.field public static final layout_signup_text_expirydate:I = 0x7f0c02c4

.field public static final layout_signup_text_password:I = 0x7f0c02d8

.field public static final layout_signup_text_password_confirm:I = 0x7f0c02da

.field public static final layout_signup_text_securitycode:I = 0x7f0c02c8

.field public static final layout_spinner_state:I = 0x7f0c02cf

.field public static final layout_updown_giftcardcoupon:I = 0x7f0c023f

.field public static final layout_widget_view:I = 0x7f0c0077

.field public static final left_selector:I = 0x7f0c006e

.field public static final line_list:I = 0x7f0c0207

.field public static final list:I = 0x7f0c0079

.field public static final listView_permission:I = 0x7f0c01fb

.field public static final list_layout_container:I = 0x7f0c0185

.field public static final list_layout_giftcardcoupon:I = 0x7f0c0242

.field public static final listline:I = 0x7f0c01a6

.field public static final listview:I = 0x7f0c0132

.field public static final listview_giftcardcoupon:I = 0x7f0c0243

.field public static final localized_layout:I = 0x7f0c01b9

.field public static final localized_link:I = 0x7f0c01bb

.field public static final localized_text:I = 0x7f0c01ba

.field public static final logGenSwitch:I = 0x7f0c0017

.field public static final logSendButton:I = 0x7f0c0024

.field public static final main_big_banner:I = 0x7f0c018b

.field public static final main_title:I = 0x7f0c0038

.field public static final main_view:I = 0x7f0c003a

.field public static final main_view_fragment_scrollview:I = 0x7f0c01b5

.field public static final main_view_pager_title:I = 0x7f0c01bc

.field public static final margin_top:I = 0x7f0c01d4

.field public static final margin_user_review_top:I = 0x7f0c0125

.field public static final master_credit_card_img:I = 0x7f0c02be

.field public static final menu_layout:I = 0x7f0c029f

.field public static final more_button:I = 0x7f0c019d

.field public static final more_button_layout:I = 0x7f0c019e

.field public static final more_details_button:I = 0x7f0c01cd

.field public static final more_loading_retry_button:I = 0x7f0c007d

.field public static final more_view:I = 0x7f0c016a

.field public static final morearrow:I = 0x7f0c01a9

.field public static final morebutton:I = 0x7f0c01a7

.field public static final most_recent:I = 0x7f0c0012

.field public static final navi_all_layout:I = 0x7f0c0297

.field public static final navigation:I = 0x7f0c0298

.field public static final navigation_padding_right:I = 0x7f0c0299

.field public static final negative:I = 0x7f0c015c

.field public static final new_selector:I = 0x7f0c0074

.field public static final new_version_available:I = 0x7f0c002e

.field public static final news_text_information:I = 0x7f0c02de

.field public static final newversion:I = 0x7f0c02e2

.field public static final nice_webview:I = 0x7f0c0309

.field public static final no_search_result_text_layout:I = 0x7f0c0183

.field public static final no_search_result_text_view:I = 0x7f0c0184

.field public static final notice_layout:I = 0x7f0c01c0

.field public static final notice_list:I = 0x7f0c017f

.field public static final notice_scroll:I = 0x7f0c01c3

.field public static final noticedate:I = 0x7f0c01c6

.field public static final noticedetail:I = 0x7f0c01bf

.field public static final noticedetail_body:I = 0x7f0c01c4

.field public static final noticedetail_date:I = 0x7f0c01c2

.field public static final noticedetail_title:I = 0x7f0c01c1

.field public static final noticelist:I = 0x7f0c0180

.field public static final noticetitle:I = 0x7f0c01c5

.field public static final notification_popup_check_layout:I = 0x7f0c01ce

.field public static final notification_popup_img:I = 0x7f0c01cc

.field public static final notification_popup_layout:I = 0x7f0c01ca

.field public static final notification_popup_text:I = 0x7f0c01cb

.field public static final notifyState:I = 0x7f0c0019

.field public static final notify_giftcard_text:I = 0x7f0c01f0

.field public static final notify_voucher_text:I = 0x7f0c020a

.field public static final ok_button:I = 0x7f0c0306

.field public static final option_layout:I = 0x7f0c0087

.field public static final padding:I = 0x7f0c0142

.field public static final padding2:I = 0x7f0c01ab

.field public static final padding_gb:I = 0x7f0c0146

.field public static final pager:I = 0x7f0c0046

.field public static final parentCategoryLayout:I = 0x7f0c0167

.field public static final parentPurchasedListLayout:I = 0x7f0c016b

.field public static final parentUncListLayout:I = 0x7f0c0189

.field public static final password_edit:I = 0x7f0c009d

.field public static final password_text:I = 0x7f0c009c

.field public static final payment_info_text:I = 0x7f0c0257

.field public static final payment_info_title:I = 0x7f0c0256

.field public static final payment_information:I = 0x7f0c021f

.field public static final payment_list:I = 0x7f0c01fd

.field public static final payment_list_widget:I = 0x7f0c0205

.field public static final payment_method:I = 0x7f0c0003

.field public static final payment_permission:I = 0x7f0c021b

.field public static final pb_progressbar:I = 0x7f0c0043

.field public static final perm_body:I = 0x7f0c0217

.field public static final permissionContents:I = 0x7f0c021a

.field public static final permissionLabel:I = 0x7f0c0219

.field public static final permissionLabelgap:I = 0x7f0c0218

.field public static final permission_list_subicon:I = 0x7f0c025a

.field public static final permission_root:I = 0x7f0c025b

.field public static final permission_title:I = 0x7f0c01fa

.field public static final permission_title_layout:I = 0x7f0c01f9

.field public static final pg_webview:I = 0x7f0c030a

.field public static final positive:I = 0x7f0c015d

.field public static final price:I = 0x7f0c01e2

.field public static final price_layout:I = 0x7f0c0271

.field public static final price_low_to_high:I = 0x7f0c0013

.field public static final price_result:I = 0x7f0c0273

.field public static final progress:I = 0x7f0c0026

.field public static final progress_large:I = 0x1010000

.field public static final progress_layout:I = 0x7f0c003d

.field public static final promotion_bottom:I = 0x7f0c01b8

.field public static final promotion_mainview:I = 0x7f0c01af

.field public static final promotion_top:I = 0x7f0c01b7

.field public static final prompt:I = 0x7f0c0303

.field public static final prompt_message:I = 0x7f0c0302

.field public static final purchase_button:I = 0x7f0c021c

.field public static final purchase_giftcard_coupon:I = 0x7f0c027b

.field public static final purchase_item_info:I = 0x7f0c021e

.field public static final purchased:I = 0x7f0c0001

.field public static final purchased_all_selector:I = 0x7f0c028d

.field public static final purchased_button_tabs:I = 0x7f0c0288

.field public static final purchased_check_all:I = 0x7f0c0286

.field public static final purchased_check_all_tabs:I = 0x7f0c0285

.field public static final purchased_list:I = 0x7f0c0179

.field public static final purchased_list_top_button:I = 0x7f0c0178

.field public static final purchased_list_withbtn_all:I = 0x7f0c028e

.field public static final purchased_list_withbtn_update:I = 0x7f0c028b

.field public static final purchased_select_all_guide:I = 0x7f0c0287

.field public static final purchased_tencent:I = 0x7f0c0002

.field public static final purchased_update_selector:I = 0x7f0c028a

.field public static final purchased_withbtn_all_layout:I = 0x7f0c028c

.field public static final purchased_withbtn_update_layout:I = 0x7f0c0289

.field public static final push_notification_check:I = 0x7f0c02b2

.field public static final push_notification_header:I = 0x7f0c02b1

.field public static final push_notification_layout:I = 0x7f0c02b0

.field public static final push_notification_message:I = 0x7f0c02b3

.field public static final rating_high_to_low:I = 0x7f0c0014

.field public static final ratingbar_detail_main_average:I = 0x7f0c00ca

.field public static final rb_review_list_item_chart:I = 0x7f0c0126

.field public static final register_button:I = 0x7f0c0206

.field public static final register_listview_giftcard:I = 0x7f0c01f2

.field public static final register_listview_voucher:I = 0x7f0c020b

.field public static final relativeLayout1:I = 0x7f0c02fe

.field public static final resetlocale_arrow_image:I = 0x7f0c02b5

.field public static final resultBox:I = 0x7f0c001f

.field public static final retry:I = 0x7f0c0084

.field public static final retry_button:I = 0x7f0c0086

.field public static final retry_layout:I = 0x7f0c0177

.field public static final retry_text:I = 0x7f0c0085

.field public static final review_detail:I = 0x7f0c0295

.field public static final review_detail_comment:I = 0x7f0c02ee

.field public static final review_detail_letter_number:I = 0x7f0c02ef

.field public static final review_detail_rating:I = 0x7f0c02ed

.field public static final review_detail_rating_layout:I = 0x7f0c02ec

.field public static final review_scrollview:I = 0x7f0c02eb

.field public static final right_selector:I = 0x7f0c0071

.field public static final root_layout:I = 0x7f0c017e

.field public static final scroll:I = 0x7f0c004e

.field public static final scrollView:I = 0x7f0c001e

.field public static final scroll_container:I = 0x7f0c0029

.field public static final scroll_view:I = 0x7f0c021d

.field public static final scrollview:I = 0x7f0c0149

.field public static final scrollview_about:I = 0x7f0c0028

.field public static final scrollview_detail_view:I = 0x7f0c00a4

.field public static final scrollview_nodata:I = 0x7f0c02a1

.field public static final search_cancel:I = 0x7f0c008f

.field public static final search_edit:I = 0x7f0c008e

.field public static final search_keyword_view:I = 0x7f0c0182

.field public static final search_list:I = 0x7f0c0181

.field public static final searching_text:I = 0x7f0c02a3

.field public static final second_subtitle:I = 0x7f0c0259

.field public static final second_title:I = 0x7f0c0258

.field public static final security_code:I = 0x7f0c027c

.field public static final security_code_title:I = 0x7f0c027d

.field public static final security_select_icon:I = 0x7f0c0281

.field public static final security_sub_text:I = 0x7f0c0280

.field public static final security_title_text:I = 0x7f0c027f

.field public static final select:I = 0x7f0c0007

.field public static final select_text_giftcardcoupon:I = 0x7f0c0240

.field public static final sendLogByEmail:I = 0x7f0c0010

.field public static final settings:I = 0x7f0c0006

.field public static final settings_support_notice:I = 0x7f0c0308

.field public static final share:I = 0x7f0c000f

.field public static final shetab_enter_pin2_code_txt:I = 0x7f0c0283

.field public static final shetab_input_pin2_code:I = 0x7f0c0284

.field public static final signin:I = 0x7f0c000b

.field public static final signout:I = 0x7f0c0009

.field public static final single_choice_item_image:I = 0x7f0c0094

.field public static final single_choice_item_layout:I = 0x7f0c0091

.field public static final skip_layout:I = 0x7f0c02e5

.field public static final small_banner_border:I = 0x7f0c02df

.field public static final small_banner_grid_view:I = 0x7f0c02f1

.field public static final small_banner_img:I = 0x7f0c02e0

.field public static final small_banner_title:I = 0x7f0c02f0

.field public static final softkey_area:I = 0x7f0c0169

.field public static final softkey_button:I = 0x7f0c0096

.field public static final softkey_button_sub_layout_gb:I = 0x7f0c0144

.field public static final softkey_button_sub_layout_ics:I = 0x7f0c0140

.field public static final softkey_layout:I = 0x7f0c0095

.field public static final softkey_negative:I = 0x7f0c0141

.field public static final softkey_negative_gb:I = 0x7f0c0147

.field public static final softkey_positive:I = 0x7f0c0143

.field public static final softkey_positive_gb:I = 0x7f0c0145

.field public static final sortby:I = 0x7f0c000d

.field public static final special_banner:I = 0x7f0c018c

.field public static final special_category_divider:I = 0x7f0c0166

.field public static final stopLogSendButton:I = 0x7f0c0025

.field public static final strpwd:I = 0x7f0c0290

.field public static final suggestAllShareContentPreferenceMessage:I = 0x7f0c02b9

.field public static final suggestAllShareContentPreferenceTitle:I = 0x7f0c02b7

.field public static final suggest_allshare_content_preference_check:I = 0x7f0c02b8

.field public static final supportAndNotices:I = 0x7f0c000c

.field public static final tabs:I = 0x7f0c0045

.field public static final tax_info:I = 0x7f0c0275

.field public static final text:I = 0x7f0c0099

.field public static final textView1:I = 0x7f0c0020

.field public static final textView2:I = 0x7f0c001c

.field public static final textView3:I = 0x7f0c001d

.field public static final textView5:I = 0x7f0c0018

.field public static final text_content:I = 0x7f0c0054

.field public static final text_dont_update:I = 0x7f0c02e3

.field public static final text_information:I = 0x7f0c0304

.field public static final title:I = 0x7f0c004d

.field public static final title_background:I = 0x7f0c016c

.field public static final title_code:I = 0x7f0c0213

.field public static final title_description:I = 0x7f0c01ae

.field public static final title_description_layout:I = 0x7f0c01ad

.field public static final title_giftcardcoupon:I = 0x7f0c023b

.field public static final title_issue_date:I = 0x7f0c0215

.field public static final title_layout:I = 0x7f0c004c

.field public static final title_padding:I = 0x7f0c01ac

.field public static final tnc_scrollview:I = 0x7f0c026f

.field public static final tnc_text_pname:I = 0x7f0c026d

.field public static final tnc_text_price:I = 0x7f0c026e

.field public static final tnc_text_tnc:I = 0x7f0c0270

.field public static final toast_layout_root:I = 0x7f0c0098

.field public static final total_price:I = 0x7f0c0272

.field public static final tv_detail_agegrade:I = 0x7f0c00dc

.field public static final tv_detail_expert_review_title:I = 0x7f0c011f

.field public static final tv_detail_main_button_like_count:I = 0x7f0c00b0

.field public static final tv_detail_main_button_reduce_price:I = 0x7f0c00b6

.field public static final tv_detail_main_button_selling_Price:I = 0x7f0c00b5

.field public static final tv_detail_main_button_uninstall:I = 0x7f0c00b3

.field public static final tv_detail_main_category_name:I = 0x7f0c00c8

.field public static final tv_detail_main_google_play_info:I = 0x7f0c00b9

.field public static final tv_detail_main_people_number:I = 0x7f0c00cb

.field public static final tv_detail_main_product_name:I = 0x7f0c00c6

.field public static final tv_detail_main_seller_name:I = 0x7f0c00c7

.field public static final tv_detail_main_uninstallig:I = 0x7f0c00b1

.field public static final tv_detail_overview_description:I = 0x7f0c00d0

.field public static final tv_detail_overview_description_title:I = 0x7f0c00cf

.field public static final tv_detail_overview_iap:I = 0x7f0c00df

.field public static final tv_detail_overview_info_date:I = 0x7f0c00db

.field public static final tv_detail_overview_info_size:I = 0x7f0c00da

.field public static final tv_detail_overview_info_version:I = 0x7f0c00d9

.field public static final tv_detail_overview_whatsnew_text:I = 0x7f0c00e5

.field public static final tv_detail_overview_whatsnew_title:I = 0x7f0c00e4

.field public static final tv_detail_related_developer_info_email_addr:I = 0x7f0c00fe

.field public static final tv_detail_related_developer_info_title:I = 0x7f0c00fa

.field public static final tv_detail_related_developer_info_webpage:I = 0x7f0c00fc

.field public static final tv_detail_related_empty_text:I = 0x7f0c010d

.field public static final tv_detail_related_other_opp_title:I = 0x7f0c00ec

.field public static final tv_detail_related_seller_info_title:I = 0x7f0c00f0

.field public static final tv_detail_related_tag_title:I = 0x7f0c0105

.field public static final tv_detail_report_app:I = 0x7f0c0101

.field public static final tv_detail_review_expert_item_date:I = 0x7f0c0111

.field public static final tv_detail_review_expert_item_link_title:I = 0x7f0c0114

.field public static final tv_detail_review_expert_item_link_url:I = 0x7f0c0115

.field public static final tv_detail_review_expert_item_name:I = 0x7f0c0110

.field public static final tv_detail_review_expert_item_review_text:I = 0x7f0c0112

.field public static final tv_detail_review_expert_list_item_link_title:I = 0x7f0c011b

.field public static final tv_detail_review_expert_list_item_link_url:I = 0x7f0c011c

.field public static final tv_detail_review_write_btn:I = 0x7f0c0135

.field public static final tv_detail_user_review_title:I = 0x7f0c0130

.field public static final tv_progress_percent:I = 0x7f0c0042

.field public static final tv_progress_percent_num:I = 0x7f0c00ac

.field public static final tv_progress_size:I = 0x7f0c0040

.field public static final tv_progress_status:I = 0x7f0c003f

.field public static final tv_progress_total_size:I = 0x7f0c0041

.field public static final tv_review_list_expert_item_review_text:I = 0x7f0c011a

.field public static final tv_review_list_item_date:I = 0x7f0c0119

.field public static final tv_review_list_item_name:I = 0x7f0c0118

.field public static final tv_review_list_item_review_text:I = 0x7f0c0127

.field public static final tv_review_list_item_seller_date:I = 0x7f0c012e

.field public static final tv_review_list_item_seller_name:I = 0x7f0c012d

.field public static final tv_review_list_item_seller_review_text:I = 0x7f0c012f

.field public static final txt_detail_main_sub_tab_overview:I = 0x7f0c00bd

.field public static final txt_detail_main_sub_tab_related:I = 0x7f0c00c3

.field public static final txt_detail_main_sub_tab_reveiw:I = 0x7f0c00c0

.field public static final unc_list:I = 0x7f0c018a

.field public static final updateNotification_arrow_image:I = 0x7f0c02e9

.field public static final update_later_layout:I = 0x7f0c02e4

.field public static final update_noti_layout:I = 0x7f0c02e6

.field public static final updatetitle:I = 0x7f0c02e1

.field public static final view_detail_related_tag_gap:I = 0x7f0c0108

.field public static final viewpager:I = 0x7f0c01bd

.field public static final visat_credit_card_img:I = 0x7f0c02bd

.field public static final voucher_detail:I = 0x7f0c0212

.field public static final voucher_detail_down_arrow:I = 0x7f0c020e

.field public static final voucher_detail_view:I = 0x7f0c020f

.field public static final voucher_scroll_view:I = 0x7f0c01ef

.field public static final webFrameLayout:I = 0x7f0c0058

.field public static final webview:I = 0x7f0c02ea

.field public static final whole_layout:I = 0x7f0c0148

.field public static final widget_about:I = 0x7f0c0027

.field public static final widget_detail_screenshot:I = 0x7f0c00e1

.field public static final widget_help_list:I = 0x7f0c017a

.field public static final widget_review_list:I = 0x7f0c0296

.field public static final widget_settings_list:I = 0x7f0c0188

.field public static final wish_check_all:I = 0x7f0c0068

.field public static final wish_check_all_ly:I = 0x7f0c0067

.field public static final wishlist:I = 0x7f0c0000


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
