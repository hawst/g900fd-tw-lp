.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;
.source "ProGuard"


# instance fields
.field imgfile:Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

.field mHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field mURL:Ljava/lang/String;

.field retryCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->retryCount:I

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->checkStagingConditionURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->imgfile:Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;II)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->retryCount:I

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->checkStagingConditionURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    invoke-direct {v0, v1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;-><init>(Ljava/lang/String;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->imgfile:Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    .line 35
    return-void
.end method

.method private checkStagingConditionURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 64
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStagingImgHostUrl()Ljava/lang/String;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 71
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :cond_0
    :goto_0
    return-object p1

    .line 72
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0
.end method

.method private notifyImgDownloadResult(Z)V
    .locals 1

    .prologue
    .line 109
    if-nez p1, :cond_0

    .line 111
    const-string v0, "notifyimage result fail"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 114
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 115
    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->imgfile:Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getHttpGet()Lorg/apache/http/client/methods/HttpGet;
    .locals 4

    .prologue
    .line 39
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStagingImgHostUrl()Ljava/lang/String;

    move-result-object v2

    .line 44
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mURL:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getNewURL()Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getOldHost()Ljava/lang/String;

    move-result-object v0

    .line 51
    :cond_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 52
    if-eqz v0, :cond_1

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v2, "Host"

    invoke-virtual {v1, v2, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method

.method public getThreadNo()I
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public isSucceed()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public needRetry()Z
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->retryCount:I

    .line 137
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->retryCount:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResult(Lorg/apache/http/HttpResponse;)V
    .locals 3

    .prologue
    .line 83
    const/4 v1, 0x0

    .line 84
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 87
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->imgfile:Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->writeFile(Ljava/io/InputStream;)Z

    move-result v1

    .line 89
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->notifyImgDownloadResult(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->notifyImgDownloadResult(Z)V

    goto :goto_0

    .line 94
    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 100
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->notifyImgDownloadResult(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 102
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->notifyImgDownloadResult(Z)V

    throw v0
.end method
