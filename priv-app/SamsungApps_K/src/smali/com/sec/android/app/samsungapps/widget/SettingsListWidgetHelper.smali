.class public Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetData;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/PreferenceScreen;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->b:Landroid/preference/PreferenceScreen;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->b:Landroid/preference/PreferenceScreen;

    .line 32
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->b:Landroid/preference/PreferenceScreen;

    .line 41
    return-void
.end method

.method public getRootScreen()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->b:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method public hasAbout()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method public hasAccountSetting()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 150
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return v2

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-ne v0, v1, :cond_1

    move v2, v1

    .line 159
    goto :goto_0

    .line 162
    :cond_1
    new-instance v3, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/app/Activity;)V

    .line 163
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->isExistSamsungAccount()Z

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v1, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    .line 169
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public hasAdPreference()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 257
    const/4 v1, 0x0

    .line 259
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 261
    const-string v3, "com.sec.android.app.secad"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 265
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public hasAutoUpdate()Z
    .locals 2

    .prologue
    .line 197
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;-><init>(Landroid/content/Context;)V

    .line 201
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->checkAutoUpdSettingVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const/4 v0, 0x1

    .line 208
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNotifyAppUpdates()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public hasNotifyStoreActivities()Z
    .locals 2

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->isUsablePushService(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    const/4 v0, 0x1

    .line 246
    :cond_0
    return v0
.end method

.method public hasPurchaseProtection()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x1

    return v0
.end method

.method public hasSearchSetting()Z
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    return v0
.end method
