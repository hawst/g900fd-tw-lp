.class public Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RequestWishListCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;

.field _Parser:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RequestWishListCommand;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RequestWishListCommand;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;->getCountPerPage()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RequestWishListCommand;->_Parser:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;

    .line 21
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 35
    return-void
.end method
