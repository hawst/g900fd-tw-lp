.class public Lcom/sec/android/app/samsungapps/ErrorHandler;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;


# static fields
.field private static f:Lcom/sec/android/app/samsungapps/ErrorHandler;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

.field c:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

.field d:Ljava/util/ArrayList;

.field e:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->c:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->d:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->e:Ljava/util/ArrayList;

    .line 31
    return-void
.end method

.method static synthetic a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 417
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 418
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    .line 420
    return-object v0
.end method

.method private static b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 134
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->context:Landroid/content/Context;

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/ErrorHandler;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/samsungapps/ErrorHandler;->f:Lcom/sec/android/app/samsungapps/ErrorHandler;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/ErrorHandler;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/ErrorHandler;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/ErrorHandler;->f:Lcom/sec/android/app/samsungapps/ErrorHandler;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/ErrorHandler;->f:Lcom/sec/android/app/samsungapps/ErrorHandler;

    return-object v0
.end method


# virtual methods
.method public addServerErrorObserver(Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->c:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public onRetry(Z)V
    .locals 5

    .prologue
    .line 81
    monitor-enter p0

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/dm;

    .line 85
    if-eqz p1, :cond_0

    .line 86
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/dm;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_0
    :try_start_1
    iget-object v2, v0, Lcom/sec/android/app/samsungapps/dm;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/dm;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    const/4 v4, 0x0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/dm;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v2, v3, v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 95
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public processError(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 13

    .prologue
    const v11, 0x7f0802cf

    const v10, 0x7f08028e

    const v9, 0x7f08023d

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 139
    if-nez p1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_3

    .line 146
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, p1, v5, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    .line 151
    :cond_3
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 153
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v0

    .line 154
    invoke-virtual {p1, p1, v5, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 156
    if-eqz v0, :cond_0

    .line 158
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080183

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0

    .line 164
    :cond_4
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 165
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    .line 166
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    if-eqz p2, :cond_0

    if-eqz v6, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isSuppressedErrorCode(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v6, v6, v5, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isDisplayingErrorPopupSuppressed()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-virtual {v6, v6, v5, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    :cond_6
    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getReqName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v7

    if-nez v7, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0, v6, v5, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    :cond_7
    const-string v2, ""

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080276

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, ""

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v8, Lcom/sec/android/app/samsungapps/dl;->a:[I

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->ordinal()I

    move-result v3

    aget v3, v8, v3

    packed-switch v3, :pswitch_data_0

    move v3, v5

    :goto_1
    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isBackgroundRetryBlocked()Z

    move-result v8

    if-eqz v8, :cond_1f

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0802ef

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-eqz v5, :cond_1e

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->d:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/dm;

    invoke-direct {v1, v6, p2}, Lcom/sec/android/app/samsungapps/dm;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->e:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/dm;

    invoke-direct {v1, v6, p2}, Lcom/sec/android/app/samsungapps/dm;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_8

    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08028e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08023d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const/4 v6, 0x1

    invoke-direct {v5, v0, v1, v2, v6}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v0, Lcom/sec/android/app/samsungapps/dh;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/dh;-><init>(Lcom/sec/android/app/samsungapps/ErrorHandler;)V

    invoke-virtual {v5, v3, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/di;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/di;-><init>(Lcom/sec/android/app/samsungapps/ErrorHandler;)V

    invoke-virtual {v5, v4, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    move-result v0

    if-nez v0, :cond_8

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_8
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_0
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move v3, v4

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move v3, v4

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move v3, v4

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->c:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;

    invoke-interface {v0, p2}, Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;->onReceiveServerError(Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_3

    :cond_9
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "5107"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "4000"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v0, v1, v5, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "5000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLoginAfterSessionExpired()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/dk;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/app/samsungapps/dk;-><init>(Lcom/sec/android/app/samsungapps/ErrorHandler;Lcom/sec/android/app/samsungapps/vlibrary/net/Request;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "3012"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "3001"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "3015"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v0, v1, v5, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const-string v2, "3013"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->b()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getRealCountryCode()Ljava/lang/String;

    move-result-object v3

    new-instance v8, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    invoke-direct {v8, v2}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/content/Context;)V

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/ErrorHandler;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ErrorHandler;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->isExistSamsungAccount()Z

    move-result v8

    if-ne v8, v4, :cond_f

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f08009a

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v5

    aput-object v0, v8, v4

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_5
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move v3, v5

    move-object v12, v0

    move-object v0, v2

    move-object v2, v1

    move-object v1, v12

    goto/16 :goto_1

    :cond_f
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f080099

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v5

    aput-object v0, v8, v4

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_10
    const-string v2, "5214"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string v2, "7001"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string v2, "7003"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_11
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_12
    const-string v2, "5215"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080210

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_13
    const-string v2, "5254"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08014b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_14
    const-string v2, "5255"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "5256"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    :cond_15
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08014d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_16
    const-string v2, "5218"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "5204"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_17
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08020f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_18
    const-string v2, "5227"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080175

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_19
    const-string v2, "7002"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080176

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_1a
    const-string v2, "7004"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    const-string v2, "7006"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    :cond_1b
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080177

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_1c
    const-string v2, "7007"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08013a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_1d
    invoke-static {v7, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_5

    :pswitch_4
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move v3, v4

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move v3, v4

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1e
    new-instance v3, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-direct {v3, v7, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/dj;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/dj;-><init>(Lcom/sec/android/app/samsungapps/ErrorHandler;)V

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto/16 :goto_0

    :cond_1f
    move v5, v3

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public release()V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method public removeServerErrorObserver(Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ErrorHandler;->c:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method
