.class final Lcom/sec/android/app/samsungapps/api/a;
.super Lcom/sec/android/app/samsungapps/api/aidl/IInstallAPI$Stub;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/api/aidl/IInstallAPI$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final installPackage(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    new-instance v1, Lcom/sec/android/app/samsungapps/api/d;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-direct {v1, v2, p3}, Lcom/sec/android/app/samsungapps/api/d;-><init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V

    invoke-static {v0, p2, p1, v1}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-static {v0, p3}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V

    goto :goto_0
.end method

.method public final installPackageWithMSG(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    new-instance v1, Lcom/sec/android/app/samsungapps/api/e;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-direct {v1, v2, p4, p2}, Lcom/sec/android/app/samsungapps/api/e;-><init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;Ljava/lang/String;)V

    invoke-static {v0, p3, p1, v1}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/a;->a:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    invoke-static {v0, p4}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V

    goto :goto_0
.end method
