.class public Lcom/sec/android/app/samsungapps/ScreenShotGallery;
.super Landroid/widget/Gallery;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

.field b:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->b:I

    .line 54
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 60
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 62
    :cond_0
    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0

    .line 65
    :cond_1
    const/high16 v0, 0x40400000    # 3.0f

    div-float v0, p3, v0

    .line 67
    invoke-super {p0, p1, p2, v0, p4}, Landroid/widget/Gallery;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 20
    packed-switch p1, :pswitch_data_0

    .line 39
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 23
    :pswitch_0
    iget v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->b:I

    if-lez v1, :cond_0

    .line 25
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    iget v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->setSel(I)V

    .line 27
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getSel()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setSelection(I)V

    .line 28
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->playSoundEffect(I)V

    goto :goto_0

    .line 31
    :pswitch_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->b:I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 33
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    iget v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->b:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->setSel(I)V

    .line 35
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getSel()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setSelection(I)V

    .line 36
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->playSoundEffect(I)V

    goto :goto_0

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setScreenShotInterface(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;)V
    .locals 1

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    .line 48
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getSel()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->b:I

    .line 49
    return-void
.end method
