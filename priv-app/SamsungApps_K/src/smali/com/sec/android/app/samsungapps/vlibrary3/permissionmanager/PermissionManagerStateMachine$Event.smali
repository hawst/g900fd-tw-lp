.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum APK_PERMISSION_COMPARE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum INVOKECOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum PACKAGE_ALREADY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum PACKAGE_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum SUCCESS_BUT_EMPTY_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum SUCCESS_EXIST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field public static final enum USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "CHECK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "SUCCESS_EXIST_PERMISSION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_EXIST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "FAIL_RECEIVE_PERMISSION"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "SUCCESS_BUT_EMPTY_PERMISSION"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_BUT_EMPTY_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "INVOKECOMPLETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->INVOKECOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "USER_AGREE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "USER_DISAGREE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "PACKAGE_NOT_INSTALLED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->PACKAGE_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "PACKAGE_ALREADY_INSTALLED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->PACKAGE_ALREADY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    const-string v1, "APK_PERMISSION_COMPARE_COMPLETE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->APK_PERMISSION_COMPARE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    .line 10
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_EXIST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_BUT_EMPTY_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->INVOKECOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->PACKAGE_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->PACKAGE_ALREADY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->APK_PERMISSION_COMPARE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    return-object v0
.end method
