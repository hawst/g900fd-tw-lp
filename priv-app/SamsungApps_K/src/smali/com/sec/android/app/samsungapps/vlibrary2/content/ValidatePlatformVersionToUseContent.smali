.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->onFinalResult(Z)V

    return-void
.end method

.method private validateCompatibleOS()V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->isPlatformVersionOkToUseContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->onFinalResult(Z)V

    .line 43
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/w;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/w;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->showIncompatibleOS(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->validateCompatibleOS()V

    .line 25
    return-void
.end method

.method public isPlatformVersionOkToUseContent()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 70
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidatePlatformVersionToUseContent;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->compatibleOS:Ljava/lang/String;

    .line 50
    if-nez v0, :cond_1

    .line 51
    const-string v0, "7"

    .line 53
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v3

    .line 54
    :try_start_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 59
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 61
    cmpg-double v0, v6, v4

    if-gez v0, :cond_2

    move v0, v2

    .line 63
    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 70
    goto :goto_0
.end method
