.class final Lcom/sec/android/app/samsungapps/view/r;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:Landroid/view/LayoutInflater;

.field b:I

.field c:Landroid/content/Context;

.field final synthetic d:Lcom/sec/android/app/samsungapps/view/CouponListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/view/CouponListView;Landroid/content/Context;)V
    .locals 2

    .prologue
    const v1, 0x7f040094

    .line 268
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/r;->d:Lcom/sec/android/app/samsungapps/view/CouponListView;

    .line 269
    invoke-direct {p0, p2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/r;->a:Landroid/view/LayoutInflater;

    .line 259
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/r;->b:I

    .line 271
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/r;->a:Landroid/view/LayoutInflater;

    .line 272
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/r;->b:I

    .line 273
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    .line 274
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/r;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;)V
    .locals 3

    .prologue
    .line 257
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    const v0, 0x7f0c0214

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-nez p2, :cond_4

    :cond_0
    const-string v0, "VoucherItemArray::showCouponCode::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_0
    const v0, 0x7f0c0216

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p2, :cond_1

    if-nez v0, :cond_5

    :cond_1
    const-string v0, "VoucherItemArray::showCouponIssueDate::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_1
    const v0, 0x7f0c01ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p2, :cond_2

    if-nez v0, :cond_6

    :cond_2
    const-string v0, "VoucherItemArray::showCouponDescription::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;->couponID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;->availablePeriodEndDate:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;->couponDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/r;Landroid/view/View;ZLcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    .line 257
    const v0, 0x7f0c020f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_3

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0c0210

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const v0, 0x7f0c0211

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    const v0, 0x7f0c0212

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    if-eqz p3, :cond_0

    if-eqz v2, :cond_0

    if-eqz v6, :cond_0

    if-nez v3, :cond_2

    :cond_0
    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz p1, :cond_1

    const v0, 0x7f0c020e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    const v2, 0x7f0801b2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const v1, 0x7f020075

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v7

    new-instance v0, Lcom/sec/android/app/samsungapps/view/s;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/view/s;-><init>(Lcom/sec/android/app/samsungapps/view/r;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;Landroid/widget/LinearLayout;)V

    invoke-virtual {v7, p3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->customerCouponDetail(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    const v2, 0x7f0801b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 278
    .line 279
    if-nez p2, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/r;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/r;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 283
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/r;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    .line 285
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v1

    const v0, 0x7f0c020c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    if-nez v1, :cond_4

    :cond_1
    const-string v0, "VoucherItemArray::showCouponName::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080277

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0800f7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0c020d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    if-nez v1, :cond_5

    :cond_2
    const-string v0, "VoucherItemArray::showCouponBalanceAndExpiryDate::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 287
    :cond_3
    :goto_1
    return-object p2

    .line 285
    :cond_4
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v6

    if-ne v5, v6, :cond_6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountPrice()D

    move-result-wide v5

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getAvailablePeriodEndDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v4

    if-ne v8, v4, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountRate()D

    move-result-wide v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "%s%%"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getAvailablePeriodEndDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    const/4 v3, 0x3

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v4

    if-ne v3, v4, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    const v5, 0x7f0800ef

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getUsageCountRemain()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    const v5, 0x7f0800ed

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/r;->c:Landroid/content/Context;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getAvailablePeriodEndDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
