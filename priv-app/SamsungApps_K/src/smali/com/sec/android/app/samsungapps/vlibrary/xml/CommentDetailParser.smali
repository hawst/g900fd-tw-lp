.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mComment:Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailParser;->mComment:Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;

    .line 11
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;-><init>()V

    .line 22
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailParser;->mComment:Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->setCommentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;)V

    .line 24
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method
