.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _Price:D

.field private _TaxIncludedPrice:D


# direct methods
.method public constructor <init>(D)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_Price:D

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 66
    const-string v0, "priceWithTax"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_TaxIncludedPrice:D

    .line 70
    :try_start_0
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_TaxIncludedPrice:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 2

    .prologue
    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_TaxIncludedPrice:D

    .line 81
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTaxIncludedPrice()D
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_TaxIncludedPrice:D

    return-wide v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_Price:D

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;)V

    invoke-virtual {v0, v1, v2, p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getPriceWithTax(DLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 47
    const-string v1, "5402"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 51
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 52
    return-void
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method
