.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 11
    const-string v2, "autoCompleteSearch2Notc"

    const-string v3, "2190"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    const-string v0, "keyword"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->getKeyword()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "countKeyword"

    const-string v1, "25"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "contentType"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->getSearchContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v0, "qlDomainCode"

    const-string v1, "sa"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceType()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;

    move-result-object v0

    .line 20
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;->none:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;

    if-eq v0, v1, :cond_0

    .line 22
    const-string v1, "qlDeviceType"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    :cond_0
    const-string v0, "qlInputMethod"

    const-string v1, "ac"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method private static cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    :try_start_0
    const-string v0, "[^^\t\r\n -\ud7ff\ue000-\ufffd]"

    .line 31
    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    .line 32
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 34
    const-string v0, ""

    goto :goto_0
.end method
