.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x9L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method protected createContentFromHashMap(Ljava/util/HashMap;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>(Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    return-object v0
.end method

.method protected getContentListFromHashMapList(Ljava/util/Vector;)V
    .locals 2

    .prologue
    .line 59
    if-nez p1, :cond_1

    .line 73
    :cond_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 66
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 70
    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->createContentFromHashMap(Ljava/util/HashMap;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    .line 71
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getSearchNextStartIndex()I
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->getContentsGettingCount()I

    move-result v0

    .line 37
    if-lez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->getIndexOfLastItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x1

    div-int/2addr v1, v0

    mul-int/2addr v0, v1

    .line 42
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateListItem(Ljava/util/Vector;I)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->getContentListFromHashMapList(Ljava/util/Vector;)V

    .line 49
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->setTotalCount(I)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->getIndexOfLastItem()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->setIndexOfLastItem(I)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->getIndexOfLastItem()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->getTotalCount()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->setCompleted(Z)V

    .line 54
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/linkedstore/SearchContentList;->setNeverLoaded(Z)V

    .line 55
    return-void
.end method
