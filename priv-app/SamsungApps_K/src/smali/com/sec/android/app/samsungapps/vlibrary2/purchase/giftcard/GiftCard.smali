.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;


# static fields
.field private static final NULLGIFTCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;


# instance fields
.field public balanceAmount:D

.field public currencyCode:Ljava/lang/String;

.field public currencyUnit:Ljava/lang/String;

.field public giftCardCode:Ljava/lang/String;

.field public giftCardImagePath:Ljava/lang/String;

.field public giftCardName:Ljava/lang/String;

.field public giftCardStatusCode:Ljava/lang/String;

.field public userExpireDate:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;-><init>(Z)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->NULLGIFTCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardCode:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardName:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyCode:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyUnit:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->userExpireDate:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardStatusCode:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardImagePath:Ljava/lang/String;

    .line 18
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 19
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardCode:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardName:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyCode:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyUnit:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->userExpireDate:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardStatusCode:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardImagePath:Ljava/lang/String;

    .line 23
    if-eqz p1, :cond_0

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardCode:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardName:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyCode:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyUnit:Ljava/lang/String;

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->balanceAmount:D

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->userExpireDate:Ljava/lang/String;

    .line 31
    const-string v0, "A"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardStatusCode:Ljava/lang/String;

    .line 33
    :cond_0
    return-void
.end method

.method public static Null()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->NULLGIFTCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;

    return-object v0
.end method


# virtual methods
.method public getBalanceAmount()D
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->balanceAmount:D

    return-wide v0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyCode:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->currencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardCode:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardImagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardName:Ljava/lang/String;

    return-object v0
.end method

.method public giftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;
    .locals 2

    .prologue
    .line 66
    const-string v0, "A"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->giftCardStatusCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    goto :goto_0
.end method

.method public userExpireDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->userExpireDate:Ljava/lang/String;

    return-object v0
.end method
