.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract cancelDeletion()V
.end method

.method public abstract deleteSelectedItemCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getCountPerPage()I
.end method

.method public abstract getWishListArray()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;
.end method

.method public abstract requestListCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
