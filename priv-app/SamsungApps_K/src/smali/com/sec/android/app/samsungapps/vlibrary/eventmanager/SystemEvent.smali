.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field bResponseRequired:Z

.field bSuccess:Z

.field private downloadingInfo:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

.field mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

.field mExtraData:Ljava/lang/Object;

.field mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    .line 67
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 68
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->downloadingInfo:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 54
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 74
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mExtraData:Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    .line 61
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 62
    return-void
.end method


# virtual methods
.method public failed()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->processed()Z

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;->onSystemRequestResult(Z)V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 171
    const/4 v0, 0x1

    .line 173
    :cond_0
    return v0
.end method

.method public getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;
    .locals 0

    .prologue
    .line 129
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    return-object p0
.end method

.method public getCheckAppUpgradeEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;
    .locals 0

    .prologue
    .line 124
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;

    return-object p0
.end method

.method public getDownloadingInfo()Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->downloadingInfo:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    return-object v0
.end method

.method public getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    return-object v0
.end method

.method public getExtraData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mExtraData:Ljava/lang/Object;

    return-object v0
.end method

.method public getInicisPaymentEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;
    .locals 0

    .prologue
    .line 139
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;

    return-object p0
.end method

.method public getSuccess()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    return v0
.end method

.method public getUpdateAppEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;
    .locals 0

    .prologue
    .line 134
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;

    return-object p0
.end method

.method public getYesOrNoEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;
    .locals 0

    .prologue
    .line 178
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    return-object p0
.end method

.method public isResponseRequired()Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processed()Z
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->requestProcessed(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z

    move-result v0

    return v0
.end method

.method public setDownloadInfo(Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->downloadingInfo:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    .line 80
    return-void
.end method

.method protected setExtraData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mExtraData:Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public setFail()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 100
    return-void
.end method

.method protected setResponseRequired()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bResponseRequired:Z

    .line 85
    return-void
.end method

.method public setSuccess()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->bSuccess:Z

    .line 95
    return-void
.end method

.method public success()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->processed()Z

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;->onSystemRequestResult(Z)V

    .line 158
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->mOnResultHandler:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 161
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
