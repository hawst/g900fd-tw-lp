.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryParser;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    .line 11
    return-void
.end method


# virtual methods
.method public getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryParser;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    return-object v0
.end method

.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryParser;->mCountry:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 16
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method
