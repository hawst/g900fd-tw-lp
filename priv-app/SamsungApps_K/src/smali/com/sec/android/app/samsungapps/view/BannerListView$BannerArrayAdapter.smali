.class public Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:Landroid/view/LayoutInflater;

.field b:I

.field final synthetic c:Lcom/sec/android/app/samsungapps/view/BannerListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/view/BannerListView;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->c:Lcom/sec/android/app/samsungapps/view/BannerListView;

    .line 159
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->b:I

    .line 160
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->a:Landroid/view/LayoutInflater;

    .line 161
    iput p3, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->b:I

    .line 162
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 166
    .line 167
    if-nez p2, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 171
    :cond_0
    const v0, 0x7f0c02ff

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->c:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/BannerListView;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->c:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->c:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;->c:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 178
    :cond_1
    return-object p2
.end method
