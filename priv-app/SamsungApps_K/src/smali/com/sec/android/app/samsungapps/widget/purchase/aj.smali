.class final Lcom/sec/android/app/samsungapps/widget/purchase/aj;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 4

    .prologue
    .line 84
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->a(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0099

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 87
    if-eqz v0, :cond_1

    .line 89
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->b(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;->getRequiredCodeLength()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->c(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->setSummary(Ljava/lang/String;)V

    .line 99
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->b(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;->setCode(Ljava/lang/String;)V

    .line 102
    :cond_1
    return-void

    .line 96
    :cond_2
    const-string v1, "[0-9]"

    const-string v2, "*"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;->a:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->setSummary(Ljava/lang/String;)V

    goto :goto_0
.end method
