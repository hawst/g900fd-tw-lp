.class final Lcom/sec/android/app/samsungapps/vlibrary/doc/q;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field a:I

.field final synthetic b:Z

.field final synthetic c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Z)V
    .locals 1

    .prologue
    .line 445
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->RequestCount:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->a:I

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bLoading:Z

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->RequestCount:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->a:I

    if-eq v0, v1, :cond_0

    .line 460
    :goto_0
    return-void

    .line 455
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->b:Z

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyFinishGettingMoreData(Z)V

    goto :goto_0

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataGetCompleted(Z)V

    goto :goto_0
.end method
