.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;->onCommentSaveResult(Z)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$700(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;Z)V

    .line 209
    if-eqz p2, :cond_0

    .line 211
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyCommentChanged(Ljava/lang/String;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->getProhibitWords()[Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->setProhibitWords([Ljava/lang/String;)V

    .line 215
    return-void
.end method
