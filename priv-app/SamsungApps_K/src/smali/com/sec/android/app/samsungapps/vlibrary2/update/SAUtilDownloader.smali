.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static mCheckSystemApp:I


# instance fields
.field private _DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

.field iRequestFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

.field private mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

.field private mContext:Landroid/content/Context;

.field private mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field private mSAUtilDownloadResultListener:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;

.field resultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

.field wrapper:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mCheckSystemApp:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->resultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->resultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->wrapper:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mSAUtilDownloadResultListener:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mContext:Landroid/content/Context;

    .line 44
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->onDownloadExSuccess(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->onFileDownloadingResult(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->amISystemApp()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getConvertedFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private amISystemApp()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 207
    sget v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mCheckSystemApp:I

    if-nez v1, :cond_0

    .line 208
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 209
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    sput v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mCheckSystemApp:I

    .line 215
    :cond_0
    :goto_0
    sget v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mCheckSystemApp:I

    if-ne v1, v0, :cond_2

    .line 218
    :goto_1
    return v0

    .line 212
    :cond_1
    const/4 v1, 0x2

    sput v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mCheckSystemApp:I

    goto :goto_0

    .line 218
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getConvertedFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->wrapper:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->productID()Ljava/lang/String;

    move-result-object v0

    .line 180
    if-nez v0, :cond_0

    .line 181
    const-string v0, ""

    .line 183
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "samsungapps-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getPackageFileName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getPackageFileName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getPackageFileName()Ljava/lang/String;

    move-result-object v0

    .line 191
    :cond_1
    return-object v0
.end method

.method private getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getContentSize()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    return-object v0
.end method

.method private notifyInstalling()V
    .locals 3

    .prologue
    .line 222
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->amISystemApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadState(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)Z

    .line 226
    :cond_0
    return-void
.end method

.method private onDownloadExSuccess(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setCancellable(Z)V

    .line 89
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 91
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    const-string v1, "com.alipay.android.app"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->onAlipayInstalled()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 92
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->createRequestFile(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/Request;

    move-result-object v0

    .line 93
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 94
    return-void
.end method

.method private onFileDownloadingResult(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 230
    if-eqz p1, :cond_2

    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->amISystemApp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->removeNotification()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getConvertedFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    .line 271
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    .line 273
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->amISystemApp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->notifyInstalling()V

    .line 282
    :cond_1
    :goto_0
    return-void

    .line 280
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private removeNotification()V
    .locals 2

    .prologue
    .line 196
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 201
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private requestDownload()V
    .locals 4

    .prologue
    .line 62
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mAppToDownload:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->getGUID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->resultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/update/f;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->downloadEx2(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 76
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 77
    return-void
.end method


# virtual methods
.method protected createRequestFile(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/Request;
    .locals 5

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->wrapper:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->contentsSize()Ljava/lang/String;

    move-result-object v0

    .line 144
    if-nez v0, :cond_0

    .line 145
    const-string v0, "0"

    .line 147
    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 148
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;

    invoke-direct {v2, p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;J)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->iRequestFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    .line 149
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->iRequestFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->iRequestFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/update/h;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V

    invoke-direct {v2, v3, p1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;)V

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;Landroid/content/Context;)V

    .line 163
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 170
    return-object v0
.end method

.method public downloadAndInstall()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->requestDownload()V

    .line 54
    return-void
.end method

.method protected installCompleted(Z)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mSAUtilDownloadResultListener:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;->onResult(Z)V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->mSAUtilDownloadResultListener:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;

    .line 100
    return-void
.end method

.method protected onAlipayInstalled()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V

    return-object v0
.end method
