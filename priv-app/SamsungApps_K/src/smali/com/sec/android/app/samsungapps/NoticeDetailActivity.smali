.class public Lcom/sec/android/app/samsungapps/NoticeDetailActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 104
    const v0, 0x7f0c01c0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 105
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 108
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public noticeDetailLoadingCompleted(Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;)V

    .line 67
    :cond_0
    const v0, 0x7f0c01c0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->getNoticeDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->setVisibleEmpty(Z)Z

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c01c1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c01c2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c01c4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-nez v2, :cond_2

    :cond_1
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->setVisibleEmpty(Z)Z

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->getNoticeDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->getNoticeTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->getNoticeDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->getNoticeDate()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->getNoticeDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->getNoticeDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->setVisibleEmpty(Z)Z

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    if-eqz v0, :cond_0

    .line 144
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;)V

    .line 150
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    .line 153
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 154
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v1, 0x7f040079

    .line 34
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->setMainViewAndEmptyView(I)V

    .line 41
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->NOTICE_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeDetailQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->setNoticeID(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->loadNoticeDetail()V

    .line 44
    return-void

    .line 39
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->setMainView(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    if-eqz v0, :cond_0

    .line 128
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;)V

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    .line 137
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 138
    return-void
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 119
    const v0, 0x7f0c0004

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 121
    const/4 v0, 0x1

    return v0
.end method
