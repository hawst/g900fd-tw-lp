.class public Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->a:Landroid/os/Handler;

    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->b:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->c:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->d:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 28
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/d;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 39
    return-void
.end method


# virtual methods
.method public done(I)V
    .locals 1

    .prologue
    .line 107
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->f:I

    .line 108
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->CONFIRM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)V

    .line 109
    return-void
.end method

.method public execute()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)V

    .line 54
    return-void
.end method

.method public failed()V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)V

    .line 114
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->b:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public invokeCompleted()V
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)V

    .line 103
    return-void
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;)V
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 61
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->d:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->c:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;->onConfirmNameAgeResult(Z)V

    goto :goto_0

    .line 67
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getRealAgeInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->f:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->setAge(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;->onConfirmNameAgeResult(Z)V

    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyNameAgeVerified()V

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;

    .line 75
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->b:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    .line 44
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;)V

    return-void
.end method
