.class final Lcom/sec/android/app/samsungapps/vlibrary/doc/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 154
    if-eqz p2, :cond_2

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->save()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->notifyLoginSuccess(Z)V

    .line 193
    :goto_0
    return-void

    .line 160
    :cond_2
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->SVRError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    if-ne v0, v1, :cond_0

    .line 162
    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3012"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mSAChecker:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->access$100(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;->isExistSamsungAccount()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setBirthDay(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setBirthMonth(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    const-string v1, "1990"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setBirthYear(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->signUp(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    goto :goto_0

    .line 181
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->signUp(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    goto :goto_0
.end method
