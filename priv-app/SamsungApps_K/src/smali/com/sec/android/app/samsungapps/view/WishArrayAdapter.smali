.class public Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.source "ProGuard"


# instance fields
.field private g:I

.field private h:I

.field private i:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private j:Lcom/sec/android/app/samsungapps/WishListActivity;

.field private k:Landroid/content/Context;

.field private l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    const v0, 0x7f0400b9

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 26
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->g:I

    .line 27
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    .line 56
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->k:Landroid/content/Context;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;)Lcom/sec/android/app/samsungapps/LoadingDialog;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->i:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;)Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;)I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    return v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;)Lcom/sec/android/app/samsungapps/WishListActivity;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    return-object v0
.end method


# virtual methods
.method public cancelDeletion()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->g:I

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->notifyDataSetChanged()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    .line 110
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->cancelDeletion()V

    .line 113
    :cond_0
    return-void
.end method

.method public contentListGetCompleted(Z)V
    .locals 3

    .prologue
    const v2, 0xa0010

    .line 159
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->contentListGetCompleted(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    check-cast v0, Lcom/sec/android/app/samsungapps/WishListActivity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/WishListActivity;

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 9

    .prologue
    .line 98
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->k:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->mListType:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 99
    return-object v0
.end method

.method public deleteSelectedItems()V
    .locals 3

    .prologue
    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->g:I

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    .line 121
    if-eqz v0, :cond_0

    .line 123
    new-instance v1, Lcom/sec/android/app/samsungapps/LoadingDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->k:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->i:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->i:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 125
    new-instance v1, Lcom/sec/android/app/samsungapps/view/bg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/bg;-><init>(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->k:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->excuteDeletion(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V

    .line 142
    :cond_0
    return-void
.end method

.method public enterDeletionMode(I)V
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->g:I

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->notifyDataSetChanged()V

    .line 93
    return-void
.end method

.method public finishGettingMoreContent(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 180
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->finishGettingMoreContent(Z)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    check-cast v0, Lcom/sec/android/app/samsungapps/WishListActivity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    if-nez v0, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->isSelectAll()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v2

    .line 188
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 189
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 190
    if-eqz v0, :cond_2

    .line 191
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    .line 188
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/WishListActivity;->updateItemClick(I)V

    .line 195
    iput v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->isCheckAllButtonChecked()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->setSelectAll(Z)V

    goto :goto_0

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->updateItemClick(I)V

    goto :goto_0
.end method

.method public getSelectedWishItemCount()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 278
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 279
    if-eqz v1, :cond_0

    .line 280
    const v0, 0x7f0c0056

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 281
    if-eqz v2, :cond_0

    .line 282
    const v0, 0x7f0200e9

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 283
    new-instance v0, Lcom/sec/android/app/samsungapps/view/bh;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/bh;-><init>(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    .line 308
    :cond_0
    :goto_0
    return-object v1

    .line 297
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 299
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 300
    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 302
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0
.end method

.method public isCheckDeletionMode()Z
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->g:I

    packed-switch v0, :pswitch_data_0

    .line 69
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    .line 65
    :pswitch_0
    const/4 v0, 0x1

    .line 66
    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isListItemMode()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->g:I

    return v0
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    return v0
.end method

.method public onItemClick(Landroid/view/View;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 210
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    check-cast v1, Lcom/sec/android/app/samsungapps/WishListActivity;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    .line 216
    const v1, 0x7f0c0293

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 217
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->toggle()V

    .line 219
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    .line 221
    const v0, 0x7f0c0058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 222
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    .line 223
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->updateItemClick(I)V

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getSelectedWishItemCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/WishListActivity;->deletionModeActionItemEnable(Z)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/WishListActivity;->setCheckAllLayoutChecked(Z)V

    .line 243
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->setUpPopupMenu(I)V

    .line 249
    :cond_0
    :goto_2
    return-void

    .line 225
    :cond_1
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    .line 226
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    .line 227
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 236
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getSelectedWishItemCount()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->setCheckAllLayoutChecked(Z)V

    .line 241
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_1

    .line 239
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/WishListActivity;->setCheckAllLayoutChecked(Z)V

    goto :goto_3

    .line 245
    :cond_4
    const-string v1, "N"

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->forSaleYn:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showContentDetailActivity(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    goto :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public onItemLongClick(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    check-cast v0, Lcom/sec/android/app/samsungapps/WishListActivity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    .line 255
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 256
    iput v4, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    .line 257
    const v1, 0x7f0c0293

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    iget v3, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->updateItemClick(I)V

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->onPositive(I)V

    .line 264
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    .line 265
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 266
    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/WishListActivity;->deletionModeActionItemEnable(Z)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->setUpPopupMenu(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/WishListActivity;->setBeforeSelectedItem(Landroid/view/View;I)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->j:Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->setCheckedAll()V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    .line 154
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 155
    return-void
.end method

.method public setLoadListHandler(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->l:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;

    .line 51
    return-void
.end method

.method public setSelectedWishItemCount(I)V
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->h:I

    .line 45
    return-void
.end method

.method protected showToastMessage()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 148
    return-void
.end method
