.class public Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    .line 530
    return-void
.end method


# virtual methods
.method public createCreditCardMenu()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080325

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 632
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v2, 0x2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/ad;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/ad;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080270

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public createCreditCardRegisterMenu()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 587
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->ableToUseGlobalCreditCard()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    if-nez v1, :cond_2

    :goto_1
    if-nez v0, :cond_3

    .line 588
    const/4 v0, 0x0

    .line 601
    :goto_2
    return-object v0

    .line 587
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 591
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->j(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080325

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 601
    :goto_3
    new-instance v1, Lcom/sec/android/app/samsungapps/MenuItem;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/ac;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/ac;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    goto :goto_2

    .line 598
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080270

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public createGiftCardMenu()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 534
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080187

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/z;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/z;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public createMyVoucherMenu()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 556
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080226

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/ab;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/ab;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public createVoucherMenu()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 545
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080188

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/aa;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/aa;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method
