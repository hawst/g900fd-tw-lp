.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 9
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    return-void
.end method

.method public static countrySearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;
    .locals 3

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;

    const-string v1, "countrySearch"

    const-string v2, "2200"

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 16
    const-string v1, "latestCountryCode"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v1, "mnc"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v1, "csc"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-object v0
.end method

.method public static countrySearchEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;

    const-string v1, "countrySearchEx"

    const-string v2, "2300"

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 25
    const-string v1, "latestCountryCode"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v1, "whoAmI"

    const-string v2, "odc"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    return-object v0
.end method


# virtual methods
.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->_IConnectURLGetter:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;->getHubURL()Ljava/lang/String;

    move-result-object v0

    .line 34
    return-object v0
.end method
