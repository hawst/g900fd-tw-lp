.class public Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;
.super Landroid/app/Service;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadService$Stub;

.field b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/downloadservice/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/downloadservice/a;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadService$Stub;

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadservice/d;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/downloadservice/d;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 2

    .prologue
    .line 23
    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1"

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;->upgrade:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    invoke-static {p3}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b(Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Ljava/lang/String;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 4

    .prologue
    .line 23
    const-string v0, "com.sec.android.app.billing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/downloadservice/e;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/downloadservice/e;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->setObserver(Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->send()V

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b(Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 0

    .prologue
    .line 23
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->b(Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 3

    .prologue
    .line 140
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v0

    .line 141
    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, v1}, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    .line 142
    new-instance v1, Lcom/sec/android/app/samsungapps/downloadservice/f;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/downloadservice/f;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V

    .line 202
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->execute()V

    .line 203
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;)Z
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/api/SChecker;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/api/SChecker;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/api/SChecker;->checkSignatureForBind()Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 1

    .prologue
    .line 126
    :try_start_0
    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;->onDownloadFailed()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadService$Stub;

    return-object v0
.end method
