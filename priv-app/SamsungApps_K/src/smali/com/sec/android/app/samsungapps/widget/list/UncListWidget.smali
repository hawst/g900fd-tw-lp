.class public Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;
.super Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;


# instance fields
.field b:Landroid/widget/AdapterView$OnItemClickListener;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

.field private e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

.field private f:Landroid/content/Context;

.field private g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;)V

    .line 216
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/ap;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/ap;-><init>(Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->b:Landroid/widget/AdapterView$OnItemClickListener;

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->a(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 216
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/ap;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/ap;-><init>(Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->b:Landroid/widget/AdapterView$OnItemClickListener;

    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->a(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 216
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/ap;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/ap;-><init>(Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->b:Landroid/widget/AdapterView$OnItemClickListener;

    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->a(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;)Lcom/sec/android/app/samsungapps/view/UncListAdapter;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->f:Landroid/content/Context;

    .line 49
    const v0, 0x7f040012

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->initView(Landroid/content/Context;I)V

    .line 50
    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->mListView:Landroid/widget/AbsListView;

    .line 52
    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 53
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/ao;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/ao;-><init>(Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->f:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getAdapter()Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/view/UncListAdapter;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    return-object v0
.end method

.method public getContentList()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->getUncList()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 190
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    :cond_0
    return-object v1
.end method

.method public loadMoreComplete()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;->onDataLoadCompleted()V

    .line 123
    :cond_0
    return-void
.end method

.method public loadWidget()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->getUncList()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->requestMore()V

    .line 97
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->loadWidget()V

    .line 98
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->start()V

    goto :goto_0
.end method

.method public loadingMore()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;->onDataLoadingMore()V

    .line 131
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->release()V

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->release()V

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    .line 85
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->release()V

    .line 86
    return-void
.end method

.method public requestList()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->start()V

    .line 203
    return-void
.end method

.method public requestListMore()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->requestMore()V

    .line 198
    return-void
.end method

.method public setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    .line 116
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 139
    sget-object v0, Lcom/sec/android/app/samsungapps/widget/list/aq;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 177
    :goto_0
    return-void

    .line 141
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 145
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetViewState(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->clear()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getContentList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->getUncList()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getTotalCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->setListCount(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->updateView()V

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getScrollState()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->loadMoreComplete()V

    .line 154
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetMoreLoading(Z)V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->getUncList()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getTotalCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->updateWidget()V

    goto :goto_0

    .line 166
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->loadingMore()V

    .line 167
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetMoreLoading(Z)V

    goto :goto_0

    .line 171
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getScrollState()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 172
    const v0, 0x7f0c007b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 173
    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 175
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetViewState(I)V

    goto/16 :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 68
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->createUncList(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->setListRequest(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/CommonAdapter;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->mListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->b:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    return-void
.end method

.method public updateList(Z)V
    .locals 3

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetViewState(I)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->clear()V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getContentList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->getUncList()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getTotalCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->setListCount(I)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->e:Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->updateView()V

    .line 214
    return-void
.end method

.method public updateWidget()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;->onDataLoadCompleted()V

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getContentList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 109
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/UncListWidget;->onWidgetViewState(I)V

    .line 111
    :cond_1
    return-void
.end method
