.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final SETTING_AUTO_UPDATE_ALWAYS:I = 0x2

.field public static final SETTING_AUTO_UPDATE_DISABLE:I = 0x0

.field public static final SETTING_AUTO_UPDATE_WIFI_ONLY:I = 0x1


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->b:Z

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->a:Landroid/content/Context;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->b:Z

    .line 24
    return-void
.end method


# virtual methods
.method public checkAutoUpdSettingVisible()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->b:Z

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    .line 31
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSetting()I
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 41
    const-string v1, "update_main_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 44
    :cond_0
    const-string v0, "1"

    .line 46
    :cond_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 53
    :goto_0
    return v0

    .line 51
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z
    .locals 3

    .prologue
    .line 58
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_2

    .line 60
    :cond_0
    if-eqz p2, :cond_1

    .line 62
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;->onSettingChangeFailed()V

    .line 64
    :cond_1
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    .line 66
    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 67
    const-string v1, "update_main_setting"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 68
    const-string v0, "AutoUpdateMainSetting"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setSetting:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v0, 0x1

    goto :goto_0
.end method
