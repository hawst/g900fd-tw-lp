.class public Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field private c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->b:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    .line 178
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a:Landroid/os/Handler;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->d:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->c:Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 30
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 153
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DELETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->d:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 158
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)Z

    .line 51
    return-void
.end method


# virtual methods
.method public delete()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 46
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->b:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;)V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 68
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isDeletableApp(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELPACKAGE_IS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELPACKAGE_IS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0

    .line 74
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->f:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->f:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;->onDeleteFailed()V

    goto :goto_0

    .line 77
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->e:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->doIHaveDeletePackagePermission()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 79
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->setOnDeletePackage(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->uninstallPackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    :goto_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    .line 82
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a()V

    goto :goto_0

    .line 86
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a()V

    goto :goto_0

    .line 89
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->f:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->f:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;->onDeleteSuccess()V

    goto/16 :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->f:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;

    .line 35
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->b:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    .line 56
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;)V

    return-void
.end method
