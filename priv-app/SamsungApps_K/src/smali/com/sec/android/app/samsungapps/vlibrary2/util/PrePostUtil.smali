.class public Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mPostloadCount:I

.field private mPostloadapp:Ljava/lang/String;

.field private mPreloadApp:Ljava/lang/String;

.field private mPreloadCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPrePostPackageInfo()Ljava/util/Vector;

    move-result-object v1

    .line 27
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPreloadCount:I

    .line 28
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPostloadCount:I

    .line 29
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPreloadApp:Ljava/lang/String;

    .line 30
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPostloadapp:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getPostLoadApp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPostloadapp:Ljava/lang/String;

    return-object v0
.end method

.method public getPostLoadCount()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPostloadCount:I

    return v0
.end method

.method public getPreLoadApp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPreloadApp:Ljava/lang/String;

    return-object v0
.end method

.method public getPreLoadCount()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->mPreloadCount:I

    return v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
