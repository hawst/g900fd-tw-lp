.class public Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

.field b:Z

.field c:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

.field public checkBuilder:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

.field private d:Landroid/content/Context;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->b:Z

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->e:Z

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    .line 59
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->checkBuilder:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Z)Z
    .locals 0

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->e:Z

    return v0
.end method


# virtual methods
.method public askUserUpdateODC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->c:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->c:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    return-object v0
.end method

.method protected getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v0

    return-object v0
.end method

.method protected getODCUpdateViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/s;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/s;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    return-object v0
.end method

.method public isDontSelftUpdateCondition()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v2

    .line 386
    sget-object v3, Lcom/sec/android/app/samsungapps/commands/w;->a:[I

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->getForceUpdate()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 405
    sget-boolean v0, Lcom/sec/android/app/samsungapps/Main;->_DeepLinkMode:Z

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 393
    :pswitch_1
    sget-boolean v2, Lcom/sec/android/app/samsungapps/Main;->_DeepLinkMode:Z

    if-eqz v2, :cond_0

    move v0, v1

    .line 394
    goto :goto_0

    .line 400
    :pswitch_2
    sget-boolean v2, Lcom/sec/android/app/samsungapps/Main;->_DeepLinkMode:Z

    if-eqz v2, :cond_0

    move v0, v1

    .line 401
    goto :goto_0

    .line 386
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public notifyChangedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/y;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/y;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    return-object v0
.end method

.method public notifyCheckAppUpgradeFinished()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/aa;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/aa;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    return-object v0
.end method

.method public notifyLimitedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/z;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/z;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    return-object v0
.end method

.method public onDialogConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->setDialogBody()V

    .line 153
    return-void
.end method

.method public setDialogBody()V
    .locals 11

    .prologue
    const v10, 0x7f0c0038

    const v9, 0x7f0801ea

    const/4 v8, -0x1

    const/4 v7, 0x1

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v2

    .line 157
    const v0, 0x7f0c0050

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 158
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 160
    const v3, 0x7f0400d8

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 162
    if-nez v3, :cond_0

    .line 364
    :goto_0
    return-void

    .line 166
    :cond_0
    const v1, 0x7f0c0055

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 167
    if-eqz v1, :cond_1

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    const-string v5, "isa_samsungapps_icon"

    const-string v6, "drawable"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 171
    :cond_1
    invoke-virtual {v0, v3, v8, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 173
    const v0, 0x7f0c02e3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 174
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    const v4, 0x7f080127

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :goto_1
    const v0, 0x7f0c02e1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 189
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(I)V

    .line 191
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ab;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/samsungapps/commands/ab;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnConfigurationChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;)V

    .line 217
    const v0, 0x7f0c02e2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    const v3, 0x7f080160

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->checkBuilder:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnConfigurationChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->c:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setICommand(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;)V

    .line 222
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dontDismissWhenClickPositive()V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    const v3, 0x7f0802f3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/samsungapps/commands/ac;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/commands/ac;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 262
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/af;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/af;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v1, 0x7f0c015d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v1, 0x7f0c009e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v1, 0x7f0c009f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 288
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    invoke-direct {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 289
    const-string v1, "update_self_setting"

    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-ne v4, v7, :cond_3

    .line 291
    :cond_2
    const-string v1, "0"

    .line 294
    :cond_3
    const-string v4, "1"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v7, :cond_4

    .line 295
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 299
    :cond_4
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/ag;

    invoke-direct {v1, p0, v0, v3}, Lcom/sec/android/app/samsungapps/commands/ag;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Landroid/widget/CheckBox;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->checkBuilder:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->isDontSelftUpdateCondition()Z

    move-result v0

    if-ne v0, v7, :cond_6

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v1, 0x7f0c02e5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 322
    :goto_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 323
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/t;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/commands/t;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 179
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    const v4, 0x7f0801a7

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v1, 0x7f0c02e4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method public showDialogToRestart(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 88
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const v1, 0x7f08027d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const v1, 0x7f0802ef

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/x;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/commands/x;-><init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 114
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 115
    return-void
.end method

.method public validateDisclaimerCommand(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;->setNewDisclaimerVersion(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;->validateDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method
