.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/l;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/k;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/k;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 2

    .prologue
    .line 46
    if-eqz p1, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/k;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V

    .line 49
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getAccountEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->loginSuccess()V

    .line 62
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/k;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;->isTokenExpired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/k;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onRequestToken()V

    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/k;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V

    goto :goto_0
.end method
