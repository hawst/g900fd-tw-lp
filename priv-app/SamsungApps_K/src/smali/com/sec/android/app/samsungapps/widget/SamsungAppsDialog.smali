.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
.super Landroid/app/Dialog;
.source "ProGuard"


# instance fields
.field a:Z

.field private b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field private c:Landroid/content/Context;

.field private d:Z

.field private e:Landroid/widget/ListView;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

.field private j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

.field private k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

.field private l:Landroid/content/DialogInterface$OnKeyListener;

.field private m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

.field private n:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

.field private o:Z

.field private p:I

.field private q:Landroid/content/BroadcastReceiver;

.field private final r:I

.field private s:Z

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f:Ljava/lang/Object;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->h:I

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->l:Landroid/content/DialogInterface$OnKeyListener;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

    .line 56
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->o:Z

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/ab;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/ab;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->q:Landroid/content/BroadcastReceiver;

    .line 73
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->r:I

    .line 932
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->t:I

    .line 933
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->u:I

    .line 965
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a:Z

    .line 80
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    .line 82
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->requestWindowFeature(I)Z

    .line 84
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    .line 86
    if-ne v0, v2, :cond_0

    .line 88
    const v0, 0x7f04000b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setContentView(I)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    const v0, 0x7f04000a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setContentView(I)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f:Ljava/lang/Object;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->h:I

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->l:Landroid/content/DialogInterface$OnKeyListener;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

    .line 56
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->o:Z

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/ab;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/ab;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->q:Landroid/content/BroadcastReceiver;

    .line 73
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->r:I

    .line 932
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->t:I

    .line 933
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->u:I

    .line 965
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a:Z

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    .line 103
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->requestWindowFeature(I)Z

    .line 104
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setContentView(I)V

    .line 105
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 951
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 952
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->t:I

    mul-int/lit8 v0, v0, 0x41

    div-int/lit8 v0, v0, 0x64

    .line 957
    :goto_0
    return v0

    .line 954
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->u:I

    mul-int/lit8 v0, v0, 0x5f

    div-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a()Landroid/content/DialogInterface$OnKeyListener;
    .locals 1

    .prologue
    .line 622
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/af;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/af;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    return-object v0
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 659
    const v1, 0x7f0c015d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 662
    const v2, 0x7f0c015c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 663
    if-eqz v1, :cond_0

    if-nez v2, :cond_2

    .line 665
    :cond_0
    const-string v1, "SamsungAppsDialog::_onBackKey::Button is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 692
    :cond_1
    :goto_0
    return v0

    .line 672
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 678
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    if-eqz v0, :cond_4

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v2, -0x2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;->onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V

    .line 684
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    :cond_4
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 686
    :catch_0
    move-exception v0

    .line 688
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAppsDialog::_onBackKey::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getPositiveButtonTitle()Ljava/lang/String;

    move-result-object v0

    .line 701
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getNegativeButtonTitle()Ljava/lang/String;

    move-result-object v1

    .line 703
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v2

    .line 704
    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 706
    const v2, 0x7f04000f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setContentView(I)V

    .line 713
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 715
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 718
    :cond_0
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->h:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 720
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->h:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setIcon(I)V

    .line 723
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 725
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 728
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 732
    :cond_3
    return-void

    .line 710
    :cond_4
    const v2, 0x7f04000e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setContentView(I)V

    goto :goto_0
.end method

.method private d()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 740
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/ag;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/ag;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method private e()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 759
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/ah;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/ah;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    return-object v0
.end method

.method private f()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 795
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/ai;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/ai;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->s:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 111
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d:Z

    .line 113
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 114
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f:Ljava/lang/Object;

    .line 115
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->h:I

    .line 117
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 118
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 119
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 120
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->l:Landroid/content/DialogInterface$OnKeyListener;

    .line 123
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->q:Landroid/content/BroadcastReceiver;

    .line 130
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

    .line 132
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 133
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAppsDialog::dismiss::IllegalArgumentException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dontDismissWhenClickPositive()V
    .locals 1

    .prologue
    .line 786
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->s:Z

    .line 787
    return-void
.end method

.method public findView(I)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 517
    .line 518
    const v0, 0x7f0c004f

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 519
    if-eqz v0, :cond_0

    .line 520
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 528
    :goto_0
    return-object v0

    .line 523
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    move-object v0, v1

    .line 528
    goto :goto_0

    .line 525
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public getButton(I)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    .line 453
    packed-switch p1, :pswitch_data_0

    .line 466
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 468
    return-object v0

    .line 458
    :pswitch_0
    const v0, 0x7f0c015d

    .line 459
    goto :goto_0

    .line 462
    :pswitch_1
    const v0, 0x7f0c015c

    goto :goto_0

    .line 453
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getIcon()I
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->h:I

    return v0
.end method

.method public getNegativeButtonTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 554
    const-string v1, ""

    .line 556
    const v0, 0x7f0c015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 557
    if-eqz v0, :cond_1

    .line 559
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 562
    :goto_0
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getPositiveButtonTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 537
    const-string v1, ""

    .line 539
    const v0, 0x7f0c015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 540
    if-eqz v0, :cond_1

    .line 542
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 545
    :goto_0
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 504
    .line 506
    const v0, 0x7f0c004f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 507
    if-eqz v0, :cond_1

    .line 509
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-le v2, v3, :cond_0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 512
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 509
    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 573
    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    .line 575
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 893
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

    if-eqz v0, :cond_0

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;->onDialogConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 897
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAppsDialog::onConfigurationChanged::"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 900
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->o:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 901
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->p:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    .line 902
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 903
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 904
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a(I)I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 905
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 906
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->p:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 913
    :cond_1
    :goto_0
    return-void

    .line 909
    :catch_0
    move-exception v0

    .line 910
    const-string v1, "SamsungAppsDialog::onConfigurationChanged::IllegalArgumentException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 911
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/16 v3, 0x8

    .line 820
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 823
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    .line 826
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->p:I

    .line 827
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->p:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->t:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->u:I

    .line 829
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 830
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->o:Z

    if-eqz v1, :cond_6

    .line 831
    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setLayout(II)V

    .line 835
    :goto_1
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 836
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 840
    const v0, 0x7f0c015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 841
    const v1, 0x7f0c015c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 842
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 844
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_3

    .line 847
    :cond_2
    const v2, 0x7f0c0142

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 848
    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 852
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 855
    const v0, 0x7f0c0051

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 856
    if-eqz v0, :cond_4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 861
    :cond_4
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 862
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 863
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 864
    return-void

    .line 827
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->u:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->t:I

    goto/16 :goto_0

    .line 833
    :cond_6
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->p:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a(I)I

    move-result v1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    goto/16 :goto_1
.end method

.method public onFinalResult(Z)V
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->n:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->n:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->sendFinalResult(Z)V

    .line 930
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 973
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a:Z

    if-nez v0, :cond_0

    .line 975
    const/4 v0, 0x1

    .line 978
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Dialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->l:Landroid/content/DialogInterface$OnKeyListener;

    .line 612
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 613
    return-void
.end method

.method public setCancelable(Z)V
    .locals 0

    .prologue
    .line 442
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d:Z

    .line 443
    return-void
.end method

.method public setDisableTouchFromOutside()V
    .locals 1

    .prologue
    .line 968
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a:Z

    .line 969
    return-void
.end method

.method public setFullLayout()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->o:Z

    .line 98
    return-void
.end method

.method public setICommand(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;)V
    .locals 0

    .prologue
    .line 922
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->n:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;

    .line 923
    return-void
.end method

.method public setIcon(I)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 165
    const v0, 0x7f0c0050

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    :cond_0
    return-void
.end method

.method public setMultiChoiceItems([Ljava/lang/String;[ZLcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 283
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 286
    const v2, 0x7f040013

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const v4, 0x7f040076

    invoke-direct {v2, v3, v4, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    move v0, v1

    .line 293
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    aget-boolean v2, p2, v0

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    :cond_0
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 301
    :cond_1
    return-void
.end method

.method public setNegativeButton(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 342
    const v0, 0x7f0c015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 343
    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 346
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 349
    :cond_0
    return-void
.end method

.method public setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 2

    .prologue
    .line 381
    const v0, 0x7f0c015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 382
    if-eqz v0, :cond_0

    .line 384
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 385
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 386
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 388
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/ae;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/ae;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    .line 395
    :cond_0
    return-void
.end method

.method public setOnConfigurationChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;)V
    .locals 0

    .prologue
    .line 884
    if-nez p1, :cond_0

    .line 889
    :goto_0
    return-void

    .line 888
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;

    goto :goto_0
.end method

.method public setPositiveButton(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 309
    const v0, 0x7f0c015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 310
    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 313
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 316
    :cond_0
    return-void
.end method

.method public setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 2

    .prologue
    .line 358
    const v0, 0x7f0c015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 359
    if-eqz v0, :cond_0

    .line 361
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->i:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 362
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 363
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 365
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/ad;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/ad;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    .line 372
    :cond_0
    return-void
.end method

.method public setPositveButtonDisable()V
    .locals 2

    .prologue
    .line 320
    const v0, 0x7f0c015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 321
    if-eqz v0, :cond_0

    .line 323
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 325
    :cond_0
    return-void
.end method

.method public setPositveButtonEnable()V
    .locals 2

    .prologue
    .line 329
    const v0, 0x7f0c015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 330
    if-eqz v0, :cond_0

    .line 332
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 334
    :cond_0
    return-void
.end method

.method public setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c()V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 212
    const v1, 0x7f040013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 220
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 224
    :cond_0
    return-void
.end method

.method public setSingleChoiceItems(Ljava/util/ArrayList;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c()V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 264
    const v1, 0x7f040013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 265
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const v2, 0x7f04001d

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setSelection(I)V

    .line 274
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 278
    :cond_0
    return-void
.end method

.method public setSingleChoiceItems([Ljava/lang/String;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c()V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 237
    const v1, 0x7f040013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->d()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    const v3, 0x7f04001f

    invoke-direct {v1, v2, v3, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setSelection(I)V

    .line 246
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->e:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 250
    :cond_0
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->f:Ljava/lang/Object;

    .line 142
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 178
    if-nez p1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    const v0, 0x7f0c004d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 184
    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->g:Ljava/lang/String;

    .line 187
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 189
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/ac;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/ac;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setView(I)V
    .locals 5

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 419
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 420
    if-nez v1, :cond_1

    .line 422
    const-string v0, "setView::layout not found"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    const v0, 0x7f0c004f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 428
    const v2, 0x7f0c0050

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 429
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 431
    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-virtual {v0, v1, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 432
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setView(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 403
    const v0, 0x7f0c004f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 404
    const v1, 0x7f0c0050

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 405
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 407
    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-virtual {v0, p1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 408
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 410
    :cond_0
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 582
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->l:Landroid/content/DialogInterface$OnKeyListener;

    if-nez v0, :cond_0

    .line 584
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 587
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 593
    :goto_0
    return-void

    .line 591
    :catch_0
    move-exception v0

    const-string v0, "show::BadTokenException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showWithBadTokenException()V
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->l:Landroid/content/DialogInterface$OnKeyListener;

    if-nez v0, :cond_0

    .line 599
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->a()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 602
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 603
    return-void
.end method
