.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOptionQuery;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;


# instance fields
.field RequestCount:I

.field protected bLoading:Z

.field protected bSupportSortOption:Z

.field private mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

.field mContentListArray:Ljava/util/ArrayList;

.field private mImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

.field private mKeyword:Ljava/lang/String;

.field mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

.field protected mPaidTypeFilter:I

.field private mProductSetListID:Ljava/lang/String;

.field protected mQueryLogInputMethod:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field protected mSearchClickURL:Ljava/lang/String;

.field protected mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

.field protected type:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mKeyword:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mProductSetListID:Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 44
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 45
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mPaidTypeFilter:I

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bLoading:Z

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->iqry:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mQueryLogInputMethod:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSearchClickURL:Ljava/lang/String;

    .line 428
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->RequestCount:I

    .line 559
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 123
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->type:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 124
    return-void
.end method

.method public static createCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 3

    .prologue
    .line 166
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)V

    .line 167
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 168
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 169
    return-object v0
.end method

.method public static createContentCategoryList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 3

    .prologue
    .line 158
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;-><init>(Ljava/lang/String;)V

    .line 159
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 160
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 161
    return-object v0
.end method

.method public static createCuratedProductList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->CURATED_PRODUCTLIST:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 221
    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setProductSetList(Ljava/lang/String;)V

    .line 224
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 226
    return-object v0
.end method

.method public static createCustomerAlsoBought(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->customerAlsoBought:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 189
    iput-object p0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 190
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 191
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 192
    return-object v0
.end method

.method public static createEtc()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    return-object v0
.end method

.method public static createFeaturedHot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->FeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    return-object v0
.end method

.method public static createProductSetList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 3

    .prologue
    .line 209
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 210
    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setProductSetList(Ljava/lang/String;)V

    .line 212
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 213
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 215
    return-object v0
.end method

.method public static createSearch()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;-><init>()V

    .line 175
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestMatch:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 176
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    .line 177
    return-object v0
.end method

.method public static createTopAll()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    return-object v0
.end method

.method public static createTopFree()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    return-object v0
.end method

.method public static createTopNew()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    return-object v0
.end method

.method public static createTopPaid()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    return-object v0
.end method

.method public static createWishList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;-><init>()V

    .line 183
    return-object v0
.end method

.method private find()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->getno()I

    move-result v1

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getPaidTypeFilter()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->compare(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setProductSetList(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mProductSetListID:Ljava/lang/String;

    .line 231
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)V
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    .line 564
    if-ne v0, p1, :cond_0

    .line 569
    :goto_0
    return-void

    .line 568
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z
    .locals 1

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->doesSupportSortOption()Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    const/4 v0, 0x0

    .line 410
    :goto_0
    return v0

    .line 409
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)V

    .line 410
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    .line 291
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->clear()V

    goto :goto_0

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 294
    return-void
.end method

.method protected createContentListWithID()Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;
    .locals 4

    .prologue
    .line 273
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->getno()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getPaidTypeFilter()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;II)V

    return-object v0
.end method

.method protected createRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 470
    .line 471
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/t;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getType()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 511
    :goto_0
    if-nez v0, :cond_0

    move-object v0, v1

    .line 514
    :cond_0
    return-object v0

    .line 474
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->customerBoughtProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 477
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->search(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 480
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->categoryProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 483
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->featuredProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 486
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->allProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 489
    :pswitch_5
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->newProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 492
    :pswitch_6
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->paidProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 495
    :pswitch_7
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->freeProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 498
    :pswitch_8
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->purchaseList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0

    .line 501
    :pswitch_9
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getProductSetList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_a
    move-object v0, p0

    .line 504
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;->getContentCategoryID()Ljava/lang/String;

    move-result-object v0

    .line 505
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->contentCategoryProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto/16 :goto_0

    .line 508
    :pswitch_b
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mProductSetListID:Ljava/lang/String;

    invoke-virtual {v0, p0, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->curatedProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto/16 :goto_0

    .line 471
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method protected createRequestReceiver(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 0

    .prologue
    .line 443
    if-nez p2, :cond_0

    .line 445
    new-instance p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;

    invoke-direct {p2, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/q;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Z)V

    .line 465
    :cond_0
    return-object p2
.end method

.method public doesSupportSortOption()Z
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bSupportSortOption:Z

    return v0
.end method

.method public findContentById(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 298
    if-nez v0, :cond_0

    move-object v0, v1

    .line 307
    :goto_0
    return-object v0

    .line 301
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 303
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 307
    goto :goto_0
.end method

.method public getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createContentListWithID()Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 262
    :cond_0
    :goto_0
    return-object v0

    .line 255
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->find()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 256
    if-nez v0, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createContentListWithID()Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    goto :goto_0
.end method

.method protected getData(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 431
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->RequestCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->RequestCount:I

    .line 432
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bLoading:Z

    .line 434
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createRequestReceiver(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v0

    .line 436
    invoke-virtual {p0, v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 437
    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 438
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 439
    return-void
.end method

.method public getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    return-object v0
.end method

.method public getIndexOfLastItem()I
    .locals 1

    .prologue
    .line 584
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 585
    if-eqz v0, :cond_0

    .line 587
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getIndexOfLastItem()I

    move-result v0

    .line 589
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public final getLoadingItemCountInOnePage()I
    .locals 1

    .prologue
    .line 594
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 595
    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getContentsGettingCount()I

    move-result v0

    .line 599
    :goto_0
    return v0

    .line 598
    :cond_0
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 599
    const/16 v0, 0x19

    goto :goto_0
.end method

.method public getPaidTypeFilter()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mPaidTypeFilter:I

    return v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductSetListID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mProductSetListID:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryLogInputMethod()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mQueryLogInputMethod:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    return-object v0
.end method

.method public getSearchClickURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSearchClickURL:Ljava/lang/String;

    return-object v0
.end method

.method public getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    return-object v0
.end method

.method public getType()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->type:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    return-object v0
.end method

.method public initAllListFirstItem()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    .line 267
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setIndexOfFirstItem(I)V

    goto :goto_0

    .line 269
    :cond_0
    return-void
.end method

.method public isListEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 604
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    .line 605
    if-nez v1, :cond_1

    .line 608
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->bLoading:Z

    return v0
.end method

.method protected notifyDataGetCompleted(Z)V
    .locals 2

    .prologue
    .line 519
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/r;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/r;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 529
    return-void
.end method

.method protected notifyDataLoading()V
    .locals 2

    .prologue
    .line 533
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/s;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/s;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 544
    return-void
.end method

.method protected notifyFinishGettingMoreData(Z)V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    .line 556
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;->finishGettingMoreContent(Z)V

    goto :goto_0

    .line 558
    :cond_0
    return-void
.end method

.method protected notifyStartGettingMoreData()V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    .line 549
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;->startGettingMoreContent()V

    goto :goto_0

    .line 551
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)Z
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mObservers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final requestDataGet(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_0

    .line 343
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->neverLoaded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataLoading()V

    .line 346
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataGetCompleted(Z)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getData(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    goto :goto_0

    .line 358
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataLoading()V

    .line 359
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataGetCompleted(Z)V

    goto :goto_0
.end method

.method public requestDataGet(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->neverLoaded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataLoading()V

    .line 372
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/o;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/o;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getData(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    goto :goto_0

    .line 390
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataLoading()V

    .line 391
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyDataGetCompleted(Z)V

    .line 392
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/p;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/p;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    goto :goto_0
.end method

.method public requestMoreData(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 415
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->more:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->notifyStartGettingMoreData()V

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 418
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 424
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getData(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    .line 426
    :cond_1
    return-void
.end method

.method public setImageResolution(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;)V
    .locals 0

    .prologue
    .line 612
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    .line 613
    return-void
.end method

.method public setKeyword(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mKeyword:Ljava/lang/String;

    .line 318
    return-void
.end method

.method public setPaidTypeFilter(I)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mPaidTypeFilter:I

    .line 107
    return-void
.end method

.method public setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mQueryLogInputMethod:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 76
    return-void
.end method

.method public setSearchClickURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSearchClickURL:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 327
    return-void
.end method

.method public setType(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->type:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 245
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 246
    return-void
.end method
