.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 10
    const-string v2, "getProductSetList2Notc"

    const-string v3, "2238"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    const-string v0, "productSetID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "alignOrder"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->getLinkedContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v0, "startNum"

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const-string v0, "endNum"

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v0, "contentType"

    const-string v1, "application"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    return-void
.end method
