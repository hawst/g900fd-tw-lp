.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPaymentWrapper;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method


# virtual methods
.method protected getClassToBeOpened()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPaymentWrapper;

    return-object v0
.end method

.method public onRequestPurchase(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/ResultReceiver;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPaymentWrapper;->start(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public start(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPaymentWrapper;->getClassToBeOpened()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 20
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;-><init>()V

    .line 21
    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V

    .line 22
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 23
    return-void
.end method
