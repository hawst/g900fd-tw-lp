.class public Lcom/sec/android/app/samsungapps/CyberCashActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

.field c:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private d:Landroid/widget/Spinner;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 130
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CyberCashActivity;)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->requestPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/CyberCashActivity;)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->onBackPressed()V

    return-void
.end method


# virtual methods
.method public getCashID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0224

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCashPassword()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0226

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCouponIssuedSEQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    if-nez v0, :cond_0

    .line 120
    const-string v0, ""

    .line 122
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPaymentMethodID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getPaymentMethodID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentPrice()D
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getPaymentPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getProductGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->e:Z

    if-nez v0, :cond_0

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->e:Z

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->cancel()V

    .line 150
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 151
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->finish()V

    .line 64
    :goto_0
    return-void

    .line 33
    :cond_0
    const v0, 0x7f04009d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->setContentView(I)V

    .line 35
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v0, 0x7f08029a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0xb0003

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    .line 36
    const v0, 0x7f0802e6

    const v1, 0x7f08023d

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0222

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findSpinner(I)Landroid/widget/Spinner;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->d:Landroid/widget/Spinner;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->setSpinnerStringArrayAdapter(Landroid/content/Context;Landroid/widget/Spinner;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getCount()I

    move-result v3

    .line 44
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    .line 46
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/samsungapps/ActivityHelper;->createStringID(Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/ActivityHelper$StringID;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 49
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cr;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cr;-><init>(Lcom/sec/android/app/samsungapps/CyberCashActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cs;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cs;-><init>(Lcom/sec/android/app/samsungapps/CyberCashActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ActivityHelper;->release()V

    .line 88
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 89
    return-void
.end method

.method public onPaymentRequest()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 135
    return-void
.end method

.method public onPaymentResult(Z)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CyberCashActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CyberCashActivity;->finish()V

    .line 141
    return-void
.end method
