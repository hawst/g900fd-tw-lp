.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field protected mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->onFinalResult(Z)V

    return-void
.end method

.method private isGUIDProductDetailCondition()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->hasLoadType()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->hasProductID()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->isStoreType()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUNCStore()Z
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected callRequest()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->isGUIDProductDetailCondition()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->isUNCStore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->requestGUIDProductDetailExCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/content/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 66
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->requestGUIDProductDetailMainCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/content/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->requestProductDetail()V

    goto :goto_0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->callRequest()V

    .line 22
    return-void
.end method

.method protected requestProductDetail()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->requestProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/content/k;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 89
    return-void
.end method
