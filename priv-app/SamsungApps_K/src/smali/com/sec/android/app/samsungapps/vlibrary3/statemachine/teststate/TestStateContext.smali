.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public clearActionList()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 35
    return-void
.end method

.method public containAction(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 41
    if-ne v1, p1, :cond_0

    .line 42
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActionList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public onAction(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method

.method public abstract sendEvent(Ljava/lang/Object;)V
.end method

.method public setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/teststate/TestStateContext;->b:Ljava/lang/Object;

    .line 13
    return-void
.end method
