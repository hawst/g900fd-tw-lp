.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _CardSortMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

.field private _Context:Landroid/content/Context;

.field private _ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

.field private _IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

.field _RegisterRequestors:Ljava/util/ArrayList;

.field private _StateInfoList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

.field private _bNeedToShowDetail:Z

.field bRegisterMode:Z

.field private dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_CardSortMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_StateInfoList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_RegisterRequestors:Ljava/util/ArrayList;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->bRegisterMode:Z

    .line 202
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_Context:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 38
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    .line 39
    iput-boolean p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_bNeedToShowDetail:Z

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    return-void
.end method

.method private registerCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 182
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 183
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    invoke-virtual {v1, p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->creditCardRegister(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 199
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 200
    return-void
.end method

.method private registerUSACard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 161
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/c;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    invoke-virtual {v1, p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->creditCardRegisterForUSA(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 177
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 178
    return-void
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    .line 112
    return-void
.end method


# virtual methods
.method protected endLoading()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 214
    :cond_0
    return-void
.end method

.method public getCardSortMap()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_CardSortMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    return-object v0
.end method

.method public getStateInfoList()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_StateInfoList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    return-object v0
.end method

.method public invokeCompleted(Z)V
    .locals 1

    .prologue
    .line 126
    if-eqz p1, :cond_0

    .line 128
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->INVOKE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->INVOKE_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    goto :goto_0
.end method

.method public invokeView()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 138
    return-void
.end method

.method public isRegisterMode()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->bRegisterMode:Z

    return v0
.end method

.method public isUSA()Z
    .locals 2

    .prologue
    .line 218
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    return v0
.end method

.method public needDisplayCardDetail()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_bNeedToShowDetail:Z

    return v0
.end method

.method public register(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->USER_SUBMIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->isUSA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->registerUSACard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->registerCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method public requestCardRegister(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->hasCard()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->bRegisterMode:Z

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_RegisterRequestors:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->REGISTERCREDITCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    .line 55
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected requestCardSort()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_CardSortMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_CardSortMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CardSortMap;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->searchCard(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 79
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 80
    return-void
.end method

.method protected requestCardStateInfo()V
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->isUSA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_StateInfoList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    .line 87
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_StateInfoList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getStateList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 101
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->RECEIVE_STATEINFO_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    goto :goto_0
.end method

.method protected sendFailSignal()V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_RegisterRequestors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 224
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_0

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_RegisterRequestors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 227
    return-void
.end method

.method protected sendRegisterSuccessSignal()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 230
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iput v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_RegisterRequestors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 233
    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_RegisterRequestors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 236
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->CREDIT_CARD_REGISTERED:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->notifyEvent(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;)V

    .line 237
    return-void
.end method

.method public setNeedToDisplayDetail(Z)V
    .locals 0

    .prologue
    .line 240
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_bNeedToShowDetail:Z

    .line 241
    return-void
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 207
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    .line 143
    return-void
.end method
