.class public abstract Lcom/sec/android/app/samsungapps/ActionBarActivity;
.super Lcom/sec/android/app/samsungapps/CommonActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# static fields
.field private static a:Z

.field private static b:Z


# instance fields
.field protected mHasSearchIcon:Z

.field protected mMainView:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 458
    sput-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a:Z

    .line 459
    sput-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mHasSearchIcon:Z

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V
    .locals 3

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAll()Ljava/lang/Boolean;

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 395
    if-eqz p5, :cond_5

    .line 397
    const/4 v0, 0x0

    :goto_0
    array-length v1, p5

    if-ge v0, v1, :cond_5

    .line 399
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    aget v2, p5, v0

    invoke-virtual {v1, v2, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 400
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    if-nez v1, :cond_1

    .line 401
    const-string v0, "ActionBarActivity::setActionBarConfiguration::Document.getInstanc().getConfig is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 416
    :cond_0
    :goto_1
    return-void

    .line 404
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v1

    if-nez v1, :cond_4

    aget v1, p5, v0

    const v2, 0xa0005

    if-eq v1, v2, :cond_3

    aget v1, p5, v0

    const v2, 0xa000b

    if-ne v1, v2, :cond_4

    .line 408
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mHasSearchIcon:Z

    .line 397
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->changeActionBarBG(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showActionBar()Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 462
    const-class v1, Lcom/sec/android/app/samsungapps/ActionBarActivity;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a:Z

    if-eqz v0, :cond_0

    .line 463
    sget-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    :goto_0
    monitor-exit v1

    return v0

    .line 467
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.sec.feature.findo"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->b:Z

    .line 468
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a:Z

    .line 470
    sget-boolean v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 462
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)V
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 440
    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 479
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    .line 485
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public enableActionItem(IZ)V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    .line 453
    :cond_0
    return-void
.end method

.method protected getActionBarVisibility()I
    .locals 2

    .prologue
    .line 182
    const/4 v0, 0x4

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v1, :cond_0

    .line 185
    const-string v1, "ActionBarActivity::getActionBarVisibility::Action Bar is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 191
    :goto_0
    return v0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 589
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 590
    sget-object v0, Lcom/sec/android/app/samsungapps/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 598
    const-string v0, "ActionBarActivity::getAccountEventType() error."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 602
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_1

    .line 604
    sget-object v0, Lcom/sec/android/app/samsungapps/c;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getUpdateAppEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->getUpdateAppEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 612
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_2

    .line 614
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    .line 615
    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 616
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 617
    if-lez v0, :cond_2

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->getTaskId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 619
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->finish()V

    .line 628
    :cond_2
    :goto_2
    return v2

    .line 593
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getOptionMenu()Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getOptionMenu()Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getOptionMenu()Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->refreshOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)V

    goto :goto_0

    .line 607
    :pswitch_1
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/BadgeNotification;->setBadgeNotification(Landroid/content/Context;I)Z

    goto :goto_1

    .line 623
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->finish()V

    goto :goto_2

    .line 590
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 604
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method protected hideActionBar()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v0, :cond_0

    .line 168
    const-string v0, "ActionBarActivity::hideActionBar::Action Bar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 174
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->hideActionBar()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->setContentView(I)V

    .line 48
    const v0, 0x7f0c003a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    .line 49
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 52
    :try_start_0
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 53
    new-instance v1, Lcom/sec/android/app/samsungapps/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/b;-><init>(Lcom/sec/android/app/samsungapps/ActionBarActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 78
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ActionBarActivity::Exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 109
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onDestroy()V

    .line 110
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 494
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 496
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 497
    const/4 v0, 0x1

    .line 500
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/16 v2, 0x52

    const/4 v0, 0x1

    .line 509
    if-ne p1, v2, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mHasSearchIcon:Z

    if-eqz v1, :cond_2

    .line 511
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 513
    instance-of v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;

    if-eqz v1, :cond_1

    .line 515
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    .line 531
    :cond_0
    :goto_0
    return v0

    .line 519
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 520
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 521
    invoke-static {p0, v1, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 526
    :cond_2
    if-eq p1, v2, :cond_0

    .line 531
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 538
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 540
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 577
    :goto_0
    return v0

    .line 543
    :cond_1
    const/16 v0, 0x54

    if-ne p1, v0, :cond_5

    .line 545
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 547
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 551
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mCurActivity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;

    if-eqz v0, :cond_4

    .line 553
    sget-object v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f0c008e

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/EditTextUtil;->getEditText(Landroid/app/Activity;I)Landroid/widget/EditText;

    move-result-object v2

    .line 555
    if-eqz v2, :cond_3

    .line 557
    sget-object v0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mCurActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;

    new-instance v3, Landroid/view/KeyEvent;

    const/16 v4, 0x42

    invoke-direct {v3, v5, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v2, v5, v3}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    :cond_3
    :goto_1
    move v0, v1

    .line 566
    goto :goto_0

    .line 562
    :cond_4
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 563
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 564
    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;Z)V

    goto :goto_1

    .line 569
    :cond_5
    const/16 v0, 0x52

    if-ne p1, v0, :cond_6

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_6

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->doNonHardKeyOptionMenu(I)V

    .line 577
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 97
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onWindowFocusChanged(Z)V

    .line 84
    if-eqz p1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->onWindowStatusChangedListener()V

    .line 89
    :cond_0
    return-void
.end method

.method protected removeMainView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    const-string v0, "ActionBarActivity::removeMainView::aView and MainView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V
    .locals 7

    .prologue
    .line 348
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 349
    return-void
.end method

.method protected setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V
    .locals 7

    .prologue
    .line 335
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 336
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 358
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 359
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V
    .locals 7

    .prologue
    .line 311
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 312
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)V
    .locals 7

    .prologue
    .line 300
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 301
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V
    .locals 7

    .prologue
    .line 323
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 324
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Z[II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 372
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, v2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 373
    return-void
.end method

.method protected setMainView(I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    .line 202
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0, v3, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActionBarActivity::setMainView::ResourceId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MainView is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setMainView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 250
    :goto_0
    return-void

    .line 248
    :cond_0
    const-string v0, "ActionBarActivity::setMainView::aView and MainView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setMainViewAndEmptyView(I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    if-lez p1, :cond_1

    .line 220
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0, v3, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0082

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 224
    if-eqz v0, :cond_0

    .line 225
    const-string v1, "isa_samsungapps_icon_dummy"

    const-string v2, "drawable"

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActionBarActivity::setMainView::ResourceId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MainView is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setMainViewAndEmptyView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mMainView:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0082

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 264
    if-eqz v0, :cond_0

    .line 265
    const-string v1, "isa_samsungapps_icon_dummy"

    const-string v2, "drawable"

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    const-string v0, "ActionBarActivity::setMainView::aView and MainView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setMenuIconWhite(Z)V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setMenIconWhite(Z)V

    .line 427
    :cond_0
    return-void
.end method

.method protected setNagativeText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v0, :cond_0

    .line 138
    const-string v0, "ActionBarActivity::setNagativeText::Action Bar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setNegativeText(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method protected setPositiveText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v0, :cond_0

    .line 121
    const-string v0, "ActionBarActivity::setPositiveText::Action Bar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setPositiveText(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method protected showActionBar()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v0, :cond_0

    .line 153
    const-string v0, "ActionBarActivity::showActionBar::Action Bar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showActionBar()Ljava/lang/Boolean;

    goto :goto_0
.end method
