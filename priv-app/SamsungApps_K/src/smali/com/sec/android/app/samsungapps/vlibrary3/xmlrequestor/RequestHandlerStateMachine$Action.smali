.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

.field public static final enum SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    const-string v1, "SEND_REQUEST"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    return-object v0
.end method
