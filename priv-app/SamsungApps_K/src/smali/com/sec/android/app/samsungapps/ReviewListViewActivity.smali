.class public Lcom/sec/android/app/samsungapps/ReviewListViewActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;


# instance fields
.field a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

.field private e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ReviewListViewActivity;)Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/ReviewListViewActivity;)Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    return-object v0
.end method


# virtual methods
.method public onCompleteCommentListLoading(Z)V
    .locals 2

    .prologue
    .line 260
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    if-nez v0, :cond_2

    .line 263
    :cond_0
    const-string v0, "ReviewListViewActivity::onCompleteCommentListLoading::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 279
    :cond_1
    :goto_0
    return-void

    .line 270
    :cond_2
    iget v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount2()I

    move-result v0

    .line 277
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->setTitle(I)V

    goto :goto_0

    .line 274
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount()I

    move-result v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, -0x1

    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->a:Landroid/content/Context;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "productid"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    const-string v1, "commentType"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-eq v1, v3, :cond_0

    const-string v1, "ReviewListViewActivity::onCreate::Using the saved Bundle"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    const-string v1, "productid"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "commentType"

    iget v2, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "productid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "commentType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v0, v3, :cond_3

    .line 51
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReviewListViewActivity::onCreate::Product ID is empty, type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->finish()V

    .line 67
    :cond_2
    :goto_0
    return-void

    .line 57
    :cond_3
    const v0, 0x7f0400bb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->setMainView(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v4, v6, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 64
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->setTitle(I)V

    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    iget v2, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->setDataLoadingObserver(Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;)V

    const v0, 0x7f0c0296

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setCommentListWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->loadWidget()V

    goto :goto_0
.end method

.method public onDeleteCommentClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 205
    :cond_0
    const-string v0, "ReviewListViewActivity::onEditCommentClick::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 228
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getID()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    new-instance v3, Lcom/sec/android/app/samsungapps/ho;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/ho;-><init>(Lcom/sec/android/app/samsungapps/ReviewListViewActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->sendRequest(Ljava/lang/String;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    .line 148
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->releaseWidget()V

    .line 151
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->d:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->clear()V

    .line 156
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    .line 158
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 159
    return-void
.end method

.method public onEditCommentClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 177
    :cond_0
    const-string v0, "ReviewListViewActivity::onEditCommentClick::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 200
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    new-instance v3, Lcom/sec/android/app/samsungapps/hm;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/hm;-><init>(Lcom/sec/android/app/samsungapps/ReviewListViewActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->sendRequest(Ljava/lang/String;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method public onExpertLinkClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V
    .locals 4

    .prologue
    .line 232
    if-nez p1, :cond_0

    .line 233
    const-string v0, "ReviewListViewActivity::onEditCommentClick::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 251
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUrl()Ljava/lang/String;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 239
    :cond_1
    const-string v0, "ReviewListViewActivity::onExpertLinkClick::URL is invalid value"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_2
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;-><init>(Landroid/content/Context;)V

    .line 247
    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 248
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    :cond_3
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openWebBrowser(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 140
    const-string v0, "productid"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v0, "commentType"

    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    :cond_0
    return-void
.end method

.method public onStartCommentListLoading()V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method public setTitle(I)V
    .locals 7

    .prologue
    const v6, 0x7f080221

    const v5, 0x7f0801fc

    const/4 v4, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 77
    const-string v0, ""

    .line 79
    if-gtz p1, :cond_2

    .line 83
    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v1, v3, :cond_1

    .line 84
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v1, :cond_6

    .line 109
    const-string v0, "ReviewListViewActivity::_setTitle::ActionBar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 113
    :goto_1
    return-void

    .line 85
    :cond_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v1, v2, :cond_0

    .line 86
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_2
    if-ne p1, v3, :cond_4

    .line 92
    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v1, v3, :cond_3

    .line 93
    const-string v0, "%s (%d)"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_3
    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v1, v2, :cond_0

    .line 95
    const-string v0, "%s (%d)"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_4
    if-le p1, v3, :cond_0

    .line 101
    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v1, v3, :cond_5

    .line 102
    const-string v0, "%s (%d)"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_5
    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->b:I

    if-ne v1, v2, :cond_0

    .line 104
    const-string v0, "%s (%d)"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 111
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    goto :goto_1
.end method
