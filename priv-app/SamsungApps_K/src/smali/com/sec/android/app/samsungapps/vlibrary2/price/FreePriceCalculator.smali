.class public Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;


# instance fields
.field private _IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    .line 11
    return-void
.end method


# virtual methods
.method public getBuyPrice()D
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getDiscountedPrice()D
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getDiscountPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getDisplayBuyPrice()D
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getNormalPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public isDiscounted()Z
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/FreePriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->isDiscounted()Z

    move-result v0

    return v0
.end method
