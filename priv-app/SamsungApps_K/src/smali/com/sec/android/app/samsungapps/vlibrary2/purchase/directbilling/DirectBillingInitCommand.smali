.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

.field private mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;Z)V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->onFinalResult(Z)V

    return-void
.end method

.method private getInitParam()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;)V

    return-object v0
.end method

.method private getResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;)V

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->getInitParam()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->getResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 59
    return-void
.end method

.method public requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->initiateOptBillingPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 51
    return-void
.end method
