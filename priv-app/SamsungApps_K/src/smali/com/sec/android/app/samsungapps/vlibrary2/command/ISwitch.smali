.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field protected _ICommandMap:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 7
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->_ICommandMap:Landroid/util/SparseArray;

    .line 11
    return-void
.end method


# virtual methods
.method public addCommand(ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->_ICommandMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 16
    return-void
.end method

.method public findCommand(I)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->_ICommandMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->selectCommandToExecute()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 26
    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->onFinalResult(Z)V

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->runSelectedCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    goto :goto_0
.end method

.method protected runSelectedCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;->_Context:Landroid/content/Context;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/command/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;)V

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 45
    return-void
.end method

.method protected abstract selectCommandToExecute()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
