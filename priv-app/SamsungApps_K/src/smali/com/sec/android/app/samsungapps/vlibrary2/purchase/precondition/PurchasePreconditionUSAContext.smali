.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private _Context:Landroid/content/Context;

.field private _Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

.field private _accountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_accountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 25
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 26
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Context:Landroid/content/Context;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)V

    return-void
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)Z

    .line 65
    return-void
.end method


# virtual methods
.method public check(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->hasCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_USA_CARDEXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)Z

    .line 55
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_USA_CARD_NOT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)Z

    goto :goto_0

    .line 53
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_NOT_USA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)Z

    goto :goto_0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 69
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 121
    :goto_0
    return-void

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_accountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->createRegisterCreditCard(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 90
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 93
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    goto :goto_0

    .line 98
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 101
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    goto :goto_0

    .line 106
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->askRegistrationOfCreditCardIsRequiredToPayTax(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    .line 35
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;)V

    return-void
.end method
