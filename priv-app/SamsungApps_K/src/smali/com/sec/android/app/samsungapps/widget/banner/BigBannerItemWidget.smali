.class public Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;
.super Landroid/widget/FrameLayout;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

.field c:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

.field d:Landroid/content/Context;

.field e:Z

.field f:Z

.field g:Z

.field h:Landroid/view/View$OnClickListener;

.field private i:I

.field private j:Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->f:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->g:Z

    .line 121
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/banner/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/banner/b;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->h:Landroid/view/View$OnClickListener;

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->e:Z

    .line 39
    const v0, 0x7f040061

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->initView(Landroid/content/Context;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->f:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->g:Z

    .line 121
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/banner/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/banner/b;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->h:Landroid/view/View$OnClickListener;

    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->BigBannerAttribute:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->e:Z

    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->e:Z

    if-eqz v0, :cond_0

    .line 48
    const v0, 0x7f040062

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->initView(Landroid/content/Context;I)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    const v0, 0x7f040061

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->initView(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->f:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->g:Z

    .line 121
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/banner/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/banner/b;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->h:Landroid/view/View$OnClickListener;

    .line 56
    sget-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->BigBannerAttribute:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->e:Z

    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->e:Z

    if-eqz v0, :cond_0

    .line 60
    const v0, 0x7f040062

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->initView(Landroid/content/Context;I)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    const v0, 0x7f040061

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->initView(Landroid/content/Context;I)V

    goto :goto_0
.end method


# virtual methods
.method public getBanner()Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    return-object v0
.end method

.method public getBannerId()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->i:I

    return v0
.end method

.method public getBannerOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public initView(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->d:Landroid/content/Context;

    .line 75
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->addView(Landroid/view/View;)V

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setFocusableInTouchMode(Z)V

    .line 77
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 81
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->refreshWidget()V

    .line 71
    return-void
.end method

.method public refreshWidget()V
    .locals 5

    .prologue
    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 147
    const v0, 0x7f0c0193

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;

    .line 152
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->e:Z

    if-eqz v1, :cond_2

    .line 153
    const v2, 0x7f070011

    .line 154
    const v1, 0x7f070012

    .line 160
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 161
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 164
    :cond_0
    const v0, 0x7f0c018f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 165
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 166
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_1
    :goto_1
    return-void

    .line 156
    :cond_2
    const v2, 0x7f070013

    .line 157
    const v1, 0x7f070015

    goto :goto_0

    .line 168
    :catch_0
    move-exception v0

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BigBannerItemWidget::refreshWidget()::OutOfMemoryError::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->f:Z

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->c:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 140
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->removeAllViews()V

    .line 142
    return-void
.end method

.method public setBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 115
    return-void
.end method

.method public setBannerId(I)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->i:I

    .line 107
    return-void
.end method

.method public setBannerWidgetData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    .line 103
    return-void
.end method

.method public setSelectedFlag(Z)V
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->g:Z

    .line 91
    return-void
.end method

.method public setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    .line 99
    return-void
.end method

.method public setWidgetData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->c:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 95
    return-void
.end method
