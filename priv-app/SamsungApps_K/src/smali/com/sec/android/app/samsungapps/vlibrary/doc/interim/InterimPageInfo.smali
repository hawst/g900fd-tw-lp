.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;
.super Ljava/util/ArrayList;
.source "ProGuard"


# instance fields
.field private InterimBGImgUrl:Ljava/lang/String;

.field private InterimTopTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterimBGImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->InterimBGImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getInterimTopTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->InterimTopTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setInterimBGImgUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->InterimBGImgUrl:Ljava/lang/String;

    .line 26
    :cond_0
    return-void
.end method

.method public setInterimTopTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 13
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->InterimTopTitle:Ljava/lang/String;

    .line 17
    :cond_0
    return-void
.end method
