.class public Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 20
    const v0, 0x7f04004f

    invoke-static {p2}, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->createContenViewList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    const/16 v2, 0x63

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    .line 22
    return-void
.end method


# virtual methods
.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 9

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/HotKeyWordListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->mListType:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewholder/HotKeyWordListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    return-object v0
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v1

    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showHotKeywordSearchList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 38
    return-void
.end method
