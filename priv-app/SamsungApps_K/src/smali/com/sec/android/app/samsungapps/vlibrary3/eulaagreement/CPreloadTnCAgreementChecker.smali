.class public Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;

    .line 10
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;

    .line 11
    return-void
.end method


# virtual methods
.method public isTncAgreed()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 16
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;->needToSeeFOTATnC()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;->fotaAgreed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28
    :cond_0
    :goto_0
    return v0

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;->needToSeeEULATnc()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;->eulaAgreed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
