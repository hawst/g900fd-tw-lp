.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final ADOBE_AIR_PACKAGE_NAME:Ljava/lang/String; = "com.adobe.air"

.field public static final ADP_PACKAGE_NAME:Ljava/lang/String; = "com.adpreference.adp"

.field public static final AD_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "1.0.0.00"

.field public static final AD_APK_PKGNAME:Ljava/lang/String; = "com.sec.android.app.secad"

.field public static final AD_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Ad Utility"

.field public static final AD_PREFERENCE_SETTING:Ljava/lang/String; = "com.sec.android.app.ad.onoff.setting"

.field public static final AD_PREFERENCE_SETTING_FROM_APK:Ljava/lang/String; = "com.sec.android.app.ad.onoff.setting.fromAPK"

.field public static final AGE_LIMIT_0:Ljava/lang/String; = "0+"

.field public static final AGE_LIMIT_12:Ljava/lang/String; = "12+"

.field public static final AGE_LIMIT_15:Ljava/lang/String; = "15+"

.field public static final AGE_LIMIT_18:Ljava/lang/String; = "18+"

.field public static final AGE_LIMIT_19:Ljava/lang/String; = "19+"

.field public static final AGE_LIMIT_4:Ljava/lang/String; = "4+"

.field public static final APPROVED_BY_SAMSUNG:I = 0x0

.field public static final APPROVED_BY_SINA:I = 0x1

.field public static final AUTHORITY:Ljava/lang/String;

.field public static final CALL_TYPE_BUY:I = 0x1

.field public static final CALL_TYPE_OPTION:I = 0x0

.field public static final CAMERA_FIRMWARE_PKG_NAME:Ljava/lang/String; = "com.sec.android.app.camerafirmware_"

.field public static final CLIENT_TYPE_ODC:Ljava/lang/String; = "odc"

.field public static final CLIENT_TYPE_OPEN:Ljava/lang/String; = "open"

.field public static final CLIENT_TYPE_UNA:Ljava/lang/String; = "una"

.field public static final CLIENT_TYPE_UNC:Ljava/lang/String; = "unc"

.field public static final CONTENT_ADOBE_AIR_TYPE:Ljava/lang/String; = "adobe_air"

.field public static final CONTENT_AGENT_TYPE:Ljava/lang/String; = "agent"

.field public static final CONTENT_ALL_TYPE:Ljava/lang/String; = "all"

.field public static final CONTENT_APP_TYPE:Ljava/lang/String; = "application"

.field public static final CONTENT_JAVA_TYPE:Ljava/lang/String; = "java"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI2:Landroid/net/Uri;

.field public static final CONTENT_WIDGET_TYPE:Ljava/lang/String; = "widget"

.field public static final CSC_PATH:Ljava/lang/String;

.field public static final DB_FAKE:I = 0x1

.field public static final DB_REAL:I = 0x0

.field public static final DEVELOPER_EMAIL_BODY:Ljava/lang/String; = "Please find the attached logs"

.field public static final DEVELOPER_EMAIL_SUBJECT_LINE:Ljava/lang/String; = "[Log]SamsungApps Log files"

.field public static final DIRECT_PUSHNOTI:I = 0x4

.field public static final DIRECT_SIGNUP:I = 0x8

.field public static final DIRECT_STUB:I = 0x1

.field public static final DISCLAIMER_PRIVACY_POLICY_URI:Ljava/lang/String; = "http://www.samsungapps.com/common/privacy.as?mcc="

.field public static final DISCLAIMER_SAMSUNGAPPS_URI:Ljava/lang/String; = "http://www.samsungapps.com/common/term.as?mcc="

.field public static final DISCLAIMER_SAMSUNG_ACCOUNT_URI:Ljava/lang/String; = "http://static.bada.com/contents/deviceterms/"

.field public static final ERROR_SERVICE_UNAVAILABLE:Ljava/lang/String; = "1000"

.field public static final FIELD_OSP_VER:Ljava/lang/String; = "OSP_VER"

.field public static final FIELD_SAC:Ljava/lang/String; = "account"

.field public static final FIELD_SAC_ACCOUNT_MODE:Ljava/lang/String; = "account_mode"

.field public static final FIELD_SAC_CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final FIELD_SAC_CLIENT_SECRET:Ljava/lang/String; = "client_secret"

.field public static final FIELD_SAC_SERVICE_NAME:Ljava/lang/String; = "service_name"

.field public static final FREE_STORE:Ljava/lang/String; = "0"

.field public static final GINGERBREAD_OPEN_API:I = 0xa

.field public static final HIDE_LOADING:I = 0x1

.field public static final HONEYCOMB_OS_VERSION:I = 0xb

.field public static final INVOKE_PMGR:Ljava/lang/String; = "com.sec.android.app.samsungapps.invoke.pmgr"

.field public static final KEY_BUTTON_STATE:Ljava/lang/String; = "buttonState"

.field public static final KEY_CALLER_TYPE:Ljava/lang/String; = "CallerType"

.field public static final KEY_COMMENT_COUNT:Ljava/lang/String; = "productCommentCnt"

.field public static final KEY_COMMENT_ID:Ljava/lang/String; = "commentID"

.field public static final KEY_DIRECT_CALL:Ljava/lang/String; = "directcall"

.field public static final KEY_GUID:Ljava/lang/String; = "GUID"

.field public static final KEY_LOAD_TYPE:Ljava/lang/String; = "loadType"

.field public static final KEY_PRODUCT_ID:Ljava/lang/String; = "productID"

.field public static final KEY_PRODUCT_ITEM_INFO:Ljava/lang/String; = "productItemInfo"

.field public static final KEY_SCREENSHOT_POS:Ljava/lang/String; = "screenshotPos"

.field public static final LOAD_TYPE_POSTLOAD:Ljava/lang/String; = "1"

.field public static final LOAD_TYPE_PRELOAD:Ljava/lang/String; = "0"

.field public static final LOAD_TYPE_STORE:Ljava/lang/String; = "2"

.field public static final LOG_FILE_TO_EMAIL_RECEIVER:Ljava/lang/String; = "SamsungAppsLog.txt"

.field public static final MIN_OS_VERSION:I = 0x7

.field public static final MIN_WIBRO_OS_VERSION:I = 0x8

.field public static final NULL_STRING:Ljava/lang/String; = ""

.field public static ODC_PACKAGE_NAME:Ljava/lang/String; = null

.field public static final ONE_SEC:I = 0x3e8

.field public static final PAID_STORE:Ljava/lang/String; = "1"

.field public static final PD_TEST_PATH:Ljava/lang/String;

.field public static final PKGNAME_STR:Ljava/lang/String; = "pkgName"

.field public static final PTAG_APPID:Ljava/lang/String; = "appId"

.field public static final PTAG_APPINFO:Ljava/lang/String; = "appInfo"

.field public static final PTAG_RESULTCODE:Ljava/lang/String; = "resultCode"

.field public static final REQ_SA_MCC:Ljava/lang/String; = "com.sec.android.app.samsungapps.req.mcc"

.field public static final RES_SA_MCC:Ljava/lang/String; = "com.sec.android.app.samsungapps.res.mcc"

.field public static final SAC_CLIENT_ID:Ljava/lang/String;

.field public static final SAC_CLIENT_SECRET:Ljava/lang/String;

.field public static final SAC_INIT_VERSION_CODE:I = 0x2ee1

.field public static final SAC_MIN_VERSION:Ljava/lang/String; = "1"

.field public static final SAC_MODIFY_MODE:Ljava/lang/String; = "ACCOUNTINFO_MODIFY"

.field public static final SAC_OSP_VER:Ljava/lang/String; = "OSP_02"

.field public static final SAC_PKGNAME:Ljava/lang/String; = "com.osp.app.signin"

.field public static final SAC_PWD_CHANGED:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_CHANGED_PASSWORD_COMPLETED"

.field public static final SAC_SERVICE_NAME:Ljava/lang/String; = "SamsungApps"

.field public static final SAC_SIGNOUT_COMPLETE:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

.field public static final SAC_SYNC_SETTING:Ljava/lang/String; = "android.settings.ACCOUNT_SYNC_SETTINGS"

.field public static final SAC_VERIFY_MODE:Ljava/lang/String; = "ACCOUNT_VERIFY"

.field public static final SAMSUNGAPPS_MCC:Ljava/lang/String; = "SAMSUNGAPPS_MCC"

.field public static final SAMSUNGAPPS_MNC:Ljava/lang/String; = "SAMSUNGAPPS_MNC"

.field public static final SAMSUNGAPPS_UTILITY:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility"

.field public static final SAMSUNGAPPS_VIRTUAL_MCC:Ljava/lang/String; = "SAMSUNGAPPS_VIRTUAL_MCC"

.field public static final SA_UTIL_DEFAULT_VER:Ljava/lang/String; = "1.0.000"

.field public static final SA_UTIL_PKGNAME:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility"

.field public static final SA_UTIL_PKGNAME_V1:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility"

.field public static final SA_UTIL_PKGNAME_V2:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility2"

.field public static final SEQ_FIRST:I = 0x0

.field public static final SEQ_SECOND:I = 0x1

.field public static final SEQ_THIRD:I = 0x2

.field public static final SERVER_URL:Ljava/lang/String; = "http://hub-odc.samsungapps.com/product/appCheck.as"

.field public static final SETTINGS_PACKAGE_NAME:Ljava/lang/String; = "com.android.settings"

.field public static final SETTINGS_PACKAGE_UNA_NAME:Ljava/lang/String; = "com.android.settings.UNASettingList"

.field public static final SETTING_VALUE:Ljava/lang/String; = "onoff"

.field public static final SHOW_LOADING:I = 0x0

.field public static final SIU_PRODUCT_NAME:Ljava/lang/String; = "Samsung Installer Utility"

.field public static final SPP_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "0.10.10.0"

.field public static final SPP_APK_PKGNAME:Ljava/lang/String; = "com.sec.spp.push"

.field public static final SPP_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Push Service"

.field public static final STR_DEFAULT_DIR:Ljava/lang/String; = "/sdcard/samsungapps"

.field public static final STR_N:Ljava/lang/String; = "N"

.field public static final STR_TRUE:Ljava/lang/String; = "true"

.field public static final STR_Y:Ljava/lang/String; = "Y"

.field public static final TAB_BTN_FREE_FIRST:Ljava/lang/String; = "0"

.field public static final TAB_BTN_PAID_FIRST:Ljava/lang/String; = "1"

.field public static UNA_PACKAGE_NAME:Ljava/lang/String; = null

.field public static final UNA_PACKAGE_NOTI_CANCEL:Ljava/lang/String; = "com.sec.android.app.samsungapps.una.NOTIBAR_CANCEL"

.field public static final UNA_PRODUCT_NAME:Ljava/lang/String; = "UNAService"

.field public static final UNA_SETTINGS:Ljava/lang/String; = "android.settings.UNA_SETTINGS"

.field public static final UNC_PRODUCT_NAME:Ljava/lang/String; = "Samsung Apps"

.field public static final UPGRADE_TRUE:Ljava/lang/String; = "1"

.field public static final USERDATA_STR:Ljava/lang/String; = "userData"

.field public static final UTIL_INSTALL_FAIL:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility.ins.fail"

.field public static final UTIL_INSTALL_SUCCESS:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility.ins.success"

.field public static final UTIL_UNINSTALL_FAIL:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility.uni.fail"

.field public static final UTIL_UNINSTALL_SUCCESS:Ljava/lang/String; = "com.sec.android.app.samsungapps.sautility.uni.success"

.field public static final VALUE_OFF:Ljava/lang/String; = "0"

.field public static final VALUE_ON:Ljava/lang/String; = "1"

.field public static final VIRTUAL_MCC_RESET:Ljava/lang/String; = "com.sec.android.app.samsungapps.reset.virtualMCC"

.field public static final VIRTUAL_MCC_SET:Ljava/lang/String; = "com.sec.android.app.samsungapps.set.virtualMCC"

.field public static final WEB_WIDGET_TYPE:Ljava/lang/String; = "web_widget"

.field public static final initStrValue:Ljava/lang/String; = "0"

.field public static final initValue:I = 0x0

.field public static final invalidValue:I = -0x1

.field public static final oneInitValue:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->UNA_PACKAGE_NAME:Ljava/lang/String;

    .line 30
    const-string v0, "com.sec.android.app.samsungapps"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->ODC_PACKAGE_NAME:Ljava/lang/String;

    .line 99
    const-string v0, "3b,72,7f,79,70,3d,7e,3d,3a,7e,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    .line 100
    const-string v0, "3c,39,49,4a,3e,3b,3c,3c,3b,35,49,47,3d,46,47,3b,3e,3a,35,47,36,3b,47,37,4a,3a,47,39,3b,3b,3a,46,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    .line 130
    const-string v0, "68,74,72,33,78,6a,68,33,66,73,69,77,74,6e,69,33,75,77,78,7b,6e,69,6a,77,33,78,66,72,78,7a,73,6c,66,75,75,74,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->AUTHORITY:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI:Landroid/net/Uri;

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/info2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI2:Landroid/net/Uri;

    .line 138
    const-string v0, "34,78,7e,78,79,6a,72,34,68,78,68,34,78,79,71,6a,78,64,68,74,69,6a,33,69,66,66,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CSC_PATH:Ljava/lang/String;

    .line 139
    const-string v0, "34,78,69,68,66,77,69,34,75,69,33,79,6a,78,79,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->PD_TEST_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SendLogFileTo(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 388
    const-string v0, "/sdcard/samsungapps"

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/SamsungAppsLog.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 391
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 392
    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    const-string v2, "android.intent.extra.EMAIL"

    new-array v3, v5, [Ljava/lang/String;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    const-string v2, "android.intent.extra.SUBJECT"

    const-string v3, "[Log]SamsungApps Log files"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 397
    const-string v2, "android.intent.extra.TEXT"

    const-string v3, "Please find the attached logs"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    const-string v2, "android.intent.extra.CC"

    new-array v3, v5, [Ljava/lang/String;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 401
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 402
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mUri = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 404
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 406
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 408
    return-void
.end method

.method private static byteArrayToIntArray([B)[I
    .locals 3

    .prologue
    .line 204
    array-length v0, p0

    new-array v1, v0, [I

    .line 205
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 207
    aget-byte v2, p0, v0

    aput v2, v1, v0

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_0
    return-object v1
.end method

.method public static coverLang(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 270
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 271
    array-length v0, v2

    new-array v3, v0, [I

    move v0, v1

    .line 273
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 275
    aget-object v4, v2, v0

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    aput v4, v3, v0

    .line 273
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_0
    const/4 v0, -0x5

    invoke-static {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->shuffleIntArray([II)[I

    move-result-object v0

    .line 280
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->intArrayToByteArray([I)[B

    move-result-object v0

    .line 282
    array-length v2, v0

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    .line 284
    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 285
    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 287
    :cond_1
    array-length v2, v0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 291
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1
.end method

.method public static formedLang(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 244
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 246
    array-length v2, v0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 247
    array-length v2, v0

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    .line 249
    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 250
    array-length v2, v0

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 253
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->byteArrayToIntArray([B)[I

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->shuffleIntArray([II)[I

    move-result-object v2

    .line 255
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 256
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 258
    aget v4, v2, v1

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 264
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v0
.end method

.method private static intArrayToByteArray([I)[B
    .locals 3

    .prologue
    .line 193
    array-length v0, p0

    new-array v1, v0, [B

    .line 194
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 196
    aget v2, p0, v0

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_0
    return-object v1
.end method

.method public static varargs isNull([Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 420
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    .line 422
    if-nez v3, :cond_1

    .line 424
    const/4 v0, 0x1

    .line 427
    :cond_0
    return v0

    .line 420
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static isValidString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 412
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    const/4 v0, 0x1

    .line 415
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static makeDirectory(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 296
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 299
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 302
    :cond_0
    return-void
.end method

.method public static removeLogFile()V
    .locals 3

    .prologue
    .line 306
    const-string v0, "/sdcard/samsungapps"

    .line 309
    const-string v1, "/sdcard/samsungapps"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->makeDirectory(Ljava/lang/String;)V

    .line 311
    new-instance v1, Ljava/io/File;

    const-string v2, "SamsungAppsLog.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static shuffleIntArray([II)[I
    .locals 3

    .prologue
    .line 215
    array-length v0, p0

    new-array v1, v0, [I

    .line 216
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 218
    aget v2, p0, v0

    add-int/2addr v2, p1

    aput v2, v1, v0

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_0
    return-object v1
.end method

.method private static swapCharacterArray([BII)[B
    .locals 4

    .prologue
    .line 226
    array-length v0, p0

    .line 228
    if-lez p1, :cond_0

    if-ge p2, v0, :cond_0

    if-le p2, p1, :cond_0

    .line 230
    const/4 v0, 0x0

    :goto_0
    sub-int v1, p2, p1

    if-ge v0, v1, :cond_0

    .line 232
    add-int v1, v0, p1

    aget-byte v1, p0, v1

    .line 233
    add-int v2, v0, p1

    sub-int v3, p2, v0

    aget-byte v3, p0, v3

    aput-byte v3, p0, v2

    .line 234
    sub-int v2, p2, v0

    aput-byte v1, p0, v2

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    return-object p0
.end method

.method public static writeLogFile(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 331
    const-string v0, "/sdcard/samsungapps"

    .line 335
    const-string v1, "/sdcard/samsungapps"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->makeDirectory(Ljava/lang/String;)V

    .line 337
    new-instance v1, Ljava/io/File;

    const-string v2, "SamsungAppsLog.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 352
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_1

    .line 356
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v0, 0x1

    invoke-direct {v2, v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 360
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 362
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 363
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 370
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    .line 384
    :cond_1
    :goto_2
    return-void

    .line 365
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_2

    .line 371
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_0
.end method
