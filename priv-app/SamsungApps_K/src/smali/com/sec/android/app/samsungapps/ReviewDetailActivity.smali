.class public Lcom/sec/android/app/samsungapps/ReviewDetailActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;


# static fields
.field public static final COMMAND_TYPE_ADD:I = 0x2

.field public static final COMMAND_TYPE_MODIFY:I = 0x1

.field public static final RATING_DEFAULT_VALUE:I = 0xa


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

.field private f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

.field private g:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 34
    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    .line 40
    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->b:I

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->c:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 60
    const-string v0, "ReviewDetailActivity"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->h:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;)V

    .line 240
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    if-eqz v0, :cond_1

    .line 229
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;)V

    goto :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;)V

    goto :goto_0

    .line 238
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReviewDetailActivity::_CommandInvokeCompleted::"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ReviewDetailActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a()V

    return-void
.end method


# virtual methods
.method public enableActionItem(IZ)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showNavi(Z)V

    .line 214
    return-void
.end method

.method public getCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-nez v0, :cond_0

    .line 483
    const-string v0, "ReviewDetailActivity::getCommentText::Helper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 484
    const-string v0, ""

    .line 487
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getComment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRatingValue()I
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-nez v0, :cond_0

    .line 453
    const-string v0, "ReviewDetailActivity::getAuthorityRating::Helper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 454
    const/4 v0, 0x0

    .line 457
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getRating()I

    move-result v0

    goto :goto_0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 522
    sget-object v0, Lcom/sec/android/app/samsungapps/hl;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 533
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    move-object v0, p1

    .line 524
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 525
    sget-object v1, Lcom/sec/android/app/samsungapps/hl;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 528
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->onBackPressed()V

    .line 529
    const/4 v0, 0x0

    goto :goto_1

    .line 522
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 525
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    if-nez v0, :cond_0

    .line 290
    :goto_0
    return-void

    .line 272
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 286
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->onCancelReview()V

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->onBackPressed()V

    goto :goto_0

    .line 278
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->onSaveReview()V

    goto :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0xa000c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->cancel()V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->onCancelReview()V

    .line 200
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 201
    return-void
.end method

.method public onCommentModifyResult(Z)V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 346
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-nez v0, :cond_2

    .line 351
    const-string v0, "ReviewDetailActivity::onCommentModifyResult::Helper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 365
    :cond_1
    :goto_0
    return-void

    .line 355
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->onCommandResult(Z)V

    .line 360
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 362
    const v0, 0x7f080225

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->finish()V

    goto :goto_0
.end method

.method public onCommentModifying()V
    .locals 1

    .prologue
    .line 332
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 334
    return-void
.end method

.method public onCommentRegisterResult(Z)V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-nez v0, :cond_2

    .line 432
    const-string v0, "ReviewDetailActivity::onCommentRegisterResult::Helper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 446
    :cond_1
    :goto_0
    return-void

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->onCommandResult(Z)V

    .line 441
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 443
    const v0, 0x7f080225

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->finish()V

    goto :goto_0
.end method

.method public onCommentRegistering()V
    .locals 1

    .prologue
    .line 373
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 375
    return-void
.end method

.method public onCommentSaveResult(Z)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 396
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-nez v0, :cond_2

    .line 401
    const-string v0, "ReviewDetailActivity::onCommentRegisterResult::Helper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 415
    :cond_1
    :goto_0
    return-void

    .line 405
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->onCommandResult(Z)V

    .line 410
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 412
    const v0, 0x7f080225

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->finish()V

    goto :goto_0
.end method

.method public onCommentSaving()V
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->g:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 385
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v5, 0xb0001

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 65
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v2, "objectid"

    invoke-virtual {p1, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v6, :cond_0

    const-string v4, "ReviewDetailActivity::onCreate::Using the saved Bundle"

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    const-string v4, "objectid"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "rating"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->b:I

    const-string v0, "review"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->c:Ljava/lang/String;

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v0, :cond_1

    .line 78
    const-string v0, "ReviewDetailActivity::onCreate::Command isn\'t ready"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->finish()V

    .line 109
    :goto_0
    return-void

    .line 86
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a()V

    .line 88
    const v0, 0x7f0400ba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->setMainView(I)V

    .line 93
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    .line 95
    if-ne v0, v3, :cond_2

    .line 97
    new-array v4, v7, [I

    fill-array-data v4, :array_0

    move-object v0, p0

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 108
    :goto_1
    const v0, 0xa000d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->enableActionItem(IZ)V

    goto :goto_0

    .line 103
    :cond_2
    new-array v4, v7, [I

    fill-array-data v4, :array_1

    move-object v0, p0

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_1

    .line 97
    nop

    :array_0
    .array-data 4
        0xa000d
        0xa000c
    .end array-data

    .line 103
    :array_1
    .array-data 4
        0xa000c
        0xa000d
    .end array-data
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 163
    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    .line 164
    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->b:I

    .line 165
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->c:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->release()V

    .line 169
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->cancel()V

    .line 175
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->clear()V

    .line 181
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    .line 184
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 185
    return-void
.end method

.method public onRequestRatingAuthority()V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method public onRequestRatingAuthorityResult(Z)V
    .locals 3

    .prologue
    .line 302
    if-eqz p1, :cond_1

    .line 304
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;-><init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->b:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->setRestoreRating(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->setRestoreComment(Ljava/lang/String;)V

    const v0, 0x7f0c0295

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    new-instance v1, Lcom/sec/android/app/samsungapps/hk;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/hk;-><init>(Lcom/sec/android/app/samsungapps/ReviewDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->loadWidget()V

    .line 310
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    const-string v0, "ReviewDetailActivity::onRequestRatingAuthorityResult::Fail !"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->finish()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 463
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 465
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/hj;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/hj;-><init>(Lcom/sec/android/app/samsungapps/ReviewDetailActivity;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 476
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->pushObject(Ljava/lang/Object;)I

    move-result v0

    .line 144
    const-string v1, "objectid"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->getRating()I

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->f:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->getReview()Ljava/lang/String;

    move-result-object v1

    .line 154
    const-string v2, "rating"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 155
    const-string v0, "review"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method public setCommentType(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 317
    if-ne p1, v0, :cond_0

    .line 318
    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    .line 324
    :goto_0
    return-void

    .line 322
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;->a:I

    goto :goto_0
.end method
