.class final Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;->b:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 97
    if-eqz p1, :cond_0

    .line 98
    const-string v1, "hcjo"

    const-string v2, "updateStatus() RESULT = SUCCESS"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;->b:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->resultHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 104
    if-ne p1, v0, :cond_1

    :goto_1
    iput v0, v1, Landroid/os/Message;->what:I

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;->b:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->resultHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 106
    return-void

    .line 100
    :cond_0
    const-string v1, "hcjo"

    const-string v2, "updateStatus() RESULT = FAIL"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
