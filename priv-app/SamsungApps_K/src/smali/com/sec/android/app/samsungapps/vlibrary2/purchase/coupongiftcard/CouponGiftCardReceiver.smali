.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;


# instance fields
.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private _ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field private activeGiftCard:Ljava/util/ArrayList;

.field private couponList:Ljava/util/ArrayList;

.field private expiredGiftCard:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 23
    return-void
.end method

.method private addNullGiftCard()V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->Null()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    .line 79
    :cond_1
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public add(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V
    .locals 2

    .prologue
    .line 39
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->giftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-ne v0, v1, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 30
    return-void
.end method

.method public complete()V
    .locals 5

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_1

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_2

    .line 98
    :cond_2
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 57
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->couponList:Ljava/util/ArrayList;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 62
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 67
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->activeGiftCard:Ljava/util/ArrayList;

    .line 68
    return-void
.end method
