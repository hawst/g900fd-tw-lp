.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;

    .line 15
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->onFinalResult(Z)V

    return-void
.end method

.method private runTokenBasePasswordConfirm()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;->getTokenBasedPasswordConfirmCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/ae;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ae;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 51
    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;->isPasswordConfirmed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;->isTokenBasedSamsungAccountInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;->isSamsungAccountInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->runTokenBasePasswordConfirm()V

    .line 40
    :goto_0
    return-void

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->_IValidateConfirmPasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;->getPasswordConfirmCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/ad;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ad;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;)V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 38
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;->onFinalResult(Z)V

    goto :goto_0
.end method
