.class final Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 88
    if-eqz p2, :cond_2

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->b:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->doDeletion(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V

    .line 115
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

    invoke-interface {v0, v2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;->onDeletionResult(ZZ)V

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->clear()V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->requestDataGet(Landroid/content/Context;)V

    .line 102
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;

    invoke-interface {v0, v1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;->onDeletionResult(ZZ)V

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->clear()V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->requestDataGet(Landroid/content/Context;)V

    .line 112
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    goto :goto_0
.end method
