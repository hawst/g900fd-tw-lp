.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 16
    const-string v2, "noticeDetail"

    const-string v3, "2342"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 18
    const-string v0, "noticeId"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeDetailQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->getNoticeID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method
