.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field mNeedAutoDownload:Z

.field mPkgName:Ljava/lang/String;

.field mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mNeedAutoDownload:Z

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mPkgName:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mPkgName:Ljava/lang/String;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onFinalResult(Z)V

    return-void
.end method

.method private onNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)V

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mPkgName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    .line 56
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->requestUpdateCheck(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    .line 57
    return-void

    .line 53
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mPkgName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    goto :goto_0
.end method

.method public removeUpdateList(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 106
    .line 108
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->needUpdate()Z

    move-result v2

    .line 110
    const-string v3, "com.sec.android.app.samsungapps"

    iget-object v4, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v0, :cond_1

    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mNeedAutoDownload:Z

    if-eq v3, v0, :cond_1

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mNeedAutoDownload:Z

    .line 119
    if-ne v2, v0, :cond_0

    .line 122
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeSamsungAppsUpdate(Z)V

    .line 123
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->version:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setLatestSamsungAppsVersion(Ljava/lang/String;)V

    .line 151
    :goto_0
    return v0

    .line 127
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeSamsungAppsUpdate(Z)V

    .line 128
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setLatestSamsungAppsVersion(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 131
    :cond_1
    const-string v3, "com.alipay.android.app"

    iget-object v4, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v0, :cond_3

    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mNeedAutoDownload:Z

    if-eq v3, v0, :cond_3

    .line 138
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mNeedAutoDownload:Z

    .line 140
    if-ne v2, v0, :cond_2

    .line 143
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeAlipayUpdate(Z)V

    goto :goto_0

    .line 147
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeAlipayUpdate(Z)V

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public setNeedAutoDownload(Z)V
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mNeedAutoDownload:Z

    .line 42
    return-void
.end method
