.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;


# instance fields
.field private _CreditCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

.field private _EasyBuyPurchase:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _IGiftCardPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;

.field private _IPurchaseCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;

.field private _IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

.field private _ISummaryPreferenceDisplayInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;

.field _OnReceiveCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field private _PaymentMethodChecker:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;

.field private _PurchaseActivity:Ljava/lang/Class;

.field private _PurchaseCommandList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field private _PurchasePreconditionUSAContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;

.field private _RequestPurchaseCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 170
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_OnReceiveCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 49
    iput-object p11, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchasePreconditionUSAContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;

    .line 50
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;

    .line 51
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 52
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseCommandList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;

    .line 53
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_EasyBuyPurchase:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 55
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_RequestPurchaseCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 56
    check-cast p5, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    invoke-virtual {p5, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->setPurchaseCouponView(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;)V

    .line 57
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseActivity:Ljava/lang/Class;

    .line 58
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_ISummaryPreferenceDisplayInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;

    .line 59
    iput-object p9, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IGiftCardPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;

    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PaymentMethodChecker:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;

    .line 162
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    invoke-direct {v0, p10}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_CreditCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->invokeUI(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->execute_mainFunction()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    return-void
.end method

.method private easybuyPurchase()V
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_EasyBuyPurchase:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 398
    return-void
.end method

.method private execute_mainFunction()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 209
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 212
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;->showNoAvailablePaymentMethod()V

    .line 228
    :goto_0
    return-void

    .line 216
    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->contains(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->contains(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_IRAN_DEBIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->contains(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 219
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->requestCardInfo()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 225
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->invokeUI(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private giftCardPurchase()V
    .locals 3

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IGiftCardPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;->createGiftCardPurchase()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 383
    return-void
.end method

.method private invokeUI(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseActivity:Ljava/lang/Class;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 233
    return-void
.end method

.method private requestCardInfo()V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_CreditCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->loadCreditCardInfo()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_OnReceiveCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 168
    return-void
.end method


# virtual methods
.method public cancelPurchase()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;->killView()V

    .line 310
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V

    .line 311
    return-void
.end method

.method public getCouponContainer()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_RequestPurchaseCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->getCouponContainer()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    move-result-object v0

    return-object v0
.end method

.method public getCreditCardCommandBuilder()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_CreditCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    return-object v0
.end method

.method public getCurrentPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PaymentMethodChecker:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->getAvaliableList()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v0

    return-object v0
.end method

.method public getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    return-object v0
.end method

.method public getSelCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    return-object v0
.end method

.method public getSummaryDisplayInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_ISummaryPreferenceDisplayInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchasePreconditionUSAContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;->check(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 204
    return-void
.end method

.method public invokeCompleted(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;)V
    .locals 3

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    .line 238
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 242
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;->addPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    goto :goto_0

    .line 244
    :cond_0
    return-void
.end method

.method protected onPurchaseMethodSelected()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;->onPurchaseMethodSelected(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    .line 277
    return-void
.end method

.method public requestCoupon()V
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setSelCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq v0, v1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;->onRequestCouponList()V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_RequestPurchaseCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 292
    :cond_0
    return-void
.end method

.method public setSelCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setSelCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    .line 301
    return-void
.end method

.method public setSelMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 2

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 261
    const/4 v0, 0x1

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    .line 265
    if-nez v0, :cond_1

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->requestCoupon()V

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onPurchaseMethodSelected()V

    .line 271
    return-void
.end method

.method public startPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V
    .locals 4

    .prologue
    .line 325
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getAppliedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 331
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->giftCardPurchase()V

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->easybuyPurchase()V

    goto :goto_0

    .line 340
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseCommandList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->getPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
