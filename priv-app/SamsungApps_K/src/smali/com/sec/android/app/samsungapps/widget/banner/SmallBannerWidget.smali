.class public Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final DEFAULT_LANDSCAPE_MAX_ITEM_COUNT:I = 0x3

.field public static final DEFAULT_PORTRAIT_MAX_ITEM_COUNT:I = 0x2


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

.field private d:Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

.field private e:I

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/util/ArrayList;

.field private h:Ljava/util/ArrayList;

.field private i:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

.field private j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->k:Z

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    const v1, 0x7f0400dd

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->initView(Landroid/content/Context;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->k:Z

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    const v1, 0x7f0400dd

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->initView(Landroid/content/Context;I)V

    .line 61
    return-void
.end method

.method private a()I
    .locals 6

    .prologue
    const v5, 0x7f0d0002

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 260
    const v2, 0x7f070049

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 261
    const v3, 0x7f07004f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 262
    sub-int v0, v1, v0

    div-int/2addr v0, v2

    .line 266
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SmallBannerWidget :: widthPixels = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " minSize = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 271
    :cond_0
    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->e:I

    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/banner/q;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/banner/q;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 124
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->refreshWidget()V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;->configurationChangedComplete()V

    .line 248
    :cond_0
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->updateWidget()V

    .line 254
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->i:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->i:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->release()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 96
    :cond_3
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->i:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->removeAllViews()V

    .line 103
    return-void
.end method

.method public setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    .line 65
    return-void
.end method

.method public setGridViewColumnNum()V
    .locals 6

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    if-nez v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 303
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07004d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07004e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 306
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setHorizontalSpacing(I)V

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setVerticalSpacing(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0, v2, v3, v2, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setPadding(IIII)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setNumColumns(I)V

    goto :goto_0
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->e:I

    .line 70
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 74
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    .line 75
    return-void
.end method

.method public updateWidget()V
    .locals 9

    .prologue
    const v8, 0x7f0801e8

    const v7, 0x7f0801e7

    const/16 v6, 0x8

    const v5, 0x7f0800dd

    const/4 v2, 0x0

    .line 130
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->k:Z

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBannerCount()I

    move-result v1

    move v0, v2

    .line 146
    :goto_1
    if-ge v0, v1, :cond_2

    .line 147
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBanner(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 158
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isFeaturedBanner()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 161
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 165
    :cond_6
    const v0, 0x7f0c02f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->e:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_a

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SmallBannerWidget :: mHotList() "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 173
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 182
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->updateWidget(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 178
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 184
    :cond_9
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setVisibility(I)V

    goto/16 :goto_0

    .line 188
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SmallBannerWidget :: mNewList()  "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 193
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 202
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->updateWidget(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 198
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPromotionTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 204
    :cond_d
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setVisibility(I)V

    goto/16 :goto_0

    .line 210
    :cond_e
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public updateWidget(Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 215
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 216
    const v0, 0x7f0c02f1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setGridViewColumnNum()V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setFocusableInTouchMode(Z)V

    .line 219
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    const v4, 0x7f0400d6

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a()I

    move-result v0

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    if-le v0, v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v5, :cond_1

    const v6, 0x7f07004f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    const v7, 0x7f07004b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/lit8 v8, v0, -0x1

    mul-int/2addr v7, v8

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v5, v6

    sub-int/2addr v5, v7

    div-int v0, v5, v0

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmallBannerWidget :: width = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    invoke-direct {v2, v3, v4, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->i:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->i:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/r;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/r;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 238
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setVisibleLoading(I)Z

    .line 240
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
