.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;


# virtual methods
.method public abstract clearAllDB()V
.end method

.method public abstract get25GLimit()I
.end method

.method public abstract get2GLimit()I
.end method

.method public abstract get3GLimit()I
.end method

.method public abstract get4GLimit()I
.end method

.method public abstract getClientType()Ljava/lang/String;
.end method

.method public abstract getCountryCode()Ljava/lang/String;
.end method

.method public abstract getCurrencyUnitHasPenny()Z
.end method

.method public abstract getCurrencyUnitPrecedes()Z
.end method

.method public abstract getEmailID()Ljava/lang/String;
.end method

.method public abstract getEnableTencent()I
.end method

.method public abstract getFlexibleTabClsf()I
.end method

.method public abstract getFlexibleTabName()Ljava/lang/String;
.end method

.method public abstract getFreeStoreClsf()I
.end method

.method public abstract getFreeTabClsf()I
.end method

.method public abstract getHeaderInfinityVersion()I
.end method

.method public abstract getLastCSC()Ljava/lang/String;
.end method

.method public abstract getLastCachedTime()Ljava/util/Date;
.end method

.method public abstract getLastMCC()Ljava/lang/String;
.end method

.method public abstract getLastMNC()Ljava/lang/String;
.end method

.method public abstract getLastPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getLastPurchaseMethodType()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
.end method

.method public abstract getLatestSamsungAppsVersion()Ljava/lang/String;
.end method

.method public abstract getMinPrice()D
.end method

.method public abstract getPushNotiSuccess()Z
.end method

.method public abstract getPushRegID()Ljava/lang/String;
.end method

.method public abstract getRealCountryCode()Ljava/lang/String;
.end method

.method public abstract getSNSVal()I
.end method

.method public abstract getURL()Ljava/lang/String;
.end method

.method public abstract getUnitDivision()Z
.end method

.method public abstract getUpdatableCount()I
.end method

.method public abstract getWifiLimit()I
.end method

.method public abstract isADchecked()Z
.end method

.method public abstract isAllowAllHost()Z
.end method

.method public abstract isChinaChargePopupNeverShowAgain()Z
.end method

.method public abstract isChinaConnectWlanWarnDoNotShowAgain()Z
.end method

.method public abstract isChinaDataChargeWarnDoNotShowAgain()Z
.end method

.method public abstract isDialogCheckedDontDisplay(Ljava/lang/String;)Z
.end method

.method public abstract isNameAuthExecuted()Z
.end method

.method public abstract isPushSvcTurnedOn()Z
.end method

.method public abstract isSuggestAllShareContentChecked()Z
.end method

.method public abstract needAlipayUpdate()Z
.end method

.method public abstract needSamsungAppsUpdate()Z
.end method

.method public abstract readServerLoadLevel()I
.end method

.method public abstract setFreeTabClsf(I)V
.end method

.method public abstract setLatestSamsungAppsVersion(Ljava/lang/String;)V
.end method

.method public abstract setNameAutheExecuted()V
.end method

.method public abstract setPushNotiSuccess()V
.end method

.method public abstract write25GLimit(I)V
.end method

.method public abstract write2GLimit(I)V
.end method

.method public abstract write3GLimit(I)V
.end method

.method public abstract write4GLimit(I)V
.end method

.method public abstract writeADChecked(Z)V
.end method

.method public abstract writeAlipayUpdate(Z)V
.end method

.method public abstract writeAllowAllHost(Z)V
.end method

.method public abstract writeChinaChargePopupNeverShowAgain(Z)V
.end method

.method public abstract writeChinaConnectWlanWarnDoNotShowAgain(Z)V
.end method

.method public abstract writeChinaDataChargeWarnDoNotShowAgain(Z)V
.end method

.method public abstract writeClientType(Ljava/lang/String;)V
.end method

.method public abstract writeCountryCode(Ljava/lang/String;)V
.end method

.method public abstract writeCurrencyUnitHasPenny(Z)V
.end method

.method public abstract writeCurrencyUnitPrecedes(Z)V
.end method

.method public abstract writeDialogDontDisplayCheck(Ljava/lang/String;)V
.end method

.method public abstract writeEmailID(Ljava/lang/String;)V
.end method

.method public abstract writeEnableTencent(I)V
.end method

.method public abstract writeFlexibleTabClsf(I)V
.end method

.method public abstract writeFlexibleTabName(Ljava/lang/String;)V
.end method

.method public abstract writeFreeStore(I)V
.end method

.method public abstract writeHeaderInfinityVersion(I)V
.end method

.method public abstract writeLastCSC(Ljava/lang/String;)V
.end method

.method public abstract writeLastCachedTime(Ljava/util/Date;)V
.end method

.method public abstract writeLastMCC(Ljava/lang/String;)V
.end method

.method public abstract writeLastMNC(Ljava/lang/String;)V
.end method

.method public abstract writeLastPhoneNumber(Ljava/lang/String;)V
.end method

.method public abstract writeLastPurchaseMethodType(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
.end method

.method public abstract writeMinPrice(Ljava/lang/String;)V
.end method

.method public abstract writePushRegID(Ljava/lang/String;)V
.end method

.method public abstract writePushSvcOnOffState(Z)V
.end method

.method public abstract writeRealCountryCode(Ljava/lang/String;)V
.end method

.method public abstract writeSNSVal(I)V
.end method

.method public abstract writeSamsungAppsUpdate(Z)V
.end method

.method public abstract writeServerLoadLeverl(I)V
.end method

.method public abstract writeSuggestAllShareContentChecked(Z)V
.end method

.method public abstract writeURL(Ljava/lang/String;)V
.end method

.method public abstract writeUnitDivision(Z)V
.end method

.method public abstract writeWifiLimit(I)V
.end method
