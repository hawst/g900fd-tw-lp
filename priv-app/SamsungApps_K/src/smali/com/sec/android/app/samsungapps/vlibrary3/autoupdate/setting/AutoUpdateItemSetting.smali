.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->c:Z

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->c:Z

    .line 22
    return-void
.end method


# virtual methods
.method public checkAutoUpdSettingVisible()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 26
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->c:Z

    if-eqz v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isKNOXApp()Z

    move-result v2

    if-nez v2, :cond_0

    .line 39
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 40
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 44
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 46
    goto :goto_0

    .line 48
    :cond_3
    const-string v2, "1"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "0"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public isDisabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    const-string v3, "update_item_disabled_list"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setUpdateDisable()Z
    .locals 3

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->isDisabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 82
    const-string v0, "update_item_disabled_list"

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    .line 85
    const-string v0, ""

    .line 87
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    const-string v2, "update_item_disabled_list"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 90
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUpdateEnable()Z
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->isDisabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 100
    const-string v0, "update_item_disabled_list"

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    if-nez v0, :cond_0

    .line 103
    const-string v0, ""

    .line 105
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string v2, "update_item_disabled_list"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 108
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
