.class public Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;
.super Landroid/webkit/WebViewClient;
.source "ProGuard"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

.field private e:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 20
    const-string v0, "http://mobile.vpay.co.kr/jsp/MISP/andown.jsp"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->a:Ljava/lang/String;

    .line 21
    const-string v0, "samsungappsinicisresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->b:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

    .line 29
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->e:Landroid/webkit/WebView;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->e:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;)Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

    return-object v0
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;->webViewEnded(Landroid/webkit/WebViewClient;)V

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 35
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;->webViewStarted(Landroid/webkit/WebViewClient;)V

    .line 40
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 41
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 132
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    .line 133
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

    .line 134
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->e:Landroid/webkit/WebView;

    .line 135
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 51
    const/4 v1, 0x0

    .line 55
    const-string v2, "http://"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    const-string v2, "https://"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    const-string v2, "javascript:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 64
    :cond_0
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    move v0, v1

    .line 127
    :cond_1
    :goto_0
    return v0

    .line 68
    :cond_2
    const-string v2, "samsungappsinicisresult://"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_3

    .line 72
    :try_start_0
    const-string v2, "EUC-KR"

    invoke-static {p2, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p2

    .line 80
    :cond_3
    :goto_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 81
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 85
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 118
    const-string v1, "ispmobile://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->d:Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;

    invoke-interface {v1, p0}, Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;->startedWithISPURL(Landroid/webkit/WebViewClient;)V

    goto :goto_0

    .line 90
    :catch_0
    move-exception v2

    const-string v2, "ispmobile://"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_4

    .line 92
    const-string v0, "<html><body></body></html>"

    const-string v2, "text/html"

    const-string v3, "EUC-KR"

    invoke-virtual {p1, v0, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08027d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080302

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/iniciswebview/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/iniciswebview/a;-><init>(Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/iniciswebview/b;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/iniciswebview/b;-><init>(Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 110
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    :cond_4
    move v0, v1

    .line 114
    goto/16 :goto_0

    :catch_1
    move-exception v2

    goto/16 :goto_1
.end method
