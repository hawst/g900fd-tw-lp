.class public Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

.field private b:Ljava/util/ArrayList;

.field private d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field protected filter:Landroid/text/InputFilter;

.field private g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

.field private i:Ljava/util/ArrayList;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

.field private k:Z

.field private l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field protected lengthFilter:Landroid/text/InputFilter;

.field private m:Landroid/view/View;

.field private final n:I

.field private o:Z

.field private p:D

.field private q:Ljava/lang/String;

.field private final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    .line 63
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->n:I

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o:Z

    .line 67
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->r:Ljava/lang/String;

    .line 271
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/h;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/h;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 381
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    .line 382
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/j;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/j;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    .line 71
    const v0, 0x7f04009f

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->initView(Landroid/content/Context;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    .line 63
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->n:I

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o:Z

    .line 67
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->r:Ljava/lang/String;

    .line 271
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/h;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/h;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 381
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    .line 382
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/j;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/j;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    .line 76
    const v0, 0x7f04009f

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->initView(Landroid/content/Context;I)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    .line 63
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->n:I

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o:Z

    .line 67
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->r:Ljava/lang/String;

    .line 271
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/h;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/h;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 381
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    .line 382
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/j;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/j;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    .line 81
    const v0, 0x7f04009f

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->initView(Landroid/content/Context;I)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    if-eqz p1, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080228

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositveButtonDisable()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/purchase/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/i;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v1, 0x7f0c0277

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/k;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v1, 0x7f0c0278

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/l;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v1, 0x7f0c0279

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/c;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v1, 0x7f0c027a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/d;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->k:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/widget/purchase/n;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/m;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/m;->a(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 51
    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v3, 0x7f0c0277

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_0

    move v1, v2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v3, 0x7f0c0278

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_1

    move v1, v2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v3, 0x7f0c0279

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_2

    move v1, v2

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->m:Landroid/view/View;

    const v3, 0x7f0c027a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_3

    :goto_0
    return v2

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method static synthetic n(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o:Z

    return v0
.end method

.method static synthetic o(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)D
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->p:D

    return-wide v0
.end method

.method static synthetic r(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;->retryLoading()V

    .line 189
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onFinishInflate()V

    .line 88
    const v0, 0x7f0c023c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 89
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/b;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v0, 0x7f0c023d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 101
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/e;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/e;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 110
    const v0, 0x7f0c023f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 111
    const v1, 0x7f0c0241

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->f:Landroid/widget/ImageView;

    .line 112
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/f;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    return-void
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->updateWidget()V

    .line 141
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 250
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 364
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 368
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/n;->clear()V

    .line 373
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

    .line 376
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    .line 377
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    .line 378
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    .line 379
    return-void
.end method

.method public set(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)V
    .locals 2

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    .line 130
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;->isKorea()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->k:Z

    .line 133
    const v0, 0x7f0c0240

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->e:Landroid/widget/TextView;

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->e:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->k:Z

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->f:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isSelected()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :cond_0
    return-void
.end method

.method public setPurchaseItemInfo(Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;)V
    .locals 2

    .prologue
    .line 735
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->getPurchaseItemPrice()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->p:D

    .line 736
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->getPurchaseItemCurrency()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->q:Ljava/lang/String;

    .line 737
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 144
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    if-ne p1, v0, :cond_2

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setVisibility(I)V

    .line 149
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setVisibleLoading(I)Z

    .line 181
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_NODATA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    if-ne p1, v0, :cond_4

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    if-eqz v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setVisibility(I)V

    .line 156
    :cond_3
    const v0, 0x7f0c0081

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 160
    const v1, 0x7f0200a4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 161
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 162
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    .line 163
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 165
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setVisibleNodata(I)Z

    goto :goto_0

    .line 166
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_RETRY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    if-ne p1, v0, :cond_6

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    if-eqz v0, :cond_5

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setVisibility(I)V

    .line 171
    :cond_5
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setVisibleRetry(I)Z

    goto :goto_0

    .line 172
    :cond_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_LOAD_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    if-ne p1, v0, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->refresh()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    if-eqz v0, :cond_7

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setVisibility(I)V

    .line 179
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setVisibleGoneAll()V

    goto :goto_0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public updateWidget()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 202
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o:Z

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;->size()I

    move-result v2

    move v0, v1

    .line 206
    :goto_1
    if-ge v0, v2, :cond_2

    .line 207
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    invoke-interface {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    move-result-object v3

    .line 208
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/android/app/samsungapps/widget/purchase/m;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/widget/purchase/m;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 212
    :cond_2
    const v0, 0x7f0c0243

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setFocusableInTouchMode(Z)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setChoiceMode(I)V

    .line 215
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/n;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/n;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g:Lcom/sec/android/app/samsungapps/widget/purchase/n;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setVisibility(I)V

    .line 218
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setVisibleLoading(I)Z

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->d:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/g;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 244
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_NODATA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;)V

    goto :goto_0
.end method
