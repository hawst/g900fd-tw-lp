.class public Lcom/sec/android/app/samsungapps/uiutil/SpinnerUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSelectedString(Landroid/view/View;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/uiutil/SpinnerUtil;->getSpinner(Landroid/view/View;I)Landroid/widget/Spinner;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/SpinnerUtil;->getSelectedString(Landroid/widget/Spinner;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSelectedString(Landroid/widget/Spinner;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    if-nez p0, :cond_1

    .line 20
    const-string v0, ""

    .line 26
    :cond_0
    :goto_0
    return-object v0

    .line 22
    :cond_1
    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 23
    if-nez v0, :cond_0

    .line 26
    const-string v0, ""

    goto :goto_0
.end method

.method public static getSpinner(Landroid/view/View;I)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 10
    if-nez v0, :cond_0

    .line 12
    const/4 v0, 0x0

    .line 14
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/widget/Spinner;

    goto :goto_0
.end method
