.class public Lcom/sec/android/app/samsungapps/CheckDialogBuilder;
.super Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
.source "ProGuard"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/String;

.field protected helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field private i:Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 20
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->b:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->c:Ljava/lang/String;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->d:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->e:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->f:Ljava/lang/String;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->g:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->h:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->b:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->c:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->d:Ljava/lang/String;

    .line 36
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->e:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08023d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->f:Ljava/lang/String;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CheckDialogBuilder;)Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;

    return-object v0
.end method


# virtual methods
.method protected setDontDisplayAgain()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeDialogDontDisplayCheck(Ljava/lang/String;)V

    .line 139
    :cond_0
    return-void
.end method

.method public setImageURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->h:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setmIsTwoButtonDialog(Z)V
    .locals 0

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->g:Z

    .line 126
    return-void
.end method

.method public setmNegativeButtonText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->f:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setmPositiveButtonText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->e:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public show()Z
    .locals 7

    .prologue
    const v4, 0x7f0c01ce

    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;

    if-nez v0, :cond_1

    .line 45
    :cond_0
    const-string v0, "Context or dialogKey or buttonListener is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    move v0, v1

    .line 113
    :goto_0
    return v0

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isDialogCheckedDontDisplay(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IDialogButtonListener;->onPositive()V

    move v0, v1

    .line 52
    goto :goto_0

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 57
    const v2, 0x7f040081

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 58
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 61
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 62
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_3

    .line 63
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 64
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v4

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v3, v5

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v3, v5

    float-to-int v3, v3

    add-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v5

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 70
    :cond_3
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->e:Ljava/lang/String;

    new-instance v4, Lcom/sec/android/app/samsungapps/s;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/s;-><init>(Lcom/sec/android/app/samsungapps/CheckDialogBuilder;)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 84
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->g:Z

    if-eqz v0, :cond_4

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->f:Ljava/lang/String;

    new-instance v4, Lcom/sec/android/app/samsungapps/t;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/t;-><init>(Lcom/sec/android/app/samsungapps/CheckDialogBuilder;)V

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 99
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 100
    const v0, 0x7f0c01cb

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    const v0, 0x7f0c01cc

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 107
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    .line 111
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CheckDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 113
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    move-result v0

    goto/16 :goto_0
.end method
