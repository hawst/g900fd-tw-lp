.class public Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private b:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

.field private d:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

.field private e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget$IPermissionPopupButtonWidgetObserver;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 35
    const v0, 0x7f040095

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->initView(Landroid/content/Context;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const v0, 0x7f040095

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->initView(Landroid/content/Context;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const v0, 0x7f040095

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->initView(Landroid/content/Context;I)V

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;)Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget$IPermissionPopupButtonWidgetObserver;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget$IPermissionPopupButtonWidgetObserver;

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0c0144

    const v4, 0x7f0c0140

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 98
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 100
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 108
    :goto_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->b:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->b:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->b:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->b:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/x;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/x;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->d:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->d:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->d:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->d:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/w;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/w;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->updateWidget()V

    .line 41
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->refreshWidget()V

    .line 90
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->updateWidget()V

    .line 79
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 94
    return-void
.end method

.method public setForAllUpdate(Z)V
    .locals 0

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->f:Z

    .line 159
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget$IPermissionPopupButtonWidgetObserver;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget$IPermissionPopupButtonWidgetObserver;

    .line 149
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public updateWidget()V
    .locals 4

    .prologue
    const v2, 0x7f080348

    const v3, 0x7f08023d

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-nez v0, :cond_0

    .line 47
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->setVisibility(I)V

    .line 74
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->f:Z

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0801b0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f0802eb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
