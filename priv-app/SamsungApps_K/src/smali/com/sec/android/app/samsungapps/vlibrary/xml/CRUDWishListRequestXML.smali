.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 11
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 13
    return-void
.end method

.method public static createWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;

    const-string v1, "createWishList"

    const-string v2, "4100"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 23
    const-string v1, "productID"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-object v0
.end method

.method public static deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;

    const-string v1, "deleteWishList"

    const-string v2, "4110"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 30
    const-string v1, "wishListId"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->wishListID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return-object v0
.end method

.method public static deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;

    const-string v1, "deleteWishList"

    const-string v2, "4110"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 37
    const-string v1, "wishListId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-object v0
.end method
