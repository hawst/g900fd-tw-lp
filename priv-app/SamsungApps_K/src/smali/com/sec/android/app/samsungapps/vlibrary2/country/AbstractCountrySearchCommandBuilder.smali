.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;


# instance fields
.field private _IDisclaimerCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;

.field private bCountrySearchExecuted:Z

.field private country:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

.field private device:Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

.field private mNetErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->device:Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    .line 31
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->country:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->_IDisclaimerCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->mNetErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    .line 35
    return-void
.end method


# virtual methods
.method public checkQAPassword()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;)V

    return-object v0
.end method

.method public clearCache()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public countrySearchCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->mNetErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V

    return-object v0
.end method

.method public countrySearchRequired()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->country:Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->needUpdate()Z

    move-result v0

    return v0
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract getCQPasswordViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public abstract invokeSelectCountryListView()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public isCountryListSearchMode()Z
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isCountryListSearchMode()Z

    move-result v0

    return v0
.end method

.method public isMCCAvailable()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->device:Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->isMCCAvailable()Z

    move-result v0

    return v0
.end method

.method public isReqCountrySearchExectuted()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->bCountrySearchExecuted:Z

    return v0
.end method

.method public isTestMode()Z
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    return v0
.end method

.method public requestCountrySearch()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;)V

    return-object v0
.end method

.method public requestCountrySearchEx()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 104
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;-><init>()V

    return-object v0
.end method

.method public selectCountryList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;)V

    return-object v0
.end method

.method public setReqCountrySearchExecuted(Z)V
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->bCountrySearchExecuted:Z

    .line 93
    return-void
.end method

.method public validateDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->_IDisclaimerCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;->validateDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method
