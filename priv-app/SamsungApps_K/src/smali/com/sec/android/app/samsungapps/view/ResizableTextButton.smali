.class public Lcom/sec/android/app/samsungapps/view/ResizableTextButton;
.super Landroid/widget/Button;
.source "ProGuard"


# instance fields
.field private a:I

.field private final b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 10
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a:I

    .line 11
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->b:I

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->c:I

    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 10
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a:I

    .line 11
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->b:I

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->c:I

    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 10
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a:I

    .line 11
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->b:I

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->c:I

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a()V

    .line 27
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getTextSize()F

    move-result v0

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 33
    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    cmpl-float v0, v1, v2

    if-lez v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getTextSize()F

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a:I

    .line 36
    :cond_0
    return-void
.end method


# virtual methods
.method public buttonTextSizeResize(II)I
    .locals 5

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 53
    int-to-float v1, p2

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 56
    sub-float/2addr v0, v3

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 59
    :goto_0
    cmpg-float v4, v0, v1

    if-gez v4, :cond_1

    .line 60
    add-int/lit8 p1, p1, -0x1

    .line 61
    const/16 v4, 0xa

    if-lt p1, v4, :cond_0

    .line 62
    const/4 v1, 0x1

    int-to-float v4, p1

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setTextSize(IF)V

    .line 63
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :cond_1
    return p1
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 40
    invoke-super/range {p0 .. p5}, Landroid/widget/Button;->onLayout(ZIIII)V

    .line 41
    if-eqz p1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 43
    iget v1, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->c:I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->c:I

    .line 44
    const/4 v0, 0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->a:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->c:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->buttonTextSizeResize(II)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setTextSize(IF)V

    .line 46
    :cond_0
    return-void
.end method
