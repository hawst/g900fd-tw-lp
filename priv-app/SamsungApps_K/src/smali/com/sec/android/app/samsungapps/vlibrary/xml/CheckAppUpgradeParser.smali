.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 11
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {p1, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "2gLimit"

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_2gLimit:I

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "25gLimit"

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_25gLimit:I

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "3gLimit"

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_3gLimit:I

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "4gLimit"

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_4gLimit:I

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "wifiLimit"

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_wifiLimit:I

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "disableBaidu"

    invoke-virtual {p1, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disableBaidu:Z

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    const-string v1, "enableTencent"

    invoke-virtual {p1, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->enableTencent:I

    .line 29
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
