.class public Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;
.super Landroid/app/Service;
.source "ProGuard"


# instance fields
.field a:Landroid/os/Handler;

.field b:Lcom/sec/android/app/samsungapps/api/aidl/IInstallAPI$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a:Landroid/os/Handler;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/api/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/api/a;-><init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->b:Lcom/sec/android/app/samsungapps/api/aidl/IInstallAPI$Stub;

    .line 171
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/api/c;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/api/c;-><init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Landroid/content/Intent;

    const-string v1, "samsungapps.receiver.intent.action.INSTALL_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/api/b;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/api/b;-><init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;)Z
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/api/SChecker;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/api/SChecker;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/api/SChecker;->checkSignatureForBind()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 19
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->fullPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v0

    move-object v1, p0

    move-object v3, p2

    move-object v5, p3

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;->b:Lcom/sec/android/app/samsungapps/api/aidl/IInstallAPI$Stub;

    return-object v0
.end method
