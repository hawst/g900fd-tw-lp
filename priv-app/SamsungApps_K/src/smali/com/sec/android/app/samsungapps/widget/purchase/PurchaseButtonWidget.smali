.class public Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;


# instance fields
.field a:Landroid/widget/CheckBox;

.field b:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Z

.field private h:I

.field private i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

.field private j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget$OnPurchaseCancelListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->g:Z

    .line 41
    const v0, 0x7f0400af

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->initView(Landroid/content/Context;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->g:Z

    .line 46
    const v0, 0x7f0400af

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->initView(Landroid/content/Context;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->g:Z

    .line 51
    const v0, 0x7f0400af

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->initView(Landroid/content/Context;I)V

    .line 52
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isKorea()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isPaymentAbleCondition()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->g:Z

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 157
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isPaymentAbleCondition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0c0144

    const v4, 0x7f0c0140

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 213
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 215
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 216
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 223
    :goto_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->onPositive()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/af;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/af;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    .line 240
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->f:Landroid/widget/Button;

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->f:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->f:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->f:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->onNegative()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->f:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/ag;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ag;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    .line 255
    return-void

    .line 220
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 221
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 164
    const v0, 0x7f0c0274

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->b:Landroid/widget/TextView;

    .line 165
    const v0, 0x7f0c0273

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->d:Landroid/widget/TextView;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->b:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->getDiscountPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->getNormalPrice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->getNormalPrice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->updateWidget()V

    .line 57
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->refreshWidget()V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;->configurationChangedComplete()V

    .line 310
    :cond_0
    return-void
.end method

.method public onNegative()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 287
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/ai;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ai;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V

    return-object v0
.end method

.method public onPositive()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 258
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/ah;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ah;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V

    return-object v0
.end method

.method public onPriceChanged()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->b()V

    .line 316
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a()V

    .line 317
    return-void
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->updateWidget()V

    .line 65
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->updateWidget()V

    .line 205
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 278
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->release()V

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    .line 284
    :cond_0
    return-void
.end method

.method public setInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    .line 61
    return-void
.end method

.method public setOnPurchaseCancelListener(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget$OnPurchaseCancelListener;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget$OnPurchaseCancelListener;

    .line 299
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 199
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    .line 200
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public updateWidget()V
    .locals 8

    .prologue
    const v3, 0x7f0801c4

    const v7, 0x7f08023d

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    if-nez v0, :cond_1

    .line 71
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setVisibility(I)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    const v0, 0x7f0c0244

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a:Landroid/widget/CheckBox;

    .line 75
    const v0, 0x7f0c009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->b()V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    if-eqz v1, :cond_6

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isFreeProduct()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f080348

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_2
    :goto_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    if-eq v1, v5, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isKorea()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 99
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/ad;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ad;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/purchase/ae;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ae;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 126
    :cond_3
    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->e:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    .line 131
    :cond_4
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    if-eq v0, v5, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a()V

    goto :goto_0

    .line 87
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 89
    const v1, 0x7f0c0275

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 95
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 120
    :cond_7
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->h:I

    if-ne v1, v5, :cond_3

    .line 122
    const v1, 0x7f0c0271

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method
