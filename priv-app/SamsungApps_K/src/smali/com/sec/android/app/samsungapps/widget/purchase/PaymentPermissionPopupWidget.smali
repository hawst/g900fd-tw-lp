.class public Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Ljava/util/HashMap;

.field b:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Z

.field f:[Ljava/lang/Object;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

.field private h:Ljava/util/ArrayList;

.field private i:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->e:Z

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->b:Landroid/widget/TextView;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->d:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0400a9

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->initView(Landroid/content/Context;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    .line 64
    const v0, 0x7f0400a9

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->initView(Landroid/content/Context;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    .line 54
    const v0, 0x7f0400a9

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->initView(Landroid/content/Context;I)V

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;I)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 26
    new-instance v6, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-boolean v11, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->e:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040096

    invoke-virtual {v0, v1, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0217

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    move v4, v5

    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v3, 0x7f040097

    invoke-virtual {v2, v3, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v3, 0x7f0c0219

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->b:Landroid/widget/TextView;

    const v3, 0x7f0c021a

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-ne v8, v11, :cond_4

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionID()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v3, ""

    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->b:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getDescription()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->d:Landroid/widget/TextView;

    if-nez v3, :cond_2

    const-string v3, ""

    :cond_2
    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_0

    :cond_4
    const/4 v8, 0x0

    :try_start_1
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v9, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    invoke-virtual {v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    const v1, 0x7f0802ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    return-void

    :catch_0
    move-exception v3

    goto :goto_2
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->updateWidget()V

    .line 70
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 189
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 190
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 194
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 199
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 204
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->a:Ljava/util/HashMap;

    .line 207
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->f:[Ljava/lang/Object;

    .line 208
    return-void
.end method

.method public setPermission(Ljava/util/HashMap;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->f:[Ljava/lang/Object;

    move v1, v2

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->f:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->f:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->i:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->f:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 104
    :cond_0
    return-void
.end method

.method public setPermissionData(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V
    .locals 1

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getPermissionList()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->a:Ljava/util/HashMap;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->existPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->a:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->setPermission(Ljava/util/HashMap;)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->setVisibility(I)V

    goto :goto_0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public updateWidget()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    const v0, 0x7f0c01fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f080199

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 83
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 84
    new-instance v1, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0400a7

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 85
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 86
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/u;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/u;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionPopupWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 93
    return-void
.end method
