.class public Lcom/sec/android/app/samsungapps/widget/SectionListView;
.super Landroid/widget/LinearLayout;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/widget/ListView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a()V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a()V

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/SectionListView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 68
    const v1, 0x7f0400e5

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 69
    if-nez v2, :cond_0

    .line 71
    const-string v0, "SectionListView::init::Layout inflate failed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 132
    :goto_0
    return-void

    .line 75
    :cond_0
    const v1, 0x7f0c0079

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    .line 76
    const v1, 0x7f0c02f9

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 82
    const v1, 0x7f0c02f8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 83
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/ao;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/ao;-><init>(Lcom/sec/android/app/samsungapps/widget/SectionListView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 93
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->showSectionScroller(Z)V

    .line 95
    const v1, 0x7f0400e2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/ap;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/ap;-><init>(Lcom/sec/android/app/samsungapps/widget/SectionListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/SectionListView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected findTracksTextView(II)Landroid/widget/TextView;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 357
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 360
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    move v2, v3

    move-object v1, v4

    .line 362
    :goto_0
    if-ge v2, v5, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 365
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setPressed(Z)V

    .line 366
    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 368
    if-nez v1, :cond_1

    .line 371
    const/4 v7, 0x2

    new-array v7, v7, [I

    .line 372
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->getLocationInWindow([I)V

    .line 374
    aget v8, v7, v3

    .line 375
    const/4 v9, 0x1

    aget v7, v7, v9

    .line 376
    invoke-virtual {v6}, Landroid/view/Display;->getWidth()I

    move-result v9

    .line 377
    invoke-virtual {v0}, Landroid/widget/TextView;->getHeight()I

    move-result v10

    add-int/2addr v10, v7

    .line 378
    if-lt p1, v8, :cond_1

    if-gt p1, v9, :cond_1

    if-lt p2, v7, :cond_1

    if-gt p2, v10, :cond_1

    .line 362
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 384
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    .line 201
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getCurrentSectionPosition(II)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move v1, v2

    .line 390
    :goto_0
    if-ge v1, v3, :cond_1

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 393
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setPressed(Z)V

    .line 394
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 396
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 397
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->getLocationInWindow([I)V

    .line 399
    aget v5, v4, v2

    .line 400
    const/4 v6, 0x1

    aget v4, v4, v6

    .line 401
    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v6

    add-int/2addr v6, v5

    .line 402
    invoke-virtual {v0}, Landroid/widget/TextView;->getHeight()I

    move-result v0

    add-int/2addr v0, v4

    .line 403
    if-lt p1, v5, :cond_0

    if-gt p1, v6, :cond_0

    if-lt p2, v4, :cond_0

    if-gt p2, v0, :cond_0

    move v0, v1

    .line 409
    :goto_1
    return v0

    .line 390
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 409
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 434
    const/4 v0, -0x1

    .line 437
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    goto :goto_0
.end method

.method protected onTracksViewTouchEvent(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 291
    .line 292
    :try_start_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    .line 297
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    float-to-int v3, v3

    .line 305
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 306
    packed-switch v4, :pswitch_data_0

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 299
    :catch_0
    move-exception v1

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SectionListView::onTracksViewTouchEvent::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :pswitch_0
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->findTracksTextView(II)Landroid/widget/TextView;

    move-result-object v2

    .line 311
    if-eqz v2, :cond_0

    .line 314
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    .line 319
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 320
    invoke-virtual {v2, v5, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 322
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/aq;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/aq;-><init>(Lcom/sec/android/app/samsungapps/widget/SectionListView;B)V

    .line 324
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->f:Landroid/os/Handler;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->getSectionPostion(Ljava/lang/String;)I

    move-result v0

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    move v0, v1

    .line 335
    goto :goto_0

    .line 339
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setPressed(Z)V

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 145
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->c:Landroid/widget/TextView;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 158
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    .line 161
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    .line 162
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->d:Landroid/widget/TextView;

    .line 163
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SectionListView::release::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 174
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->showSectionScroller(Z)V

    .line 176
    :cond_0
    return-void
.end method

.method public setListOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 188
    :cond_0
    return-void
.end method

.method public setPostion(I)V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 423
    :cond_0
    return-void
.end method

.method public showSectionScroller(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 221
    const-string v0, "SectionListView::showSectionScroller::TracksView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    if-ne p1, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 227
    if-ne p1, v3, :cond_7

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SectionListView;->getAdapter()Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    move-result-object v0

    .line 230
    if-nez v0, :cond_3

    .line 232
    const-string v0, "SectionListView::showSectionScroller::adapter is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    .line 236
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->getSections()[Ljava/lang/Object;

    move-result-object v4

    .line 237
    if-eqz v4, :cond_4

    array-length v0, v4

    if-nez v0, :cond_5

    .line 239
    :cond_4
    const-string v0, "SectionListView::showSectionScroller::sections is empty"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_6

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    :cond_6
    move v3, v1

    .line 258
    :goto_2
    array-length v1, v4

    if-ge v3, v1, :cond_0

    .line 260
    const v1, 0x7f0400e1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 261
    aget-object v2, v4, v3

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 258
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 268
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0
.end method
