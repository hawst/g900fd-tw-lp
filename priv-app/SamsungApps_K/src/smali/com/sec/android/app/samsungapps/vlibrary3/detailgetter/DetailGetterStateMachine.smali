.class public Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 35
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;

    .line 19
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 66
    const-string v0, "DetailGetterStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 84
    :goto_0
    :pswitch_0
    return-void

    .line 72
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;->ON_REQUEST_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 81
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;->ON_REQUEST_FORCE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)Z
    .locals 3

    .prologue
    .line 24
    const-string v0, "DetailGetterStateMachine"

    const-string v1, "excute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 25
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 61
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 28
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 31
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->REQUEST_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->REQUEST_FORCE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 42
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 53
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_6
    .end packed-switch

    .line 28
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 39
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 50
    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 88
    const-string v0, "DetailGetterStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    return-void
.end method
