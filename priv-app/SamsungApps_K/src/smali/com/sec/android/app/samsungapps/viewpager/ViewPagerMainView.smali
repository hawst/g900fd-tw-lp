.class public Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;


# static fields
.field public static final MAIN_PAGE_INDEX:I

.field public static bBackPressed:Z


# instance fields
.field a:Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

.field b:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;

.field c:J

.field d:Z

.field private e:I

.field private f:Z

.field private g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

.field private h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

.field private i:Landroid/hardware/SensorManager;

.field private j:Landroid/hardware/Sensor;

.field private k:I

.field private l:I

.field private m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

.field protected mContext:Landroid/content/Context;

.field protected mFinishFlag:Z

.field protected mFinishToast:Landroid/widget/Toast;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->bBackPressed:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0xff

    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 44
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishFlag:Z

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mContext:Landroid/content/Context;

    .line 49
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->e:I

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->f:Z

    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->b:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;

    .line 60
    iput v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->k:I

    .line 61
    iput v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->l:I

    .line 299
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->n:I

    .line 321
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->c:J

    .line 383
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendMainPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V

    .line 213
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    .line 217
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 218
    instance-of v1, v0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;

    if-eqz v1, :cond_2

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    .line 226
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1, v2, v2, v2}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->createLogDataForPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 228
    return-void

    .line 220
    :cond_2
    instance-of v1, v0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    if-eqz v1, :cond_3

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    goto :goto_0

    .line 222
    :cond_3
    instance-of v0, v0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)Landroid/app/Activity;
    .locals 0

    .prologue
    .line 42
    return-object p0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 390
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    .line 393
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->d:Z

    .line 395
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public initSensorManager()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->j:Landroid/hardware/Sensor;

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 415
    return-void
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 282
    const/4 v0, 0x0

    .line 283
    packed-switch p1, :pswitch_data_0

    .line 296
    :goto_0
    :pswitch_0
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 297
    return-void

    .line 285
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 286
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 290
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/CategoryListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onAutoLoginResult(Z)V
    .locals 0

    .prologue
    .line 405
    invoke-static {p1, p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->requestAndShowNoticeList(ZLandroid/content/Context;)V

    .line 406
    if-eqz p1, :cond_0

    .line 407
    invoke-static {p1, p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->requestAndShowType6NoticeList(ZLandroid/content/Context;)V

    .line 409
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 326
    sput-boolean v6, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->bBackPressed:Z

    .line 327
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 329
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->c:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xfa0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishFlag:Z

    if-ne v2, v6, :cond_1

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 333
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishFlag:Z

    .line 334
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->getTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps(Ljava/lang/Integer;)V

    .line 341
    :goto_0
    return-void

    .line 336
    :cond_1
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishFlag:Z

    .line 337
    const v2, 0x7f0801d3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    .line 339
    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->c:J

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f040075

    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    sput-boolean v2, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->bBackPressed:Z

    .line 77
    const-string v0, "VerificationLog"

    const-string v3, "onCreate"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->setDoNotInitGlobal()V

    .line 79
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 80
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->getFakeModel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 83
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/Main;->getCacheCleanManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->toBMode()V

    .line 88
    :goto_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    const v4, 0xb0001

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->setActionBarConfiguration(Ljava/lang/String;Z[II)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0xa0001

    invoke-virtual {v0, v3, v4, v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;

    .line 93
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mContext:Landroid/content/Context;

    .line 94
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->setMainViewAndEmptyView(I)V

    .line 100
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->initSensorManager()V

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->f:Z

    .line 103
    const v0, 0xa0005

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->enableActionItem(IZ)V

    const v0, 0xa0007

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->enableActionItem(IZ)V

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c01bd

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0c01bc

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 104
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/aa;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/aa;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 114
    const-string v0, "VerificationLog"

    const-string v1, "Executed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-void

    :cond_2
    move v0, v1

    .line 82
    goto/16 :goto_0

    .line 85
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/Main;->getCacheCleanManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->toNormalMode()V

    goto/16 :goto_1

    .line 97
    :cond_4
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->setMainView(I)V

    goto :goto_2

    .line 88
    :array_0
    .array-data 4
        0xa0005
        0xa0007
    .end array-data
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 260
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->cancel()V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishToast:Landroid/widget/Toast;

    .line 266
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishFlag:Z

    .line 267
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->f:Z

    .line 269
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 270
    return-void
.end method

.method public onInitCompleted(I)V
    .locals 8

    .prologue
    const v7, 0x7f0c01bd

    const v3, 0x7f0c01bc

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 304
    sget-boolean v0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->bBackPressed:Z

    if-ne v0, v5, :cond_0

    .line 319
    :goto_0
    return-void

    .line 308
    :cond_0
    iput p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->n:I

    .line 310
    const v0, 0xa0008

    invoke-virtual {p0, v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)V

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "viewpager or tab is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 312
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->initialized()V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    if-eqz v0, :cond_7

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getCurrentTabDescription(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 310
    :cond_3
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->n:I

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;I)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a:Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a:Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a:Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/viewpager/PagerAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->setOffscreenPageLimit(I)V

    if-eqz v0, :cond_4

    if-ne v0, v5, :cond_6

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->setCurrentItem(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/ab;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/ab;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->requestUpdateCheck()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/ac;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/ac;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;)V

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->a(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setCurrentTabSelect(I)V

    const v0, 0xa0005

    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->enableActionItem(IZ)V

    const v0, 0xa0007

    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->enableActionItem(IZ)V

    goto :goto_1

    :cond_6
    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->h:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->setCurrentItem(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->g:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setVisibility(I)V

    goto :goto_2

    .line 317
    :cond_7
    const-string v0, "ViewPagerMainView onInitCompleted(): tabs is NULL"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onInitFailed()V
    .locals 0

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->finish()V

    .line 401
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendMainPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V

    .line 348
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->d:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_3

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    .line 362
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onPause()V

    .line 363
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 367
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 369
    const-string v0, "VerificationLog"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1, v2, v2, v2}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->createLogDataForPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->m:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->j:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->i:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->j:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 379
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mFinishFlag:Z

    .line 380
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->d:Z

    .line 381
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4

    .prologue
    .line 419
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 420
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 422
    iget v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->k:I

    if-eq v2, v0, :cond_1

    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->l:I

    iget v3, v1, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v3, :cond_1

    .line 424
    :cond_0
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->l:I

    .line 425
    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->k:I

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->invalidate()V

    .line 429
    :cond_1
    return-void
.end method

.method protected requestUpdateCheck()V
    .locals 1

    .prologue
    .line 276
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->updateCheck()V

    .line 277
    return-void
.end method
