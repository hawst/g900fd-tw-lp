.class final Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final packageDeleted(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 378
    if-eq p2, v1, :cond_0

    .line 381
    const-string v0, "[ERROR] Package delete return code : %d"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iput p2, v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$300(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$300(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 388
    :cond_1
    return-void
.end method

.method public final packageDeleted(Z)V
    .locals 2

    .prologue
    .line 394
    if-nez p1, :cond_0

    .line 396
    const-string v0, "[ERROR] Package delete is failed(this is called under GB)"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iput-boolean p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->isSuccessful:Z

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$300(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$300(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 402
    return-void
.end method
