.class public Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

.field private b:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private e:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

.field private f:Landroid/widget/ListView;

.field private g:Ljava/util/ArrayList;

.field private h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/g;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->h:Landroid/view/View$OnClickListener;

    .line 35
    const v0, 0x7f04003f

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->initView(Landroid/content/Context;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/g;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->h:Landroid/view/View$OnClickListener;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->b:Landroid/content/Context;

    .line 41
    const v0, 0x7f04003f

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->initView(Landroid/content/Context;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/g;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->h:Landroid/view/View$OnClickListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->b:Landroid/content/Context;

    .line 47
    const v0, 0x7f04003f

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->initView(Landroid/content/Context;I)V

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-nez v0, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const v0, 0x7f0c0130

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 87
    const v1, 0x7f0c0131

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 88
    const v1, 0x7f0c0134

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 90
    if-eqz v0, :cond_2

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080221

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    new-array v3, v11, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount2()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :cond_2
    const v0, 0x7f0c0133

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout;

    .line 95
    if-eqz v6, :cond_0

    .line 100
    const v0, 0x7f0c0132

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    .line 101
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->b:Landroid/content/Context;

    const v2, 0x7f04003e

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;ILcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->e:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->e:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v11}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 108
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 109
    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 119
    :cond_3
    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 112
    :cond_4
    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    .line 113
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount2()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_3

    .line 116
    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->updateWidget()V

    .line 150
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->b:Landroid/content/Context;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->clear()V

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->e:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->e:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->clear()V

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->e:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    .line 75
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->f:Landroid/widget/ListView;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->h:Landroid/view/View$OnClickListener;

    .line 77
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 78
    return-void
.end method

.method public setReviewsWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    .line 60
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 52
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    .line 56
    :cond_0
    return-void

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    const/16 v3, 0xa

    if-ge v1, v3, :cond_0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->isSeller()Z

    move-result v3

    if-nez v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updateWidget()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-nez v0, :cond_0

    .line 130
    :cond_0
    return-void
.end method
