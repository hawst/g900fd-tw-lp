.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
.implements Lcom/sec/android/app/samsungapps/vlibrary/purchase/PurchaseMethodAvailablity;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPaymentPriceGetter;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;


# instance fields
.field private _mCustomerBoughtContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field private _mCyberCashInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

.field private _mOptBillList:Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;

.field private _mPrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

.field private _mSellerTag:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;

.field public aliPay:Ljava/lang/String;

.field public bundleYN:Z

.field public compatibleOS:Ljava/lang/String;

.field public contentGradeImgUrl:Ljava/lang/String;

.field public contentLanguage:Ljava/lang/String;

.field public createDate:Ljava/lang/String;

.field public creditcardYN:Z

.field public discountPhoneBillPrice:D

.field public discountPhoneBillYn:Z

.field public lastUpdateDate:Ljava/lang/String;

.field public multipleDeviceNCount:Ljava/lang/String;

.field public nameAuthYN:Ljava/lang/String;

.field public optBillList:Ljava/lang/String;

.field public optBillYn:Z

.field public orderItemSeq:Ljava/lang/String;

.field public pSmsPurchaseYN:Z

.field public phonebillPrice:D

.field public phonebillYN:Z

.field public prepaidCardList:Ljava/lang/String;

.field public productCommentCnt:I

.field public productDescription:Ljava/lang/String;

.field public productStatus:I

.field public purchasedMethod:Ljava/lang/String;

.field public realContentsSize:J

.field public rentalPhonebillPriceArray:Ljava/lang/String;

.field public rentalPriceArray:Ljava/lang/String;

.field public rentalTermArray:Ljava/lang/String;

.field public rentalYn:Z

.field public restrictedAge:Ljava/lang/String;

.field public sellerMedal:Ljava/lang/String;

.field public sellerReputation:Ljava/lang/String;

.field public sellerUrl:Ljava/lang/String;

.field public settlementRate:D

.field public shopID:Ljava/lang/String;

.field public supportEmail:Ljava/lang/String;

.field public testCreditcardYn:Z

.field public testPSMSPurchaseYn:Z

.field public tnbYn:Z

.field public trialYN:Z

.field public validDate:Ljava/lang/String;

.field public youtubeRtspUrl:Ljava/lang/String;

.field public youtubeScreenShoutUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 81
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createCustomerAlsoBought(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mCustomerBoughtContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mCyberCashInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mPrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mOptBillList:Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;

    .line 90
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    const/4 v1, 0x0

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 91
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;

    const-string v1, "sellertag"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mSellerTag:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;

    .line 95
    return-void
.end method


# virtual methods
.method public existPrepaidCardList()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->prepaidCardList:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->prepaidCardList:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlipayPaymentMethod()Ljava/lang/String;
    .locals 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->aliPay:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->aliPay:Ljava/lang/String;

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 453
    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 454
    const-string v0, ""

    .line 458
    :goto_0
    return-object v0

    .line 456
    :cond_0
    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 458
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getChinaPrepaidCardInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mPrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    return-object v0
.end method

.method public getCustomerBoughtContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mCustomerBoughtContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    return-object v0
.end method

.method public getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mCyberCashInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    return-object v0
.end method

.method public getDiscountPSMSPrice()D
    .locals 2

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountPhoneBillYn:Z

    if-eqz v0, :cond_0

    .line 438
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountPhoneBillPrice:D

    .line 440
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->phonebillPrice:D

    goto :goto_0
.end method

.method public getDiscountPhoneBillPrice()D
    .locals 2

    .prologue
    .line 501
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountPhoneBillPrice:D

    return-wide v0
.end method

.method public getDiscountPrice()D
    .locals 2

    .prologue
    .line 423
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountFlag:Z

    if-eqz v0, :cond_0

    .line 425
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->reducePrice:D

    .line 427
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellingPrice:D

    goto :goto_0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getMultipleDeviceDownloadCount()Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;
    .locals 2

    .prologue
    .line 364
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->multipleDeviceNCount:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/MultipleDeviceDownloadCount;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getNormalPSMSPrice()D
    .locals 2

    .prologue
    .line 432
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->phonebillPrice:D

    return-wide v0
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellingPrice:D

    return-wide v0
.end method

.method public getOptBillInfoList()Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mOptBillList:Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderItemSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderItemSeq:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentPrice()D
    .locals 2

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountFlag:Z

    if-eqz v0, :cond_0

    .line 339
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->reducePrice:D

    .line 341
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellingPrice:D

    goto :goto_0
.end method

.method public getPaymentPrice(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;
    .locals 6

    .prologue
    .line 471
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 476
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellingPrice:D

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->reducePrice:D

    iget-boolean v5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountFlag:Z

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;-><init>(DDZ)V

    :goto_0
    return-object v0

    .line 474
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->phonebillPrice:D

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountPhoneBillPrice:D

    iget-boolean v5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountPhoneBillYn:Z

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;-><init>(DDZ)V

    goto :goto_0

    .line 471
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getPhoneBillPrice()D
    .locals 2

    .prologue
    .line 496
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->phonebillPrice:D

    return-wide v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getRealContentSize()J
    .locals 2

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->realContentsSize:J

    return-wide v0
.end method

.method public getReducePrice()D
    .locals 2

    .prologue
    .line 516
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->reducePrice:D

    return-wide v0
.end method

.method public getRestrictedAge()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 172
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->restrictedAge:Ljava/lang/String;

    const-string v2, "+"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 177
    const-string v1, "0"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :cond_0
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 188
    :goto_0
    return v0

    .line 186
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 188
    const/4 v0, 0x0

    goto :goto_0

    .line 183
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public getSellerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellerID:Ljava/lang/String;

    return-object v0
.end method

.method public getSellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellerName:Ljava/lang/String;

    return-object v0
.end method

.method public getSellerTag()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mSellerTag:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;

    return-object v0
.end method

.method public getSellingPrice()D
    .locals 2

    .prologue
    .line 511
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellingPrice:D

    return-wide v0
.end method

.method public getWishState()Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;
    .locals 3

    .prologue
    .line 369
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->wishListYn:Z

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->alreadyPurchased:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;-><init>(ZZ)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOrderID()Z
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x1

    .line 234
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOrderItemSeq()Z
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderItemSeq:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderItemSeq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    const/4 v0, 0x1

    .line 247
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAlipayPurchaseSupported()Z
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->aliPay:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->aliPay:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCreditCardPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->creditcardYN:Z

    return v0
.end method

.method public isCreditCardSupported()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->creditcardYN:Z

    if-eqz v0, :cond_0

    .line 128
    const/4 v0, 0x1

    .line 130
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCyberCashSupported()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mCyberCashInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDirectBillingPurchaseAvailable()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 394
    .line 396
    const/4 v2, 0x6

    new-array v3, v2, [Ljava/lang/String;

    const-string v2, "BOG"

    aput-object v2, v3, v1

    const-string v2, "SFR"

    aput-object v2, v3, v0

    const/4 v2, 0x2

    const-string v4, "O2DE"

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string v4, "DETLF"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string v4, "ESTLF"

    aput-object v4, v3, v2

    const/4 v2, 0x5

    const-string v4, "UKTLF"

    aput-object v4, v3, v2

    .line 398
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->optBillYn:Z

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->optBillList:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->optBillList:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 400
    :cond_0
    const-string v0, "ContentDetail::isDirectBillingPurchaseAvailable::Not Supported Opt"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    move v0, v1

    .line 413
    :cond_1
    :goto_0
    return v0

    .line 404
    :cond_2
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 406
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->optBillList:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eq v5, v0, :cond_1

    .line 404
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public isDiscounted()Z
    .locals 1

    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountFlag:Z

    return v0
.end method

.method public isExecutable(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 351
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isKnoxMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isKnoxApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isExecutable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 359
    :goto_0
    return v0

    .line 358
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isExecutable(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isFreeContent()Z
    .locals 4

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 120
    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKnoxApp()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    const-string v2, "for KNOX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 312
    :cond_0
    :goto_0
    return v0

    .line 309
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    const-string v2, "Samsung KNOX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNameAuthRequired(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 194
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v2

    if-nez v2, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->nameAuthYN:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->nameAuthYN:Ljava/lang/String;

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->nameAuthYN:Ljava/lang/String;

    const-string v3, "V"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRestrictedAge()I

    move-result v2

    const/16 v3, 0x12

    if-lt v2, v3, :cond_0

    move v0, v1

    .line 204
    goto :goto_0

    .line 209
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRestrictedAge()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 210
    goto :goto_0
.end method

.method public isPPCSupported()Z
    .locals 1

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->existPrepaidCardList()Z

    move-result v0

    return v0
.end method

.method public isPSMSPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->pSmsPurchaseYN:Z

    return v0
.end method

.method public isPSMSPurchaseSupported()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->pSmsPurchaseYN:Z

    return v0
.end method

.method public isPhonbillSupported()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->phonebillYN:Z

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPhoneBillDiscounted()Z
    .locals 1

    .prologue
    .line 506
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountPhoneBillYn:Z

    return v0
.end method

.method public isPhoneBillPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->phonebillYN:Z

    return v0
.end method

.method public isPurchaseDetail()Z
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public isPurchased()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 167
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->alreadyPurchased:Z

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTestPSMSPurchaseSupported()Z
    .locals 1

    .prologue
    .line 325
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isTestPurchaseSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->testPSMSPurchaseYn:Z

    .line 328
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrialSupported()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->trialYN:Z

    return v0
.end method

.method public launch(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 290
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isKnoxMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isKnoxApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    .line 301
    :goto_0
    return v2

    .line 298
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->launchApp(Ljava/lang/String;Z)Z

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->notifyBroadcastIntentForLaunchApp(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 334
    return-void
.end method

.method public setChinaPrepaidCardInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mPrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    .line 286
    return-void
.end method

.method public setContentSize(J)V
    .locals 0

    .prologue
    .line 320
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->contentsSize:J

    .line 321
    return-void
.end method

.method public setCyberCashInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mCyberCashInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    .line 276
    return-void
.end method

.method public setOptBillInfoList(Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->_mOptBillList:Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;

    .line 262
    return-void
.end method

.method public setOrderID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    .line 317
    return-void
.end method
