.class final Lcom/sec/android/app/samsungapps/bk;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

.field final synthetic b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/bk;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 811
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    if-eqz p1, :cond_2

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 798
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Review User review is successful.    mUserCommentList size is ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 800
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetData(ILjava/lang/Object;)V

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->loadWidget(I)V

    .line 808
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->onWidgetSetViewState()V

    goto :goto_0

    .line 805
    :cond_2
    const-string v0, "Review User review is failed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 806
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bk;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetState(I)V

    goto :goto_1
.end method
