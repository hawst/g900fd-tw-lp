.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

.field protected _TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->confirmPayment(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    return-object v0
.end method

.method private confirmPayment(Z)V
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;->getResponseTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;->getRetryCount()I

    move-result v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/h;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->createTimeOutRetrier(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 70
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/i;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;->getConfirmResult()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->requestConfirmPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 94
    return-void
.end method


# virtual methods
.method protected createTimeOutRetrier(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;-><init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V

    .line 54
    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;->getRetryCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->confirmPayment(Z)V

    .line 32
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onReceiveConfirmResult(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    if-eqz p1, :cond_2

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;->isFinalMapSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 103
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->onFinalResult(Z)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->retry()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 112
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->onFinalResult(Z)V

    goto :goto_0

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 121
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->onFinalResult(Z)V

    goto :goto_0
.end method

.method public requestConfirmPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_IPSMSConfirmData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 38
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 40
    return-void
.end method

.method protected sendRequest(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->confirmSMSPurchaseNS(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 49
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 50
    return-void
.end method
