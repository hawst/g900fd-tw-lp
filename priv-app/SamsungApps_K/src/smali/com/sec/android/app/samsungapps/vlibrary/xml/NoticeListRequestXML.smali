.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 16
    const-string v2, "noticeList"

    const-string v3, "2341"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 18
    const-string v0, "startNum"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeList()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getStartPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const-string v0, "endNum"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeList()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getEndPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected getItemCountPerPage()I
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0xf

    return v0
.end method
