.class public Lcom/sec/android/app/samsungapps/LoadingDialog;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/LoadingDialog;->a:Landroid/content/Context;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/LoadingDialog;->a:Landroid/content/Context;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/LoadingDialog;->a:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method public end()V
    .locals 1

    .prologue
    .line 68
    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    if-eqz v0, :cond_0

    .line 70
    sget-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_0
    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    .line 76
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setCancelable(Z)V
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->setCancelable(Z)V

    .line 84
    :cond_0
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/LoadingDialog;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/LoadingDialog;->a:Landroid/content/Context;

    .line 49
    :cond_0
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/LoadingDialog;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;-><init>(Landroid/content/Context;)V

    .line 50
    sput-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->setCancelable(Z)V

    .line 51
    sget-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-static {}, Lcom/sec/android/app/samsungapps/NotiDialog;->getSearchKeyDisabled()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 52
    sget-object v0, Lcom/sec/android/app/samsungapps/LoadingDialog;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
