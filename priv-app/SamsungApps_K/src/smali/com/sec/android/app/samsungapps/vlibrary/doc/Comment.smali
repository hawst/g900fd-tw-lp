.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;


# instance fields
.field public averageRating:I

.field public commentID:Ljava/lang/String;

.field public date:Ljava/lang/String;

.field public loginID:Ljava/lang/String;

.field mCommentDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;

.field public productComment:Ljava/lang/String;

.field public sellerCommentYn:Z

.field public userID:Ljava/lang/String;

.field public userName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->mCommentDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;

    .line 27
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 28
    return-void
.end method


# virtual methods
.method public compareUserID(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->userID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->userID:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->productComment:Ljava/lang/String;

    return-object v0
.end method

.method public getCommentDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->mCommentDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getExpertSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpertTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpertUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpertUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->commentID:Ljava/lang/String;

    return-object v0
.end method

.method public getLoginID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->loginID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "-"

    .line 81
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v2

    if-nez v2, :cond_2

    .line 99
    :cond_0
    :goto_1
    return-object v0

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->loginID:Ljava/lang/String;

    goto :goto_0

    .line 86
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    .line 87
    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->compareUserID(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 88
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    const-string v0, "@"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 92
    if-lez v0, :cond_3

    .line 93
    const/4 v0, 0x0

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 95
    goto :goto_1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->userName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "-"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->userName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->averageRating:I

    return v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->userID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "-"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->userID:Ljava/lang/String;

    goto :goto_0
.end method

.method public isExpertComment()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public isSeller()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->sellerCommentYn:Z

    return v0
.end method

.method public setCommentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->mCommentDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;

    .line 165
    return-void
.end method
