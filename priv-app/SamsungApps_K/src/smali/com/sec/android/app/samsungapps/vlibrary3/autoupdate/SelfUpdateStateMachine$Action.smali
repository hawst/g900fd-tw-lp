.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum NOTIFY_UPDATED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum REQ_DOWNLOADEX:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum REQ_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum SET_FOREGROUND_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum UNSET_FOREGROUND_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field public static final enum UPDATING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "REQ_UPDATE_CHECK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->REQ_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "REQ_DOWNLOADEX"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->REQ_DOWNLOADEX:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "UPDATING"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->UPDATING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "NOTIFY_UPDATED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->NOTIFY_UPDATED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "SET_FOREGROUND_PROCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->SET_FOREGROUND_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    const-string v1, "UNSET_FOREGROUND_PROCESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->UNSET_FOREGROUND_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    .line 19
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->REQ_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->REQ_DOWNLOADEX:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->UPDATING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->NOTIFY_UPDATED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->SET_FOREGROUND_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->UNSET_FOREGROUND_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Action;

    return-object v0
.end method
