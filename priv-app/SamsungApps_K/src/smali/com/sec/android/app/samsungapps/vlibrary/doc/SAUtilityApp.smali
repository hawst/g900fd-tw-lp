.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private apkInfo:Ljava/lang/String;

.field private mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private mContext:Landroid/content/Context;

.field private mDeaultVersion:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPkgFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mContext:Landroid/content/Context;

    .line 21
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 23
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mDeaultVersion:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mName:Ljava/lang/String;

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->makeApkInfoToRequest()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->apkInfo:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mContext:Landroid/content/Context;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 34
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mDeaultVersion:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mName:Ljava/lang/String;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->makeApkInfoToRequest()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->apkInfo:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mContext:Landroid/content/Context;

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 45
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mDeaultVersion:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mName:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPkgFileName:Ljava/lang/String;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->makeApkInfoToRequest()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->apkInfo:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private makeApkInfoToRequest()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 54
    const-string v0, ""

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    const-string v2, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;-><init>()V

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->getSamsungAppsVersion()Ljava/lang/String;

    move-result-object v0

    .line 80
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, ""

    .line 82
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "@0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    :goto_2
    return-object v0

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    const-string v2, "com.sec.android.app.samsungapps.una2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPacakgeVersionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 80
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 84
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mDeaultVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public compareGUID(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getApkInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->apkInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPkgFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPkgFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCodeList()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v0

    .line 111
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
