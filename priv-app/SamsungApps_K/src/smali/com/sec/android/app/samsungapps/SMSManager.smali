.class public Lcom/sec/android/app/samsungapps/SMSManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/telephony/SmsManager;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Landroid/app/PendingIntent;

.field g:Landroid/app/PendingIntent;

.field h:Landroid/content/BroadcastReceiver;

.field i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

.field j:I

.field private final k:I

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/content/Intent;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 20
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 46
    const/4 v0, -0x4

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->k:I

    .line 48
    const-string v0, "ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->l:Ljava/lang/String;

    .line 49
    const-string v0, "android.provider.Telephony.SMS_RECEIVED"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->m:Ljava/lang/String;

    .line 108
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 109
    iput p2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    invoke-static {v0, p2, p3, p4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/SMSManager$Listener;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 20
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 46
    const/4 v0, -0x4

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->k:I

    .line 48
    const-string v0, "ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->l:Ljava/lang/String;

    .line 49
    const-string v0, "android.provider.Telephony.SMS_RECEIVED"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->m:Ljava/lang/String;

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    new-instance v2, Landroid/content/Intent;

    const-string v3, "ACTION_MSG_SENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/SMSManager$Listener;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 20
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 46
    const/4 v0, -0x4

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->k:I

    .line 48
    const-string v0, "ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->l:Ljava/lang/String;

    .line 49
    const-string v0, "android.provider.Telephony.SMS_RECEIVED"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->m:Ljava/lang/String;

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 75
    iput p3, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "ACTION_MSG_SENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {v0, p3, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/SMSManager$Listener;ILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 20
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 46
    const/4 v0, -0x4

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->k:I

    .line 48
    const-string v0, "ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->l:Ljava/lang/String;

    .line 49
    const-string v0, "android.provider.Telephony.SMS_RECEIVED"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->m:Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    .line 91
    iput p3, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p3, p4, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 93
    return-void
.end method

.method private a()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/sec/android/app/samsungapps/hq;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hq;-><init>(Lcom/sec/android/app/samsungapps/SMSManager;)V

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 123
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    .line 124
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    .line 125
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 127
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 128
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    .line 129
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    .line 130
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 131
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 132
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    return-object v0
.end method

.method public getDeliveryIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public getDestinationNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 395
    const-string v0, ""

    .line 397
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 402
    :cond_0
    return-object v0
.end method

.method public getManager()Landroid/telephony/SmsManager;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    const-string v0, ""

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 434
    :cond_0
    return-object v0
.end method

.method public getRequestCode()I
    .locals 1

    .prologue
    .line 461
    iget v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    return v0
.end method

.method public getSentIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public getSourceNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 411
    const-string v0, ""

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 418
    :cond_0
    return-object v0
.end method

.method public sendMessage()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    if-nez v0, :cond_1

    :cond_0
    move v0, v6

    .line 230
    :goto_0
    return v0

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_2

    move v0, v6

    .line 188
    goto :goto_0

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_3

    .line 198
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 201
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_4

    move v0, v6

    .line 203
    goto :goto_0

    .line 206
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    if-eqz v0, :cond_5

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_5

    .line 210
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SMSManager;->a()Landroid/content/BroadcastReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "ACTION_MSG_SENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 218
    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    move v0, v7

    .line 230
    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    const/4 v4, -0x4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/SMSManager$Listener;->onSendResult(Landroid/content/Context;Landroid/content/Intent;II)V

    move v0, v6

    .line 224
    goto :goto_0
.end method

.method public sendMessage(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 241
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_1

    :cond_0
    move v0, v6

    .line 291
    :goto_0
    return v0

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    if-nez v0, :cond_3

    :cond_2
    move v0, v6

    .line 250
    goto :goto_0

    .line 253
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_4

    .line 260
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 263
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v7, :cond_5

    move v0, v6

    .line 265
    goto :goto_0

    .line 268
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    if-eqz v0, :cond_6

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_6

    .line 272
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SMSManager;->a()Landroid/content/BroadcastReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "ACTION_MSG_SENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 277
    :cond_6
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->b:Landroid/telephony/SmsManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/SMSManager;->g:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    move v0, v7

    .line 291
    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    const/4 v4, -0x4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/SMSManager$Listener;->onSendResult(Landroid/content/Context;Landroid/content/Intent;II)V

    move v0, v6

    .line 286
    goto :goto_0
.end method

.method public setDestinationNumber(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 304
    const/4 v0, 0x0

    .line 306
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 308
    const/4 v0, 0x1

    .line 309
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->c:Ljava/lang/String;

    .line 312
    :cond_0
    return v0
.end method

.method public setMessage(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 344
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 346
    const/4 v0, 0x1

    .line 347
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->e:Ljava/lang/String;

    .line 350
    :cond_0
    return v0
.end method

.method public setRequestCode(I)Z
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    .line 362
    if-ltz p1, :cond_0

    .line 364
    const/4 v0, 0x1

    .line 365
    iput p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->j:I

    .line 368
    :cond_0
    return v0
.end method

.method public setSourceNumber(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 325
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 327
    const/4 v0, 0x1

    .line 328
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMSManager;->d:Ljava/lang/String;

    .line 331
    :cond_0
    return v0
.end method
