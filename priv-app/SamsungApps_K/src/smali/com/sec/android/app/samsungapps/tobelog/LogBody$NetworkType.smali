.class public final enum Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum MOBILE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

.field public static final enum WIFI:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->MOBILE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->WIFI:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->MOBILE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->WIFI:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    return-object v0
.end method
