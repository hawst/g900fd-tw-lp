.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mContext:Landroid/content/Context;

.field mObserverList:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;

.field private mState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Loading:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mObserverList:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;

    .line 64
    return-void
.end method

.method private notifyUpdate(I)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mObserverList:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;

    .line 39
    invoke-interface {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;->update(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;I)V

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mObserverList:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    return-object v0
.end method

.method public abstract getID()Ljava/lang/String;
.end method

.method protected getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final getState()Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    return-object v0
.end method

.method protected getThis()Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;
    .locals 0

    .prologue
    .line 89
    return-object p0
.end method

.method public abstract getType()J
.end method

.method public abstract onPrepare()V
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mObserverList:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;->remove(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method protected sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 80
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mContext:Landroid/content/Context;

    .line 18
    return-void
.end method

.method final setState(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;)V
    .locals 1

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->notifyUpdate(I)V

    .line 62
    return-void
.end method

.method final setState(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;I)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->mState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 55
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->notifyUpdate(I)V

    .line 56
    return-void
.end method
