.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodAvailabilityChecker;


# static fields
.field public static final ALIPAY_PKGNAME:Ljava/lang/String; = "com.alipay.android.app"

.field public static final SAMSUNG_WALLET_PKGNAME:Ljava/lang/String; = "com.sec.android.wallet"


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _Context:Landroid/content/Context;

.field private _ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

.field private _IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    .line 25
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_Context:Landroid/content/Context;

    .line 28
    return-void
.end method

.method private isChina()Z
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;->getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->China:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFrance()Z
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;->getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->France:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGermany()Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;->getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Germany:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isIran()Z
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;->getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Iran:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isKorea()Z
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;->getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Korea:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUkraine()Z
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_ICountry:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;->getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Ukraine:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAvailable(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z
    .locals 6

    .prologue
    const-wide v4, 0x408f400000000000L    # 1000.0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 65
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isKorea()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isPhoneBillPurchaseAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPaymentPrice()D

    move-result-wide v2

    cmpg-double v2, v2, v4

    if-gez v2, :cond_2

    move v0, v1

    .line 73
    goto :goto_0

    .line 75
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isKorea()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isCreditCardPurchaseAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    .line 79
    goto :goto_0

    .line 81
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPaymentPrice()D

    move-result-wide v2

    cmpg-double v2, v2, v4

    if-gez v2, :cond_4

    move v0, v1

    .line 83
    goto :goto_0

    .line 85
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isKorea()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 89
    goto :goto_0

    .line 93
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isChina()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isAlipayPurchaseSupported()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 95
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_Context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 96
    const-string v3, "com.alipay.android.app"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    .line 97
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->needAlipayUpdate()Z

    move-result v3

    .line 98
    if-eq v2, v0, :cond_0

    if-eq v3, v0, :cond_0

    :cond_5
    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isChina()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isCyberCashSupported()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    .line 111
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isChina()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isPPCSupported()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    move v0, v1

    goto/16 :goto_0

    .line 113
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isKorea()Z

    move-result v2

    if-eqz v2, :cond_8

    move v0, v1

    .line 115
    goto/16 :goto_0

    .line 117
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isChina()Z

    move-result v2

    if-eqz v2, :cond_9

    move v0, v1

    .line 119
    goto/16 :goto_0

    .line 121
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isPSMSPurchaseAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isTestPSMSPurchaseSupported()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isTestPurchaseSupported()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_a
    move v0, v1

    goto/16 :goto_0

    .line 123
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isFrance()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isGermany()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 125
    goto/16 :goto_0

    .line 127
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isDirectBillingPurchaseAvailable()Z

    move-result v0

    goto/16 :goto_0

    .line 129
    :pswitch_8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isChina()Z

    move-result v2

    if-eqz v2, :cond_c

    move v0, v1

    .line 131
    goto/16 :goto_0

    .line 133
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isIran()Z

    move-result v2

    if-nez v2, :cond_d

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isUkraine()Z

    move-result v2

    if-nez v2, :cond_d

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isKorea()Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isCreditCardPurchaseAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_d
    move v0, v1

    .line 137
    goto/16 :goto_0

    .line 139
    :pswitch_9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isUkraine()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_IPaymentMethodCheckInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;->isCreditCardPurchaseAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_e
    move v0, v1

    .line 143
    goto/16 :goto_0

    .line 145
    :pswitch_a
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isIran()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 149
    goto/16 :goto_0

    .line 152
    :pswitch_b
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->isKorea()Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 154
    goto/16 :goto_0

    .line 156
    :cond_f
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->_Context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 157
    const-string v3, "com.sec.android.wallet"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 161
    goto/16 :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public getAvaliableList()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;
    .locals 6

    .prologue
    .line 169
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;-><init>()V

    .line 170
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->values()[Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 172
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentMethodAvailabilityChecker;->getAvailable(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 174
    invoke-virtual {v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_1
    return-object v1
.end method
