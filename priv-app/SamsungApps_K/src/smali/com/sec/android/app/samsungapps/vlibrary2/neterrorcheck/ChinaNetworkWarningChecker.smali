.class public Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _ICheckChinaChargeWarnCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->_ICheckChinaChargeWarnCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    .line 17
    return-void
.end method

.method private checkChinaConnectWlanWarnDoNotShowAgain()V
    .locals 2

    .prologue
    .line 97
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeChinaConnectWlanWarnDoNotShowAgain(Z)V

    .line 99
    return-void
.end method

.method private checkChinaDataChargeWarnDoNotShowAgain()V
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeChinaDataChargeWarnDoNotShowAgain(Z)V

    .line 104
    return-void
.end method

.method private invokePopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;Z)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->_ICheckChinaChargeWarnCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;)V

    invoke-interface {v0, p1, v1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;->showChinaNetworkWarningPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IChinaNetworkWarninigSetter;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;Z)V

    .line 76
    return-void
.end method

.method private isChina()Z
    .locals 1

    .prologue
    .line 84
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 85
    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v0

    goto :goto_0
.end method

.method private isChinaConnectWlanWarnDoNotShowAgain()Z
    .locals 1

    .prologue
    .line 92
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isChinaConnectWlanWarnDoNotShowAgain()Z

    move-result v0

    return v0
.end method

.method private isChinaDataChargeWarnDoNotShowAgain()Z
    .locals 1

    .prologue
    .line 79
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isChinaDataChargeWarnDoNotShowAgain()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->isChina()Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;->onOk()V

    .line 60
    :goto_0
    return-void

    .line 26
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    .line 27
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v1

    .line 29
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->isChinaConnectWlanWarnDoNotShowAgain()Z

    move-result v0

    if-nez v0, :cond_1

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/a;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;)V

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->invokePopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;Z)V

    goto :goto_0

    .line 43
    :cond_1
    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->isChinaDataChargeWarnDoNotShowAgain()Z

    move-result v0

    if-nez v0, :cond_2

    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/b;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->invokePopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;Z)V

    goto :goto_0

    .line 58
    :cond_2
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;->onOk()V

    goto :goto_0
.end method

.method public setCheckDataChargeDoNotShowAgain()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->checkChinaDataChargeWarnDoNotShowAgain()V

    .line 108
    return-void
.end method

.method public setCheckWlanConnectDoNotShowAgain()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->checkChinaConnectWlanWarnDoNotShowAgain()V

    .line 112
    return-void
.end method
