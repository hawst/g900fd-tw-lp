.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;
.super Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
.source "ProGuard"


# instance fields
.field mAccountEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->mAccountEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    .line 21
    return-void
.end method

.method public static LogedIn()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedIn:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    return-object v0
.end method

.method public static LogoutFailed()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogOutFailed:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    return-object v0
.end method

.method public static LogoutSuccess()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    return-object v0
.end method

.method public static requestAutoLogin(Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->RequestAutoLogin:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    .line 60
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->setResponseRequired()V

    .line 61
    return-object v0
.end method

.method public static requestSignUp(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->RequestSignUp:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    .line 66
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->setExtraData(Ljava/lang/Object;)V

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->setResponseRequired()V

    .line 68
    return-object v0
.end method


# virtual methods
.method public getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->mAccountEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    return-object v0
.end method
