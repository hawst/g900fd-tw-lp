.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# instance fields
.field _NetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 64
    const-string v2, "MicroPayment"

    const-string v3, "3200"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->_NetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 65
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->_NetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 67
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 69
    const-string v0, "guid"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v0, "mcc"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v0, "mnc"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v0, "authNID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getAuthNID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 74
    const-string v0, "authPNum"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getAuthPNum()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/DigitExtractor;->extract(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 75
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->phoneEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    const-string v0, "usimCD"

    const-string v1, "U"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :goto_0
    const-string v1, "agreeCD"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->userAgreed()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "01"

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "carrier"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getCarrierName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 85
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->isRequestAuthNumber()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 86
    const-string v0, "typeCD"

    const-string v1, "A"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :goto_2
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->phoneEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    const-string v0, "otp"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getUserEntererdConfirmMSGNo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 103
    :cond_0
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->phoneEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    const-string v0, "udid"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_1
    const-string v0, "reserved01"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "reserved02"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v0, "reserved03"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "reserved04"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v0, "reserved05"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "rentalTerm"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getCouponIssuedSEQ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 114
    const-string v0, "couponIssuedSEQ"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->getCouponIssuedSEQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 116
    :cond_2
    return-void

    .line 81
    :cond_3
    const-string v0, "usimCD"

    const-string v1, "K"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 83
    :cond_4
    const-string v0, "00"

    goto :goto_1

    .line 90
    :cond_5
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;->phoneEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 92
    const-string v0, "typeCD"

    const-string v1, "U"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 96
    :cond_6
    const-string v0, "typeCD"

    const-string v1, "K"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method protected phoneEnabled()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method
