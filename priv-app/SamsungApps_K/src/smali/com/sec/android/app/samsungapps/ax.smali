.class final Lcom/sec/android/app/samsungapps/ax;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 1901
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClickExpandReview(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const v1, 0x7f0c00a4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 1957
    if-eqz v0, :cond_0

    if-eqz v0, :cond_0

    if-nez v0, :cond_1

    .line 1963
    :cond_0
    :goto_0
    return-void

    .line 1960
    :cond_1
    invoke-virtual {v0, p1, p2}, Landroid/widget/ScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1961
    invoke-virtual {v0}, Landroid/widget/ScrollView;->requestFocus()Z

    goto :goto_0
.end method

.method public final onClickUserReviewDelete(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V
    .locals 3

    .prologue
    .line 1933
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->deleteComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/az;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/az;-><init>(Lcom/sec/android/app/samsungapps/ax;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 1951
    return-void
.end method

.method public final onClickUserReviewEdit(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V
    .locals 3

    .prologue
    .line 1919
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->modifyComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/ay;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ay;-><init>(Lcom/sec/android/app/samsungapps/ax;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 1929
    return-void
.end method

.method public final onClickUserReviewMore()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1908
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1910
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1911
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_REVIEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ax;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    .line 1914
    :cond_0
    return-void
.end method
