.class public final enum Lcom/sec/android/app/samsungapps/tobelog/LogPage;
.super Ljava/lang/Enum;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/tobelog/ILogPage;


# static fields
.field public static final enum CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum NOTIFICATION_BAR:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum SEARCH:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum SETTINGS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public static final enum STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field private static final synthetic b:[Lcom/sec/android/app/samsungapps/tobelog/LogPage;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "STAFF_PICKS"

    const-string v2, "1000"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "CHART"

    const-string v2, "1001"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "CATEGORY_LIST"

    const-string v2, "1002"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "SEARCH"

    const-string v2, "2000"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SEARCH:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "SEARCH_RESULT"

    const-string v2, "2001"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "CATEGORY_PRODUCT_LIST"

    const/4 v2, 0x5

    const-string v3, "9000"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "PRODUCT_DETAIL"

    const/4 v2, 0x6

    const-string v3, "9001"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "CONTENTS_SET_LIST"

    const/4 v2, 0x7

    const-string v3, "9002"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "NOTIFICATION_BAR"

    const/16 v2, 0x8

    const-string v3, "3000"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->NOTIFICATION_BAR:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "SETTINGS"

    const/16 v2, 0x9

    const-string v3, "3001"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SETTINGS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const-string v1, "EMPTY"

    const/16 v2, 0xa

    const-string v3, "9999"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 5
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SEARCH:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->NOTIFICATION_BAR:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SETTINGS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->b:[Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->a:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public static fromName(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 4

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->values()[Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v1

    .line 38
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 39
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 40
    aget-object v0, v1, v0

    .line 44
    :goto_1
    return-object v0

    .line 38
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 1

    .prologue
    .line 5
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->b:[Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/LogPage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    return-object v0
.end method


# virtual methods
.method public final value()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->a:Ljava/lang/String;

    return-object v0
.end method
