.class public Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final HOVER_TYPE_BIG:I = 0x2

.field public static final HOVER_TYPE_CHART:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static removeHovering(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 65
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->removeHovering(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public static setBubbleHovering(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Landroid/widget/ScrollView;I)V
    .locals 18

    .prologue
    .line 57
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 58
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 59
    new-instance v7, Lcom/sec/android/app/samsungapps/view/bb;

    const/4 v2, 0x0

    invoke-direct {v7, v2}, Lcom/sec/android/app/samsungapps/view/bb;-><init>(B)V

    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual/range {p3 .. p3}, Landroid/widget/ScrollView;->getWidth()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v6, 0x7f07000e

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const/4 v10, 0x2

    move/from16 v0, p4

    if-ne v0, v10, :cond_5

    const v10, 0x7f07000c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    const v11, 0x7f07000d

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v2, v10

    add-int/2addr v4, v9

    :cond_0
    :goto_0
    add-int/2addr v4, v6

    div-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_a

    div-int/lit8 v2, v6, 0x2

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->c:I

    if-le v8, v4, :cond_6

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_13

    const v2, 0x7f0c019c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v6

    :goto_1
    neg-int v2, v2

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    const v2, 0x7f04006c

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    const/16 v2, 0x5053

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    .line 60
    :cond_1
    :goto_2
    iget v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    iget v8, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    iget v9, v7, Lcom/sec/android/app/samsungapps/view/bb;->c:I

    iget v7, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    new-instance v10, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->getCustomView()Landroid/view/View;

    move-result-object v11

    const v2, 0x7f0c01a5

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    const v3, 0x7f0c01a2

    invoke-virtual {v11, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0c01a3

    invoke-virtual {v11, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0c01a4

    invoke-virtual {v11, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RatingBar;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_2

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    move-object/from16 v0, p2

    iget-wide v14, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountPrice:D

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08029b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v3, 0x7f0c0063

    invoke-virtual {v11, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0c0062

    invoke-virtual {v11, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p2

    iget-boolean v11, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountFlag:Z

    if-eqz v11, :cond_11

    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v11

    or-int/lit8 v11, v11, 0x10

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setPaintFlags(I)V

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v12

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    move-wide/from16 v0, v16

    invoke-virtual {v12, v0, v1, v13}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-wide/16 v11, 0x0

    cmpl-double v3, v14, v11

    if-nez v3, :cond_10

    move-object v3, v6

    :goto_3
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_4
    if-eqz v5, :cond_3

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    move-object/from16 v0, p2

    iget v6, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->averageRating:I

    int-to-double v11, v6

    mul-double/2addr v3, v11

    double-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/widget/RatingBar;->setRating(F)V

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->setViewToBeHovered(Landroid/view/View;)V

    const/4 v2, 0x0

    invoke-virtual {v10, v9, v7, v8, v2}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->showAirView(IIIZ)V

    .line 62
    :cond_4
    return-void

    .line 59
    :cond_5
    const/4 v10, 0x1

    move/from16 v0, p4

    if-ne v0, v10, :cond_0

    const v10, 0x7f07001c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    sub-int/2addr v3, v10

    const v10, 0x7f070023

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    add-int/2addr v4, v10

    const v10, 0x7f070024

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    add-int/2addr v4, v10

    const v10, 0x7f070022

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v4, v9

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getTopIndicatorHeight(Landroid/content/Context;)I

    move-result v9

    sub-int/2addr v5, v9

    goto/16 :goto_0

    :cond_6
    sub-int v2, v4, v8

    if-ge v2, v5, :cond_8

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_7

    const v2, 0x7f0c019c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v6, v2

    :cond_7
    neg-int v2, v6

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    const v2, 0x7f04006c

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    const/16 v2, 0x5053

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    goto/16 :goto_2

    :cond_8
    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_9

    const v2, 0x7f0c0199

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v6, v2

    :cond_9
    iput v6, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    const v2, 0x7f04006e

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    const/16 v2, 0x3033

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    goto/16 :goto_2

    :cond_a
    neg-int v2, v6

    div-int/lit8 v2, v2, 0x2

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->c:I

    if-le v8, v4, :cond_c

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_b

    const v2, 0x7f0c019c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v6, v2

    :cond_b
    neg-int v2, v6

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    const v2, 0x7f04006d

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    const/16 v2, 0x5055

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    goto/16 :goto_2

    :cond_c
    sub-int v2, v4, v8

    if-ge v2, v5, :cond_e

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_d

    const v2, 0x7f0c019c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v6, v2

    :cond_d
    neg-int v2, v6

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    const v2, 0x7f04006d

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    const/16 v2, 0x5055

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    goto/16 :goto_2

    :cond_e
    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_f

    const v2, 0x7f0c0199

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v6, v2

    :cond_f
    iput v6, v7, Lcom/sec/android/app/samsungapps/view/bb;->d:I

    const v2, 0x7f04006f

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->a:I

    const/16 v2, 0x3035

    iput v2, v7, Lcom/sec/android/app/samsungapps/view/bb;->b:I

    goto/16 :goto_2

    .line 60
    :cond_10
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v3, v14, v15, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :cond_11
    const/16 v11, 0x8

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    const-wide/16 v14, 0x0

    cmpl-double v3, v12, v14

    if-nez v3, :cond_12

    :goto_5
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_12
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v3, v12, v13, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    :cond_13
    move v2, v6

    goto/16 :goto_1
.end method

.method public static setFHAnimation(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 70
    if-eqz p1, :cond_0

    .line 71
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->isEnabledAirView(Landroid/content/ContentResolver;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v0, p2}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 78
    :cond_0
    return-void
.end method

.method public static setTextViewHovering(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 26
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;

    const/4 v1, -0x1

    invoke-direct {v0, p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;-><init>(Landroid/content/Context;II)V

    .line 28
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->setViewToBeHovered(Landroid/view/View;)V

    .line 29
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->showToolTip(Ljava/lang/String;)V

    .line 30
    if-eqz p1, :cond_0

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    .line 34
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
