.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field private static final serialVersionUID:J = -0x37de452b51614a64L


# instance fields
.field private _Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 37
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->clear()V

    .line 42
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 26
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->add(Ljava/lang/Object;)Z

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 29
    :cond_0
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 19
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method
