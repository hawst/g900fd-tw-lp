.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 8
    const-string v2, "termInformation"

    const-string v3, "5060"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 9
    return-void
.end method

.method public static help(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;
    .locals 3

    .prologue
    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 14
    const-string v1, "flag"

    const-string v2, "3"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v1, "fileFlag"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    return-object v0
.end method
