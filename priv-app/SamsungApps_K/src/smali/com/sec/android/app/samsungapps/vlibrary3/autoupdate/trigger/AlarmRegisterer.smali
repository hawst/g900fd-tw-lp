.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->a:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->b:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public checkAlarmExist()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 36
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 38
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->a:Landroid/content/Context;

    const/high16 v3, 0x20000000

    invoke-static {v1, v0, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 40
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public register(J)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->a:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 26
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 28
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 29
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->a:Landroid/content/Context;

    invoke-static {v1, v4, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 31
    const/4 v1, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    move-wide v4, p1

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 32
    return-void
.end method
