.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 20
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 35
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 24
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 25
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;-><init>()V

    .line 40
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/ObjectWriter;->setFieldValueFromMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Object;Z)Z

    .line 41
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 30
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method
