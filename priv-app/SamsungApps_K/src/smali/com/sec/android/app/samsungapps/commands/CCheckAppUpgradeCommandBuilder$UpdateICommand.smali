.class public Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    new-instance v1, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const v3, 0x7f0400d9

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->setDialogBody()V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 372
    return-void
.end method

.method public sendFinalResult(Z)V
    .locals 2

    .prologue
    .line 375
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/Main;->_OldDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    if-eqz v0, :cond_0

    .line 376
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;-><init>(Landroid/content/Context;)V

    .line 377
    sget-object v1, Lcom/sec/android/app/samsungapps/Main;->_OldDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;->save(Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;)V

    .line 379
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder$UpdateICommand;->onFinalResult(Z)V

    .line 380
    return-void
.end method
