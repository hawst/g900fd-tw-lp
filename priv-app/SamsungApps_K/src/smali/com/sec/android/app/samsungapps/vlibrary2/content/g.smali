.class final Lcom/sec/android/app/samsungapps/vlibrary2/content/g;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    if-nez p2, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;Z)V

    .line 124
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->isValid()Z

    move-result v0

    if-nez v0, :cond_2

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;Z)V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_IGetDetailOverviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setDetailOverview(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;Z)V

    goto :goto_0
.end method
