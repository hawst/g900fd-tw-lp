.class public Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;


# instance fields
.field _CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    .line 19
    return-void
.end method


# virtual methods
.method public clearCreditCardInfo()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    .line 39
    return-void
.end method

.method public endLoading()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 70
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 71
    return-void
.end method

.method public getCreditCardInfo()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    return-object v0
.end method

.method public isCreditCardInfoValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 44
    if-nez v1, :cond_1

    .line 46
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v2

    if-nez v2, :cond_0

    .line 52
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public loadCreditCardInfo()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;)V

    return-object v0
.end method

.method public setCreditCardInfo(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    .line 34
    return-void
.end method

.method public startLoading()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 63
    return-void
.end method
