.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/ISigChecker;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;

    .line 20
    return-void
.end method

.method private static a(Ljava/security/cert/Certificate;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 53
    check-cast p0, Ljava/security/cert/X509Certificate;

    .line 54
    if-eqz p0, :cond_1

    .line 55
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v1

    .line 56
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 57
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, v1, v0

    .line 58
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;

    if-eqz v0, :cond_0

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;->getCertificateBytes(Ljava/lang/String;)[B

    move-result-object v0

    const-string v5, "X.509"

    invoke-static {v5}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v5

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;->a(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v3, v5, v3

    .line 33
    const-string v5, "hcjo"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "elapsed time for sig check "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "hcjo"

    const-string v3, "sig success"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v1

    .line 49
    :goto_0
    return v0

    .line 40
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V

    move v0, v2

    .line 42
    goto :goto_0

    .line 43
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 45
    const-string v0, "hcjo"

    const-string v2, "sig check not possible"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 46
    goto :goto_0

    .line 48
    :cond_0
    const-string v0, "hcjo"

    const-string v1, "sig failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 49
    goto :goto_0
.end method


# virtual methods
.method public validate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
