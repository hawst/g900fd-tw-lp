.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _ContentList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

.field private _List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

.field _ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

.field gear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

.field mContext:Landroid/content/Context;

.field mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

.field xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    .line 70
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mContext:Landroid/content/Context;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->request()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->onFinalResult(Z)V

    return-void
.end method

.method private isGear2()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->isGear2(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private request()V
    .locals 5

    .prologue
    const/16 v3, 0xf

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    if-nez v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getStartNumber()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getFirstEndNum()I

    move-result v0

    if-le v0, v3, :cond_1

    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getFirstEndNum()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-direct {v0, v3, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;-><init>(IILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 89
    :goto_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->isGear2(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getNextStartNumber()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getNextEndNumber()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->gear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;->purchaseListExMulti(IILcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    move-result-object v0

    .line 100
    :goto_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/f;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;)V

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->request(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 115
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 86
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-direct {v0, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;-><init>(ILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    goto :goto_1

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->xmlGen:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getNextStartNumber()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getNextEndNumber()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;->purchaseListExMulti(II)Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->isGear2()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->gear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->gear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->setConnectionObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager$IGearAPIConnectionStateObserver;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->gear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->connect()V

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->request()V

    goto :goto_0
.end method
