.class public Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;


# instance fields
.field bKill:Z

.field mHttpClient:Lorg/apache/http/client/HttpClient;

.field mStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

.field netapi:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

.field requestFileQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

.field requestImageQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

.field requestPostQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

.field requestUrlQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->netapi:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->bKill:Z

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 51
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->netapi:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 52
    return-void
.end method

.method private addReq(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    .locals 2

    .prologue
    .line 227
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->isPOST()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 229
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setNetStageCounter(Lcom/sec/android/app/samsungapps/vlibrary/net/b;)V

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/b;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setStageNo(I)V

    .line 233
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->getThreadNo()I

    move-result v0

    .line 234
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->getRequestQueue(I)Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 235
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->getRequestQueue(I)Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;->addRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 237
    :cond_1
    return-void
.end method

.method private createHttpClient(Z)Lorg/apache/http/client/HttpClient;
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 72
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 73
    const-string v0, "http.protocol.version"

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-interface {v1, v0, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 75
    const/16 v0, 0x32

    .line 76
    if-eqz p1, :cond_0

    .line 79
    const/16 v0, 0x5a

    .line 88
    :cond_0
    new-instance v2, Lorg/apache/http/HttpHost;

    sget-object v3, Lorg/apache/http/conn/params/ConnRouteParams;->NO_HOST:Lorg/apache/http/HttpHost;

    invoke-direct {v2, v3}, Lorg/apache/http/HttpHost;-><init>(Lorg/apache/http/HttpHost;)V

    .line 89
    const-string v3, "http.route.default-proxy"

    invoke-interface {v1, v3, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 91
    mul-int/lit16 v0, v0, 0x3e8

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 92
    const v0, 0xc350

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 93
    const/16 v0, 0x2000

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 94
    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 95
    invoke-static {v1, v4}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 97
    const-string v0, "http.conn-manager.max-per-route"

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/net/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    invoke-interface {v1, v0, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 105
    :try_start_0
    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v0

    .line 111
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStageDataHostURL()Ljava/lang/String;

    .line 127
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeAllowAllHost(Z)V

    .line 130
    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 133
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v5

    const/16 v6, 0x50

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 134
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "https"

    const/16 v5, 0x1bb

    invoke-direct {v3, v4, v0, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 136
    new-instance v3, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v3, v1, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 138
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0, v3, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private endThread()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->bKill:Z

    .line 194
    return-void
.end method

.method private getRequestQueue(I)Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;
    .locals 1

    .prologue
    .line 56
    packed-switch p1, :pswitch_data_0

    .line 63
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 58
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestPostQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestImageQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    goto :goto_0

    .line 60
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestFileQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    goto :goto_0

    .line 61
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestUrlQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private startThread()V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->bKill:Z

    .line 188
    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/http/client/methods/HttpGet;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public increaseStageNo()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/b;->a()V

    .line 199
    return-void
.end method

.method public init()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestPostQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    .line 152
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestImageQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    .line 153
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestFileQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    .line 154
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->requestUrlQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadManager;

    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->createHttpClient(Z)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 158
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/b;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->startThread()V

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->increaseStageNo()V

    .line 162
    return-void
.end method

.method public sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    .locals 0

    .prologue
    .line 219
    if-nez p1, :cond_0

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->addReq(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method public shutDown()V
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->endThread()V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 204
    return-void
.end method

.method public updateHttpClient()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-nez v2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isAllowAllHost()Z

    move-result v2

    .line 176
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStageDataHostURL()Ljava/lang/String;

    move-result-object v3

    .line 177
    if-eqz v3, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eq v3, v0, :cond_2

    .line 178
    :goto_1
    if-eq v2, v0, :cond_0

    .line 180
    const-string v0, "NetAPI::updateHttpClient::Again Create HTTP Client"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 181
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->createHttpClient(Z)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 177
    goto :goto_1
.end method
