.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisCommunicator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCommunicator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public requestComplete(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->completeInicisMobilePurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 31
    return-void
.end method

.method public requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 16
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->initInicisMobilePurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 18
    invoke-virtual {v0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 20
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 21
    return-void
.end method
