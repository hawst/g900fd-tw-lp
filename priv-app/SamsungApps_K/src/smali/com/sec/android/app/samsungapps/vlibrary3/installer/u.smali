.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final packageInstalled(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 321
    if-ne p2, v0, :cond_1

    .line 322
    :goto_0
    if-nez v0, :cond_0

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->_silnceInstallErrCode:Ljava/lang/String;

    .line 325
    :cond_0
    if-eqz v0, :cond_3

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 335
    :goto_1
    return-void

    .line 321
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto :goto_1

    .line 333
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/u;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto :goto_1
.end method
