.class public Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mRequestXML:Ljava/lang/String;

.field private mURL:Ljava/lang/String;

.field private textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mURL:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mRequestXML:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mURL:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public execute(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 93
    const/4 v1, 0x0

    .line 95
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->makeHttpPost()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v1

    .line 96
    invoke-interface {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;->execute(Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 97
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    invoke-direct {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;-><init>(Lorg/apache/http/HttpResponse;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->isStatusCode200OK()Z
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_8

    move-result v0

    .line 158
    :goto_0
    return v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 101
    throw v0

    .line 102
    :catch_1
    move-exception v0

    .line 103
    invoke-virtual {v0}, Lorg/apache/http/conn/ConnectTimeoutException;->printStackTrace()V

    .line 104
    throw v0

    .line 106
    :catch_2
    move-exception v0

    .line 108
    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 112
    :cond_0
    invoke-virtual {v0}, Lorg/apache/http/conn/HttpHostConnectException;->printStackTrace()V

    .line 113
    throw v0

    .line 115
    :catch_3
    move-exception v0

    .line 117
    if-eqz v1, :cond_1

    .line 119
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 121
    :cond_1
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 122
    throw v0

    .line 124
    :catch_4
    move-exception v0

    .line 126
    if-eqz v1, :cond_2

    .line 128
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 130
    :cond_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 131
    throw v0

    .line 133
    :catch_5
    move-exception v0

    .line 135
    if-eqz v1, :cond_3

    .line 137
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 139
    :cond_3
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 140
    throw v0

    .line 142
    :catch_6
    move-exception v0

    .line 144
    if-eqz v1, :cond_4

    .line 146
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 148
    :cond_4
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 149
    throw v0

    .line 151
    :catch_7
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 156
    :catch_8
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getResultString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    if-nez v0, :cond_0

    .line 167
    const-string v0, ""

    .line 169
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public is200OK()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    if-nez v0, :cond_0

    .line 175
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->textresp:Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->isStatusCode200OK()Z

    move-result v0

    goto :goto_0
.end method

.method protected makeHttpPost()Lorg/apache/http/client/methods/HttpPost;
    .locals 5

    .prologue
    .line 34
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mURL:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 37
    const-string v1, "User-Agent"

    const-string v2, "SAMSUNG-Android"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 41
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getHubHost()Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getHeaderHost()Ljava/lang/String;

    move-result-object v2

    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    .line 45
    if-nez v3, :cond_3

    .line 47
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getInfinityVersion()Ljava/lang/String;

    move-result-object v1

    .line 68
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 71
    const-string v2, "Infinity-Version"

    invoke-virtual {v0, v2, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mRequestXML:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mRequestXML:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mRequestXML:Ljava/lang/String;

    .line 81
    new-instance v2, Lorg/apache/http/entity/StringEntity;

    const-string v3, "UTF-8"

    invoke-direct {v2, v1, v3}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :cond_2
    :goto_1
    return-object v0

    .line 51
    :cond_3
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getHubURL()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->mURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 53
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const-string v2, "Host"

    invoke-virtual {v0, v2, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    const-string v1, "Host"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method
