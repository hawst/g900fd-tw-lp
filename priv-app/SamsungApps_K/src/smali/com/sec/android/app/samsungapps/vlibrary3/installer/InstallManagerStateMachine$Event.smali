.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum B_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum B_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum B_PREPARE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum B_PREPARE_SUCCESS_NOT_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum FOREGROUND_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum INSTALL_KNOX_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum INSTALL_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum INSTALL_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum INSTALL_SYSTEM_APP_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum KNOX_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum RECEIVED_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum SILENCE_INSTALL_API_CALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "INSTALL_SYSTEM_APP"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "INSTALL_NOT_SYSTEM_APP"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "INSTALL_KNOX_APP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_KNOX_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "SILENCE_INSTALL_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "SILENCE_INSTALL_COMPLETED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "SILENCE_INSTALL_API_CALL_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_API_CALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "KNOX_INSTALL_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->KNOX_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "KNOX_INSTALL_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "FOREGROUND_INSTALL_COMPLETED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->FOREGROUND_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "REQUEST_B_INSTALL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "B_INSTALL_SUCCESS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "B_INSTALL_FAILED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "INSTALL_SYSTEM_APP_BMODE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "B_PREPARE_SUCCESS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_PREPARE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "INSTALL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "RECEIVED_LOCK"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->RECEIVED_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    const-string v1, "B_PREPARE_SUCCESS_NOT_SYSTEMAPP"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_PREPARE_SUCCESS_NOT_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    .line 19
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_KNOX_APP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->SILENCE_INSTALL_API_CALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->KNOX_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->FOREGROUND_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL_SYSTEM_APP_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_PREPARE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->RECEIVED_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_PREPARE_SUCCESS_NOT_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    return-object v0
.end method
