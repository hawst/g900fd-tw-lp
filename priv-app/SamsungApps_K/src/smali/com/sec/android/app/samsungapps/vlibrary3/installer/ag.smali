.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager$IGearAPIConnectionStateObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConnected()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;)V

    .line 47
    return-void
.end method

.method public final onConnectionFailed()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "onConnectionFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;

    const-string v1, "WO:FAILED"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public final onDisconnected()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "WGTInstallerStateMachine"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ag;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;

    const-string v1, "WO:DISCONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;Ljava/lang/String;)V

    .line 35
    return-void
.end method
