.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFileFake;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
.source "ProGuard"


# instance fields
.field private mTextFileReader:Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;)V

    .line 21
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFileFake;->mTextFileReader:Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;

    .line 22
    return-void
.end method


# virtual methods
.method protected concrete_sendpacket(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/net/g;
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFileFake;->mTextFileReader:Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;->readFile()Ljava/lang/String;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/net/g;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/g;-><init>()V

    .line 30
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->b:Z

    .line 31
    iput-object v0, v1, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->a:Ljava/lang/String;

    .line 32
    return-object v1
.end method
