.class public Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;


# static fields
.field private static e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->a:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 38
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->c:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;

    .line 39
    sput-object p3, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    .line 40
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;

    .line 41
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZZ)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;
    .locals 12

    .prologue
    .line 83
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 84
    const/4 v6, 0x0

    .line 85
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v6, 0x1

    .line 88
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->c:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;

    new-instance v5, Lcom/sec/android/app/samsungapps/downloadhelper/a;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/downloadhelper/a;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    sget-object v8, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    iget-object v9, p0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    new-instance v10, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;

    invoke-direct {v10}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;-><init>()V

    move-object v1, p1

    move-object v2, p2

    move v7, p3

    move/from16 v11, p4

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;ZZLcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;Z)V

    .line 93
    new-instance v1, Lcom/sec/android/app/samsungapps/downloadhelper/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/samsungapps/downloadhelper/c;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V

    .line 196
    return-object v0
.end method


# virtual methods
.method public createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZZ)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    return-object v0
.end method

.method public createDownloaderWithoutKnox(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZZ)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    return-object v0
.end method
