.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CPreloadAutoUpdateCheckConfig;
.super Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;-><init>(Landroid/content/Context;)V

    .line 9
    return-void
.end method


# virtual methods
.method protected getClassNameForAlarmRegister()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string v0, "com.sec.android.app.samsungapps.PreloadUpdateAlarmReceiver"

    return-object v0
.end method

.method protected getDefaultIntervalSec()J
    .locals 2

    .prologue
    .line 33
    const-wide/32 v0, 0x15180

    return-wide v0
.end method

.method protected getIntervalSharedPrefName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string v0, "PRELOAD_UPDATE_INTERVAL"

    return-object v0
.end method

.method protected getLastUpdateCheckStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    const-string v0, "LASTPRELOADUPDATECHECK"

    return-object v0
.end method

.method protected getUpdateCycleServerResponseFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "emergencyUpdateCycle"

    return-object v0
.end method
