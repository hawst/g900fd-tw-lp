.class public Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V

    .line 17
    return-void
.end method


# virtual methods
.method public getDisclaimerViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bb;-><init>(Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;)V

    return-object v0
.end method
