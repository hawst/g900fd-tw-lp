.class final Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field a:I

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)V
    .locals 1

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCurExecuteCount:I
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->a:I

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCurExecuteCount:I
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->a:I

    if-eq v0, v1, :cond_0

    .line 147
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->isDiscountedContent()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->clear()V

    .line 146
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;Z)V

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->checkEnable()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)V

    goto :goto_1
.end method
