.class final Lcom/sec/android/app/samsungapps/ap;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 1699
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClickAddReview()V
    .locals 0

    .prologue
    .line 1796
    return-void
.end method

.method public final onClickCancelBtn()V
    .locals 2

    .prologue
    .line 1720
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 1721
    return-void
.end method

.method public final onClickGetBtn()V
    .locals 1

    .prologue
    .line 1708
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->isOpenGearManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1710
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    .line 1716
    :goto_0
    return-void

    .line 1714
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->s(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    goto :goto_0
.end method

.method public final onClickLikeBtn()V
    .locals 1

    .prologue
    .line 1789
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    .line 1790
    return-void
.end method

.method public final onClickShareBtn()V
    .locals 3

    .prologue
    .line 1763
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1764
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Z)Z

    .line 1765
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    new-instance v2, Lcom/sec/android/app/samsungapps/aq;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/aq;-><init>(Lcom/sec/android/app/samsungapps/ap;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 1784
    :cond_0
    return-void
.end method

.method public final onClickTabBtn(I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1725
    .line 1727
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->t(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/bu;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1728
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->t(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/bu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 1729
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->t(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/bu;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->u(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/bu;->b(I)V

    .line 1730
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->t(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/bu;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/samsungapps/bu;->a(I)V

    .line 1731
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->t(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/bu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/bu;->b()I

    move-result v3

    move v4, v3

    move v3, v0

    .line 1734
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 1736
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    move v5, v2

    .line 1739
    :goto_2
    const/4 v0, 0x3

    if-ge v5, v0, :cond_1

    .line 1740
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v6, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1739
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_0
    move v0, v2

    .line 1728
    goto :goto_0

    .line 1743
    :cond_1
    const/4 v0, 0x2

    if-le p1, v0, :cond_3

    .line 1744
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v6, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1748
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)V

    .line 1749
    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1751
    if-nez v3, :cond_2

    .line 1752
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getNeedLoad()Z

    move-result v0

    if-ne v0, v1, :cond_4

    .line 1753
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0, v4, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;IZ)V

    .line 1758
    :cond_2
    :goto_4
    return-void

    .line 1746
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v6, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_3

    .line 1755
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0, v4, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;IZ)V

    goto :goto_4

    :cond_5
    move v3, v2

    move v4, v2

    goto :goto_1
.end method

.method public final onClickWishListBtn()V
    .locals 1

    .prologue
    .line 1703
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ap;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->q(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    .line 1704
    return-void
.end method
