.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginResponseParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginResponseParser;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginResponseParser;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 11
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;-><init>()V

    .line 16
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 17
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginResponseParser;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V

    .line 18
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 28
    return-void
.end method
