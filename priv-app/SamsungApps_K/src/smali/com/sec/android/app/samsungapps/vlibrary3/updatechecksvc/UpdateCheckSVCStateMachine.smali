.class public Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;

    .line 40
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 106
    const-string v0, "UpdateCheckSVCStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 131
    :goto_0
    :pswitch_0
    return-void

    .line 112
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->CLEAR_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 113
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->INIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 119
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->REQ_GET_DOWNLOADLIST:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->CALC_UPDATE_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 46
    const-string v0, "UpdateCheckSVCStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 101
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 50
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 53
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->INIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 61
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 74
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->CALL_GETDLLIST:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 85
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->UPDATE_COUNT_CALC:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 96
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_6
        :pswitch_9
        :pswitch_c
    .end packed-switch

    .line 50
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 58
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 71
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 82
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 93
    :pswitch_data_5
    .packed-switch 0x8
        :pswitch_d
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method
