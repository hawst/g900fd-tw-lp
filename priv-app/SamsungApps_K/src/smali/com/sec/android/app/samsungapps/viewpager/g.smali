.class final Lcom/sec/android/app/samsungapps/viewpager/g;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/g;->a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onContentClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 2

    .prologue
    .line 231
    if-nez p1, :cond_0

    .line 233
    const-string v0, "ChartViewFragment::onContentClick::Content is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/g;->a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V

    goto :goto_0
.end method

.method public final onMoreClick()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x17

    const/4 v2, 0x0

    .line 208
    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/g;->a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->getWidgetData()Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/g;->a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->getWidgetData()Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    .line 213
    :goto_0
    if-eqz v0, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/g;->a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->CONTENT_GAME_CATEGORY_ID:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    .line 222
    :goto_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_GAMES:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    .line 223
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/g;->a:Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->CONTENT_GAME_CATEGORY_ID:Ljava/lang/String;

    invoke-static {v0, v2, v1, v3, v4}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
