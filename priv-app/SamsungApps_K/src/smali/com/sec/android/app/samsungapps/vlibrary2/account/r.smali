.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/r;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->access$002(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLogedOut()V

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->notifyLogoutSuccess(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;Z)V

    .line 45
    return-void
.end method
