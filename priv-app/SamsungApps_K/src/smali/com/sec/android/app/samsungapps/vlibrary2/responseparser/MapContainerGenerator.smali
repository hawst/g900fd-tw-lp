.class public Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;


# instance fields
.field private _IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->clearContainer()V

    .line 18
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceiveParsingResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V
    .locals 6

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getHeaderMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 24
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getBodyListMap()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 26
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->openMap()V

    .line 27
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 28
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 30
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->closeMap()V

    goto :goto_0

    .line 34
    :cond_1
    return-void
.end method
