.class public Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->a:Ljava/util/HashMap;

    .line 16
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;)Z
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 32
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 33
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    .line 35
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionType()Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 37
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getPermissionMap()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method public hasCreatedPermission()Z
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->CREATE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->a(Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;)Z

    move-result v0

    return v0
.end method

.method public hasNewPermission()Z
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->NEW:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->a(Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;)Z

    move-result v0

    return v0
.end method
