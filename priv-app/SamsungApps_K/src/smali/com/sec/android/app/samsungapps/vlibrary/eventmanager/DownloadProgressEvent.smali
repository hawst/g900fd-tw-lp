.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mDownload:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

.field mDownloadProgressEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent$DownloadProgressEventType;

.field mProgress:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent$DownloadProgressEventType;Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;->mDownload:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    .line 16
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;->mProgress:I

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;->mDownloadProgressEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent$DownloadProgressEventType;

    .line 18
    return-void
.end method


# virtual methods
.method public getDownload()Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;->mDownload:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    return-object v0
.end method

.method public getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent$DownloadProgressEventType;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;->mDownloadProgressEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent$DownloadProgressEventType;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/DownloadProgressEvent;->mProgress:I

    return v0
.end method
