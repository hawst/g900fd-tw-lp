.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProviderCreator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;


# instance fields
.field private _Context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProviderCreator;->_Context:Landroid/content/Context;

    .line 11
    return-void
.end method


# virtual methods
.method public createAutoUpdateNotification()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;
    .locals 3

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProviderCreator;->_Context:Landroid/content/Context;

    const-string v2, "notify_app_updates_setting"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method
