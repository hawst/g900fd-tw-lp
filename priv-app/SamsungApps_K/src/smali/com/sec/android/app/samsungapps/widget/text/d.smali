.class final Lcom/sec/android/app/samsungapps/widget/text/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/d;->a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTextSingleLineChanged(Landroid/widget/TextView;Z)V
    .locals 9

    .prologue
    const v8, 0x7f0c00e7

    const/16 v7, 0x8

    const v6, 0x7f0c00e6

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/d;->a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 607
    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 608
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/d;->a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    const v5, 0x7f0c00e3

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 610
    if-nez v4, :cond_0

    .line 630
    :goto_0
    return-void

    .line 615
    :cond_0
    if-ne p2, v2, :cond_1

    .line 617
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/d;->a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 619
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 629
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/d;->a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    if-nez p2, :cond_2

    move v0, v2

    :goto_2
    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)V

    goto :goto_0

    .line 623
    :cond_1
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/d;->a:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 625
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 626
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    goto :goto_1

    :cond_2
    move v0, v3

    .line 629
    goto :goto_2
.end method
