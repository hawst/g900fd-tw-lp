.class final Lcom/sec/android/app/samsungapps/view/ac;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/ab;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/ab;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    .line 196
    const-string v0, "kkangna"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "result 3 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-boolean v2, v2, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->requestAdultCheck()V

    .line 204
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    .line 202
    const-string v0, "kkangna"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "result 4 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ac;->a:Lcom/sec/android/app/samsungapps/view/ab;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/view/ab;->a:Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    iget-boolean v2, v2, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
