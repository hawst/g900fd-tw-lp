.class public Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:I

.field b:Ljava/util/ArrayList;

.field c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->a:I

    .line 21
    iput p2, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->a:I

    .line 22
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->b:Ljava/util/ArrayList;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->c:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 28
    if-eqz p2, :cond_0

    .line 30
    check-cast p2, Landroid/widget/LinearLayout;

    .line 37
    :goto_0
    const v0, 0x7f0c0258

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    const v0, 0x7f0c0259

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionType()Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->NEW:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    if-ne v1, v2, :cond_1

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->c:Landroid/content/Context;

    const v3, 0x7f08029c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :goto_1
    return-object p2

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    iget v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto :goto_0

    .line 48
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public release()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;->c:Landroid/content/Context;

    .line 55
    return-void
.end method
