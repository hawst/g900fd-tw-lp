.class final Lcom/sec/android/app/samsungapps/widget/list/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 128
    sub-int v0, p4, p3

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->saveOldY(I)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->b(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    if-lt p2, v0, :cond_0

    if-eqz p4, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->hasMoreItems()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;->requestListMore()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/a;->a:Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setIndexOfFirstItem(I)V

    .line 157
    :cond_0
    return-void
.end method
