.class public Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static b:Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->b:Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 19
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->b:Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    return-object v0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public flextibleTabViewUpdated()V
    .locals 2

    .prologue
    .line 162
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->FlextibleTabUpdated:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 163
    return-void
.end method

.method public hideUpdateAllButton()V
    .locals 2

    .prologue
    .line 154
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->HideUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 155
    return-void
.end method

.method public notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;

    .line 42
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;->handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z
    .locals 1

    .prologue
    .line 33
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public showBannerContentList(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 68
    return-void
.end method

.method public showBannerContentList(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 72
    return-void
.end method

.method public showBannerContentList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 77
    return-void
.end method

.method public showChangeCreditCardView()V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowChangeCreditCardView:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 133
    return-void
.end method

.method public showContentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetail:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 64
    return-void
.end method

.method public showContentDetailActivity(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 2

    .prologue
    .line 158
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetailActivity:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 159
    return-void
.end method

.method public showCreditCardRegister()V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowCreditCardRegister:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 125
    return-void
.end method

.method public showCustomerSupport()V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->CustomerSupport:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 97
    return-void
.end method

.method public showFeaturedTab()V
    .locals 2

    .prologue
    .line 141
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;->ShowFeaturedTab:Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 142
    return-void
.end method

.method public showFeaturedUpdatableTab()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;->ShowFeaturedUpdatableTab:Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 60
    return-void
.end method

.method public showFlexibleTab()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;->SHOWFLEXIBLETAB:Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 56
    return-void
.end method

.method public showGiftCards()V
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowGiftcards:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 113
    return-void
.end method

.method public showHotKeywordSearchList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayHotKeywordSearchList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 85
    return-void
.end method

.method public showHotTab()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;->SHOWHOTTAB:Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 52
    return-void
.end method

.method public showInformation()V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->Information:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 101
    return-void
.end method

.method public showMyTabDownload()V
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowMyTabDownload:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 105
    return-void
.end method

.method public showNewTab()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;->SHOWNEWTAB:Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/FeaturedTabEvent$FeatureTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 48
    return-void
.end method

.method public showNotices()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->Notices:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 93
    return-void
.end method

.method public showPushPreference()V
    .locals 2

    .prologue
    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowPushPreference:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 137
    return-void
.end method

.method public showRemoveSnsAccountMenu()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowRemoveSnsAccountMenu:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 121
    return-void
.end method

.method public showSeriesContentList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplaySeries:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 81
    return-void
.end method

.method public showUnifiedBillingCreditCard()V
    .locals 2

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowUnifiedBillingCreditCard:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 129
    return-void
.end method

.method public showUpdateAllButton()V
    .locals 2

    .prologue
    .line 150
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowUpdateAllButton:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 151
    return-void
.end method

.method public showVoucherList()V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowVoucherList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 109
    return-void
.end method

.method public showVouchers()V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowVouchers:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 117
    return-void
.end method

.method public showWishList()V
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowWishList:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 89
    return-void
.end method

.method public updateAllInMyDownloads()V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->UpdateAll:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->notifyUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    .line 147
    return-void
.end method
