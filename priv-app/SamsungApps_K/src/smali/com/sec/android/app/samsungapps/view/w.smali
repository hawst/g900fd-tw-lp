.class final Lcom/sec/android/app/samsungapps/view/w;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:Landroid/view/LayoutInflater;

.field b:I

.field c:Landroid/content/Context;

.field final synthetic d:Lcom/sec/android/app/samsungapps/view/GiftCardListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/view/GiftCardListView;Landroid/content/Context;)V
    .locals 2

    .prologue
    const v1, 0x7f040087

    .line 283
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/w;->d:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    .line 284
    invoke-direct {p0, p2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/w;->a:Landroid/view/LayoutInflater;

    .line 273
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/w;->b:I

    .line 286
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/w;->a:Landroid/view/LayoutInflater;

    .line 287
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/w;->b:I

    .line 288
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/w;->c:Landroid/content/Context;

    .line 289
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 293
    .line 294
    if-nez p2, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/w;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/w;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 298
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    .line 300
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v3

    if-eqz p2, :cond_3

    if-eqz v3, :cond_3

    const v0, 0x7f0c01f6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c01d5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v2, 0x7f0c0059

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-nez v2, :cond_4

    :cond_1
    const-string v0, "VoucherItemArray::showGiftCardState::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_0
    const v0, 0x7f0c01f4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_7

    const-string v0, "VoucherItemArray::showGiftCardPrice::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_1
    const-string v2, "Expires"

    const v0, 0x7f0c01f7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c01f8

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    if-nez v1, :cond_8

    :cond_2
    const-string v0, "VoucherItemArray::showReducePrice::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 302
    :cond_3
    :goto_2
    return-object p2

    .line 300
    :cond_4
    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardImagePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/w;->d:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c(Lcom/sec/android/app/samsungapps/view/GiftCardListView;)Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->giftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v1

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/w;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f080180

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0200f8

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/w;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0801f1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0200f1

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/w;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v4

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v5, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/w;->c:Landroid/content/Context;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->userExpireDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
