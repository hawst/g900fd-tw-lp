.class final Lcom/sec/android/app/samsungapps/lr;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/lq;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/lq;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lr;->a:Lcom/sec/android/app/samsungapps/lq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onUpdateCheckFailed(I)V
    .locals 3

    .prologue
    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lr;->a:Lcom/sec/android/app/samsungapps/lq;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/lq;->a:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;->onUpdateCheckFailed(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateCheckSVC::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onUpdateCheckSuccess(I)V
    .locals 3

    .prologue
    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lr;->a:Lcom/sec/android/app/samsungapps/lq;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/lq;->a:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;->onUpdateCheckSuccess(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateCheckSVC::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
