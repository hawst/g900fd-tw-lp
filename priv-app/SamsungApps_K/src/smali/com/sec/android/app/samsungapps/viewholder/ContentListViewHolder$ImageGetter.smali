.class public Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder$ImageGetter;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder$ImageGetter;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 634
    .line 635
    const-string v0, "google_tencent_icon"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder$ImageGetter;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    const v0, 0x7f02012d

    .line 646
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder$ImageGetter;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 647
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 648
    return-object v0

    .line 643
    :cond_0
    const v0, 0x7f020124

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
