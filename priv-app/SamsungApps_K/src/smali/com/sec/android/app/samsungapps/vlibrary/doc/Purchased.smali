.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private _bSelected:Z

.field public bundle:Z

.field public contentsSize:J

.field public loadType:Ljava/lang/String;

.field public ordItemSeq:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public platformVersion:Ljava/lang/String;

.field public purchaseDate:Ljava/lang/String;

.field public purchasedPrice:D

.field public updateClsf:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ad;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ad;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Landroid/os/Parcel;)V

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->_bSelected:Z

    .line 44
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 187
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->_bSelected:Z

    .line 31
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->updateClsf:Z

    .line 33
    return-void
.end method


# virtual methods
.method public getConvertedFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 123
    const-string v0, ".apk"

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isWGTOnly()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    const-string v0, ".wgt"

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getConvertedFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->versionCode:Ljava/lang/String;

    .line 133
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 135
    :cond_2
    const-string v1, "0"

    .line 137
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "samsungapps-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->loadType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 209
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productName:Ljava/lang/String;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productName:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getOrderID()Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->orderID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getOrderItemSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->ordItemSeq:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->purchaseDate:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 218
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->purchaseDate:Ljava/lang/String;

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->purchaseDate:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;
    .locals 0

    .prologue
    .line 75
    return-object p0
.end method

.method public getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRealContentSize()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    .line 69
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->realContentSize:I

    int-to-long v1, v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    goto :goto_0
.end method

.method public getUpdateClsf()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->updateClsf:Z

    return v0
.end method

.method public hasOrderID()Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasOrderID()Z

    move-result v0

    .line 83
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->orderID:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->orderID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOrderItemSeq()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->ordItemSeq:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->ordItemSeq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x1

    return v0
.end method

.method public isPrePostApp()Z
    .locals 2

    .prologue
    .line 145
    const-string v0, "2"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->loadType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isPrePostApp::loadType "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->loadType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->_bSelected:Z

    return v0
.end method

.method protected final isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 157
    if-nez p1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v0

    goto :goto_0
.end method

.method public setContentSize(J)V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->setContentSize(J)V

    .line 117
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->contentsSize:J

    .line 118
    return-void
.end method

.method public setOrderID(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    .line 111
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->orderID:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setSelect(Z)V
    .locals 0

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->_bSelected:Z

    .line 197
    return-void
.end method

.method public setUpdated()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->updateClsf:Z

    .line 56
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->writeToParcel(Landroid/os/Parcel;I)V

    .line 50
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 51
    return-void
.end method
