.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private e:Landroid/os/Handler;

.field private f:Z

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->e:Landroid/os/Handler;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->b:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 33
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->e:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 51
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    .line 57
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Action;)V
    .locals 4

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 74
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->ALREADY_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    goto :goto_0

    .line 80
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->NOT_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    goto :goto_0

    .line 84
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;->onLoginCheckSuccess()V

    goto :goto_0

    .line 87
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->b:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 103
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->f:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 106
    :pswitch_4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/c;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;)V

    .line 122
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->forceLoad()V

    goto :goto_0

    .line 125
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;->onLoginCheckFailed()V

    goto :goto_0

    .line 128
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->f:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->PAID_TYPE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    goto :goto_0

    .line 134
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->PAID_TYPE_NOT_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    goto :goto_0

    .line 138
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->b:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;

    .line 40
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

    .line 62
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$State;)V

    return-void
.end method

.method public userAgree(Z)V
    .locals 1

    .prologue
    .line 152
    if-eqz p1, :cond_0

    .line 154
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;)V

    goto :goto_0
.end method
