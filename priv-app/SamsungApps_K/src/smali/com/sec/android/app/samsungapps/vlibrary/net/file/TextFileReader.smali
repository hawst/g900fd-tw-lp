.class public Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;->mFile:Ljava/io/File;

    .line 15
    return-void
.end method


# virtual methods
.method public readFile()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 19
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;->mFile:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v4, "UTF8"

    invoke-direct {v0, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 25
    const v0, 0xa000

    new-array v3, v0, [C

    .line 27
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 28
    :cond_0
    :goto_0
    const/4 v5, -0x1

    if-eq v0, v5, :cond_1

    .line 30
    invoke-virtual {v2, v3}, Ljava/io/BufferedReader;->read([C)I

    move-result v0

    .line 31
    if-lez v0, :cond_0

    .line 33
    invoke-virtual {v4, v3, v1, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
