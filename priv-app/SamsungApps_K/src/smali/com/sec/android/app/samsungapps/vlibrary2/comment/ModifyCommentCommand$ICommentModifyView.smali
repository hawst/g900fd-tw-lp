.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getCommentText()Ljava/lang/String;
.end method

.method public abstract getRatingValue()I
.end method

.method public abstract onCommentModifyResult(Z)V
.end method

.method public abstract onCommentModifying()V
.end method

.method public abstract onRequestRatingAuthority()V
.end method

.method public abstract onRequestRatingAuthorityResult(Z)V
.end method
