.class public Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;


# instance fields
.field private bKnoxMode:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;->bKnoxMode:Z

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;->mContext:Landroid/content/Context;

    .line 19
    return-void
.end method


# virtual methods
.method public createRequest(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;->bKnoxMode:Z

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;-><init>()V

    .line 30
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-direct {v1, p1, v0, p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;)V

    .line 31
    invoke-virtual {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 32
    invoke-virtual {v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;->bKnoxMode:Z

    if-eqz v0, :cond_0

    .line 37
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v0

    .line 38
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_KNOX"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setFakeModel(Ljava/lang/String;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->isNoShipTestMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "__TST"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setFakeModel(Ljava/lang/String;)V

    .line 45
    :cond_1
    return-object v1
.end method
