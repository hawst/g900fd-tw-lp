.class public Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field public static instance:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 159
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 195
    :goto_0
    return-void

    .line 163
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ASKQP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 166
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 167
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 170
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 173
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 177
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 180
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_SIGNAL_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 183
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_GEOIP_SEARCH_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 186
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ASKCLISTP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 189
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 192
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SHOW_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 16
    const-string v2, "CountrySearchStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "execute:"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " :: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 17
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 154
    :goto_0
    return v0

    .line 20
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    move v0, v1

    .line 40
    goto :goto_0

    .line 23
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    .line 154
    :goto_1
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 26
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 31
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 34
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 37
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 44
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_2

    move v0, v1

    .line 54
    goto :goto_0

    .line 47
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 51
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 58
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_3

    move v0, v1

    .line 70
    goto :goto_0

    .line 61
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 64
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->AVAILABLE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 67
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SHOWING_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 74
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_4

    move v0, v1

    .line 83
    goto :goto_0

    .line 77
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 80
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 87
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_5

    move v0, v1

    .line 96
    goto/16 :goto_0

    .line 90
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_1

    .line 93
    :pswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 100
    :pswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_6

    move v0, v1

    .line 109
    goto/16 :goto_0

    .line 103
    :pswitch_15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 106
    :pswitch_16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 113
    :pswitch_17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_7

    move v0, v1

    .line 121
    goto/16 :goto_0

    .line 116
    :pswitch_18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 119
    :pswitch_19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 125
    :pswitch_1a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_8

    move v0, v1

    .line 135
    goto/16 :goto_0

    .line 128
    :pswitch_1b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->AVAILABLE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 132
    :pswitch_1c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 139
    :pswitch_1d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/j;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ordinal()I

    move-result v2

    aget v0, v0, v2

    sparse-switch v0, :sswitch_data_0

    move v0, v1

    .line 148
    goto/16 :goto_0

    .line 142
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 145
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 17
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
        :pswitch_a
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
    .end packed-switch

    .line 20
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 44
    :pswitch_data_2
    .packed-switch 0x7
        :pswitch_8
        :pswitch_9
        :pswitch_9
    .end packed-switch

    .line 58
    :pswitch_data_3
    .packed-switch 0xa
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 74
    :pswitch_data_4
    .packed-switch 0xd
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 87
    :pswitch_data_5
    .packed-switch 0xf
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 100
    :pswitch_data_6
    .packed-switch 0x11
        :pswitch_15
        :pswitch_16
    .end packed-switch

    .line 113
    :pswitch_data_7
    .packed-switch 0x13
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 125
    :pswitch_data_8
    .packed-switch 0x7
        :pswitch_1b
        :pswitch_1c
        :pswitch_1c
    .end packed-switch

    .line 139
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 11
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 201
    return-void
.end method
