.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IReqCountrySearchCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;->_IReqCountrySearchCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 24
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 25
    if-nez v0, :cond_0

    .line 27
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;->onFinalResult(Z)V

    .line 39
    :goto_0
    return-void

    .line 31
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->requestContryInfo(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand;->_IReqCountrySearchCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ReqCountrySearchCommand$IReqCountrySearchCommandData;->setReqCountrySearchExecuted(Z)V

    goto :goto_0
.end method
