.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISelfUpdateManagerFactory;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->a:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->b:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    .line 20
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 21
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    .line 22
    return-void
.end method


# virtual methods
.method public createSelfUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;
    .locals 5

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->b:Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;)V

    return-object v0
.end method
