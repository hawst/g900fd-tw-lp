.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;Z)V

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->getModifyCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->_Context:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method
