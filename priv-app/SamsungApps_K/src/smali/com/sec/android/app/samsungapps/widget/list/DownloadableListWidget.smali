.class public Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;
.super Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;


# instance fields
.field b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

.field private f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

.field private g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

.field private h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

.field private i:I

.field private j:I

.field private k:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

.field public refreshList:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;)V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    .line 48
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->i:I

    .line 49
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->j:I

    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    .line 48
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->i:I

    .line 49
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->j:I

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    .line 48
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->i:I

    .line 49
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->j:I

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;)Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    const v1, 0x7f040048

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;-><init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;)V

    .line 110
    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;)Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    return-object v0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 379
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 381
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 383
    :cond_0
    return-object v1
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    .line 73
    const v0, 0x7f04004a

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->initView(Landroid/content/Context;I)V

    .line 74
    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->mListView:Landroid/widget/AbsListView;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFocusableInTouchMode(Z)V

    .line 76
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 77
    instance-of v0, p1, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    if-eqz v0, :cond_0

    .line 78
    check-cast p1, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->k:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    .line 81
    :cond_0
    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 82
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/h;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/h;-><init>(Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 388
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 392
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 394
    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 398
    :cond_1
    return-object v1
.end method

.method private c()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v1

    .line 403
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 405
    if-eqz v0, :cond_0

    .line 407
    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 409
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 411
    :cond_1
    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 413
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 417
    :cond_2
    return-object v2
.end method


# virtual methods
.method public cancellAll()V
    .locals 3

    .prologue
    .line 681
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 682
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 684
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    goto :goto_0

    .line 686
    :cond_0
    return-void
.end method

.method public bridge synthetic getAdapter()Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    return-object v0
.end method

.method public getCountNotInstalledItem()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 473
    :cond_1
    return v2

    .line 467
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, v0

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v1, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 470
    add-int/lit8 v2, v2, 0x1

    move v0, v2

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v1, v3

    .line 468
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public getDownloadableItemCount()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 711
    .line 712
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 713
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_2

    .line 715
    :cond_0
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_2
    move v1, v2

    .line 724
    :cond_1
    return v1

    .line 723
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public getInstallingCount()I
    .locals 4

    .prologue
    .line 693
    const/4 v0, 0x0

    .line 694
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v1

    .line 695
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 697
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 699
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    .line 702
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 689
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public hideUpdateAll()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    if-nez v0, :cond_1

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getUpdatableListCount()I

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onUpdate()V

    goto :goto_0
.end method

.method public installAll()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 443
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 444
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 446
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    move v1, v3

    :goto_1
    if-eqz v1, :cond_0

    .line 448
    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createNoAccountBasedDownloadHelperFactoryForDemo(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v1, v5, v0, v3}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    .line 449
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->execute()V

    goto :goto_0

    .line 446
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v5

    if-nez v5, :cond_3

    move v1, v2

    goto :goto_1

    :cond_3
    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v1, v2

    goto :goto_1

    :cond_4
    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_1

    .line 452
    :cond_6
    return-void
.end method

.method public isAllPage()Z
    .locals 2

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public itemClick(Landroid/view/View;I)V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 613
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 614
    if-nez v0, :cond_1

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListLinkUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 620
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListLinkUrl()Ljava/lang/String;

    move-result-object v0

    .line 621
    const-string v2, "deeplink://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_3

    .line 623
    :try_start_1
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    aget-object v2, v0, v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "action : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    array-length v2, v0

    if-ne v2, v5, :cond_2

    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x3

    aget-object v0, v0, v2

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v1, v2, v0

    const-string v5, "="

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    aget-object v5, v1, v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "param : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v1, v1, v6

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/high16 v0, 0x14000000

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadableListWidget::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 640
    :catch_1
    move-exception v0

    const-string v0, "DownloadableListWidget :: itemClick ArrayIndexOutOfBoundsException "

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 625
    :cond_3
    :try_start_3
    const-string v1, "samsungapps://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 627
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->createDeepLink(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->setIsInternal(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isBannerList()Z

    move-result v1

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBannerListID()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x4

    invoke-static {v1, v2, v3, v0, v4}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isCategoryList()Z

    move-result v1

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryID()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x17

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isDetailPage()Z

    move-result v1

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getDetailID()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x1a

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isMainView()Z

    move-result v0

    if-ne v0, v3, :cond_0

    const-string v0, "DownloadableList::runDeepLink::Not Defined"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 629
    :cond_7
    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 631
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "goHttpDeeplink:: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 636
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v2, 0x7

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0
.end method

.method public itemLongClick(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 648
    return-void
.end method

.method public loadMoreComplete()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onDataLoadCompleted()V

    .line 259
    :cond_0
    return-void
.end method

.method public loadWidget()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->requestMore()V

    .line 154
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->loadWidget()V

    .line 155
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->load()V

    goto :goto_0
.end method

.method public loadingMore()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onDataLoadingMore()V

    .line 267
    :cond_0
    return-void
.end method

.method public refreshArrayAdapter()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->notifyDataSetChanged()V

    .line 275
    return-void
.end method

.method public refreshBeforeSelectedItem()V
    .locals 0

    .prologue
    .line 678
    return-void
.end method

.method public refreshWidget()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-nez v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DownloadableListWidget :: refreshWidget "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 213
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    if-eqz v0, :cond_4

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->clear()V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setListEOF(Z)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getUpdatableListCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 222
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 228
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->checkUpdateCount()V

    .line 239
    :goto_2
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->updatesListViewEOF()V

    .line 226
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    goto :goto_1

    .line 232
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->updateWidget()V

    goto :goto_2

    .line 237
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->notifyDataSetChanged()V

    goto :goto_2
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->clear()V

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->release()V

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    if-eqz v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->release()V

    .line 136
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    .line 142
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->release()V

    .line 143
    return-void
.end method

.method public requestList()V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->load()V

    .line 484
    return-void
.end method

.method public requestListMore()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->requestMore()V

    .line 479
    return-void
.end method

.method public saveOldY(I)V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->saveOldY(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 248
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->j:I

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_0
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->i:I

    goto :goto_0
.end method

.method public setClickListener(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 98
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V
    .locals 6

    .prologue
    const v5, 0x7f0c007f

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 280
    sget-object v0, Lcom/sec/android/app/samsungapps/widget/list/i;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 282
    :pswitch_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 283
    :cond_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 287
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->k:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->k:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->isBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 291
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_1
    if-nez v0, :cond_4

    .line 294
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 300
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getScrollState()I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->loadMoreComplete()V

    .line 303
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetMoreLoading(Z)V

    .line 305
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->updateWidget()V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 291
    goto :goto_1

    .line 310
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->loadingMore()V

    .line 311
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetMoreLoading(Z)V

    goto :goto_0

    .line 315
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getScrollState()I

    move-result v0

    if-ne v0, v3, :cond_7

    .line 317
    const v0, 0x7f0c007b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 318
    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onDataLoadCompleted()V

    goto :goto_0

    .line 323
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 280
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 102
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;)Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setListRequest(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/CommonAdapter;)V

    .line 106
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 114
    check-cast p2, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 116
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;)Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setListRequest(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/CommonAdapter;)V

    .line 120
    return-void
.end method

.method public showUpdateAll()V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    if-nez v0, :cond_1

    .line 502
    :cond_0
    :goto_0
    return-void

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getUpdatableListCount()I

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onUpdate()V

    goto :goto_0
.end method

.method public updateList(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    if-nez v0, :cond_1

    .line 674
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshList:Z

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 660
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->clear()V

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setListEOF(Z)V

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getUpdatableListCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 666
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080208

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 667
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    goto :goto_0

    .line 669
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->updatesListViewEOF()V

    .line 670
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    goto :goto_0
.end method

.method public updateWidget()V
    .locals 5

    .prologue
    const v4, 0x7f080214

    const/4 v3, 0x2

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-nez v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->clear()V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->setListEOF(Z)V

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DownloadableListWidget :: size() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DownloadableListWidget :: isEOF() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->isEOF()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 172
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setEmptyTextSize(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->g:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->updatesListViewEOF()V

    .line 176
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 177
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 184
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->checkUpdateCount()V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->mListView:Landroid/widget/AbsListView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 197
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onDataLoadCompleted()V

    goto/16 :goto_0

    .line 181
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setEmptyText(Ljava/lang/String;)V

    goto :goto_1

    .line 187
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onUpdate()V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->f:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->updateViewEOF()V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    if-gtz v0, :cond_5

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->h:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onUpdate()V

    .line 191
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->onWidgetViewState(I)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setEmptyText(Ljava/lang/String;)V

    .line 194
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->mListView:Landroid/widget/AbsListView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_2
.end method
