.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;


# instance fields
.field mProtocolMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field mResponseHeaderMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mProtocolMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mResponseHeaderMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 32
    return-void
.end method

.method private findname(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 289
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    .line 290
    invoke-interface {v0}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v1

    .line 291
    if-lez v1, :cond_0

    .line 293
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Attr;

    .line 294
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 296
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private findvalue(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 273
    const-string v1, ""

    .line 274
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 275
    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 276
    invoke-interface {v2, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 277
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 278
    if-eqz v3, :cond_0

    .line 279
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 283
    :cond_1
    const-string v1, "\r\n"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    return-object v0
.end method

.method private mapFromNode(Lorg/w3c/dom/Node;)Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 7

    .prologue
    .line 239
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 240
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 241
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    .line 242
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 243
    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 244
    const-string v5, "value"

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 246
    invoke-interface {v4}, Lorg/w3c/dom/Node;->hasAttributes()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 248
    invoke-interface {v4}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 253
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->findvalue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 254
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->findname(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    .line 256
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 257
    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 242
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_1
    return-object v1
.end method

.method private onParseListObject(Ljava/util/ArrayList;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V
    .locals 0

    .prologue
    .line 234
    invoke-virtual {p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->addBodyListListMap(Ljava/util/ArrayList;)V

    .line 235
    return-void
.end method

.method private onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V
    .locals 0

    .prologue
    .line 164
    invoke-virtual {p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->setHeaderMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 165
    return-void
.end method

.method private parseErrorNode(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V
    .locals 5

    .prologue
    .line 36
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 37
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 38
    invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 39
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    .line 41
    const-string v4, "errorString"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 43
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v3

    const-string v4, "errorCode"

    invoke-interface {v3, v4}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 44
    if-eqz v3, :cond_0

    .line 46
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v3

    const-string v4, "errorCode"

    invoke-interface {v3, v4}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 47
    invoke-virtual {p2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->setServerErrorCode(Ljava/lang/String;)V

    .line 49
    :cond_0
    invoke-interface {v2}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 51
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->setServerErrorMsg(Ljava/lang/String;)V

    .line 37
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_2
    return-void
.end method


# virtual methods
.method protected onParseObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V
    .locals 0

    .prologue
    .line 268
    invoke-virtual {p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->addBodyListMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 269
    return-void
.end method

.method protected parseObject(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)Z
    .locals 8

    .prologue
    .line 192
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 198
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 199
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_2

    .line 200
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 201
    const-string v6, "value"

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 203
    invoke-interface {v5}, Lorg/w3c/dom/Node;->hasAttributes()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 205
    invoke-interface {v5}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 210
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->findvalue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    .line 211
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->findname(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    .line 213
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 214
    invoke-virtual {v1, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 199
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_1
    const-string v6, "sublist"

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 221
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mapFromNode(Lorg/w3c/dom/Node;)Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 224
    :cond_2
    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->onParseObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V

    .line 225
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 227
    invoke-direct {p0, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->onParseListObject(Ljava/util/ArrayList;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V

    .line 229
    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method protected parseProtocolHeader(Lorg/w3c/dom/Element;)V
    .locals 5

    .prologue
    .line 131
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 135
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 136
    invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Attr;

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mProtocolMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 135
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 139
    :cond_0
    return-void
.end method

.method protected parseResponseBody(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V
    .locals 5

    .prologue
    .line 169
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 170
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 171
    invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 173
    invoke-interface {v2}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 176
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    .line 178
    const-string v4, "errorInfo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 180
    invoke-direct {p0, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseErrorNode(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V

    .line 170
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_1
    invoke-virtual {p0, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseObject(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)Z

    goto :goto_1

    .line 188
    :cond_2
    return-void
.end method

.method protected parseResponseHeader(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V
    .locals 5

    .prologue
    .line 145
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 146
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 148
    invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Attr;

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mResponseHeaderMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 146
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mResponseHeaderMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-string v1, "returnCode"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 153
    if-nez v0, :cond_1

    .line 155
    const-string v0, "0"

    invoke-virtual {p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->setServerErrorCode(Ljava/lang/String;)V

    .line 156
    const-string v0, ""

    invoke-virtual {p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->setServerErrorMsg(Ljava/lang/String;)V

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->mResponseHeaderMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V

    .line 160
    return-void
.end method

.method public parseXML(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;-><init>()V

    .line 60
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 62
    :try_start_0
    const-string v3, "<"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 63
    if-lez v3, :cond_0

    .line 65
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 72
    :cond_0
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 73
    new-instance v4, Lorg/xml/sax/InputSource;

    invoke-direct {v4, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 77
    :try_start_1
    const-string v5, "parseXML"

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 79
    invoke-virtual {v2, v4}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 80
    invoke-interface {v2}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 82
    const-string v4, "parseProtocolHeader"

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseProtocolHeader(Lorg/w3c/dom/Element;)V

    .line 86
    const-string v4, "response"

    invoke-interface {v2, v4}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 87
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_5

    move-result v4

    if-nez v4, :cond_1

    .line 126
    :goto_0
    return-object v0

    .line 67
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 90
    :cond_1
    const/4 v4, 0x0

    :try_start_2
    invoke-interface {v2, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 91
    const-string v4, "parseResponseHeader"

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseResponseHeader(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V

    .line 93
    const-string v4, "parseResponseBody"

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseResponseBody(Lorg/w3c/dom/Node;Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;)V

    .line 95
    const-string v2, "end parse"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_5

    .line 125
    invoke-virtual {v3}, Ljava/io/StringReader;->close()V

    move-object v0, v1

    .line 126
    goto :goto_0

    .line 97
    :catch_1
    move-exception v1

    .line 98
    const-string v2, "Exception occured while parsing xml ParserConfigurationException"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 99
    invoke-virtual {v1}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_0

    .line 102
    :catch_2
    move-exception v1

    .line 104
    const-string v2, "Exception occured while parsing xml SAXException"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v1}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_0

    .line 108
    :catch_3
    move-exception v1

    .line 110
    const-string v2, "Exception occured while parsing xml IOException"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 114
    :catch_4
    move-exception v1

    .line 116
    const-string v2, "Exception occured while parsing xml IOException"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 120
    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method
