.class public Lcom/sec/android/app/samsungapps/view/UncListAdapter;
.super Lcom/sec/android/app/samsungapps/view/CommonAdapter;
.source "ProGuard"


# instance fields
.field private e:Landroid/content/Context;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;-><init>(Landroid/content/Context;II)V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->e:Landroid/content/Context;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    .line 24
    return-void
.end method


# virtual methods
.method public cancelAll()V
    .locals 4

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->getCount()I

    move-result v0

    .line 40
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 41
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 40
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_1
    return-void
.end method

.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 9

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->e:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->mListType:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V

    return-object v0
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public updateAll()V
    .locals 6

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->getCount()I

    move-result v2

    .line 28
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 29
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;->e:Landroid/content/Context;

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;

    invoke-direct {v5}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;-><init>()V

    invoke-virtual {v3, v4, v0, v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->updateContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;)V

    .line 28
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 36
    :cond_1
    return-void
.end method
