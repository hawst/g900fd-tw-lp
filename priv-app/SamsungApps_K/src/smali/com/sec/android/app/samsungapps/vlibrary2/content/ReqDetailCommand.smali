.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field protected _IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected createRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/content/m;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 46
    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;->createRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 33
    return-void
.end method
