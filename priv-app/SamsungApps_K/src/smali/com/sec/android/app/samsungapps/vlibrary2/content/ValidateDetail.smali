.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _GetDetail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;->_GetDetail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 16
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 33
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;->_GetDetail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/v;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/v;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ValidateDetail;)V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
