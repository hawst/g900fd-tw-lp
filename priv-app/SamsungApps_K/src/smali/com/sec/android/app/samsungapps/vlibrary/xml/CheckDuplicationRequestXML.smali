.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 10
    const-string v2, "checkDuplication"

    const-string v3, "5100"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    const-string v0, "emailID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "password"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "rePassword"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->getRePassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    return-void
.end method
