.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPaymentCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command$IIranPin2CommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;


# instance fields
.field private _EasyBuy:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;->_EasyBuy:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 22
    return-void
.end method


# virtual methods
.method public changeIranCreditCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;)V

    return-object v0
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public easyBuyCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;->_EasyBuy:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    return-object v0
.end method

.method public abstract getIranCreditCardViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getIranDebitURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->getIranDebitUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPin2Command()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command$IIranPin2CommandData;)V

    return-object v0
.end method

.method public abstract getPin2VieInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public hasCreditCard()Z
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->hasCard()Z

    move-result v0

    return v0
.end method

.method public payment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IIranPayment$IIranPaymentData;)V

    return-object v0
.end method

.method public registerIranCreditCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;)V

    return-object v0
.end method

.method public setCardRegisterResult(Z)V
    .locals 2

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRegistered:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    .line 59
    return-void
.end method

.method public setPin2(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setCVC(Ljava/lang/String;)V

    .line 76
    return-void
.end method
