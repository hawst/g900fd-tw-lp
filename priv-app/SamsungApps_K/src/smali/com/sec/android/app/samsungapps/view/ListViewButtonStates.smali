.class public final enum Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ALL:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum ALL_PAID_FREE_:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum SORTBY:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum UPDATE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field public static final enum UPDATE_ALL:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "ALL_PAID_FREE_NEW"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "ALL_PAID_FREE_"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "ALL_NEW"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "SORTBY"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->SORTBY:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "UPDATE_ALL"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->UPDATE_ALL:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "NONE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "ALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    const-string v1, "UPDATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->UPDATE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 3
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->SORTBY:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->UPDATE_ALL:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->UPDATE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->a:[Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->a:[Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    return-object v0
.end method
