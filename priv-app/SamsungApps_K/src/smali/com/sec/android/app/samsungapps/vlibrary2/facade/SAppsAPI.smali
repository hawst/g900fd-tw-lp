.class public Lcom/sec/android/app/samsungapps/vlibrary2/facade/SAppsAPI;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field _CouponGetter:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListGetter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/facade/SAppsAPI;->_CouponGetter:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListGetter;

    return-void
.end method


# virtual methods
.method public requestCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/facade/SAppsAPI;->_CouponGetter:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListGetter;

    invoke-virtual {p1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getCouponRequestCondition(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;

    move-result-object v1

    invoke-interface {v0, v1, p2, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListGetter;->requestPurchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 15
    return-void
.end method
