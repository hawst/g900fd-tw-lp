.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field private static final serialVersionUID:J = -0x44292dc8ef6e0bc4L


# instance fields
.field bCompleted:Z

.field mCategoryWhoNeedsChild:Ljava/util/ArrayList;

.field private map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field parentID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->mCategoryWhoNeedsChild:Ljava/util/ArrayList;

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->bCompleted:Z

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->parentID:Ljava/lang/String;

    .line 21
    return-void
.end method

.method private findCategoryInThisObject(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 97
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->compareID(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategoryInThisObject(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x0

    .line 60
    :goto_0
    return v0

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->isTerminalNode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getChildList()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->mCategoryWhoNeedsChild:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_1
    invoke-virtual {p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->setSibling(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V

    .line 60
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Z

    move-result v0

    return v0
.end method

.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 147
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->setComplete(Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->mCategoryWhoNeedsChild:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 84
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->setSibling(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V

    goto :goto_0

    .line 86
    :cond_0
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 87
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->clear()V

    .line 135
    return-void
.end method

.method public clearSel()V
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 107
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->clearSel()V

    goto :goto_0

    .line 109
    :cond_0
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 156
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Z

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 35
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    .line 36
    if-eqz v1, :cond_1

    move-object v0, v1

    .line 44
    :goto_0
    return-object v0

    .line 40
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 44
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 124
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCategoryNeedingChildList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->mCategoryWhoNeedsChild:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getParent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 26
    :cond_0
    const/4 v0, 0x0

    .line 29
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    goto :goto_0
.end method

.method public getParentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->parentID:Ljava/lang/String;

    return-object v0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->bCompleted:Z

    return v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 142
    return-void
.end method

.method public selCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)Z
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 114
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->selCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const/4 v0, 0x1

    .line 118
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setComplete(Z)V
    .locals 0

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->bCompleted:Z

    .line 76
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
