.class public Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# static fields
.field protected static final RESULT_CANCELED:I = 0x0

.field protected static final RESULT_FAILED:I = 0x1

.field protected static final RESULT_OK:I = -0x1


# instance fields
.field protected mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

.field protected mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const v3, 0x8727

    const/4 v1, 0x1

    .line 76
    if-nez p1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_4_0366(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.CONFIRM_PASSWORD_POPUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 82
    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v1, "client_secret"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string v1, "theme"

    const-string v2, "light"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 98
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->startLoading()V

    .line 99
    return-void

    .line 90
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 91
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "client_secret"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    const-string v1, "account_mode"

    const-string v2, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method protected endLoading()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 72
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 103
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 105
    const v0, 0x8727

    if-ne p1, v0, :cond_0

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Result Code of Confirm password : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->endLoading()V

    .line 111
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->onConfirmPasswordSuccess()V

    .line 137
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->finish()V

    .line 138
    :goto_1
    return-void

    .line 115
    :cond_1
    if-ne p2, v2, :cond_4

    .line 116
    const/4 v0, 0x0

    .line 117
    if-eqz p3, :cond_2

    .line 118
    const-string v0, "error_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    :cond_2
    if-eqz v0, :cond_3

    const-string v1, "SAC_0105"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 123
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->a(Z)V

    goto :goto_1

    .line 126
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->onConfirmPasswordFailed()V

    goto :goto_0

    .line 131
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->onConfirmPasswordFailed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mConfirmPasswordManager:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    if-nez v0, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->finish()V

    .line 44
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAccountConfirmPasswordActivity::ClassCastException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->finish()V

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->start()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onDestroy()V

    .line 54
    return-void
.end method

.method protected start()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->a(Z)V

    .line 49
    return-void
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountConfirmPasswordActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 63
    :cond_0
    return-void
.end method
