.class final Lcom/sec/android/app/samsungapps/pushclient/e;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/pushclient/PushService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0x3e8

    .line 361
    const-string v0, "[BroadcastReceiver] onReceive"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 368
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BroadcastReceiver] action:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 380
    const-string v0, "appId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 381
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BroadcastReceiver] appId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 386
    if-nez v0, :cond_1

    .line 388
    const-string v0, "[BroadcastReceiver] appId==null ( retry )"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "S1000"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 402
    const-string v1, "com.sec.spp.RegistrationFail"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 404
    const-string v1, "Error"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 405
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[BroadcastReceiver] PUSH_REGISTRATION_FAIL. appid is default:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "S"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto :goto_0

    .line 414
    :cond_2
    const-string v1, "com.sec.spp.DeRegistrationFail"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 416
    const-string v1, "Error"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 417
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[BroadcastReceiver]PUSH_DEREGISTRATION_FAIL. appid is default:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "S"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto :goto_0

    .line 426
    :cond_3
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    const-string v0, "com.sec.spp.Status"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 434
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BroadcastReceiver] registrationState:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 438
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 444
    :pswitch_0
    const-string v0, "RegistrationID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    const-string v1, "[BroadcastReceiver]PUSH_REGISTRATION_SUCCESS"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 448
    if-eqz v0, :cond_4

    .line 450
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    .line 459
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BroadcastReceiver]RegID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getPushNotiSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto/16 :goto_0

    .line 454
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "S"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 477
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto/16 :goto_0

    .line 486
    :pswitch_1
    const-string v0, "[BroadcastReceiver]PUSH_DEREGISTRATION_SUCCESS"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->i(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto/16 :goto_0

    .line 497
    :pswitch_2
    const-string v0, "Error"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 500
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BroadcastReceiver]PUSH_REGISTRATION_FAIL("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 505
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "S"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 513
    :pswitch_3
    const-string v0, "Error"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 516
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BroadcastReceiver] PUSH_DEREGISTRATION_FAIL("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 521
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/e;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "S"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 534
    :cond_6
    const-string v0, "[BroadcastReceiver] action!=com.sec.spp.RegistrationChangedAction"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
