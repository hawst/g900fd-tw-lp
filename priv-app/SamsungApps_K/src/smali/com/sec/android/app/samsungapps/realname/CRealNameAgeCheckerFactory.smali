.class public Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static createRealNameAgeConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    new-instance v1, Lcom/sec/android/app/samsungapps/realname/a;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/realname/a;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V

    return-object v0
.end method


# virtual methods
.method public create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheck;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;->createRealNameAgeConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/realname/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/realname/b;-><init>(Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;)V

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V

    .line 59
    return-object v0
.end method
