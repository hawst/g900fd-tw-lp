.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum GO_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

.field public static final enum GO_NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    const-string v1, "GO_NormalMODE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->GO_NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    const-string v1, "GO_BMODE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->GO_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->GO_NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->GO_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    return-object v0
.end method
