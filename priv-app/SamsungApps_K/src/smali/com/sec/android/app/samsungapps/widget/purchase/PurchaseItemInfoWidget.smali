.class public Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->b:Landroid/content/Context;

    .line 27
    const v0, 0x7f0400a2

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->initView(Landroid/content/Context;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->b:Landroid/content/Context;

    .line 34
    const v0, 0x7f0400a2

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->initView(Landroid/content/Context;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->b:Landroid/content/Context;

    .line 41
    const v0, 0x7f0400a2

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->initView(Landroid/content/Context;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public displayprice()V
    .locals 3

    .prologue
    .line 68
    const v0, 0x7f0c0254

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    const v1, 0x7f0c0255

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->discountFlag()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->getNormalPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->getDiscountPrice()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->isAlreadyPurchasedContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const v0, 0x7f0c0253

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 89
    :cond_0
    return-void

    .line 81
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->getNormalPrice()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public loadWidget()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    if-nez v0, :cond_0

    .line 59
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->setVisibility(I)V

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->updateWidget()V

    goto :goto_0
.end method

.method public onPriceChanged()V
    .locals 0

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->displayprice()V

    .line 132
    return-void
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->loadWidget()V

    .line 52
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->displayprice()V

    .line 120
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 136
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 137
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    .line 138
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->b:Landroid/content/Context;

    .line 139
    return-void
.end method

.method public set(Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    .line 47
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 125
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    .line 126
    return-void
.end method

.method public updateWidget()V
    .locals 4

    .prologue
    .line 95
    const v0, 0x7f0c0251

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->appTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->mContext:Landroid/content/Context;

    const-string v2, "isa_samsungapps_default"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->productImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 106
    const v0, 0x7f0c0252

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->sellerInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->a:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;->isWidgetContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->displayprice()V

    .line 114
    return-void
.end method
