.class public Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field filename:Ljava/lang/String;

.field imagefile:Ljava/io/File;

.field mBitmap:Landroid/graphics/Bitmap;

.field mReqHeight:I

.field mReqWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->filename:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 21
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    .line 22
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqWidth:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    .line 65
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->filename:Ljava/lang/String;

    .line 67
    invoke-virtual {p2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 68
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->filename:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->exist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->decodeBitmap()Z

    .line 73
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->filename:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 21
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    .line 22
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqWidth:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    .line 25
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    .line 26
    iput p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqWidth:I

    .line 28
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->filename:Ljava/lang/String;

    .line 29
    invoke-virtual {p2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 30
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->filename:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->exist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->decodeBitmap()Z

    .line 37
    :cond_0
    return-void
.end method

.method private decodeBitmap()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 145
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 147
    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    if-nez v3, :cond_0

    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqWidth:I

    if-eqz v3, :cond_1

    .line 150
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 154
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;)I

    move-result v3

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 157
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 158
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 166
    if-eqz v2, :cond_2

    .line 167
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mBitmap:Landroid/graphics/Bitmap;

    .line 177
    :goto_0
    return v0

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 177
    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    const-string v2, "SamsungApps"

    const-string v3, "OutofMemory"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    move v0, v1

    .line 175
    goto :goto_0
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 81
    if-ltz v0, :cond_0

    .line 83
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 87
    :cond_0
    return-object p0
.end method


# virtual methods
.method public calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 41
    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 42
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 45
    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    if-gt v1, v3, :cond_0

    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqWidth:I

    if-le v2, v3, :cond_1

    .line 46
    :cond_0
    if-le v2, v1, :cond_3

    .line 47
    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    if-nez v2, :cond_2

    .line 50
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 61
    :cond_1
    :goto_0
    return v0

    .line 54
    :cond_2
    int-to-float v0, v1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 57
    :cond_3
    int-to-float v0, v2

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mReqWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method public exist()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public writeFile(Ljava/io/InputStream;)Z
    .locals 5

    .prologue
    const/16 v0, 0x800

    .line 102
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 113
    :goto_0
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->imagefile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 114
    const/16 v2, 0x800

    :try_start_2
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 117
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    .line 119
    :goto_1
    const/4 v3, 0x0

    const/16 v4, 0x800

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 120
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_0
    move-exception v1

    .line 126
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    .line 137
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->decodeBitmap()Z

    move-result v0

    return v0

    .line 107
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 123
    :cond_0
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 132
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 127
    :catch_3
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2
.end method
