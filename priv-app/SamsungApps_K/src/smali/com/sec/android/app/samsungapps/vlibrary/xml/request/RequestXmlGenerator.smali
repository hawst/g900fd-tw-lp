.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;


# instance fields
.field private mRequestData:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->mRequestData:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

    .line 19
    return-void
.end method

.method private writeCloseTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 1

    .prologue
    .line 33
    const-string v0, ""

    invoke-interface {p2, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 34
    return-void
.end method

.method private writeOpenTagWithMapAttribute(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V
    .locals 4

    .prologue
    .line 24
    const-string v0, ""

    invoke-interface {p2, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 26
    invoke-virtual {p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 27
    const-string v3, ""

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p2, v3, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    .line 29
    :cond_0
    return-void
.end method

.method private writeParameters(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V
    .locals 5

    .prologue
    .line 39
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 40
    const-string v1, ""

    const-string v3, "value"

    invoke-interface {p1, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 41
    const-string v3, ""

    const-string v4, "name"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v3, v4, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 42
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 43
    const-string v0, ""

    const-string v1, "param"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public getLogXML()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public getXML()Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 51
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v0

    .line 52
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 54
    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 56
    const-string v2, "SamsungProtocol"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->mRequestData:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;->getSamsungProtocolMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->writeOpenTagWithMapAttribute(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V

    .line 58
    const-string v2, "request"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->mRequestData:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;->getSamsungProtocolMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->writeOpenTagWithMapAttribute(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->mRequestData:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;->getParameterMap()Ljava/util/HashMap;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->writeParameters(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V

    .line 63
    const-string v2, "request"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->writeCloseTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    .line 64
    const-string v2, "SamsungProtocol"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestXmlGenerator;->writeCloseTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    .line 66
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 70
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 72
    const-string v0, ""

    goto :goto_0
.end method
