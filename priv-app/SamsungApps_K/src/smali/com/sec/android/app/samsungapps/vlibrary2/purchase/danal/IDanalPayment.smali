.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mDanalActivity:Ljava/lang/Class;

.field private mDanalRequestCommunicator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;

.field private mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

.field private mFinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field private mValidator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalActivity:Ljava/lang/Class;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mFinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mValidator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalRequestCommunicator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 38
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalRequestCommunicator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalActivity:Ljava/lang/Class;

    .line 40
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mFinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->onFinalResult(Z)V

    return-void
.end method

.method private invokeUI(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalActivity:Ljava/lang/Class;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 60
    return-void
.end method


# virtual methods
.method public checkAgeLimitation(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)Z
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mValidator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mValidator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalValidator;->checkAgeLimitation()Z

    move-result v0

    return v0
.end method

.method public getCouponSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getItemToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->invokeUI(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    .line 150
    return-void
.end method

.method protected onPaymentSuccess()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;)V

    return-object v0
.end method

.method protected onResultSendAuthRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;)V

    return-object v0
.end method

.method public openDisclaimer(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 167
    const-string v0, "http://web.teledit.com/Danal/Notice/help/samsung/yak.html"

    .line 168
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 172
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IDanalPayment::openDisclaimer::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestAuthNumber(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;->startLoading()V

    .line 82
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalRequestCommunicator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->onResultSendAuthRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->_Context:Landroid/content/Context;

    invoke-interface {v1, p1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;->requestAuthNumber(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Landroid/content/Context;)V

    .line 84
    return-void
.end method

.method public requestPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)V
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;->startLoading()V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mDanalRequestCommunicator:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->onPaymentSuccess()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->mFinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->_Context:Landroid/content/Context;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestComminucator;->requestPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Landroid/content/Context;)V

    .line 98
    return-void
.end method
