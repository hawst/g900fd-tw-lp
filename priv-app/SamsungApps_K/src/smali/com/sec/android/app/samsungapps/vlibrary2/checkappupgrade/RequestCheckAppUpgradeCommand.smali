.class public Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CheckAppUpgradeResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;->_CheckAppUpgradeResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;->_Context:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;->requestCheckAppUpgrade(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public requestCheckAppUpgrade(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;->_CheckAppUpgradeResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/t;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/t;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/RequestCheckAppUpgradeCommand;)V

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->checkAppUpgrade(Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->dontDisplayErrorPopup()V

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 41
    return-void
.end method
