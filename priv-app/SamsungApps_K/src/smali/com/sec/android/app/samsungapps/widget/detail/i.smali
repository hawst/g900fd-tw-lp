.class final Lcom/sec/android/app/samsungapps/widget/detail/i;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/widget/LinearLayout;

.field final synthetic b:Landroid/widget/LinearLayout;

.field final synthetic c:[I

.field final synthetic d:I

.field final synthetic e:I

.field final synthetic f:Landroid/widget/TextView;

.field final synthetic g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;[IIILandroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->a:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->b:Landroid/widget/LinearLayout;

    iput-object p4, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->c:[I

    iput p5, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->d:I

    iput p6, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->e:I

    iput-object p7, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->f:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1058
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1062
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1075
    :cond_0
    :goto_0
    return-void

    .line 1066
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->a:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->c:[I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    .line 1067
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->c:[I

    aget v2, v2, v5

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->d:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1068
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->e:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 1070
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->f:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->e:I

    iget v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->e:I

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1071
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->f:Landroid/widget/TextView;

    int-to-float v0, v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v3

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v2, v6, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1072
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->g:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->f:Landroid/widget/TextView;

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->buttonTextSizeResize(Landroid/content/Context;Landroid/widget/TextView;I)I

    move-result v0

    .line 1073
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/i;->f:Landroid/widget/TextView;

    int-to-float v0, v0

    invoke-virtual {v1, v6, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method
