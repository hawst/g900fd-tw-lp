.class public Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field final A:I

.field final B:Ljava/lang/String;

.field final C:Ljava/lang/String;

.field final D:Ljava/lang/String;

.field final E:Ljava/lang/String;

.field final F:I

.field final G:I

.field H:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

.field private I:Landroid/content/Context;

.field private J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field private final K:Ljava/lang/String;

.field private L:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

.field private M:Z

.field private N:I

.field a:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field c:Landroid/content/BroadcastReceiver;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:I

.field final i:I

.field final j:I

.field final k:Ljava/lang/String;

.field final l:Ljava/lang/String;

.field final m:Ljava/lang/String;

.field final n:Ljava/lang/String;

.field final o:I

.field final p:I

.field final q:Ljava/lang/String;

.field final r:Ljava/lang/String;

.field final s:Ljava/lang/String;

.field final t:Ljava/lang/String;

.field final u:I

.field final v:I

.field final w:I

.field final x:I

.field final y:I

.field final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c:Landroid/content/BroadcastReceiver;

    .line 62
    const-string v0, "off"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d:Ljava/lang/String;

    .line 63
    const-string v0, "Data"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->e:Ljava/lang/String;

    .line 64
    const-string v0, "wifi"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->f:Ljava/lang/String;

    .line 65
    const-string v0, "on"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->g:Ljava/lang/String;

    .line 70
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->h:I

    .line 71
    iput v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->i:I

    .line 72
    iput v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->j:I

    .line 77
    const-string v0, "off"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->k:Ljava/lang/String;

    .line 78
    const-string v0, "Data"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->l:Ljava/lang/String;

    .line 79
    const-string v0, "wifi"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->m:Ljava/lang/String;

    .line 80
    const-string v0, "on"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->n:Ljava/lang/String;

    .line 85
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->o:I

    .line 86
    iput v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->p:I

    .line 91
    const-string v0, "off"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->q:Ljava/lang/String;

    .line 92
    const-string v0, "Data"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->r:Ljava/lang/String;

    .line 93
    const-string v0, "wifi"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->s:Ljava/lang/String;

    .line 94
    const-string v0, "on"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->t:Ljava/lang/String;

    .line 99
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->u:I

    .line 100
    iput v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->v:I

    .line 101
    iput v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->w:I

    .line 107
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->x:I

    .line 108
    iput v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->y:I

    .line 113
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->z:I

    .line 114
    iput v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->A:I

    .line 119
    const-string v0, "true"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->B:Ljava/lang/String;

    .line 120
    const-string v0, "false"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->C:Ljava/lang/String;

    .line 121
    const-string v0, "true"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->D:Ljava/lang/String;

    .line 122
    const-string v0, "false"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->E:Ljava/lang/String;

    .line 123
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->F:I

    .line 124
    iput v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->G:I

    .line 126
    const-string v0, "SettingListActivityHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->K:Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->H:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 1553
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->M:Z

    .line 1554
    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->N:I

    .line 133
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    .line 134
    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 229
    const/4 v0, 0x0

    .line 231
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    .line 238
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "una_setting"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 258
    :goto_0
    return v0

    .line 242
    :catch_0
    move-exception v1

    const-string v1, "SettingListActivityHelper::getUpdateNotificationDefaultValue::SettingNotFoundException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    .line 248
    const/16 v1, 0x48

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 249
    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 251
    :cond_1
    const-string v0, "0"

    .line 254
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createForceLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/ji;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/ji;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/kj;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/kj;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080276

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v5, 0x7f080238

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->M:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d()I

    move-result v0

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 471
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 472
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->logout()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/jo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/jo;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 480
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method private c()[Ljava/lang/String;
    .locals 8

    .prologue
    const v7, 0x7f08018b

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 563
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f08019f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getExtraPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    .line 575
    new-array v0, v6, [Ljava/lang/String;

    .line 576
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 577
    aput-object v1, v0, v5

    .line 590
    :goto_0
    return-object v0

    .line 584
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 585
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 586
    aput-object v1, v0, v5

    .line 587
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f0801a0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0
.end method

.method private d()I
    .locals 2

    .prologue
    .line 612
    const/4 v0, 0x0

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 616
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;-><init>(Landroid/content/Context;)V

    .line 617
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->getSetting()I

    move-result v0

    .line 619
    :cond_0
    return v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->L:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getAutoUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->userCancel()V

    return-void
.end method

.method private e()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 899
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 902
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080304

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 903
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080305

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 905
    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->g()I

    move-result v0

    return v0
.end method

.method private f()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 1027
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 1030
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080304

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1031
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080305

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1033
    return-object v0
.end method

.method private g()I
    .locals 3

    .prologue
    .line 1043
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1046
    const-string v1, "notify_store_activities_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1048
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1050
    :cond_0
    const-string v0, "0"

    .line 1053
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1056
    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->j()Landroid/content/BroadcastReceiver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->i()I

    move-result v0

    return v0
.end method

.method private h()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 1213
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 1216
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080304

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1217
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080305

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1219
    return-object v0
.end method

.method private i()I
    .locals 3

    .prologue
    .line 1231
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1232
    const-string v1, "ad_preference_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1234
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1236
    :cond_0
    const-string v0, "1"

    .line 1240
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1242
    return v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->N:I

    return v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->N:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->N:I

    return v0
.end method

.method private j()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 1557
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1558
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c:Landroid/content/BroadcastReceiver;

    .line 1560
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/jh;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/jh;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->N:I

    return v0
.end method

.method private k()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 1719
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 1722
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080304

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1723
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080305

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1725
    return-object v0
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->M:Z

    return v0
.end method


# virtual methods
.method final a(I)V
    .locals 3

    .prologue
    .line 1843
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1844
    const-string v1, "purchase_protection_setting"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1845
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    .line 142
    return-void
.end method

.method public getSubTitleAutoUpdate()Ljava/lang/String;
    .locals 3

    .prologue
    .line 525
    const-string v0, ""

    .line 527
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d()I

    move-result v1

    .line 528
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    .line 530
    packed-switch v1, :pswitch_data_0

    .line 551
    :cond_0
    :goto_0
    return-object v0

    .line 533
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v1, 0x7f08018a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 537
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f08019f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 541
    :pswitch_2
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getExtraPhoneType()I

    move-result v1

    if-eqz v1, :cond_0

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v1, 0x7f0801a0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 530
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getSubTitleUpdateNotification()Ljava/lang/String;
    .locals 3

    .prologue
    .line 150
    const-string v0, ""

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a()I

    move-result v1

    .line 154
    packed-switch v1, :pswitch_data_0

    .line 175
    :goto_0
    return-object v0

    .line 157
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v1, 0x7f080305

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 161
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->doesSupportPhoneFeature()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f080297

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 174
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v1, 0x7f080338

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAboutClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x1

    return v0
.end method

.method public onAccountClick(Landroid/preference/Preference;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 437
    const-string v1, ""

    .line 440
    new-instance v2, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/app/Activity;)V

    .line 441
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->isExistSamsungAccount()Z

    move-result v0

    if-ne v0, v4, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v4, :cond_1

    .line 446
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 466
    :goto_0
    return-object v0

    .line 456
    :cond_0
    const-string v0, "SettingListActivityHelper::onAccountClick::Not existing Samsung account"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 457
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b()V

    move-object v0, v1

    .line 459
    goto :goto_0

    .line 462
    :cond_1
    const-string v0, "SettingListActivityHelper::onAccountClick::Not existing account"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 463
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b()V

    move-object v0, v1

    goto :goto_0
.end method

.method public onAdPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 1251
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 1255
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 1257
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->h()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1258
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/OptionItem;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->h()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {v3, v4}, Lcom/sec/android/app/samsungapps/widget/OptionItem;-><init>(Ljava/lang/String;)V

    .line 1259
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1257
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1262
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v4, 0x7f04001d

    invoke-direct {v0, v3, v4, v2}, Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1264
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 1265
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080317

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1266
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->i()I

    move-result v2

    new-instance v3, Lcom/sec/android/app/samsungapps/iu;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/iu;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1386
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/jb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jb;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1400
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/jc;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/jc;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1407
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 1409
    const/4 v0, 0x1

    return v0
.end method

.method public onAdSettingClick(Landroid/preference/Preference;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 401
    if-ne p2, v1, :cond_1

    .line 406
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeADChecked(Z)V

    .line 408
    new-instance v0, Lcom/sec/android/app/samsungapps/AdNotificationDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/AdNotificationDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 409
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/AdNotificationDialogBuilder;->create()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    .line 410
    if-nez v0, :cond_0

    .line 412
    const-string v0, "SettingListActivityHelper::onAdSettingClick::_dialog is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 427
    :goto_0
    return v2

    .line 416
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto :goto_0

    .line 424
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeADChecked(Z)V

    goto :goto_0
.end method

.method public onAllShareClick(Landroid/preference/Preference;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 493
    if-ne p2, v2, :cond_0

    .line 495
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeSuggestAllShareContentChecked(Z)V

    .line 496
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifySuggestAllShareContent()V

    .line 503
    :goto_0
    return v2

    .line 500
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeSuggestAllShareContentChecked(Z)V

    goto :goto_0
.end method

.method public onAutoUpdateClick(Landroid/preference/Preference;)Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 637
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 640
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;-><init>(Landroid/content/Context;)V

    .line 642
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 644
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 645
    new-instance v4, Lcom/sec/android/app/samsungapps/widget/OptionItem;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c()[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v0

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    aput-object v11, v6, v1

    aput-object v11, v6, v10

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v9, 0x7f0801a1

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    aget-object v6, v6, v0

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/samsungapps/widget/OptionItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 649
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v5, 0x7f04001d

    invoke-direct {v0, v4, v5, v3}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 651
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 652
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v4, 0x7f08019b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d()I

    move-result v3

    new-instance v4, Lcom/sec/android/app/samsungapps/jp;

    invoke-direct {v4, p0, v2, v0}, Lcom/sec/android/app/samsungapps/jp;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;)V

    invoke-virtual {v1, v0, v3, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 867
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/kh;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/kh;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 881
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/ki;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ki;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 889
    return v10
.end method

.method public onNotifyAppUpdatesClick(Landroid/preference/Preference;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 955
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 956
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 959
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 962
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 964
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->e()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 965
    new-instance v4, Lcom/sec/android/app/samsungapps/widget/OptionItem;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->e()[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v0

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v8, 0x7f0801a1

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v7, 0x0

    aput-object v7, v6, v2

    aget-object v6, v6, v0

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/samsungapps/widget/OptionItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 964
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 969
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v5, 0x7f04001d

    invoke-direct {v0, v4, v5, v3}, Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 970
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 971
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v5, 0x7f080161

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 972
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getSettingsProviderCreator(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;->createAutoUpdateNotification()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;->isOn()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    new-instance v4, Lcom/sec/android/app/samsungapps/kk;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/app/samsungapps/kk;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/NotifyAppUpdatesArrayAdapter;)V

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 993
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/kl;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/kl;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f08023d

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/samsungapps/iq;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/iq;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 1017
    return v2

    :cond_2
    move v1, v2

    .line 972
    goto :goto_1
.end method

.method public onNotifyStoreActivitiesClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 1065
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 1068
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 1070
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->f()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1071
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/OptionItem;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->f()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {v3, v4}, Lcom/sec/android/app/samsungapps/widget/OptionItem;-><init>(Ljava/lang/String;)V

    .line 1072
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1070
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1075
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v4, 0x7f04001d

    invoke-direct {v0, v3, v4, v2}, Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1077
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 1078
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080162

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1079
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->g()I

    move-result v2

    new-instance v3, Lcom/sec/android/app/samsungapps/ir;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/ir;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/is;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/is;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/it;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/it;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1195
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1196
    const-string v1, "intent.action.push_reg"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1197
    const-string v1, "intent.action.push_dereg"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1198
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1199
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->j()Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 1203
    return v5
.end method

.method public onPurchaseProtectionClick(Landroid/preference/Preference;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1734
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 1738
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 1740
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 1742
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->k()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1743
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/OptionItem;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->k()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v6, v5, v1

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v7, 0x7f0800fd

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    aget-object v5, v5, v0

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/samsungapps/widget/OptionItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1742
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1747
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v4, 0x7f04001d

    invoke-direct {v0, v3, v4, v2}, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1749
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 1750
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080102

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1751
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;->getPreferenceDefaultValue()I

    move-result v2

    new-instance v3, Lcom/sec/android/app/samsungapps/jj;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/jj;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1814
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/jm;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jm;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1829
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/jn;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/jn;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1836
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 1838
    return v8
.end method

.method public onPushNotificationClick(Landroid/preference/Preference;Z)Z
    .locals 3

    .prologue
    .line 369
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 370
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 375
    const-string v1, "extra_command"

    const-string v2, "cmd_registration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 387
    return p2

    .line 382
    :cond_0
    const-string v1, "extra_command"

    const-string v2, "cmd_deregistration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onResetLocaleClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    return v0
.end method

.method public onSearchSettingClick(Landroid/preference/Preference;)Z
    .locals 11

    .prologue
    const/4 v6, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1450
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1453
    new-array v3, v6, [Ljava/lang/String;

    const-string v0, "user_keywords_search_setting"

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    const-string v0, "auto_complete_search_setting"

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v10

    .line 1458
    const-string v0, ""

    aget-object v1, v3, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1460
    const-string v0, "true"

    aput-object v0, v3, v9

    .line 1463
    :cond_0
    const-string v0, ""

    aget-object v1, v3, v10

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1465
    const-string v0, "true"

    aput-object v0, v3, v10

    .line 1468
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1469
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 1471
    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 1472
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 1473
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v4, 0x7f0802f1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1474
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-array v5, v6, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    new-array v6, v6, [Z

    fill-array-data v6, :array_0

    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v7, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    const-string v0, "user_keywords_search_setting"

    invoke-virtual {v7, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "auto_complete_search_setting"

    invoke-virtual {v7, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_3
    const-string v1, "true"

    const-string v8, "user_keywords_search_setting"

    invoke-virtual {v7, v8, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_4
    if-eqz v0, :cond_5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    :cond_5
    const-string v0, "true"

    const-string v8, "auto_complete_search_setting"

    invoke-virtual {v7, v8, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    aput-boolean v1, v6, v9

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v6, v10

    new-instance v0, Lcom/sec/android/app/samsungapps/jd;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/samsungapps/jd;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;[Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMultiChoiceItems([Ljava/lang/String;[ZLcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1505
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/je;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/je;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1520
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v4, 0x7f0802ef

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/sec/android/app/samsungapps/jf;

    invoke-direct {v4, p0, v2, v3}, Lcom/sec/android/app/samsungapps/jf;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;[Ljava/lang/String;)V

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1529
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 1531
    return v10

    .line 1474
    :array_0
    .array-data 1
        0x1t
        0x1t
    .end array-data
.end method

.method public onUpdateNotificationClick(Landroid/preference/Preference;)Z
    .locals 8

    .prologue
    const v7, 0x7f080305

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 273
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v2, 0x7f0802b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getExtraPhoneType()I

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v6, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080304

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a()I

    move-result v2

    new-instance v3, Lcom/sec/android/app/samsungapps/ip;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/ip;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems([Ljava/lang/String;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/jg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jg;-><init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->J:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 329
    return v4

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080297

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v5

    aput-object v2, v0, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->I:Landroid/content/Context;

    const v3, 0x7f080338

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    goto :goto_0
.end method

.method public setWidget(Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;)V
    .locals 0

    .prologue
    .line 1535
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->L:Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    .line 1536
    return-void
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 1540
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 1542
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 1545
    :cond_0
    return-void
.end method
