.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->_ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->_ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->needSendSMS()Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->onFinalResult(Z)V

    .line 38
    :goto_0
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->_ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;->getPSMSDestNo()Ljava/lang/String;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->_ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;->getPSMSSendingMessage()Ljava/lang/String;

    move-result-object v1

    .line 29
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->_ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;->createSMSSender()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/o;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/o;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;)V

    invoke-interface {v2, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;->sendSMS(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    goto :goto_0
.end method

.method protected needSendSMS()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;->_ISendPSMSData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;->needSendSMS()Z

    move-result v0

    return v0
.end method
