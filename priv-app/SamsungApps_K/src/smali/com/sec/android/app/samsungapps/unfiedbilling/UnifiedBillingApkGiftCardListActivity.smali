.class public Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;
.super Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;
.source "ProGuard"


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->a:I

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->b:I

    return-void
.end method

.method private a()Lcom/sec/android/app/billing/helper/GiftCardData;
    .locals 5

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/billing/helper/GiftCardData;

    invoke-direct {v0}, Lcom/sec/android/app/billing/helper/GiftCardData;-><init>()V

    .line 65
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    .line 66
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->appServiceID:Ljava/lang/String;

    .line 67
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getRealCountryCode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->country:Ljava/lang/String;

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 70
    const-string v3, "_"

    const-string v4, "-"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 71
    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->language:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->uxVersion:Ljava/lang/String;

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->uxVersion:Ljava/lang/String;

    .line 73
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getBillingServerType()Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->getUPServerURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->upServerURL:Ljava/lang/String;

    .line 75
    new-instance v2, Lcom/sec/android/app/billing/helper/UserInfo;

    invoke-direct {v2}, Lcom/sec/android/app/billing/helper/UserInfo;-><init>()V

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    .line 76
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getUserID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/UserInfo;->userID:Ljava/lang/String;

    .line 77
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/UserInfo;->userEmail:Ljava/lang/String;

    .line 78
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/UserInfo;->authAppID:Ljava/lang/String;

    .line 79
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    sget-object v3, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->_Token:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/UserInfo;->accessToken:Ljava/lang/String;

    .line 80
    const-string v2, "+00:00"

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->timeOffset:Ljava/lang/String;

    .line 81
    new-instance v2, Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-direct {v2}, Lcom/sec/android/app/billing/helper/DeviceInfo;-><init>()V

    iput-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    .line 82
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceID:Ljava/lang/String;

    .line 84
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_KNOX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceID:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_0
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceUID:Ljava/lang/String;

    .line 94
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    const-string v3, "M|640|480"

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->displayType:Ljava/lang/String;

    .line 95
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->mcc:Ljava/lang/String;

    .line 96
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->mnc:Ljava/lang/String;

    .line 97
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->csc:Ljava/lang/String;

    .line 98
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getOpenApiVersion()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->osVersion:Ljava/lang/String;

    .line 99
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->loadODCVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->appVersion:Ljava/lang/String;

    .line 101
    return-object v0

    :catch_0
    move-exception v2

    goto :goto_0

    .line 92
    :catch_1
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 106
    packed-switch p2, :pswitch_data_0

    .line 115
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->finish()V

    .line 117
    return-void

    .line 109
    :pswitch_0
    const-string v0, "Calling UnifiedBilling apk GiftCardList Activity succeeded."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :pswitch_1
    const-string v0, "Calling UnifiedBilling apk GiftCardList Activity canceled."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/unfiedbilling/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/c;-><init>(Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 37
    return-void
.end method

.method protected onExecute()V
    .locals 8

    .prologue
    .line 43
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->a()Lcom/sec/android/app/billing/helper/GiftCardData;

    move-result-object v5

    .line 44
    invoke-static {v5, p0}, Lcom/sec/android/app/billing/helper/RequestParamValidator;->check(Lcom/sec/android/app/billing/helper/GiftCardData;Landroid/content/Context;)V

    .line 45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 47
    invoke-static {p0}, Lcom/sec/android/app/billing/helper/UPHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/billing/helper/UPHelper;

    move-result-object v0

    const/16 v2, 0x4e30

    const-string v4, "GIFT_CARD"

    const-string v6, ""

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->aidlHandler:Landroid/os/Handler;

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/billing/helper/UPHelper;->requestBilling(Landroid/app/Activity;IILjava/lang/String;Lcom/sec/android/app/billing/helper/GiftCardData;Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 59
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedBillingApkGiftCardListActivity::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->finish()V

    goto :goto_0

    .line 51
    :catch_1
    move-exception v0

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedBillingApkGiftCardListActivity::SendIntentException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/IntentSender$SendIntentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->finish()V

    goto :goto_0

    .line 54
    :catch_2
    move-exception v0

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedBillingApkGiftCardListActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;->finish()V

    goto :goto_0
.end method
