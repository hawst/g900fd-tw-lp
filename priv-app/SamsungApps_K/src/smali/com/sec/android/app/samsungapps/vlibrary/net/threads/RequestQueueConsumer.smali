.class public Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private _IRequestQueueConsumerData:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;

.field private _NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_IRequestQueueConsumerData:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;

    .line 15
    return-void
.end method

.method private sleep()V
    .locals 2

    .prologue
    .line 57
    const-wide/16 v0, 0xa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_IRequestQueueConsumerData:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;->onDestroy()V

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_IRequestQueueConsumerData:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;

    .line 52
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_IRequestQueueConsumerData:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;->take()Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;

    move-result-object v0

    .line 23
    if-nez v0, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->onDestroy()V

    .line 45
    :goto_0
    return-void

    .line 28
    :cond_1
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 30
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V

    .line 33
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->isSucceed()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 35
    :goto_1
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->isSucceed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->needRetry()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->sleep()V

    .line 38
    const-string v1, "retry"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->_NetAPI:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;->send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;->sleep()V

    goto :goto_1

    .line 42
    :cond_2
    const-string v1, "end retry loop"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 44
    :cond_3
    if-nez v0, :cond_0

    goto :goto_0
.end method
