.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 0

    .prologue
    .line 9
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    return-void
.end method

.method public static register(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;

    const-string v2, "customerCouponRegister"

    const-string v3, "4030"

    move-object v1, p0

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 15
    const-string v1, "couponCode"

    invoke-virtual {v0, v1, p1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 16
    return-object v0
.end method
