.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:I

.field private d:Z

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Landroid/view/View;

.field private k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

.field private l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

.field private m:Ljava/util/ArrayList;

.field private n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;


# direct methods
.method public constructor <init>(ILjava/lang/String;IZZ)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    .line 31
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->b:I

    .line 36
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->c:I

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->d:Z

    .line 46
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->e:I

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->f:Z

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->g:Z

    .line 61
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->h:Z

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->j:Landroid/view/View;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 95
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->b:I

    .line 96
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    .line 97
    iput p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->c:I

    .line 98
    iput-boolean p4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->f:Z

    .line 99
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->g:Z

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    .line 102
    return-void
.end method


# virtual methods
.method public addSubItem(IILjava/lang/String;Z)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    .line 317
    if-ltz p1, :cond_0

    if-eqz p3, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 321
    :cond_0
    const-string v0, "addSubItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 322
    const/4 v0, 0x0

    .line 329
    :goto_0
    return-object v0

    .line 325
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v3, -0x1

    const/4 v4, 0x0

    move v1, p1

    move-object v2, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addSubItem(ILjava/lang/String;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 268
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 272
    :cond_0
    const-string v0, "addSubItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 273
    const/4 v0, 0x0

    .line 280
    :goto_0
    return-object v0

    .line 276
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v3, -0x1

    move v1, p1

    move-object v2, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addSubItem(ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 245
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 249
    :cond_0
    const-string v0, "addSubItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 250
    const/4 v0, 0x0

    .line 257
    :goto_0
    return-object v0

    .line 253
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v3, -0x1

    move v1, p1

    move-object v2, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, p3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addSubItem(ILjava/lang/String;Z)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    .line 292
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 296
    :cond_0
    const-string v0, "addSubItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 297
    const/4 v0, 0x0

    .line 304
    :goto_0
    return-object v0

    .line 300
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v3, -0x1

    const/4 v4, 0x0

    move v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 114
    :cond_0
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->b:I

    .line 115
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->c:I

    .line 117
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->f:Z

    .line 118
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->g:Z

    .line 119
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->h:Z

    .line 121
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 122
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->j:Landroid/view/View;

    .line 125
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public create(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 199
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    const v2, 0x7f040018

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    move-object v1, v0

    .line 207
    :goto_0
    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->setNotifyOnChange(Z)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 213
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 216
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->e:I

    if-ne v3, v4, :cond_0

    .line 218
    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setChecked(Z)V

    .line 219
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->updateChecked(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V

    .line 222
    :cond_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setAdapter(Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;)V

    goto :goto_1

    .line 204
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    const v2, 0x7f040019

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    move-object v1, v0

    goto :goto_0

    .line 225
    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 227
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->e:I

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setSingleChoiceItems(Landroid/widget/ArrayAdapter;ILcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    const v3, 0x7f0802b7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;)V

    .line 233
    :cond_3
    return-object v0
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->b:I

    return v0
.end method

.method public getListener()Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    return-object v0
.end method

.method public getParent()Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    return-object v0
.end method

.method public getResourceImg()I
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->c:I

    return v0
.end method

.method public getSubItem(I)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    return-object v0
.end method

.method public getSubItemArray()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSubItemCheckedIndex()I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->e:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->j:Landroid/view/View;

    return-object v0
.end method

.method public hasCheck()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->g:Z

    return v0
.end method

.method public hasChild()Z
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->f:Z

    return v0
.end method

.method public hasSubItem(I)Z
    .locals 3

    .prologue
    .line 365
    const/4 v1, 0x0

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 368
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 371
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 373
    const/4 v0, 0x1

    .line 378
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->h:Z

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->d:Z

    return v0
.end method

.method public onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 135
    if-nez v0, :cond_0

    .line 137
    const-string v0, "Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 144
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setChecked(Z)V

    .line 145
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->updateChecked(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V

    .line 147
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getAdapter()Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_1

    .line 150
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->notifyDataSetChanged()V

    .line 154
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 164
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    const-string v1, "SamsungAppsMenuItem::onClick::IllegalArgumentException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 159
    :catch_1
    move-exception v1

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SamsungAppsMenuItem::onClick::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public removeSubItem(I)Z
    .locals 4

    .prologue
    .line 339
    const/4 v1, 0x0

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 343
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 344
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 346
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 347
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 349
    const/4 v1, 0x1

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move v0, v1

    .line 355
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    .line 509
    return-void
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 493
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->h:Z

    .line 494
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->a:Landroid/content/Context;

    .line 459
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 468
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->d:Z

    .line 469
    return-void
.end method

.method public setHasCheck(Z)V
    .locals 0

    .prologue
    .line 488
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->g:Z

    .line 489
    return-void
.end method

.method public setHasChild(Z)V
    .locals 0

    .prologue
    .line 483
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->f:Z

    .line 484
    return-void
.end method

.method public setId(I)V
    .locals 0

    .prologue
    .line 463
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->b:I

    .line 464
    return-void
.end method

.method public setListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->n:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 524
    return-void
.end method

.method public setParent(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->l:Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 519
    return-void
.end method

.method public setResourceImg(I)V
    .locals 0

    .prologue
    .line 473
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->c:I

    .line 474
    return-void
.end method

.method public setSubItem(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 514
    return-void
.end method

.method public setSubItemCheckedIndex(I)V
    .locals 0

    .prologue
    .line 478
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->e:I

    .line 479
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 498
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->i:Ljava/lang/String;

    .line 499
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->j:Landroid/view/View;

    .line 504
    return-void
.end method

.method public updateChecked(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 175
    :cond_0
    const-string v0, "updateChecked::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 190
    :goto_0
    return-void

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 180
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 183
    if-eq v0, p1, :cond_2

    .line 185
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setChecked(Z)V

    goto :goto_1

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->k:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
