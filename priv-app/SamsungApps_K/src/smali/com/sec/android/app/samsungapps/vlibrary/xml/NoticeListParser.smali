.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 19
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;-><init>()V

    .line 24
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 25
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeArray()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeList()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v0

    .line 38
    const-string v1, "startNum"

    invoke-virtual {p1, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 39
    if-gt v1, v4, :cond_0

    .line 40
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->clear()V

    .line 42
    :cond_0
    const-string v1, "totalCount"

    invoke-virtual {p1, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 44
    if-eq v1, v3, :cond_1

    .line 45
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->setTotalCount(I)V

    .line 48
    :cond_1
    const-string v2, "endNum"

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 50
    if-eq v1, v3, :cond_2

    if-ne v2, v3, :cond_3

    .line 59
    :cond_2
    :goto_0
    return-void

    .line 54
    :cond_3
    if-ne v1, v2, :cond_4

    .line 55
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->setCompleted(Z)V

    .line 58
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->setNeverLoaded(Z)V

    goto :goto_0
.end method
