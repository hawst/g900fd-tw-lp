.class public Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;


# instance fields
.field protected _IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

.field a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private d:Landroid/os/Handler;

.field private e:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->b:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    .line 26
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->d:Landroid/os/Handler;

    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 230
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 232
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCodeString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    .line 233
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->d:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/c;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 60
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onPaymentSuccessForDownloadURL()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/e;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->forceLoad()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_ORDERID:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    .line 81
    :goto_0
    return-void

    .line 69
    :cond_0
    const-string v0, "2"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_PREPOSTAPP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    goto :goto_0

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_FREE_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    goto :goto_0

    .line 79
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_PAID_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    goto :goto_0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->b:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;)V
    .locals 7

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/i;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 88
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onNeedPayment()V

    goto :goto_0

    .line 91
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/h;

    invoke-direct {v6, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 94
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->onReqDownloadEx()V

    goto :goto_0

    .line 97
    :pswitch_3
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/d;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->easybuyPurchase(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->RECEIVE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 100
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLFailed()V

    goto/16 :goto_0

    .line 103
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLSucceed()V

    goto/16 :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;)V

    return-void
.end method

.method protected onReqDownloadEx()V
    .locals 7

    .prologue
    .line 184
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/f;

    invoke-direct {v6, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->downloadEx(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 202
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 203
    return-void
.end method

.method protected onReqDownloadEx2()V
    .locals 4

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    .line 208
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/g;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->downloadEx2(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 223
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 224
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    .line 39
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->b:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    .line 44
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$State;)V

    return-void
.end method
