.class final Lcom/sec/android/app/samsungapps/commands/ab;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field final synthetic b:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/ab;->b:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/commands/ab;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDialogConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ab;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ab;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 198
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/ab;->b:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 200
    if-eqz v2, :cond_0

    .line 201
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 203
    :pswitch_0
    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 204
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 208
    :pswitch_1
    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 209
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
