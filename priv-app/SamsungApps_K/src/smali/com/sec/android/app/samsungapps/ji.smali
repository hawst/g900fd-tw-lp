.class final Lcom/sec/android/app/samsungapps/ji;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

.field final synthetic b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V
    .locals 0

    .prologue
    .line 1657
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ji;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 1660
    if-eqz p1, :cond_0

    .line 1665
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1666
    const-string v1, "extra_command"

    const-string v2, "cmd_registration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1667
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1668
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 1669
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Z)Z

    .line 1679
    :goto_0
    return-void

    .line 1673
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Z)Z

    .line 1674
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ji;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "notify_store_activities_setting"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1676
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ji;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    const-string v1, "W5000c"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Ljava/lang/String;)V

    goto :goto_0
.end method
