.class public Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;


# instance fields
.field protected mDownloadItemCreator:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;->mDownloadItemCreator:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    .line 18
    return-void
.end method


# virtual methods
.method public createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;
    .locals 6

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;->createDownloadPrecheckerFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;->mDownloadItemCreator:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V

    return-object v0
.end method

.method public createDownloadPrecheckerFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;->createPermissionFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;)V

    return-object v0
.end method

.method protected createPermissionFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;-><init>(Z)V

    return-object v0
.end method

.method public createUpdateCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;
    .locals 6

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;->createDownloadPrecheckerFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;->mDownloadItemCreator:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V

    return-object v0
.end method
