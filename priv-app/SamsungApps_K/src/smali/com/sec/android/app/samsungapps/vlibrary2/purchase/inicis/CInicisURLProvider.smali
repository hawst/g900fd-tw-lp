.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IURLProvider;


# instance fields
.field protected final ACTION_URL_CREDIT_CARD:Ljava/lang/String;

.field protected final ACTION_URL_CULTURE_GIFTCARD:Ljava/lang/String;

.field protected final ACTION_URL_HAPPY_MONEY:Ljava/lang/String;

.field protected final ACTION_URL_PHONE_BILL:Ljava/lang/String;

.field protected final ACTION_URL_TRANSFER_FUNDS:Ljava/lang/String;

.field protected final ACTION_URL_U_POINT:Ljava/lang/String;

.field protected final ACTION_URL_VIRTUAL_ACCOUNT:Ljava/lang/String;

.field private final INICIS_CANCEL_RESULT_SCHEME:Ljava/lang/String;

.field private final INICIS_RESULT_SCHEME:Ljava/lang/String;

.field protected final ISP_DOWNLOAD_URL:Ljava/lang/String;

.field private _InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "https://mobile.inicis.com/smart/wcard/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_CREDIT_CARD:Ljava/lang/String;

    .line 24
    const-string v0, "https://mobile.inicis.com/smart/vbank/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_VIRTUAL_ACCOUNT:Ljava/lang/String;

    .line 25
    const-string v0, "https://mobile.inicis.com/smart/mobile/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_PHONE_BILL:Ljava/lang/String;

    .line 26
    const-string v0, "https://mobile.inicis.com/smart/culture/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_CULTURE_GIFTCARD:Ljava/lang/String;

    .line 27
    const-string v0, "https://mobile.inicis.com/smart/hpmn/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_HAPPY_MONEY:Ljava/lang/String;

    .line 28
    const-string v0, "https://mobile.inicis.com/smart/bank/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_TRANSFER_FUNDS:Ljava/lang/String;

    .line 29
    const-string v0, "https://mobile.inicis.com/smart/upoint/"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ACTION_URL_U_POINT:Ljava/lang/String;

    .line 82
    const-string v0, "samsungappsinicisresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->INICIS_RESULT_SCHEME:Ljava/lang/String;

    .line 83
    const-string v0, "samsungappsiniciscancelresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->INICIS_CANCEL_RESULT_SCHEME:Ljava/lang/String;

    .line 84
    const-string v0, "http://mobile.vpay.co.kr/jsp/MISP/andown.jsp"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->ISP_DOWNLOAD_URL:Ljava/lang/String;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    .line 16
    return-void
.end method

.method private makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x3d

    .line 52
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 54
    :cond_0
    const-string v0, ""

    .line 77
    :goto_0
    return-object v0

    .line 61
    :cond_1
    :try_start_0
    const-string v0, "EUC-KR"

    invoke-static {p2, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 69
    :goto_1
    const-string v0, "P_MID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "&"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private parameters()Ljava/lang/String;
    .locals 4

    .prologue
    .line 88
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->getMerchantID()Ljava/lang/String;

    move-result-object v0

    .line 92
    const-string v2, "P_MID"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->getPaymentID()Ljava/lang/String;

    move-result-object v0

    .line 96
    const-string v2, "P_OID"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->getPayPrice()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    const-string v2, "P_AMT"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    .line 104
    const-string v2, "P_UNAME"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 107
    const-string v0, ""

    .line 108
    const-string v2, "P_NOTI"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    const-string v0, "samsungappsiniciscancelresult://"

    .line 112
    const-string v2, "P_CANCEL_URL"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 115
    const-string v0, "samsungappsinicisresult://"

    .line 116
    const-string v2, "P_NEXT_URL"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->getNotiURL()Ljava/lang/String;

    move-result-object v0

    .line 120
    const-string v2, "P_NOTI_URL"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 123
    const-string v0, "samsungappsinicisresult://"

    .line 124
    const-string v2, "P_RETURN_URL"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->getProductName()Ljava/lang/String;

    move-result-object v0

    .line 128
    const-string v2, "P_GOODS"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 132
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v0

    .line 137
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_0

    const-string v2, "+821"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    :cond_0
    const-string v2, "P_MOBILE"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string v2, "P_EMAIL"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    const-string v0, "1"

    .line 148
    const-string v2, "P_HPP_METHOD"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    const-string v0, ""

    .line 152
    const-string v2, "P_VBANK_DT"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    const-string v0, ""

    .line 156
    const-string v2, "P_CARD_OPTION"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 159
    const-string v0, ""

    .line 160
    const-string v2, "P_APP_BASE"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    const-string v0, ""

    .line 164
    const-string v2, "P_MLOGO_IMAGE"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->getProductImageURL()Ljava/lang/String;

    move-result-object v0

    .line 168
    const-string v2, "P_GOOD_IMAGE"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 171
    const-string v0, "nexturl=get"

    .line 172
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173
    const-string v2, "&device=tablet"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    :cond_1
    const-string v2, "P_RESERVED"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 184
    const-string v0, ""

    .line 185
    const-string v2, "P_TAX"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 188
    const-string v0, ""

    .line 189
    const-string v2, "P_TAXFREE"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->makeParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 191
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 134
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private url()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->isUpoint()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 47
    const-string v0, ""

    :goto_0
    return-object v0

    .line 41
    :pswitch_0
    const-string v0, "https://mobile.inicis.com/smart/wcard/"

    goto :goto_0

    .line 43
    :pswitch_1
    const-string v0, "https://mobile.inicis.com/smart/mobile/"

    goto :goto_0

    .line 45
    :pswitch_2
    const-string v0, "https://mobile.inicis.com/smart/upoint/"

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->url()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->parameters()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;->isValid()Z

    move-result v0

    return v0
.end method

.method public setIInicisURLInfoProvider(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLProvider;->_InicisURLInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;

    .line 21
    return-void
.end method
