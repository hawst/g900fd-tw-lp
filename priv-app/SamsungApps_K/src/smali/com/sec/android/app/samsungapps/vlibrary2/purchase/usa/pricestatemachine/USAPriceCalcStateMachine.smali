.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    return-void
.end method

.method private entry(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    :goto_0
    :pswitch_0
    return-void

    .line 93
    :pswitch_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->requestPrice()V

    .line 94
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->startLoading()V

    goto :goto_0

    .line 97
    :pswitch_2
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->sendFailSignal()V

    .line 98
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 102
    :pswitch_3
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->sendSuccessSignal()V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private exit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 115
    :goto_0
    return-void

    .line 112
    :pswitch_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->endLoading()V

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    .line 133
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    return-object v0
.end method

.method private setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->exit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    .line 82
    invoke-interface {p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->entry(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    .line 84
    return-void
.end method


# virtual methods
.method public execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V
    .locals 3

    .prologue
    .line 29
    const-string v0, "hcjo"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "USAPriceCals "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/d;->b:[I

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 77
    :goto_0
    return-void

    .line 33
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 36
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->hasCard()Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 49
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->NOT_USA_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 53
    :cond_1
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->REQUESTSTATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 58
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 61
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 64
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 69
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 72
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->REQUESTSTATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_6
    .end packed-switch

    .line 33
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 58
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 69
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_7
    .end packed-switch
.end method
