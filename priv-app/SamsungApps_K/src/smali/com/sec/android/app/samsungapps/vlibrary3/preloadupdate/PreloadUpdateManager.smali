.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;

.field private d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

.field private i:Landroid/os/CountDownTimer;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

.field private k:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    .line 270
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;

    .line 300
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->k:Landroid/os/Handler;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->d:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->e:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    .line 44
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;

    .line 45
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 46
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    .line 47
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->k:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 184
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->e()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->k:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/k;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 309
    return-void
.end method

.method private static b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 188
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 195
    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 194
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private static c()Z
    .locals 2

    .prologue
    .line 225
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->d()Ljava/lang/String;

    move-result-object v0

    .line 226
    const-string v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ACG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "USC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LRA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    :try_start_0
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 241
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    const-string v0, ""

    goto :goto_0

    .line 240
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private e()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;->onPreloadUpdateFailed()V

    .line 261
    :cond_0
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 158
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->j:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;->isOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V

    .line 173
    :goto_0
    return-void

    .line 164
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a()V

    goto :goto_0

    .line 166
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V

    goto :goto_0

    .line 171
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a()V

    goto :goto_0
.end method

.method public executeWithOutTimeCheck()V
    .locals 1

    .prologue
    .line 199
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->DIRECT_EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)V

    .line 202
    :cond_0
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 66
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/l;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 69
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;->onPreloadUpdateSuccess()V

    goto :goto_0

    .line 72
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/j;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getEmergencyDownloadList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 75
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->e()V

    goto :goto_0

    .line 78
    :pswitch_3
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->d:Landroid/content/Context;

    invoke-direct {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->getRegisteredVersionCode()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->d:Landroid/content/Context;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->registerEmergencyUpdateNotification(I)V

    goto :goto_0

    .line 81
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->d:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;->createPreloadAutoUpdateChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;->check()V

    goto :goto_0

    .line 84
    :pswitch_5
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->i:Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->i:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 87
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->i:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->i:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->i:Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 78
    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager$IPreloadUpdateManagerObserver;

    .line 52
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    .line 57
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;)V

    return-void
.end method
