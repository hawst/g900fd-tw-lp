.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

.field c:Ljava/util/ArrayList;

.field d:Landroid/content/ServiceConnection;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->c:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->d:Landroid/content/ServiceConnection;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->e:Landroid/content/Context;

    .line 30
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;

    .line 63
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;->onServiceBinded()V

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;)V
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/b;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->c:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/b;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->bindService(Landroid/os/IBinder;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->d:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V
    .locals 4

    .prologue
    .line 33
    :goto_0
    const-string v0, "ServiceBinder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exit:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/b;->a:[I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->ordinal()I

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    .line 35
    const-string v0, "ServiceBinder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "entry:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/b;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 36
    :cond_0
    :goto_1
    :pswitch_0
    return-void

    .line 35
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b()V

    sget-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    goto :goto_0

    :pswitch_2
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->d:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    sget-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->c:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    sget-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->c:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a()V

    goto :goto_1

    :pswitch_4
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;

    .line 72
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;->onServiceBindFailed()V

    goto :goto_0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 75
    return-void
.end method


# virtual methods
.method protected abstract bindService(Landroid/os/IBinder;)V
.end method

.method public checkServiceConnection(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;)V
    .locals 3

    .prologue
    .line 176
    const-string v0, "ServiceBinder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "checkService:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/b;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 195
    :goto_0
    :pswitch_0
    return-void

    .line 181
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    .line 184
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    .line 189
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a()V

    goto :goto_0

    .line 192
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b()V

    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public release()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 199
    const-string v1, "ServiceBinder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "release:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/b;->a:[I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 213
    :goto_0
    :pswitch_0
    return v0

    .line 203
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    .line 208
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 210
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;->e:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/c;)V

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
