.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method private creatInfo(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/update/d;
    .locals 3

    .prologue
    .line 82
    :try_start_0
    const-string v0, "\\|\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 83
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;)V

    .line 84
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->c:Ljava/lang/String;

    .line 85
    const/4 v2, 0x1

    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->a:Ljava/lang/String;

    .line 86
    const/4 v2, 0x2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->b:Ljava/lang/String;

    .line 88
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->d:Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private createInfoListFromGearApi(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 60
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 62
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->getAPI()Lcom/samsung/android/aidl/ICheckAppInstallState;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/aidl/ICheckAppInstallState;->getInstalledWGTPackageInfo()Ljava/util/List;

    move-result-object v0

    .line 64
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->creatInfo(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/update/d;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 77
    :cond_1
    :goto_1
    return-object v1

    :catch_1
    move-exception v0

    goto :goto_1

    .line 76
    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;
    .locals 3

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;-><init>(Landroid/content/Context;)V

    .line 17
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPrePostPackageInfo()Ljava/util/Vector;

    move-result-object v1

    .line 19
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;-><init>()V

    .line 21
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadCount:I

    .line 22
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadCount:I

    .line 23
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    .line 24
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    .line 25
    return-object v2
.end method

.method public createWithWGT(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;
    .locals 5

    .prologue
    .line 30
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->create(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    move-result-object v1

    .line 32
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->isReady()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->createInfoListFromGearApi(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)Ljava/util/ArrayList;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;

    .line 36
    iget-boolean v3, v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->d:Z

    if-eqz v3, :cond_1

    .line 38
    iget v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadCount:I

    .line 39
    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 41
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    .line 43
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    goto :goto_0

    .line 47
    :cond_1
    iget v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadCount:I

    .line 48
    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 50
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    .line 52
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/d;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    goto/16 :goto_0

    .line 56
    :cond_3
    return-object v1
.end method
