.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/TRequestBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;
.source "ProGuard"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V

    .line 27
    return-void
.end method

.method private makeRequest(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 6

    .prologue
    .line 41
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const-string v1, "hcjo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "foundxml:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;-><init>()V

    .line 45
    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;

    invoke-direct {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;-><init>(Ljava/io/File;)V

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFileFake;

    move-object v1, p3

    move-object v3, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFileFake;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/file/TextFileReader;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;)V

    .line 48
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TRequestBuilder;->mErrorPreprocessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 53
    :goto_0
    return-object v0

    .line 52
    :cond_0
    const-string v0, "hcjo"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "didn\'t find xml:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-super {p0, p3, p4, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 31
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 32
    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 35
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/TRequestBuilder;->makeRequest(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method
