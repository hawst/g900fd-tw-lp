.class public Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

.field private mCurExecuteCount:I

.field private mProductId:Ljava/lang/String;

.field private mPurchaseCouponView:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;

.field private mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCurExecuteCount:I

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mProductId:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseCouponView:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 41
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mProductId:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseCouponView:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCurExecuteCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->checkEnable()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;Z)V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->onFinalResult(Z)V

    return-void
.end method

.method private checkEnable()V
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSellingPrice()D

    move-result-wide v2

    .line 94
    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->checkAvailablity(D)V

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getNormalPrice()D

    move-result-wide v2

    .line 100
    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v2

    .line 101
    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    .line 103
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPaymentMinPrice()D

    move-result-wide v4

    .line 105
    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 107
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->enable(Z)V

    goto :goto_0

    .line 111
    :cond_1
    return-void
.end method

.method private getResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;)V

    return-object v0
.end method


# virtual methods
.method public getCouponContainer()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCurExecuteCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCurExecuteCount:I

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseCouponView:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;->getCurrentPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mProductId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getCouponRequestCondition(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mCouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->getResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->requestPurchaseCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 81
    return-void
.end method

.method public requestPurchaseCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->purchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 69
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 70
    return-void
.end method

.method public setPurchaseCouponView(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd;->mPurchaseCouponView:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/PurchaseCouponReqCmd$IPurchaseCouponView;

    .line 50
    return-void
.end method
