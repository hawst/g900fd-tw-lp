.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getOrderID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getPaymentID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isLastReq()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;->a:Z

    return v0
.end method
