.class public Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;
.super Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;
.source "ProGuard"


# static fields
.field public static final CAMERA_FIRMWARE_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.camerafirmware_"

.field static final CONTENT_TYPE_APPLICATION:Ljava/lang/String; = "1"

.field private static final URI_UNA1:Ljava/lang/String;

.field private static final URI_UNA2:Ljava/lang/String;


# instance fields
.field private mDB:Landroid/net/Uri;

.field private final newPkgName:Ljava/lang/String;

.field private final oldPkgName:Ljava/lang/String;

.field private unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "68,74,73,79,6a,73,79,3f,34,34,68,74,72,33,78,6a,68,33,66,78,78,74,77,6a,33,7a,73,66,78,6a,77,7b,6e,68,6a,34,66,75,75,79,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->URI_UNA1:Ljava/lang/String;

    .line 26
    const-string v0, "68,74,73,79,6a,73,79,3f,34,34,68,74,72,33,78,6a,68,33,66,78,79,74,77,6a,33,7a,73,66,78,6a,77,7b,6e,68,6a,37,34,66,75,75,78,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->URI_UNA2:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mDB:Landroid/net/Uri;

    .line 23
    const-string v0, "com.sec.android.app.samsungapps.una"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->oldPkgName:Ljava/lang/String;

    .line 24
    const-string v0, "com.sec.android.app.samsungapps.una2"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->newPkgName:Ljava/lang/String;

    .line 34
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->checkInstalled()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mDB:Landroid/net/Uri;

    .line 23
    const-string v0, "com.sec.android.app.samsungapps.una"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->oldPkgName:Ljava/lang/String;

    .line 24
    const-string v0, "com.sec.android.app.samsungapps.una2"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->newPkgName:Ljava/lang/String;

    .line 34
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->checkInstalled()V

    .line 45
    return-void
.end method

.method private checkInstalled()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "com.sec.android.app.samsungapps.una2"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->c:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 50
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->URI_UNA2:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mDB:Landroid/net/Uri;

    .line 60
    :goto_0
    return-void

    .line 54
    :cond_0
    const-string v0, "com.sec.android.app.samsungapps.una"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 56
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->URI_UNA1:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mDB:Landroid/net/Uri;

    goto :goto_0

    .line 59
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    goto :goto_0
.end method

.method private getAltStr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 215
    if-nez p1, :cond_0

    .line 218
    :goto_0
    return-object p2

    :cond_0
    move-object p2, p1

    goto :goto_0
.end method

.method private getCameraFirmware()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 97
    const/4 v0, 0x1

    :try_start_0
    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "com.sec.android.app.camerafirmware_%"

    aput-object v1, v4, v0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "AppID like ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 102
    if-nez v0, :cond_0

    move-object v0, v6

    .line 118
    :goto_0
    return-object v0

    .line 105
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 107
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    .line 108
    goto :goto_0

    .line 111
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 118
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_0
.end method

.method private getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mDB:Landroid/net/Uri;

    return-object v0
.end method

.method private isPreloadedApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 1

    .prologue
    .line 266
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dumpDBList()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mDB:Landroid/net/Uri;

    const-string v5, "AppID"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 225
    if-nez v0, :cond_0

    .line 262
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 231
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 235
    :cond_1
    const-string v1, "AppID:%s\nVersion:%s\nContentType:%s\nLoadType:%s\n"

    .line 237
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 239
    const-string v2, "AppID"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 240
    const-string v3, "Version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 241
    const-string v4, "contentType"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 242
    const-string v5, "loadType"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 246
    :cond_2
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 247
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 248
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 249
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 251
    const-string v10, "NULL"

    invoke-direct {p0, v6, v10}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getAltStr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 252
    const-string v10, "NULL"

    invoke-direct {p0, v7, v10}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getAltStr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 253
    const-string v10, "NULL"

    invoke-direct {p0, v8, v10}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getAltStr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 254
    const-string v10, "NULL"

    invoke-direct {p0, v9, v10}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getAltStr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 256
    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v6, 0x1

    aput-object v7, v10, v6

    const/4 v6, 0x2

    aput-object v8, v10, v6

    const/4 v6, 0x3

    aput-object v9, v10, v6

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 257
    const-string v6, " "

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 259
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 261
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getHiddenAppInfo(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 173
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;-><init>(Landroid/content/Context;)V

    .line 175
    const-string v1, ""

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getThisPackageName()Ljava/lang/String;

    move-result-object v3

    .line 178
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getMyPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_4

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eq v4, v6, :cond_0

    .line 184
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "@"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 187
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->loadODCVersionCode()I

    move-result v4

    .line 188
    if-ltz v4, :cond_3

    .line 189
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "@CODE"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "@1"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_2

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v1

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eq v4, v6, :cond_1

    .line 204
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "@"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 207
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "||"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "@1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    :cond_2
    return-object v0

    .line 192
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "@"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->loadODCVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "@1"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public getPackageInfo()Landroid/content/pm/PackageInfo;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method

.method public getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 123
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 125
    :cond_0
    const-string v1, "invalid package name\r\n"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 163
    :cond_1
    :goto_0
    return-object v0

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 136
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    goto :goto_0

    .line 143
    :catch_0
    move-exception v1

    const-string v1, "com.sec.android.app.camerafirmware_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 145
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getCameraFirmware()Landroid/database/Cursor;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_1

    .line 148
    const-string v0, "AppID"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 152
    const-string v2, "Version"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 153
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 154
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 156
    new-instance v0, Landroid/content/pm/PackageInfo;

    invoke-direct {v0}, Landroid/content/pm/PackageInfo;-><init>()V

    .line 157
    iput-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 158
    iput-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 160
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->c:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    if-ne v0, v1, :cond_0

    .line 76
    const-string v0, "com.sec.android.app.samsungapps.una2"

    .line 82
    :goto_0
    return-object v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    if-ne v0, v1, :cond_1

    .line 80
    const-string v0, "com.sec.android.app.samsungapps.una"

    goto :goto_0

    .line 82
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getPrePostPackageInfo()Ljava/util/Vector;
    .locals 12

    .prologue
    .line 278
    const-string v3, ""

    .line 279
    const-string v2, ""

    .line 280
    const/4 v1, 0x0

    .line 281
    const/4 v0, 0x0

    .line 283
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 285
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v5, 0x2000

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v4

    .line 287
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v4, v3

    move v3, v1

    move-object v1, v2

    move v2, v0

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 293
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v9, 0x2000

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    move-object v6, v5

    .line 300
    :goto_1
    if-eqz v6, :cond_0

    .line 302
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v9}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v5

    .line 306
    const/4 v9, 0x2

    if-ne v5, v9, :cond_2

    .line 308
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "-"

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 315
    :goto_2
    iget v9, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 316
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 318
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->isPreloadedApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "||"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 325
    :cond_1
    add-int/lit8 v0, v3, 0x1

    .line 326
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%s@%s@%s@%s"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v6, v10, v11

    const/4 v6, 0x1

    aput-object v5, v10, v6

    const/4 v5, 0x2

    aput-object v9, v10, v5

    const/4 v5, 0x3

    const-string v6, "0"

    aput-object v6, v10, v5

    invoke-static {v4, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v0

    goto/16 :goto_0

    .line 297
    :catch_0
    move-exception v5

    const/4 v5, 0x0

    move-object v6, v5

    goto :goto_1

    .line 312
    :cond_2
    iget-object v5, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_2

    .line 330
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_6

    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "||"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 335
    :goto_3
    add-int/lit8 v1, v2, 0x1

    .line 336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%s@%s@%s@%s"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v6, v10, v11

    const/4 v6, 0x1

    aput-object v5, v10, v6

    const/4 v5, 0x2

    aput-object v9, v10, v5

    const/4 v5, 0x3

    const-string v6, "1"

    aput-object v6, v10, v5

    invoke-static {v2, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    .line 338
    goto/16 :goto_0

    .line 341
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->getCameraFirmware()Landroid/database/Cursor;

    move-result-object v5

    .line 342
    if-eqz v5, :cond_5

    .line 346
    :try_start_1
    const-string v0, "AppID"

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 347
    const-string v6, "Version"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 349
    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 350
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 352
    add-int/lit8 v0, v3, 0x1

    .line 353
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "||%s@%s@%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v8, v10, v11

    const/4 v8, 0x1

    aput-object v6, v10, v8

    const/4 v6, 0x2

    const-string v8, "0"

    aput-object v8, v10, v6

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    .line 360
    :goto_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 363
    :goto_5
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 364
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 365
    invoke-virtual {v7, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-virtual {v7, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 368
    return-object v7

    :catch_1
    move-exception v0

    move v0, v3

    goto :goto_4

    :catch_2
    move-exception v3

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_5

    :cond_6
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public isInstalled()Z
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;->unainstalled:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
