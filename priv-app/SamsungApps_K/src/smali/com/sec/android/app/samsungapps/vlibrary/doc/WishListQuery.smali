.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.source "ProGuard"


# instance fields
.field WishItemsTobeDeleted:Ljava/util/ArrayList;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->wishList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->doDeletion(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V

    return-void
.end method

.method private doDeletion(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 72
    :cond_0
    if-eqz p1, :cond_1

    .line 73
    invoke-interface {p1, v1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;->onDeletionResult(ZZ)V

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->clear()V

    .line 76
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->requestDataGet(Landroid/content/Context;)V

    .line 77
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    .line 121
    :goto_0
    return-void

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 83
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/aj;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 118
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 120
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelDeletion()V
    .locals 3

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 36
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 37
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    goto :goto_0

    .line 40
    :cond_0
    return-void
.end method

.method protected createRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->wishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 26
    return-object v0
.end method

.method public excuteDeletion(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_2

    .line 49
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 51
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 52
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->WishItemsTobeDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 59
    if-eqz p1, :cond_2

    .line 60
    invoke-interface {p1, v3, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;->onDeletionResult(ZZ)V

    .line 66
    :cond_2
    :goto_1
    return-void

    .line 64
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->doDeletion(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery$WishItemDeleteObserver;Landroid/content/Context;)V

    goto :goto_1
.end method
