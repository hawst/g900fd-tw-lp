.class public Lcom/sec/android/app/billing/helper/ThumbnailImageURL;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public mdpi:Ljava/lang/String;

.field public xhdpi:Ljava/lang/String;

.field public xxhdpi:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/billing/helper/l;

    invoke-direct {v0}, Lcom/sec/android/app/billing/helper/l;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/helper/ThumbnailImageURL;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;

    invoke-direct {v0}, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;-><init>()V

    .line 29
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->mdpi:Ljava/lang/String;

    .line 30
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->xhdpi:Ljava/lang/String;

    .line 31
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->xxhdpi:Ljava/lang/String;

    .line 33
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->mdpi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->xhdpi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->xxhdpi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24
    return-void
.end method
