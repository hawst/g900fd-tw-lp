.class final Lcom/sec/android/app/samsungapps/ir;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field a:I

.field b:Landroid/content/Intent;

.field final synthetic c:Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;

.field final synthetic d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;)V
    .locals 3

    .prologue
    .line 1080
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ir;->c:Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1081
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->f(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/ir;->a:I

    .line 1083
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->b:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return-void

    .line 1092
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    new-instance v1, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 1093
    new-instance v9, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1094
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 1096
    if-nez p2, :cond_3

    .line 1098
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SETTINGS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_SETTING_ON_OFF:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    sget-object v6, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->ON:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    .line 1106
    :cond_2
    :goto_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/ir;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1108
    :pswitch_0
    if-nez p2, :cond_4

    .line 1113
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    goto :goto_0

    .line 1100
    :cond_3
    if-ne p2, v11, :cond_2

    .line 1102
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SETTINGS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_SETTING_ON_OFF:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    sget-object v6, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->OFF:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    goto :goto_1

    .line 1116
    :cond_4
    if-ne p2, v11, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/LoadingDialog;->setCancelable(Z)V

    .line 1126
    const-string v0, "notify_store_activities_setting"

    invoke-virtual {v9, v0, v10}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->c:Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;->notifyDataSetChanged()V

    .line 1128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->b:Landroid/content/Intent;

    const-string v1, "extra_command"

    const-string v2, "cmd_deregistration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ir;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 1135
    :pswitch_1
    if-nez p2, :cond_5

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 1141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/LoadingDialog;->setCancelable(Z)V

    .line 1145
    const-string v0, "notify_store_activities_setting"

    invoke-virtual {v9, v0, v10}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->c:Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;->notifyDataSetChanged()V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->b:Landroid/content/Intent;

    const-string v1, "extra_command"

    const-string v2, "cmd_registration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ir;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ir;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 1151
    :cond_5
    if-ne p2, v11, :cond_0

    .line 1156
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    goto/16 :goto_0

    .line 1106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
