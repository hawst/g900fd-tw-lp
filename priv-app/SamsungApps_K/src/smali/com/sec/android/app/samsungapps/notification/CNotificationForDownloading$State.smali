.class public final enum Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOWNLOADPROGRESS:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

.field public static final enum INSTALLED:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

.field public static final enum INSTALLING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

.field public static final enum WAITING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->IDLE:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    const-string v1, "DOWNLOADPROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->DOWNLOADPROGRESS:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->WAITING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    const-string v1, "INSTALLING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->INSTALLING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    const-string v1, "INSTALLED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->INSTALLED:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->IDLE:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->DOWNLOADPROGRESS:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->WAITING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->INSTALLING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->INSTALLED:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->a:[Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->a:[Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    return-object v0
.end method
