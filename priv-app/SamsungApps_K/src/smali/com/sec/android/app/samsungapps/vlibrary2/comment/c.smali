.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 127
    if-eqz p2, :cond_0

    .line 129
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyCommentChanged(Ljava/lang/String;)V

    .line 149
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;->onCommentRegisterResult(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;Z)V

    .line 151
    return-void

    .line 134
    :cond_0
    :try_start_0
    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 140
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->setDuplicatedMyReview(Z)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    const-string v0, ">>>> addComment :: NumberFormatException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->getProhibitWords()[Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->setProhibitWords([Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 134
    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
