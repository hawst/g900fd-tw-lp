.class public Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field _Array:Ljava/util/ArrayList;

.field private _StartPos:I

.field private _bCompleted:Z

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_Array:Ljava/util/ArrayList;

    .line 19
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 38
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_Array:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_Array:Ljava/util/ArrayList;

    .line 44
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_Array:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    :cond_0
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResult()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_Array:Ljava/util/ArrayList;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 23
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 63
    const-string v0, "startNum"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 64
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_StartPos:I

    .line 73
    :goto_0
    const-string v0, "totalCount"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    .line 99
    return-void

    .line 69
    :cond_0
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_StartPos:I

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentListParser;->_Array:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
