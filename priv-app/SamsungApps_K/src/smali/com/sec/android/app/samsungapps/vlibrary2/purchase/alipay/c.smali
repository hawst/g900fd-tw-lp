.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;Z)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->b:Z

    .line 83
    return-void
.end method


# virtual methods
.method public final getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public final getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->orderid:Ljava/lang/String;

    return-object v0
.end method

.method public final getPaymentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->paymentid:Ljava/lang/String;

    return-object v0
.end method

.method public final getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public final lastReq()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;->b:Z

    return v0
.end method
