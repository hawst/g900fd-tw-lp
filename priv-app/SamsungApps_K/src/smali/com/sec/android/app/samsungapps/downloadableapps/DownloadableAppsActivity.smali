.class public Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

.field private b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

.field private c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

.field private d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

.field private e:Landroid/content/Context;

.field private f:I

.field private g:Z

.field private h:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

.field private i:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

.field private j:Landroid/widget/Button;

.field private k:Lcom/sec/android/app/samsungapps/downloadableapps/i;

.field private l:Ljava/lang/String;

.field private m:J

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f:I

    .line 49
    sget-object v0, Lcom/sec/android/app/samsungapps/downloadableapps/i;->a:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->k:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->l:Ljava/lang/String;

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->m:J

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->n:Ljava/lang/String;

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->o:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/downloadableapps/i;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->k:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Lcom/sec/android/app/samsungapps/downloadableapps/i;)Lcom/sec/android/app/samsungapps/downloadableapps/i;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->k:Lcom/sec/android/app/samsungapps/downloadableapps/i;

    return-object p1
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    move-result-object v0

    if-nez v0, :cond_6

    .line 239
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->g:Z

    if-nez v0, :cond_1

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    iget v4, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f:I

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->setCommandType(I)V

    const v0, 0x7f0c0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-direct {v0, p0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b()Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setClickListener(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->loadWidget()V

    const v0, 0x7f0c0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b()Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setClickListener(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->loadWidget()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v2, :cond_7

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    move v4, v0

    :goto_1
    const v0, 0x7f0c0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0176

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    if-nez v1, :cond_3

    .line 245
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v3

    .line 239
    goto :goto_0

    :cond_3
    const v5, 0x7f0802fa

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "More from Samsung Apps"

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadableapps/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/g;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_5
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 243
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->refreshWidget()V

    goto :goto_2

    :cond_7
    move v4, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 470
    const v0, 0x7f0c0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 471
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 472
    if-eqz p1, :cond_1

    .line 473
    const v1, 0x7f08008d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listDescription:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 476
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 478
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b()Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;
    .locals 1

    .prologue
    .line 327
    new-instance v0, Lcom/sec/android/app/samsungapps/downloadableapps/f;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/f;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Z
    .locals 4

    .prologue
    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->m:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->m:J

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->h:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->j:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a()V

    return-void
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->finish()V

    return-void
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 38
    const v0, 0x7f0c0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c004d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    aput-object v5, v4, v3

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x2

    aput-object v0, v4, v5

    const/4 v0, 0x3

    aput-object v1, v4, v0

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    iget-object v4, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGImgUrl:Ljava/lang/String;

    const v0, 0x7f0c016c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getWidgetState()I

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0c016f

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->showDownloadAll(ZI)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listTitle:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listTitle:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listTitle:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getItemCount()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    :goto_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a(Z)V

    :cond_3
    return-void

    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->l:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iput-object v4, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->l:Ljava/lang/String;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->o:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_2
.end method


# virtual methods
.method public isBackPressed()Z
    .locals 1

    .prologue
    .line 462
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->g:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->g:Z

    .line 386
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 387
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 391
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 392
    const v0, 0x7f0c014c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setTabTextSize(Landroid/content/res/Configuration;)V

    .line 397
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/high16 v2, 0x1000000

    .line 77
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 84
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PRODUCTSETID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->n:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "TITLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->o:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 95
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcessWithoutAutoLogin;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcessWithoutAutoLogin;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e:Landroid/content/Context;

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buttonType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f:I

    .line 98
    const v0, 0x7f040053

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->setContentView(I)V

    .line 100
    const v0, 0x7f0c0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->j:Landroid/widget/Button;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->j:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadableapps/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/a;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    new-instance v0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadableapps/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/b;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/downloadableapps/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/c;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;-><init>(Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->h:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;

    .line 175
    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Global;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a()V

    .line 193
    :goto_1
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 195
    const v0, 0x7f0c016d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 196
    if-eqz v0, :cond_1

    .line 197
    new-instance v1, Lcom/sec/android/app/samsungapps/downloadableapps/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/e;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    :cond_1
    return-void

    .line 90
    :catch_0
    move-exception v0

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->o:Ljava/lang/String;

    .line 91
    const-string v0, "dummy"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->n:Ljava/lang/String;

    goto :goto_0

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->i:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    new-instance v1, Lcom/sec/android/app/samsungapps/downloadableapps/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/downloadableapps/d;-><init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->startThreadForInitializingData(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->clear()V

    .line 257
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->release()V

    .line 262
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->release()V

    .line 268
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    if-eqz v0, :cond_3

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->release()V

    .line 274
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    .line 277
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 278
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 283
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 285
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->d:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "buttonType"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f:I

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->f:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->setCommandType(I)V

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->b()Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;->onClickTab()V

    .line 291
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 249
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 250
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c:Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->updateWidget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 233
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 231
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public showDownloadAll(ZI)V
    .locals 2

    .prologue
    .line 400
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 401
    if-eqz v0, :cond_0

    .line 402
    if-eqz p1, :cond_1

    .line 403
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
