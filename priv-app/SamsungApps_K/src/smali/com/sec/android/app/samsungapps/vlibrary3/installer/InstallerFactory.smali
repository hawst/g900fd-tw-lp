.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract createDeltaSupportedInstaller(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.end method

.method public abstract createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.end method

.method public abstract createSigProtectedDeltaSupportedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.end method

.method public abstract createSigProtectedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.end method

.method public abstract createWGTInAPKInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.end method

.method public abstract createWGTOnlyInstaller(Landroid/content/Context;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
.end method
