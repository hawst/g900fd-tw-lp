.class public Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 34
    iput-object p9, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    .line 35
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-nez v1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->findDownloadingContent(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;)Z
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected prepareView()V
    .locals 0

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showProductType()V

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showPurchaseStatus()V

    .line 42
    return-void
.end method

.method public showPurchaseStatus()V
    .locals 7

    .prologue
    const v6, 0x7f0802f3

    const/16 v2, 0x8

    const/4 v5, 0x1

    const v4, 0x7f08015a

    const/4 v3, 0x0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_View:Landroid/view/View;

    if-nez v0, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->empty_loading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showProductName()V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showProductImg()V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showProductRating()V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showCategory()V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showProductnormalPrice()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/h;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/h;-><init>(Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->hideRating()V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->progresstext:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->makeDownloadProgressText(Ljava/lang/Long;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->progressLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->showRating()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->hasOrderID()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 155
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    const v2, 0x7f0802e5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 164
    :cond_5
    const-string v1, "2"

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->loadType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 172
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 178
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
