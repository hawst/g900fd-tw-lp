.class public Lcom/sec/android/app/samsungapps/PermissionPopupActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

.field private c:Z

.field private d:Z

.field private e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

.field private f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private h:Z

.field private i:Landroid/widget/LinearLayout;

.field private j:Z

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->d:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    .line 30
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->h:Z

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->i:Landroid/widget/LinearLayout;

    .line 136
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->j:Z

    .line 159
    iput v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->k:I

    .line 160
    iput v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->l:I

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/PermissionPopupActivity;)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    return-object v0
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->i:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 154
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->k:I

    mul-int/lit8 v0, v0, 0x41

    div-int/lit8 v0, v0, 0x64

    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    :cond_1
    return-void

    .line 154
    :cond_2
    iget v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->l:I

    mul-int/lit8 v0, v0, 0x5f

    div-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 129
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 130
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 133
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->d:Z

    .line 134
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->a(Landroid/content/res/Configuration;)V

    .line 149
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    .line 36
    instance-of v1, v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    if-eqz v1, :cond_0

    .line 37
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    if-nez v0, :cond_1

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->finish()V

    .line 61
    :goto_0
    return-void

    .line 46
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->getPermissionMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->setPermissionInfoList(Ljava/util/HashMap;)V

    .line 51
    const v0, 0x7f040098

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_2

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->k:I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->l:I

    .line 55
    :cond_2
    :goto_1
    const v0, 0x7f0c0148

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->i:Landroid/widget/LinearLayout;

    .line 56
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->a(Landroid/content/res/Configuration;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->isForUpdate()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->h:Z

    .line 59
    const v0, 0x7f0c01fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/sec/android/app/samsungapps/ga;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/ga;-><init>(Lcom/sec/android/app/samsungapps/PermissionPopupActivity;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_3
    const v0, 0x7f0c021b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->setPermissionData(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->loadWidget()V

    const v0, 0x7f0c021c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    new-instance v1, Lcom/sec/android/app/samsungapps/gb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gb;-><init>(Lcom/sec/android/app/samsungapps/PermissionPopupActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->setObserver(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget$IPermissionPopupButtonWidgetObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->h:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->setForAllUpdate(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->loadWidget()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->invokeCompleted()V

    goto/16 :goto_0

    .line 54
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->l:I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->k:I

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->release()V

    .line 110
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;->release()V

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupButtonWidget;

    .line 118
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->c:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->j:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->d:Z

    if-nez v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 124
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    .line 125
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionPopupActivity;->j:Z

    .line 141
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 142
    return-void
.end method
