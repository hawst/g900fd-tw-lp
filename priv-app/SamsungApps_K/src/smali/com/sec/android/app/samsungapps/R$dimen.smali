.class public final Lcom/sec/android/app/samsungapps/R$dimen;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final action_bar_height_land:I = 0x7f070044

.field public static final action_bar_height_port:I = 0x7f070043

.field public static final activity_horizontal_margin:I = 0x7f070000

.field public static final activity_vertical_margin:I = 0x7f070001

.field public static final banner_bottom_line_height:I = 0x7f070019

.field public static final banner_button_width:I = 0x7f07000c

.field public static final banner_height:I = 0x7f07000a

.field public static final banner_hover_offset:I = 0x7f07000e

.field public static final banner_inner_item_height:I = 0x7f07000d

.field public static final banner_total_height:I = 0x7f07000b

.field public static final banner_width:I = 0x7f070009

.field public static final bigbanner_top_margin:I = 0x7f070017

.field public static final button_padding_left:I = 0x7f070037

.field public static final button_text_size:I = 0x7f070036

.field public static final chart_banner_total_height:I = 0x7f070006

.field public static final chart_banner_width:I = 0x7f070005

.field public static final chartbanner_first_item_margin_left:I = 0x7f07001d

.field public static final chartbanner_first_item_margin_right:I = 0x7f07001e

.field public static final chartbanner_first_item_width:I = 0x7f07001c

.field public static final chartbanner_item_h_spacing:I = 0x7f07001f

.field public static final chartbanner_item_margin_right:I = 0x7f070021

.field public static final chartbanner_item_num_height:I = 0x7f070022

.field public static final chartbanner_item_v_spacing:I = 0x7f070020

.field public static final chartbanner_title_height:I = 0x7f070023

.field public static final chartbanner_title_margin_top:I = 0x7f070024

.field public static final chartbanner_total_width_renewal:I = 0x7f07001b

.field public static final content_detail_button_text_size:I = 0x7f070034

.field public static final content_detail_sale_text_size:I = 0x7f070035

.field public static final content_detail_widget_bottom_margin:I = 0x7f070032

.field public static final content_detail_widget_gab_layout_height:I = 0x7f07002d

.field public static final content_detail_widget_large_title_layout_height:I = 0x7f07002b

.field public static final content_detail_widget_left_right_margin:I = 0x7f070030

.field public static final content_detail_widget_list_item_left_right_margin:I = 0x7f070033

.field public static final content_detail_widget_more_btn_layout_height:I = 0x7f07002e

.field public static final content_detail_widget_more_btn_layout_width:I = 0x7f07002f

.field public static final content_detail_widget_normal_title_layout_height:I = 0x7f07002c

.field public static final content_detail_widget_screenshot_image_long:I = 0x7f070053

.field public static final content_detail_widget_screenshot_image_long_frame:I = 0x7f070051

.field public static final content_detail_widget_screenshot_image_short:I = 0x7f070054

.field public static final content_detail_widget_screenshot_image_short_frame:I = 0x7f070052

.field public static final content_detail_widget_screenshot_youtube_height:I = 0x7f070058

.field public static final content_detail_widget_screenshot_youtube_height_frame:I = 0x7f070056

.field public static final content_detail_widget_screenshot_youtube_width:I = 0x7f070057

.field public static final content_detail_widget_screenshot_youtube_width_frame:I = 0x7f070055

.field public static final content_detail_widget_top_margin:I = 0x7f070031

.field public static final curtain_height:I = 0x7f070014

.field public static final curtain_item_height:I = 0x7f070016

.field public static final curtain_item_width:I = 0x7f070015

.field public static final curtain_one_item_width:I = 0x7f070012

.field public static final curtain_one_width:I = 0x7f070011

.field public static final curtain_width:I = 0x7f070013

.field public static final custom_action_item_button_width:I = 0x7f070047

.field public static final four_px_line_divider:I = 0x7f070010

.field public static final horizontal_contentList_margin:I = 0x7f070026

.field public static final horizontal_content_spacing:I = 0x7f070028

.field public static final horizontal_content_width:I = 0x7f070027

.field public static final horizontal_item_size:I = 0x7f070048

.field public static final hover_bubble_popup_height:I = 0x7f07001a

.field public static final left_right_margin:I = 0x7f07002a

.field public static final main_banner_height:I = 0x7f070008

.field public static final main_banner_width:I = 0x7f070007

.field public static final max_button_width:I = 0x7f070038

.field public static final option_menu_for_actionbar_width:I = 0x7f07003c

.field public static final option_menu_item_height:I = 0x7f07003f

.field public static final option_menu_item_height_original:I = 0x7f07003e

.field public static final option_menu_item_height_textsize:I = 0x7f070040

.field public static final option_menu_max_height:I = 0x7f07003d

.field public static final padding_large:I = 0x7f070004

.field public static final padding_medium:I = 0x7f070003

.field public static final padding_of_giftcardnoti:I = 0x7f07003a

.field public static final padding_of_option_menu:I = 0x7f070039

.field public static final padding_small:I = 0x7f070002

.field public static final pager_tab_height:I = 0x7f070018

.field public static final popupmenu_add_width:I = 0x7f070041

.field public static final popupmenu_minus_offset_anchor:I = 0x7f070042

.field public static final search_edit_margin_right:I = 0x7f07003b

.field public static final small_banner_gridview_horizontal_spacing:I = 0x7f07004c

.field public static final small_banner_gridview_padding_bottom:I = 0x7f07004e

.field public static final small_banner_gridview_padding_left:I = 0x7f07004f

.field public static final small_banner_gridview_padding_top:I = 0x7f07004d

.field public static final small_banner_gridview_spacing:I = 0x7f07004b

.field public static final small_banner_height:I = 0x7f07004a

.field public static final small_banner_imageview_padding:I = 0x7f070050

.field public static final small_banner_width:I = 0x7f070049

.field public static final toastpopup_center_margin:I = 0x7f070046

.field public static final toastpopup_left_right_margin:I = 0x7f070045

.field public static final top_bottom_margin:I = 0x7f070029

.field public static final two_px_line_divider:I = 0x7f07000f

.field public static final write_review_min_height:I = 0x7f070025


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
