.class final Lcom/sec/android/app/samsungapps/eq;
.super Landroid/os/AsyncTask;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Ljava/lang/String;J)V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 412
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/eq;->b:Z

    .line 406
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->c:Ljava/lang/String;

    .line 407
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->d:Ljava/lang/String;

    .line 408
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    .line 409
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    .line 413
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/eq;->c:Ljava/lang/String;

    .line 414
    iput-wide p3, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    .line 415
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->d:Ljava/lang/String;

    .line 416
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Ljava/lang/String;JB)V
    .locals 0

    .prologue
    .line 403
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/eq;-><init>(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Ljava/lang/String;J)V

    return-void
.end method

.method private b()Ljava/io/File;
    .locals 2

    .prologue
    .line 420
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/sdcard/samsungapps/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/eq;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 421
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 422
    return-object v1
.end method

.method private varargs c()Ljava/lang/Long;
    .locals 13

    .prologue
    const-wide/16 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 428
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/eq;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    :try_start_1
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 446
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 447
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 449
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 450
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->b()Ljava/io/File;

    move-result-object v1

    .line 451
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-ne v2, v10, :cond_2

    .line 453
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 466
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 467
    if-nez v2, :cond_4

    .line 523
    :goto_1
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    cmp-long v0, v0, v11

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 531
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 532
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->b()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->commonStartActivity(Landroid/content/Intent;)V

    .line 536
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    .line 538
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_2
    return-object v0

    .line 437
    :catch_0
    move-exception v0

    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::MalformedURLException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 438
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    .line 455
    :cond_2
    :try_start_2
    new-instance v2, Ljava/io/File;

    const-string v3, "/sdcard/samsungapps"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 456
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 457
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 521
    :catch_1
    move-exception v0

    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::IOException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 522
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/eq;->cancel(Z)Z

    goto :goto_1

    .line 459
    :cond_3
    :try_start_3
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 460
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 461
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    goto :goto_0

    .line 472
    :cond_4
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 476
    :cond_5
    :goto_3
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/eq;->b:Z

    if-ne v1, v10, :cond_7

    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/eq;->b:Z

    .line 517
    :cond_6
    :goto_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 484
    :cond_7
    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v1

    .line 493
    const/4 v3, -0x1

    if-eq v1, v3, :cond_6

    .line 495
    :try_start_5
    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/eq;->f:J
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 501
    :try_start_6
    invoke-virtual {v2, v1}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 510
    :try_start_7
    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    iget-wide v5, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    const-wide/16 v7, 0x64

    div-long/2addr v5, v7

    rem-long/2addr v3, v5

    cmp-long v1, v3, v11

    if-eqz v1, :cond_8

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    iget-wide v5, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_5

    .line 513
    :cond_8
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/eq;->publishProgress([Ljava/lang/Object;)V

    goto :goto_3

    .line 488
    :catch_2
    move-exception v0

    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::inputStream occurs IOException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 489
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/eq;->cancel(Z)Z

    goto :goto_4

    .line 505
    :catch_3
    move-exception v0

    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::outputStream occurs IOException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 506
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/eq;->cancel(Z)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_4
.end method

.method private d()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const v1, 0x7f0c0249

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 617
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const v2, 0x7f0c024b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 618
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const v3, 0x7f0c024a

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 619
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const v4, 0x7f0c024c

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 620
    if-eqz v1, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 622
    :cond_0
    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::progressUpdate"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 661
    :goto_0
    return-void

    .line 626
    :cond_1
    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 628
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_0

    .line 633
    :cond_2
    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    div-long/2addr v4, v6

    long-to-int v5, v4

    .line 635
    iget-wide v6, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 636
    iget-wide v6, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 637
    const-string v7, "0.00"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v10, :cond_3

    .line 639
    const-string v7, "%sKB"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v4, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 642
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/samsungapps/eq;->d:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    const-string v7, "%s / %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v4, v8, v9

    aput-object v6, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 644
    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 646
    const/16 v4, 0x64

    if-ne v5, v4, :cond_4

    .line 648
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 649
    const-string v2, ""

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    invoke-virtual {v1, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 652
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_0

    .line 657
    :cond_4
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/webkit/WebView;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 543
    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::forceCancel::"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 545
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->c:Ljava/lang/String;

    .line 546
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->d:Ljava/lang/String;

    .line 547
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    .line 548
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/eq;->b:Z

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Lcom/sec/android/app/samsungapps/eq;)Lcom/sec/android/app/samsungapps/eq;

    .line 552
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->d()V

    .line 553
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 403
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->c()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 558
    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::onCancelled::"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 560
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 562
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->c:Ljava/lang/String;

    .line 563
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->d:Ljava/lang/String;

    .line 564
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    .line 565
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Lcom/sec/android/app/samsungapps/eq;)Lcom/sec/android/app/samsungapps/eq;

    .line 568
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->d()V

    .line 569
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 403
    check-cast p1, Ljava/lang/Long;

    const-string v0, "InicisMobilePurchaseView::FileDownloadTask::onPostExecute::Download done"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/eq;->a:Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Lcom/sec/android/app/samsungapps/eq;)Lcom/sec/android/app/samsungapps/eq;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 4

    .prologue
    .line 584
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->b()Ljava/io/File;

    move-result-object v0

    .line 585
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 587
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 590
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 592
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/eq;->e:J

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    .line 599
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->d()V

    .line 601
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 602
    return-void

    .line 596
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/eq;->f:J

    goto :goto_0
.end method

.method protected final synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 403
    check-cast p1, [Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/eq;->d()V

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    return-void
.end method
