.class public Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;
.super Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected getPreferenceDefaultValue()I
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/NotifyStoreActivitiesArrayAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 28
    const-string v1, "notify_store_activities_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 32
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 37
    return v0
.end method
