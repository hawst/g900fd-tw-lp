.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IGetGiftCardListData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;

.field private _Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field private _Status:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

.field list:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->list:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_IGetGiftCardListData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_Status:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_Status:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_IGetGiftCardListData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_Status:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->list:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->giftCardList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 29
    return-void
.end method
