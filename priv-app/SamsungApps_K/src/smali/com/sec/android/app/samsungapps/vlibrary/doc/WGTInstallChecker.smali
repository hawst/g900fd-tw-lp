.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;
.source "ProGuard"


# instance fields
.field private mGearAPI:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;-><init>(Landroid/content/Context;)V

    .line 12
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->mGearAPI:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    .line 13
    return-void
.end method

.method private getContentVersionCode(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)J
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVersionCode()Ljava/lang/String;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 80
    :cond_0
    const-wide/16 v0, -0x1

    .line 84
    :goto_0
    return-wide v0

    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->getVersionCode(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private getVersionCode(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 68
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 71
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private getWGTInstallVersion(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 32
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->mGearAPI:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->mGearAPI:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->getAPI()Lcom/samsung/android/aidl/ICheckAppInstallState;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/aidl/ICheckAppInstallState;->checkInstalledWGTVersion(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 45
    :cond_0
    :goto_0
    return-object v0

    .line 37
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getWGTVersion(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)J
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->getWGTInstallVersion(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Ljava/lang/String;

    move-result-object v0

    .line 59
    if-nez v0, :cond_0

    .line 60
    const-wide/16 v0, -0x1

    .line 62
    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->getVersionCode(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private isPurchased(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 126
    instance-of v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWGTInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->getWGTInstallVersion(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private preconditionForUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 135
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->isPurchased(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 139
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "0"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "3"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->isWGTInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    .line 26
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    goto :goto_0
.end method

.method public isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x0

    .line 90
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTOnly()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->getWGTVersion(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)J

    move-result-wide v1

    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->getContentVersionCode(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)J

    move-result-wide v3

    .line 93
    cmp-long v5, v1, v6

    if-eqz v5, :cond_0

    cmp-long v5, v3, v6

    if-nez v5, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 97
    const/4 v0, 0x1

    goto :goto_0

    .line 102
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    goto :goto_0
.end method

.method public isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->preconditionForUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    .line 121
    :goto_0
    return v0

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 121
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    goto :goto_0
.end method
