.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static CountryNameMCCTable:[[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    const/16 v0, 0xec

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "412"

    aput-object v2, v1, v4

    const-string v2, "AF"

    aput-object v2, v1, v5

    const-string v2, "Afghanistan"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "276"

    aput-object v2, v1, v4

    const-string v2, "AL"

    aput-object v2, v1, v5

    const-string v2, "Albania"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "603"

    aput-object v2, v1, v4

    const-string v2, "DZ"

    aput-object v2, v1, v5

    const-string v2, "Algeria"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "544"

    aput-object v2, v1, v4

    const-string v2, "AS"

    aput-object v2, v1, v5

    const-string v2, "American Samoa (US)"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "213"

    aput-object v3, v2, v4

    const-string v3, "AD"

    aput-object v3, v2, v5

    const-string v3, "Andorra"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "631"

    aput-object v3, v2, v4

    const-string v3, "AO"

    aput-object v3, v2, v5

    const-string v3, "Angola"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "365"

    aput-object v3, v2, v4

    const-string v3, "AI"

    aput-object v3, v2, v5

    const-string v3, "Anguilla"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "344"

    aput-object v3, v2, v4

    const-string v3, "AG"

    aput-object v3, v2, v5

    const-string v3, "Antigua and Barbuda"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "722"

    aput-object v3, v2, v4

    const-string v3, "AR"

    aput-object v3, v2, v5

    const-string v3, "Argentine Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "283"

    aput-object v3, v2, v4

    const-string v3, "AM"

    aput-object v3, v2, v5

    const-string v3, "Armenia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "363"

    aput-object v3, v2, v4

    const-string v3, "AW"

    aput-object v3, v2, v5

    const-string v3, "Aruba (Netherlands)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "505"

    aput-object v3, v2, v4

    const-string v3, "AU"

    aput-object v3, v2, v5

    const-string v3, "Australia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "232"

    aput-object v3, v2, v4

    const-string v3, "AT"

    aput-object v3, v2, v5

    const-string v3, "Austria"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "400"

    aput-object v3, v2, v4

    const-string v3, "AZ"

    aput-object v3, v2, v5

    const-string v3, "Azerbaijani Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "364"

    aput-object v3, v2, v4

    const-string v3, "BS"

    aput-object v3, v2, v5

    const-string v3, "Bahamas"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "426"

    aput-object v3, v2, v4

    const-string v3, "BH"

    aput-object v3, v2, v5

    const-string v3, "Bahrain"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "470"

    aput-object v3, v2, v4

    const-string v3, "BD"

    aput-object v3, v2, v5

    const-string v3, "Bangladesh"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "342"

    aput-object v3, v2, v4

    const-string v3, "BB"

    aput-object v3, v2, v5

    const-string v3, "Barbados"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "257"

    aput-object v3, v2, v4

    const-string v3, "BY"

    aput-object v3, v2, v5

    const-string v3, "Belarus"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "206"

    aput-object v3, v2, v4

    const-string v3, "BE"

    aput-object v3, v2, v5

    const-string v3, "Belgium"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "702"

    aput-object v3, v2, v4

    const-string v3, "BZ"

    aput-object v3, v2, v5

    const-string v3, "Belize"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "616"

    aput-object v3, v2, v4

    const-string v3, "BJ"

    aput-object v3, v2, v5

    const-string v3, "Benin"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "350"

    aput-object v3, v2, v4

    const-string v3, "BM"

    aput-object v3, v2, v5

    const-string v3, "Bermuda (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "402"

    aput-object v3, v2, v4

    const-string v3, "BT"

    aput-object v3, v2, v5

    const-string v3, "Bhutan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "736"

    aput-object v3, v2, v4

    const-string v3, "BO"

    aput-object v3, v2, v5

    const-string v3, "Bolivia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "218"

    aput-object v3, v2, v4

    const-string v3, "BA"

    aput-object v3, v2, v5

    const-string v3, "Bosnia and Herzegovina"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "652"

    aput-object v3, v2, v4

    const-string v3, "BW"

    aput-object v3, v2, v5

    const-string v3, "Botswana"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "724"

    aput-object v3, v2, v4

    const-string v3, "BR"

    aput-object v3, v2, v5

    const-string v3, "Brazil"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "348"

    aput-object v3, v2, v4

    const-string v3, "VG"

    aput-object v3, v2, v5

    const-string v3, "British Virgin Islands (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "528"

    aput-object v3, v2, v4

    const-string v3, "BN"

    aput-object v3, v2, v5

    const-string v3, "Brunei Darussalam"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "284"

    aput-object v3, v2, v4

    const-string v3, "BG"

    aput-object v3, v2, v5

    const-string v3, "Bulgaria"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "613"

    aput-object v3, v2, v4

    const-string v3, "BF"

    aput-object v3, v2, v5

    const-string v3, "Burkina Faso"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "642"

    aput-object v3, v2, v4

    const-string v3, "BI"

    aput-object v3, v2, v5

    const-string v3, "Burundi"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "456"

    aput-object v3, v2, v4

    const-string v3, "KH"

    aput-object v3, v2, v5

    const-string v3, "Cambodia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "624"

    aput-object v3, v2, v4

    const-string v3, "CM"

    aput-object v3, v2, v5

    const-string v3, "Cameroon"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "302"

    aput-object v3, v2, v4

    const-string v3, "CA"

    aput-object v3, v2, v5

    const-string v3, "Canada"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "625"

    aput-object v3, v2, v4

    const-string v3, "CV"

    aput-object v3, v2, v5

    const-string v3, "Cape Verde"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "346"

    aput-object v3, v2, v4

    const-string v3, "KY"

    aput-object v3, v2, v5

    const-string v3, "Cayman Islands (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "623"

    aput-object v3, v2, v4

    const-string v3, "CF"

    aput-object v3, v2, v5

    const-string v3, "Central African Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "622"

    aput-object v3, v2, v4

    const-string v3, "TD"

    aput-object v3, v2, v5

    const-string v3, "Chad"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "730"

    aput-object v3, v2, v4

    const-string v3, "CL"

    aput-object v3, v2, v5

    const-string v3, "Chile"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "460"

    aput-object v3, v2, v4

    const-string v3, "CN"

    aput-object v3, v2, v5

    const-string v3, "China"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "461"

    aput-object v3, v2, v4

    const-string v3, "CN"

    aput-object v3, v2, v5

    const-string v3, "China"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "732"

    aput-object v3, v2, v4

    const-string v3, "CO"

    aput-object v3, v2, v5

    const-string v3, "Colombia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "654"

    aput-object v3, v2, v4

    const-string v3, "KM"

    aput-object v3, v2, v5

    const-string v3, "Comoros"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "629"

    aput-object v3, v2, v4

    const-string v3, "CG"

    aput-object v3, v2, v5

    const-string v3, "Republic of the Congo"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "548"

    aput-object v3, v2, v4

    const-string v3, "CK"

    aput-object v3, v2, v5

    const-string v3, "Cook Islands (NZ)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "712"

    aput-object v3, v2, v4

    const-string v3, "CR"

    aput-object v3, v2, v5

    const-string v3, "Costa Rica"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x30

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "612"

    aput-object v3, v2, v4

    const-string v3, "CI"

    aput-object v3, v2, v5

    const-string v3, "C\u00f4te d\'Ivoire"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x31

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "219"

    aput-object v3, v2, v4

    const-string v3, "HR"

    aput-object v3, v2, v5

    const-string v3, "Croatia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x32

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "368"

    aput-object v3, v2, v4

    const-string v3, "CU"

    aput-object v3, v2, v5

    const-string v3, "Cuba"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "362"

    aput-object v3, v2, v4

    const-string v3, "CW"

    aput-object v3, v2, v5

    const-string v3, "Cura\u00e7ao (Netherlands)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "280"

    aput-object v3, v2, v4

    const-string v3, "CY"

    aput-object v3, v2, v5

    const-string v3, "Cyprus"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x35

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "230"

    aput-object v3, v2, v4

    const-string v3, "CZ"

    aput-object v3, v2, v5

    const-string v3, "Czech Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x36

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "630"

    aput-object v3, v2, v4

    const-string v3, "CD"

    aput-object v3, v2, v5

    const-string v3, "Democratic Republic of the Congo"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x37

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "238"

    aput-object v3, v2, v4

    const-string v3, "DK"

    aput-object v3, v2, v5

    const-string v3, "Denmark"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x38

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "638"

    aput-object v3, v2, v4

    const-string v3, "DJ"

    aput-object v3, v2, v5

    const-string v3, "Djibouti"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x39

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "366"

    aput-object v3, v2, v4

    const-string v3, "DM"

    aput-object v3, v2, v5

    const-string v3, "Dominica"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "370"

    aput-object v3, v2, v4

    const-string v3, "DO"

    aput-object v3, v2, v5

    const-string v3, "Dominican Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "514"

    aput-object v3, v2, v4

    const-string v3, "TL"

    aput-object v3, v2, v5

    const-string v3, "East Timor"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "740"

    aput-object v3, v2, v4

    const-string v3, "EC"

    aput-object v3, v2, v5

    const-string v3, "Ecuador"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "602"

    aput-object v3, v2, v4

    const-string v3, "EG"

    aput-object v3, v2, v5

    const-string v3, "Egypt"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "706"

    aput-object v3, v2, v4

    const-string v3, "SV"

    aput-object v3, v2, v5

    const-string v3, "El Salvador"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "627"

    aput-object v3, v2, v4

    const-string v3, "GQ"

    aput-object v3, v2, v5

    const-string v3, "Equatorial Guinea"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x40

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "657"

    aput-object v3, v2, v4

    const-string v3, "ER"

    aput-object v3, v2, v5

    const-string v3, "Eritrea"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x41

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "248"

    aput-object v3, v2, v4

    const-string v3, "EE"

    aput-object v3, v2, v5

    const-string v3, "Estonia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x42

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "636"

    aput-object v3, v2, v4

    const-string v3, "ET"

    aput-object v3, v2, v5

    const-string v3, "Ethiopia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x43

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "750"

    aput-object v3, v2, v4

    const-string v3, "FK"

    aput-object v3, v2, v5

    const-string v3, "Falkland Islands (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x44

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "288"

    aput-object v3, v2, v4

    const-string v3, "FO"

    aput-object v3, v2, v5

    const-string v3, "Faroe Islands (Denmark)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x45

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "542"

    aput-object v3, v2, v4

    const-string v3, "FJ"

    aput-object v3, v2, v5

    const-string v3, "Fiji"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x46

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "244"

    aput-object v3, v2, v4

    const-string v3, "FI"

    aput-object v3, v2, v5

    const-string v3, "Finland"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x47

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "208"

    aput-object v3, v2, v4

    const-string v3, "FR"

    aput-object v3, v2, v5

    const-string v3, "France"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x48

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "742"

    aput-object v3, v2, v4

    const-string v3, "GF"

    aput-object v3, v2, v5

    const-string v3, "French Guiana (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x49

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "547"

    aput-object v3, v2, v4

    const-string v3, "PF"

    aput-object v3, v2, v5

    const-string v3, "French Polynesia (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "628"

    aput-object v3, v2, v4

    const-string v3, "GA"

    aput-object v3, v2, v5

    const-string v3, "Gabonese Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "607"

    aput-object v3, v2, v4

    const-string v3, "GM"

    aput-object v3, v2, v5

    const-string v3, "Gambia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "282"

    aput-object v3, v2, v4

    const-string v3, "GE"

    aput-object v3, v2, v5

    const-string v3, "Georgia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "262"

    aput-object v3, v2, v4

    const-string v3, "DE"

    aput-object v3, v2, v5

    const-string v3, "Germany"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "620"

    aput-object v3, v2, v4

    const-string v3, "GH"

    aput-object v3, v2, v5

    const-string v3, "Ghana"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "266"

    aput-object v3, v2, v4

    const-string v3, "GI"

    aput-object v3, v2, v5

    const-string v3, "Gibraltar (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x50

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "202"

    aput-object v3, v2, v4

    const-string v3, "GR"

    aput-object v3, v2, v5

    const-string v3, "Greece"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x51

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "290"

    aput-object v3, v2, v4

    const-string v3, "GL"

    aput-object v3, v2, v5

    const-string v3, "Greenland (Denmark)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x52

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "352"

    aput-object v3, v2, v4

    const-string v3, "GD"

    aput-object v3, v2, v5

    const-string v3, "Grenada"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x53

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "340"

    aput-object v3, v2, v4

    const-string v3, "GP"

    aput-object v3, v2, v5

    const-string v3, "Guadeloupe (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x54

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "535"

    aput-object v3, v2, v4

    const-string v3, "GU"

    aput-object v3, v2, v5

    const-string v3, "Guam (US)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x55

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "704"

    aput-object v3, v2, v4

    const-string v3, "GT"

    aput-object v3, v2, v5

    const-string v3, "Guatemala"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x56

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "611"

    aput-object v3, v2, v4

    const-string v3, "GN"

    aput-object v3, v2, v5

    const-string v3, "Guinea"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x57

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "632"

    aput-object v3, v2, v4

    const-string v3, "GW"

    aput-object v3, v2, v5

    const-string v3, "Guinea-Bissau"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x58

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "738"

    aput-object v3, v2, v4

    const-string v3, "GY"

    aput-object v3, v2, v5

    const-string v3, "Guyana"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x59

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "372"

    aput-object v3, v2, v4

    const-string v3, "HT"

    aput-object v3, v2, v5

    const-string v3, "Haiti"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "708"

    aput-object v3, v2, v4

    const-string v3, "HN"

    aput-object v3, v2, v5

    const-string v3, "Honduras"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "454"

    aput-object v3, v2, v4

    const-string v3, "HK"

    aput-object v3, v2, v5

    const-string v3, "Hong Kong (PRC)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "216"

    aput-object v3, v2, v4

    const-string v3, "HU"

    aput-object v3, v2, v5

    const-string v3, "Hungary"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "274"

    aput-object v3, v2, v4

    const-string v3, "IS"

    aput-object v3, v2, v5

    const-string v3, "Iceland"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "404"

    aput-object v3, v2, v4

    const-string v3, "IN"

    aput-object v3, v2, v5

    const-string v3, "India"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "405"

    aput-object v3, v2, v4

    const-string v3, "IN"

    aput-object v3, v2, v5

    const-string v3, "India"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x60

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "406"

    aput-object v3, v2, v4

    const-string v3, "IN"

    aput-object v3, v2, v5

    const-string v3, "India"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x61

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "510"

    aput-object v3, v2, v4

    const-string v3, "ID"

    aput-object v3, v2, v5

    const-string v3, "Indonesia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x62

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "432"

    aput-object v3, v2, v4

    const-string v3, "IR"

    aput-object v3, v2, v5

    const-string v3, "Iran"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x63

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "418"

    aput-object v3, v2, v4

    const-string v3, "IQ"

    aput-object v3, v2, v5

    const-string v3, "Iraq"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x64

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "272"

    aput-object v3, v2, v4

    const-string v3, "IE"

    aput-object v3, v2, v5

    const-string v3, "Ireland"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x65

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "425"

    aput-object v3, v2, v4

    const-string v3, "IL"

    aput-object v3, v2, v5

    const-string v3, "Israel"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x66

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "222"

    aput-object v3, v2, v4

    const-string v3, "IT"

    aput-object v3, v2, v5

    const-string v3, "Italy"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x67

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "338"

    aput-object v3, v2, v4

    const-string v3, "JM"

    aput-object v3, v2, v5

    const-string v3, "Jamaica"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x68

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "441"

    aput-object v3, v2, v4

    const-string v3, "JP"

    aput-object v3, v2, v5

    const-string v3, "Japan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x69

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "440"

    aput-object v3, v2, v4

    const-string v3, "JP"

    aput-object v3, v2, v5

    const-string v3, "Japan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "416"

    aput-object v3, v2, v4

    const-string v3, "JO"

    aput-object v3, v2, v5

    const-string v3, "Jordan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "401"

    aput-object v3, v2, v4

    const-string v3, "KZ"

    aput-object v3, v2, v5

    const-string v3, "Kazakhstan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "639"

    aput-object v3, v2, v4

    const-string v3, "KE"

    aput-object v3, v2, v5

    const-string v3, "Kenya"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "545"

    aput-object v3, v2, v4

    const-string v3, "KI"

    aput-object v3, v2, v5

    const-string v3, "Kiribati"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "467"

    aput-object v3, v2, v4

    const-string v3, "KP"

    aput-object v3, v2, v5

    const-string v3, "Korea, North"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "450"

    aput-object v3, v2, v4

    const-string v3, "KR"

    aput-object v3, v2, v5

    const-string v3, "Korea, Republic of"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x70

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "419"

    aput-object v3, v2, v4

    const-string v3, "KW"

    aput-object v3, v2, v5

    const-string v3, "Kuwait"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x71

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "437"

    aput-object v3, v2, v4

    const-string v3, "KG"

    aput-object v3, v2, v5

    const-string v3, "Kyrgyz Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x72

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "457"

    aput-object v3, v2, v4

    const-string v3, "LA"

    aput-object v3, v2, v5

    const-string v3, "Laos"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x73

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "247"

    aput-object v3, v2, v4

    const-string v3, "LV"

    aput-object v3, v2, v5

    const-string v3, "Latvia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x74

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "415"

    aput-object v3, v2, v4

    const-string v3, "LB"

    aput-object v3, v2, v5

    const-string v3, "Lebanon"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x75

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "651"

    aput-object v3, v2, v4

    const-string v3, "LS"

    aput-object v3, v2, v5

    const-string v3, "Lesotho"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x76

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "618"

    aput-object v3, v2, v4

    const-string v3, "LR"

    aput-object v3, v2, v5

    const-string v3, "Liberia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x77

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "606"

    aput-object v3, v2, v4

    const-string v3, "LY"

    aput-object v3, v2, v5

    const-string v3, "Libya"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x78

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "295"

    aput-object v3, v2, v4

    const-string v3, "LI"

    aput-object v3, v2, v5

    const-string v3, "Liechtenstein"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x79

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "246"

    aput-object v3, v2, v4

    const-string v3, "LT"

    aput-object v3, v2, v5

    const-string v3, "Lithuania"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "270"

    aput-object v3, v2, v4

    const-string v3, "LU"

    aput-object v3, v2, v5

    const-string v3, "Luxembourg"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "455"

    aput-object v3, v2, v4

    const-string v3, "MO"

    aput-object v3, v2, v5

    const-string v3, "Macau (PRC)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "294"

    aput-object v3, v2, v4

    const-string v3, "MK"

    aput-object v3, v2, v5

    const-string v3, "Republic of Macedonia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "646"

    aput-object v3, v2, v4

    const-string v3, "MG"

    aput-object v3, v2, v5

    const-string v3, "Madagascar"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "650"

    aput-object v3, v2, v4

    const-string v3, "MW"

    aput-object v3, v2, v5

    const-string v3, "Malawi"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "502"

    aput-object v3, v2, v4

    const-string v3, "MY"

    aput-object v3, v2, v5

    const-string v3, "Malaysia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x80

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "472"

    aput-object v3, v2, v4

    const-string v3, "MV"

    aput-object v3, v2, v5

    const-string v3, "Maldives"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x81

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "610"

    aput-object v3, v2, v4

    const-string v3, "ML"

    aput-object v3, v2, v5

    const-string v3, "Mali"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x82

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "278"

    aput-object v3, v2, v4

    const-string v3, "MT"

    aput-object v3, v2, v5

    const-string v3, "Malta"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x83

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "551"

    aput-object v3, v2, v4

    const-string v3, "MH"

    aput-object v3, v2, v5

    const-string v3, "Marshall Islands"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x84

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "340"

    aput-object v3, v2, v4

    const-string v3, "MQ"

    aput-object v3, v2, v5

    const-string v3, "Martinique (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x85

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "609"

    aput-object v3, v2, v4

    const-string v3, "MR"

    aput-object v3, v2, v5

    const-string v3, "Mauritania"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x86

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "617"

    aput-object v3, v2, v4

    const-string v3, "MU"

    aput-object v3, v2, v5

    const-string v3, "Mauritius"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x87

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "334"

    aput-object v3, v2, v4

    const-string v3, "MX"

    aput-object v3, v2, v5

    const-string v3, "Mexico"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x88

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "550"

    aput-object v3, v2, v4

    const-string v3, "FM"

    aput-object v3, v2, v5

    const-string v3, "Federated States of Micronesia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x89

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "259"

    aput-object v3, v2, v4

    const-string v3, "MD"

    aput-object v3, v2, v5

    const-string v3, "Moldova"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "212"

    aput-object v3, v2, v4

    const-string v3, "MC"

    aput-object v3, v2, v5

    const-string v3, "Monaco"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "428"

    aput-object v3, v2, v4

    const-string v3, "MN"

    aput-object v3, v2, v5

    const-string v3, "Mongolia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "297"

    aput-object v3, v2, v4

    const-string v3, "ME"

    aput-object v3, v2, v5

    const-string v3, "Montenegro (Republic of)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "354"

    aput-object v3, v2, v4

    const-string v3, "MS"

    aput-object v3, v2, v5

    const-string v3, "Montserrat (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "604"

    aput-object v3, v2, v4

    const-string v3, "MA"

    aput-object v3, v2, v5

    const-string v3, "Morocco"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "643"

    aput-object v3, v2, v4

    const-string v3, "MZ"

    aput-object v3, v2, v5

    const-string v3, "Mozambique"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x90

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "414"

    aput-object v3, v2, v4

    const-string v3, "MM"

    aput-object v3, v2, v5

    const-string v3, "Myanmar"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x91

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "649"

    aput-object v3, v2, v4

    const-string v3, "NA"

    aput-object v3, v2, v5

    const-string v3, "Namibia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x92

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "536"

    aput-object v3, v2, v4

    const-string v3, "NR"

    aput-object v3, v2, v5

    const-string v3, "Nauru"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x93

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "429"

    aput-object v3, v2, v4

    const-string v3, "NP"

    aput-object v3, v2, v5

    const-string v3, "Nepal"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x94

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "204"

    aput-object v3, v2, v4

    const-string v3, "NL"

    aput-object v3, v2, v5

    const-string v3, "Netherlands"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x95

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "546"

    aput-object v3, v2, v4

    const-string v3, "NC"

    aput-object v3, v2, v5

    const-string v3, "New Caledonia (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x96

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "530"

    aput-object v3, v2, v4

    const-string v3, "NZ"

    aput-object v3, v2, v5

    const-string v3, "New Zealand"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x97

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "710"

    aput-object v3, v2, v4

    const-string v3, "NI"

    aput-object v3, v2, v5

    const-string v3, "Nicaragua"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x98

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "614"

    aput-object v3, v2, v4

    const-string v3, "NE"

    aput-object v3, v2, v5

    const-string v3, "Niger"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x99

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "621"

    aput-object v3, v2, v4

    const-string v3, "NG"

    aput-object v3, v2, v5

    const-string v3, "Nigeria"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "555"

    aput-object v3, v2, v4

    const-string v3, "NU"

    aput-object v3, v2, v5

    const-string v3, "Niue"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "534"

    aput-object v3, v2, v4

    const-string v3, "MP"

    aput-object v3, v2, v5

    const-string v3, "Northern Mariana Islands (US)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "242"

    aput-object v3, v2, v4

    const-string v3, "NO"

    aput-object v3, v2, v5

    const-string v3, "Norway"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "422"

    aput-object v3, v2, v4

    const-string v3, "OM"

    aput-object v3, v2, v5

    const-string v3, "Oman"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "410"

    aput-object v3, v2, v4

    const-string v3, "PK"

    aput-object v3, v2, v5

    const-string v3, "Pakistan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "552"

    aput-object v3, v2, v4

    const-string v3, "PW"

    aput-object v3, v2, v5

    const-string v3, "Palau"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "425"

    aput-object v3, v2, v4

    const-string v3, "PS"

    aput-object v3, v2, v5

    const-string v3, "Palestine"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "714"

    aput-object v3, v2, v4

    const-string v3, "PA"

    aput-object v3, v2, v5

    const-string v3, "Panama"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "537"

    aput-object v3, v2, v4

    const-string v3, "PG"

    aput-object v3, v2, v5

    const-string v3, "Papua New Guinea"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "744"

    aput-object v3, v2, v4

    const-string v3, "PY"

    aput-object v3, v2, v5

    const-string v3, "Paraguay"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "716"

    aput-object v3, v2, v4

    const-string v3, "PE"

    aput-object v3, v2, v5

    const-string v3, "Per\u00fa"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "515"

    aput-object v3, v2, v4

    const-string v3, "PH"

    aput-object v3, v2, v5

    const-string v3, "Philippines"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "260"

    aput-object v3, v2, v4

    const-string v3, "PL"

    aput-object v3, v2, v5

    const-string v3, "Poland"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "268"

    aput-object v3, v2, v4

    const-string v3, "PT"

    aput-object v3, v2, v5

    const-string v3, "Portugal"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "330"

    aput-object v3, v2, v4

    const-string v3, "PR"

    aput-object v3, v2, v5

    const-string v3, "Puerto Rico (US)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "427"

    aput-object v3, v2, v4

    const-string v3, "QA"

    aput-object v3, v2, v5

    const-string v3, "Qatar"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "647"

    aput-object v3, v2, v4

    const-string v3, "RE"

    aput-object v3, v2, v5

    const-string v3, "R\u00e9union (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xab

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "226"

    aput-object v3, v2, v4

    const-string v3, "RO"

    aput-object v3, v2, v5

    const-string v3, "Romania"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xac

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "250"

    aput-object v3, v2, v4

    const-string v3, "RU"

    aput-object v3, v2, v5

    const-string v3, "Russian Federation"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xad

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "635"

    aput-object v3, v2, v4

    const-string v3, "RW"

    aput-object v3, v2, v5

    const-string v3, "Rwandese Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xae

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "356"

    aput-object v3, v2, v4

    const-string v3, "KN"

    aput-object v3, v2, v5

    const-string v3, "Saint Kitts and Nevis"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "358"

    aput-object v3, v2, v4

    const-string v3, "LC"

    aput-object v3, v2, v5

    const-string v3, "Saint Lucia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "308"

    aput-object v3, v2, v4

    const-string v3, "PM"

    aput-object v3, v2, v5

    const-string v3, "Saint Pierre and Miquelon (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "360"

    aput-object v3, v2, v4

    const-string v3, "VC"

    aput-object v3, v2, v5

    const-string v3, "Saint Vincent and the Grenadines"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "549"

    aput-object v3, v2, v4

    const-string v3, "WS"

    aput-object v3, v2, v5

    const-string v3, "Samoa"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "292"

    aput-object v3, v2, v4

    const-string v3, "SM"

    aput-object v3, v2, v5

    const-string v3, "San Marino"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "626"

    aput-object v3, v2, v4

    const-string v3, "ST"

    aput-object v3, v2, v5

    const-string v3, "S\u00e3o Tom\u00e9 and Pr\u00edncipe"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "420"

    aput-object v3, v2, v4

    const-string v3, "SA"

    aput-object v3, v2, v5

    const-string v3, "Saudi Arabia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "608"

    aput-object v3, v2, v4

    const-string v3, "SN"

    aput-object v3, v2, v5

    const-string v3, "Senegal"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "220"

    aput-object v3, v2, v4

    const-string v3, "RS"

    aput-object v3, v2, v5

    const-string v3, "Serbia (Republic of)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "633"

    aput-object v3, v2, v4

    const-string v3, "SC"

    aput-object v3, v2, v5

    const-string v3, "Seychelles"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "619"

    aput-object v3, v2, v4

    const-string v3, "SL"

    aput-object v3, v2, v5

    const-string v3, "Sierra Leone"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xba

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "525"

    aput-object v3, v2, v4

    const-string v3, "SG"

    aput-object v3, v2, v5

    const-string v3, "Singapore"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "231"

    aput-object v3, v2, v4

    const-string v3, "SK"

    aput-object v3, v2, v5

    const-string v3, "Slovakia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "293"

    aput-object v3, v2, v4

    const-string v3, "SI"

    aput-object v3, v2, v5

    const-string v3, "Slovenia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "540"

    aput-object v3, v2, v4

    const-string v3, "SB"

    aput-object v3, v2, v5

    const-string v3, "Solomon Islands"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "637"

    aput-object v3, v2, v4

    const-string v3, "SO"

    aput-object v3, v2, v5

    const-string v3, "Somalia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "655"

    aput-object v3, v2, v4

    const-string v3, "ZA"

    aput-object v3, v2, v5

    const-string v3, "South Africa"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "214"

    aput-object v3, v2, v4

    const-string v3, "ES"

    aput-object v3, v2, v5

    const-string v3, "Spain"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "413"

    aput-object v3, v2, v4

    const-string v3, "LK"

    aput-object v3, v2, v5

    const-string v3, "Sri Lanka"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "634"

    aput-object v3, v2, v4

    const-string v3, "SD"

    aput-object v3, v2, v5

    const-string v3, "Sudan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "746"

    aput-object v3, v2, v4

    const-string v3, "SR"

    aput-object v3, v2, v5

    const-string v3, "Suriname"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "653"

    aput-object v3, v2, v4

    const-string v3, "SZ"

    aput-object v3, v2, v5

    const-string v3, "Swaziland"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "240"

    aput-object v3, v2, v4

    const-string v3, "SE"

    aput-object v3, v2, v5

    const-string v3, "Sweden"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "228"

    aput-object v3, v2, v4

    const-string v3, "CH"

    aput-object v3, v2, v5

    const-string v3, "Switzerland"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "417"

    aput-object v3, v2, v4

    const-string v3, "SY"

    aput-object v3, v2, v5

    const-string v3, "Syria"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "466"

    aput-object v3, v2, v4

    const-string v3, "TW"

    aput-object v3, v2, v5

    const-string v3, "Taiwan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "436"

    aput-object v3, v2, v4

    const-string v3, "TJ"

    aput-object v3, v2, v5

    const-string v3, "Tajikistan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xca

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "640"

    aput-object v3, v2, v4

    const-string v3, "TZ"

    aput-object v3, v2, v5

    const-string v3, "Tanzania"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "520"

    aput-object v3, v2, v4

    const-string v3, "TH"

    aput-object v3, v2, v5

    const-string v3, "Thailand"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "615"

    aput-object v3, v2, v4

    const-string v3, "TG"

    aput-object v3, v2, v5

    const-string v3, "Togolese Republic"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "539"

    aput-object v3, v2, v4

    const-string v3, "TO"

    aput-object v3, v2, v5

    const-string v3, "Tonga"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xce

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "374"

    aput-object v3, v2, v4

    const-string v3, "TT"

    aput-object v3, v2, v5

    const-string v3, "Trinidad and Tobago"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "605"

    aput-object v3, v2, v4

    const-string v3, "TN"

    aput-object v3, v2, v5

    const-string v3, "Tunisia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "286"

    aput-object v3, v2, v4

    const-string v3, "TR"

    aput-object v3, v2, v5

    const-string v3, "Turkey"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "438"

    aput-object v3, v2, v4

    const-string v3, "TM"

    aput-object v3, v2, v5

    const-string v3, "Turkmenistan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "376"

    aput-object v3, v2, v4

    const-string v3, "TC"

    aput-object v3, v2, v5

    const-string v3, "Turks and Caicos Islands (UK)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "641"

    aput-object v3, v2, v4

    const-string v3, "UG"

    aput-object v3, v2, v5

    const-string v3, "Uganda"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "255"

    aput-object v3, v2, v4

    const-string v3, "UA"

    aput-object v3, v2, v5

    const-string v3, "Ukraine"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "424"

    aput-object v3, v2, v4

    const-string v3, "AE"

    aput-object v3, v2, v5

    const-string v3, "United Arab Emirates"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "430"

    aput-object v3, v2, v4

    const-string v3, "AE"

    aput-object v3, v2, v5

    const-string v3, "United Arab Emirates"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "431"

    aput-object v3, v2, v4

    const-string v3, "AE"

    aput-object v3, v2, v5

    const-string v3, "United Arab Emirates"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "235"

    aput-object v3, v2, v4

    const-string v3, "GB"

    aput-object v3, v2, v5

    const-string v3, "United Kingdom"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "234"

    aput-object v3, v2, v4

    const-string v3, "GB"

    aput-object v3, v2, v5

    const-string v3, "United Kingdom"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xda

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "310"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "311"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "312"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "313"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xde

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "314"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "315"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "316"

    aput-object v3, v2, v4

    const-string v3, "US"

    aput-object v3, v2, v5

    const-string v3, "United States of America"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "332"

    aput-object v3, v2, v4

    const-string v3, "VI"

    aput-object v3, v2, v5

    const-string v3, "United States Virgin Islands (US)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "748"

    aput-object v3, v2, v4

    const-string v3, "UY"

    aput-object v3, v2, v5

    const-string v3, "Uruguay"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "434"

    aput-object v3, v2, v4

    const-string v3, "UZ"

    aput-object v3, v2, v5

    const-string v3, "Uzbekistan"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "541"

    aput-object v3, v2, v4

    const-string v3, "VU"

    aput-object v3, v2, v5

    const-string v3, "Vanuatu"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "225"

    aput-object v3, v2, v4

    const-string v3, "VA"

    aput-object v3, v2, v5

    const-string v3, "Vatican City State"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "734"

    aput-object v3, v2, v4

    const-string v3, "VE"

    aput-object v3, v2, v5

    const-string v3, "Venezuela"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "452"

    aput-object v3, v2, v4

    const-string v3, "VN"

    aput-object v3, v2, v5

    const-string v3, "Viet Nam"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "543"

    aput-object v3, v2, v4

    const-string v3, "WF"

    aput-object v3, v2, v5

    const-string v3, "Wallis and Futuna (France)"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "421"

    aput-object v3, v2, v4

    const-string v3, "YE"

    aput-object v3, v2, v5

    const-string v3, "Yemen"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xea

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "645"

    aput-object v3, v2, v4

    const-string v3, "ZM"

    aput-object v3, v2, v5

    const-string v3, "Zambia"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "648"

    aput-object v3, v2, v4

    const-string v3, "ZW"

    aput-object v3, v2, v5

    const-string v3, "Zimbabwe"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    return-void
.end method

.method public static findCountryNameByMCC(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 28
    if-nez p0, :cond_0

    .line 29
    const-string v0, ""

    .line 39
    :goto_0
    return-object v0

    .line 31
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    array-length v2, v0

    move v0, v1

    .line 32
    :goto_1
    if-ge v0, v2, :cond_2

    .line 34
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    aget-object v3, v3, v0

    aget-object v3, v3, v1

    invoke-virtual {p0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 36
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    aget-object v0, v1, v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    goto :goto_0

    .line 32
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 39
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static findISOCountryNameByMCC(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11
    if-nez p0, :cond_0

    .line 12
    const-string v0, ""

    .line 22
    :goto_0
    return-object v0

    .line 14
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    array-length v2, v0

    move v0, v1

    .line 15
    :goto_1
    if-ge v0, v2, :cond_2

    .line 17
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    aget-object v3, v3, v0

    aget-object v3, v3, v1

    invoke-virtual {p0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 19
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->CountryNameMCCTable:[[Ljava/lang/String;

    aget-object v0, v1, v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 15
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 22
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method
