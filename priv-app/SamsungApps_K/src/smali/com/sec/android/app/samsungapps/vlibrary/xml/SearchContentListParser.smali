.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;
.source "ProGuard"


# instance fields
.field private _b7000ErrorMeansNeedToMoveResultToRecommendedList:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_b7000ErrorMeansNeedToMoveResultToRecommendedList:Z

    .line 10
    return-void
.end method


# virtual methods
.method public onEndParse()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->mServerError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    const-string v1, "7000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_b7000ErrorMeansNeedToMoveResultToRecommendedList:Z

    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->mServerError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->setErrorCode(Ljava/lang/String;)V

    .line 18
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_bSuccess:Z

    .line 21
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_bSuccess:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_b7000ErrorMeansNeedToMoveResultToRecommendedList:Z

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    .line 24
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;->_TemporalContentList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->onUpdateRecommendKeyword(Ljava/util/ArrayList;)V

    .line 29
    :goto_0
    return-void

    .line 28
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->onEndParse()V

    goto :goto_0
.end method
