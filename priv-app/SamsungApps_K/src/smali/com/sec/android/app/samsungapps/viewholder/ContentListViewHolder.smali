.class public Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected _Adapter:Lcom/sec/android/app/samsungapps/view/IContentArrayAdapter;

.field protected _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

.field protected _Context:Landroid/content/Context;

.field protected _DefaultImage:Landroid/graphics/drawable/Drawable;

.field protected _ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

.field protected _Position:I

.field protected _View:Landroid/view/View;

.field protected _bSingleColumn:Z

.field protected _defaultPrice:Ljava/lang/String;

.field private a:I

.field protected adult_icon:Landroid/widget/ImageView;

.field protected appDescription:Landroid/widget/TextView;

.field protected appSize:Landroid/widget/TextView;

.field private b:Z

.field protected btnRightly:Landroid/widget/Button;

.field private c:Z

.field protected categoryName:Landroid/widget/TextView;

.field private d:Z

.field protected deleteCheckboxGap:Landroid/view/View;

.field protected deletecheckbox:Landroid/widget/CheckedTextView;

.field protected divider:Landroid/view/View;

.field protected downloadCancel:Landroid/widget/ImageView;

.field protected empty_loading:Landroid/view/View;

.field protected google_tencent_icon:Landroid/widget/ImageView;

.field protected installed:Landroid/widget/TextView;

.field protected installedTextView:Landroid/widget/TextView;

.field protected isShowInstalledMark:Z

.field protected layout_list_itemly:Landroid/view/View;

.field protected layout_list_itemly_centerly:Landroid/view/View;

.field protected layout_list_itemly_pricely:Landroid/view/View;

.field protected layout_list_itemly_rightly:Landroid/view/View;

.field protected normalPrice:Landroid/widget/TextView;

.field protected notavailableAppLayout:Landroid/view/View;

.field protected price:Landroid/widget/TextView;

.field protected productType:Landroid/widget/ImageView;

.field protected product_count:Landroid/widget/TextView;

.field protected progressAllText:Landroid/widget/TextView;

.field protected progressLayout:Landroid/view/View;

.field protected progressPercentText:Landroid/widget/TextView;

.field protected progressStatusText:Landroid/widget/TextView;

.field protected progressbar:Landroid/widget/ProgressBar;

.field protected progresstext:Landroid/widget/TextView;

.field protected purchasedDate:Landroid/widget/TextView;

.field protected rating:Landroid/widget/RatingBar;

.field protected remove_item_chek:Landroid/view/View;

.field protected searchView:Landroid/widget/TextView;

.field protected textname:Landroid/widget/TextView;

.field protected webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    .line 91
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Adapter:Lcom/sec/android/app/samsungapps/view/IContentArrayAdapter;

    .line 92
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    .line 93
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    .line 94
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_DefaultImage:Landroid/graphics/drawable/Drawable;

    .line 95
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_defaultPrice:Ljava/lang/String;

    .line 96
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_bSingleColumn:Z

    .line 97
    iput p7, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->a:I

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c005e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0059

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c005f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c014e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->purchasedDate:Landroid/widget/TextView;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0063

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0062

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->price:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0064

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0293

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c003d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->progressLayout:Landroid/view/View;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c003f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->progressStatusText:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0042

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->progressPercentText:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0040

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->progresstext:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0041

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->progressAllText:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->progressbar:Landroid/widget/ProgressBar;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0044

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0160

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->searchView:Landroid/widget/TextView;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0065

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->empty_loading:Landroid/view/View;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0056

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly:Landroid/view/View;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0061

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0154

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->btnRightly:Landroid/widget/Button;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->productType:Landroid/widget/ImageView;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->installedTextView:Landroid/widget/TextView;

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0060

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->notavailableAppLayout:Landroid/view/View;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c005d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c01a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->product_count:Landroid/widget/TextView;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c005b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->adult_icon:Landroid/widget/ImageView;

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0155

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->remove_item_chek:Landroid/view/View;

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0150

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->appSize:Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0294

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->deleteCheckboxGap:Landroid/view/View;

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c014f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->appDescription:Landroid/widget/TextView;

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c015f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->divider:Landroid/view/View;

    .line 133
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    :cond_0
    return-void
.end method


# virtual methods
.method protected cropCenterBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 591
    if-nez p1, :cond_0

    .line 616
    :goto_0
    return-object v0

    .line 597
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 598
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 601
    int-to-float v3, v1

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 602
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 606
    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 609
    invoke-static {p1, v1, v2, v3, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 611
    :catch_0
    move-exception v1

    .line 612
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentListViewHolder::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected cropScaledCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 514
    if-nez p1, :cond_1

    .line 546
    :cond_0
    :goto_0
    return-object v0

    .line 520
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 521
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 524
    int-to-float v3, v1

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v5

    float-to-int v3, v3

    .line 525
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 529
    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 532
    invoke-static {p1, v1, v2, v3, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 534
    const/4 v1, 0x1

    invoke-static {v6, p3, p3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 535
    if-eqz v2, :cond_0

    .line 539
    if-nez v2, :cond_2

    move-object v1, v0

    .line 540
    :goto_1
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 541
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    .line 542
    goto :goto_0

    .line 539
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ge v3, p2, :cond_3

    if-ge v1, p3, :cond_3

    move-object v1, v2

    goto :goto_1

    :cond_3
    if-le v3, p2, :cond_7

    sub-int v5, v3, p2

    div-int/lit8 v5, v5, 0x2

    :goto_2
    if-le v1, p3, :cond_4

    sub-int v4, v1, p3

    div-int/lit8 v4, v4, 0x2

    :cond_4
    if-le p2, v3, :cond_5

    move p2, v3

    :cond_5
    if-ge p3, v1, :cond_6

    move p3, v1

    :cond_6
    invoke-static {v2, v5, v4, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 544
    :catch_0
    move-exception v1

    .line 545
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentListViewHolder::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move v5, v4

    goto :goto_2
.end method

.method public displayNormalItem()V
    .locals 0

    .prologue
    .line 621
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showProductName()V

    .line 622
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showProductImg()V

    .line 623
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showCategory()V

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showProductType()V

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showAdultIcon()V

    .line 626
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showProductRating()V

    .line 627
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showProductnormalPrice()V

    .line 628
    return-void
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Position:I

    return v0
.end method

.method public hideRating()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 365
    :cond_0
    return-void
.end method

.method protected isDestroyed()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->b:Z

    return v0
.end method

.method public isShowInstalledMark()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->isShowInstalledMark:Z

    return v0
.end method

.method protected prepareView()V
    .locals 0

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->displayNormalItem()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->showCommonStatus()V

    .line 174
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->b:Z

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    .line 139
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->prepareView()V

    .line 150
    return-void
.end method

.method public setIsShowInstalledMark(Z)V
    .locals 0

    .prologue
    .line 154
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->isShowInstalledMark:Z

    .line 155
    return-void
.end method

.method public setPosition(I)V
    .locals 0

    .prologue
    .line 163
    iput p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Position:I

    .line 164
    return-void
.end method

.method protected setViewDisabled()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 475
    :cond_0
    return-void
.end method

.method protected setViewEnabled()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 485
    :cond_0
    return-void
.end method

.method public showAdultIcon()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->adult_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultIcon(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->c:Z

    .line 306
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->c:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->adult_icon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->adult_icon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showCategory()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isPrePostApp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 328
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/e;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 345
    :cond_0
    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showCommonStatus()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 458
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->a:I

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 465
    :cond_1
    return-void
.end method

.method public showInstalledText()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->isShowInstalledMark()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->installedTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->installedTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 197
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->a()V

    goto :goto_0

    .line 201
    :catch_1
    move-exception v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->a()V

    goto :goto_0
.end method

.method public showProductImg()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultBlur(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->d:Z

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 276
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->d:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_DefaultImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showProductName()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 226
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Position:I

    add-int/lit8 v0, v0, 0x1

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[S_I_N_A]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[S_I_N_A]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/d;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 268
    :cond_1
    return-void

    .line 235
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v1

    if-ne v1, v5, :cond_3

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". <img src=\"google_tencent_icon\" /> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder$ImageGetter;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder$ImageGetter;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 239
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public showProductRating()V
    .locals 5

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    const-wide/high16 v1, 0x3fe0000000000000L    # 0.5

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->averageRating:I

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    goto :goto_0
.end method

.method public showProductType()V
    .locals 4

    .prologue
    const/16 v0, 0x8

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->productType:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultIcon(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->c:Z

    .line 294
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->c:Z

    if-nez v1, :cond_2

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->productType:Landroid/widget/ImageView;

    const-string v2, "widget"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->contentType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 300
    :cond_1
    :goto_0
    return-void

    .line 297
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->productType:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showProductnormalPrice()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 384
    if-nez v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    iget-wide v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    .line 390
    iget-wide v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountPrice:D

    .line 392
    iget-boolean v5, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountFlag:Z

    if-eqz v5, :cond_3

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v5

    iget-wide v6, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    iget-object v8, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    cmpl-double v1, v3, v9

    if-nez v1, :cond_2

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_defaultPrice:Ljava/lang/String;

    .line 405
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->price:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->price:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/f;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/g;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_0

    .line 403
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 410
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->normalPrice:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    cmpl-double v3, v1, v9

    if-nez v3, :cond_4

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_defaultPrice:Ljava/lang/String;

    .line 420
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->price:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 418
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v3, v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public showRating()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->rating:Landroid/widget/RatingBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto :goto_0
.end method
