.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 10
    return-void
.end method


# virtual methods
.method public contentsSize()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "contentsSize"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deltaContentsSize()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "deltaContentsSize"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deltaDownloadURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "deltaDownloadURL"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public downloadUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "downloadUri"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public orderID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "orderID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public paymentID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "paymentID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public paymentRequestCompleteDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "paymentRequestCompleteDate"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public paymentStatusCD()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "paymentStatusCD"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public productID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "productID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public productName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "productName"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public successYn()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "successYn"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
