.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _CHINA_ALIPAY:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _CHINA_CYBERCASH:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _CHINA_PPC:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _DIRECT_BILLING:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _IRAN_DEBIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _KOREA_DANAL:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _KOREA_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _PSMS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _SAMSUNG_WALLET:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field public static final enum _UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;


# instance fields
.field private _CVCDigitCount:I

.field private _CVCRequired:Z

.field private _bAllCouponRequest:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x4

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v1, "_Normal"

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 11
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v4, "_KOREA_DANAL"

    move v6, v2

    move v7, v2

    move v8, v5

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_DANAL:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 12
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_PSMS"

    move v8, v13

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_PSMS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 13
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_DIRECT_BILLING"

    move v8, v14

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_DIRECT_BILLING:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 14
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_KOREA_INICIS"

    move v8, v12

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 15
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_KOREA_UPOINT"

    const/4 v8, 0x5

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 16
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_CHINA_ALIPAY"

    const/4 v8, 0x6

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_ALIPAY:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 17
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_CHINA_PPC"

    const/4 v8, 0x7

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_PPC:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 18
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_CHINA_CYBERCASH"

    const/16 v8, 0x8

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_CYBERCASH:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 19
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_GLOBAL_CREDIT_CARD"

    const/16 v8, 0x9

    move v9, v2

    move v10, v12

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 20
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_UKRAINE_CREDIT_CARD"

    const/16 v8, 0xa

    move v9, v5

    move v10, v12

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 21
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_IRAN_DEBIT_CARD"

    const/16 v8, 0xb

    move v9, v2

    move v10, v12

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_IRAN_DEBIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 22
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    const-string v7, "_SAMSUNG_WALLET"

    const/16 v8, 0xc

    move v9, v2

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;-><init>(Ljava/lang/String;IZIZ)V

    sput-object v6, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_SAMSUNG_WALLET:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 9
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_DANAL:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_PSMS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v1, v0, v13

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_DIRECT_BILLING:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v1, v0, v14

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_ALIPAY:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_PPC:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_CYBERCASH:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_IRAN_DEBIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_SAMSUNG_WALLET:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZIZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CVCRequired:Z

    .line 26
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CVCDigitCount:I

    .line 31
    iput-boolean p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CVCRequired:Z

    .line 32
    iput p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CVCDigitCount:I

    .line 33
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_bAllCouponRequest:Z

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method


# virtual methods
.method public final getCVCDigitCount()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CVCDigitCount:I

    return v0
.end method

.method public final getCouponRequestCondition(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/a;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getPaymentMinPrice()D
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/b;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 99
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 103
    :goto_0
    return-wide v0

    .line 89
    :pswitch_0
    const-wide v0, 0x4073600000000000L    # 310.0

    .line 90
    goto :goto_0

    .line 96
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getMinPrice()D

    move-result-wide v0

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;
    .locals 6

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_PSMS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne p0, v0, :cond_0

    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;->getPhoneBillPrice()D

    move-result-wide v1

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;->getDiscountPhoneBillPrice()D

    move-result-wide v3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;->isPhoneBillDiscounted()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;-><init>(DDZ)V

    .line 54
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;->getSellingPrice()D

    move-result-wide v1

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;->getReducePrice()D

    move-result-wide v3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;->isDiscounted()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;-><init>(DDZ)V

    goto :goto_0
.end method

.method public final isAllCoupon()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_bAllCouponRequest:Z

    return v0
.end method

.method public final isCVCRequired()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CVCRequired:Z

    return v0
.end method
