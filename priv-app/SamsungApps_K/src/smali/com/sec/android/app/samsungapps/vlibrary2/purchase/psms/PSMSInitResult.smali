.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field protected final DEFAULT_NEED_SEND_SMS:I

.field protected final DEFAULT_RETRY_COUNT:I

.field protected final DEFAULT_RETRY_WAIT_TIME:I

.field public confirmMsg:Ljava/lang/String;

.field private mMessagesList:Ljava/util/ArrayList;

.field private mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private mShortCodeList:Ljava/util/ArrayList;

.field public message:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public paymentID:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public randomKey:Ljava/lang/String;

.field public responseTime:I

.field public retryCount:I

.field public sendSMS:I

.field public shortCode:Ljava/lang/String;

.field public tncMsg:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x5

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->responseTime:I

    .line 18
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->sendSMS:I

    .line 19
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->retryCount:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->productID:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->orderID:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->paymentID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->shortCode:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->message:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->randomKey:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->tncMsg:Ljava/lang/String;

    .line 29
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mShortCodeList:Ljava/util/ArrayList;

    .line 30
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mMessagesList:Ljava/util/ArrayList;

    .line 31
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 33
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->DEFAULT_RETRY_COUNT:I

    .line 34
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->DEFAULT_RETRY_WAIT_TIME:I

    .line 35
    iput v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->DEFAULT_NEED_SEND_SMS:I

    .line 39
    iput v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->sendSMS:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->responseTime:I

    .line 41
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->retryCount:I

    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 43
    return-void
.end method

.method private createMessages()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->message:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->message:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mMessagesList:Ljava/util/ArrayList;

    .line 129
    :goto_0
    return-object v0

    .line 118
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mMessagesList:Ljava/util/ArrayList;

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->message:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 124
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 126
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mMessagesList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mMessagesList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private createShortCodes()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->shortCode:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->shortCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mShortCodeList:Ljava/util/ArrayList;

    .line 105
    :goto_0
    return-object v0

    .line 94
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mShortCodeList:Ljava/util/ArrayList;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->shortCode:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 100
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 102
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mShortCodeList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mShortCodeList:Ljava/util/ArrayList;

    goto :goto_0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 76
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->createShortCodes()Ljava/util/ArrayList;

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->createMessages()Ljava/util/ArrayList;

    .line 70
    return-void
.end method

.method public existConfirmMsg()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public existRandomKey()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->randomKey:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->randomKey:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public existTNC()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->tncMsg:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->tncMsg:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfirmMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mMessagesList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->orderID:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->paymentID:Ljava/lang/String;

    return-object v0
.end method

.method public getRandomKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->randomKey:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseTime()J
    .locals 2

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->responseTime:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->retryCount:I

    return v0
.end method

.method public getSendSMS()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 144
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->sendSMS:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShortCode(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->mShortCodeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTNCMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->tncMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getTNCSMSMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getMessage(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTNCSMSNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getShortCode(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method
