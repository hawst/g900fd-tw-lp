.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;


# instance fields
.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private _ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field private couponList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 19
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 24
    return-void
.end method

.method public complete()V
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 47
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 39
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->couponList:Ljava/util/ArrayList;

    .line 40
    return-void
.end method
