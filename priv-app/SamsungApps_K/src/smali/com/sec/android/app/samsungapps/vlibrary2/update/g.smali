.class final Lcom/sec/android/app/samsungapps/vlibrary2/update/g;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 113
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    const-string v1, "com.alipay.android.app"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->onAlipayInstalled()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 115
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.alipay.android.app"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v4, :cond_0

    .line 117
    const-string v0, "SAUtilityNeedUpdaet::onAlipayInstalled::Wrong package name"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 121
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/k;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 124
    :pswitch_0
    const-string v0, "SAUtilityNeedUpdate::onAlipayInstalled::Installed::"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->stopLoadingDialog()V

    .line 126
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeAlipayUpdate(Z)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->installCompleted(Z)V

    goto :goto_0

    .line 131
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->stopLoadingDialog()V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->installCompleted(Z)V

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
