.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _IGiftCardCouponInterface:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

.field _IPriceChangeListeners:Ljava/util/ArrayList;

.field private _IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

.field private _SecCode:Ljava/lang/String;

.field _SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

.field _SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

.field _SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceChangeListeners:Ljava/util/ArrayList;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 26
    return-void
.end method


# virtual methods
.method public addIGiftCardCouponInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IGiftCardCouponInterface:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    .line 114
    return-void
.end method

.method public addIPriceChangeListener(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public createIPriceCalculator()Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculatorCreator;->createPaidPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    move-result-object v0

    return-object v0
.end method

.method public createRealPurchaseInfo(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setSelCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->setSelGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V

    .line 164
    return-object p1
.end method

.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SecCode:Ljava/lang/String;

    return-object v0
.end method

.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequiredCodeLength()I
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getCVCDigitCount()I

    move-result v0

    .line 146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    return-object v0
.end method

.method public getSelGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    return-object v0
.end method

.method public getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method

.method public getSelPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method

.method public getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->getSelCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->getSelGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->getSelPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    return-object v0
.end method

.method public isAllCoupon()Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->isAllCoupon()Z

    move-result v0

    return v0
.end method

.method public isSecurityCodeRequired()Z
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->getRequiredCodeLength()I

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const/4 v0, 0x1

    .line 137
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public notifyChangedPrice()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;

    .line 76
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;->onPriceChanged()V

    goto :goto_0

    .line 78
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceChangeListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 95
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IPriceChangeListeners:Ljava/util/ArrayList;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IGiftCardCouponInterface:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    .line 97
    return-void
.end method

.method public save()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public selCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)Z
    .locals 1

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->notifyChangedPrice()V

    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public selGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)Z
    .locals 1

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->notifyChangedPrice()V

    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public selPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z
    .locals 1

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SelPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->notifyChangedPrice()V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IGiftCardCouponInterface:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_IGiftCardCouponInterface:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;->onPaymentMethodChanged()V

    .line 38
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->_SecCode:Ljava/lang/String;

    .line 152
    return-void
.end method
