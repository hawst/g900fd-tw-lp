.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final A_SIGNED_NAME:Ljava/lang/String; = "Junos Pulse for Galaxy S and Tab 7.0"

.field private static final B_SIGNED_NAME:Ljava/lang/String; = "Junos Pulse"

.field private static final SIGNED_PLATFORM_NEW:I = 0x3

.field private static final SIGNED_PLATFORM_OLD:I = 0x1

.field private static final SIGNED_PLATFORM_UNKNOWN:I

.field private static mInstance:Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;


# instance fields
.field mContext:Landroid/content/Context;

.field mSignedKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mInstance:Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mSignedKey:I

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mContext:Landroid/content/Context;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mContext:Landroid/content/Context;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->preparePlatformKey()V

    .line 41
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mInstance:Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mInstance:Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    .line 33
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mInstance:Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    goto :goto_0
.end method

.method private preparePlatformKey()V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 52
    const-string v0, "JunosPulseChecker::preparePlatformKey::context is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mSignedKey:I

    .line 61
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    if-nez v1, :cond_3

    .line 64
    :cond_2
    const-string v0, "JunosPulseChecker::preparePlatformKey::document is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getPlatformKey()Ljava/lang/String;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 76
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mSignedKey:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JunosPulseChecker::preparePlatformKey::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 93
    .line 95
    if-nez p1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v2

    .line 101
    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eq v3, v1, :cond_0

    .line 106
    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mSignedKey:I

    if-ne v3, v1, :cond_3

    .line 108
    const-string v3, "Junos Pulse"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    :cond_2
    :goto_1
    move v0, v1

    .line 124
    goto :goto_0

    .line 113
    :cond_3
    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mSignedKey:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 115
    const-string v3, "Junos Pulse for Galaxy S and Tab 7.0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    goto :goto_1

    .line 120
    :cond_4
    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->mSignedKey:I

    if-nez v3, :cond_0

    .line 122
    const-string v3, "Junos Pulse for Galaxy S and Tab 7.0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v1, :cond_2

    const-string v3, "Junos Pulse"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    goto :goto_1
.end method
