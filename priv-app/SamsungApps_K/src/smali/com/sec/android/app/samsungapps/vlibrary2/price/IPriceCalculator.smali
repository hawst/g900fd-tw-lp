.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getBuyPrice()D
.end method

.method public abstract getDiscountedPrice()D
.end method

.method public abstract getDisplayBuyPrice()D
.end method

.method public abstract getNormalPrice()D
.end method

.method public abstract isDiscounted()Z
.end method
