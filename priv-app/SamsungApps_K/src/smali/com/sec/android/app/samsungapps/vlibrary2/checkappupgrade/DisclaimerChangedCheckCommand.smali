.class public Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CheckAppUpgradeResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

.field private _CurDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->_CurDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->_CheckAppUpgradeResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 19
    return-void
.end method

.method private onDisclaimerChanged()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method


# virtual methods
.method protected checkNewDisclaimer()Z
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->isDisclaimerAlreadyAcceptedByUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    .line 44
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->isDiclamerUpdated()Z

    move-result v0

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->checkNewDisclaimer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->onDisclaimerChanged()V

    .line 31
    :goto_0
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method protected isDiclamerUpdated()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/j;->a:[I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->_CurDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->_CheckAppUpgradeResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget-object v4, v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disclaimerVer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->compareVersion(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 70
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 64
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 68
    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected isDisclaimerAlreadyAcceptedByUser()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/DisclaimerChangedCheckCommand;->_CurDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->isAgreed()Z

    move-result v0

    return v0
.end method
