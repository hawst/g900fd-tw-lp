.class public Lcom/sec/android/app/samsungapps/initializer/Config;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->a:Z

    .line 8
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->b:Z

    .line 9
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->c:Z

    .line 10
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->d:Z

    .line 11
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->e:Z

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->f:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->g:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->h:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->i:Ljava/lang/String;

    .line 16
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->j:Z

    .line 17
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->k:Z

    .line 18
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->l:Z

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->m:Z

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->n:Z

    .line 22
    const v0, 0x15180

    iput v0, p0, Lcom/sec/android/app/samsungapps/initializer/Config;->o:I

    return-void
.end method

.method private static a()Z
    .locals 1

    .prologue
    .line 34
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 40
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public getAutoUpdateDefaultInterval()I
    .locals 1

    .prologue
    .line 97
    const v0, 0x15180

    return v0
.end method

.method public getCSC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, ""

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, ""

    return-object v0
.end method

.method public getMNC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, ""

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, ""

    return-object v0
.end method

.method public isGameHubSupported()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public isGeoIpBasedCountrySearch()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method public isKnoxMode()Z
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Config;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isLogMode()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public isReleaseMode()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public isSamsungAccountUsed()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public isSamsungUpdateMode()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    :cond_0
    return v1
.end method

.method public isSlienceInstallSupported()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public isUnifiedBillingSupported()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public isUsingApkVersionUnifiedBilling()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method
