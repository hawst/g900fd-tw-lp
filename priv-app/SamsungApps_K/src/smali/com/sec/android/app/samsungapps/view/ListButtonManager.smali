.class public Lcom/sec/android/app/samsungapps/view/ListButtonManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/Boolean;

.field private b:Landroid/view/View;

.field private c:I

.field private d:Landroid/content/Context;

.field private e:Landroid/widget/AbsListView;

.field private f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/AbsListView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 53
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h:I

    .line 54
    const v0, -0xba4a0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i:I

    .line 355
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j:I

    .line 356
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->k:I

    .line 41
    if-nez p2, :cond_0

    .line 51
    :goto_0
    return-void

    .line 45
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    .line 47
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->e:Landroid/widget/AbsListView;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isPaidTabFirst()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LBM isPaid: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/content/res/Configuration;)I
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 361
    const/16 v0, 0xff

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v2, v3, :cond_1

    .line 363
    const/4 v0, 0x4

    .line 368
    :cond_0
    :goto_0
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v1, :cond_2

    .line 369
    iget v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j:I

    div-int v0, v1, v0

    .line 374
    :goto_1
    return v0

    .line 364
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 365
    goto :goto_0

    .line 371
    :cond_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->k:I

    div-int v0, v1, v0

    goto :goto_1
.end method

.method private a()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private a(I)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0071

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v2, 0x7f0c006f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v3, 0x7f0c0072

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v8, 0x7f0c0075

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 68
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 69
    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 70
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 71
    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 74
    iget v8, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h:I

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 75
    iget v8, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h:I

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 76
    iget v8, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h:I

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 77
    iget v8, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h:I

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    packed-switch p1, :pswitch_data_0

    .line 98
    :goto_0
    return-void

    .line 82
    :pswitch_0
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 90
    :pswitch_2
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i:I

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 94
    :pswitch_3
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i:I

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 6

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 204
    if-nez p1, :cond_1

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    packed-switch v0, :pswitch_data_0

    .line 321
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->e()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 323
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v0, v1, :cond_b

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 328
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 329
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 342
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CHM"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 345
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 347
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->requestDataGet()V

    goto :goto_0

    .line 212
    :pswitch_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 213
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 219
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_1

    .line 225
    :cond_3
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    goto/16 :goto_1

    .line 229
    :pswitch_1
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 232
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 238
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_1

    .line 244
    :cond_4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    goto/16 :goto_1

    .line 249
    :cond_5
    invoke-virtual {p1, v5}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 251
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 255
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_1

    .line 261
    :cond_6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    goto/16 :goto_1

    .line 266
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 269
    invoke-virtual {p1, v5}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 271
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 275
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_1

    .line 281
    :cond_7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    goto/16 :goto_1

    .line 286
    :cond_8
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 288
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 292
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_1

    .line 298
    :cond_9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    goto/16 :goto_1

    .line 303
    :pswitch_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 304
    invoke-virtual {p1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 306
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 310
    invoke-virtual {p1, v4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setPaidTypeFilter(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_1

    .line 316
    :cond_a
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->recent:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    goto/16 :goto_1

    .line 331
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v0, v1, :cond_c

    .line 333
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto/16 :goto_2

    .line 337
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto/16 :goto_2

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/ListButtonManager;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;I)V
    .locals 2

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    if-eq v0, p2, :cond_0

    iput p2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->e:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->isMoreLoadingViewShown()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->showMoreLoadingView(Z)V

    :cond_0
    return-void
.end method

.method private b()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private c()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0072

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private d()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private e()Landroid/widget/RelativeLayout;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private f()Landroid/widget/RelativeLayout;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c006d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private g()Landroid/widget/RelativeLayout;
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0070

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private h()Landroid/widget/RelativeLayout;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private i()Landroid/view/View;
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    const v1, 0x7f0c0069

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private j()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v0, :cond_0

    .line 188
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 199
    :goto_0
    return-object v0

    .line 191
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ai;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 199
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 197
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getCurrButtonPosition()I
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    return v0
.end method

.method public manageButtonStates(Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;Landroid/view/View;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 419
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    .line 423
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v0, :cond_2

    .line 426
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 429
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->e()Landroid/widget/RelativeLayout;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ae;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/view/ae;-><init>(Lcom/sec/android/app/samsungapps/view/ListButtonManager;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f()Landroid/widget/RelativeLayout;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/view/af;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/view/af;-><init>(Lcom/sec/android/app/samsungapps/view/ListButtonManager;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g()Landroid/widget/RelativeLayout;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ag;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/view/ag;-><init>(Lcom/sec/android/app/samsungapps/view/ListButtonManager;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h()Landroid/widget/RelativeLayout;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ah;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/view/ah;-><init>(Lcom/sec/android/app/samsungapps/view/ListButtonManager;Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v0, v1, :cond_3

    .line 432
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 436
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->i()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 437
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0802b3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 439
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08029c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 443
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08029d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 445
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 454
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->setTabTextSize(Landroid/content/res/Configuration;)V

    .line 457
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 458
    iput v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    .line 461
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-eqz v0, :cond_5

    .line 463
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ai;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 500
    :cond_5
    :goto_2
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ai;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 522
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 532
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v1, :cond_9

    .line 536
    invoke-direct {p0, v6}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 537
    iput v6, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    .line 545
    :goto_4
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    packed-switch v0, :pswitch_data_2

    :pswitch_0
    goto/16 :goto_0

    .line 549
    :pswitch_1
    invoke-direct {p0, p3}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_0

    .line 448
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 450
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08029d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 466
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 467
    iput v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto :goto_2

    .line 470
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 472
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 473
    iput v5, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto :goto_2

    .line 477
    :cond_7
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 478
    iput v4, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto :goto_2

    .line 482
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 484
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 485
    iput v4, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto/16 :goto_2

    .line 489
    :cond_8
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 490
    iput v5, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto/16 :goto_2

    .line 494
    :pswitch_5
    invoke-direct {p0, v6}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 495
    iput v6, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto/16 :goto_2

    .line 502
    :pswitch_6
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_3

    .line 508
    :pswitch_7
    invoke-direct {p0, p3}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_3

    .line 513
    :pswitch_8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 514
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 516
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_3

    .line 521
    :pswitch_9
    invoke-direct {p0, p3}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    goto/16 :goto_3

    .line 541
    :cond_9
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(I)V

    .line 542
    iput v2, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c:I

    goto/16 :goto_4

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 500
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_8
    .end packed-switch

    .line 545
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 502
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    .line 516
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 560
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->e()Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 561
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->f()Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 562
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g()Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 563
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->h()Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 570
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 571
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b:Landroid/view/View;

    .line 572
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    .line 573
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->e:Landroid/widget/AbsListView;

    .line 574
    return-void

    .line 564
    :catch_0
    move-exception v0

    .line 565
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ListButtonManager::NullPointerException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 566
    :catch_1
    move-exception v0

    .line 567
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ListButtonManager::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTabTextSize(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/16 v4, 0xf

    const/4 v3, 0x1

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    if-nez p1, :cond_2

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    .line 387
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j:I

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->k:I

    .line 396
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 398
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a()Landroid/widget/TextView;

    move-result-object v0

    .line 399
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 401
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 402
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d()Landroid/widget/TextView;

    move-result-object v0

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 406
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 407
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->b()Landroid/widget/TextView;

    move-result-object v0

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 411
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->c()Landroid/widget/TextView;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 391
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->k:I

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ListButtonManager;->j:I

    goto :goto_1
.end method
