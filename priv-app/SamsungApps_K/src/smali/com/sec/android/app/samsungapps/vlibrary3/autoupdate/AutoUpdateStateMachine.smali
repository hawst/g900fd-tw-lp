.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;

    .line 28
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 106
    const-string v0, "AutoUpdateStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_0
    :pswitch_0
    return-void

    .line 109
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_WHETHER_INITIALIZED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_IF_UPDATETIMING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 117
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 118
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->RUN_SELLER_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 129
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_SELF_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 33
    const-string v0, "AutoUpdateStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 101
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 36
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 38
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->INITIALIZE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 45
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->CHECK_UPDATE_TIMING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 48
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 56
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->CHECK_SELF_UPD:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 69
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->SELLER_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->SELLER_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 96
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->REQUEST_SELLER_UPDATE_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 93
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_f
    .end packed-switch

    .line 36
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 43
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 53
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 67
    :pswitch_data_4
    .packed-switch 0x7
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 88
    :pswitch_data_5
    .packed-switch 0xa
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 139
    const-string v0, "AutoUpdateStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    return-void
.end method
