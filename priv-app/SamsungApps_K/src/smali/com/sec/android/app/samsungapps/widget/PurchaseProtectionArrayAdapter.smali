.class public Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;
.super Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 15
    return-void
.end method


# virtual methods
.method public getPreferenceDefaultValue()I
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getPurchaseProtectionSettingValue()I

    move-result v0

    .line 27
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
