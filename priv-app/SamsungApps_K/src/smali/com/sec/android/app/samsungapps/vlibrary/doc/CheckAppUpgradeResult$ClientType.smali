.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

.field public static final enum ODC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

.field public static final enum Open:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

.field public static final enum UNA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

.field public static final enum UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    const-string v1, "Open"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->Open:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    .line 239
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    const-string v1, "ODC"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ODC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    .line 240
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    const-string v1, "UNC"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    .line 241
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    const-string v1, "UNA"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    .line 236
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->Open:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ODC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;
    .locals 1

    .prologue
    .line 236
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;
    .locals 1

    .prologue
    .line 236
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    return-object v0
.end method
