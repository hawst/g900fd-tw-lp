.class public Lcom/sec/android/app/samsungapps/widget/OptionItem;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->a:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->a:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->b:Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->a:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->c:Z

    return v0
.end method

.method public setCheck(Z)V
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionItem;->c:Z

    .line 28
    return-void
.end method
