.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginCommandData;

.field private _ILoginView:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;

.field private _IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_ILoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginCommandData;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_ILoginView:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->onFinalResult(Z)V

    return-void
.end method

.method private onSamsungAccountValidated()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_Context:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->invokeUI(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method private validateEmail(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 86
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 87
    :cond_0
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private validatePassword(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->validateEmail(Ljava/lang/String;)Z

    move-result v1

    .line 98
    if-nez v1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 101
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x6

    if-lt v2, v3, :cond_0

    move v0, v1

    .line 104
    goto :goto_0
.end method


# virtual methods
.method public cancelLogin()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->onFinalResult(Z)V

    .line 48
    return-void
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->onSamsungAccountValidated()V

    .line 28
    return-void
.end method

.method public invokeComplete(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_ILoginView:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;

    .line 43
    return-void
.end method

.method protected invokeUI(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    invoke-interface {v0, p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method public notifySuccess()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->onFinalResult(Z)V

    .line 110
    return-void
.end method

.method public requestLogin(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;)V
    .locals 3

    .prologue
    .line 52
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;->getEmailID()Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;->getPassword()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->validateEmail(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_ILoginView:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;->onInvalidEmailID()V

    .line 82
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->validatePassword(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_ILoginView:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;->onInvalidPassword()V

    goto :goto_0

    .line 65
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v2

    .line 66
    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setPass(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->_ILoginView:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;->onLoadingStart()V

    .line 70
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/n;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;)V

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->login(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Z)V

    goto :goto_0
.end method
