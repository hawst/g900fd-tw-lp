.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IAlipayInstallCheckCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->_IAlipayInstallCheckCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->installAlipay()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->onFinalResult(Z)V

    return-void
.end method

.method private installAlipay()V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->_IAlipayInstallCheckCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;->installAlipay()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 52
    return-void
.end method

.method private isAlipayInstalled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 77
    const-string v1, "com.alipay.android.app"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private needAlipayUpdate(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->needAlipayUpdate()Z

    move-result v1

    .line 66
    if-ne v1, v0, :cond_0

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->needAlipayUpdate(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->onFinalResult(Z)V

    .line 41
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->_Context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->isAlipayInstalled(Landroid/content/Context;)Z

    move-result v0

    .line 27
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->_IAlipayInstallCheckCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;->onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
