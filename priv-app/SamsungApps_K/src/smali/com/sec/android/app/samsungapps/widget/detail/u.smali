.class final Lcom/sec/android/app/samsungapps/widget/detail/u;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/u;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 4

    .prologue
    .line 401
    if-eqz p1, :cond_0

    .line 402
    const-string v0, "ReviewDetailWidget::onCommandResult:: onSaveReview()"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 430
    :goto_0
    return-void

    .line 404
    :cond_0
    const-string v0, "ReviewDetailWidget::onCommandResult:: onSaveReview() Fail!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/u;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->n(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->isMyReviewDuplicated()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    const-string v0, "ReviewDetailWidget::onCommandResult:: onSaveReview() MyReview Duplicated!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/u;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->o(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    goto :goto_0

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/u;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/RatingBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setIsIndicator(Z)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/u;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/v;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/detail/v;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/u;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
