.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;
.super Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;
.source "ProGuard"


# instance fields
.field protected _IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

.field private _hasOldDetail:Z

.field private _oldAlreadyPurchase:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)V

    .line 18
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_oldAlreadyPurchase:Z

    .line 19
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_hasOldDetail:Z

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    .line 16
    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_Context:Landroid/content/Context;

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_hasOldDetail:Z

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->alreadyPurchased:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_oldAlreadyPurchase:Z

    .line 33
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;->onDetailLoading(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 36
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 37
    return-void

    .line 30
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_hasOldDetail:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_oldAlreadyPurchase:Z

    goto :goto_0
.end method

.method protected notifyAlreadyPurchased()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->superOnFinalResult(Z)V

    .line 66
    return-void
.end method

.method protected onFinalResult(Z)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_IDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;->onDetailUpdated(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)V

    .line 45
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_hasOldDetail:Z

    if-eqz v0, :cond_1

    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->_oldAlreadyPurchase:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->alreadyPurchased:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->mContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail;->notifyAlreadyPurchased()V

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method protected superOnFinalResult(Z)V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetProductDetailCommand;->onFinalResult(Z)V

    .line 71
    return-void
.end method
