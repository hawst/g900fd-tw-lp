.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationParser;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 10
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 20
    const-string v0, "realAge"

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 21
    if-eq v0, v1, :cond_0

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationParser;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getRealAgeInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->setAge(I)V

    .line 25
    :cond_0
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method
