.class public Lcom/sec/android/app/samsungapps/SMS;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Landroid/telephony/SmsManager;

.field e:Landroid/app/PendingIntent;

.field f:Landroid/content/Context;

.field g:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private j:Lcom/sec/android/app/samsungapps/hp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->d:Landroid/telephony/SmsManager;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->e:Landroid/app/PendingIntent;

    .line 25
    const-string v0, "ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->h:Ljava/lang/String;

    .line 26
    const-string v0, "android.provider.Telephony.SMS_RECEIVED"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->i:Ljava/lang/String;

    .line 100
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->g:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SMS;->f:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/SMS;->a:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->b:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/SMS;->c:Ljava/lang/String;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SMS;)Lcom/sec/android/app/samsungapps/hp;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->j:Lcom/sec/android/app/samsungapps/hp;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/SMS;)Lcom/sec/android/app/samsungapps/hp;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->j:Lcom/sec/android/app/samsungapps/hp;

    return-object v0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/SMS$SMSObserver;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->g:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 104
    return-void
.end method

.method protected notifySendResult(Z)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->g:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/SMS$SMSObserver;

    .line 97
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/SMS$SMSObserver;->sendSuccess(Z)V

    goto :goto_0

    .line 99
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/SMS$SMSObserver;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->g:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public send()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->f:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "ACTION_MSG_SENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v6, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->e:Landroid/app/PendingIntent;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->e:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->j:Lcom/sec/android/app/samsungapps/hp;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/hp;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hp;-><init>(Lcom/sec/android/app/samsungapps/SMS;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->j:Lcom/sec/android/app/samsungapps/hp;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMS;->j:Lcom/sec/android/app/samsungapps/hp;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "ACTION_MSG_SENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 52
    :cond_0
    :try_start_0
    const-string v0, "PSMS : start smsManager.sendTextMessage"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 56
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->e:Landroid/app/PendingIntent;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SMS;->d:Landroid/telephony/SmsManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SMS;->a:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    const-string v0, "PSMS : fail smsManager.sendTextMessage"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/SMS;->notifySendResult(Z)V

    goto :goto_0
.end method
