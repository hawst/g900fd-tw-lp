.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_EMAIL_ACCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

.field public static final enum CHECK_INSTALL_AND_VERSION:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

.field public static final enum IS_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

.field public static final enum REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    const-string v1, "IS_LOGED_IN"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->IS_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    const-string v1, "CHECK_INSTALL_AND_VERSION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->CHECK_INSTALL_AND_VERSION:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    const-string v1, "NOTIFY_SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    const-string v1, "CHECK_EMAIL_ACCOUNT"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->CHECK_EMAIL_ACCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    const-string v1, "REQUEST_LOGIN"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    const-string v1, "NOTIFY_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    .line 10
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->IS_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->CHECK_INSTALL_AND_VERSION:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->CHECK_EMAIL_ACCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    return-object v0
.end method
