.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _MNC:Ljava/lang/String;

.field private _defaultMNC:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->_MNC:Ljava/lang/String;

    .line 10
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->_defaultMNC:Z

    .line 11
    return-void
.end method


# virtual methods
.method public getMNC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->_MNC:Ljava/lang/String;

    return-object v0
.end method

.method public isDefaultValue()Z
    .locals 1

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->_defaultMNC:Z

    return v0
.end method
