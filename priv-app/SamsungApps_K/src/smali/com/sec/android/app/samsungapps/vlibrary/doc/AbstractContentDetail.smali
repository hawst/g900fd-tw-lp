.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;


# instance fields
.field public GUID:Ljava/lang/String;

.field public IAPSupportYn:Ljava/lang/String;

.field private _ICategoryList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

.field private _ISellerList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

.field private _SINAContentYn:Z

.field public alreadyPurchased:Z

.field public averageRating:I

.field public bAppMasterInfo:Ljava/lang/String;

.field public bAppType:Ljava/lang/String;

.field public bGearVersion:Ljava/lang/String;

.field public categoryID:Ljava/lang/String;

.field public categoryID2:Ljava/lang/String;

.field public categoryName:Ljava/lang/String;

.field public categoryName2:Ljava/lang/String;

.field public categoryPath:Ljava/lang/String;

.field public categoryPath2:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public contentURL:Ljava/lang/String;

.field public contentsSize:J

.field public currencyUnit:Ljava/lang/String;

.field public discountFlag:Z

.field public likeCount:I

.field public linkProductStore:Ljava/lang/String;

.field public linkProductYn:Z

.field public loadType:Ljava/lang/String;

.field public mobileWalletSupportYn:Ljava/lang/String;

.field public myCmtYn:Ljava/lang/String;

.field public myRatingYn:Ljava/lang/String;

.field public needToLogin:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public platformVersion:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public productImgUrl:Ljava/lang/String;

.field public productName:Ljava/lang/String;

.field public ratingParticipants:I

.field public reducePrice:D

.field public sellerID:Ljava/lang/String;

.field public sellerName:Ljava/lang/String;

.field public sellingPrice:D

.field public userLikeYn:Z

.field public version:Ljava/lang/String;

.field public versionCode:Ljava/lang/String;

.field public wishListId:Ljava/lang/String;

.field public wishListYn:Z


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_SINAContentYn:Z

    .line 87
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_SINAContentYn:Z

    .line 76
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;

    invoke-static {p1, v0, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    const-string v1, "[S_I_N_A]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_SINAContentYn:Z

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    const-string v1, "[S_I_N_A]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 81
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    .line 83
    :cond_0
    return-void
.end method

.method private isStrValid(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 482
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    const/4 v0, 0x1

    .line 485
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public getBAppMasterInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->bAppMasterInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getBAppType()Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->bAppType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->bAppType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 451
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;->NotBApp:Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;

    .line 464
    :goto_0
    return-object v0

    .line 452
    :cond_1
    const-string v0, "01"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->bAppType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 454
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;->B_AllInOneType:Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;

    goto :goto_0

    .line 456
    :cond_2
    const-string v0, "02"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->bAppType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 458
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;->B_Master_SlaveType:Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;

    goto :goto_0

    .line 460
    :cond_3
    const-string v0, "03"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->bAppType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 462
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;->B_StandAlone:Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;

    goto :goto_0

    .line 464
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;->NotBApp:Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail$BAppType;

    goto :goto_0
.end method

.method public getCategoryProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_ICategoryList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    return-object v0
.end method

.method public final getConvertedFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->versionCode:Ljava/lang/String;

    .line 93
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 95
    :cond_0
    const-string v0, "0"

    .line 97
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "samsungapps-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->getRealContentSize()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    return-object v0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->loadType:Ljava/lang/String;

    return-object v0
.end method

.method public getRealContentSize()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->contentsSize:J

    return-wide v0
.end method

.method public getSellerProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_ISellerList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    return-object v0
.end method

.method public getVAverageRating()F
    .locals 4

    .prologue
    .line 271
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->averageRating:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public getVCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getVCategoryName()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryName:Ljava/lang/String;

    .line 252
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryPath2:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryName2:Ljava/lang/String;

    .line 261
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 263
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 265
    :cond_0
    return-object v0

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryPath:Ljava/lang/String;

    goto :goto_0

    .line 258
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryPath2:Ljava/lang/String;

    goto :goto_1
.end method

.method public getVContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->contentType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 163
    const-string v0, "application"

    .line 166
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->contentType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVContentURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->contentURL:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 405
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->contentURL:Ljava/lang/String;

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->contentURL:Ljava/lang/String;

    return-object v0
.end method

.method public getVCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->currencyUnit:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 297
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->currencyUnit:Ljava/lang/String;

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->currencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getVGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->GUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->GUID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVLikeCount()I
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->likeCount:I

    return v0
.end method

.method public getVLinkStoreName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->linkProductStore:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 350
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->linkProductStore:Ljava/lang/String;

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->linkProductStore:Ljava/lang/String;

    return-object v0
.end method

.method public getVProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productID:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 195
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productID:Ljava/lang/String;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getVProductImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productImgUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 205
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productImgUrl:Ljava/lang/String;

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getVProductName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 174
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    const-string v1, "[S_I_N_A]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    const-string v1, "[S_I_N_A]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 179
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    const-string v1, "_terminated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    const-string v1, "_terminated"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 184
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    return-object v0
.end method

.method public getVRatingParticipants()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->ratingParticipants:I

    return v0
.end method

.method public getVReducePrice()D
    .locals 2

    .prologue
    .line 289
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->reducePrice:D

    return-wide v0
.end method

.method public getVSellerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerID:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 225
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerID:Ljava/lang/String;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerID:Ljava/lang/String;

    return-object v0
.end method

.method public getVSellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 215
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerName:Ljava/lang/String;

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerName:Ljava/lang/String;

    return-object v0
.end method

.method public getVSellingPrice()D
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellingPrice:D

    return-wide v0
.end method

.method public hasLike()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->userLikeYn:Z

    return v0
.end method

.method public hasMyComment()Z
    .locals 2

    .prologue
    .line 437
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->myCmtYn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasMyRating()Z
    .locals 2

    .prologue
    .line 431
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->myRatingYn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasSellerID()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellerID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWishList()Z
    .locals 1

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->wishListYn:Z

    return v0
.end method

.method public hasWishListID()Z
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->wishListId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isAlreadyPurchased()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->alreadyPurchased:Z

    return v0
.end method

.method public isDiscount()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->discountFlag:Z

    return v0
.end method

.method public isLinkApp()Z
    .locals 1

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->linkProductYn:Z

    return v0
.end method

.method public isMobileWalletSupportYn()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 374
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->mobileWalletSupportYn:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Y"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->mobileWalletSupportYn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 381
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPackageInstalled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 419
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 420
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPrePostApp()Z
    .locals 2

    .prologue
    .line 388
    const-string v0, "2"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->loadType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    const/4 v0, 0x1

    .line 392
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSINAContent()Z
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_SINAContentYn:Z

    return v0
.end method

.method public isUpdatable()Z
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetContent()Z
    .locals 2

    .prologue
    .line 425
    const-string v0, "widget"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->getVContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public needToLoginToDownload()Z
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->needToLogin:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->isStrValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->needToLogin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    const/4 v0, 0x0

    .line 493
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAlreadyPurchased()V
    .locals 0

    .prologue
    .line 359
    return-void
.end method

.method public setCategoryProductList(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;)V
    .locals 5

    .prologue
    .line 139
    const/4 v0, 0x0

    .line 140
    if-eqz p1, :cond_2

    .line 142
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;-><init>()V

    .line 143
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->size()I

    move-result v3

    .line 144
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->getVGUID()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;->getVGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-interface {p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 153
    :cond_2
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_ICategoryList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    .line 154
    return-void
.end method

.method public setSellerProductList(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->_ISellerList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    .line 135
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 119
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 120
    return-void
.end method
