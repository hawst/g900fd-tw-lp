.class final Lcom/sec/android/app/samsungapps/api/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;

.field final synthetic b:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/api/c;->b:Lcom/sec/android/app/samsungapps/api/SamsungAppsInstallService;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/api/c;->a:Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/api/c;->a:Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;

    const/16 v1, 0x3e7

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/api/aidl/IInstallResultCallback;->onInstallFailed(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAppsInstallService::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
