.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field mRecommendKeywords:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->mRecommendKeywords:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public clearRecommendKeyword()V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->mRecommendKeywords:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 21
    return-void
.end method

.method public getRecommendKeywordList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->mRecommendKeywords:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onUpdateRecommendKeyword(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 24
    monitor-enter p0

    .line 26
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->clear()V

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->mRecommendKeywords:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 28
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 30
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->mRecommendKeywords:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 32
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->setTotalCount(I)V

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->setNeverLoaded(Z)V

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->setCompleted(Z)V

    .line 36
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
