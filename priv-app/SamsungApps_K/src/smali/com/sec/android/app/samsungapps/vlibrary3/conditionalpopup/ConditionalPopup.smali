.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field protected _Context:Landroid/content/Context;

.field private a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

.field private c:Landroid/os/CountDownTimer;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a:Landroid/os/Handler;

    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->b:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->_Context:Landroid/content/Context;

    .line 22
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 33
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    .line 135
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->b:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public invokeCompleted()V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->INVOKE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    .line 114
    return-void
.end method

.method protected abstract matchCondition()Z
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;)V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 48
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->matchCondition()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    goto :goto_0

    .line 54
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->DOESNT_MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    goto :goto_0

    .line 58
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->_Context:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->onInvokePopup(Landroid/content/Context;)V

    goto :goto_0

    .line 61
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->c:Landroid/os/CountDownTimer;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->c:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 77
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->c:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->c:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->c:Landroid/os/CountDownTimer;

    goto :goto_0

    .line 84
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->d:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->d:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;->onConditionalPopupFail()V

    goto :goto_0

    .line 87
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->d:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->d:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;->onConditionalPopupSuccess()V

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;)V

    return-void
.end method

.method protected abstract onInvokePopup(Landroid/content/Context;)V
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->d:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;

    .line 95
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->b:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    .line 38
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;)V

    return-void
.end method

.method public userAgree(Z)V
    .locals 1

    .prologue
    .line 118
    if-eqz p1, :cond_0

    .line 120
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->a(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)V

    goto :goto_0
.end method
