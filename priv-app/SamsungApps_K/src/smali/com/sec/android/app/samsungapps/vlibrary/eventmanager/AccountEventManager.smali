.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;
.super Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/BaseEventManager;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/BaseEventManager;-><init>()V

    return-void
.end method


# virtual methods
.method protected getMyEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    return-object v0
.end method

.method public loginSuccess()V
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->LogedIn()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 10
    return-void
.end method

.method public logoutFailed()V
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->LogoutFailed()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 20
    return-void
.end method

.method public logoutSuccess()V
    .locals 1

    .prologue
    .line 14
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->LogoutSuccess()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 15
    return-void
.end method

.method public requestAutoLogin(Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V
    .locals 1

    .prologue
    .line 34
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->requestAutoLogin(Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->notifyResponseRequiredEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 35
    return-void
.end method

.method public requestSignUp(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V
    .locals 1

    .prologue
    .line 39
    invoke-static {p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->requestSignUp(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->notifyResponseRequiredEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 40
    return-void
.end method
