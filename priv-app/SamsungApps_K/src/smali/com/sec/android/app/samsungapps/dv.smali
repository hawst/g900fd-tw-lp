.class final Lcom/sec/android/app/samsungapps/dv;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

.field protected c:I

.field protected d:Landroid/widget/TextView;

.field protected e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/dv;->a:Landroid/content/Context;

    .line 384
    const v0, 0x7f0c01e1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/dv;->d:Landroid/widget/TextView;

    .line 385
    const v0, 0x7f0c01e2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/dv;->e:Landroid/widget/TextView;

    .line 386
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 389
    iput p1, p0, Lcom/sec/android/app/samsungapps/dv;->c:I

    .line 390
    return-void
.end method

.method public final a(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V
    .locals 5

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/dv;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/dv;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/dv;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/dv;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/dv;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/dv;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v3

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    return-void
.end method
