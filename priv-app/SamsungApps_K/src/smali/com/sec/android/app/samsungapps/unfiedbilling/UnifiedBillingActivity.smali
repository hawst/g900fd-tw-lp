.class public Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;
.super Landroid/app/Activity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

.field protected mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    return-void
.end method

.method public static getUPServerURL(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 273
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    :cond_1
    const-string v0, "https://cn-mop.samsungosp.com"

    .line 296
    :goto_0
    return-object v0

    .line 277
    :cond_2
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0

    .line 279
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 280
    const-string v1, "PRD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 282
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0

    .line 284
    :cond_4
    const-string v1, "STG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 286
    const-string v0, "https://stg-api.samsungosp.com"

    goto :goto_0

    .line 288
    :cond_5
    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 290
    const-string v0, "https://cn-mop.samsungosp.com"

    goto :goto_0

    .line 292
    :cond_6
    const-string v1, "PRT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 294
    const-string v0, "https://stg-mop.samsungosp.com"

    goto :goto_0

    .line 296
    :cond_7
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0
.end method


# virtual methods
.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    .line 441
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGearPlatformVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 403
    packed-switch p2, :pswitch_data_0

    .line 431
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->finish()V

    .line 433
    return-void

    .line 407
    :pswitch_1
    const-string v0, "PAYMENT_RECEITE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    const-string v1, "SIGNATURE"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 418
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 422
    :pswitch_3
    const-string v0, "ERROR_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 423
    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 403
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->finish()V

    .line 266
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->finish()V

    goto :goto_0

    .line 68
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->getInitResult()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    move-result-object v1

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    .line 73
    new-instance v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    invoke-direct {v2}, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;-><init>()V

    .line 74
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    .line 75
    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->storeReqeustID:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    .line 83
    new-instance v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    invoke-direct {v3}, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;-><init>()V

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    .line 84
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPrice()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->totalAmount:Ljava/lang/String;

    .line 85
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    const-string v4, "0"

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->tax:Ljava/lang/String;

    .line 86
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    const-string v4, "N"

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->taxIncluded:Ljava/lang/String;

    .line 88
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->currency:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->currency:Ljava/lang/String;

    .line 89
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    new-array v4, v7, [Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    .line 90
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    new-instance v4, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    invoke-direct {v4}, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;-><init>()V

    aput-object v4, v3, v6

    .line 91
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->productID:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productID:Ljava/lang/String;

    .line 92
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    .line 93
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPrice()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->amount:Ljava/lang/String;

    .line 94
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    const-string v4, "0"

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->tax:Ljava/lang/String;

    .line 95
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPSMSPrice()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->phonebillAmount:Ljava/lang/String;

    .line 111
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 113
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    new-instance v4, Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;

    invoke-direct {v4}, Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;-><init>()V

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->thumbnailImageURL:Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;

    .line 114
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->thumbnailImageURL:Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;->mdpi:Ljava/lang/String;

    .line 115
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->thumbnailImageURL:Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;->xhdpi:Ljava/lang/String;

    .line 116
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->thumbnailImageURL:Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    iput-object v0, v3, Lcom/samsungosp/billingup/client/requestparam/ThumbnailImageURL;->xxhdpi:Ljava/lang/String;

    .line 124
    :cond_1
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/UserInfo;-><init>()V

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 125
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->userId:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    .line 126
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->userName:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userName:Ljava/lang/String;

    .line 127
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->userEmail:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    .line 128
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->authAppID:Ljava/lang/String;

    .line 129
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    sget-object v3, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->_Token:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    .line 136
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;-><init>()V

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    .line 137
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->countryCode:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    .line 138
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    new-instance v3, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    invoke-direct {v3}, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;-><init>()V

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    .line 139
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->addGiftCardnCouponURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    .line 140
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->getGiftCardnCouponURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    .line 141
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->requestOrderURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    .line 142
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->notiPaymentResultURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    .line 143
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->retrieveTaxURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    .line 150
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;-><init>()V

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 151
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceUID:Ljava/lang/String;

    .line 152
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    const-string v3, "M|640|480"

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    .line 154
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getFakeModel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getFakeModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    .line 158
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getFakeModel()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    .line 175
    :cond_2
    :goto_1
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    .line 176
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    .line 177
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    .line 189
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;-><init>()V

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    .line 191
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    .line 192
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getBillingServerType()Ljava/lang/String;

    move-result-object v3

    .line 195
    iget-object v4, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 196
    :cond_3
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    const-string v4, "STG2"

    iput-object v4, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 197
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    const-string v4, "https://stg-api.samsungosp.com"

    iput-object v4, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    .line 199
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 201
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 202
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-static {p0, v3}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getUPServerURL(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    .line 224
    :cond_5
    :goto_2
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;-><init>()V

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    .line 225
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->paymentType:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;->paymentType:Ljava/lang/String;

    .line 226
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->exceptionPaymentMethods:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;->exceptionPaymentMethods:Ljava/lang/String;

    .line 228
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->offerType:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;->offerType:Ljava/lang/String;

    .line 229
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    const-string v3, "N"

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    .line 236
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;-><init>()V

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    .line 237
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->timeStamp:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->timeStamp:Ljava/lang/String;

    .line 238
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->baseString:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->baseString:Ljava/lang/String;

    .line 239
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->signature:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->signature:Ljava/lang/String;

    .line 247
    const-string v0, "+00:00"

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    .line 252
    :try_start_1
    invoke-static {v2, p0}, Lcom/samsungosp/billingup/client/UnifiedPayment;->checkout(Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;Landroid/content/Context;)Landroid/content/Intent;
    :try_end_1
    .catch Lcom/samsungosp/billingup/client/util/UnifiedPaymentException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 263
    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 264
    const v0, 0x7f050007

    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->overridePendingTransition(II)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->invokeCompleted()V

    goto/16 :goto_0

    .line 162
    :cond_6
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    .line 164
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 165
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_KNOX"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_1

    .line 172
    :catch_1
    move-exception v3

    goto/16 :goto_1

    .line 204
    :cond_7
    iget-object v0, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 206
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    iget-object v3, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 208
    iget-object v0, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    const-string v3, "PRD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 210
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v4

    if-nez v4, :cond_8

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const-string v0, "https://cn-mop.samsungosp.com"

    :goto_3
    iput-object v0, v3, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    goto/16 :goto_2

    :cond_9
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_3

    .line 213
    :cond_a
    iget-object v0, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    const-string v3, "STG2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 214
    iget-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    const-string v3, "https://stg-api.samsungosp.com"

    iput-object v3, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    goto/16 :goto_2

    .line 260
    :catch_2
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->finish()V

    goto/16 :goto_0

    :catch_3
    move-exception v3

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onDestroy()V

    .line 398
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 399
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 389
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 390
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 449
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 450
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 458
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 459
    return-void
.end method
