.class public Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker;


# instance fields
.field private _TurkeyConditionalPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private bNeedRetry:Z

.field private mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

.field private mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->_TurkeyConditionalPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->checkNetDisconnection(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Z)Z
    .locals 0

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->bNeedRetry:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->notifyFailed()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->checkChina(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->notifySuccess()V

    return-void
.end method

.method private checkAirplaneMode(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 100
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isWIFIConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->isAirplaneMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;->showAirplaneMode(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IConfirm;)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->checkTurkey(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private checkChina(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V

    .line 137
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;)V

    .line 149
    return-void
.end method

.method private checkNetDisconnection(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isConnectedDataNetwork()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;)V

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;->showNetworkDisconnectedPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IRetryObserver;)V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->checkAirplaneMode(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private checkTurkey(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->_TurkeyConditionalPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/h;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->_TurkeyConditionalPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    .line 132
    return-void
.end method

.method private isAirplaneMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 152
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isAirplaneMode()Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private networkErrorCheckOnlyForJB(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;Landroid/content/Context;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;)V

    .line 70
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->execute(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method private notifyFailed()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->bNeedRetry:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;->onNetworkCheckFailed(Z)V

    .line 40
    :cond_0
    return-void
.end method

.method private notifySuccess()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;->onNetworkCheckSuccess()V

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;)V
    .locals 1

    .prologue
    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->bNeedRetry:Z

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;->checkNetDisconnection(Landroid/content/Context;)V

    .line 32
    return-void
.end method
