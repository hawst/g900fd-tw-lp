.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;


# instance fields
.field a:Landroid/os/Handler;

.field b:Ljava/lang/Runnable;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a:Landroid/os/Handler;

    .line 77
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    .line 125
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/aa;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/aa;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->b:Ljava/lang/Runnable;

    .line 70
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->d:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->e:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->setState(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;Z)V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ab;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ab;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->setState(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;

    return-object v0
.end method


# virtual methods
.method protected entry()V
    .locals 3

    .prologue
    .line 29
    const-string v1, "SigCheckerForInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "entry:"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ac;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 43
    :goto_0
    :pswitch_0
    return-void

    .line 32
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->b:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/y;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/y;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 40
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/x;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/x;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected exit()V
    .locals 3

    .prologue
    .line 24
    const-string v1, "SigCheckerForInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "exit:"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    return-void
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->getIdleState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    move-result-object v0

    return-object v0
.end method

.method public install()V
    .locals 3

    .prologue
    .line 152
    const-string v1, "SigCheckerForInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "install:"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ac;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 158
    :goto_0
    return-void

    .line 155
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;->CHECK_SIGNATURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    .line 170
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->userCancel()V

    .line 165
    :cond_0
    return-void
.end method
