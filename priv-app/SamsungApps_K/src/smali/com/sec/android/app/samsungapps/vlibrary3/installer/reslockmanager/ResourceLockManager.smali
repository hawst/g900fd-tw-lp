.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->d:Landroid/os/Handler;

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->d:Landroid/os/Handler;

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;

    .line 15
    return-void
.end method


# virtual methods
.method public dequeue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V
    .locals 2

    .prologue
    .line 45
    monitor-enter p0

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    if-ne p1, v0, :cond_2

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;->onResourceFreed()V

    .line 57
    :cond_0
    monitor-exit p0

    .line 69
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;->onRun(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;)V

    .line 69
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public enqueue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V
    .locals 1

    .prologue
    .line 24
    monitor-enter p0

    .line 26
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;->onResourceAcquired()V

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;->onRun(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;)V

    .line 40
    :goto_0
    monitor-exit p0

    return-void

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
