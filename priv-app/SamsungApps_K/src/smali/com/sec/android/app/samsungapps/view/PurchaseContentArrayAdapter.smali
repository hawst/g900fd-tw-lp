.class public Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field private g:Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x63

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;

    .line 35
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 37
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    .line 38
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 168
    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 170
    :goto_0
    if-ge v2, v3, :cond_0

    .line 172
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    add-int/lit8 v0, v1, 0x1

    .line 170
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->downloadingCount()I

    move-result v0

    .line 178
    if-nez v0, :cond_1

    const/4 v0, 0x1

    if-le v1, v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;->showUpdateAll()V

    .line 186
    :goto_2
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;->hideUpdateAll()V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private static a(Z)V
    .locals 1

    .prologue
    .line 149
    if-eqz p0, :cond_0

    .line 150
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showUpdateAllButton()V

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->hideUpdateAllButton()V

    goto :goto_0
.end method


# virtual methods
.method public cancelAll()V
    .locals 4

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getCount()I

    move-result v0

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 114
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 115
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 118
    sget-object v2, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 112
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 121
    :cond_1
    return-void
.end method

.method public contentListGetCompleted(Z)V
    .locals 0

    .prologue
    .line 161
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->contentListGetCompleted(Z)V

    .line 163
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a()V

    .line 164
    return-void
.end method

.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 10

    .prologue
    .line 229
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->mListType:I

    const/4 v9, 0x0

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;)V

    return-object v0
.end method

.method public downloadingCount()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 125
    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 127
    :goto_0
    if-ge v2, v3, :cond_0

    .line 129
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 130
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 132
    add-int/lit8 v0, v1, 0x1

    .line 127
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 135
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getContentListSize()I
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->isLoading()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-nez v0, :cond_1

    .line 217
    :cond_0
    const/4 v0, -0x1

    .line 220
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    goto :goto_0
.end method

.method protected handleMyTabEvent(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;)V
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/samsungapps/view/as;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;->getMyTabEventType()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 91
    :goto_0
    return-void

    .line 88
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->updateAll()V

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 54
    sget-object v0, Lcom/sec/android/app/samsungapps/view/as;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 61
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 57
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestDataGet(Landroid/content/Context;)V

    goto :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/samsungapps/view/as;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 81
    :goto_0
    return-void

    .line 78
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getMyTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->handleMyTabEvent(Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;)V

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method public onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->notifyDataSetChanged()V

    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a()V

    .line 192
    return-void
.end method

.method public onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->notifyDataSetChanged()V

    .line 197
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a()V

    .line 198
    return-void
.end method

.method public refreshUpdateAllMenu(I)V
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x2

    if-lt p1, v0, :cond_0

    .line 203
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a(Z)V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a(Z)V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->g:Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;

    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 46
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 47
    return-void
.end method

.method public updateAll()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->a(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getCount()I

    move-result v2

    move v1, v0

    .line 98
    :goto_0
    if-ge v1, v2, :cond_1

    .line 100
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;

    invoke-direct {v5}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;-><init>()V

    invoke-virtual {v3, v4, v0, v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->updateContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;)V

    .line 98
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 106
    :cond_1
    return-void
.end method
