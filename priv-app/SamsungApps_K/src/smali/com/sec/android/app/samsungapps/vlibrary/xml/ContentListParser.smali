.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field protected _ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

.field private _IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

.field private _LastIndex:I

.field private _StartPos:I

.field _TemporalContentList:Ljava/util/ArrayList;

.field protected _TotalCount:I

.field private _bCompleted:Z

.field protected _bSuccess:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_TemporalContentList:Ljava/util/ArrayList;

    .line 14
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bSuccess:Z

    .line 15
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_StartPos:I

    .line 16
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_TotalCount:I

    .line 17
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_LastIndex:I

    .line 18
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bCompleted:Z

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 25
    return-void
.end method


# virtual methods
.method protected createContent(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 45
    return-object v0
.end method

.method public getList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    return-object v0
.end method

.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->createContent(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    .line 32
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_IKNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isKNOXApp()Z

    move-result v1

    if-nez v1, :cond_1

    .line 37
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_TemporalContentList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_1
    return-void
.end method

.method public onEndParse()V
    .locals 5

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bSuccess:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_StartPos:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_LastIndex:I

    iget v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_TotalCount:I

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_TemporalContentList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->onUpdateListItem(IIILjava/util/ArrayList;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setNeverLoaded(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_ContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bCompleted:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setCompleted(Z)V

    .line 105
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onEndParse()V

    .line 106
    return-void
.end method

.method protected onPostParseError()V
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bSuccess:Z

    .line 57
    :cond_0
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 61
    const-string v0, "startNum"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 62
    if-ne v0, v3, :cond_4

    .line 64
    iput v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_StartPos:I

    .line 71
    :goto_0
    const-string v0, "totalCount"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 72
    if-eq v0, v2, :cond_0

    .line 74
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_TotalCount:I

    .line 77
    :cond_0
    const-string v1, "endNum"

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 79
    if-eq v1, v2, :cond_1

    .line 81
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_LastIndex:I

    .line 84
    :cond_1
    if-eq v0, v2, :cond_2

    if-ne v1, v2, :cond_5

    .line 86
    :cond_2
    const-string v0, "endOfList"

    invoke-virtual {p1, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bCompleted:Z

    .line 96
    :cond_3
    :goto_1
    return-void

    .line 67
    :cond_4
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_StartPos:I

    goto :goto_0

    .line 92
    :cond_5
    if-ne v0, v1, :cond_3

    .line 94
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;->_bCompleted:Z

    goto :goto_1
.end method
