.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;Z)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getConfirmURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisWebClientResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;->getConfirmURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getLastRequest()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->a:Z

    return v0
.end method

.method public final getOrderID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v0

    const-string v1, "orderID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_InitMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v0

    const-string v1, "paymentID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getV3DToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_IInicisWebClientResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;->getV3dToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isUpoint()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->_UPoint:Z
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;)Z

    move-result v0

    return v0
.end method
