.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;


# instance fields
.field _Context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method public getGroupedPermissionInfoArray(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 120
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 121
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->getPermissionInfoArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    .line 122
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 129
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 135
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 141
    :cond_0
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 142
    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 146
    array-length v7, v6

    move v0, v2

    :goto_2
    if-ge v0, v7, :cond_4

    aget-object v8, v6, v0

    .line 148
    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 150
    const/4 v0, 0x1

    .line 159
    :goto_3
    if-eqz v0, :cond_2

    .line 162
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 163
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 146
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 168
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 169
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 175
    :cond_3
    return-object v3

    :cond_4
    move v0, v2

    goto :goto_3
.end method

.method public getPermissionInfo(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;
    .locals 7

    .prologue
    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PermissionInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 39
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 42
    iget-object v4, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->getPermissionGroupInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionGroupInfo;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PermissionGroupInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PermissionGroupInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    .line 73
    :goto_0
    return-object v0

    .line 62
    :catch_0
    move-exception v0

    const-string v0, "sapps"

    const-string v1, "NameNotFoundException for Permission"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    :goto_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 68
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_1
.end method

.method public getPermissionInfoArray(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    .line 79
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 80
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;

    const-string v0, "\\|\\|"

    invoke-direct {v3, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->getSize()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 86
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;->_Context:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;-><init>(Landroid/content/Context;)V

    .line 94
    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->get(I)Ljava/lang/String;

    move-result-object v4

    .line 95
    if-eqz v4, :cond_0

    .line 96
    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 100
    invoke-interface {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;->getPermissionInfo(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    move-result-object v0

    .line 109
    :goto_1
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->hasPermissionInfo()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 110
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 104
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "android.permission."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 105
    invoke-interface {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;->getPermissionInfo(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    move-result-object v0

    goto :goto_1

    .line 114
    :cond_2
    return-object v2
.end method
