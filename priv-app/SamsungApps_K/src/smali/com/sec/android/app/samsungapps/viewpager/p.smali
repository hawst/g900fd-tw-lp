.class final Lcom/sec/android/app/samsungapps/viewpager/p;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 142
    sub-int v0, p4, p3

    .line 143
    if-lt p2, v0, :cond_0

    if-eqz p4, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->hasMoreItems()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Landroid/widget/AbsListView;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;

    if-nez v0, :cond_0

    .line 151
    add-int v0, p2, p3

    if-ne v0, p4, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->requestMoreData()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Landroid/widget/AbsListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Landroid/widget/AbsListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/p;->a:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setIndexOfFirstItem(I)V

    .line 171
    :cond_0
    return-void
.end method
