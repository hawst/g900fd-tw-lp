.class public Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _bAccount2Installed:Z

.field private _mcc:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_bAccount2Installed:Z

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_mcc:Ljava/lang/String;

    .line 8
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_bAccount2Installed:Z

    .line 9
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_mcc:Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public getPrivacyPolicyURI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_bAccount2Installed:Z

    if-eqz v0, :cond_0

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://static.bada.com/contents/legal/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_mcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/all/pp.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 24
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.samsungapps.com/common/privacy.as?mcc="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_mcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTermnConditionURI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_bAccount2Installed:Z

    if-eqz v0, :cond_0

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://static.bada.com/contents/legal/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_mcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/all/allinone.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 16
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.samsungapps.com/common/term.as?mcc="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/tncpackage/TNCURLGetter;->_mcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
