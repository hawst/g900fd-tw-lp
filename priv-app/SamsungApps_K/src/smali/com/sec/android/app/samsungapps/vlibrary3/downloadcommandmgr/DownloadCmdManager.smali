.class public Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field private static b:Landroid/os/Handler;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

.field private g:Z

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->b:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->d:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 36
    iput-boolean p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->g:Z

    .line 37
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;

    .line 38
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    return-object v0
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)V
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 50
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

    return-object v0
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)V

    .line 144
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    move-result-object v0

    return-object v0
.end method

.method protected isActivityActive(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 77
    if-nez p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 81
    :cond_1
    instance-of v2, p1, Landroid/app/Activity;

    if-eqz v2, :cond_2

    .line 83
    check-cast p1, Landroid/app/Activity;

    .line 84
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 85
    goto :goto_0

    :cond_2
    move v0, v1

    .line 89
    goto :goto_0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;)V
    .locals 5

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 67
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->d:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->isActivityActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;->execute()V

    goto :goto_0

    .line 70
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->d:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->isActivityActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getNormalPrice()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->add(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;->createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;->execute()V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;->ONDESTROY:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)V

    .line 149
    return-void
.end method

.method public onPrecheckResult(Z)V
    .locals 1

    .prologue
    .line 152
    if-eqz p1, :cond_0

    .line 154
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;->PRECHECK_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;->PRECHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)V

    goto :goto_0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;

    .line 170
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    .line 55
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;)V

    return-void
.end method
