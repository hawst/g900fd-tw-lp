.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _CreditCardRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;

.field _ICardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;->_CreditCardRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;->_ICardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;Z)V
    .locals 0

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;->_ICardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;->hasRegisteredCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 37
    :goto_0
    return-void

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;->_CreditCardRegisterCmd:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardValidator;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
