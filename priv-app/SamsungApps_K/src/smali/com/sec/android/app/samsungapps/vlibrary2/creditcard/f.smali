.class final Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 29
    if-eqz p2, :cond_1

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_CreditCardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;->setCreditCardInfo(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;)V

    .line 42
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;->endLoading()V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;Z)V

    .line 44
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;->clearCreditCardInfo()V

    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->_ILoadCreditCardInfoData:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/LoadCreditCardInfo$ILoadCreditCardInfoData;->clearCreditCardInfo()V

    goto :goto_0
.end method
