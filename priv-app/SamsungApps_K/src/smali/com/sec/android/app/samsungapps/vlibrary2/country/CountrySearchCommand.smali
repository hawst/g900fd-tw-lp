.class public Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private _ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field mCountrySearchCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private mNetErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 196
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mCountrySearchCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mNetErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    .line 29
    return-void
.end method

.method private askCLPassword()V
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->checkQAPassword()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 149
    return-void
.end method

.method private askQAPassword()V
    .locals 3

    .prologue
    .line 117
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const-string v1, "000"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastMCC(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->checkQAPassword()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 132
    return-void
.end method

.method private clearCache()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->clearCache()V

    .line 114
    return-void
.end method

.method private isCountryListSearchMode()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->isCountryListSearchMode()Z

    move-result v0

    return v0
.end method

.method private isTestMode()Z
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->isTestMode()Z

    move-result v0

    return v0
.end method

.method private requestCountryList()V
    .locals 3

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->selectCountryList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 266
    return-void
.end method

.method private requestDisclaimerCheck()V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->validateDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 176
    return-void
.end method

.method private requestGEOIPCSearch()V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->requestCountrySearchEx()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mCountrySearchCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mCountrySearchCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 230
    return-void
.end method

.method private requestMCCCountrySearch()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->requestCountrySearch()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 110
    return-void
.end method

.method private sendFailSignal()V
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->onFinalResult(Z)V

    .line 234
    return-void
.end method

.method private sendGeoIPSearchFailSignal()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->showGeoIPFailed(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V

    .line 249
    return-void
.end method

.method private sendSuccessSignal()V
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyCountryDecided()V

    .line 158
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->onFinalResult(Z)V

    .line 159
    return-void
.end method

.method private showChinaNetworkPopup()V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mNetErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/ChinaNetworkWarningChecker$IChinaNetworkWarningCheckerObserver;)V

    .line 194
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->countrySearchRequired()Z

    move-result v0

    if-nez v0, :cond_2

    .line 56
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_NOT_REQ:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    goto :goto_0

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_ICountryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;->isMCCAvailable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    goto :goto_0

    .line 64
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->isCountryListSearchMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 66
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    goto :goto_0

    .line 69
    :cond_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isGeoIpBasedCountrySearch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCX_GEOIPO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    goto :goto_0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;)V
    .locals 2

    .prologue
    .line 270
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/i;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 304
    :goto_0
    return-void

    .line 273
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->askQAPassword()V

    goto :goto_0

    .line 276
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->clearCache()V

    goto :goto_0

    .line 279
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->requestDisclaimerCheck()V

    goto :goto_0

    .line 282
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->requestGEOIPCSearch()V

    goto :goto_0

    .line 285
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->requestMCCCountrySearch()V

    goto :goto_0

    .line 288
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendGeoIPSearchFailSignal()V

    goto :goto_0

    .line 291
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendFailSignal()V

    goto :goto_0

    .line 294
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendSuccessSignal()V

    goto :goto_0

    .line 297
    :pswitch_8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->askCLPassword()V

    goto :goto_0

    .line 300
    :pswitch_9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->requestCountryList()V

    goto :goto_0

    .line 303
    :pswitch_a
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->showChinaNetworkPopup()V

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;)V

    return-void
.end method

.method protected sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)Z

    .line 44
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 154
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;)V

    return-void
.end method
