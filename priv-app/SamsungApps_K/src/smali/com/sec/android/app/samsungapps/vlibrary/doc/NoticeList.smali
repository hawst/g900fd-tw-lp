.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# instance fields
.field private bCompleted:Z

.field bLoading:Z

.field private bNeverLoaded:Z

.field globalCount:I

.field private mEndPosition:I

.field mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

.field private mStartPosition:I

.field private mTotalCount:I

.field public noticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->noticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 20
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mStartPosition:I

    .line 21
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mEndPosition:I

    .line 22
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bNeverLoaded:Z

    .line 23
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bCompleted:Z

    .line 24
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mTotalCount:I

    .line 25
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bLoading:Z

    .line 76
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->globalCount:I

    .line 127
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->noticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 31
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 132
    return-void
.end method

.method public findNotice(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;

    .line 41
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->getNoticeID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 45
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEndPosition()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mEndPosition:I

    return v0
.end method

.method public getNoticeContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->noticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    return-object v0
.end method

.method public getStartPosition()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mStartPosition:I

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mTotalCount:I

    return v0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bCompleted:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bLoading:Z

    return v0
.end method

.method public isNeverLoaded()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bNeverLoaded:Z

    return v0
.end method

.method public loadMoreNoticeList()V
    .locals 3

    .prologue
    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bLoading:Z

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->notifyMoreNoticeLoading()V

    .line 107
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->noticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/aa;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/aa;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->noticeList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 115
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 117
    return-void
.end method

.method public loadNoticeList()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->isNeverLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->notifyLoadCompleted(Z)V

    .line 101
    :goto_0
    return-void

    .line 86
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->globalCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->globalCount:I

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bLoading:Z

    .line 88
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->noticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/z;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/z;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->noticeList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 99
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method protected notifyLoadCompleted(Z)V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;

    .line 141
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;->noticeLoadingCompleted(Z)V

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method

.method protected notifyMoreNoticeLoading()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;

    .line 148
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;->moreNoticeLoading()V

    goto :goto_0

    .line 150
    :cond_0
    return-void
.end method

.method protected notifyMoreNoticeLoadingCompleted(Z)V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;

    .line 154
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;->moreNoticeLoadingCompleted(Z)V

    goto :goto_0

    .line 156
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 137
    return-void
.end method

.method public setCompleted(Z)V
    .locals 0

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bCompleted:Z

    .line 162
    return-void
.end method

.method public setEndPosition(I)V
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mEndPosition:I

    .line 62
    return-void
.end method

.method public setNeverLoaded(Z)V
    .locals 0

    .prologue
    .line 176
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->bNeverLoaded:Z

    .line 177
    return-void
.end method

.method public setStartPosition(I)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mStartPosition:I

    .line 54
    return-void
.end method

.method public setTotalCount(I)V
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->mTotalCount:I

    .line 70
    return-void
.end method
