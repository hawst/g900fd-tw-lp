.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 25
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public closeMap()V
    .locals 4

    .prologue
    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;-><init>()V

    .line 18
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-class v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 19
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V

    .line 20
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method
