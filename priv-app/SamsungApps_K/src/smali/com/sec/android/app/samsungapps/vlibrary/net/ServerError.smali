.class public Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mErrorcode:Ljava/lang/String;

.field mErrormessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrorcode:Ljava/lang/String;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrormessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrorcode:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrormessage:Ljava/lang/String;

    return-object v0
.end method

.method public isError()Z
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrorcode:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    .line 26
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setErrorCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 7
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrorcode:Ljava/lang/String;

    .line 8
    return-void
.end method

.method public setErrorMsg(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->mErrormessage:Ljava/lang/String;

    .line 11
    return-void
.end method
