.class public final Lcom/sec/android/app/samsungapps/uiutil/UiUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 367
    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 368
    const-string v2, "fr"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    const/4 v0, 0x1

    .line 385
    :cond_0
    :goto_0
    return v0

    .line 379
    :catch_0
    move-exception v1

    const-string v1, "UiUtil::isFrench()::Locale information is empty or not correct"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :catch_1
    move-exception v1

    .line 384
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UiUtil::isFrench()::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static buttonTextSizeResize(Landroid/content/Context;Landroid/widget/TextView;I)I
    .locals 6

    .prologue
    .line 734
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v2, v0

    .line 735
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 738
    if-nez p2, :cond_0

    .line 739
    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 744
    :goto_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    add-int/2addr v1, v4

    int-to-float v4, v1

    .line 745
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 746
    sub-float/2addr v0, v4

    move v5, v1

    move v1, v0

    move v0, v5

    .line 748
    :goto_1
    cmpg-float v4, v1, v0

    if-gez v4, :cond_2

    .line 749
    add-int/lit8 v2, v2, -0x1

    .line 750
    const/16 v4, 0xa

    if-le v2, v4, :cond_1

    .line 751
    const/4 v0, 0x1

    int-to-float v4, v2

    invoke-virtual {p1, v0, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 752
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_1

    .line 741
    :cond_0
    int-to-float v0, p2

    goto :goto_0

    :cond_1
    move v1, v0

    .line 754
    goto :goto_1

    .line 757
    :cond_2
    return v2
.end method

.method public static buttonTextSizeResize(Landroid/content/Context;Landroid/widget/TextView;II)I
    .locals 5

    .prologue
    .line 769
    .line 770
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 773
    if-nez p2, :cond_0

    .line 774
    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 779
    :goto_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    int-to-float v3, v1

    .line 780
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 781
    sub-float/2addr v0, v3

    move v4, v1

    move v1, v0

    move v0, v4

    .line 783
    :goto_1
    cmpg-float v3, v1, v0

    if-gez v3, :cond_2

    .line 784
    add-int/lit8 p3, p3, -0x1

    .line 785
    const/16 v3, 0xa

    if-lt p3, v3, :cond_1

    .line 786
    const/4 v0, 0x1

    int-to-float v3, p3

    invoke-virtual {p1, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 787
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_1

    .line 776
    :cond_0
    int-to-float v0, p2

    goto :goto_0

    :cond_1
    move v1, v0

    .line 789
    goto :goto_1

    .line 792
    :cond_2
    return p3
.end method

.method public static checkActionbarTitleEmpty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 993
    if-eqz p0, :cond_1

    .line 994
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 995
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 996
    :cond_0
    const/4 p0, 0x0

    .line 1000
    :cond_1
    return-object p0
.end method

.method public static convertVersionNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 901
    if-nez p0, :cond_0

    .line 902
    const/4 v0, 0x0

    .line 913
    :goto_0
    return-object v0

    .line 904
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 905
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "."

    invoke-direct {v1, p0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 907
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 908
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 909
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 910
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 913
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected static countChar(Ljava/lang/String;ILjava/lang/String;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 552
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    if-lez p1, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 556
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    move v4, v1

    move v2, v1

    move v0, v1

    .line 558
    :goto_0
    if-ge v4, v5, :cond_3

    .line 560
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    move v3, v1

    .line 561
    :goto_1
    if-ge v3, v6, :cond_4

    .line 562
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v7, v8, :cond_1

    .line 563
    add-int/lit8 v3, v0, 0x1

    .line 564
    const/4 v0, 0x1

    move v9, v0

    move v0, v3

    move v3, v9

    .line 568
    :goto_2
    if-nez v3, :cond_0

    .line 569
    add-int/lit8 v2, v2, 0x1

    .line 572
    :cond_0
    if-lt v2, p1, :cond_2

    .line 578
    :goto_3
    return v0

    .line 561
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 558
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_3
    move v0, v1

    .line 578
    goto :goto_3

    :cond_4
    move v3, v1

    goto :goto_2
.end method

.method public static dialogTitleTextResize(Landroid/content/Context;Landroid/widget/TextView;II)I
    .locals 5

    .prologue
    .line 828
    if-nez p0, :cond_1

    .line 855
    :cond_0
    return p3

    .line 833
    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 836
    if-nez p2, :cond_2

    .line 837
    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 842
    :goto_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    int-to-float v3, v1

    .line 843
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 844
    sub-float/2addr v0, v3

    move v4, v1

    move v1, v0

    move v0, v4

    .line 846
    :goto_1
    cmpg-float v3, v1, v0

    if-gez v3, :cond_0

    .line 847
    add-int/lit8 p3, p3, -0x1

    .line 848
    const/16 v3, 0x12

    if-le p3, v3, :cond_3

    .line 849
    const/4 v0, 0x1

    int-to-float v3, p3

    invoke-virtual {p1, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 850
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_1

    .line 839
    :cond_2
    int-to-float v0, p2

    goto :goto_0

    :cond_3
    move v1, v0

    .line 852
    goto :goto_1
.end method

.method protected static endOf(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 490
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    if-ltz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 493
    invoke-static {p2, p3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->removeChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 495
    if-eqz v1, :cond_1

    .line 496
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, p1

    .line 498
    if-eqz p3, :cond_0

    .line 499
    invoke-virtual {p0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v2, v1, p3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->countChar(Ljava/lang/String;ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 506
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static forceAssertion()V
    .locals 1

    .prologue
    .line 917
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static forceAssertion(Z)V
    .locals 0

    .prologue
    .line 654
    if-eqz p0, :cond_0

    .line 655
    invoke-static {}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->forceAssertion()V

    .line 657
    :cond_0
    return-void
.end method

.method public static formatMBSize(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 311
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 324
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    const-string v3, "Ko"

    .line 333
    :goto_1
    div-double v1, v0, v4

    .line 334
    cmpl-double v0, v1, v4

    if-ltz v0, :cond_3

    .line 336
    invoke-static {}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    const-string v0, "Mo"

    .line 344
    :goto_2
    div-double/2addr v1, v4

    move-wide v7, v1

    move-object v2, v0

    move-wide v0, v7

    .line 349
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "%.02f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 351
    const-string v0, "."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    .line 352
    :goto_4
    if-lez v0, :cond_2

    .line 353
    const/16 v1, 0x2c

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 354
    add-int/lit8 v0, v0, -0x3

    goto :goto_4

    .line 317
    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 330
    :cond_0
    const-string v3, "KB"

    goto :goto_1

    .line 342
    :cond_1
    const-string v0, "MB"

    goto :goto_2

    .line 357
    :cond_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    move-wide v7, v1

    move-wide v0, v7

    move-object v2, v3

    goto :goto_3
.end method

.method public static getAlertDialogButtonOrder(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 81
    if-nez p0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    .line 93
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 94
    :try_start_1
    const-class v2, Lcom/android/internal/R$layout;

    const-string v4, "tw_alert_dialog_holo"

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 104
    :goto_1
    const/4 v4, 0x0

    :try_start_2
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    .line 106
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    .line 107
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v2

    .line 109
    :goto_2
    if-eq v2, v0, :cond_4

    .line 110
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v5

    .line 111
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v5, :cond_3

    .line 112
    const-string v6, "id"

    invoke-interface {v4, v2}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 113
    invoke-interface {v4, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "@"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 116
    const-string v7, "android:id/button1"

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 120
    const-string v7, "android:id/button3"

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v0, v1

    .line 122
    goto :goto_0

    .line 101
    :catch_0
    move-exception v2

    const-class v2, Lcom/android/internal/R$layout;

    const-string v4, "alert_dialog_holo"

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    goto :goto_1

    .line 111
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 126
    :cond_3
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    goto :goto_2

    .line 128
    :catch_1
    move-exception v0

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAlertDialogButtonOrder::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    :cond_4
    move v0, v1

    .line 132
    goto/16 :goto_0
.end method

.method public static getTopIndicatorHeight(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 975
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    move-object v0, p0

    .line 976
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 977
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 978
    iget v0, v1, Landroid/graphics/Rect;->top:I

    .line 980
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 982
    const/4 v2, 0x1

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 984
    if-le v0, v1, :cond_0

    .line 985
    const/4 v0, 0x0

    .line 988
    :cond_0
    return v0
.end method

.method protected static getUniqueWordArray(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 593
    if-eqz p0, :cond_7

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 595
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 596
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 600
    if-eqz p1, :cond_0

    .line 601
    const-string v0, " "

    const-string v3, ""

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 604
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move v0, v2

    .line 607
    :cond_1
    const-string v4, " "

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 608
    if-ltz v4, :cond_3

    .line 609
    if-le v4, v0, :cond_2

    .line 612
    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 613
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->removeChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 614
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 615
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    :cond_2
    add-int/lit8 v0, v4, 0x1

    .line 619
    if-lt v0, v1, :cond_1

    .line 622
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 623
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 624
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 626
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v5, v2

    .line 627
    :goto_1
    if-ge v5, v7, :cond_8

    .line 628
    if-eq v3, v5, :cond_5

    .line 629
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 630
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v8, -0x1

    if-eq v1, v8, :cond_5

    .line 631
    const/4 v1, 0x1

    .line 636
    :goto_2
    if-nez v1, :cond_4

    .line 637
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 627
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_6
    move-object v0, v4

    .line 643
    :goto_3
    return-object v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    move v1, v2

    goto :goto_2
.end method

.method public static hasSoftNavigationButton(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 137
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    .line 138
    const-string v1, "config_showNavigationBar"

    const-string v2, "bool"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 140
    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_0
.end method

.method protected static highlightExpression(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<font color=\"#00A8FF\">"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 440
    return-object v0
.end method

.method protected static indexOf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 455
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 457
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 460
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->removeChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 461
    invoke-static {v1, p2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->removeChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 462
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 464
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 466
    if-ltz v0, :cond_0

    if-eqz p2, :cond_0

    .line 467
    add-int/lit8 v1, v0, 0x1

    invoke-static {p0, v1, p2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->countChar(Ljava/lang/String;ILjava/lang/String;)I

    move-result v1

    .line 468
    add-int/2addr v0, v1

    .line 474
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static isHardwareMenuAvailable(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 942
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 944
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 946
    :cond_0
    const/4 v0, 0x0

    .line 953
    :goto_0
    return v0

    .line 951
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isKitkatMenuAvailable(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 957
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_1

    .line 959
    new-array v2, v1, [Ljava/lang/Object;

    aput-object p0, v2, v0

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 968
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 965
    goto :goto_0
.end method

.method public static isTalkbackEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 859
    if-nez p0, :cond_0

    .line 897
    :cond_0
    return v0
.end method

.method public static makeStringLineFeed(Landroid/widget/TextView;Ljava/lang/String;F)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1011
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 1012
    invoke-virtual {v2, p1, v5, p2, v6}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    .line 1017
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1021
    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 1025
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1026
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isSpace(C)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isSpace(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1029
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1034
    :cond_0
    invoke-virtual {v2, p1, v5, p2, v6}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    .line 1036
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1039
    :cond_1
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1040
    return-void
.end method

.method public static recursiveRecycle(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 53
    if-nez p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 60
    check-cast v0, Landroid/view/ViewGroup;

    .line 61
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 62
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 63
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->recursiveRecycle(Landroid/view/View;)V

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 66
    :cond_2
    instance-of v1, p0, Landroid/widget/AdapterView;

    if-nez v1, :cond_3

    .line 67
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 71
    :cond_3
    instance-of v0, p0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 72
    check-cast p0, Landroid/widget/ImageView;

    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected static removeChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 520
    if-nez p0, :cond_1

    .line 521
    const/4 p0, 0x0

    .line 537
    :cond_0
    return-object p0

    .line 524
    :cond_1
    if-eqz p1, :cond_0

    .line 527
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    move v0, v1

    .line 528
    :goto_0
    if-ge v0, v2, :cond_0

    .line 529
    const/4 v3, 0x2

    new-array v3, v3, [C

    .line 530
    const/16 v4, 0x5c

    aput-char v4, v3, v1

    .line 531
    const/4 v4, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aput-char v5, v3, v4

    .line 532
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([C)V

    .line 534
    const-string v3, ""

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 528
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected static replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 401
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->highlightExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 425
    :goto_0
    return-object p0

    .line 406
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 407
    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 409
    sget-object v1, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v2, :cond_1

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v2, :cond_1

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v2, :cond_1

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v1, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 413
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->highlightExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 414
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 415
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 416
    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 419
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->highlightExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 423
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static setTitleColor(Landroid/widget/TextView;Z)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1051
    if-eqz p1, :cond_0

    .line 1052
    const v0, -0xf94627

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1056
    :goto_0
    const v0, 0x401d70a4    # 2.46f

    const/high16 v1, 0x51000000

    invoke-virtual {p0, v0, v2, v2, v1}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1057
    return-void

    .line 1054
    :cond_0
    const v0, -0xa0a0b

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public static setTitleTextSizeResize(Landroid/content/Context;Landroid/widget/TextView;I)I
    .locals 3

    .prologue
    .line 803
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v1, v0

    .line 804
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    .line 806
    :goto_0
    if-ge p2, v0, :cond_0

    .line 807
    const/16 v0, 0xa

    if-lt v1, v0, :cond_0

    .line 808
    add-int/lit8 v1, v1, -0x1

    .line 811
    const/4 v0, 0x1

    int-to-float v2, v1

    invoke-virtual {p1, v0, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 812
    invoke-virtual {p1}, Landroid/widget/TextView;->onPreDraw()Z

    .line 813
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    goto :goto_0

    .line 816
    :cond_0
    return v1
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 648
    if-eqz p0, :cond_0

    .line 649
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 651
    :cond_0
    return-void
.end method

.method public static subString(Ljava/lang/String;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 281
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 286
    const-string v1, "\\+"

    const-string v2, "\\\\+"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 287
    const-string v1, "\\("

    const-string v2, "\\\\("

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    const-string v1, "\\)"

    const-string v2, "\\\\)"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    const-string v1, "\\["

    const-string v2, "\\\\["

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    const-string v1, "\\]"

    const-string v2, "\\\\]"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 297
    :goto_0
    return-object p0

    .line 294
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I
    .locals 3

    .prologue
    .line 922
    const/4 v0, 0x0

    .line 923
    if-eqz p1, :cond_1

    .line 924
    const/4 v0, 0x1

    int-to-float v1, p2

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 926
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 928
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    .line 929
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 930
    int-to-float v2, p4

    sub-float v1, v2, v1

    .line 932
    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 938
    :goto_0
    return p3

    :cond_0
    move p3, p2

    .line 935
    goto :goto_0

    :cond_1
    move p3, v0

    goto :goto_0
.end method

.method public static textSizeChanged(Landroid/content/Context;Landroid/widget/TextView;)I
    .locals 7

    .prologue
    .line 667
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v1, v0

    .line 668
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 669
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 671
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    .line 672
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 673
    const/4 v5, 0x1

    const/high16 v6, 0x42c80000    # 100.0f

    invoke-static {v5, v6, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    sub-float/2addr v2, v4

    .line 675
    :goto_0
    cmpg-float v4, v2, v0

    if-gez v4, :cond_1

    .line 676
    add-int/lit8 v1, v1, -0x1

    .line 677
    const/16 v4, 0xb

    if-le v1, v4, :cond_0

    .line 678
    int-to-float v0, v1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 679
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_0

    :cond_0
    move v2, v0

    .line 681
    goto :goto_0

    .line 684
    :cond_1
    return v1
.end method

.method public static textSizeChangedLimit(Landroid/content/Context;Landroid/widget/TextView;I)I
    .locals 6

    .prologue
    .line 696
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 723
    :cond_0
    return p2

    .line 700
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 701
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 703
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 707
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v3

    float-to-int p2, v0

    .line 708
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v3, v0, 0x2

    .line 709
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    .line 710
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 711
    int-to-float v3, v3

    sub-float/2addr v3, v4

    .line 712
    const/4 v5, 0x1

    float-to-int v3, v3

    int-to-float v3, v3

    invoke-static {v5, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    sub-float/2addr v1, v4

    .line 714
    :goto_0
    cmpg-float v3, v1, v0

    if-gez v3, :cond_0

    .line 715
    add-int/lit8 p2, p2, -0x1

    .line 716
    const/16 v3, 0xb

    if-le p2, v3, :cond_2

    .line 717
    int-to-float v0, p2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 718
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_0

    :cond_2
    move v1, v0

    .line 720
    goto :goto_0
.end method

.method public static textToHighlight(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 194
    if-eqz p1, :cond_0

    if-nez p0, :cond_2

    .line 195
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 266
    :cond_1
    :goto_0
    return-object p1

    .line 198
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v1, :cond_1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v1, :cond_1

    .line 202
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 203
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    .line 205
    :try_start_0
    const-string v2, " !@#$%^&*()-_+=|~,.?[]();:\'\""

    .line 210
    if-ltz v1, :cond_3

    if-gtz v0, :cond_4

    .line 212
    :cond_3
    invoke-static {p1, p0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->indexOf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 213
    invoke-static {p1, v1, p0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->endOf(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 216
    :cond_4
    if-ltz v1, :cond_5

    if-gtz v0, :cond_a

    .line 219
    :cond_5
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getUniqueWordArray(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 220
    if-eqz v3, :cond_a

    .line 222
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 223
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 224
    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 225
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    invoke-static {p1, v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->indexOf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 228
    invoke-static {p1, v4, v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->endOf(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 230
    if-ltz v4, :cond_6

    if-lez v0, :cond_6

    .line 231
    new-instance v5, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;

    invoke-direct {v5, v4, v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;-><init>(II)V

    invoke-interface {v1, v5}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 261
    :catch_0
    move-exception v0

    goto :goto_0

    .line 236
    :cond_7
    const-string v0, ""

    .line 237
    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 238
    new-instance v1, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;-><init>(II)V

    move-object v2, v1

    move-object v1, v0

    .line 239
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 240
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;

    .line 241
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getEnd()I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getStart()I

    move-result v5

    if-ge v4, v5, :cond_8

    .line 242
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getEnd()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getStart()I

    move-result v4

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 244
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getStart()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getEnd()I

    move-result v4

    invoke-static {p1, v2, v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->subString(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->highlightExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v0

    .line 246
    goto :goto_2

    .line 247
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->getEnd()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 253
    :cond_a
    if-ltz v1, :cond_1

    if-lez v0, :cond_1

    .line 257
    invoke-static {p1, v1, v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->subString(Ljava/lang/String;II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 264
    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0
.end method
