.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

.field private birthDay:Ljava/lang/String;

.field private birthMonth:Ljava/lang/String;

.field private birthYear:Ljava/lang/String;

.field private mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

.field mEmailID:Ljava/lang/String;

.field mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

.field mPassword:Ljava/lang/String;

.field private mSAChecker:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    .line 353
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthDay:Ljava/lang/String;

    .line 354
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthYear:Ljava/lang/String;

    .line 355
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthMonth:Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mSAChecker:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->load()V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    .line 353
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthDay:Ljava/lang/String;

    .line 354
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthYear:Ljava/lang/String;

    .line 355
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthMonth:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mPassword:Ljava/lang/String;

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->save()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mSAChecker:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->signUp(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    return-void
.end method

.method private save()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeEmailID(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method private signUp(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 132
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/a;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->join(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    .line 146
    return-void
.end method


# virtual methods
.method public _isLogedIn()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public confirmPassword(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 294
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBirthDay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    .line 305
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthDay:Ljava/lang/String;

    goto :goto_0
.end method

.method public getBirthMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    .line 323
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthMonth:Ljava/lang/String;

    goto :goto_0
.end method

.method public getBirthYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    .line 314
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthYear:Ljava/lang/String;

    goto :goto_0
.end method

.method public getEmailID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    return-object v0
.end method

.method public getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    return-object v0
.end method

.method public getNewsLetterReceive()Z
    .locals 1

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->newsLetterReceive()Z

    move-result v0

    .line 362
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getRealAge()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->getAge()I

    move-result v0

    .line 88
    :goto_0
    return v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->getRealAge()I

    move-result v0

    goto :goto_0

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRealAgeInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    .line 370
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCard()Z
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    if-nez v0, :cond_0

    .line 258
    const/4 v0, 0x0

    .line 260
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->hasCardInfo()Z

    move-result v0

    goto :goto_0
.end method

.method public isAdult()Z
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getRealAge()I

    move-result v0

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 96
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoLoginAvailable()Z
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mPassword:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x1

    .line 246
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNameAgeAuthorized()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->ageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeInfo;->isValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    iget-boolean v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->nameAuthYn:Z

    if-nez v1, :cond_0

    .line 113
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected load()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getEmailID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isNameAuthExecuted()Z

    .line 123
    return-void
.end method

.method public login(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Z)V
    .locals 2

    .prologue
    .line 150
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, p0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->login(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 195
    if-eqz p2, :cond_0

    .line 197
    const-string v1, "2000"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 198
    const-string v1, "3001"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 199
    const-string v1, "3002"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 200
    const-string v1, "3003"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 201
    const-string v1, "3004"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 202
    const-string v1, "3005"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 203
    const-string v1, "3006"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 204
    const-string v1, "3007"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 205
    const-string v1, "3008"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 206
    const-string v1, "3009"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 207
    const-string v1, "3010"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 208
    const-string v1, "3011"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 209
    const-string v1, "3012"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 210
    const-string v1, "3013"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 211
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 213
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->notifyLoginProgress()V

    .line 215
    return-void
.end method

.method protected notifyCardRemoveCompleted(Z)V
    .locals 2

    .prologue
    .line 234
    if-eqz p1, :cond_0

    .line 236
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    .line 238
    :cond_0
    return-void
.end method

.method protected notifyLoginProgress()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method protected notifyLoginSuccess(Z)V
    .locals 1

    .prologue
    .line 226
    if-eqz p1, :cond_0

    .line 228
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getAccountEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->loginSuccess()V

    .line 230
    :cond_0
    return-void
.end method

.method public removeCard()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 267
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->notifyCardRemoveCompleted(Z)V

    .line 283
    :goto_0
    return v0

    .line 270
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->resetEasybuySetting(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 282
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 283
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setBirthDay(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    .line 332
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthDay:Ljava/lang/String;

    .line 333
    return-void
.end method

.method public setBirthMonth(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    .line 350
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthMonth:Ljava/lang/String;

    .line 351
    return-void
.end method

.method public setBirthYear(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    .line 341
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->birthYear:Ljava/lang/String;

    .line 342
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setLogedOut()V
    .locals 1

    .prologue
    .line 219
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mEmailID:Ljava/lang/String;

    .line 220
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mPassword:Ljava/lang/String;

    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->save()V

    .line 222
    return-void
.end method

.method public setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mLoginInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    .line 54
    return-void
.end method

.method public setPass(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mPassword:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setReference(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->mSAChecker:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;

    .line 35
    return-void
.end method
