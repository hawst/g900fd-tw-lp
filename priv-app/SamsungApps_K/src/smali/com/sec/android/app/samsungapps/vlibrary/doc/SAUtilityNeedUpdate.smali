.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public GUID:Ljava/lang/String;

.field public contentSize:J

.field public downLoadURI:Ljava/lang/String;

.field public loadType:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPkgFileName:Ljava/lang/String;

.field public upgrade:Z

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 12
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 13
    return-void
.end method


# virtual methods
.method public getContentSize()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->contentSize:J

    return-wide v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->mPkgFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->version:Ljava/lang/String;

    return-object v0
.end method

.method public needUpdate()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->upgrade:Z

    return v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->mName:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setPkgFileName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->mPkgFileName:Ljava/lang/String;

    .line 45
    return-void
.end method
