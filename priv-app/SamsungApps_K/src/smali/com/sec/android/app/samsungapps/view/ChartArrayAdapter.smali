.class public Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# static fields
.field public static final TOTAL_CHART_SIZE_LAND:I = 0xc

.field public static final TOTAL_CHART_SIZE_PORT:I = 0x6


# instance fields
.field protected _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

.field private a:I

.field protected adult_icon:Landroid/widget/ImageView;

.field private b:Landroid/view/LayoutInflater;

.field private c:Z

.field private d:Z

.field private e:Z

.field protected final mContext:Landroid/content/Context;

.field protected mTextViewResourceId:I

.field protected mainLayout:Landroid/view/View;

.field protected webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;IIZ)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a:I

    .line 49
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->b:Landroid/view/LayoutInflater;

    .line 50
    iput p2, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mTextViewResourceId:I

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    .line 52
    iput p5, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a:I

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->c:Z

    return p1
.end method


# virtual methods
.method protected cropCenterBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 280
    if-nez p1, :cond_0

    .line 305
    :goto_0
    return-object v0

    .line 286
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 287
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 290
    int-to-float v3, v1

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 291
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 295
    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 298
    invoke-static {p1, v1, v2, v3, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 300
    :catch_0
    move-exception v1

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ChartArrayAdapter::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected cropScaledCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 203
    if-nez p1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object v0

    .line 209
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 210
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 213
    int-to-float v3, v1

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v5

    float-to-int v3, v3

    .line 214
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 218
    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 221
    invoke-static {p1, v1, v2, v3, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 223
    const/4 v1, 0x1

    invoke-static {v6, p3, p3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 224
    if-eqz v2, :cond_0

    .line 228
    if-nez v2, :cond_2

    move-object v1, v0

    .line 229
    :goto_1
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 230
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    .line 231
    goto :goto_0

    .line 228
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ge v3, p2, :cond_3

    if-ge v1, p3, :cond_3

    move-object v1, v2

    goto :goto_1

    :cond_3
    if-le v3, p2, :cond_7

    sub-int v5, v3, p2

    div-int/lit8 v5, v5, 0x2

    :goto_2
    if-le v1, p3, :cond_4

    sub-int v4, v1, p3

    div-int/lit8 v4, v4, 0x2

    :cond_4
    if-le p2, v3, :cond_5

    move p2, v3

    :cond_5
    if-ge p3, v1, :cond_6

    move p3, v1

    :cond_6
    invoke-static {v2, v5, v4, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 233
    :catch_0
    move-exception v1

    .line 234
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ChartArrayAdapter::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move v5, v4

    goto :goto_2
.end method

.method public findItemPosition(Landroid/view/View;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 190
    const v0, 0x7f0c019b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 193
    :try_start_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 198
    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    return v0

    .line 196
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 57
    .line 58
    if-nez p2, :cond_1

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mTextViewResourceId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    .line 62
    :goto_0
    const v0, 0x7f0c0056

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mainLayout:Landroid/view/View;

    .line 63
    const v0, 0x7f0c0059

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 64
    const v0, 0x7f0c005b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->adult_icon:Landroid/widget/ImageView;

    .line 66
    const v0, 0x7f0c0199

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 70
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    .line 73
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    const v2, 0x7f0c019b

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 76
    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    add-int/lit8 v6, p1, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    const-string v0, "%d"

    new-array v4, v8, [Ljava/lang/Object;

    add-int/lit8 v5, p1, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 80
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a:I

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->setHovering(Landroid/view/View;I)V

    .line 83
    :cond_0
    return-object v3

    :cond_1
    move-object v3, p2

    goto :goto_0
.end method

.method public isHovered()Z
    .locals 1

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->c:Z

    return v0
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 4

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 89
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->showProductImg()V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->showAdultIcon()V

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    const-string v2, "isa_chart_default_small"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setHovering(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/view/j;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/j;-><init>(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 187
    :cond_0
    return-void
.end method

.method public showAdultIcon()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->adult_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultIcon(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->e:Z

    .line 128
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->e:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->adult_icon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->adult_icon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showProductImg()V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/i;-><init>(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setConverter(Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultBlur(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->d:Z

    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->d:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    const-string v2, "isa_chart_default_small"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setBackgroundResource(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->webImage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mainLayout:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    const v3, 0x7f0800d6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
