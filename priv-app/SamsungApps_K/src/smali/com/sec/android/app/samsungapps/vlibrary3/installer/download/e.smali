.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNeedPayment()V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;->onNeedPayment()V

    .line 455
    return-void
.end method

.method public final onPaymentSuccessForDownloadURL()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getDownloadURI()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;->onURLSucceed(Ljava/lang/String;)V

    .line 461
    return-void
.end method

.method public final onURLFailed()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;->onURLFailed()V

    .line 450
    return-void
.end method

.method public final onURLSucceed()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/e;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->getDownloadURI()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;->onURLSucceed(Ljava/lang/String;)V

    .line 445
    return-void
.end method
