.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field operator:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->operator:Ljava/util/ArrayList;

    .line 11
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 12
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 14
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 16
    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 17
    array-length v5, v4

    if-ne v5, v12, :cond_1

    .line 18
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->operator:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    aget-object v7, v4, v1

    aget-object v8, v4, v10

    aget-object v4, v4, v11

    invoke-direct {v6, v7, v8, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_1
    array-length v5, v4

    const/4 v6, 0x4

    if-ne v5, v6, :cond_0

    .line 22
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->operator:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    aget-object v7, v4, v1

    aget-object v8, v4, v10

    aget-object v9, v4, v11

    aget-object v4, v4, v12

    invoke-direct {v6, v7, v8, v9, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 26
    :cond_2
    return-void
.end method


# virtual methods
.method public getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->operator:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->operator:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
