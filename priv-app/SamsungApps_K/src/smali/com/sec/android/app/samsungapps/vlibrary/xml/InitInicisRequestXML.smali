.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 31
    const-string v2, "initInicisMobilePurchase"

    const-string v3, "3180"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 33
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v2

    .line 37
    const-string v3, "productID"

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v0, "imei"

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 39
    const-string v0, "guid"

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 41
    const-string v0, "mcc"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v0, "mnc"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 49
    const-string v0, "reserved01"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 50
    const-string v0, "reserved02"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMSI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 55
    :goto_1
    const-string v0, "reserved03"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "reserved04"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v0, "reserved05"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v0, "rentalTerm"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v1, "upointYn"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->isUPoint()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Y"

    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    const-string v0, "couponIssuedSEQ"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getCouponSeq()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->isPhoneBill()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "microPurchaseYn"

    const-string v1, "Y"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_1
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->isMobileWallet()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 69
    const-string v0, "mobileWalletYn"

    const-string v1, "Y"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :goto_3
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    const-string v0, "giftCardCode"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;->getGiftCardCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_2
    return-void

    .line 44
    :cond_3
    const-string v0, "mcc"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v0, "mnc"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 52
    :cond_4
    const-string v0, "reserved01"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "reserved02"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 59
    :cond_5
    const-string v0, "N"

    goto :goto_2

    .line 73
    :cond_6
    const-string v0, "mobileWalletYn"

    const-string v1, "N"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
