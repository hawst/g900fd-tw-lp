.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field public static final enum INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field public static final enum INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field public static final enum UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field public static final enum WAIT_RESOURCE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "WAIT_RESOURCE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->WAIT_RESOURCE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "INSTALL"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "UPDATE_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    const-string v1, "INSTALL_FAILED_WITH_CODE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    .line 10
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->WAIT_RESOURCE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    return-object v0
.end method
