.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;

    .line 17
    return-void
.end method


# virtual methods
.method public createDeltaSupportedInstaller(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 7

    .prologue
    .line 32
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;->createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v2

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V

    .line 34
    return-object v0
.end method

.method public createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 7

    .prologue
    .line 22
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-direct {v3, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;-><init>(Ljava/io/File;)V

    .line 23
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;

    invoke-direct {v6, p1, p3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    move-object v1, p1

    move-object v2, p3

    move v4, p4

    move v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;ZZLcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;)V

    .line 25
    invoke-virtual {v0, p5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 26
    return-object v0
.end method

.method public createSigProtectedDeltaSupportedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 8

    .prologue
    .line 53
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    move/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;->createSigProtectedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v2

    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V

    .line 55
    invoke-virtual {v0, p7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 56
    return-object v0
.end method

.method public createSigProtectedInstaller(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 7

    .prologue
    .line 42
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;->createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;

    invoke-direct {v1, v2, p2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;)V

    .line 44
    invoke-virtual {v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 45
    return-object v1
.end method

.method public createWGTInAPKInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 7

    .prologue
    .line 72
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-direct {v3, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;-><init>(Ljava/io/File;)V

    .line 73
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceWgtInApkInstallManager;

    invoke-direct {v6, p1, p3, p5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceWgtInApkInstallManager;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    .line 74
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;ZZLcom/sec/android/app/samsungapps/vlibrary3/installer/IBDeviceInstallManager;)V

    .line 76
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;

    invoke-direct {v1, p1, p4, p5, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;)V

    .line 77
    invoke-virtual {v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 78
    return-object v1
.end method

.method public createWGTOnlyInstaller(Landroid/content/Context;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInstaller;-><init>(Landroid/content/Context;Ljava/io/File;)V

    .line 64
    invoke-interface {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 65
    return-object v0
.end method
