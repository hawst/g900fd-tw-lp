.class public Lcom/sec/android/app/samsungapps/CommonActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;


# static fields
.field protected static final REQUEST_CODE_SAC_SIGN_IN:I = 0x2001

.field public static final SAC_INIT_VERSION_CODE:I = 0x2ee1

.field public static final SAC_PKGNAME:Ljava/lang/String; = "com.osp.app.signin"

.field public static mCurActivity:Landroid/app/Activity;

.field public static sActivityCount:I


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field protected mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

.field protected mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field protected mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

.field protected mIsS2I:Z

.field protected mOptionMenu:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

.field protected mTheme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 244
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 85
    iput v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mTheme:I

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mOptionMenu:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 89
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->a:Z

    .line 90
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mIsS2I:Z

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 135
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->b:Z

    .line 1103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->c:Z

    return-void
.end method

.method private static a([F)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 793
    .line 794
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 795
    float-to-int v3, v3

    add-int/2addr v1, v3

    .line 794
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 797
    :cond_0
    return v1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CommonActivity;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/CommonActivity;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->c:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/CommonActivity;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->c:Z

    return v0
.end method

.method public static checkIntentSafe(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1338
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1339
    invoke-virtual {v2, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 1340
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 1342
    :goto_0
    if-eqz v2, :cond_1

    .line 1345
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 1340
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1345
    goto :goto_1
.end method

.method public static commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1371
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;Z)V

    .line 1372
    return-void
.end method

.method public static commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;Z)V
    .locals 3

    .prologue
    .line 1378
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonActivity::commonStartActivity::startActivity="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 1380
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1388
    :goto_0
    return-void

    .line 1384
    :catch_0
    move-exception v0

    .line 1386
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CommonActivity::commonStartActivity::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1492
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1494
    return-object v0
.end method

.method public static handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z
    .locals 2

    .prologue
    .line 1899
    sget-object v0, Lcom/sec/android/app/samsungapps/z;->b:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1906
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1903
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const/16 v1, 0x11

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    .line 1904
    const/4 v0, 0x1

    goto :goto_0

    .line 1899
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected static openTencentDetailPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1271
    if-nez p0, :cond_0

    .line 1272
    const-string v0, "CommonActivity::openTencentDetailPage::Context is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 1307
    :goto_0
    return-void

    .line 1276
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1279
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "qqplazasamsung://details?id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&productid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1280
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1282
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tencent Deeplink URL : qqplazasamsung://details?id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&productid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 1284
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->checkIntentSafe(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1285
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 1287
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 1288
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1289
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1290
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;)V

    .line 1291
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/w;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/w;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1305
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto/16 :goto_0
.end method

.method public static requestAndShowNoticeList(ZLandroid/content/Context;)V
    .locals 3

    .prologue
    .line 1986
    if-nez p1, :cond_0

    .line 1988
    const-string v0, "CommonActivity::requestAndShowNoticeList::aContext is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 2019
    :goto_0
    return-void

    .line 1992
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 1993
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;-><init>()V

    .line 1994
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->clear()V

    .line 1998
    new-instance v2, Lcom/sec/android/app/samsungapps/x;

    invoke-direct {v2, v1, v0, p0}, Lcom/sec/android/app/samsungapps/x;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Z)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList$NotificationListObserver;)V

    .line 2018
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->requestNotifications()V

    goto :goto_0
.end method

.method public static requestAndShowType6NoticeList(ZLandroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2026
    if-nez p1, :cond_1

    .line 2028
    const-string v0, "CommonActivity::requestAndShowType6NoticeList::aContext is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 2064
    :cond_0
    :goto_0
    return-void

    .line 2032
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 2033
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->giftCardIssueFlag:I

    if-eq v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->voucherIssueFlag:I

    if-ne v1, v2, :cond_0

    :cond_2
    if-eqz p0, :cond_0

    .line 2038
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;-><init>()V

    .line 2039
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->clear()V

    .line 2041
    new-instance v2, Lcom/sec/android/app/samsungapps/y;

    invoke-direct {v2, v1, v0, p0}, Lcom/sec/android/app/samsungapps/y;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Z)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList$NotificationListObserver;)V

    .line 2062
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->requestType6Notifications()V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1411
    if-nez p0, :cond_0

    .line 1415
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move v7, p1

    .line 1414
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1418
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1422
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move v7, p2

    .line 1421
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1498
    const/4 v0, 0x0

    .line 1500
    packed-switch p7, :pswitch_data_0

    .line 1511
    :goto_0
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1512
    return-void

    .line 1502
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1503
    const-string v1, "_titleText"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1504
    const-string v1, "_sellerID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1505
    const-string v1, "_sellerName"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1507
    const-string v1, "cdcontainer"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1508
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 1500
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const v6, 0x7f0801f7

    const/high16 v4, 0x20000000

    const/high16 v5, 0x24000000

    .line 1515
    packed-switch p7, :pswitch_data_0

    .line 1838
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1520
    :pswitch_1
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/SamsungAppsMainActivity;

    invoke-direct {v2, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1521
    const/high16 v1, 0x24000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1523
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1525
    :catch_0
    move-exception v1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1526
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1528
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 1533
    :pswitch_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 1534
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1837
    :goto_1
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 1538
    :pswitch_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/CategoryListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1539
    const/high16 v2, 0x20020000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1

    .line 1543
    :pswitch_4
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1544
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1545
    const-string v2, "_deeplink_categoryId"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1546
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1547
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1

    .line 1551
    :pswitch_5
    if-eqz p3, :cond_0

    .line 1554
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1555
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne p3, v2, :cond_1

    .line 1556
    sget-object p3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 1558
    :cond_1
    const-string v2, "_listType"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1559
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1560
    sget-object v2, Lcom/sec/android/app/samsungapps/z;->a:[I

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    .line 1562
    :pswitch_6
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1563
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1

    .line 1566
    :pswitch_7
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1567
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1568
    const-string v2, "_listType"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1571
    :pswitch_8
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1572
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1573
    const-string v2, "_listType"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1576
    :pswitch_9
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1577
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1578
    const-string v2, "_listType"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1581
    :pswitch_a
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1582
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1585
    :pswitch_b
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1586
    const-string v2, "_description"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1587
    const-string v2, "_productSetListId"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1588
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1591
    :pswitch_c
    const-string v2, "_titleText"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1592
    const-string v2, "_description"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1593
    const-string v2, "_productSetListId"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1594
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1619
    :pswitch_d
    if-eqz p1, :cond_0

    .line 1624
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1626
    new-instance v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1627
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->requestInvokeDetail()V

    goto/16 :goto_0

    .line 1631
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1633
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->openTencentDetailPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1638
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1639
    const-string v2, "from"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1640
    const-string v2, "productid"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getVproductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1641
    const-string v2, "cdcontainer"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1647
    :pswitch_e
    if-eqz p2, :cond_0

    .line 1652
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1654
    new-instance v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1655
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->requestInvokeDetail()V

    goto/16 :goto_0

    .line 1659
    :cond_4
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isLinkApp()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1661
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->openTencentDetailPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1666
    :cond_5
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1667
    const-string v2, "from"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1668
    const-string v2, "productid"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVproductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1669
    const-string v2, "cdcontainer"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1670
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1678
    :pswitch_f
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1680
    new-instance v1, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1681
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->requestInvokeDetail()V

    goto/16 :goto_0

    .line 1685
    :cond_6
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isLinkApp()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1687
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->openTencentDetailPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1692
    :cond_7
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1693
    const-string v2, "from"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1694
    const-string v2, "cdcontainer"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1695
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1700
    :pswitch_10
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1701
    const-string v2, "productid"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVproductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1702
    const-string v2, "cdcontainer"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1708
    :pswitch_11
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 1709
    const-string v1, "GUID"

    invoke-virtual {v2, v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1710
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1711
    const-string v3, "from"

    const/16 v4, 0x1a

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1712
    const-string v3, "cdcontainer"

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v4, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1713
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1718
    :pswitch_12
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 1719
    const-string v2, "DEFAULT_STRING_FOR_SEARCH"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1720
    const/high16 v2, 0x20010000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1732
    :pswitch_13
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1733
    const-string v2, "_titleText"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0802f7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1734
    const-string v2, "_listType"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->wishList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1735
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1736
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1744
    :pswitch_14
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1745
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1751
    :pswitch_15
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1767
    :pswitch_16
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/NoticeDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1768
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->NOTICE_ID:Ljava/lang/String;

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1771
    :pswitch_17
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1772
    const-string v2, "commentType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1773
    const-string v2, "productid"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1774
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1777
    :pswitch_18
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ReviewListViewActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1778
    const-string v2, "commentType"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1779
    const-string v2, "productid"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1784
    :pswitch_19
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1788
    :pswitch_1a
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Landroid/content/Context;)V

    .line 1789
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPBillingCondition()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1791
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPApkCondition()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1793
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkGiftCardListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1794
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1798
    :cond_8
    check-cast p0, Landroid/app/Activity;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    new-instance v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    invoke-direct {v2}, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;-><init>()V

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getRealCountryCode()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->country:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "_"

    const-string v5, "-"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->language:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getBillingServerType()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getUPServerURL(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    new-instance v3, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-direct {v3}, Lcom/samsungosp/billingup/client/requestparam/UserInfo;-><init>()V

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getUserID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->authAppID:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    sget-object v4, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->_Token:Ljava/lang/String;

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    const-string v3, "+00:00"

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->timeOffset:Ljava/lang/String;

    new-instance v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-direct {v3}, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;-><init>()V

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    :try_start_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_KNOX"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_2

    :cond_9
    :goto_2
    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceUID:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    const-string v4, "M|640|480"

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getOpenApiVersion()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->osVersion:Ljava/lang/String;

    iget-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->loadODCVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->appVersion:Ljava/lang/String;

    :try_start_2
    invoke-static {v2, p0}, Lcom/samsungosp/billingup/client/UnifiedPayment;->makeGiftCardIntent(Lcom/samsungosp/billingup/client/requestparam/GiftCardData;Landroid/content/Context;)Landroid/content/Intent;
    :try_end_2
    .catch Lcom/samsungosp/billingup/client/util/UnifiedPaymentException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    invoke-virtual {p0, v1, v7}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    const v1, 0x7f050007

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1804
    :cond_a
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/GiftCardListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1805
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1810
    :pswitch_1b
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/CouponListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1811
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1815
    :pswitch_1c
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/GiftCardAndVoucherActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1816
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1820
    :pswitch_1d
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1821
    const-string v2, "_gift_card_code_string"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1822
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1826
    :pswitch_1e
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    .line 1830
    :pswitch_1f
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_1

    :catch_2
    move-exception v3

    goto/16 :goto_2

    .line 1798
    :catch_3
    move-exception v3

    goto/16 :goto_2

    .line 1515
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_f
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_10
        :pswitch_1e
        :pswitch_1f
    .end packed-switch

    .line 1560
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1425
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1429
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move v7, p2

    .line 1428
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;ILjava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1475
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1479
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move v4, p2

    move-object v5, v1

    move-object v6, p3

    move v7, p4

    .line 1478
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;ILjava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1446
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1450
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move v7, p2

    .line 1449
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1453
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1457
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, v1

    move-object v6, v1

    move v7, p3

    .line 1456
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1460
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1464
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, v1

    move-object v6, p3

    move v7, p4

    .line 1463
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1467
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1471
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    .line 1470
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1432
    if-nez p0, :cond_0

    .line 1436
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, p1

    move v7, p2

    .line 1435
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1439
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1443
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, p1

    move-object v5, v1

    move-object v6, p2

    move v7, p3

    .line 1442
    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public checkLoginState()Z
    .locals 1

    .prologue
    .line 1914
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    return v0
.end method

.method public commonStartActivity(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1352
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1359
    :goto_0
    return-void

    .line 1355
    :catch_0
    move-exception v0

    .line 1357
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "commonStartActivity::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public commonStartActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 1401
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1408
    :goto_0
    return-void

    .line 1404
    :catch_0
    move-exception v0

    .line 1406
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "commonStartActivityForResult::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 1

    .prologue
    .line 1968
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 1969
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v0

    .line 1971
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1962
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    .line 1964
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGearPlatformVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2068
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 1

    .prologue
    .line 1975
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    .line 1978
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto :goto_0
.end method

.method public getTotalActivityCount()I
    .locals 2

    .prologue
    .line 255
    sget v0, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    sget v1, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected initialized()V
    .locals 3

    .prologue
    .line 261
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 263
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->initialize(Landroid/content/Context;)V

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getTotalActivityCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogUtils;->findEntryPoint(Landroid/app/Activity;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setEntryPoint(Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V

    .line 273
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_START:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendAppStartAndFinishLog(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V

    .line 276
    :cond_0
    return-void

    .line 264
    :catch_0
    move-exception v0

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CommonActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 5

    .prologue
    const/high16 v4, 0x20020000

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 612
    const/4 v0, 0x0

    .line 613
    packed-switch p1, :pswitch_data_0

    .line 660
    :cond_0
    :goto_0
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 661
    return-void

    .line 616
    :pswitch_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 617
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 621
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/CategoryListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 622
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 626
    :pswitch_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v1

    if-eq v1, v2, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 629
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 630
    const-string v1, "buttonType"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 631
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 633
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->a:Z

    if-eqz v1, :cond_0

    .line 634
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->a:Z

    .line 635
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/u;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/u;-><init>(Lcom/sec/android/app/samsungapps/CommonActivity;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 613
    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 213
    packed-switch p2, :pswitch_data_0

    .line 222
    :goto_0
    return-void

    .line 216
    :pswitch_0
    const-string v0, "Calling UnifiedBilling GiftCardList Activity succeeded."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :pswitch_1
    const-string v0, "Buyer cancels."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 493
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 496
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/high16 v3, 0x1000000

    const/4 v2, 0x1

    .line 148
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mTheme:I

    if-nez v0, :cond_0

    .line 150
    invoke-static {}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getInstance()Lcom/sec/android/app/samsungapps/ThemeInfo;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mTheme:I

    .line 151
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mTheme:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->setTheme(I)V

    .line 157
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 164
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->requestWindowFeature(I)Z

    .line 165
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 167
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 168
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->b:Z

    if-nez v0, :cond_2

    .line 169
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 171
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->a:Z

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->setCurActivity()V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "amIS2I"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mIsS2I:Z

    .line 176
    new-instance v0, Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 177
    sget v0, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonActivity : onCreate : Current Activity count is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getTotalActivityCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->destroy()Ljava/lang/Boolean;

    .line 193
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->a:Z

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->recursiveRecycle(Landroid/view/View;)V

    .line 195
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 197
    sget v0, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonActivity : onDestroy : Current Activity count is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getTotalActivityCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getTotalActivityCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_1

    .line 202
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_FINISH:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendAppStartAndFinishLog(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V

    .line 204
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->destroy()V

    .line 207
    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 208
    return-void
.end method

.method public onLongClickActionItemActionBar(ILandroid/view/View;)Z
    .locals 11

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v0, :cond_0

    .line 674
    const-string v0, "CommonActivity::onLongClickActionItemActionBar::Action Bar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 675
    const/4 v0, 0x0

    .line 789
    :goto_0
    return v0

    .line 678
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 741
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonActivity::onLongClickActionItemActionBar::Invalid action type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 742
    const/4 v0, 0x0

    goto :goto_0

    .line 681
    :pswitch_1
    const v0, 0x7f0802f1

    move v1, v0

    .line 745
    :goto_1
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 746
    const/4 v0, 0x2

    new-array v3, v0, [I

    .line 748
    invoke-virtual {p2, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 749
    invoke-virtual {p2, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 751
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f040025

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 752
    const v0, 0x7f0c0099

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 753
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 755
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getToolTip()Landroid/widget/Toast;

    move-result-object v1

    .line 757
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070045

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    .line 758
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070046

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 759
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070047

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 762
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    new-array v8, v8, [F

    .line 763
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    .line 764
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 765
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v8}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 767
    invoke-static {v8}, Lcom/sec/android/app/samsungapps/CommonActivity;->a([F)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 768
    add-int/2addr v5, v6

    add-int/2addr v0, v5

    .line 771
    div-int/lit8 v5, v7, 0x2

    sub-int/2addr v5, v0

    .line 773
    if-nez v1, :cond_1

    .line 775
    new-instance v0, Landroid/widget/Toast;

    invoke-direct {v0, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 776
    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 777
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 778
    const/16 v1, 0x33

    const/4 v4, 0x0

    aget v4, v2, v4

    add-int/2addr v4, v5

    const/4 v5, 0x1

    aget v2, v2, v5

    const/4 v5, 0x1

    aget v3, v3, v5

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v4, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 779
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setToolTip(Landroid/widget/Toast;)V

    .line 787
    :goto_2
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 789
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 685
    :pswitch_2
    const v0, 0x7f080141

    move v1, v0

    .line 686
    goto/16 :goto_1

    .line 689
    :pswitch_3
    const v0, 0x7f0801ea

    move v1, v0

    .line 690
    goto/16 :goto_1

    .line 693
    :pswitch_4
    const v0, 0x7f0802f5

    move v1, v0

    .line 694
    goto/16 :goto_1

    .line 698
    :pswitch_5
    const v0, 0x7f08023d

    move v1, v0

    .line 699
    goto/16 :goto_1

    .line 702
    :pswitch_6
    const v0, 0x7f0802f0

    move v1, v0

    .line 703
    goto/16 :goto_1

    .line 706
    :pswitch_7
    const v0, 0x7f0800d4

    move v1, v0

    .line 709
    goto/16 :goto_1

    .line 712
    :pswitch_8
    const v0, 0x7f0802a1

    move v1, v0

    .line 713
    goto/16 :goto_1

    .line 716
    :pswitch_9
    const v0, 0x7f0802e9

    move v1, v0

    .line 717
    goto/16 :goto_1

    .line 720
    :pswitch_a
    const v0, 0x7f0802a1

    move v1, v0

    .line 721
    goto/16 :goto_1

    .line 724
    :pswitch_b
    const v0, 0x7f0801e0

    move v1, v0

    .line 725
    goto/16 :goto_1

    .line 729
    :pswitch_c
    const v0, 0x7f0802f7

    move v1, v0

    .line 730
    goto/16 :goto_1

    .line 733
    :pswitch_d
    const v0, 0x7f08011a

    move v1, v0

    .line 734
    goto/16 :goto_1

    .line 737
    :pswitch_e
    const v0, 0x7f0802f2

    move v1, v0

    .line 738
    goto/16 :goto_1

    .line 783
    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 784
    const/16 v0, 0x33

    const/4 v4, 0x0

    aget v4, v2, v4

    add-int/2addr v4, v5

    const/4 v5, 0x1

    aget v2, v2, v5

    const/4 v5, 0x1

    aget v3, v3, v5

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v1, v0, v4, v2}, Landroid/widget/Toast;->setGravity(III)V

    move-object v0, v1

    goto :goto_2

    .line 678
    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_d
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_2
        :pswitch_e
        :pswitch_c
        :pswitch_c
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 529
    packed-switch p1, :pswitch_data_0

    .line 599
    :goto_0
    return-void

    .line 576
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 578
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->isGearMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 582
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    .line 594
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    goto :goto_0

    .line 586
    :cond_2
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_1

    .line 589
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mIsS2I:Z

    if-ne v0, v3, :cond_1

    .line 591
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_1

    .line 598
    :pswitch_1
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0xa0000
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->doHomeKeyOptionMenu()V

    .line 241
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 242
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->doNonHardKeyOptionMenu(I)V

    .line 520
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 231
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->refreshTalkbackEnabled(Landroid/content/Context;)V

    .line 232
    return-void
.end method

.method public onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1111
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1267
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1115
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1118
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->c:Z

    .line 1121
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/v;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/v;-><init>(Lcom/sec/android/app/samsungapps/CommonActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 1142
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/SettingsListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1143
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 1144
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1146
    :cond_2
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1147
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 1155
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SETTINGS_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 1163
    :pswitch_3
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->emailID:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SendLogFileTo(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1165
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 1168
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1169
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_4

    .line 1170
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1172
    :cond_4
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1173
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 1181
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 1187
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1188
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_6

    .line 1189
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1191
    :cond_6
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1192
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 1200
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 1207
    :pswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->startTencentPurchasedActivity()V

    goto/16 :goto_0

    .line 1211
    :pswitch_7
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1212
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_8

    .line 1213
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1215
    :cond_8
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1216
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 1224
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PAYMENT_METHOD_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 1230
    :pswitch_8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1231
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_a

    .line 1232
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1234
    :cond_a
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1235
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 1243
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_ANNOUNCEMENT_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 1249
    :pswitch_9
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/HelpListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1250
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_c

    .line 1251
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1253
    :cond_c
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1254
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 1262
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_HELP_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 1111
    :pswitch_data_0
    .packed-switch 0x7f0c0000
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 8

    .prologue
    const v7, 0x7f0200c6

    const v6, 0x7f0801c2

    const v5, 0x7f0200c7

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 819
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 821
    const v0, 0x7f0c000b

    const v1, 0x7f0802b5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c9

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 822
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_0

    .line 824
    const v0, 0x7f0c0003

    const v1, 0x7f08021a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v7, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 861
    :cond_0
    :goto_0
    return v4

    .line 829
    :cond_1
    const v0, 0x7f0c000b

    const v1, 0x7f0802b5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c9

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 830
    const/high16 v0, 0x7f0c0000

    const v1, 0x7f0802f7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200ca

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 835
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 837
    const v0, 0x7f0c0001

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v5, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 843
    const v0, 0x7f0c0002

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f08014f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v5, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 852
    :goto_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_2

    .line 854
    const v0, 0x7f0c0003

    const v1, 0x7f08021a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v7, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 857
    :cond_2
    const v0, 0x7f0c0004

    const v1, 0x7f08018c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c4

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 858
    const v0, 0x7f0c0006

    const v1, 0x7f0802b4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c8

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 859
    const v0, 0x7f0c0005

    const v1, 0x7f08021b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c5

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_0

    .line 849
    :cond_3
    const v0, 0x7f0c0001

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v5, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto :goto_1
.end method

.method public onSamsungAppsCreateSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z
    .locals 1

    .prologue
    .line 1090
    const/4 v0, 0x1

    return v0
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 12

    .prologue
    const v11, 0x7f0c0001

    const v10, 0x7f0c000f

    const v1, 0x7f0c0003

    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 872
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-ne v0, v9, :cond_11

    .line 877
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v9, :cond_3

    .line 879
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 880
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 883
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c6

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 1055
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->isAutoLoginInProgress()Z

    move-result v0

    if-eq v0, v9, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v9, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-ne v0, v9, :cond_2

    .line 1059
    :cond_1
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 1062
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1067
    :goto_1
    return v2

    .line 890
    :cond_3
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 891
    const/high16 v0, 0x7f0c0000

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 894
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    move v5, v9

    .line 897
    :goto_2
    const/high16 v4, 0x7f0c0000

    const v0, 0x7f0802f7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200ca

    move-object v3, p1

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 900
    :cond_4
    invoke-virtual {p1, v11}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 906
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v9, :cond_a

    .line 909
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 910
    const/4 v5, 0x2

    .line 913
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0801c2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c7

    move-object v3, p1

    move v4, v11

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 941
    :cond_5
    :goto_4
    const v0, 0x7f0c0002

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 943
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v9, :cond_7

    .line 945
    const/4 v5, 0x2

    .line 946
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 947
    const/4 v5, 0x3

    .line 949
    :cond_6
    const v4, 0x7f0c0002

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0801c2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f08014f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c7

    move-object v3, p1

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 963
    :cond_7
    :goto_5
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 965
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_9

    .line 967
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 969
    const/4 v5, 0x2

    .line 970
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 971
    const/4 v5, 0x3

    .line 973
    :cond_8
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c6

    move-object v3, p1

    move v4, v1

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 1004
    :cond_9
    :goto_6
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    .line 1005
    const-string v1, "1"

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->writeHistory:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0c0010

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1007
    const v0, 0x7f0c0010

    const-string v1, "Send Log By Email"

    const v3, 0x7f02015b

    invoke-virtual {p1, v0, v1, v3, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_0

    .line 921
    :cond_a
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 922
    const/4 v5, 0x2

    .line 924
    :goto_7
    const v0, 0x7f0801c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c7

    move-object v3, p1

    move v4, v11

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_4

    .line 929
    :cond_b
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 932
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 933
    const/4 v5, 0x2

    .line 935
    :goto_8
    invoke-virtual {p1, v11}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 936
    const v0, 0x7f0801c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c7

    move-object v3, p1

    move v4, v11

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_4

    .line 957
    :cond_c
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 959
    const v0, 0x7f0c0002

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    goto/16 :goto_5

    .line 979
    :cond_d
    const/4 v5, 0x3

    .line 980
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 981
    const/4 v5, 0x4

    .line 983
    :cond_e
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c6

    move-object v3, p1

    move v4, v1

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_6

    .line 991
    :cond_f
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 993
    const/4 v5, 0x2

    .line 994
    invoke-virtual {p1, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 995
    const/4 v5, 0x3

    .line 997
    :cond_10
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 998
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c6

    move-object v3, p1

    move v4, v1

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_6

    .line 1018
    :cond_11
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v9, :cond_14

    .line 1020
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-ne v0, v9, :cond_12

    .line 1022
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 1039
    :cond_12
    :goto_9
    const v0, 0x7f0c0010

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-ne v0, v9, :cond_13

    .line 1041
    const v0, 0x7f0c0010

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 1042
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->removeLogFile()V

    .line 1045
    :cond_13
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1047
    const v1, 0x7f0c000b

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0802b5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c9

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_0

    .line 1027
    :cond_14
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-nez v0, :cond_15

    .line 1029
    invoke-virtual {p1, v11}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-ne v0, v9, :cond_15

    .line 1031
    invoke-virtual {p1, v11}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 1034
    :cond_15
    const/high16 v0, 0x7f0c0000

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 1035
    const v0, 0x7f0c0002

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 1036
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    goto :goto_9

    :cond_16
    move v2, v9

    .line 1067
    goto/16 :goto_1

    :cond_17
    move v5, v9

    goto/16 :goto_8

    :cond_18
    move v5, v9

    goto/16 :goto_7

    :cond_19
    move v5, v9

    goto/16 :goto_3

    :cond_1a
    move v5, v2

    goto/16 :goto_2
.end method

.method public onSamsungAppsOpenSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z
    .locals 1

    .prologue
    .line 1100
    const/4 v0, 0x1

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method public refreshOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)V
    .locals 0

    .prologue
    .line 1077
    if-eqz p1, :cond_0

    .line 1078
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 1079
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->refreshOptionmenu()V

    .line 1081
    :cond_0
    return-void
.end method

.method protected setCurActivity()V
    .locals 0

    .prologue
    .line 183
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 184
    return-void
.end method

.method protected setDoNotInitGlobal()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->b:Z

    .line 140
    return-void
.end method

.method protected setVisibleEmpty(Landroid/view/View;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 385
    .line 387
    if-ne p2, v0, :cond_0

    .line 394
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleLoading(Landroid/view/View;I)Z

    .line 395
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleNodata(Landroid/view/View;I)Z

    .line 404
    :goto_0
    return v0

    .line 399
    :cond_0
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleLoading(Landroid/view/View;I)Z

    .line 400
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleNodata(Landroid/view/View;I)Z

    .line 401
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleEmptyLayout(Landroid/view/View;I)Z

    move v0, v1

    goto :goto_0
.end method

.method protected setVisibleEmpty(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 291
    .line 293
    if-ne p1, v0, :cond_0

    .line 300
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleLoading(I)Z

    .line 301
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleNodata(I)Z

    .line 310
    :goto_0
    return v0

    .line 305
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleLoading(I)Z

    .line 306
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleNodata(I)Z

    .line 307
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleEmptyLayout(I)Z

    move v0, v1

    goto :goto_0
.end method

.method protected setVisibleEmptyLayout(I)Z
    .locals 2

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 323
    const v1, 0x7f0c007e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 324
    if-eqz v1, :cond_0

    .line 326
    const/4 v0, 0x1

    .line 327
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 330
    :cond_0
    return v0
.end method

.method protected setVisibleEmptyLayout(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 415
    .line 417
    if-nez p1, :cond_1

    .line 428
    :cond_0
    :goto_0
    return v0

    .line 421
    :cond_1
    const v1, 0x7f0c007e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 422
    if-eqz v1, :cond_0

    .line 424
    const/4 v0, 0x1

    .line 425
    invoke-virtual {v1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibleLoading(I)Z
    .locals 2

    .prologue
    .line 341
    const/4 v0, 0x0

    .line 343
    const v1, 0x7f0c007f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 344
    if-eqz v1, :cond_0

    .line 346
    const/4 v0, 0x1

    .line 347
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 350
    :cond_0
    if-nez p1, :cond_1

    .line 352
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleEmptyLayout(I)Z

    .line 355
    :cond_1
    return v0
.end method

.method public setVisibleLoading(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 439
    .line 441
    if-nez p1, :cond_1

    .line 457
    :cond_0
    :goto_0
    return v0

    .line 445
    :cond_1
    const v1, 0x7f0c007f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 446
    if-eqz v1, :cond_2

    .line 448
    const/4 v0, 0x1

    .line 449
    invoke-virtual {v1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 452
    :cond_2
    if-nez p2, :cond_0

    .line 454
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleEmptyLayout(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method public setVisibleNodata(I)Z
    .locals 2

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 368
    const v1, 0x7f0c0080

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 369
    if-eqz v1, :cond_0

    .line 371
    const/4 v0, 0x1

    .line 372
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 375
    :cond_0
    if-nez p1, :cond_1

    .line 377
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleEmptyLayout(I)Z

    .line 380
    :cond_1
    return v0
.end method

.method public setVisibleNodata(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 468
    .line 470
    if-nez p1, :cond_1

    .line 486
    :cond_0
    :goto_0
    return v0

    .line 474
    :cond_1
    const v1, 0x7f0c0080

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 475
    if-eqz v1, :cond_2

    .line 477
    const/4 v0, 0x1

    .line 478
    invoke-virtual {v1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 481
    :cond_2
    if-nez p2, :cond_0

    .line 483
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->setVisibleEmptyLayout(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1937
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 1940
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1944
    :goto_0
    return-void

    .line 1941
    :catch_0
    move-exception v0

    .line 1942
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CommonActivity::ActivityNotFoundException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1950
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 1951
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 1953
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1957
    :goto_0
    return-void

    .line 1954
    :catch_0
    move-exception v0

    .line 1955
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CommonActivity::ActivityNotFoundException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startHomeActivity()V
    .locals 3

    .prologue
    .line 1919
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1920
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1921
    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1925
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1931
    :goto_0
    return-void

    .line 1927
    :catch_0
    move-exception v0

    .line 1929
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CommonActivity::startHomeActivity::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startTencentPurchasedActivity()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1311
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1312
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "open://updatelist?id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->ODC_PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1313
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1315
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->checkIntentSafe(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1317
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1324
    :goto_0
    return-void

    .line 1321
    :cond_0
    const v0, 0x7f080148

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x7f08014f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1322
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0
.end method
