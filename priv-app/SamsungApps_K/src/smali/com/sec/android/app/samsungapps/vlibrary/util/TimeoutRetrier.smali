.class public Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private handler:Landroid/os/Handler;

.field protected mRetryAction:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;

.field protected mRetryCount:I

.field protected mRetryTime:J

.field private mTimer:Landroid/os/CountDownTimer;


# direct methods
.method public constructor <init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->handler:Landroid/os/Handler;

    .line 16
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryAction:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;

    .line 17
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryCount:I

    .line 18
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryTime:J

    .line 19
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 79
    return-void
.end method

.method protected createTimer()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/c;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryTime:J

    mul-long v2, v1, v6

    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryTime:J

    mul-long/2addr v4, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mTimer:Landroid/os/CountDownTimer;

    .line 44
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mTimer:Landroid/os/CountDownTimer;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryAction:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;

    .line 86
    return-void
.end method

.method public retry()Z
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryCount:I

    if-nez v0, :cond_0

    .line 57
    const/4 v0, 0x0

    .line 61
    :goto_0
    return v0

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->sendRetryMsgToHandler()V

    .line 61
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected runAction()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 48
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryCount:I

    if-lez v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryAction:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryCount:I

    if-gt v2, v0, :cond_1

    :goto_0
    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;->retry(Z)V

    .line 51
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryCount:I

    .line 52
    return-void

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected sendRetryMsgToHandler()V
    .locals 6

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mRetryTime:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 67
    return-void
.end method

.method protected timerRestart()V
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->createTimer()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->mTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 74
    return-void
.end method
