.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 5

    .prologue
    .line 38
    if-eqz p1, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IGCreditCardReqComm:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_Context:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IGCreditCardParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;
    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IResultMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/d;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;)V

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;->requestPayment(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->onPaymentResult(Z)V

    goto :goto_0
.end method
