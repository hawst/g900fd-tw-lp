.class public Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 20
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;

    .line 33
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 92
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 112
    :goto_0
    :pswitch_0
    return-void

    .line 95
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->INVOKE_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 104
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)Z
    .locals 2

    .prologue
    .line 39
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 87
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 42
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 45
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->CONDITION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 53
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->SHOW_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 64
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->WAIT_USER_RESPONSE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 79
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch

    .line 42
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 50
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 61
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 76
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 116
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 122
    :goto_0
    return-void

    .line 119
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
