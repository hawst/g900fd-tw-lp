.class public Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final BUFF_SIZE:I = 0x800

.field public static final STR_DEFAULT_DIR:Ljava/lang/String; = "/sdcard/samsungapps"


# instance fields
.field bCanceled:Z

.field bCompleteFile:Z

.field bResumeDownload:Z

.field fos:Ljava/io/FileOutputStream;

.field mAbsolutePath:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mFile:Ljava/io/File;

.field mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

.field useSD:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mAbsolutePath:Ljava/lang/String;

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bResumeDownload:Z

    .line 21
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->useSD:Z

    .line 23
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    .line 24
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    .line 26
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCompleteFile:Z

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mContext:Landroid/content/Context;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->prepareFile()V

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->isCompletelyDownloaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCompleteFile:Z

    .line 39
    :cond_0
    return-void
.end method

.method private createDir(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 76
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createDownloadingFile()V
    .locals 5

    .prologue
    const v1, 0x8000

    const/4 v2, 0x0

    .line 143
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bResumeDownload:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 153
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->useSD:Z

    if-eqz v3, :cond_2

    .line 155
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    if-ne v0, v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-direct {v3, v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    .line 161
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 150
    goto :goto_0

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->getSaveFileName()Ljava/lang/String;

    move-result-object v2

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    goto :goto_1
.end method

.method private static createInternalStorageFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 64
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 65
    return-object v1
.end method

.method public static delete(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 115
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->createInternalStorageFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    .line 259
    return-void
.end method

.method public createNewFile()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 103
    return-void
.end method

.method public delete()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public download(Ljava/io/BufferedInputStream;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->createDownloadingFile()V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    if-nez v2, :cond_1

    .line 241
    :cond_0
    :goto_0
    return v0

    .line 173
    :cond_1
    const/16 v2, 0x800

    :try_start_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 175
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 176
    const-wide/16 v2, 0x0

    .line 178
    :cond_2
    const/4 v5, 0x0

    const/16 v6, 0x800

    invoke-virtual {p1, v4, v5, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    .line 179
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    const-wide/16 v5, 0x1

    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :goto_1
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    long-to-int v5, v5

    .line 188
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-double v6, v6

    sub-double/2addr v6, v2

    const-wide v8, 0x408f400000000000L    # 1000.0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_3

    .line 190
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-double v2, v2

    .line 191
    int-to-long v5, v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgress(J)V

    .line 193
    :cond_3
    iget-boolean v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    if-eqz v5, :cond_2

    .line 195
    const-string v1, "FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 197
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 229
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgressEnd(Z)V

    .line 230
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    if-eqz v1, :cond_0

    .line 232
    const-string v1, "finally __ FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v1

    goto :goto_0

    .line 209
    :cond_4
    :try_start_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->isCompletelyDownloaded()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-int v2, v2

    .line 212
    int-to-long v2, v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgress(J)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 213
    :try_start_5
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 229
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgressEnd(Z)V

    .line 230
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    if-eqz v2, :cond_5

    .line 232
    const-string v1, "finally __ FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 241
    :catch_1
    move-exception v1

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto/16 :goto_0

    .line 218
    :cond_6
    :try_start_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 219
    :try_start_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 229
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgressEnd(Z)V

    .line 230
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    if-eqz v1, :cond_0

    .line 232
    const-string v1, "finally __ FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 241
    :catch_2
    move-exception v1

    goto/16 :goto_0

    :catch_3
    move-exception v1

    .line 223
    :try_start_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 229
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgressEnd(Z)V

    .line 230
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    if-eqz v1, :cond_0

    .line 232
    const-string v1, "finally __ FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_0

    .line 241
    :catch_4
    move-exception v1

    goto/16 :goto_0

    .line 227
    :catchall_0
    move-exception v1

    .line 228
    :try_start_9
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 229
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgressEnd(Z)V

    .line 230
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCanceled:Z

    if-eqz v2, :cond_7

    .line 232
    const-string v1, "finally __ FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_0

    .line 241
    :catch_5
    move-exception v1

    goto/16 :goto_0

    :cond_7
    throw v1

    :catch_6
    move-exception v5

    goto/16 :goto_1
.end method

.method public existDownloadingFile()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public getAbsPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bCompleteFile:Z

    return v0
.end method

.method public isCompletelyDownloaded()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v1

    .line 249
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 253
    :cond_0
    return v0
.end method

.method protected notifyProgress(J)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->updateDownloadedSize(J)V

    .line 134
    return-void
.end method

.method protected notifyProgressEnd(Z)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->downloadEnded(Z)V

    .line 139
    return-void
.end method

.method prepareFile()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->createDir(Ljava/lang/String;)Z

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFileInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->getSaveFileName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->createInternalStorageFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->useSD:Z

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->mAbsolutePath:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->existDownloadingFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->bResumeDownload:Z

    .line 59
    :cond_0
    return-void
.end method
