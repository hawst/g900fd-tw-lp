.class public Lcom/sec/android/app/samsungapps/Main;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# static fields
.field public static _DeepLinkMode:Z

.field public static _OldDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

.field private static a:Z

.field private static b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

.field public static contentNotificationManager:Lcom/sec/android/app/samsungapps/ContentNotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/samsungapps/Main;->a:Z

    .line 108
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/Main;->_DeepLinkMode:Z

    .line 109
    sput-object v1, Lcom/sec/android/app/samsungapps/Main;->_OldDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 110
    sput-object v1, Lcom/sec/android/app/samsungapps/Main;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/DeepLinker;)V
    .locals 5

    .prologue
    .line 267
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 268
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 269
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/InitializeCommandBuilder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/InitializeCommandBuilder;-><init>(Landroid/content/Context;)V

    .line 270
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->initializer(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/ex;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ex;-><init>(Lcom/sec/android/app/samsungapps/Main;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;)V

    .line 284
    new-instance v1, Lcom/sec/android/app/samsungapps/ey;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/ey;-><init>(Lcom/sec/android/app/samsungapps/Main;Lcom/sec/android/app/samsungapps/DeepLinker;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext;->check(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;)V

    .line 305
    const v0, 0x7f040075

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->setContentView(I)V

    .line 306
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 307
    const v0, 0x7f0c01bd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 308
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 310
    const v0, 0x7f0c0082

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 311
    if-eqz v0, :cond_0

    .line 312
    const-string v3, "isa_samsungapps_icon_dummy"

    const-string v4, "drawable"

    invoke-static {p0, v3, v4}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 315
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 316
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 317
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 319
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/Main;Lcom/sec/android/app/samsungapps/DeepLinker;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/Main;->a(Lcom/sec/android/app/samsungapps/DeepLinker;)V

    return-void
.end method

.method private a(Ljava/lang/Class;)V
    .locals 2

    .prologue
    .line 338
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 339
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 341
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->startActivity(Landroid/content/Intent;)V

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->finish()V

    .line 343
    return-void
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 44
    const-string v2, "productid"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    const-string v3, "GUID"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    const-string v4, "action"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 47
    const-string v5, "notificationid"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 49
    if-nez v4, :cond_1

    .line 52
    if-eq v5, v7, :cond_0

    .line 53
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 54
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    move v0, v1

    .line 105
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    const/4 v6, 0x2

    if-eq v4, v6, :cond_2

    if-ne v4, v8, :cond_5

    :cond_2
    if-eq v5, v7, :cond_5

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/Main;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/Main;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 62
    if-ne v4, v8, :cond_4

    .line 64
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0, p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isExecutable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0, p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    :cond_3
    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_4
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 71
    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->launchApp(Ljava/lang/String;Z)Z

    .line 72
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 73
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    move v0, v1

    .line 74
    goto :goto_0

    .line 78
    :cond_5
    const/4 v2, 0x5

    if-ne v4, v2, :cond_0

    .line 79
    const-string v0, "Clicked push message notification"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 83
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->initialize(Landroid/content/Context;)V

    .line 84
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->NOTIFICATION_BAR:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_MESSAGE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    .line 91
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 93
    if-nez v0, :cond_6

    move v0, v1

    .line 94
    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Main::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 97
    :cond_6
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 98
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 99
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/Main;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 102
    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 365
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCacheCleanManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;
    .locals 2

    .prologue
    .line 113
    sget-object v0, Lcom/sec/android/app/samsungapps/Main;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;-><init>()V

    .line 115
    sput-object v0, Lcom/sec/android/app/samsungapps/Main;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/ew;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ew;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;)V

    .line 123
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/Main;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/high16 v6, 0x10000000

    const/4 v5, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 128
    sput-boolean v1, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;->bBackPressed:Z

    .line 129
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->requestWindowFeature(I)Z

    .line 130
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 133
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 135
    sget-boolean v2, Lcom/sec/android/app/samsungapps/Main;->a:Z

    sput-boolean v1, Lcom/sec/android/app/samsungapps/Main;->a:Z

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 139
    const-string v2, "KNOXMODE"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 141
    const-string v4, "CANCELDOWNLOAD"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 143
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 145
    const-string v0, "PACKAGENAME"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const-string v0, "NOTIFICATIONID"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 149
    if-eq v1, v5, :cond_0

    .line 151
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->finish()V

    .line 231
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Global;->clearInitialized()V

    .line 159
    sget-boolean v4, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bkNOXmode:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eq v4, v5, :cond_2

    .line 161
    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bNeedClearCache:Z

    .line 164
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v0, :cond_3

    .line 166
    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bkNOXmode:Z

    .line 173
    :goto_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/Main;->a(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->finish()V

    goto :goto_0

    .line 170
    :cond_3
    sput-boolean v1, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bkNOXmode:Z

    goto :goto_1

    .line 182
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/samsungapps/OdcUpdateProgressActivity;->SAMSUNGAPPS_UPDATING:Z

    if-ne v2, v0, :cond_5

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_5

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->finish()V

    goto :goto_0

    .line 189
    :cond_5
    new-instance v2, Lcom/sec/android/app/samsungapps/intentPreProcess/IntentPreProcessForNFC;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/intentPreProcess/IntentPreProcessForNFC;-><init>()V

    invoke-virtual {v2, p0}, Lcom/sec/android/app/samsungapps/intentPreProcess/IntentPreProcessForNFC;->preProcess(Landroid/app/Activity;)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLink(Landroid/content/Intent;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v2

    .line 192
    sput-object v2, Lcom/sec/android/app/samsungapps/Main;->_OldDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 193
    if-nez v2, :cond_6

    .line 195
    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;-><init>(Landroid/content/Context;)V

    .line 196
    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;->load()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v2

    .line 197
    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;->clearDeepLink()V

    .line 199
    :cond_6
    if-eqz v2, :cond_7

    .line 201
    sput-boolean v0, Lcom/sec/android/app/samsungapps/Main;->_DeepLinkMode:Z

    .line 202
    new-instance v4, Lcom/sec/android/app/samsungapps/DeepLinker;

    invoke-direct {v4, p0, v2}, Lcom/sec/android/app/samsungapps/DeepLinker;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;)V

    .line 203
    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/DeepLinker;->needRunDeepLink()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 205
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/Main;->a(Lcom/sec/android/app/samsungapps/DeepLinker;)V

    goto :goto_0

    .line 209
    :cond_7
    sput-boolean v1, Lcom/sec/android/app/samsungapps/Main;->_DeepLinkMode:Z

    .line 215
    const-string v2, "CallerType"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    const-string v3, "8"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_9

    .line 217
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->start(Z)V

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 215
    goto :goto_2

    .line 222
    :cond_9
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/Main;->getCacheCleanManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->toNormalMode()V

    .line 224
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->clearBadgeCount(Landroid/content/Context;)V

    .line 225
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v2

    if-ne v2, v0, :cond_a

    :goto_3
    if-eqz v0, :cond_b

    const-string v0, "Main::startMainTab::Not Supported::SamsungUpdates(Except knox2mode)"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/Main;->finish()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MainMain::NullPointerException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 225
    goto :goto_3

    :cond_b
    :try_start_1
    const-class v0, Lcom/sec/android/app/samsungapps/SamsungAppsMainActivity;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->a(Ljava/lang/Class;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-class v0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/Main;->a(Ljava/lang/Class;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 228
    :catch_2
    move-exception v0

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Main::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
