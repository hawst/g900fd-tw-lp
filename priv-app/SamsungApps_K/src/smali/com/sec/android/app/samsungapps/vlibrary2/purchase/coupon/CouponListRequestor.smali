.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

.field protected _ICouponReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;

.field _bCouponResult:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_bCouponResult:Z

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_ICouponReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;

    .line 23
    return-void
.end method

.method private checkResult()V
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->iscompleteReceive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_ICouponReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;->complete()V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->onFinalResult(Z)V

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_ICouponReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;->clear()V

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->requestCouponList()V

    .line 30
    return-void
.end method

.method protected iscompleteReceive()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_bCouponResult:Z

    return v0
.end method

.method protected onReceiveCouponList(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48
    if-eqz p1, :cond_1

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_bCouponResult:Z

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 53
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_ICouponReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;->add(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    goto :goto_0

    .line 55
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->checkResult()V

    .line 62
    :goto_1
    return-void

    .line 59
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_bCouponResult:Z

    .line 60
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->onFinalResult(Z)V

    goto :goto_1
.end method

.method protected requestCouponList()V
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->customerCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 73
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 74
    return-void
.end method
