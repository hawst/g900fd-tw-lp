.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract checkConfirmMSG()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract checkRandomKey()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract checkTNC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createSMSSender()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;
.end method

.method public abstract init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract moDelivery()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract psmsPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract sendSMS()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
