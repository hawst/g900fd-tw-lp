.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _GiftCardCode:Ljava/lang/String;

.field private _IDeleteGiftCardData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;

.field private iLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->_GiftCardCode:Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->_IDeleteGiftCardData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->iLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->_IDeleteGiftCardData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->iLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->iLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 29
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;->_GiftCardCode:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->deleteGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 37
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 38
    return-void
.end method
