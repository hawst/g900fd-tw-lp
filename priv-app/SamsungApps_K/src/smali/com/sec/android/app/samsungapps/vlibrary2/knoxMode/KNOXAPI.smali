.class public Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;


# static fields
.field public static bNeedClearCache:Z

.field public static bkNOXmode:Z


# instance fields
.field private _ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bNeedClearCache:Z

    .line 15
    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bkNOXmode:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    .line 20
    :try_start_0
    new-instance v0, Lcom/sec/knox/containeragent/ContainerManager;

    invoke-direct {v0, p1}, Lcom/sec/knox/containeragent/ContainerManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    :goto_0
    return-void

    .line 21
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getLaunchIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 108
    const-string v0, "KNOX"

    const-string v2, "KNOX launch"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-virtual {v0, p2}, Lcom/sec/knox/containeragent/ContainerManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    if-eqz v0, :cond_0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 115
    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    move-object v0, v1

    .line 120
    goto :goto_0
.end method

.method public installPackage(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-virtual {v1}, Lcom/sec/knox/containeragent/ContainerManager;->isContainerAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    :goto_0
    return v0

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/a;

    invoke-direct {v2, p0, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    invoke-virtual {v1, p2, v2}, Lcom/sec/knox/containeragent/ContainerManager;->installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerManagerCallback;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    .line 62
    invoke-interface {p3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_0
.end method

.method public isExecutable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 70
    const-string v1, "KNOX"

    const-string v2, "KNOX isExecutable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-virtual {v1, p2}, Lcom/sec/knox/containeragent/ContainerManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    .line 75
    const/4 v0, 0x1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 78
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0
.end method

.method public isKnoxMode()Z
    .locals 1

    .prologue
    .line 30
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bkNOXmode:Z

    return v0
.end method

.method public launch(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 88
    const-string v1, "KNOX"

    const-string v2, "KNOX launch"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-virtual {v1, p2}, Lcom/sec/knox/containeragent/ContainerManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 91
    if-eqz v1, :cond_0

    .line 93
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    const/4 v0, 0x1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 98
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    invoke-virtual {v0}, Lcom/sec/knox/containeragent/ContainerManager;->unbindContainerManager()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->_ContainerManager:Lcom/sec/knox/containeragent/ContainerManager;

    .line 131
    :cond_0
    return-void
.end method
