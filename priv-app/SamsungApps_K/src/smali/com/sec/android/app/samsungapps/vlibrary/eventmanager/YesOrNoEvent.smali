.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;
.super Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
.source "ProGuard"


# instance fields
.field mYesOrNoEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

.field mYesOrNoResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;)V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->mYesOrNoEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->mYesOrNoResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;

    .line 15
    return-void
.end method


# virtual methods
.method public getYesOrNoEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->mYesOrNoEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    return-object v0
.end method

.method public no()V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->mYesOrNoResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;->onYesOrNoResult(Z)V

    .line 25
    return-void
.end method

.method public yes()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->mYesOrNoResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;->onYesOrNoResult(Z)V

    .line 20
    return-void
.end method
