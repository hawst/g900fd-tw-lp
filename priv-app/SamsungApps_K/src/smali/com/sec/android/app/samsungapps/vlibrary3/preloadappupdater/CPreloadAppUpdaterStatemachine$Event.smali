.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOWNLOAD_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

.field public static final enum DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

.field public static final enum FAIL_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

.field public static final enum MEET_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

.field public static final enum UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    const-string v1, "FAIL_PRECONDITON"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->FAIL_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    const-string v1, "MEET_PRECONDITON"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->MEET_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    const-string v1, "DOWNLOAD_CANCELED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->DOWNLOAD_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    const-string v1, "DOWNLOAD_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    const-string v1, "UPDATE_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    .line 18
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->FAIL_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->MEET_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->DOWNLOAD_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    return-object v0
.end method
