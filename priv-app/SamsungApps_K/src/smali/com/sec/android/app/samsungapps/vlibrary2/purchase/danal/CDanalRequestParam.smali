.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;


# instance fields
.field private _CarrierName:Ljava/lang/String;

.field private _ConfirmNumber:Ljava/lang/String;

.field _CouponSeq:Ljava/lang/String;

.field _ItemToBuy:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

.field private _NID:Ljava/lang/String;

.field private _PhoneNumber:Ljava/lang/String;

.field private _bRequestAuthNumber:Z

.field private _bUserAgreed:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_ConfirmNumber:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_ItemToBuy:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_CouponSeq:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_CarrierName:Ljava/lang/String;

    .line 22
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_NID:Ljava/lang/String;

    .line 23
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_PhoneNumber:Ljava/lang/String;

    .line 24
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_bUserAgreed:Z

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_bRequestAuthNumber:Z

    .line 26
    return-void
.end method


# virtual methods
.method public addPaymentInfo(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_ConfirmNumber:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_bRequestAuthNumber:Z

    .line 32
    return-void
.end method

.method public getAuthNID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_NID:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthPNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_PhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getCarrierName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_CarrierName:Ljava/lang/String;

    return-object v0
.end method

.method public getCouponIssuedSEQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_CouponSeq:Ljava/lang/String;

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_ItemToBuy:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_ItemToBuy:Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserEntererdConfirmMSGNo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_ConfirmNumber:Ljava/lang/String;

    return-object v0
.end method

.method public isRequestAuthNumber()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_bRequestAuthNumber:Z

    return v0
.end method

.method public phoneEnabled()Z
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    .line 77
    if-nez v0, :cond_0

    .line 79
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->iSPhoneSupported()Z

    move-result v0

    goto :goto_0
.end method

.method public userAgreed()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/CDanalRequestParam;->_bUserAgreed:Z

    return v0
.end method
