.class public Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mDownloadMethod:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;


# direct methods
.method public constructor <init>(IZ)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    if-eqz p2, :cond_0

    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;->download:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker;->mDownloadMethod:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    .line 30
    :goto_0
    return-void

    .line 21
    :cond_0
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;->downloadEx:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker;->mDownloadMethod:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    goto :goto_0

    .line 27
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;->easyBuyPurchase:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker;->mDownloadMethod:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    goto :goto_0
.end method


# virtual methods
.method public getDownloadMethod()Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker;->mDownloadMethod:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadMethodChecker$DownloadMethod;

    return-object v0
.end method
