.class public Lcom/sec/android/app/samsungapps/pushclient/PushInterface;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/spp/push/IPushClientService;

.field private b:Landroid/content/Context;

.field private c:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/pushclient/a;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushInterface;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->c:Landroid/content/ServiceConnection;

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/pushclient/PushInterface;Lcom/sec/spp/push/IPushClientService;)Lcom/sec/spp/push/IPushClientService;
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    return-object p1
.end method


# virtual methods
.method public bind()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 68
    const-string v1, "bind()"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_0

    .line 76
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->c:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v1, v3, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindService(mPushClientConnection) requested. bResult="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    :goto_0
    return v0

    .line 82
    :cond_0
    const-string v0, "startService Fail"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deregistration(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 150
    const-string v1, "deregistration()"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_0

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v1, p1}, Lcom/sec/spp/push/IPushClientService;->deregistration(Ljava/lang/String;)V

    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mService.deregistration("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") requested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 159
    :catch_0
    move-exception v1

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.deregistration() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_0
    const-string v1, "mService is null"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public finalize()V
    .locals 3

    .prologue
    .line 53
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->unbind()Z

    .line 61
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "finalize() failed - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRegId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180
    const-string v0, "getRegId()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-eqz v0, :cond_0

    .line 186
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v0, p1}, Lcom/sec/spp/push/IPushClientService;->getRegId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mService.getRegId("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") requested. regid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_0
    return-object v0

    .line 190
    :catch_0
    move-exception v0

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mService.getRegId() failed - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 197
    :cond_0
    const-string v0, "mService is null"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getRegisteredAppIDs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 208
    const-string v0, "getRegisteredAppIDs()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    const/4 v0, 0x0

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_3

    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v1}, Lcom/sec/spp/push/IPushClientService;->getRegisteredAppIDs()[Ljava/lang/String;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 217
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 219
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Registered Application ID : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222
    :cond_0
    if-eqz v0, :cond_2

    array-length v1, v0

    if-gtz v1, :cond_2

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registered Application is empty - mAppLists.length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    :cond_1
    :goto_1
    return-object v0

    .line 228
    :cond_2
    const-string v1, "Registered Application is empty - mAppLists is null "

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 231
    :catch_0
    move-exception v1

    .line 233
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getRegisteredAppIDs() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 238
    :cond_3
    const-string v1, "mService is null"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isPushAvailable()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 258
    const-string v1, "isPushAvailable()"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_0

    .line 264
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v1}, Lcom/sec/spp/push/IPushClientService;->isPushAvailable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 274
    :cond_0
    :goto_0
    return v0

    .line 266
    :catch_0
    move-exception v1

    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.isPushAvailable() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isPushServerAvailable()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-eqz v0, :cond_0

    .line 248
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registration(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 120
    const-string v1, "registration()"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_0

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v2, p1, v1}, Lcom/sec/spp/push/IPushClientService;->registration(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mService.registration("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") requested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 130
    :catch_0
    move-exception v1

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.registration() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_0
    const-string v1, "mService is null"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unbind()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 94
    const-string v0, "unbind"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a:Lcom/sec/spp/push/IPushClientService;

    if-nez v0, :cond_0

    .line 112
    :goto_0
    return v3

    .line 102
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->c:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    const-string v0, "IllegalArgumentException occurred"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    .line 109
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
