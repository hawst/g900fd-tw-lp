.class final Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

.field b:J

.field final synthetic c:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V
    .locals 3

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->c:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    .line 210
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->b:J

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getSizeToDownload()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->setSize(J)V

    .line 214
    return-void
.end method


# virtual methods
.method public final downloadEnded(Z)V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public final getDownloadURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->c:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getURLToDownload()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    return-object v0
.end method

.method public final getSaveFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->c:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final updateDownloadedSize(J)V
    .locals 0

    .prologue
    .line 222
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;->b:J

    .line 223
    return-void
.end method
