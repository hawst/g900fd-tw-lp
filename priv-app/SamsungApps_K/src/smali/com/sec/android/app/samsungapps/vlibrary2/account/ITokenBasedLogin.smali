.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    .line 15
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V

    return-void
.end method

.method private loginAccount()V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;->loginSamsungAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/m;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 89
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onRequestToken()V

    .line 27
    :goto_0
    return-void

    .line 25
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->loginAccount()V

    goto :goto_0
.end method

.method protected onRequestToken()V
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;->requestToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 71
    return-void
.end method
