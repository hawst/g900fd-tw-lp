.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IStateInfoRequestCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand$IStateInfoRequestCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand$IStateInfoRequestCommandData;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;->_IStateInfoRequestCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand$IStateInfoRequestCommandData;

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;->_IStateInfoRequestCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand$IStateInfoRequestCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand$IStateInfoRequestCommandData;->getNewlyCreatedStateInfoList()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoList;

    move-result-object v0

    .line 24
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/StateInfoRequestCommand;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getStateList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 39
    return-void
.end method
