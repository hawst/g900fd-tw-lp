.class public Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field public contentsSize:Ljava/lang/String;

.field public deltaContentsSize:Ljava/lang/String;

.field public deltaDownloadURL:Ljava/lang/String;

.field public downLoadURI:Ljava/lang/String;

.field public gSignatureDownloadURL:Ljava/lang/String;

.field public installSize:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public productName:Ljava/lang/String;

.field public signature:Ljava/lang/String;

.field public successYn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->installSize:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productID:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productName:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaDownloadURL:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaContentsSize:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->signature:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->orderID:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->successYn:Ljava/lang/String;

    .line 22
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 139
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    .line 143
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    const-string v0, "downLoadURI"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    .line 62
    :cond_0
    const-string v0, "contentsSize"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    .line 67
    :cond_1
    const-string v0, "installSize"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->installSize:Ljava/lang/String;

    .line 72
    :cond_2
    const-string v0, "productID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productID:Ljava/lang/String;

    .line 77
    :cond_3
    const-string v0, "productName"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productName:Ljava/lang/String;

    .line 82
    :cond_4
    const-string v0, "deltaDownloadURL"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 84
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaDownloadURL:Ljava/lang/String;

    .line 87
    :cond_5
    const-string v0, "deltaContentsSize"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 89
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaContentsSize:Ljava/lang/String;

    .line 92
    :cond_6
    const-string v0, "signature"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 94
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->signature:Ljava/lang/String;

    .line 96
    :cond_7
    const-string v0, "gSignatureDownloadURL"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 98
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    .line 100
    :cond_8
    const-string v0, "orderID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 101
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->orderID:Ljava/lang/String;

    .line 103
    :cond_9
    const-string v0, "successYn"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 105
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->successYn:Ljava/lang/String;

    .line 107
    :cond_a
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public copyFrom(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Z
    .locals 1

    .prologue
    .line 26
    :try_start_0
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->installSize:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->installSize:Ljava/lang/String;

    .line 29
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productID:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productID:Ljava/lang/String;

    .line 30
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productName:Ljava/lang/String;

    .line 31
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaDownloadURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaDownloadURL:Ljava/lang/String;

    .line 32
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaContentsSize:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaContentsSize:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->signature:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->signature:Ljava/lang/String;

    .line 34
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    .line 35
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->orderID:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->orderID:Ljava/lang/String;

    .line 36
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->successYn:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->successYn:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    .line 38
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 40
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGearSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    return-object v0
.end method

.method public hasGearSignature()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->gSignatureDownloadURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportingDeltaDownload()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaDownloadURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaContentsSize:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->installSize:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productID:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->productName:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method
