.class public Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# instance fields
.field private a:Landroid/webkit/WebView;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->onBackPressed()V

    .line 73
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 24
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "webViewUrl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->b:Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->finish()V

    .line 31
    :cond_0
    const v0, 0x7f0400db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->setMainView(I)V

    .line 32
    const v0, 0x7f0802fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v4, v6, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 34
    const v0, 0x7f0c02ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    const-string v0, "WebView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 35
    :goto_0
    return-void

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;->a:Landroid/webkit/WebView;

    .line 48
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 49
    return-void
.end method
