.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->unregisterBroadcastReceiver()V

    .line 88
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 90
    const-string v2, "PaymentCompleteReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "action :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "com.sec.android.wallet.PAYMENT_COMPLETED_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->_ISamsungWalletExecData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;->getWK()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const-string v1, "isSuccess"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->b:Z

    .line 94
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->b:Z

    if-eqz v1, :cond_1

    .line 96
    const-string v1, "trid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->c:Ljava/lang/String;

    .line 97
    const-string v1, "returnParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->e:Ljava/lang/String;

    .line 99
    const-string v1, "pType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->d:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;Z)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    const-string v1, "errorCode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->f:Ljava/lang/String;

    .line 103
    const-string v1, "errorMsg"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->g:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;Z)V

    goto :goto_0
.end method
