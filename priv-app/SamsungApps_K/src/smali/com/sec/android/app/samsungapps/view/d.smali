.class final Lcom/sec/android/app/samsungapps/view/d;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Landroid/widget/TextView;

.field b:Landroid/view/View;

.field c:Landroid/widget/ImageView;

.field d:Landroid/widget/ImageView;

.field final synthetic e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/d;->e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const v0, 0x7f0c0163

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->a:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0c0164

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->c:Landroid/widget/ImageView;

    .line 76
    const v0, 0x7f0c0165

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->d:Landroid/widget/ImageView;

    .line 77
    const v0, 0x7f0c0166

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->b:Landroid/view/View;

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;Landroid/view/View;B)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/d;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/d;ILcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 2

    .prologue
    .line 68
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "1"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->specialCategoryYn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    iget-object v1, p3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->specialCategoryYn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/d;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
