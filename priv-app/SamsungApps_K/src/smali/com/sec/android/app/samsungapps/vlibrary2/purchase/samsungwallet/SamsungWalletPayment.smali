.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PaymentMethodCommand;
.source "ProGuard"


# static fields
.field private static final TIMEOUT:Ljava/lang/String; = "30"


# instance fields
.field private _ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

.field initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

.field loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;->getFinalMapContainer()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PaymentMethodCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->endLoading()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->onFinalResult(Z)V

    return-void
.end method

.method private endLoading()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 179
    :cond_0
    return-void
.end method

.method private requestInit()V
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->initInicisMobilePurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 98
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 99
    return-void
.end method

.method private startLoading()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->loadingDlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 170
    :cond_0
    return-void
.end method


# virtual methods
.method protected createParamForWalletAPK()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 108
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->startLoading()V

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->requestInit()V

    .line 37
    return-void
.end method

.method protected onComplete()V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    if-nez v0, :cond_0

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->onFinalResult(Z)V

    .line 240
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->startLoading()V

    .line 189
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->getFinalMapContainer()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->completeInicisMobilePurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 239
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method protected onStartWallet()V
    .locals 3

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->createParamForWalletAPK()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;

    move-result-object v0

    .line 141
    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->onFinalResult(Z)V

    .line 162
    :goto_0
    return-void

    .line 146
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
