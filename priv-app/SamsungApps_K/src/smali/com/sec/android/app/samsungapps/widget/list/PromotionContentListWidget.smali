.class public Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

.field private d:Ljava/util/ArrayList;

.field private e:Landroid/widget/GridView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    .line 45
    const v0, 0x7f040071

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->initView(Landroid/content/Context;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    .line 50
    const v0, 0x7f040071

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->initView(Landroid/content/Context;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    .line 55
    const v0, 0x7f040071

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->initView(Landroid/content/Context;I)V

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;)Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 250
    const v0, 0x7f0c01ad

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 251
    const v1, 0x7f0c01ac

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 253
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 255
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 257
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    const v0, 0x7f0c01a7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 94
    const v1, 0x7f0c01a9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 95
    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 98
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 99
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 100
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/ae;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/ae;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0800d6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 116
    :goto_0
    return-void

    .line 112
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 113
    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 114
    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;)V
    .locals 6

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->CURATED_PRODUCTLIST:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListDescription()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getIndex()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public displayTitle()V
    .locals 3

    .prologue
    .line 264
    const v0, 0x7f0c004d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 266
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    const v1, 0x7f0801f6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public getHorizontalContentList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumColumns()I
    .locals 4

    .prologue
    const v3, 0x7f0d0001

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 222
    const v2, 0x7f070027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 223
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/2addr v0, v1

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 230
    :cond_0
    return v0
.end method

.method public loadWidget()V
    .locals 0

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->updateWidget()V

    .line 90
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 307
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->refreshWidget()V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;->configurationChangedComplete()V

    .line 312
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 121
    if-eqz v0, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    const/4 v2, 0x7

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    .line 124
    :cond_0
    return-void
.end method

.method public refreshItemColumns()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->getNumColumns()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/af;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/af;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->updateWidget()V

    .line 280
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 76
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 83
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->removeAllViews()V

    .line 86
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 65
    return-void
.end method

.method public setGridView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 340
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/GridView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setVisibleNodata(I)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 316
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 318
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 319
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 320
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 322
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 323
    const v1, 0x7f0c0081

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 325
    const v1, 0x7f0801d0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    .line 61
    return-void
.end method

.method public updateWidget()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 141
    :cond_2
    invoke-direct {p0, v6}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a(Z)V

    .line 143
    const-string v0, "kimss"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateWidget : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getIndex()I

    move-result v0

    if-ne v0, v5, :cond_3

    .line 146
    const v0, 0x7f0c01a6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_3

    .line 148
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->size()I

    move-result v2

    move v1, v6

    .line 155
    :goto_1
    if-ge v1, v2, :cond_6

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 159
    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_4
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 166
    :cond_5
    if-eqz v0, :cond_4

    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_2

    .line 172
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 174
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->getNumColumns()I

    move-result v1

    .line 175
    const v2, 0x7f0c0142

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 176
    const v3, 0x7f0c01ab

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 178
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 179
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 181
    if-le v0, v1, :cond_8

    .line 182
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a(Z)V

    .line 183
    add-int/lit8 v0, v0, -0x1

    :goto_4
    if-lt v0, v1, :cond_8

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 183
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    :cond_7
    move v0, v6

    .line 172
    goto :goto_3

    .line 188
    :cond_8
    const v0, 0x7f0c00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setFocusableInTouchMode(Z)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->setGridView()V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-nez v0, :cond_9

    .line 192
    new-instance v0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f040069

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->d:Ljava/util/ArrayList;

    const/16 v4, 0x63

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;II)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 198
    :goto_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->refreshItemColumns()V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->e:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 201
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->setVisibleLoading(I)Z

    .line 208
    :goto_6
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->displayTitle()V

    .line 209
    const v0, 0x7f0c01ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListDescription()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->getListDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v6}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a(I)V

    goto/16 :goto_0

    .line 195
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    goto :goto_5

    .line 205
    :cond_a
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->setVisibleNodata(I)Z

    goto :goto_6

    .line 209
    :cond_b
    invoke-direct {p0, v7}, Lcom/sec/android/app/samsungapps/widget/list/PromotionContentListWidget;->a(I)V

    goto/16 :goto_0
.end method
