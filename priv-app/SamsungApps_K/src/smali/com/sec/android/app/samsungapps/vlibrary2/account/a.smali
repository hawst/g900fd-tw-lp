.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasRegisteredCard()Z
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->hasCard()Z

    move-result v0

    return v0
.end method

.method public final setCardInfo(Z)V
    .locals 2

    .prologue
    .line 71
    if-eqz p1, :cond_0

    .line 73
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    .line 75
    :cond_0
    return-void
.end method
