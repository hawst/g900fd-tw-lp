.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract calcPaymentPrice(D)D
.end method

.method public abstract checkAvailablity(D)V
.end method

.method public abstract enable(Z)V
.end method

.method public abstract getAvailablePeriodEndDate()Ljava/lang/String;
.end method

.method public abstract getAvailablePeriodStartDate()Ljava/lang/String;
.end method

.method public abstract getCouponID()Ljava/lang/String;
.end method

.method public abstract getCouponIssuedSEQ()Ljava/lang/String;
.end method

.method public abstract getCouponName()Ljava/lang/String;
.end method

.method public abstract getCouponSeq()Ljava/lang/String;
.end method

.method public abstract getCurrencyUnit()Ljava/lang/String;
.end method

.method public abstract getDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;
.end method

.method public abstract getDiscountPrice()D
.end method

.method public abstract getDiscountRate()D
.end method

.method public abstract getDiscountType()I
.end method

.method public abstract getMaxPrice()D
.end method

.method public abstract getMinPrice()D
.end method

.method public abstract getUsageCount()I
.end method

.method public abstract getUsageCountRemain()I
.end method

.method public abstract isRemainPriceLost(D)Z
.end method

.method public abstract setDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;)V
.end method
