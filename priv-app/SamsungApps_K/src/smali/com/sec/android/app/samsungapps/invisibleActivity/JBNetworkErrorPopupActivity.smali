.class public Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    .line 84
    new-instance v0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/a;-><init>(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    .line 145
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 142
    iget v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->c:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 143
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;->notifiedAndEndApp()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    .line 60
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 23
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    .line 54
    :goto_0
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 29
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JBNetworkErrorPopupActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    goto :goto_0

    .line 39
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "errorType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->c:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 45
    :goto_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    .line 48
    const-class v0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :catch_1
    move-exception v0

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JBNetworkErrorPopupActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 52
    :cond_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->c:I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.sec.android.app.popupuireceiver"

    const-string v3, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "network_err_type"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "mobile_data_only"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :try_start_2
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception:: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    const-class v0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onDestroy()V

    .line 82
    return-void
.end method
