.class public Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;


# instance fields
.field mSystemRequestHandler:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

.field mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestHandler:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 20
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;

    .line 14
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;

    return-object v0
.end method


# virtual methods
.method protected addRequest(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;)V
    .locals 2

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 25
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 26
    const/4 v0, 0x1

    .line 28
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->add(Ljava/lang/Object;)Z

    .line 29
    if-eqz v0, :cond_1

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->callQueueHandlerIfQueueNotEmpty()V

    .line 32
    :cond_1
    return-void
.end method

.method public addSystemRequestQueueHandler(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueueHandler;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestHandler:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method protected callQueueHandlerIfQueueNotEmpty()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestHandler:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueueHandler;

    .line 40
    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueueHandler;->handleSystemRequest(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;)V

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method

.method public getFirstRequest()Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeSystemRequestQueueHandler(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueueHandler;)Z
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestHandler:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 61
    :cond_0
    return v0
.end method

.method protected requestProcessed(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->mSystemRequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->remove(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->callQueueHandlerIfQueueNotEmpty()V

    .line 49
    return-void
.end method
