.class final Lcom/sec/android/app/samsungapps/lf;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SignUpActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 3

    .prologue
    const/16 v2, 0xe

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0, p2, p3, p4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Lcom/sec/android/app/samsungapps/SignUpActivity;III)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 454
    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Lcom/sec/android/app/samsungapps/SignUpActivity;III)V

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    const v2, 0x7f0c02dd

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 462
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Lcom/sec/android/app/samsungapps/SignUpActivity;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->f(Lcom/sec/android/app/samsungapps/SignUpActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->g(Lcom/sec/android/app/samsungapps/SignUpActivity;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->g(Lcom/sec/android/app/samsungapps/SignUpActivity;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->calcAge(III)I

    move-result v0

    .line 470
    if-gez v0, :cond_3

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->h(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    goto :goto_0

    .line 473
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->getAgeLimitation()I

    move-result v0

    .line 474
    if-ne v0, v2, :cond_4

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Lcom/sec/android/app/samsungapps/SignUpActivity;I)V

    goto :goto_0

    .line 477
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lf;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    const/16 v1, 0x12

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Lcom/sec/android/app/samsungapps/SignUpActivity;I)V

    goto :goto_0
.end method
