.class public Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListGetter;


# instance fields
.field _CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

.field _Listeners:Ljava/util/ArrayList;

.field private _SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_Listeners:Ljava/util/ArrayList;

    .line 21
    return-void
.end method


# virtual methods
.method public addListener(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public getCouponContainer()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    return-object v0
.end method

.method public getSelCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    return-object v0
.end method

.method public hasList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public removeListener(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 85
    return-void
.end method

.method public request(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->requestPurchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 31
    return-void
.end method

.method public requestPurchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/a;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->purchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 56
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;

    .line 59
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;->onRequestCouponList()V

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method

.method public setSelCoupon(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;->findCouponBySeq(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;

    .line 68
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CCouponListGetter;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;->onSelCouponChanged(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method
