.class public Lcom/sec/android/app/samsungapps/widget/AboutWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

.field private d:Z

.field private final e:Ljava/lang/String;

.field protected scrollY:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->d:Z

    .line 40
    const-string v0, "AboutWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->e:Ljava/lang/String;

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->d:Z

    .line 40
    const-string v0, "AboutWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->e:Ljava/lang/String;

    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->d:Z

    .line 40
    const-string v0, "AboutWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->e:Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    if-nez v0, :cond_0

    .line 110
    const-string v0, "AboutWidget::_bindView::Helper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 193
    :goto_0
    return-void

    .line 114
    :cond_0
    const v0, 0x7f0c002a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 115
    if-eqz v0, :cond_1

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    const-string v2, "isa_samsungapps_icon"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    :cond_1
    const v0, 0x7f0c002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    const v0, 0x7f0c002c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 124
    const v1, 0x7f0c002d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 125
    const v2, 0x7f0c002f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 126
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-nez v2, :cond_3

    .line 128
    :cond_2
    const-string v0, "AboutWidget::_bindView::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    const v4, 0x7f0802f3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    const v4, 0x7f080294

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->getCurrentVersion()Ljava/lang/String;

    move-result-object v4

    .line 139
    const-string v5, "%s %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v3, v6, v7

    aput-object v4, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0801ed

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->getLatestVersion()Ljava/lang/String;

    move-result-object v3

    .line 146
    const-string v4, "(%s %s)"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v0, v5, v7

    aput-object v3, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    const v0, 0x7f0c002e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->isUpdateAvailable()Z

    move-result v1

    if-eq v1, v8, :cond_6

    .line 155
    if-eqz v0, :cond_4

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080165

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :cond_4
    invoke-virtual {v2, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 159
    invoke-virtual {v2, v7}, Landroid/widget/Button;->setFocusable(Z)V

    .line 170
    :goto_1
    const v0, 0x7f0c0030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 171
    const v1, 0x7f0c0032

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 172
    const v2, 0x7f0c0031

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 173
    const v3, 0x7f0c0033

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 174
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    if-nez v1, :cond_8

    .line 176
    :cond_5
    const-string v0, "AboutWidget::_bindView::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    :cond_6
    if-eqz v0, :cond_7

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080160

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    :cond_7
    invoke-virtual {v2, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 167
    invoke-virtual {v2, v8}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_1

    .line 180
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 181
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<u>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "</u>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "<u>"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "</u>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<u>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</u>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<u>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</u>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 183
    :cond_9
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    .line 75
    const v0, 0x7f040003

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->initView(Landroid/content/Context;I)V

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/AboutWidget;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 33
    const v0, 0x7f0c0034

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    if-nez v1, :cond_1

    :cond_0
    const-string v0, "AboutWidget_onReceivedNetResult::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    if-ne p1, v3, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->getHelpText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->changText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->setFocusable(Z)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->setFocusableInTouchMode(Z)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->requestFocus()Z

    :goto_1
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/h;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/h;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->post(Ljava/lang/Runnable;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getIsItemClicked()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->d:Z

    return v0
.end method

.method public loadWidget()V
    .locals 6

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    if-nez v0, :cond_1

    .line 92
    :cond_0
    const-string v0, "AboutWidget::loadWidget::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a()V

    .line 97
    const v0, 0x7f0c0030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0032

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c0031

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0c0033

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0c002f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    if-nez v1, :cond_3

    :cond_2
    const-string v0, "AboutWidget::_registerListener::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 99
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/f;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/g;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    goto :goto_0

    .line 97
    :cond_3
    new-instance v5, Lcom/sec/android/app/samsungapps/widget/a;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/widget/a;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/b;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/c;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/d;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/e;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidget;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 370
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 81
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 82
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    .line 84
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 85
    return-void
.end method

.method public setIsItemClicked(Z)V
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->d:Z

    .line 50
    return-void
.end method

.method public setWidgetClickListener(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 378
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;

    .line 379
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 384
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->a:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 385
    return-void
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 364
    return-void
.end method
