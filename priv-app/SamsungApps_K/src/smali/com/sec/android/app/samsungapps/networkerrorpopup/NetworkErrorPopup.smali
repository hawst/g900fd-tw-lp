.class public Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;


# instance fields
.field a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;->a:Z

    return-void
.end method


# virtual methods
.method public showAirplaneMode(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IConfirm;)V
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const v1, 0x7f080276

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0xff0c

    invoke-static {p1, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 45
    const v1, 0x7f0802ef

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/networkerrorpopup/a;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/networkerrorpopup/a;-><init>(Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IConfirm;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 54
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 55
    return-void
.end method

.method public showChinaNetworkWarningPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IChinaNetworkWarninigSetter;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 133
    if-eqz p4, :cond_1

    .line 135
    const v0, 0x7f080117

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 136
    const v0, 0xff48

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    new-instance v2, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;

    invoke-direct {v2, p1, v1, v0, v7}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 148
    const v0, 0x7f0802ef

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/sec/android/app/samsungapps/networkerrorpopup/e;

    move-object v1, p0

    move v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/networkerrorpopup/e;-><init>(Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;ZLcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IChinaNetworkWarninigSetter;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;)V

    invoke-virtual {v2, v6, v0}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 169
    const v0, 0x7f08023d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/networkerrorpopup/f;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/networkerrorpopup/f;-><init>(Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IOnOkObserver;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 179
    new-array v0, v7, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setDisableTouchFromOutside()V

    .line 183
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->show()Z

    .line 184
    return-void

    .line 140
    :cond_1
    const v0, 0x7f0801e3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    const v0, 0xff49

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public showJellyBeanNetworkPopup(Landroid/content/Context;ILcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;)V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 35
    const-string v1, "errorType"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 36
    const-class v1, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-static {p1, v1, p3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method public showNetworkDisconnectedPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IRetryObserver;)V
    .locals 3

    .prologue
    .line 60
    const-string v0, "Network is not available"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 62
    new-instance v1, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const v0, 0x7f040077

    invoke-direct {v1, p1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;I)V

    .line 63
    const v0, 0x7f0801da

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/networkerrorpopup/b;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/networkerrorpopup/b;-><init>(Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IRetryObserver;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 76
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v2, 0x7f0c0038

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/networkerrorpopup/c;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/samsungapps/networkerrorpopup/c;-><init>(Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnConfigurationChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;)V

    .line 107
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/networkerrorpopup/d;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/networkerrorpopup/d;-><init>(Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IRetryObserver;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 125
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 126
    return-void
.end method
