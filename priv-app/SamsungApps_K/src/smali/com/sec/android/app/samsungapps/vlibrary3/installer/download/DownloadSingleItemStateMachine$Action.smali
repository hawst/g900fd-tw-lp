.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ADD_DLSTATE_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum ADD_QUICKPANEL_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum CHECK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum CLEAR_CONTENT_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum CLEAR_OLD_DOWNLOAD_PROGRESS_VAR:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum DEQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum DL_STATE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum ENQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum FORCE_UPDATE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum INSTALL_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_DOWNLOADING_TO_GAMEHUB:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_INSTALL_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_STATE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum REMOVE_DL_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum REMOVE_QUICKPANEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum REQUEST_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum REQ_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum SET_DL_STATE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum SET_DL_STATE_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum SET_DOWNLOADURI_FROMPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum SET_QUICKPANEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum SET_QUICKPANEL_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum SET_QUICKPANEL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field public static final enum URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "URL_REQUEST"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "REQUEST_RES_LOCK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "REQUEST_DOWNLOAD"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_CANCELED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "CANCEL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "REQ_INSTALL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQ_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "REQUEST_PAYMENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "ADD_QUICKPANEL_WAITING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ADD_QUICKPANEL_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "SET_QUICKPANEL_DOWNLOADING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "SET_QUICKPANEL_FAILED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "SET_QUICKPANEL_COMPLETED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "REMOVE_QUICKPANEL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_QUICKPANEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "RELEASE_RES_LOCK"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "ADD_DLSTATE_WAITING"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ADD_DLSTATE_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "SET_DL_STATE_DOWNLOADING"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DL_STATE_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "CHECK_DETAIL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CHECK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "SET_DL_STATE_COMPLETE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DL_STATE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "ENQUEUE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ENQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "DEQUEUE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DEQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_STATE_CHANGED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_STATE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "DL_STATE_INSTALLING"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DL_STATE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "REMOVE_DL_STATE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_DL_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "SET_DOWNLOADURI_FROMPAYMENT"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DOWNLOADURI_FROMPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "FORCE_UPDATE_DETAIL"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->FORCE_UPDATE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_DOWNLOADING_TO_GAMEHUB"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_DOWNLOADING_TO_GAMEHUB:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_PAYMENT_SUCCESS"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "CLEAR_OLD_DOWNLOAD_PROGRESS_VAR"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CLEAR_OLD_DOWNLOAD_PROGRESS_VAR:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "INSTALL_CANCEL"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->INSTALL_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "NOTIFY_INSTALL_FAIL_CODE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_INSTALL_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "CLEAR_CONTENT_DETAIL"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CLEAR_CONTENT_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "START_TIMER"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    const-string v1, "STOP_TIMER"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    .line 20
    const/16 v0, 0x22

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQ_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ADD_QUICKPANEL_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_QUICKPANEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_QUICKPANEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->RELEASE_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ADD_DLSTATE_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DL_STATE_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CHECK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DL_STATE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->ENQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DEQUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_STATE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->DL_STATE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->REMOVE_DL_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->SET_DOWNLOADURI_FROMPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->FORCE_UPDATE_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_DOWNLOADING_TO_GAMEHUB:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CLEAR_OLD_DOWNLOAD_PROGRESS_VAR:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->INSTALL_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->NOTIFY_INSTALL_FAIL_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->CLEAR_CONTENT_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$Action;

    return-object v0
.end method
