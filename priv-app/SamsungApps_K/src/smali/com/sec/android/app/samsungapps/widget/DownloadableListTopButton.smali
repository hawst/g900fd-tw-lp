.class public Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

.field private b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

.field private d:I

.field private e:I

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 14
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 15
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 16
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->d:I

    .line 17
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->e:I

    .line 23
    const v0, 0x7f040049

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->initView(Landroid/content/Context;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 15
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 16
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->d:I

    .line 17
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->e:I

    .line 28
    const v0, 0x7f040049

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->initView(Landroid/content/Context;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 15
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 16
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->d:I

    .line 17
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->e:I

    .line 33
    const v0, 0x7f040049

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->initView(Landroid/content/Context;I)V

    .line 34
    return-void
.end method

.method private a(Landroid/content/res/Configuration;)I
    .locals 2

    .prologue
    .line 209
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 212
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->d:I

    div-int/lit8 v0, v0, 0x2

    .line 217
    :goto_0
    return v0

    .line 214
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->e:I

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 153
    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 154
    const v1, 0x7f0c0157

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 160
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 162
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 170
    :pswitch_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 165
    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;)Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f0c0159

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    .line 44
    const v0, 0x7f0c0156

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->updateWidget()V

    .line 53
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setTabTextSize(Landroid/content/res/Configuration;)V

    .line 54
    return-void

    .line 51
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->updateWidget()V

    .line 80
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->clear()V

    .line 96
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 98
    return-void
.end method

.method public setClickListener(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton$IDownloadableListener;

    .line 38
    return-void
.end method

.method public setEnableButton(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a(I)V

    .line 113
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 119
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setEnableButton(IZ)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 141
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 137
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setTabTextSize(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/16 v4, 0xf

    const/4 v3, 0x1

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    if-nez p1, :cond_2

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    .line 230
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->d:I

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->e:I

    .line 239
    :cond_2
    :goto_1
    const v0, 0x7f0c0158

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 240
    if-eqz v0, :cond_3

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 244
    :cond_3
    const v0, 0x7f0c015b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 245
    if-eqz v0, :cond_0

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 234
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->e:I

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->d:I

    goto :goto_1
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 85
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 86
    return-void
.end method

.method public updateWidget()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 64
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    .line 68
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    if-nez v0, :cond_2

    .line 74
    :cond_0
    :goto_1
    return-void

    .line 66
    :cond_1
    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setEnableButton(IZ)V

    goto :goto_0

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->g:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/n;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/n;-><init>(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->f:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/o;-><init>(Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 72
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/DownloadableListTopButton;->setVisibility(I)V

    goto :goto_1
.end method
