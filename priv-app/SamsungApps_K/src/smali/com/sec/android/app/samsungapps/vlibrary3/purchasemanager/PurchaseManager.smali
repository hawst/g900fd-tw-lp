.class public Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->e:Landroid/os/Handler;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->b:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    .line 33
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->e:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 134
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V

    .line 139
    return-void
.end method

.method public getResultURI()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->getURLContainer()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;)V
    .locals 3

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 56
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->paymentCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->b:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 62
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)V

    goto :goto_0

    .line 65
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;->onPaymentFailed()V

    goto :goto_0

    .line 71
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;->onPaymentSuccess()V

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManager$IPurchaseManagerObserver;

    .line 49
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    .line 38
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;)V

    return-void
.end method
