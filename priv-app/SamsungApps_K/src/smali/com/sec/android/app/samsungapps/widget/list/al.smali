.class final Lcom/sec/android/app/samsungapps/widget/list/al;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 176
    sub-int v0, p4, p3

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;I)I

    .line 178
    if-lt p2, v0, :cond_0

    if-eqz p4, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->isEOF()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->saveOldY(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->loadMoreCommentList()V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/al;->a:Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method
