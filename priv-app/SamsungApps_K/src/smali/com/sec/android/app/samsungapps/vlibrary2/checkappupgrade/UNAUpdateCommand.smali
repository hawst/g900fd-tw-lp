.class public Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;
.source "ProGuard"


# instance fields
.field _bNeedODCUpdate:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_bNeedODCUpdate:Z

    .line 16
    iput-boolean p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_bNeedODCUpdate:Z

    .line 17
    return-void
.end method


# virtual methods
.method protected exitSamsungApps()V
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_bNeedODCUpdate:Z

    if-nez v0, :cond_0

    .line 62
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    .line 64
    :cond_0
    return-void
.end method

.method protected getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "__UNIQUE_UNA"

    return-object v0
.end method

.method protected getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "_UNIQUE_UNA"

    return-object v0
.end method

.method protected getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    const-string v0, "UNA"

    return-object v0
.end method

.method protected getSizeToDownload()J
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    .line 38
    if-nez v0, :cond_0

    .line 40
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 41
    const-wide/16 v0, 0x0

    .line 43
    :goto_0
    return-wide v0

    :cond_0
    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->bdSize:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method protected getURLToDownload()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    .line 52
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 53
    const-string v0, ""

    .line 55
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->bdUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method protected notifyInstallCompleted()V
    .locals 4

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_bNeedODCUpdate:Z

    if-nez v0, :cond_0

    .line 71
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->notifyInstallCompleted()V

    .line 84
    :goto_0
    return-void

    .line 75
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateAuto;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateAuto;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/u;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/u;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/UNAUpdateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateAuto;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
