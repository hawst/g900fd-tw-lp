.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILogin:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;->_ILogin:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 15
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 19
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;->_ILogin:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/q;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/q;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginValidator;->onFinalResult(Z)V

    goto :goto_0
.end method
