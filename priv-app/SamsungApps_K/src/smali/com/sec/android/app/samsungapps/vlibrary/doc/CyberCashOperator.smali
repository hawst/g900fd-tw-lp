.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mName:Ljava/lang/String;

.field mPaymentID:Ljava/lang/String;

.field mUrl:Ljava/lang/String;

.field seqStr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->mName:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->mPaymentID:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->mUrl:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->seqStr:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentMethodID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->mPaymentID:Ljava/lang/String;

    return-object v0
.end method

.method public getSeqNumberString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->seqStr:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->mUrl:Ljava/lang/String;

    return-object v0
.end method
