.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/io/File;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->c:Ljava/io/File;

    .line 33
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    .line 34
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 119
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallSuccess()V

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setState(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed(Ljava/lang/String;)V

    .line 146
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setState(Ljava/lang/Object;)V

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a()V

    .line 148
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed()V

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setState(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onForegroundInstalling()V

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setState(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V
    .locals 2

    .prologue
    .line 13
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->c:Ljava/io/File;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/File;->setReadable(Z)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/af;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V
    .locals 2

    .prologue
    .line 13
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/af;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "W:14202_DL_FAILED"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected entry()V
    .locals 4

    .prologue
    .line 71
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/af;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 84
    :goto_0
    :pswitch_0
    return-void

    .line 74
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/af;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    const-string v0, "W:14201_INVALID"

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->c:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;->createWithoutExpectedSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ae;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ae;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;)V

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->request()V

    goto :goto_0

    .line 81
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ad;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ad;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 74
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
    .end packed-switch
.end method

.method protected exit()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getIdleState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    move-result-object v0

    return-object v0
.end method

.method public install()V
    .locals 2

    .prologue
    .line 38
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/af;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 42
    :goto_0
    return-void

    .line 41
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->DOWNLOAD_SIGNATURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    .line 61
    return-void
.end method

.method public userCancel()V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller$State;

    if-ne v0, v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/WGTInApkWithSignatureInstaller;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->userCancel()V

    .line 56
    :cond_0
    return-void
.end method
