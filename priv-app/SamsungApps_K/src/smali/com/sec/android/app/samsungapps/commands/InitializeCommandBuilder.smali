.class public Lcom/sec/android/app/samsungapps/commands/InitializeCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 10
    new-instance v2, Lcom/sec/android/app/samsungapps/commands/CountrySearchCommandBuilder;

    new-instance v0, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/commands/CountrySearchCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;)V

    new-instance v3, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getCheckAppUpgradeCommandBuilder()Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;

    invoke-direct {v5, p1}, Lcom/sec/android/app/samsungapps/commands/DisclaimerCommandBuilder;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;)V

    .line 12
    return-void
.end method
