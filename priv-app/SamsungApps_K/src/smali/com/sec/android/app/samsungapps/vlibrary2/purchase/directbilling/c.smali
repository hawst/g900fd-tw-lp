.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 120
    if-ne p2, v1, :cond_2

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->isFinalMapSuccess()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->retry()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V

    goto :goto_0

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->access$600(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V

    goto :goto_0
.end method
