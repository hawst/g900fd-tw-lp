.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field count:I

.field mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->count:I

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 13
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 17
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method protected onPostParseError()V
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setNeverLoaded(Z)V

    .line 25
    :cond_0
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 29
    const-string v0, "startNum"

    invoke-virtual {p1, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 30
    if-gt v0, v5, :cond_3

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->clear()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->count:I

    .line 40
    :goto_0
    const-string v0, "totalCount"

    invoke-virtual {p1, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 41
    const-string v1, "endNum"

    invoke-virtual {p1, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 42
    if-eq v1, v4, :cond_0

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setIndexOfLastItem(I)V

    .line 46
    :cond_0
    if-eq v0, v4, :cond_1

    if-ne v1, v4, :cond_4

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setCompleted(Z)V

    .line 56
    :cond_2
    :goto_1
    return-void

    .line 37
    :cond_3
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->count:I

    goto :goto_0

    .line 51
    :cond_4
    if-ne v0, v1, :cond_2

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setCompleted(Z)V

    goto :goto_1
.end method
