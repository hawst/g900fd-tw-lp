.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;

    .line 19
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 76
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 101
    :goto_0
    :pswitch_0
    return-void

    .line 99
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;->SAVE_ORDERID:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 100
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;->REQUEST_LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)Z
    .locals 2

    .prologue
    .line 25
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 65
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 28
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 31
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 62
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 28
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 54
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method
