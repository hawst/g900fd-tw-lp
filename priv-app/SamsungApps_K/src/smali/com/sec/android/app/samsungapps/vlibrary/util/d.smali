.class final enum Lcom/sec/android/app/samsungapps/vlibrary/util/d;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

.field public static final enum b:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

.field public static final enum c:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

.field private static final synthetic d:[Lcom/sec/android/app/samsungapps/vlibrary/util/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    const-string v1, "NOT_INSTALLED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    const-string v1, "OLDVERSION_INSTALLED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    const-string v1, "NEWVERSION_INSTALLED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->c:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->c:Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->d:[Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/d;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/util/d;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/d;->d:[Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/util/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/util/d;

    return-object v0
.end method
