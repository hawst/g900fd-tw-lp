.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mICheckRatingAuthorityCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;

.field private mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->mICheckRatingAuthorityCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->mICheckRatingAuthorityCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 26
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->mICheckRatingAuthorityCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand$ICheckRatingAuthorityCommandData;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->mMyReviewData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/d;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->ratingAuthority(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CheckRatingAuthorityCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 39
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 40
    return-void
.end method
