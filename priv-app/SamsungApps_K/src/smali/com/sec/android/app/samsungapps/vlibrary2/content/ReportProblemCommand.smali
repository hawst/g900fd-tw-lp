.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _AppDefect:Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;

.field private _ProductID:Ljava/lang/String;

.field private _ReportProblemActivity:Ljava/lang/Class;

.field private _SellerName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_AppDefect:Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_ProductID:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_SellerName:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 27
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_ReportProblemActivity:Ljava/lang/Class;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_ProductID:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_SellerName:Ljava/lang/String;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->sendReport()V

    .line 55
    return-void
.end method

.method public sendReport()V
    .locals 3

    .prologue
    .line 81
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_AppDefect:Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/content/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->reportAppDefect(Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 93
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 94
    return-void
.end method
