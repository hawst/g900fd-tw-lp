.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

.field public static final enum CREDIT_CARD_REGISTERED:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    const-string v1, "CREDIT_CARD_REGISTERED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->CREDIT_CARD_REGISTERED:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    .line 3
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->CREDIT_CARD_REGISTERED:Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;

    return-object v0
.end method
