.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public contentsSize:J

.field public downloadUri:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public paymentID:Ljava/lang/String;

.field public paymentRequestCompleteDate:Ljava/lang/String;

.field public paymentStatusCD:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public productName:Ljava/lang/String;

.field public successYn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isSuccess()Z
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;->successYn:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;->successYn:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
