.class public Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 25
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 125
    const-string v0, "SellerAppAutoUpdateManagerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 126
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 153
    :goto_0
    :pswitch_0
    return-void

    .line 129
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->CLEAR_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->CHECK_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->REQUEST_LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->REQUEST_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 139
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 140
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 143
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->SINGLE_UPDATE_AND_QUEUECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->DISPLAY_REMAIN_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 149
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 150
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 45
    const-string v0, "SellerAppAutoUpdateManagerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 46
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 120
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 49
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 52
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 60
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 71
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 81
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 84
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SINGLE_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 98
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->INCREASE_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SINGLE_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SINGLE_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 108
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->INCREASE_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 109
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SINGLE_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 112
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 115
    :pswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->REQUEST_CANCEL_ITEM:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_9
        :pswitch_d
    .end packed-switch

    .line 49
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 57
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 68
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 81
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 95
    :pswitch_data_5
    .packed-switch 0x9
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 157
    const-string v0, "SellerAppAutoUpdateManagerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    return-void
.end method
