.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/am;

.field private b:Landroid/content/Context;

.field private c:Z

.field private d:Z

.field private e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

.field private i:Ljava/util/ArrayList;

.field private j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

.field private k:I

.field private l:I

.field private m:Landroid/hardware/SensorManager;

.field private n:Landroid/hardware/Sensor;

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    .line 43
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->c:Z

    .line 48
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->d:Z

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    .line 58
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    .line 63
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    .line 68
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    .line 73
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 80
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->k:I

    .line 81
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    .line 851
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/al;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/al;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a:Lcom/sec/android/app/samsungapps/widget/am;

    .line 880
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    .line 129
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    .line 130
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 131
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    const v1, 0x7f0400c0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    .line 140
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    .line 141
    return-void

    .line 138
    :cond_0
    const v1, 0x7f0400c1

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    return-object v0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 384
    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 388
    const v0, 0x7f0c0079

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 389
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 390
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    .line 391
    if-eqz v1, :cond_9

    .line 392
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->getCount()I

    move-result v3

    .line 393
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f070043

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f070044

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    .line 398
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->k:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    if-ge v1, v2, :cond_3

    .line 401
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->k:I

    .line 405
    :goto_0
    int-to-float v1, v1

    sub-float/2addr v1, v6

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getTopIndicatorHeight(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    sub-float v2, v1, v2

    .line 408
    :cond_0
    div-float v1, v2, v4

    float-to-int v1, v1

    .line 409
    if-ge v1, v3, :cond_8

    .line 413
    :goto_1
    int-to-float v1, v1

    mul-float v3, v1, v4

    .line 415
    cmpg-float v1, v2, v3

    if-gez v1, :cond_7

    .line 419
    :goto_2
    if-eqz p2, :cond_4

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 428
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 429
    int-to-float v1, v1

    sub-float/2addr v1, v6

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getTopIndicatorHeight(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v1, v4

    .line 434
    :goto_4
    cmpg-float v4, v1, v2

    if-gez v4, :cond_6

    move v2, v3

    .line 439
    :goto_5
    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 440
    iget v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    int-to-float v4, v4

    cmpl-float v2, v4, v2

    if-nez v2, :cond_1

    const/high16 v1, -0x40000000    # -2.0f

    :cond_1
    float-to-int v1, v1

    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 441
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 444
    :cond_2
    return-void

    .line 403
    :cond_3
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    goto :goto_0

    .line 425
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    goto :goto_3

    .line 431
    :cond_5
    int-to-float v1, v1

    sub-float/2addr v1, v5

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getTopIndicatorHeight(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v1, v4

    goto :goto_4

    :cond_6
    move v1, v2

    move v2, v3

    goto :goto_5

    :cond_7
    move v2, v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_1

    :cond_9
    move v1, v2

    goto :goto_5
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 374
    new-array v3, v0, [Ljava/lang/Object;

    aput-object v2, v3, v1

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 375
    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 380
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    .line 641
    if-ltz p1, :cond_0

    if-eqz p3, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 645
    :cond_0
    const-string v0, "addItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 646
    const/4 v0, 0x0

    .line 653
    :goto_0
    return-object v0

    .line 649
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p3

    move v3, p4

    move v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 651
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addItem(ILjava/lang/String;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 663
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 667
    :cond_0
    const-string v0, "addItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 668
    const/4 v0, 0x0

    .line 675
    :goto_0
    return-object v0

    .line 671
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v3, -0x1

    move v1, p1

    move-object v2, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 673
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addItem(ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 592
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 596
    :cond_0
    const-string v0, "addItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 597
    const/4 v0, 0x0

    .line 604
    :goto_0
    return-object v0

    .line 600
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    .line 616
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 620
    :cond_0
    const-string v0, "addItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 621
    const/4 v0, 0x0

    .line 628
    :goto_0
    return-object v0

    .line 624
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 626
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addItem(ILjava/lang/String;IZZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    .line 711
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 715
    :cond_0
    const-string v0, "addItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 716
    const/4 v0, 0x0

    .line 723
    :goto_0
    return-object v0

    .line 719
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 721
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addItem(ILjava/lang/String;Z)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 6

    .prologue
    .line 686
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 690
    :cond_0
    const-string v0, "addItem::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 691
    const/4 v0, 0x0

    .line 698
    :goto_0
    return-object v0

    .line 694
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    const/4 v3, -0x1

    const/4 v4, 0x0

    move v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;-><init>(ILjava/lang/String;IZZ)V

    .line 696
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->dismiss()Z

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->clear()V

    .line 272
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    .line 273
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    .line 274
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 275
    return-void
.end method

.method public create()Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;
    .locals 4

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    if-nez v0, :cond_1

    .line 566
    :cond_0
    const-string v0, "create::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 567
    const/4 p0, 0x0

    .line 581
    :goto_0
    return-object p0

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 571
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    const v2, 0x7f040018

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    .line 575
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->setNotifyOnChange(Z)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    const v1, 0x7f0c0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 578
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 579
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 573
    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    const v2, 0x7f040019

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    goto :goto_1
.end method

.method public dismiss()Z
    .locals 2

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    .line 154
    const/4 v0, 0x1

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeSensorManager()V

    .line 159
    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;
    .locals 5

    .prologue
    .line 234
    const/4 v2, 0x0

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 238
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 239
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 242
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 249
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 250
    return-object v1

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public hasItem()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasItem(I)Z
    .locals 3

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 190
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 191
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 194
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 201
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public hasMenu()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->d:Z

    return v0
.end method

.method public initSensorManager()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->m:Landroid/hardware/SensorManager;

    .line 862
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->m:Landroid/hardware/SensorManager;

    if-nez v0, :cond_1

    .line 872
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->n:Landroid/hardware/Sensor;

    .line 867
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->m:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->n:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a:Lcom/sec/android/app/samsungapps/widget/am;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setRemoveSensorManagerListener(Lcom/sec/android/app/samsungapps/widget/am;)V

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    .line 176
    :cond_0
    return v0
.end method

.method public notifyDataChanged()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->notifyDataSetChanged()V

    .line 286
    :cond_0
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 891
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 790
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 795
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->hasCheck()Z

    move-result v1

    if-ne v1, v4, :cond_1

    .line 797
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setChecked(Z)V

    .line 798
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    if-nez v1, :cond_4

    :cond_0
    const-string v1, "updateChecked::Not Already Object"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 801
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v1, :cond_2

    .line 803
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    .line 809
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->hasChild()Z

    move-result v1

    if-ne v1, v4, :cond_a

    .line 811
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getSubItemArray()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_8

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsCreateSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z

    move-result v1

    .line 814
    if-ne v1, v4, :cond_7

    .line 816
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsOpenSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z

    move-result v1

    .line 817
    if-ne v1, v4, :cond_3

    .line 819
    invoke-virtual {v0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setParent(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V

    .line 820
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setContext(Landroid/content/Context;)V

    .line 821
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->create(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 847
    :cond_3
    :goto_1
    return-void

    .line 798
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    if-eq v1, v0, :cond_5

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setChecked(Z)V

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 826
    :cond_7
    const-string v0, "onItemClick::Can\'t create sub menu"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 831
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsOpenSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z

    move-result v1

    .line 832
    if-ne v1, v4, :cond_9

    .line 834
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->create(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto :goto_1

    .line 838
    :cond_9
    const-string v0, "onItemClick::Can\'t open sub menu"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 844
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->j:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V

    goto :goto_1
.end method

.method public onOptionConfiguration(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 485
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a(Landroid/view/View;Z)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 491
    if-eqz v0, :cond_0

    .line 493
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 499
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getTopIndicatorHeight(Landroid/content/Context;)I

    move-result v1

    add-int/2addr v0, v1

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    const/16 v3, 0x35

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setAtLocation(Landroid/view/View;III)V

    .line 529
    :cond_0
    :goto_1
    return-void

    .line 497
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 504
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/ak;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/widget/ak;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2

    .prologue
    .line 884
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 885
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->refreshOptionMenuGravity()V

    .line 887
    :cond_0
    return-void
.end method

.method public refreshOptionMenuGravity()V
    .locals 7

    .prologue
    const/16 v6, 0x55

    const/16 v5, 0x53

    const/16 v4, 0xa

    const/4 v3, -0x1

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 533
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 534
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 535
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 537
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 538
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 541
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    if-eq v0, v6, :cond_1

    .line 542
    iput v6, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setAtLocation(Landroid/view/View;III)V

    .line 556
    :cond_1
    :goto_0
    return-void

    .line 548
    :cond_2
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    if-eq v0, v5, :cond_1

    .line 549
    iput v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->o:I

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method public refreshOptionmenu()V
    .locals 2

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->notifyDataChanged()V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a(Landroid/view/View;Z)V

    .line 291
    return-void
.end method

.method public removeItem(I)Z
    .locals 4

    .prologue
    .line 733
    const/4 v1, 0x0

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 737
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 738
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 740
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 741
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v3

    if-ne v3, p1, :cond_3

    .line 743
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->clear()V

    .line 744
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 745
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 747
    goto :goto_0

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    if-eqz v0, :cond_1

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->notifyDataSetChanged()V

    .line 754
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v0, :cond_2

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->update()V

    .line 759
    :cond_2
    return v1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected removeSensorManager()V
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->m:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 878
    :cond_0
    return-void
.end method

.method public setEnableItem(IZ)V
    .locals 2

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->getItem(I)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    move-result-object v0

    .line 213
    if-nez v0, :cond_1

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAppsOptionMenu::enableItem::item not found, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setEnabled(Z)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setHardKey(Z)V
    .locals 0

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->c:Z

    .line 120
    return-void
.end method

.method public setOptionMenuGravity()V
    .locals 6

    .prologue
    const/16 v5, 0x51

    const/16 v4, 0xa

    const/4 v3, -0x1

    .line 460
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 461
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 462
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 464
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 465
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    const/16 v2, 0x55

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 481
    :goto_0
    return-void

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    const/16 v2, 0x53

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v5, v3, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0

    .line 478
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v5, v3, v3}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method public setOptionMenuGravityForKitkat()V
    .locals 5

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->a(Landroid/view/View;Z)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 452
    if-eqz v0, :cond_0

    .line 453
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getTopIndicatorHeight(Landroid/content/Context;)I

    move-result v1

    add-int/2addr v0, v1

    .line 454
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    const/16 v3, 0x35

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 457
    :cond_0
    return-void
.end method

.method public setParentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    .line 111
    return-void
.end method

.method public show()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const v7, 0x7f070039

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->f:Landroid/view/View;

    if-nez v3, :cond_1

    .line 304
    :cond_0
    const-string v1, "show::Not Already Obejct"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 368
    :goto_0
    return v0

    .line 308
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->h:Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->d:Z

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    if-nez v3, :cond_3

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 317
    if-nez v3, :cond_2

    .line 319
    const-string v1, "SamsungAp;psOptionMenu::show::metrics is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->k:I

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    .line 327
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->c:Z

    if-ne v0, v2, :cond_5

    .line 328
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->k:I

    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    if-le v0, v3, :cond_4

    .line 330
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->l:I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 340
    :goto_1
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    const/4 v4, -0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setHeight(I)V

    .line 342
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setWidth(I)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setFocusable(Z)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setTouchable(Z)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 347
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 351
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setContentView(Landroid/view/View;)V

    .line 353
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->c:Z

    if-nez v0, :cond_9

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setAnimationStyle(I)V

    .line 360
    :cond_3
    :goto_4
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->c:Z

    if-ne v0, v2, :cond_a

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setOptionMenuGravity()V

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->initSensorManager()V

    :goto_5
    move v0, v2

    .line 368
    goto/16 :goto_0

    .line 334
    :cond_4
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->k:I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    goto :goto_1

    .line 337
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f07003c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    .line 347
    goto :goto_2

    .line 349
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    if-eqz v3, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f02016f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_8
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 356
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->e:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    const v1, 0x7f0900cc

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->setAnimationStyle(I)V

    goto :goto_4

    .line 364
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setOptionMenuGravityForKitkat()V

    goto :goto_5
.end method
