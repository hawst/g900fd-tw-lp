.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand$ICountrySearchCommandInteractor;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract checkQAPassword()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract clearCache()V
.end method

.method public abstract countrySearchRequired()Z
.end method

.method public abstract isCountryListSearchMode()Z
.end method

.method public abstract isMCCAvailable()Z
.end method

.method public abstract isTestMode()Z
.end method

.method public abstract requestCountrySearch()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestCountrySearchEx()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract selectCountryList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract validateDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
