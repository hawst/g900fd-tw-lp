.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.source "ProGuard"


# instance fields
.field mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

.field mSellerDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;

.field mSellerID:Ljava/lang/String;

.field mSellerName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V
    .locals 2

    .prologue
    .line 19
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerID:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerName:Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerID:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerName:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method protected createRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 1

    .prologue
    .line 38
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    .line 39
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->sellerProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method public getContentDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    return-object v0
.end method

.method public getSellerDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;

    return-object v0
.end method

.method public getSellerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerID:Ljava/lang/String;

    return-object v0
.end method

.method public getSellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerName:Ljava/lang/String;

    return-object v0
.end method

.method public setSellerDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->mSellerDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/SellerDetail;

    .line 55
    return-void
.end method
