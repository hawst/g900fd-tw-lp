.class public Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# static fields
.field private static b:Z


# instance fields
.field a:Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field protected mDeviceType:Lcom/sec/android/allshare/Device$DeviceType;

.field protected mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;

    .line 47
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/sec/android/allshare/Device$DeviceType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mDeviceType:Lcom/sec/android/allshare/Device$DeviceType;

    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/p;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/p;-><init>(Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->a:Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 45
    sput-boolean v0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->b:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;)Z
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->a:Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/allshare/ServiceProvider;->getDeviceFinder()Lcom/sec/android/allshare/DeviceFinder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mDeviceType:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/allshare/DeviceFinder;->setDeviceFinderEventListener(Lcom/sec/android/allshare/Device$DeviceType;Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mDeviceType:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/allshare/DeviceFinder;->getDevices(Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static isDetected()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->b:Z

    return v0
.end method


# virtual methods
.method public deleteServiceProvider()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;

    if-eqz v0, :cond_0

    .line 181
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;

    invoke-static {v0}, Lcom/sec/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/sec/android/allshare/ServiceProvider;)V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 186
    :catch_0
    move-exception v0

    const-string v0, "AllShareDeviceDetectCommand::deleteServiceProvider::deleteServiceProvider::NullPointerException is occured."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected enabledWiFi()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isWIFIConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->b:Z

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->enabledWiFi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->mServiceProvider:Lcom/sec/android/allshare/ServiceProvider;

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->deleteServiceProvider()V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;->_Context:Landroid/content/Context;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/q;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/commands/q;-><init>(Lcom/sec/android/app/samsungapps/commands/AllShareDeviceDetectCommand;)V

    invoke-static {v0, v1}, Lcom/sec/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;)Lcom/sec/android/allshare/ERROR;

    move-result-object v0

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FRAMEWORK_NOT_INSTALLED:Lcom/sec/android/allshare/ERROR;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    .line 89
    :cond_1
    return-void
.end method
