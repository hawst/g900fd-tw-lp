.class public Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;
.super Lcom/sec/android/app/samsungapps/CommonActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;


# instance fields
.field private a:Z

.field private b:Landroid/text/TextWatcher;

.field private c:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field private d:Z

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

.field private f:Lcom/sec/android/app/samsungapps/LoadingDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b:Landroid/text/TextWatcher;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->d:Z

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/cz;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/cz;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 256
    const v0, 0x7f0c0237

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 257
    const v1, 0x7f0c022e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 258
    const v2, 0x7f0c022f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 259
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 261
    :cond_0
    const-string v0, "DanalMicroPaymentActivity::setTextChangedListener::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 332
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b:Landroid/text/TextWatcher;

    if-eqz v3, :cond_2

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b:Landroid/text/TextWatcher;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 273
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b:Landroid/text/TextWatcher;

    .line 285
    :cond_2
    :goto_1
    new-instance v1, Lcom/sec/android/app/samsungapps/da;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/da;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 308
    new-instance v0, Lcom/sec/android/app/samsungapps/db;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/db;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 278
    :cond_3
    new-instance v3, Landroid/telephony/PhoneNumberFormattingTextWatcher;

    invoke-direct {v3}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b:Landroid/text/TextWatcher;

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b:Landroid/text/TextWatcher;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 42
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->openDisclaimer(Landroid/content/Context;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 432
    .line 434
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    if-ne v2, v0, :cond_1

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v3, 0x7f0c0237

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v2

    .line 437
    if-eqz v2, :cond_1

    .line 439
    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 447
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v3, 0x7f0c023a

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findCheckBox(I)Landroid/widget/CheckBox;

    move-result-object v2

    .line 448
    if-eqz v2, :cond_2

    .line 450
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 459
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 460
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c022d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findSpinner(I)Landroid/widget/Spinner;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->d:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->requestAuthNumber(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b()V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)Lcom/sec/android/app/samsungapps/ActivityHelper;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a()V

    return-void
.end method


# virtual methods
.method public endLoading()V
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 677
    :cond_0
    return-void
.end method

.method public getAuthNID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v2, 0x7f0c022f

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v2, 0x7f0c0232

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthPNum()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 626
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    if-eq v0, v4, :cond_1

    .line 630
    const-string v0, ""

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->getPhoneNumSeperate(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 637
    :cond_0
    :goto_0
    return-object v0

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c022e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCarrierName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 590
    const-string v0, ""

    .line 592
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 594
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v2, 0x7f0c022d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findSpinner(I)Landroid/widget/Spinner;

    move-result-object v1

    .line 595
    if-eqz v1, :cond_0

    .line 597
    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 599
    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 608
    :cond_0
    :goto_0
    return-object v0

    .line 605
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getKoreaNetworkOperator()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCouponIssuedSEQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    if-nez v0, :cond_0

    .line 574
    const-string v0, ""

    .line 578
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->getItemToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getKoreaNetworkOperator()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 486
    const-string v0, ""

    .line 487
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v1

    .line 495
    const-string v2, "05"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_1

    .line 497
    const-string v0, "SKT"

    .line 508
    :cond_0
    :goto_0
    return-object v0

    .line 499
    :cond_1
    const-string v2, "08"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 501
    const-string v0, "KT"

    goto :goto_0

    .line 503
    :cond_2
    const-string v2, "06"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 505
    const-string v0, "LGT"

    goto :goto_0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->getItemToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserEntererdConfirmMSGNo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0237

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isPhoneSupported()Z
    .locals 1

    .prologue
    .line 477
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->iSPhoneSupported()Z

    move-result v0

    return v0
.end method

.method public isRequestAuthNumber()Z
    .locals 1

    .prologue
    .line 656
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->d:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->cancel()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    .line 87
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    .line 88
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const v9, 0x7f0c022c

    const/4 v8, 0x2

    const v7, 0x7f0c0230

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 54
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f04009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->setContentView(I)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "DanalMicroPaymentActivity::onCreate::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 69
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalView;)V

    .line 67
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0x7f0802aa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showActionBar()Ljava/lang/Boolean;

    .line 68
    const v0, 0x7f0802e6

    const v1, 0x7f08023d

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ct;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ct;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cu;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cu;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cv;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cv;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0236

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cw;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cw;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0238

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cx;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cx;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c023a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findCheckBox(I)Landroid/widget/CheckBox;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/cy;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/cy;-><init>(Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0c022d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f040021

    invoke-direct {v1, p0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f040022

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const-string v2, "SKT"

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string v2, "KT"

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string v2, "LGT"

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->isPhoneSupported()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a()V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->getPhoneNumSeperate(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-boolean v5, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    if-eq v5, v3, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findButton(I)Landroid/widget/Button;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "-"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "%s (%s)"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getKoreaNetworkOperator()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    if-eq v0, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0234

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0235

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v4

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0234

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0235

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->c:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ActivityHelper;->release()V

    .line 75
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onDestroy()V

    .line 76
    return-void
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->onBackPressed()V

    .line 683
    return-void
.end method

.method public onPaymentSuccess(Z)V
    .locals 1

    .prologue
    .line 551
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->finish()V

    .line 555
    :cond_0
    return-void
.end method

.method public onPurchasePressed()V
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->checkAgeLimitation(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)Z

    move-result v0

    .line 517
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 519
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->d:Z

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalPayment;->requestPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;)V

    .line 530
    :goto_0
    return-void

    .line 527
    :cond_0
    const v0, 0x7f080307

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 528
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto :goto_0
.end method

.method public onSendAuthNumberRequest(Z)V
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 540
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->b()V

    .line 541
    const v0, 0x7f080314

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 543
    :cond_0
    return-void
.end method

.method public phoneEnabled()Z
    .locals 1

    .prologue
    .line 650
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startLoading()V
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 664
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/DanalMicroPaymentActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 668
    return-void
.end method

.method public userAgreed()Z
    .locals 1

    .prologue
    .line 620
    const/4 v0, 0x1

    return v0
.end method
