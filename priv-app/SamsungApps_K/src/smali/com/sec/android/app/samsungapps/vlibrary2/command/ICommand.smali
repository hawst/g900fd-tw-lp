.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static Nothing:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field protected _Context:Landroid/content/Context;

.field protected _FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field private _NextFailTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _NextSuccessCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _NextSuccessTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _PrevCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _bCanceled:Z

.field private _bNotified:Z

.field private _bRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/d;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/d;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->Nothing:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 12
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_PrevCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bCanceled:Z

    .line 16
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bRunning:Z

    .line 17
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bNotified:Z

    return-void
.end method

.method public static dummyCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/b;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/b;-><init>()V

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bCanceled:Z

    if-eqz v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bCanceled:Z

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onCancel()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method public final execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bNotified:Z

    .line 22
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bCanceled:Z

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_Context:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bRunning:Z

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_PrevCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_PrevCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    new-instance p2, Lcom/sec/android/app/samsungapps/vlibrary2/command/c;

    invoke-direct {p2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    move-object p0, v0

    goto :goto_0
.end method

.method protected abstract impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
.end method

.method public final isCanceled()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bCanceled:Z

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bRunning:Z

    return v0
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method protected onFinalResult(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bCanceled:Z

    if-eqz v0, :cond_1

    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bNotified:Z

    if-nez v0, :cond_0

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bNotified:Z

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_4

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 76
    :cond_2
    :goto_1
    if-eqz p1, :cond_5

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_3

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_Context:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->Nothing:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 88
    :cond_3
    :goto_2
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_bRunning:Z

    goto :goto_0

    .line 71
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_1

    .line 84
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextFailTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextFailTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_Context:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->Nothing:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_2
.end method

.method public setNextSuccessCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 114
    return-void
.end method

.method public setNextTriggeredCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Z)V
    .locals 0

    .prologue
    .line 98
    if-eqz p2, :cond_0

    .line 99
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextSuccessTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_NextFailTriggerCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    goto :goto_0
.end method

.method public setPreExcutedCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->_PrevCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 109
    return-void
.end method
