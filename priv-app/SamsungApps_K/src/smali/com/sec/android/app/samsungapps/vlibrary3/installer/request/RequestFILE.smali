.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Landroid/os/Handler;

.field b:Ljava/io/InputStream;

.field c:I

.field d:Landroid/os/Handler;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

.field private j:Lorg/apache/http/client/methods/HttpGet;

.field private k:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field private l:Landroid/content/Context;

.field private m:Landroid/os/CountDownTimer;

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->f:Z

    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a:Landroid/os/Handler;

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->b:Ljava/io/InputStream;

    .line 177
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->k:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    .line 335
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->c:I

    .line 448
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->d:Landroid/os/Handler;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->l:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;

    .line 49
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    .line 50
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    const/4 v0, 0x2

    .line 328
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    const/4 v0, 0x1

    .line 332
    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;

    return-object v0
.end method

.method private a(ZJ)Lorg/apache/http/client/methods/HttpGet;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-object v0

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    .line 102
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStagingAppHostUrl()Ljava/lang/String;

    move-result-object v2

    .line 105
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 107
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getNewURL()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getOldHost()Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_2
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 114
    if-eqz v0, :cond_3

    .line 116
    const-string v1, "Host"

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_3
    if-eqz p1, :cond_4

    .line 122
    const-string v0, "Range"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "bytes="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 124
    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;J)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->d:Landroid/os/Handler;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lorg/apache/http/HttpResponse;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->b:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    array-length v3, v0

    if-eqz v3, :cond_0

    aget-object v3, v0, v1

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    aget-object v0, v0, v1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v3, "application/octet-stream"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HEADER_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->f:Z

    if-eqz v0, :cond_3

    const-string v0, "Content-Range"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_3
    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RESUME_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOAD_READY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_2
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/j;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 541
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    .line 396
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->URL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    .line 397
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Ljava/io/InputStream;)Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->download(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)Lorg/apache/http/client/methods/HttpGet;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->j:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method


# virtual methods
.method public cancel()Z
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    .line 185
    const/4 v0, 0x0

    return v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->k:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public handoverAgreeFromWiFiTo3G(Z)V
    .locals 1

    .prologue
    .line 545
    if-eqz p1, :cond_0

    .line 546
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HANDOVER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    .line 552
    :goto_0
    return-void

    .line 550
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HANDOVER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 200
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 203
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/b;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->open()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_COMPLETED_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_EXISTING_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_FILE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_NEW_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    .line 206
    :pswitch_5
    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(ZJ)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->j:Lorg/apache/http/client/methods/HttpGet;

    goto :goto_0

    .line 209
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->getFileSize()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(ZJ)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->j:Lorg/apache/http/client/methods/HttpGet;

    goto :goto_0

    .line 212
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 215
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 218
    :pswitch_9
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 221
    :pswitch_a
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/h;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 224
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->close()V

    goto :goto_0

    .line 227
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->deleteFile()V

    goto :goto_0

    .line 230
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;->cancel()V

    goto :goto_0

    .line 233
    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 236
    :pswitch_f
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->n:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 238
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto/16 :goto_0

    .line 242
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto/16 :goto_0

    .line 246
    :pswitch_10
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->n:I

    goto/16 :goto_0

    .line 249
    :pswitch_11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->m:Landroid/os/CountDownTimer;

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->m:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 265
    :pswitch_12
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->m:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->m:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 268
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->m:Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 272
    :pswitch_13
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->n:I

    goto/16 :goto_0

    .line 275
    :pswitch_14
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->l:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->l:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v0, "RequestFILEStateMachine"

    const-string v2, "isNetworkOn: true"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto/16 :goto_0

    :cond_3
    const-string v1, "RequestFILEStateMachine"

    const-string v2, "isNetworkOn: false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->onRequestURL()V

    goto/16 :goto_0

    .line 278
    :pswitch_15
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->add(Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate$IWaitNetworkActive;)V

    goto/16 :goto_0

    .line 281
    :pswitch_16
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->c:I

    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a()I

    move-result v0

    if-ne v0, v4, :cond_5

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NORMAL_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto/16 :goto_0

    .line 284
    :pswitch_17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->c:I

    goto/16 :goto_0

    .line 287
    :pswitch_18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;->onNeedDownloadThrough3GConnection()V

    goto/16 :goto_0

    .line 290
    :pswitch_19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->j:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->j:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    goto/16 :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 203
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;)V

    return-void
.end method

.method protected final onRequestURL()V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->h:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;->requestUpdatedURL(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload$IURLGetResult;)V

    .line 391
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public request()V
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->SEND:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    .line 62
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;

    .line 56
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->k:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    .line 191
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;)V

    return-void
.end method
