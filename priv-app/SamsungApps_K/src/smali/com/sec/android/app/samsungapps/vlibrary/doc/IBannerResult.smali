.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getBannerDescription()Ljava/lang/String;
.end method

.method public abstract getBannerIndex()I
.end method

.method public abstract getBannerLinkURL()Ljava/lang/String;
.end method

.method public abstract getBannerPosByGUI()I
.end method

.method public abstract getBannerPromotionTitle()Ljava/lang/String;
.end method

.method public abstract getBannerType()I
.end method

.method public abstract getContentDetailContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
.end method

.method public abstract getLinkedContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getURL()Ljava/lang/String;
.end method

.method public abstract isBigBanner()Z
.end method

.method public abstract isFeaturedBanner()Z
.end method

.method public abstract isInteractionBanner()Z
.end method

.method public abstract isLinkProduct()Z
.end method

.method public abstract isNewBanner()Z
.end method

.method public abstract isURLBanner()Z
.end method

.method public abstract needGetProductSetList()Z
.end method

.method public abstract needProductDetail()Z
.end method
