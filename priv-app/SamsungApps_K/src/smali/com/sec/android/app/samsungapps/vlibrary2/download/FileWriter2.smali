.class public Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;
.source "ProGuard"


# instance fields
.field private _IFileWriterInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

.field private _IFileWriterListener:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;

.field private _TotalSize:J

.field handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;)V

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->handler:Landroid/os/Handler;

    .line 15
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterListener:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;

    .line 16
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_TotalSize:J

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)J
    .locals 2

    .prologue
    .line 9
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_TotalSize:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;)Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->_IFileWriterListener:Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;

    return-object v0
.end method


# virtual methods
.method protected notifyProgress(J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgress(J)V

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->handler:Landroid/os/Handler;

    long-to-int v1, p1

    invoke-virtual {v0, v2, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 24
    return-void
.end method

.method protected notifyProgressEnd(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->notifyProgressEnd(Z)V

    .line 29
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->handler:Landroid/os/Handler;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v1, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 31
    return-void

    :cond_0
    move v0, v2

    .line 29
    goto :goto_0
.end method
