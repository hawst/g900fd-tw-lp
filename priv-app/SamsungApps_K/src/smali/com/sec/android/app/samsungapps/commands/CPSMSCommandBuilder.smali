.class public Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;->a:Landroid/content/Context;

    .line 35
    return-void
.end method


# virtual methods
.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ah;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/ah;-><init>(Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;)V

    return-object v0
.end method

.method public createSMSSender()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bw;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/commands/bw;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getAskConfirmMSGViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/al;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/al;-><init>(Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;)V

    return-object v0
.end method

.method public getNotiTNCSendFailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/aj;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/aj;-><init>(Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;)V

    return-object v0
.end method

.method public getPSMSRandomkeyViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ak;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/ak;-><init>(Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;)V

    return-object v0
.end method

.method public getTNCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ai;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/ai;-><init>(Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;)V

    return-object v0
.end method
