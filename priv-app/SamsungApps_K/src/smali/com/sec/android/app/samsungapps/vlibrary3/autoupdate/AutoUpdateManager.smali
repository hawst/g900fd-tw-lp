.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

.field private f:Landroid/os/Handler;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISelfUpdateManagerFactory;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->f:Landroid/os/Handler;

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->b:Landroid/content/Context;

    .line 38
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    .line 39
    invoke-interface {p6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISelfUpdateManagerFactory;->createSelfUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    .line 40
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    .line 41
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    .line 42
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;->onAutoUpdateFailed()V

    .line 186
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->f:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 110
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 2

    .prologue
    .line 114
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->f:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;->isOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    goto :goto_0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public isIdle()Z
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;)V
    .locals 3

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 62
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;->isOn()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->ON_NO_UPDATE_TIME:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->b:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;->createAutoUpdateChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;->check()V

    goto :goto_0

    .line 65
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;)V

    goto :goto_0

    .line 68
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a()V

    goto :goto_0

    .line 71
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;->onAutoUpdateFinished()V

    goto :goto_0

    .line 74
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->execute()V

    goto :goto_0

    .line 78
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;->onAutoUpdateSuccess()V

    goto :goto_0

    .line 81
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager$ISelfUpdateManagerObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->execute()V

    goto :goto_0

    .line 84
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->userCancel()V

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;)V

    return-void
.end method

.method public onDisplayRemainCount(I)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;->onDisplayRemainCount(I)V

    .line 213
    :cond_0
    return-void
.end method

.method public onSelfUpdateResult(Z)V
    .locals 1

    .prologue
    .line 217
    if-eqz p1, :cond_0

    .line 218
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->SELF_UPD_CHECK_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->SELF_UPD_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    goto :goto_0
.end method

.method public onSelfUpdated()V
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->SELF_UPD_UPDATED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    .line 227
    return-void
.end method

.method public onSellerAutoUpdateFailed()V
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->SELLER_UPDATE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    .line 206
    return-void
.end method

.method public onSellerAutoUpdateSuccess()V
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->SELLER_UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    .line 201
    return-void
.end method

.method public selfCancel()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;->CHECK_SELF_UPD:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    if-ne v0, v1, :cond_1

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->userCancel()V

    .line 242
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    .line 244
    :cond_1
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;

    .line 46
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    .line 51
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$State;)V

    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Event;)V

    .line 235
    return-void
.end method
