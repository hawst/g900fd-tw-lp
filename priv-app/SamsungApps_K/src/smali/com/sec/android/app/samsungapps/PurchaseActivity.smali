.class public Lcom/sec/android/app/samsungapps/PurchaseActivity;
.super Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;
.implements Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget$OnPurchaseCancelListener;


# instance fields
.field a:Z

.field b:Z

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

.field private d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

.field private e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

.field private f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

.field private g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

.field private h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

.field private i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

.field private j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

.field private k:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

.field private l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

.field private m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

.field private n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

.field private o:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field private p:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

.field private q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

.field private r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->k:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    .line 305
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a:Z

    .line 407
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->b:Z

    .line 408
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->s:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/PurchaseActivity;)V
    .locals 7

    .prologue
    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->load()V

    const v0, 0x7f0c021e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    new-instance v6, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->createIPriceCalculator()Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    move-result-object v1

    invoke-direct {v6, p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->set(Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->refresh()V

    const v0, 0x7f0c027b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->set(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->setPurchaseItemInfo(Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->refresh()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->start()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentListCreator;->createList(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->o:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    const v0, 0x7f0c0003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->p:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->o:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->p:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->set(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->loadWidget()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;->start()V

    const v0, 0x7f0c027c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->loadWidget()V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->k:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    const v0, 0x7f0c021f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->k:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->loadWidget()V

    const v0, 0x7f0c021b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getPermissionData()Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->setPermissionData(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->loadWidget()V

    const v0, 0x7f0c021c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    const v3, 0x7f08029b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->createFormatter(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->createIPriceCalculator()Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V

    invoke-virtual {v6, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->loadWidget()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setConfChangedCompleteListener(Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setOnPurchaseCancelListener(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget$OnPurchaseCancelListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->addIGiftCardCouponInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->addIPriceChangeListener(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->addIPriceChangeListener(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->addIPriceChangeListener(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->changeHeaderTailView()V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne p1, v0, :cond_1

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->p:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;->isCreditCardInfoValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;->selectPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/PurchaseActivity;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getLastPurchaseMethodType()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->contains(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPaymentMethods()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/PurchaseActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    return-object v0
.end method


# virtual methods
.method public addPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 0

    .prologue
    .line 405
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 437
    return-object p0
.end method

.method protected getFocus()V
    .locals 0

    .prologue
    .line 472
    return-void
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 381
    const v0, 0x7f0c021d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method protected getTailView()Landroid/view/View;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    return-object v0
.end method

.method protected getaddingResourceId()I
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 358
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 359
    const v0, 0x7f0c0029

    .line 363
    :goto_0
    return v0

    .line 361
    :cond_0
    const v0, 0x7f0c0148

    goto :goto_0
.end method

.method protected getremovingResourceId()I
    .locals 2

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 370
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 371
    const v0, 0x7f0c0148

    .line 375
    :goto_0
    return v0

    .line 373
    :cond_0
    const v0, 0x7f0c0029

    goto :goto_0
.end method

.method public killView()V
    .locals 0

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->finish()V

    .line 443
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a:Z

    if-nez v0, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->saveLastMethod()V

    .line 310
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a:Z

    .line 311
    const-string v0, "onBackPressed() : backpressed in Common billing UI"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->cancelPurchase()V

    .line 316
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onBackPressed()V

    .line 317
    return-void
.end method

.method public onCardDeleted()V
    .locals 0

    .prologue
    .line 454
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->onBackPressed()V

    .line 455
    return-void
.end method

.method public onCouponGetCompleted(Z)V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const v1, 0x7f0802e6

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 77
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->finish()V

    .line 107
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PurchaseActivity::ClassCastException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->finish()V

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0, p0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->invokeCompleted(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getCreditCardCommandBuilder()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardCommandBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->p:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    .line 90
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    new-instance v1, Lcom/sec/android/app/samsungapps/gc;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gc;-><init>(Lcom/sec/android/app/samsungapps/PurchaseActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->ready(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 103
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    new-instance v1, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;)V

    .line 105
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->getInstance()Lcom/sec/android/app/samsungapps/ErrorHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/ErrorHandler;->addServerErrorObserver(Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->getInstance()Lcom/sec/android/app/samsungapps/ErrorHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/ErrorHandler;->removeServerErrorObserver(Lcom/sec/android/app/samsungapps/ErrorHandler$IServerErrorObserver;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;)V

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->saveLastMethod()V

    .line 233
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onDestroy()V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->release()V

    .line 239
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a:Z

    if-nez v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->cancelPurchase()V

    .line 247
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    if-eqz v0, :cond_3

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->release()V

    .line 251
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    .line 254
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    if-eqz v0, :cond_4

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->release()V

    .line 256
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    .line 259
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    if-eqz v0, :cond_5

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->release()V

    .line 261
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    .line 264
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    if-eqz v0, :cond_6

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->release()V

    .line 266
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;

    .line 269
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    if-eqz v0, :cond_7

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->release()V

    .line 271
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->h:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    .line 274
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    if-eqz v0, :cond_8

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->release()V

    .line 276
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->i:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    .line 279
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    if-eqz v0, :cond_9

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->release()V

    .line 281
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    .line 284
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    if-eqz v0, :cond_a

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->release()V

    .line 286
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    .line 289
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    if-eqz v0, :cond_b

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;->release()V

    .line 291
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    .line 294
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    if-eqz v0, :cond_c

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->release()V

    .line 296
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    .line 299
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->n:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;

    if-eqz v0, :cond_d

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->o:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 301
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->o:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 303
    :cond_d
    return-void
.end method

.method public onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 432
    const/4 v0, 0x0

    return-object v0
.end method

.method public onPriceUpdated()V
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/PaymentInfoMgr;->notifyChangedPrice()V

    .line 448
    return-void
.end method

.method public onPurchaseCancel()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->a:Z

    .line 322
    return-void
.end method

.method public onPurchaseCompleted(Z)V
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->s:Z

    .line 413
    if-eqz p1, :cond_0

    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->b:Z

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchaseActivity;->finish()V

    .line 421
    :cond_0
    return-void
.end method

.method public onPurchaseMethodSelected(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 0

    .prologue
    .line 427
    return-void
.end method

.method public onReceiveServerError(Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 461
    const-string v0, "5107"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->execute()V

    .line 466
    :cond_0
    return-void
.end method

.method public onRequestCouponList()V
    .locals 0

    .prologue
    .line 399
    return-void
.end method

.method protected requestFocus()V
    .locals 0

    .prologue
    .line 478
    return-void
.end method

.method protected saveLastMethod()V
    .locals 3

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    if-nez v0, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;->size()I

    move-result v1

    .line 331
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    .line 333
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 335
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchaseActivity;->m:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodMgr;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->getPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastPurchaseMethodType(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    goto :goto_0

    .line 331
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public updateView()V
    .locals 0

    .prologue
    .line 388
    return-void
.end method
