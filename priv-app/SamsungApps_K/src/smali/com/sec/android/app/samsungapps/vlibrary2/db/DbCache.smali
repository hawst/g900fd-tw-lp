.class public Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field _DBLength:I

.field dbcache:[Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->dbcache:[Ljava/lang/String;

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->_DBLength:I

    .line 9
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->_DBLength:I

    .line 10
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->_DBLength:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->dbcache:[Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->_DBLength:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->dbcache:[Ljava/lang/String;

    .line 39
    return-void
.end method

.method public get(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->dbcache:[Ljava/lang/String;

    aget-object v0, v0, p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    :goto_0
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 21
    const-string v0, ""

    goto :goto_0
.end method

.method public set(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->dbcache:[Ljava/lang/String;

    aput-object p2, v0, p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
