.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_TIMEOUT_FOR_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field public static final enum NOTIFY_FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field public static final enum REGISTER_NOTIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field public static final enum REQUEST_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field public static final enum START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field public static final enum STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "NOTIFY_FINISH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "REQUEST_UPDATECHECK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->REQUEST_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "REGISTER_NOTIFICATION"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->REGISTER_NOTIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "CHECK_TIMEOUT_FOR_UPDATE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->CHECK_TIMEOUT_FOR_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "START_TIMER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    const-string v1, "STOP_TIMER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    .line 17
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->REQUEST_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->REGISTER_NOTIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->CHECK_TIMEOUT_FOR_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    return-object v0
.end method
