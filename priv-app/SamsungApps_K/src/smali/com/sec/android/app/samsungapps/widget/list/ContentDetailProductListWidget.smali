.class public Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final CATEGORY_PRODUCT_LIST:I = 0x1

.field public static final SELLER_PRODUCT_LIST:I


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field private g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

.field private final h:I

.field private final i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 34
    const-string v0, "ContentDetailProductListWidget::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->a:Ljava/lang/String;

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->h:I

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->i:I

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    .line 58
    const v0, 0x7f040035

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->initView(Landroid/content/Context;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-string v0, "ContentDetailProductListWidget::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->a:Ljava/lang/String;

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->h:I

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->i:I

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    .line 64
    const v0, 0x7f040035

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->initView(Landroid/content/Context;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const-string v0, "ContentDetailProductListWidget::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->a:Ljava/lang/String;

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->h:I

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->i:I

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    .line 71
    const v0, 0x7f040035

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->initView(Landroid/content/Context;I)V

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 231
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    .line 232
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->k:I

    .line 237
    :goto_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->k:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->j:I

    .line 239
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 240
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->j:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->j:I

    .line 242
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ContentDetailProductListWidget::setMaxDspGridItem() mMaxDspGridItem ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 243
    return-void

    .line 234
    :cond_1
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->k:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    if-nez v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->size()I

    move-result v4

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v2, v3

    .line 254
    :goto_1
    if-ge v2, v4, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;

    move-result-object v1

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v5

    move-object v0, v1

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 258
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->j:I

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v0, v5, :cond_2

    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->m:Z

    goto :goto_0

    .line 264
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->m:Z

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->updateWidget()V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->a()V

    .line 224
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b()V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->updateWidget()V

    .line 226
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 107
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 108
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 76
    return-void
.end method

.method public setListType(I)V
    .locals 0

    .prologue
    .line 217
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    .line 218
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 84
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 85
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->l:Z

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->a()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->b()V

    .line 90
    return-void
.end method

.method public updateWidget()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 121
    const v0, 0x7f0c00ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout;

    .line 122
    if-nez v6, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    const v0, 0x7f0c00ec

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 127
    if-eqz v0, :cond_2

    .line 128
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    if-ne v1, v5, :cond_4

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    :cond_2
    :goto_1
    const v0, 0x7f0c00ed

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/LinearLayout;

    .line 136
    if-eqz v7, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 141
    :cond_3
    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 131
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 143
    :cond_5
    const v0, 0x7f0c00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-nez v0, :cond_7

    .line 146
    new-instance v0, Lcom/sec/android/app/samsungapps/view/DetailContentListItemArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f04004d

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->e:Ljava/util/ArrayList;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/view/DetailContentListItemArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;II)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->k:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setNumColumns(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setFocusableInTouchMode(Z)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/e;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 181
    const v0, 0x7f0c00eb

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 182
    if-eqz v0, :cond_0

    .line 187
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->m:Z

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->n:I

    if-ne v1, v5, :cond_9

    .line 188
    :cond_6
    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 189
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 190
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    goto/16 :goto_0

    .line 150
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->g:Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;->requestFocus()Z

    .line 153
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->f:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 192
    :cond_9
    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 193
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 194
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 195
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/f;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method
