.class public Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setFullLayout()V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f0802ef

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 43
    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 45
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 46
    if-eqz v2, :cond_0

    .line 47
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 49
    :pswitch_0
    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 50
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 54
    :pswitch_1
    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f0802ef

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    .line 72
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f0802ef

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 77
    return-void
.end method

.method public static createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
    .locals 4

    .prologue
    .line 80
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const v1, 0x7f08027d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v2, 0x7f0802ef

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 84
    return-object v0
.end method

.method public static createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
    .locals 4

    .prologue
    .line 89
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v2, 0x7f0802ef

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 92
    return-object v0
.end method


# virtual methods
.method public getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 110
    :cond_0
    return-void
.end method

.method public setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 103
    return-void
.end method

.method public setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 99
    return-void
.end method

.method public show()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->mContext:Landroid/content/Context;

    const-string v0, "activity"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v6, 0x64

    if-eq v5, v6, :cond_1

    iget v5, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_0

    :cond_1
    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 117
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->showWithBadTokenException()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move v0, v1

    .line 125
    :goto_1
    return v0

    :cond_3
    move v0, v2

    .line 115
    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    const-string v0, "CustomDialogBuilder.show exception"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    :cond_4
    move v0, v2

    .line 125
    goto :goto_1
.end method
