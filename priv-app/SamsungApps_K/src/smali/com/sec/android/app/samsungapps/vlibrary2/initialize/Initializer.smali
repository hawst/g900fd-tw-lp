.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;


# instance fields
.field private mCheckAppUpgradeObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;

.field private mContext:Landroid/content/Context;

.field private mInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

.field private mNetwork:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;

.field private mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mContext:Landroid/content/Context;

    .line 22
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mNetwork:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;

    .line 23
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mCheckAppUpgradeObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->callInitializeCommand()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->onSuccess()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mCheckAppUpgradeObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;

    return-object v0
.end method

.method private callCheckUpgrade()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;

    .line 89
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->checkAppUpgradeCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 98
    return-void
.end method

.method private callInitializeCommand()V
    .locals 3

    .prologue
    .line 52
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 66
    return-void
.end method

.method private checkNetworkError()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mNetwork:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;->create()Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker$INetworkErrorCheckerObserver;)V

    .line 49
    return-void
.end method

.method private onSuccess()V
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->autoLogin()V

    .line 75
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->callCheckUpgrade()V

    .line 76
    return-void
.end method


# virtual methods
.method protected autoLogin()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mInitializeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;->autoLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 85
    return-void
.end method

.method public execute()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->checkNetworkError()V

    .line 34
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/Initializer;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;

    .line 29
    return-void
.end method
