.class final Lcom/sec/android/app/samsungapps/vlibrary2/list/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 48
    const-string v0, "requestPurchasedList::PurchasedList is destroyed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 52
    :cond_0
    if-eqz p1, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADCOMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    goto :goto_0
.end method
