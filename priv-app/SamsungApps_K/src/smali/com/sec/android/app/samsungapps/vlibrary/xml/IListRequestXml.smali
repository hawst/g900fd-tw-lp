.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 7

    .prologue
    .line 15
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 17
    return-void
.end method

.method private addContentTypeAsParam()V
    .locals 2

    .prologue
    .line 49
    const-string v0, "contentType"

    const-string v1, "all"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method


# virtual methods
.method protected addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V
    .locals 3

    .prologue
    .line 35
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    const-string v1, "imgWidth"

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v1, "imgHeight"

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :goto_0
    return-void

    .line 42
    :cond_0
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addContentTypeAsParam()V

    .line 57
    return-void
.end method

.method protected final addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->getItemCountInOnePage(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)I

    move-result v0

    .line 22
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getIndexOfLastItem()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getIndexOfLastItem()I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 24
    const-string v2, "startNum"

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const-string v1, "endNum"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "alignOrder"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    const-string v0, "alignOrder"

    const-string v1, "recent"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final getItemCountInOnePage(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)I
    .locals 1

    .prologue
    .line 30
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getLoadingItemCountInOnePage()I

    move-result v0

    return v0
.end method
