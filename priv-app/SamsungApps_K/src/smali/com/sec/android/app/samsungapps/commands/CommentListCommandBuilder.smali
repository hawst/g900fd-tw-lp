.class public Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AbstractCommentListCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Ljava/lang/String;IZ)V

    .line 22
    return-void
.end method


# virtual methods
.method public deleteComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;Ljava/lang/String;)V

    .line 86
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/as;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/commands/as;-><init>(Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->setPreExcutedCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 88
    return-object v0
.end method

.method public getAddCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/aq;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/aq;-><init>(Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;)V

    return-object v0
.end method

.method public getModifyCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ap;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/ap;-><init>(Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;)V

    return-object v0
.end method

.method public getReviewDetailViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/ar;-><init>(Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;)V

    return-object v0
.end method
