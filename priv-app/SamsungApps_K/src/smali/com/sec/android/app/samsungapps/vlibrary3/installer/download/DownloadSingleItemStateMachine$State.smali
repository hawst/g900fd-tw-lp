.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum DETAIL_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum HANDLE_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum REFRESH_DETAIL_AFTER_PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum REFRESH_DETAL_AFTERPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field public static final enum WAIT_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "URL_REQUEST"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "WAIT_RES_LOCK"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->WAIT_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "CANCELLING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "INSTALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "SUCCESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "CANCELED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "PAYMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "DETAIL_CHECK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DETAIL_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "HANDLE_FAILURE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "HANDLE_PAYMENT_SUCCESS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "REFRESH_DETAL_AFTERPAYMENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->REFRESH_DETAL_AFTERPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    const-string v1, "REFRESH_DETAIL_AFTER_PAYMENT_FAILED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->REFRESH_DETAIL_AFTER_PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    .line 10
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->URL_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->WAIT_RES_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->DETAIL_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->HANDLE_PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->REFRESH_DETAL_AFTERPAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->REFRESH_DETAIL_AFTER_PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    return-object v0
.end method
