.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;

    .line 36
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 88
    const-string v0, "CPreloadAppUpdaterStatemachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 103
    :goto_0
    return-void

    .line 91
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CHECK_UPDATE_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->SIGNAL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 95
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CLEAR_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->SIGNAL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CLEAR_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->START_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;)Z
    .locals 3

    .prologue
    .line 42
    const-string v0, "CPreloadAppUpdaterStatemachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 43
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 77
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 46
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 49
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 57
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 68
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 46
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 54
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 65
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 111
    const-string v0, "CPreloadAppUpdaterStatemachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    return-void
.end method
