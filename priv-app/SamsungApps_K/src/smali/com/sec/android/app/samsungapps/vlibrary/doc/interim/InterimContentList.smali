.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# instance fields
.field private InterimSymbolImgUrl:Ljava/lang/String;

.field private listContentSetID:Ljava/lang/String;

.field private listDescription:Ljava/lang/String;

.field private listTitle:Ljava/lang/String;

.field private seeMoreBtnTargetData:Ljava/lang/String;

.field private seeMoreBtnTargetType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterimSymbolImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->InterimSymbolImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getListContentSetID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->listContentSetID:Ljava/lang/String;

    return-object v0
.end method

.method public getListDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->listDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getListTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->listTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getSeeMoreBtnTargetData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->seeMoreBtnTargetData:Ljava/lang/String;

    return-object v0
.end method

.method public getSeeMoreBtnTargetType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->seeMoreBtnTargetType:Ljava/lang/String;

    return-object v0
.end method

.method public setInterimSymbolImgUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->InterimSymbolImgUrl:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setListContentSetID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->listContentSetID:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setListDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->listDescription:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setListTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->listTitle:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public setSeeMoreBtnTargetData(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->seeMoreBtnTargetData:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setSeeMoreBtnTargetType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->seeMoreBtnTargetType:Ljava/lang/String;

    .line 39
    return-void
.end method
