.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 83
    const-string v0, "requestPurchaseHistHide::PurchasedList is destroyed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 96
    :goto_0
    return-void

    .line 87
    :cond_0
    if-eqz p1, :cond_1

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_PURCHASED_DELETE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->start()V

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_PURCHASED_DELETE_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    goto :goto_0
.end method
