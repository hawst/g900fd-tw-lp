.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a:Landroid/os/Handler;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->d:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 41
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    .line 48
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;)V
    .locals 3

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->ALREADY_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    goto :goto_0

    .line 71
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->NOT_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    goto :goto_0

    .line 75
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->ACCOUTN_INSTALLED_AND_VEROK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    goto :goto_0

    .line 83
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->ACCOUTN_NOT_INSTALLED_OR_VERX:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    goto :goto_0

    .line 86
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;->onLoginCheckSuccess()V

    goto :goto_0

    .line 89
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->EMAIL_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    goto :goto_0

    .line 95
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->EMAIL_DOESNT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)V

    goto :goto_0

    .line 99
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->d:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 114
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;->onLoginCheckFailed()V

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;

    .line 30
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    .line 53
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;)V

    return-void
.end method
