.class public Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static _bAutoLoginInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->_bAutoLoginInProgress:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isAutoLoginInProgress()Z
    .locals 1

    .prologue
    .line 8
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->_bAutoLoginInProgress:Z

    return v0
.end method

.method public static setAutoLoginInProgress(Z)V
    .locals 0

    .prologue
    .line 13
    sput-boolean p0, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->_bAutoLoginInProgress:Z

    .line 14
    return-void
.end method
