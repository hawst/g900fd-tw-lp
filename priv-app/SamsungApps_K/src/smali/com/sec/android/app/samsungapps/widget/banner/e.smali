.class final Lcom/sec/android/app/samsungapps/widget/banner/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/e;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const v4, 0x7f0c0193

    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/e;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/e;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadCount()I

    move-result v3

    move v1, v2

    .line 129
    :goto_1
    if-ge v1, v3, :cond_3

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/e;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadView(I)Landroid/view/View;

    move-result-object v0

    .line 131
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;

    .line 132
    if-eqz v0, :cond_2

    .line 134
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setSelectedFlag(Z)V

    .line 129
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 138
    :cond_3
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;

    .line 139
    if-eqz v0, :cond_0

    .line 141
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setSelectedFlag(Z)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method
