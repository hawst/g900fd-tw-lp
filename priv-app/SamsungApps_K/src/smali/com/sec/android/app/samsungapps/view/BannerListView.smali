.class public Lcom/sec/android/app/samsungapps/view/BannerListView;
.super Lcom/sec/android/app/samsungapps/view/MyGridListView;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

.field b:Landroid/content/Context;

.field c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

.field d:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

.field private e:Z

.field protected mArrayAdapter:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MyGridListView;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->e:Z

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/view/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/c;-><init>(Lcom/sec/android/app/samsungapps/view/BannerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BannerListView;->init(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/MyGridListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->e:Z

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/view/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/c;-><init>(Lcom/sec/android/app/samsungapps/view/BannerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BannerListView;->init(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/MyGridListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->e:Z

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/view/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/c;-><init>(Lcom/sec/android/app/samsungapps/view/BannerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    .line 36
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BannerListView;->init(Landroid/content/Context;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected empty()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->b:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;

    const v1, 0x7f0400e7

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/android/app/samsungapps/view/BannerListView$BannerArrayAdapter;-><init>(Lcom/sec/android/app/samsungapps/view/BannerListView;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/BannerListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getBannerList()Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/BannerListView;->updateView()V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;->startNewSession(Landroid/content/Context;)V

    .line 74
    new-instance v0, Lcom/sec/android/app/samsungapps/view/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/b;-><init>(Lcom/sec/android/app/samsungapps/view/BannerListView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/BannerListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 94
    return-void
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->request()V

    .line 57
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->b:Landroid/content/Context;

    .line 186
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/MyGridListView;->release()V

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    const-string v0, "Featured::Activity is finished"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;->dispatch()V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;->stopSession()V

    .line 199
    :cond_1
    return-void
.end method

.method protected runBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V
    .locals 3

    .prologue
    .line 98
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needProductDetail()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showContentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isURLBanner()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerLinkURL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->b:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 112
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showBannerContentList(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBannerPosNew(Z)V
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->e:Z

    .line 52
    return-void
.end method

.method protected updateView()V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 137
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->e:Z

    if-eqz v2, :cond_1

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isNewBanner()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 140
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;->trackCountOfBannerPageview(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V

    .line 142
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->e:Z

    if-nez v2, :cond_0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isFeaturedBanner()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 145
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;->trackCountOfBannerPageview(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/BannerListView;->setNoDataEmptyView()V

    .line 152
    :cond_3
    return-void
.end method
