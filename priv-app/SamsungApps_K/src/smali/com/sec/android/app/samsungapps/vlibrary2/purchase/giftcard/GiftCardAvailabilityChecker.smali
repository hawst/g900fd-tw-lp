.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardAvailabilityChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method public checkAvailability(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;DZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44
    if-nez p1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->Null()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v2

    if-ne p2, v2, :cond_2

    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_2
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v2

    sub-double v2, p3, v2

    .line 53
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gtz v4, :cond_3

    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :cond_3
    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v4, :cond_4

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne p1, v4, :cond_0

    .line 63
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPaymentMinPrice()D

    move-result-wide v4

    .line 64
    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    .line 68
    goto :goto_0
.end method

.method public checkAvailability(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 14
    if-nez p1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v0

    .line 18
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->Null()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v2

    if-ne p2, v2, :cond_2

    move v0, v1

    .line 20
    goto :goto_0

    .line 22
    :cond_2
    invoke-interface {p3}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getNormalPrice()D

    move-result-wide v2

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v4

    sub-double/2addr v2, v4

    .line 23
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gtz v4, :cond_3

    move v0, v1

    .line 25
    goto :goto_0

    .line 27
    :cond_3
    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v4, :cond_4

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne p1, v4, :cond_0

    .line 33
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPaymentMinPrice()D

    move-result-wide v4

    .line 35
    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    .line 39
    goto :goto_0
.end method
