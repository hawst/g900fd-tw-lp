.class public Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
.source "ProGuard"


# static fields
.field public static final CHART_SIZE_LAND_DIVIDER:I = 0x8

.field public static final CHART_SIZE_PORT_DIVIDER:I = 0x5

.field public static final TOTAL_CHART_SIZE_LAND:I = 0x17

.field public static final TOTAL_CHART_SIZE_PORT:I = 0xe


# instance fields
.field a:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->a:Z

    .line 36
    return-void
.end method


# virtual methods
.method public displayNormalItem()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->_View:Landroid/view/View;

    const v1, 0x7f0c0057

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->showProductImg(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method protected prepareView()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->displayNormalItem()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->showCommonStatus()V

    .line 71
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 1

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 40
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->prepareView()V

    .line 43
    :cond_0
    return-void
.end method

.method public setHovering(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->a:Z

    if-nez v0, :cond_0

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->a:Z

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewholder/c;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 116
    :cond_0
    return-void
.end method

.method public showProductImg(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ChartItemViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 62
    :cond_0
    return-void
.end method
