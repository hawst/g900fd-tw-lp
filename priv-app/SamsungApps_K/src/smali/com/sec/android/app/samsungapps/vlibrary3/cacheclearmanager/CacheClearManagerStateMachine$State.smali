.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

.field public static final enum NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    const-string v1, "BMODE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    const-string v1, "NormalMODE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    return-object v0
.end method
