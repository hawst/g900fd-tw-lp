.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    if-nez p1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V

    .line 69
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;->hasToken()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_ITokenBasedLoginData:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;->loginODCWithToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->_Context:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/k;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;Z)V

    goto :goto_0
.end method
