.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloadState;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getDontOpenDetailPage()Z
.end method

.method public abstract getDownloadedSize()I
.end method

.method public abstract getGUID()Ljava/lang/String;
.end method

.method public abstract getLoadType()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getTotalSize()I
.end method

.method public abstract isCancellable()Z
.end method
