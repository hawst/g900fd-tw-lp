.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

.field public static final enum NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

.field public static final enum RUN_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

.field public static final enum SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

.field public static final enum SHOW_EMERGENCY_ASKING_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    const-string v1, "SEND_REQUEST"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    const-string v1, "SHOW_EMERGENCY_ASKING_POPUP"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->SHOW_EMERGENCY_ASKING_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    const-string v1, "NOTIFY_FINISHED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    const-string v1, "RUN_UPDATE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->RUN_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->SHOW_EMERGENCY_ASKING_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->RUN_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    return-object v0
.end method
