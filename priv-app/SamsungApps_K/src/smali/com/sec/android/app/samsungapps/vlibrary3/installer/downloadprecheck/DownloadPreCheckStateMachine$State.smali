.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum CHECK_DETAIL_INFO_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum CHECK_MULTIPLE_DOWNLOADCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum CHECK_NEEDTOSHOW_ALREADYPURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum CHECK_TRUKEY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum NET_DOWNLOAD_SIZE_LIMIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum P2CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum REMEMBER_FREEPAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum STORAGE_SPACE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field public static final enum VALIDATE_COMPATABILE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "LOGINCHECK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "CHECK_TRUKEY_CONDITION"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_TRUKEY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "CHECK_DETAIL_INFO_EXIST"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_DETAIL_INFO_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "STORAGE_SPACE_CHECK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->STORAGE_SPACE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "NET_DOWNLOAD_SIZE_LIMIT_CHECK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->NET_DOWNLOAD_SIZE_LIMIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "CHECK_MULTIPLE_DOWNLOADCOUNT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_MULTIPLE_DOWNLOADCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "P2CONFIRM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->P2CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "CHECK_AGE_LIMIT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "VALIDATE_COMPATABILE_OS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->VALIDATE_COMPATABILE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "CHECK_REAL_NAME_AGE_NEED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "PERMISSION_CHECK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "SUCCESS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "REMEMBER_FREEPAID_STATE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->REMEMBER_FREEPAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    const-string v1, "CHECK_NEEDTOSHOW_ALREADYPURCHASED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_NEEDTOSHOW_ALREADYPURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    .line 11
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_TRUKEY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_DETAIL_INFO_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->STORAGE_SPACE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->NET_DOWNLOAD_SIZE_LIMIT_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_MULTIPLE_DOWNLOADCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->P2CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->VALIDATE_COMPATABILE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->REMEMBER_FREEPAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->CHECK_NEEDTOSHOW_ALREADYPURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    return-object v0
.end method
