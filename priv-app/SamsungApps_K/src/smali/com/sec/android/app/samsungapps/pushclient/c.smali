.class final Lcom/sec/android/app/samsungapps/pushclient/c;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/pushclient/PushService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v2, 0x5

    const/4 v3, 0x0

    .line 146
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 148
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 321
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 158
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->isPushServerAvailable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 160
    if-ge v0, v2, :cond_1

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BgDownloadService handler]retry cnt = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    const/4 v1, 0x1

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v1, v0, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 169
    invoke-virtual {p0, v0, v4, v5}, Lcom/sec/android/app/samsungapps/pushclient/c;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "C"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    .line 174
    const-string v0, "[BgDownloadService handler]QUEUE_CMD_SPP_REGISTRATION FAIL"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Ljava/lang/String;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4

    .line 187
    const-string v0, "[BgDownloadService handler]Already Registered !!! "

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->c(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getPushNotiSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto/16 :goto_0

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto/16 :goto_0

    .line 211
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->e(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    const-string v0, "[BgDownloadService handler]SPP Reg Error!"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "S"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 235
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->isPushServerAvailable()Z

    move-result v1

    if-nez v1, :cond_6

    .line 237
    if-ge v0, v2, :cond_5

    .line 239
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BgDownloadService handler]retry cnt = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    const/4 v1, 0x2

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v1, v0, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 245
    invoke-virtual {p0, v0, v4, v5}, Lcom/sec/android/app/samsungapps/pushclient/c;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 249
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "C"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    .line 250
    const-string v0, "[BgDownloadService handler]QUEUE_CMD_SPP_DEREGISTRATION FAIL"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Ljava/lang/String;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_8

    .line 263
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Ljava/lang/String;

    move-result-object v0

    .line 266
    :cond_8
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_9

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->g(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    const-string v0, "[BgDownloadService handler]SPP Dereg Error!"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "S"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 280
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Z)V

    .line 283
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.action.push_dereg"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 284
    const-string v1, "extra.push.status"

    const-string v2, "SUCCESS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->sendBroadcast(Landroid/content/Intent;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->h(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto/16 :goto_0

    .line 308
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 316
    :pswitch_4
    const-string v0, "registered deepLink noti message"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/c;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
