.class final Lcom/sec/android/app/samsungapps/vlibrary2/update/l;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

.field b:J

.field final synthetic c:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;J)V
    .locals 2

    .prologue
    .line 289
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->c:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    .line 287
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->b:J

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->setSize(J)V

    .line 291
    return-void
.end method


# virtual methods
.method public final downloadEnded(Z)V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public final getDownloadURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->c:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->wrapper:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->downloadUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    return-object v0
.end method

.method public final getSaveFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->c:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getConvertedFileName()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$600(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final updateDownloadedSize(J)V
    .locals 0

    .prologue
    .line 299
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/l;->b:J

    .line 300
    return-void
.end method
