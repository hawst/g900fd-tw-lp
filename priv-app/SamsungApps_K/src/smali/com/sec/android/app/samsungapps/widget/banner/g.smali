.class final Lcom/sec/android/app/samsungapps/widget/banner/g;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 347
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 358
    :goto_0
    return-void

    .line 349
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->a(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->c(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->b:Z

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d(Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;->getBannerCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->playNext()V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/g;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->d:Landroid/os/Handler;

    const/4 v1, 0x4

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 347
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
