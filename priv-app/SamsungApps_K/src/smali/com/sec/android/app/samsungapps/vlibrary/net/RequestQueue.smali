.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mRequestQueue:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->mRequestQueue:Ljava/util/ArrayList;

    .line 11
    return-void
.end method


# virtual methods
.method public addQueue(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    .locals 1

    .prologue
    .line 15
    monitor-enter p0

    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->mRequestQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public take()Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x0

    .line 24
    monitor-enter p0

    .line 25
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->mRequestQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->mRequestQueue:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->mRequestQueue:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 30
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
