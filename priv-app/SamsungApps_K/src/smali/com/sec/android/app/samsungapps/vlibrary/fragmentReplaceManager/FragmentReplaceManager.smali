.class public Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private bActivate:Z

.field private mContainer:Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;

.field mRequestList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->bActivate:Z

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mContainer:Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mContainer:Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;

    .line 13
    return-void
.end method


# virtual methods
.method public isActive()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->bActivate:Z

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 69
    return-void
.end method

.method public replaceFragment(ILjava/lang/Object;Z)V
    .locals 3

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->bActivate:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mContainer:Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;->concrete_replaceFragment(ILjava/lang/Object;Z)V

    .line 64
    :goto_0
    return-void

    .line 47
    :cond_0
    if-nez p3, :cond_2

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 50
    if-eqz v0, :cond_2

    .line 52
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;->a:I

    if-ne v0, p1, :cond_1

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 52
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 62
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;

    invoke-direct {v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;-><init>(ILjava/lang/Object;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setActive()V
    .locals 5

    .prologue
    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->bActivate:Z

    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;

    .line 21
    iget v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;->a:I

    .line 22
    iget-object v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;->b:Ljava/lang/Object;

    .line 23
    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/a;->c:Z

    .line 24
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mContainer:Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;

    invoke-interface {v4, v2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager$FragmentContainer;->concrete_replaceFragment(ILjava/lang/Object;Z)V

    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 28
    :cond_1
    return-void
.end method

.method public setDeactive()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/fragmentReplaceManager/FragmentReplaceManager;->bActivate:Z

    .line 33
    return-void
.end method
