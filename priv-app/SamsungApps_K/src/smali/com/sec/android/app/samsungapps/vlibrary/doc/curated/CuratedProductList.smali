.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# instance fields
.field private index:I

.field private listDescription:Ljava/lang/String;

.field private listTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->index:I

    return v0
.end method

.method public getListDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->listDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getListTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->listTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setIndex(I)V
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->index:I

    .line 41
    return-void
.end method

.method public setListDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->listDescription:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setListTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->listTitle:Ljava/lang/String;

    .line 31
    return-void
.end method
