.class public Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;
.super Landroid/widget/CheckBox;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/res/TypedArray;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a:Landroid/content/res/TypedArray;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b:Landroid/graphics/drawable/Drawable;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a:Landroid/content/res/TypedArray;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b:Landroid/graphics/drawable/Drawable;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 32
    sget-object v0, Lcom/android/internal/R$styleable;->CompoundButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 33
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b:Landroid/graphics/drawable/Drawable;

    .line 34
    sget-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->CheckBoxImage:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a:Landroid/content/res/TypedArray;

    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a:Landroid/content/res/TypedArray;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b:Landroid/graphics/drawable/Drawable;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 24
    sget-object v0, Lcom/android/internal/R$styleable;->CompoundButton:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 25
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b:Landroid/graphics/drawable/Drawable;

    .line 26
    sget-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->CheckBoxImage:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a:Landroid/content/res/TypedArray;

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a()V

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/content/res/TypedArray;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a:Landroid/content/res/TypedArray;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/view/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/a;-><init>(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)V

    invoke-super {p0, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 76
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-object v0
.end method


# virtual methods
.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 81
    return-void
.end method
