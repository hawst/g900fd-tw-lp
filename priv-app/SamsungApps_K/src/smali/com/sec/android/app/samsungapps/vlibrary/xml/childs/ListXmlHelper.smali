.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V
    .locals 2

    .prologue
    .line 25
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public static addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V
    .locals 3

    .prologue
    .line 12
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    const-string v1, "imgWidth"

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v1, "imgHeight"

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    :goto_0
    return-void

    .line 19
    :cond_0
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;II)V
    .locals 3

    .prologue
    .line 68
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    const-string v2, "startNum"

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v0, "endNum"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public static addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V
    .locals 3

    .prologue
    .line 59
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->getItemCountInOnePage(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)I

    move-result v0

    .line 60
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getIndexOfLastItem()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getIndexOfLastItem()I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    const-string v2, "startNum"

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "endNum"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public static addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "alignOrder"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    const-string v0, "alignOrder"

    const-string v1, "recent"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    const-string v0, "alignOrder"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method private static getItemCountInOnePage(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)I
    .locals 1

    .prologue
    .line 54
    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getLoadingItemCountInOnePage()I

    move-result v0

    return v0
.end method

.method public static setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "contentType"

    const-string v1, "all"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public static setStatus(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;I)V
    .locals 2

    .prologue
    .line 86
    const-string v0, "status"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public static setStatus(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V
    .locals 2

    .prologue
    .line 81
    const-string v0, "status"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;->getPaidTypeFilter()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method
