.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseDetail;
.super Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;
.source "ProGuard"


# instance fields
.field public bundle:Ljava/lang/String;

.field public contentGradeImgUrl:Ljava/lang/String;

.field public couponName:Ljava/lang/String;

.field public discountInfo:Ljava/lang/String;

.field public downloadPeriod:Ljava/lang/String;

.field public installSize:Ljava/lang/String;

.field public normalPrice:D

.field public paymentAmount:D

.field public purchaseDate:Ljava/lang/String;

.field public settlementType:Ljava/lang/String;

.field public updateClsf:Z

.field public updateDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseDetail;

    const/4 v1, 0x0

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseDetail;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 11
    return-void
.end method


# virtual methods
.method public isPurchaseDetail()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    return v0
.end method
