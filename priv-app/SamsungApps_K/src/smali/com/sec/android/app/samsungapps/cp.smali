.class final Lcom/sec/android/app/samsungapps/cp;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/cp;->a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 4

    .prologue
    .line 76
    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 78
    const-string v0, "CustomQaPwdCheckDialogBuilder::onPositiveListener:: Activity is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->w(Ljava/lang/String;)V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    const-string v0, ""

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/cp;->a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/cp;->a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 86
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const v3, 0x7f0802c7

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 90
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/cp;->a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/cp;->a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->a(Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/cp;->a:Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/CustomQaPwdCheckDialogBuilder;->startQaStorePwdCheck(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CustomQaPwdCheckDialogBuilder::onPositiveListener::e = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method
