.class final Lcom/sec/android/app/samsungapps/viewholder/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 249
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 250
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setTextViewHovering(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 263
    :cond_0
    :goto_0
    return v3

    .line 253
    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setFHAnimation(Landroid/content/Context;Landroid/view/View;Z)V

    .line 255
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->isHoverPopupPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHovered(Z)V

    goto :goto_0

    .line 259
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/d;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHovered(Z)V

    goto :goto_0
.end method
