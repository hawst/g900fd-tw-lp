.class public Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# static fields
.field protected static final RESULT_CANCELED:I = 0x0

.field protected static final RESULT_FAILED:I = 0x1

.field protected static final RESULT_OK:I = -0x1


# instance fields
.field protected mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field protected mRequestTokenCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mRequestTokenCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method


# virtual methods
.method protected endLoading()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 102
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 106
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 108
    if-eqz p3, :cond_0

    const v0, 0x8fbf

    if-ne p1, v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Result Code of token request by new interface : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 114
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    invoke-direct {v0, p3, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;-><init>(Landroid/content/Intent;I)V

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mRequestTokenCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onResultToGetToken(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 123
    :goto_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 124
    const-string v0, "error_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    const-string v1, "error_message"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    const-string v2, "SAC"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "errorCode : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const-string v0, "SAC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "errorMessage : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->finish()V

    .line 134
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "SAC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "errorCode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 119
    :catch_1
    move-exception v0

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAccountNewInterfaceHandlerActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 36
    :try_start_0
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mRequestTokenCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mRequestTokenCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->finish()V

    .line 55
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAccountNewInterfaceHandlerActivity::ClassCaseException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->finish()V

    goto :goto_0

    .line 49
    :cond_0
    if-eqz v1, :cond_1

    .line 51
    const-string v0, "isAutoLogin"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 54
    :cond_1
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "server_url"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "api_server_url"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "auth_server_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cc"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "birthday"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "email_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mcc"

    aput-object v2, v0, v1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "client_id"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "progress_theme"

    const-string v2, "invisible"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mRequestTokenCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->getExpiredToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v2, "expired_access_token"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    const v0, 0x8fbf

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onDestroy()V

    .line 84
    return-void
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;->mLoadingDialog:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 93
    :cond_0
    return-void
.end method
