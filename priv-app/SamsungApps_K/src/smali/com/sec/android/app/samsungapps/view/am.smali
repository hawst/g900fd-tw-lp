.class final Lcom/sec/android/app/samsungapps/view/am;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/ListViewController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/ListViewController;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 133
    sub-int v0, p4, p3

    .line 134
    if-lt p2, v0, :cond_0

    if-eqz p4, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->hasMoreItems()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b(Lcom/sec/android/app/samsungapps/view/ListViewController;)Landroid/widget/AbsListView;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;

    if-nez v0, :cond_0

    .line 142
    add-int v0, p2, p3

    if-ne v0, p4, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->requestMoreData()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b(Lcom/sec/android/app/samsungapps/view/ListViewController;)Landroid/widget/AbsListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b(Lcom/sec/android/app/samsungapps/view/ListViewController;)Landroid/widget/AbsListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/ListViewController;->a(Lcom/sec/android/app/samsungapps/view/ListViewController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/am;->a:Lcom/sec/android/app/samsungapps/view/ListViewController;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/ListViewController;->b(Lcom/sec/android/app/samsungapps/view/ListViewController;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setIndexOfFirstItem(I)V

    .line 162
    :cond_0
    return-void
.end method
