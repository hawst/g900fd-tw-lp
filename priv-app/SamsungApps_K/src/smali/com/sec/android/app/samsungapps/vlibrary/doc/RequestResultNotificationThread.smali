.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;


# instance fields
.field private _bEndApplication:Z

.field _handlerInterfaces:Ljava/util/ArrayList;

.field handler:Landroid/os/Handler;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/af;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/af;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->handler:Landroid/os/Handler;

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_bEndApplication:Z

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_handlerInterfaces:Ljava/util/ArrayList;

    .line 12
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;)Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->take()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;)Z
    .locals 1

    .prologue
    .line 8
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_bEndApplication:Z

    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    return-object v0
.end method

.method private put(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_handlerInterfaces:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private take()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;
    .locals 3

    .prologue
    .line 77
    monitor-enter p0

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_handlerInterfaces:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x0

    monitor-exit p0

    .line 85
    :goto_0
    return-object v0

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_handlerInterfaces:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_handlerInterfaces:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 85
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->put(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 43
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ag;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ag;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 44
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 45
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->_bEndApplication:Z

    .line 38
    return-void
.end method
