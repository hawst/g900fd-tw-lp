.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 10
    const-string v2, "customerBoughtProductList2Notc"

    const-string v3, "2236"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    const-string v0, "productID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "alignOrder"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 16
    add-int/lit8 v1, v0, 0xf

    .line 17
    const-string v2, "startNum"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v0, "endNum"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const-string v0, "contentType"

    const-string v1, "application"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    return-void
.end method
