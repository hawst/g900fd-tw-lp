.class final Lcom/sec/android/app/samsungapps/ak;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 1243
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 4

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1269
    :cond_0
    :goto_0
    return-void

    .line 1252
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->h(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setDetailButtonManager(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;)V

    .line 1253
    if-eqz p1, :cond_2

    .line 1255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->i(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    .line 1256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->j(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onContentDetailLoaded(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-nez v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->categoryID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->createLogDataForDetailPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    goto :goto_0

    .line 1266
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ak;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(IZ)V

    goto :goto_0
.end method
