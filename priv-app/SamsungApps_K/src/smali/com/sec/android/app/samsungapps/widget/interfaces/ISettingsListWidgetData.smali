.class public interface abstract Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract hasAbout()Z
.end method

.method public abstract hasAccountSetting()Z
.end method

.method public abstract hasAdPreference()Z
.end method

.method public abstract hasAutoUpdate()Z
.end method

.method public abstract hasNotifyAppUpdates()Z
.end method

.method public abstract hasNotifyStoreActivities()Z
.end method

.method public abstract hasPurchaseProtection()Z
.end method

.method public abstract hasSearchSetting()Z
.end method
