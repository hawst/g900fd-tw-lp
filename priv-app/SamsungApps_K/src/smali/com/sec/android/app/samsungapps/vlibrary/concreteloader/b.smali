.class final Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 421
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 424
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pkgname:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;->packageDeleted(Ljava/lang/String;I)V

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 430
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget-boolean v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->isSuccessful:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;->packageDeleted(Z)V

    goto :goto_0
.end method
