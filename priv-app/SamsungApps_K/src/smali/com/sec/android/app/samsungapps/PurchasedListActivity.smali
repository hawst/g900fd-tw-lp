.class public Lcom/sec/android/app/samsungapps/PurchasedListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList$IAllSelectableItemListListener;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

.field b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

.field private d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

.field private e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

.field private f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

.field private g:Landroid/content/Context;

.field private h:I

.field private i:Z

.field private j:I

.field private k:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    .line 341
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->k:J

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/PurchasedListActivity;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->j:I

    return p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 192
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->i:Z

    if-eqz v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->setCommandType(I)V

    .line 202
    const v0, 0x7f0c0179

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    .line 203
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;->getMinCountToRequestPurchaseItemList()I

    move-result v2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;ILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c()Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->loadWidget()V

    .line 211
    const v0, 0x7f0c0178

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c()Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setWidgetData(Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->loadWidget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    goto :goto_0

    .line 223
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;->createChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    new-instance v1, Lcom/sec/android/app/samsungapps/ge;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ge;-><init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;->check(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshWidget()V

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v2, :cond_1

    const v0, 0xa0010

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PurchasedListActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a()V

    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 344
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->k:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 345
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->k:J

    .line 346
    const/4 v0, 0x1

    .line 348
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;
    .locals 1

    .prologue
    .line 451
    new-instance v0, Lcom/sec/android/app/samsungapps/gh;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/gh;-><init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public ShowDialog()V
    .locals 6

    .prologue
    const v5, 0x7f0802a1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->deletingCount()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080243

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 308
    :goto_0
    invoke-static {p0, v1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/gf;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/gf;-><init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/gg;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/gg;-><init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 337
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 339
    return-void

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080242

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->deletingCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public changeActionBar(I)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const v0, 0x7f0801c2

    const v5, 0xb0001

    const/4 v3, 0x1

    .line 226
    packed-switch p1, :pswitch_data_0

    .line 253
    :goto_0
    return-void

    .line 229
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [I

    const/4 v0, 0x0

    const v6, 0xa0008

    aput v6, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 235
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 241
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 247
    :pswitch_3
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0xa000f
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 235
    :array_0
    .array-data 4
        0xa0010
        0xa0008
    .end array-data

    .line 241
    :array_1
    .array-data 4
        0xa000f
        0xa0008
    .end array-data

    .line 247
    :array_2
    .array-data 4
        0xa0011
        0xa0008
    .end array-data
.end method

.method public deletionModeActionItemEnable(Z)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isListItemMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa000e

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isListItemMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0018

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method public displayDeleteScreen()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const v1, 0x7f0801c2

    const v5, 0xb0002

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isListItemMode()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 424
    new-array v4, v4, [I

    fill-array-data v4, :array_0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 433
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showOptionItem(Z)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setLayoutTopButton(I)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->addIAllSelectableItemListListener(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList$IAllSelectableItemListListener;)V

    .line 437
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getSelectedCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setUpPopupMenu(I)V

    .line 440
    return-void

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isListItemMode()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 428
    new-array v4, v4, [I

    fill-array-data v4, :array_1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 424
    nop

    :array_0
    .array-data 4
        0xa000e
        0xa0008
    .end array-data

    .line 428
    :array_1
    .array-data 4
        0xa0018
        0xa0008
    .end array-data
.end method

.method public displayNormalScreen()V
    .locals 4

    .prologue
    const v3, 0xa0010

    const/16 v2, 0x11

    const/4 v1, 0x0

    .line 412
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setDeletionMode(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->deSelectAll()Z

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshArrayAdapter()V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setLayoutTopButton(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showOptionItem(Z)V

    .line 418
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->changeActionBar(I)V

    .line 419
    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 420
    return-void
.end method

.method public enableActionItem(IZ)V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    .line 279
    return-void
.end method

.method public getPurchasedListManager()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 257
    sget-object v0, Lcom/sec/android/app/samsungapps/gi;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 265
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    move-object v0, p1

    .line 259
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 260
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->finish()V

    .line 262
    const/4 v0, 0x0

    goto :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public isSelectAll()Z
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    if-nez v0, :cond_0

    .line 635
    const/4 v0, 0x0

    .line 637
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->isCheckAllButtonChecked()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 352
    sparse-switch p1, :sswitch_data_0

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 354
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    const v0, 0xa0011

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->changeActionBar(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->updateAll()V

    goto :goto_0

    .line 362
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isAllCancellable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->cancelAll()V

    .line 366
    const v0, 0xa000f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->changeActionBar(I)V

    goto :goto_0

    .line 372
    :sswitch_2
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 376
    :sswitch_3
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 381
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->ShowDialog()V

    goto :goto_0

    .line 385
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->displayNormalScreen()V

    goto :goto_0

    .line 388
    :sswitch_6
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setDeletionMode(I)V

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->displayDeleteScreen()V

    goto :goto_0

    .line 352
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0005 -> :sswitch_2
        0xa0007 -> :sswitch_3
        0xa000a -> :sswitch_5
        0xa000e -> :sswitch_4
        0xa000f -> :sswitch_0
        0xa0010 -> :sswitch_6
        0xa0011 -> :sswitch_1
        0xa0018 -> :sswitch_4
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const v3, 0xa0010

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 558
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->displayNormalScreen()V

    .line 559
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getWidgetState()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 560
    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 571
    :goto_0
    return-void

    .line 562
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getCountNotInstalledItem()I

    move-result v2

    if-lez v2, :cond_1

    .line 563
    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 562
    goto :goto_1

    .line 568
    :cond_2
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->i:Z

    .line 569
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 600
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setTabTextSize(Landroid/content/res/Configuration;)V

    .line 605
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 48
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    .line 51
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buttonType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    .line 53
    const v0, 0x7f040055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setMainView(I)V

    .line 58
    const v0, 0x7f0801c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v4, v3, [I

    const/4 v0, 0x0

    const v5, 0xa0008

    aput v5, v4, v0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

    new-instance v1, Lcom/sec/android/app/samsungapps/gd;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/gd;-><init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->checkLogin()V

    .line 77
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->clear()V

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->release()V

    .line 152
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->release()V

    .line 158
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->release()V

    .line 164
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    .line 167
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

    if-eqz v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->release()V

    .line 170
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;

    .line 173
    :cond_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 174
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 175
    return-void
.end method

.method public onListStateChanged()V
    .locals 0

    .prologue
    .line 596
    return-void
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 575
    packed-switch p1, :pswitch_data_0

    .line 584
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNavigationActionBar(ILandroid/view/View;)V

    .line 585
    :goto_0
    return-void

    .line 578
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->onBackPressed()V

    goto :goto_0

    .line 575
    nop

    :pswitch_data_0
    .packed-switch 0xa0000
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 182
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "buttonType"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->setCommandType(I)V

    .line 186
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c()Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->h:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onClickTab(I)V

    .line 188
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 139
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onPause()V

    .line 140
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const v3, 0xa0010

    const/4 v0, 0x0

    .line 81
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshArrayAdapter()V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getCountNotInstalledItem()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 90
    :cond_0
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 95
    :cond_1
    :goto_0
    return-void

    .line 92
    :cond_2
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v1

    if-nez v1, :cond_1

    .line 294
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v1

    .line 287
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 291
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 292
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 293
    const v0, 0x7f0c0001

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 294
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDeletionMode(I)V
    .locals 2

    .prologue
    .line 395
    packed-switch p1, :pswitch_data_0

    .line 406
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PurchasedListActivity::setDeletionMode::aPurchasedDeleteMode="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 409
    :goto_0
    return-void

    .line 397
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->enterDeletionMode(I)V

    goto :goto_0

    .line 400
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->enterDeletionMode(I)V

    goto :goto_0

    .line 403
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->enterDeletionMode(I)V

    goto :goto_0

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSelectAll(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    if-nez v0, :cond_0

    .line 631
    :goto_0
    return-void

    .line 620
    :cond_0
    if-eqz p1, :cond_1

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->selectAll(Landroid/content/Context;)Z

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e:Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getSelectedCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->j:I

    .line 624
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_0

    .line 626
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->deSelectAll()Z

    .line 627
    iput v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->j:I

    .line 629
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_0
.end method

.method public setUpPopupMenu(I)V
    .locals 5

    .prologue
    .line 609
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setSelectedText(Ljava/lang/String;)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f:Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isAllSelected(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setCheckAllButton(Ljava/lang/Boolean;)V

    .line 613
    :cond_0
    return-void
.end method
