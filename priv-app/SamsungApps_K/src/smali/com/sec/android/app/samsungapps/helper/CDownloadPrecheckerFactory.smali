.class public Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

    .line 36
    return-void
.end method

.method public static createAgeLimitWarnPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/helper/AgeLimitWarnPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v0
.end method

.method public static createCompatibleOSWarn(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v0
.end method

.method public static createFreeStoragePopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v0
.end method

.method public static createMultipleDevCountPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/helper/MultipleDevCountPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v0
.end method

.method public static createNetworkSizeLimitPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v0
.end method

.method public static createPwordConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/helper/d;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/helper/d;-><init>()V

    new-instance v2, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;-><init>()V

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method

.method public static createTurkeyPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v0
.end method


# virtual methods
.method public create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createDownloadPreCheckManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;

    move-result-object v0

    return-object v0
.end method

.method public createDownloadPreCheckManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;
    .locals 16

    .prologue
    .line 116
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createLoginForDownloadManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;

    move-result-object v5

    .line 117
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createTurkeyPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    move-result-object v7

    .line 118
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createMultipleDevCountPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    move-result-object v8

    .line 119
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createFreeStoragePopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    move-result-object v9

    .line 120
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createNetworkSizeLimitPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    move-result-object v10

    .line 121
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createAgeLimitWarnPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    move-result-object v11

    .line 122
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createCompatibleOSWarn(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    move-result-object v12

    .line 123
    new-instance v13, Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;

    invoke-direct {v13}, Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;-><init>()V

    .line 124
    new-instance v15, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v15, v0, v1}, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 125
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createPwordConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    move-result-object v14

    .line 126
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v15}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;)V

    .line 133
    return-object v2
.end method

.method public createLoginForDownloadManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 46
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/b;-><init>(Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;)V

    invoke-direct {v1, p1, p2, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V

    .line 63
    return-object v1
.end method
