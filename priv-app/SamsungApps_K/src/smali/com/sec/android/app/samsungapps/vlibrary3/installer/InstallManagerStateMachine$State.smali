.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum BINSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum KNOX_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum SILENCE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field public static final enum SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "SILENCE_INSTALLING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "FOREGROUND_INSTALLING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "KNOX_INSTALLING"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->KNOX_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "SILENCE_INSTALL_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "SILENCE_INSTALL_COMPLETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "KNOX_INSTALL_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "BINSTALL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->BINSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "PREPARE_B"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "REQUEST_LOCK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    const-string v1, "CHECK_INSTALL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    .line 11
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->KNOX_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->BINSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    return-object v0
.end method
