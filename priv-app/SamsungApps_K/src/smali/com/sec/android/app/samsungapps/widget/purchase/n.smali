.class final Lcom/sec/android/app/samsungapps/widget/purchase/n;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    const v1, 0x7f0400a0

    .line 522
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    .line 523
    invoke-direct {p0, p2, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 525
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->b:Landroid/view/LayoutInflater;

    .line 526
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->c:I

    .line 527
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->d:Landroid/content/Context;

    .line 528
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/n;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    .line 537
    if-nez p2, :cond_0

    .line 538
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->b:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->c:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 539
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/o;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->d:Landroid/content/Context;

    invoke-direct {v1, v2, v3, p2}, Lcom/sec/android/app/samsungapps/widget/purchase/o;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Landroid/content/Context;Landroid/view/View;)V

    .line 541
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    .line 545
    :goto_0
    invoke-virtual {v2, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a(I)V

    .line 546
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/n;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/purchase/m;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;Lcom/sec/android/app/samsungapps/widget/purchase/m;)V

    .line 547
    return-object p2

    .line 543
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/purchase/o;

    move-object v2, v1

    goto :goto_0
.end method
