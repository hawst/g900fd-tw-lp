.class final Lcom/sec/android/app/samsungapps/view/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    if-eqz p2, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->isFocused()Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 59
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    .line 62
    :cond_0
    if-eqz v0, :cond_4

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    .line 65
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 71
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->c(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 74
    :cond_2
    return-void

    .line 57
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->a(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    goto :goto_0

    .line 68
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/a;->a:Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->b(Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/AnimatedCheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
