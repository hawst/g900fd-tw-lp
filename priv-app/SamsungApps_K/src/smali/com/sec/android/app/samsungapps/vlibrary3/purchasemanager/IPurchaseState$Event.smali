.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field public static final enum PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field public static final enum PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field public static final enum PERMISSION_DOESNT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field public static final enum PERMISSION_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field public static final enum PERMISSION_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field public static final enum PERMISSION_RECEIVE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "PERMISSION_EXIST"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "PERMISSION_DOESNT_EXIST"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_DOESNT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "PERMISSION_RECEIVE_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_RECEIVE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "PERMISSION_RECEIVED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "PAYMENT_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    const-string v1, "PAYMENT_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    .line 9
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_DOESNT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_RECEIVE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PERMISSION_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PAYMENT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->PAYMENT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    return-object v0
.end method
