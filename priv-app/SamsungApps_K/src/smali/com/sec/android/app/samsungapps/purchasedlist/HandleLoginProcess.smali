.class public Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;

.field private c:Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;)V
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 191
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->a:Landroid/content/Context;

    .line 192
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->b:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;

    .line 193
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 194
    new-instance v0, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->c:Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    .line 195
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->b:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->b:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;->onLogedinOk()V

    .line 174
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;)V
    .locals 2

    .prologue
    .line 18
    sget-object v1, Lcom/sec/android/app/samsungapps/purchasedlist/c;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->DONE:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;)V
    .locals 2

    .prologue
    .line 18
    sget-object v1, Lcom/sec/android/app/samsungapps/purchasedlist/c;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->FINISH:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public checkLogin()V
    .locals 2

    .prologue
    .line 148
    sget-object v1, Lcom/sec/android/app/samsungapps/purchasedlist/c;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 163
    :goto_0
    return-void

    .line 151
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->a()V

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_1

    .line 157
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->DONE:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 155
    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    .line 161
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->TRY_LOGIN:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected entry()V
    .locals 3

    .prologue
    .line 62
    sget-object v1, Lcom/sec/android/app/samsungapps/purchasedlist/c;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 77
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 65
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->a()V

    goto :goto_0

    .line 68
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->b:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->b:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$IObserver;->onNeedFinish()V

    goto :goto_0

    .line 73
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->c:Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;->createCancellableLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/purchasedlist/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/purchasedlist/a;-><init>(Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;->start(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;)V

    .line 74
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->a:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/purchasedlist/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/purchasedlist/b;-><init>(Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected exit()V
    .locals 3

    .prologue
    .line 34
    sget-object v1, Lcom/sec/android/app/samsungapps/purchasedlist/c;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 46
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 43
    :pswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;->end()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HandleLoginProcess::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->IDLE:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getIdleState()Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    move-result-object v0

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 223
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/c;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 226
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/samsungapps/purchasedlist/c;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;->FINISH:Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/purchasedlist/HandleLoginProcess;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 226
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public release()V
    .locals 1

    .prologue
    .line 199
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 200
    return-void
.end method
