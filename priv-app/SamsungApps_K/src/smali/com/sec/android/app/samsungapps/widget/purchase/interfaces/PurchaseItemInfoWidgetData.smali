.class public Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->c:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 20
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    .line 21
    return-void
.end method


# virtual methods
.method public appTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public category()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "category"

    return-object v0
.end method

.method public count()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public discountFlag()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->isDiscounted()Z

    move-result v0

    return v0
.end method

.method public getDiscountPrice()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    .line 69
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->c:Landroid/content/Context;

    const v3, 0x7f08029b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->createFormatter(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getBuyPrice()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNormalPrice()Ljava/lang/String;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->c:Landroid/content/Context;

    const v3, 0x7f08029b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->createFormatter(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getNormalPrice()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPurchaseItemCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseItemPrice()D
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getBuyPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public isAlreadyPurchasedContent()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isAlreadyPurchased()Z

    move-result v0

    return v0
.end method

.method public isWidgetContent()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isWidgetContent()Z

    move-result v0

    return v0
.end method

.method public productImageURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductImageURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public rating()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->c:Landroid/content/Context;

    .line 75
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 76
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    .line 77
    return-void
.end method

.method public sellerInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
