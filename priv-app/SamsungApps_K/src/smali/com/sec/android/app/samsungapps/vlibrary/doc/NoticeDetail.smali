.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public noticeDate:Ljava/lang/String;

.field public noticeDescription:Ljava/lang/String;

.field public noticeId:Ljava/lang/String;

.field public noticeTitle:Ljava/lang/String;

.field public noticeType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeId:Ljava/lang/String;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeType:I

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeTitle:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDescription:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDate:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeId:Ljava/lang/String;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeType:I

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeTitle:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDescription:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDate:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeId:Ljava/lang/String;

    .line 27
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeType:I

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeTitle:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDescription:Ljava/lang/String;

    .line 30
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDate:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getNoticeDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDate:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeId:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNoticeType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeType:I

    return v0
.end method

.method public setNoticeDate(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDate:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setNoticeDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeDescription:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setNoticeID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeId:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setNoticeTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeTitle:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setNoticeType(I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;->noticeType:I

    .line 51
    return-void
.end method
