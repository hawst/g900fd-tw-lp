.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

.field private h:Ljava/lang/String;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

.field private k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a:Landroid/os/Handler;

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->c:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->d:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->e:Ljava/lang/String;

    .line 37
    iput-wide p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->f:J

    .line 38
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->h:Ljava/lang/String;

    .line 39
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->l:Ljava/lang/String;

    .line 40
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/g;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 51
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 55
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 69
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/i;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 71
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->e:Ljava/lang/String;

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->f:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->d:Ljava/lang/String;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/h;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;)V

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor$IRequestFILEObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->request()V

    goto :goto_0

    .line 74
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->WAIT_LOCK_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    goto :goto_0

    .line 77
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->i:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->h:Ljava/lang/String;

    move-object v5, p0

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto :goto_0

    .line 82
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    invoke-interface {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;->onSelfUpdateResult(Z)V

    goto :goto_0

    .line 85
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->l:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;->onInstallFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;->onSelfUpdateResult(Z)V

    goto :goto_0

    .line 91
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;->onInstalling()V

    goto :goto_0

    .line 94
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->cancel()Z

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;)V

    return-void
.end method

.method public onCanceled()V
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 155
    return-void
.end method

.method public onForegroundInstalling()V
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->ON_FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 189
    return-void
.end method

.method public onInstallFailed()V
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 194
    return-void
.end method

.method public onInstallFailed(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->l:Ljava/lang/String;

    .line 183
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->SILENCE_INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 184
    return-void
.end method

.method public onInstallSuccess()V
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 199
    return-void
.end method

.method public onNeedDownloadThrough3GConnection()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->j:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;->handoverAgreeFromWiFiTo3G(Z)V

    .line 150
    return-void
.end method

.method public onProgress(I)V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->f:J

    invoke-interface {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;->onProgress(IJ)V

    .line 145
    :cond_0
    return-void
.end method

.method public onRequestFILEResult(Z)V
    .locals 1

    .prologue
    .line 159
    if-eqz p1, :cond_0

    .line 160
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    goto :goto_0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;

    .line 168
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    .line 60
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$State;)V

    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;)V

    .line 203
    return-void
.end method
