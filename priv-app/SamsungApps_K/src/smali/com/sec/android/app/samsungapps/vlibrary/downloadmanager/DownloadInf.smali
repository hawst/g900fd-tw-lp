.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;


# virtual methods
.method public abstract compareByGUID(Ljava/lang/String;)Z
.end method

.method public abstract compareByProductID(Ljava/lang/String;)Z
.end method

.method public abstract deleteFile(Landroid/content/Context;)Z
.end method

.method public abstract getAbsPath(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
.end method

.method public abstract getGUID()Ljava/lang/String;
.end method

.method public abstract getOrderID()Ljava/lang/String;
.end method

.method public abstract getOrderItemSeq()Ljava/lang/String;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getProductName()Ljava/lang/String;
.end method

.method public abstract getProgressPercentage()I
.end method

.method public abstract getUID()Ljava/lang/String;
.end method

.method public abstract isAD()Z
.end method

.method public abstract isCompletelyDownloaded(Landroid/content/Context;)Z
.end method

.method public abstract isSPP()Z
.end method

.method public abstract setContentSize(J)V
.end method

.method public abstract setDownloadURI(Ljava/lang/String;)V
.end method

.method public abstract setOrderID(Ljava/lang/String;)V
.end method

.method public abstract setOrderIDSeq(Ljava/lang/String;)V
.end method
