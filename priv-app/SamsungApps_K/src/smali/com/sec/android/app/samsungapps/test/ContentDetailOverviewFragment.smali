.class public Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->setWidget(Lcom/sec/android/app/samsungapps/widget/CommonWidget;)V

    .line 20
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreate(Landroid/os/Bundle;)V

    .line 21
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    return-object v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->release()V

    .line 45
    return-void
.end method

.method public setResizeInMultiWindow()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method
