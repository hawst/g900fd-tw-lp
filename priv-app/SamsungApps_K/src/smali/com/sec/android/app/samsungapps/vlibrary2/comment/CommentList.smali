.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
.super Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field public static final COMMENT_LIST_TYPE_EXPERT:I = 0x1

.field public static final COMMENT_LIST_TYPE_USERS:I = 0x2

.field private static final serialVersionUID:J = 0x701eadf31b7bd068L


# instance fields
.field private mProductID:Ljava/lang/String;

.field private mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private mType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    if-eqz p3, :cond_0

    const/16 v0, 0x19

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mType:I

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mProductID:Ljava/lang/String;

    .line 37
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mType:I

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mProductID:Ljava/lang/String;

    .line 39
    return-void

    .line 35
    :cond_0
    const/16 v0, 0xf

    goto :goto_0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 74
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_1

    .line 52
    const/4 v0, 0x0

    .line 54
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 63
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_1
    return-void

    .line 58
    :cond_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    goto :goto_0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mProductID:Ljava/lang/String;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 45
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 92
    return-void
.end method
