.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _MultipleDownloadCountCheck:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _UpdateDetailViewAfterGetDetail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->_UpdateDetailViewAfterGetDetail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->_MultipleDownloadCountCheck:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 16
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->onFinalResult(Z)V

    return-void
.end method

.method private isLogedIn()Z
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->multipleDownloadCountCheck()V

    .line 39
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->_UpdateDetailViewAfterGetDetail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;)V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method protected multipleDownloadCountCheck()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->_MultipleDownloadCountCheck:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/content/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/DownloadPrecheckForDetailPage;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 50
    return-void
.end method
