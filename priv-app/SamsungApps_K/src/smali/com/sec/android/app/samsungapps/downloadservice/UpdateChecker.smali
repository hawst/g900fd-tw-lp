.class public Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->e:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->c:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->b:Ljava/lang/String;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;)V
    .locals 2

    .prologue
    .line 15
    sget-object v1, Lcom/sec/android/app/samsungapps/downloadservice/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->SUCCEED:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;)V
    .locals 2

    .prologue
    .line 15
    sget-object v1, Lcom/sec/android/app/samsungapps/downloadservice/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->FAILED:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected entry()V
    .locals 5

    .prologue
    .line 44
    sget-object v1, Lcom/sec/android/app/samsungapps/downloadservice/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 57
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 46
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->d:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->d:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;->onFailed()V

    goto :goto_0

    .line 51
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->e:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->getVersionCodeList()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->e:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    new-instance v4, Lcom/sec/android/app/samsungapps/downloadservice/g;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/downloadservice/g;-><init>(Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;)V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->updateCheck(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 54
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->d:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->d:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;->onSuccess()V

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected exit()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->IDLE:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->getIdleState()Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->e:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    return-object v0
.end method

.method public send()V
    .locals 2

    .prologue
    .line 72
    sget-object v1, Lcom/sec/android/app/samsungapps/downloadservice/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 77
    :goto_0
    :pswitch_0
    return-void

    .line 76
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;->SENDING:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker;->d:Lcom/sec/android/app/samsungapps/downloadservice/UpdateChecker$IUpdateCheckerObserver;

    .line 40
    return-void
.end method
