.class public Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

.field protected _ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

.field protected _IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

.field private _IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

.field protected mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

.field odc:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    .line 43
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 44
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->onFileDownloadingResult(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    return-void
.end method

.method private final getAbsoluteFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 304
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->odc:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;->getIRequestFileInfo()Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;)V

    .line 305
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->getAbsPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isForceUpdate()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 84
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v2

    if-nez v2, :cond_0

    .line 98
    :goto_0
    :pswitch_0
    return v0

    .line 88
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->getForceUpdate()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;

    move-result-object v2

    .line 89
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/q;->a:[I

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ForceUpdateType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 94
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 96
    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onFileDownloadingResult(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 154
    if-eqz p1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_Context:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->odc:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;->a:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;->getSaveFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getGUID()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;->createInstaller(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    move-result-object v0

    .line 187
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyInstalling()V

    .line 195
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    const-string v1, "0"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyInstallFailed(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->exitSamsungApps()V

    .line 193
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->onFinalResult(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected askUpdate()V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;->askUserUpdateODC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/m;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 60
    return-void
.end method

.method protected createDLState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 115
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getProductName()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 116
    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setCancellable(Z)V

    .line 117
    return-object v0
.end method

.method protected createRequestFile(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/Request;
    .locals 4

    .prologue
    .line 127
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/r;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V

    .line 128
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/n;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V

    invoke-direct {v2, v0, p1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/FileWriter2;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;)V

    invoke-direct {v1, v0, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;Landroid/content/Context;)V

    .line 143
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/o;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/o;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 150
    return-object v1
.end method

.method protected exitSamsungApps()V
    .locals 1

    .prologue
    .line 204
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    .line 205
    return-void
.end method

.method protected getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    const-string v0, "odc9820938409234.apk"

    return-object v0
.end method

.method protected getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->ODC_PACKAGE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method protected getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    const-string v0, "odc9820938409234.apk"

    return-object v0
.end method

.method protected getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "Samsung Apps"

    return-object v0
.end method

.method protected getSizeToDownload()J
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    .line 246
    if-eqz v0, :cond_0

    .line 248
    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcSize:I

    int-to-long v0, v0

    .line 252
    :goto_0
    return-wide v0

    .line 251
    :cond_0
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 252
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected getURLToDownload()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    .line 258
    if-eqz v0, :cond_0

    .line 260
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcUrl:Ljava/lang/String;

    .line 264
    :goto_0
    return-object v0

    .line 263
    :cond_0
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 264
    const-string v0, ""

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->askUpdate()V

    .line 49
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;)V
    .locals 2

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    .line 105
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->odc:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/s;

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->createDLState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 107
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_Context:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->createRequestFile(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/Request;

    move-result-object v0

    .line 109
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 110
    return-void
.end method

.method protected notifyInstallCompleted()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyInstallCompleted()V

    .line 200
    return-void
.end method

.method protected onUserAgreeToUpdate(Z)V
    .locals 2

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 80
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->isForceUpdate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->onFinalResult(Z)V

    .line 73
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    goto :goto_0

    .line 77
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->onFinalResult(Z)V

    goto :goto_0
.end method
