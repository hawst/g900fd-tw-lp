.class public Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private _ClassTypeToBeCreated:Ljava/lang/Class;

.field private _Container:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;

.field _ObjectList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ObjectList:Ljava/util/ArrayList;

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_Container:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ClassTypeToBeCreated:Ljava/lang/Class;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ObjectList:Ljava/util/ArrayList;

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_Container:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_Container:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;

    .line 23
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;->clearObjectList()V

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ClassTypeToBeCreated:Ljava/lang/Class;

    .line 25
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getObjectArray()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ObjectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 4

    .prologue
    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ClassTypeToBeCreated:Ljava/lang/Class;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 36
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_ObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_Container:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;->_Container:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;->addObject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 44
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 46
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 48
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 50
    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 52
    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method
