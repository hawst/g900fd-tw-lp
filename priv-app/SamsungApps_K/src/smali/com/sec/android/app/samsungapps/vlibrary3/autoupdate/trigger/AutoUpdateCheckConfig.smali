.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract existAlarm()Z
.end method

.method public abstract getDefaultInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
.end method

.method public abstract getInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
.end method

.method public abstract isTimedOutByCompareLoadedIntervalAndLastUpdTime()Z
.end method

.method public abstract loadInterval()Z
.end method

.method public abstract readLastUpdateCheckTime()J
.end method

.method public abstract registerAlarm(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
.end method

.method public abstract saveAndSetInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
.end method

.method public abstract writeLastUpdateCheckedTimeNow()V
.end method
