.class final Lcom/sec/android/app/samsungapps/magazinehome/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/magazinehome/d;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 392
    if-eqz p1, :cond_0

    .line 394
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;

    .line 400
    if-eqz p2, :cond_1

    .line 402
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/d;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    .line 411
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/d;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    goto :goto_0

    .line 404
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->needRetry()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/d;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->requestFromWeb(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    goto :goto_0

    .line 407
    :cond_2
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/d;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    goto :goto_0
.end method
