.class public Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.source "ProGuard"


# static fields
.field public static final NUM_OF_PADDING:I = 0x2


# instance fields
.field a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

.field b:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

.field private f:Landroid/content/Context;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 10

    .prologue
    .line 36
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 37
    check-cast p9, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    .line 38
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->f:Landroid/content/Context;

    .line 40
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    .line 41
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-nez v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->findDownloadingContent(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)Z
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a()Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isKNOXApp()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->GUID:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isExecutable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    .line 366
    :goto_0
    return-void

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 357
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 358
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->GUID:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isExecutable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected cancelDl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->cancelDl(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;->updateList(Z)V

    .line 89
    :cond_0
    return-void
.end method

.method protected handleWating()V
    .locals 2

    .prologue
    .line 370
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleWating()V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;->refreshActionBar(Z)V

    .line 374
    :cond_0
    return-void
.end method

.method public onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/viewholder/n;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    goto :goto_0

    .line 60
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;->updateList(Z)V

    goto :goto_1

    .line 65
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;->refreshActionBar(Z)V

    goto :goto_1

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected prepareView()V
    .locals 0

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showProductType()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showPurchaseStatus()V

    .line 47
    return-void
.end method

.method public showCommonStatus()V
    .locals 10

    .prologue
    const v9, 0x7f0c0056

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->installedTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setFocusable(Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iget v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Position:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->purchasedDate:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->purchasedDate:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deleteCheckboxGap:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->installedTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_9

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->progressLayout:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showRating()V

    .line 131
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "1"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v8, :cond_8

    .line 133
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v7}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 142
    :goto_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_0

    .line 112
    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isPrePostApp()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->purchasedDate:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 115
    :cond_7
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getPurchaseDate()Ljava/lang/String;

    move-result-object v1

    .line 116
    const/16 v2, 0x2d

    const/16 v3, 0x3b

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->purchasedDate:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080113

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v4, v1, v6, v6, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->processDateFormat(Landroid/content/Context;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 136
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v6}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deleteCheckboxGap:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 147
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly:Landroid/view/View;

    const v1, 0x7f0c0044

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setNextFocusLeftId(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setNextFocusRightId(I)V

    goto/16 :goto_0

    .line 159
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setFocusable(Z)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly:Landroid/view/View;

    const v1, 0x7f0c0154

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setNextFocusRightId(I)V

    goto/16 :goto_0
.end method

.method public showProductnormalPrice()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    iget v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Position:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 171
    iget-wide v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->purchasedPrice:D

    .line 172
    iget-wide v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->discountPrice:D

    .line 174
    iget-boolean v5, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->discountFlag:Z

    if-eqz v5, :cond_2

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->normalPrice:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->normalPrice:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->normalPrice:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->normalPrice:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v5

    iget-wide v6, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->price:D

    iget-object v8, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    cmpl-double v1, v3, v9

    if-nez v1, :cond_1

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_defaultPrice:Ljava/lang/String;

    .line 183
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->price:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->price:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/j;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/j;-><init>(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->normalPrice:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/k;-><init>(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 214
    :cond_0
    return-void

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 185
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->normalPrice:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    cmpl-double v3, v1, v9

    if-nez v3, :cond_3

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_defaultPrice:Ljava/lang/String;

    .line 191
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->price:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 189
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v3, v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public showPurchaseStatus()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const v9, 0x7f0801e4

    const v8, 0x7f08015a

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 217
    const-string v0, ""

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 220
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070038

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v4, v2, 0x2

    sub-int/2addr v3, v4

    .line 221
    int-to-float v1, v1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v4

    float-to-int v1, v1

    .line 223
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_View:Landroid/view/View;

    if-nez v4, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->clearFocus()V

    .line 228
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showCommonStatus()V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showProductName()V

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showProductImg()V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showAdultIcon()V

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showCategory()V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showProductnormalPrice()V

    .line 236
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly:Landroid/view/View;

    const v5, 0x7f0c0153

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/samsungapps/viewholder/l;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/viewholder/l;-><init>(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    if-eqz v4, :cond_2

    .line 250
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v4, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 253
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->hideRating()V

    .line 255
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 258
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    .line 259
    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v4

    .line 261
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->progresstext:Landroid/widget/TextView;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0, v4, v7}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->makeDownloadProgressText(Ljava/lang/Long;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/samsungapps/viewholder/m;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/viewholder/m;-><init>(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eq v0, v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-ne v0, v4, :cond_c

    .line 333
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020092

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    const v5, 0x7f090035

    invoke-virtual {v0, v4, v5}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 340
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-static {v0, v4, v3, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->buttonTextSizeResize(Landroid/content/Context;Landroid/widget/TextView;II)I

    move-result v0

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v1, v2, v7, v2, v7}, Landroid/widget/Button;->setPadding(IIII)V

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    int-to-float v0, v0

    invoke-virtual {v1, v10, v0}, Landroid/widget/Button;->setTextSize(IF)V

    goto/16 :goto_0

    .line 263
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->progressLayout:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->showRating()V

    .line 267
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    .line 270
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v4, v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 271
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->hasOrderID()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 275
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 279
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 282
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b()V

    goto/16 :goto_1

    .line 284
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    const v4, 0x7f0801e5

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 290
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 294
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->b()V

    goto/16 :goto_1

    .line 299
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    const v4, 0x7f0801e5

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 336
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020091

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    const v5, 0x7f090039

    invoke-virtual {v0, v4, v5}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    goto/16 :goto_2
.end method
