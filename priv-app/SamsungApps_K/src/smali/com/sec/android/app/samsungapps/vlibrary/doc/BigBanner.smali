.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;
.source "ProGuard"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public interactionBannerYn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/i;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/i;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;-><init>(Landroid/os/Parcel;)V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;->interactionBannerYn:Z

    .line 32
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;->interactionBannerYn:Z

    .line 26
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 27
    return-void
.end method


# virtual methods
.method public isBigBanner()Z
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public isInteractionBanner()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;->interactionBannerYn:Z

    return v0
.end method

.method public setInteractionBanner(Z)V
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;->interactionBannerYn:Z

    .line 52
    return-void
.end method
