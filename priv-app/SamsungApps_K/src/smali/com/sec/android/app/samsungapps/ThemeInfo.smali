.class public Lcom/sec/android/app/samsungapps/ThemeInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final NONE:I = -0x1

.field public static final THEME_BLACK:I = 0xd

.field public static final THEME_BLACK_DIALOG:I = 0xe

.field public static final THEME_BLACK_DIALOG_NOTITLE:I = 0xf

.field public static final THEME_BLACK_NOTITLE:I = 0x10

.field public static final THEME_BLACK_NO_ACTIONBAR:I = 0x11

.field public static final THEME_DEVICE:I = 0x1

.field public static final THEME_DEVICE_DIALOG:I = 0x3

.field public static final THEME_DEVICE_DIALOG_NOTITLE:I = 0x4

.field public static final THEME_DEVICE_NOTITLE:I = 0x5

.field public static final THEME_DEVICE_NOTITLE_TRANS:I = 0x6

.field public static final THEME_DEVICE_NO_ACTIONBAR:I = 0x7

.field public static final THEME_DEVICE_TRANS:I = 0x2

.field public static final THEME_NO_DISPLAY:I = 0x12

.field public static final THEME_PLATFORM_MAX:I = 0x103013d

.field public static final THEME_PLATFORM_MIN:I = 0x1030008

.field public static final THEME_WHITE:I = 0x8

.field public static final THEME_WHITE_DIALOG:I = 0x9

.field public static final THEME_WHITE_DIALOG_NOTITLE:I = 0xa

.field public static final THEME_WHITE_NOTITLE:I = 0xb

.field public static final THEME_WHITE_NO_ACTIONBAR:I = 0xc

.field private static a:Lcom/sec/android/app/samsungapps/ThemeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/ThemeInfo;->a:Lcom/sec/android/app/samsungapps/ThemeInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/ThemeInfo;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/samsungapps/ThemeInfo;->a:Lcom/sec/android/app/samsungapps/ThemeInfo;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/android/app/samsungapps/ThemeInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/ThemeInfo;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/ThemeInfo;->a:Lcom/sec/android/app/samsungapps/ThemeInfo;

    .line 62
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/ThemeInfo;->a:Lcom/sec/android/app/samsungapps/ThemeInfo;

    return-object v0
.end method

.method public static getTheme(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 133
    .line 137
    if-nez p0, :cond_0

    .line 139
    const-string v1, "ThemeInfo::getTheme::Context is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 171
    :goto_0
    return v0

    .line 143
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 145
    :cond_1
    const-string v1, "ThemeInfo::getTheme::pkgName is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 153
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 154
    if-nez v1, :cond_3

    .line 156
    const-string v1, "ThemeInfo::getTheme::pkgInfo is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 162
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ThemeInfo::getTheme::NameNotFoundException,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_3
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    goto :goto_0
.end method

.method public static isBlackTheme(I)Z
    .locals 2

    .prologue
    const v1, 0x1030006

    .line 104
    if-eqz p0, :cond_0

    const v0, 0x1030005

    if-eq p0, v0, :cond_0

    const v0, 0x103000b

    if-eq p0, v0, :cond_0

    if-eq p0, v1, :cond_0

    if-eq p0, v1, :cond_0

    const v0, 0x103006b

    if-eq p0, v0, :cond_0

    const v0, 0x1030128

    if-eq p0, v0, :cond_0

    const v0, 0x103006f

    if-eq p0, v0, :cond_0

    const v0, 0x103012e

    if-eq p0, v0, :cond_0

    const v0, 0x1030071

    if-eq p0, v0, :cond_0

    const v0, 0x1030129

    if-eq p0, v0, :cond_0

    const v0, 0x103006c

    if-eq p0, v0, :cond_0

    const v0, 0x7f11001d

    if-eq p0, v0, :cond_0

    const v0, 0x7f110020

    if-ne p0, v0, :cond_1

    .line 121
    :cond_0
    const/4 v0, 0x1

    .line 128
    :goto_0
    return v0

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getTheme(I)I
    .locals 1

    .prologue
    .line 93
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(II)I

    move-result v0

    return v0
.end method

.method public getTheme(II)I
    .locals 6

    .prologue
    const v2, 0x103006c

    const v1, 0x103000b

    const/16 v5, 0xd

    const/16 v4, 0xa

    const/16 v3, 0xe

    .line 75
    const/4 v0, 0x7

    if-lt p2, v0, :cond_0

    if-le p2, v3, :cond_1

    .line 79
    :cond_0
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 82
    :cond_1
    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    .line 83
    :cond_2
    :goto_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ThemeInfo::getTheme::aThemeType is wrong value, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Api :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 88
    :cond_3
    return v0

    .line 82
    :pswitch_0
    if-gt p2, v4, :cond_4

    const v0, 0x1030005

    goto :goto_0

    :cond_4
    if-gt p2, v5, :cond_5

    const v0, 0x103006b

    goto :goto_0

    :cond_5
    if-lt p2, v3, :cond_2

    const v0, 0x1030128

    goto :goto_0

    :pswitch_1
    if-gt p2, v4, :cond_6

    const v0, 0x103000f

    goto :goto_0

    :cond_6
    if-gt p2, v5, :cond_7

    const v0, 0x103007d

    goto :goto_0

    :cond_7
    if-lt p2, v3, :cond_2

    const v0, 0x103013c

    goto :goto_0

    :pswitch_2
    if-gt p2, v4, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    if-gt p2, v5, :cond_9

    const v0, 0x103006f

    goto :goto_0

    :cond_9
    if-lt p2, v3, :cond_2

    const v0, 0x103012e

    goto :goto_0

    :pswitch_3
    if-gt p2, v4, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    if-gt p2, v5, :cond_b

    const v0, 0x1030071

    goto :goto_0

    :cond_b
    if-lt p2, v3, :cond_2

    const v0, 0x1030130

    goto :goto_0

    :pswitch_4
    if-gt p2, v4, :cond_c

    const v0, 0x1030006

    goto :goto_0

    :cond_c
    if-gt p2, v5, :cond_d

    move v0, v2

    goto :goto_0

    :cond_d
    if-lt p2, v3, :cond_2

    const v0, 0x1030129

    goto :goto_0

    :pswitch_5
    if-gt p2, v4, :cond_e

    const v0, 0x1030010

    goto :goto_0

    :cond_e
    if-gt p2, v5, :cond_f

    const v0, 0x1030010

    goto :goto_0

    :cond_f
    if-lt p2, v3, :cond_2

    const v0, 0x1030010

    goto/16 :goto_0

    :pswitch_6
    if-gt p2, v4, :cond_10

    const v0, 0x1030006

    goto/16 :goto_0

    :cond_10
    if-gt p2, v5, :cond_11

    move v0, v2

    goto/16 :goto_0

    :cond_11
    if-lt p2, v3, :cond_2

    const v0, 0x1030129

    goto/16 :goto_0

    :pswitch_7
    if-gt p2, v4, :cond_12

    const v0, 0x103000c

    goto/16 :goto_0

    :cond_12
    if-gt p2, v5, :cond_13

    const v0, 0x103006e

    goto/16 :goto_0

    :cond_13
    if-lt p2, v3, :cond_2

    const v0, 0x103012b

    goto/16 :goto_0

    :pswitch_8
    if-gt p2, v4, :cond_14

    move v0, v1

    goto/16 :goto_0

    :cond_14
    if-gt p2, v5, :cond_15

    const v0, 0x1030073

    goto/16 :goto_0

    :cond_15
    if-lt p2, v3, :cond_2

    const v0, 0x1030132

    goto/16 :goto_0

    :pswitch_9
    if-gt p2, v4, :cond_16

    move v0, v1

    goto/16 :goto_0

    :cond_16
    if-gt p2, v5, :cond_17

    const v0, 0x1030075

    goto/16 :goto_0

    :cond_17
    if-lt p2, v3, :cond_2

    const v0, 0x1030134

    goto/16 :goto_0

    :pswitch_a
    if-gt p2, v4, :cond_18

    const v0, 0x103000d

    goto/16 :goto_0

    :cond_18
    if-gt p2, v5, :cond_19

    const v0, 0x10300f0

    goto/16 :goto_0

    :cond_19
    if-lt p2, v3, :cond_2

    const v0, 0x103012c

    goto/16 :goto_0

    :pswitch_b
    if-gt p2, v4, :cond_1a

    const v0, 0x103000d

    goto/16 :goto_0

    :cond_1a
    if-gt p2, v5, :cond_1b

    const v0, 0x10300f0

    goto/16 :goto_0

    :cond_1b
    if-lt p2, v3, :cond_2

    const v0, 0x103012c

    goto/16 :goto_0

    :pswitch_c
    if-gt p2, v4, :cond_1c

    const v0, 0x1030008

    goto/16 :goto_0

    :cond_1c
    if-gt p2, v5, :cond_1d

    const v0, 0x103006b

    goto/16 :goto_0

    :cond_1d
    if-lt p2, v3, :cond_2

    const v0, 0x1030128

    goto/16 :goto_0

    :pswitch_d
    if-gt p2, v4, :cond_1e

    move v0, v1

    goto/16 :goto_0

    :cond_1e
    if-gt p2, v5, :cond_1f

    const v0, 0x103006f

    goto/16 :goto_0

    :cond_1f
    if-lt p2, v3, :cond_2

    const v0, 0x103012e

    goto/16 :goto_0

    :pswitch_e
    if-gt p2, v4, :cond_20

    move v0, v1

    goto/16 :goto_0

    :cond_20
    if-gt p2, v5, :cond_21

    const v0, 0x1030071

    goto/16 :goto_0

    :cond_21
    if-lt p2, v3, :cond_2

    const v0, 0x1030130

    goto/16 :goto_0

    :pswitch_f
    if-gt p2, v4, :cond_22

    const v0, 0x1030009

    goto/16 :goto_0

    :cond_22
    if-gt p2, v5, :cond_23

    move v0, v2

    goto/16 :goto_0

    :cond_23
    if-lt p2, v3, :cond_2

    const v0, 0x1030129

    goto/16 :goto_0

    :pswitch_10
    if-gt p2, v4, :cond_24

    const v0, 0x1030009

    goto/16 :goto_0

    :cond_24
    if-gt p2, v5, :cond_25

    move v0, v2

    goto/16 :goto_0

    :cond_25
    if-lt p2, v3, :cond_2

    const v0, 0x1030129

    goto/16 :goto_0

    :pswitch_11
    const v0, 0x1030055

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public isAvailable(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 182
    if-nez p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 189
    :cond_1
    const v1, 0x1030008

    if-lt p1, v1, :cond_2

    const v1, 0x103013d

    if-le p1, v1, :cond_0

    .line 195
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
