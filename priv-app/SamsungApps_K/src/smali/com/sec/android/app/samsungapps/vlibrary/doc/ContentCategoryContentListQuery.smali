.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.source "ProGuard"


# instance fields
.field mContentCategoryID:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ContentCategoryProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;->mContentCategoryID:Ljava/lang/String;

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;->mContentCategoryID:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method protected createRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;->getContentCategoryID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->contentCategoryProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 26
    return-object v0
.end method

.method public getContentCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;->mContentCategoryID:Ljava/lang/String;

    return-object v0
.end method
