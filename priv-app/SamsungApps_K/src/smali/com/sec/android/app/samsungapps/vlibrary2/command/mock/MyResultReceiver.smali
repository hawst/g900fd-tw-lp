.class public Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field public bGetResult:Z

.field public bResult:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bGetResult:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bResult:Z

    .line 24
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bGetResult:Z

    .line 25
    return-void
.end method

.method public fail()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bGetResult:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bResult:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCommandResult(Z)V
    .locals 1

    .prologue
    .line 10
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bResult:Z

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bGetResult:Z

    .line 12
    return-void
.end method

.method public success()Z
    .locals 1

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bResult:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MyResultReceiver;->bGetResult:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
