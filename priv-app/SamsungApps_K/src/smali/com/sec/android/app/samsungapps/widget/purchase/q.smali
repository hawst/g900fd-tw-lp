.class final Lcom/sec/android/app/samsungapps/widget/purchase/q;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 142
    const v0, 0x7f0c017d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 143
    const v1, 0x7f0c0204

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 144
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->c(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->c(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->c(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    move-result-object v2

    invoke-interface {v2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->getPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->selectPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->a(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/widget/purchase/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/s;->notifyDataSetChanged()V

    .line 154
    :cond_3
    invoke-virtual {v1}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0802ae

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/q;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->c(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->onRegisterCreditCard()V

    goto :goto_0
.end method
