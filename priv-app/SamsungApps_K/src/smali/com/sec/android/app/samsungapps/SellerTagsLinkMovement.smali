.class public Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;
.super Landroid/text/method/LinkMovementMethod;
.source "ProGuard"


# static fields
.field public static mInstance:Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;->mInstance:Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;->mInstance:Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;->mInstance:Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;->mInstance:Lcom/sec/android/app/samsungapps/SellerTagsLinkMovement;

    goto :goto_0
.end method


# virtual methods
.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 22
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/LinkMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 25
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
