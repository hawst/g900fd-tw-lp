.class final Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgr$ISeltUpdateDownloadMgrObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInstallFailed(Ljava/lang/String;)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x64

    const/4 v8, 0x1

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    move-result-object v11

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    const/4 v9, 0x0

    move-wide v6, v4

    move v10, v8

    invoke-static/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/NotiInfoCreator;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Z)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;

    move-result-object v0

    invoke-interface {v11, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->removeItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    .line 165
    return-void
.end method

.method public final onInstalling()V
    .locals 12

    .prologue
    const-wide/16 v4, 0x64

    const/4 v8, 0x1

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    move-result-object v11

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    const/4 v9, 0x0

    move-wide v6, v4

    move v10, v8

    invoke-static/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/NotiInfoCreator;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Z)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;

    move-result-object v0

    invoke-interface {v11, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->addInstallItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 171
    return-void
.end method

.method public final onProgress(IJ)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    move-result-object v11

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    int-to-long v4, p1

    const/4 v9, 0x0

    move-wide v6, p2

    move v10, v8

    invoke-static/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/NotiInfoCreator;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Z)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;

    move-result-object v0

    invoke-interface {v11, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 158
    return-void
.end method

.method public final onSelfUpdateResult(Z)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const-wide/16 v4, 0x64

    const/4 v8, 0x1

    .line 143
    if-eqz p1, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    move-result-object v11

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    move-wide v6, v4

    move v10, v8

    invoke-static/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/NotiInfoCreator;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Z)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;

    move-result-object v0

    invoke-interface {v11, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->setItemInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    .line 152
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;

    move-result-object v11

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    move-wide v6, v4

    move v10, v8

    invoke-static/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/NotiInfoCreator;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Z)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;

    move-result-object v0

    invoke-interface {v11, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;->removeItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/l;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    goto :goto_0
.end method
