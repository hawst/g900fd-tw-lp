.class public Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static _context:Landroid/content/Context;

.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;


# instance fields
.field bRunning:Z

.field knoxInstallCommandQueue:Ljava/util/ArrayList;

.field resultHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    .line 23
    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->_context:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->knoxInstallCommandQueue:Ljava/util/ArrayList;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->bRunning:Z

    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->resultHandler:Landroid/os/Handler;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->checkQueue()V

    return-void
.end method

.method private checkQueue()V
    .locals 2

    .prologue
    .line 53
    monitor-enter p0

    .line 55
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->bRunning:Z

    if-eqz v0, :cond_0

    .line 57
    monitor-exit p0

    .line 65
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->knoxInstallCommandQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 62
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->knoxInstallCommandQueue:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->knoxInstallCommandQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 64
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->_context:Landroid/content/Context;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->install(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;)V

    .line 65
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;
    .locals 1

    .prologue
    .line 27
    sput-object p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->_context:Landroid/content/Context;

    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    .line 31
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    return-object v0
.end method

.method private install(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;)V
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->bRunning:Z

    .line 93
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;->packagePath:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;)V

    invoke-interface {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->installPackage(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)Z

    move-result v0

    .line 109
    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->resultHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 112
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 113
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->resultHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;)Z
    .locals 1

    .prologue
    .line 38
    monitor-enter p0

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->knoxInstallCommandQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x0

    monitor-exit p0

    .line 46
    :goto_0
    return v0

    .line 45
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->checkQueue()V

    .line 46
    const/4 v0, 0x1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
