.class public Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _CardTypeTable:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

.field public city:Ljava/lang/String;

.field public creditCardLast4Number:Ljava/lang/String;

.field public creditCardType:Ljava/lang/String;

.field public expireMonth:Ljava/lang/String;

.field public expireYear:Ljava/lang/String;

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field public state:Ljava/lang/String;

.field public street:Ljava/lang/String;

.field valid:Z

.field public validYN:Ljava/lang/String;

.field public zip:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, "0000"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->creditCardLast4Number:Ljava/lang/String;

    .line 9
    const-string v0, "2000"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireYear:Ljava/lang/String;

    .line 10
    const-string v0, "00"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->expireMonth:Ljava/lang/String;

    .line 11
    const-string v0, "000"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->creditCardType:Ljava/lang/String;

    .line 19
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->valid:Z

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 21
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    const-string v2, "001"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->VISA:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    const-string v3, "002"

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->MasterCard:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    const-string v3, "003"

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->AmericanExpress:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    const-string v3, "004"

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Discover:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    const-string v3, "007"

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->JCB:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->_CardTypeTable:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    .line 42
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 71
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->clear()V

    .line 76
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->valid:Z

    .line 66
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCreditCardLast4Digit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->creditCardLast4Number:Ljava/lang/String;

    return-object v0
.end method

.method public getCreditCardType()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;
    .locals 6

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->creditCardType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->creditCardType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Invalid:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    .line 110
    :goto_0
    return-object v0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->creditCardType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->_CardTypeTable:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 105
    invoke-virtual {v4, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 107
    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/a;->a()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 110
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Etc:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->valid:Z

    return v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->clear()V

    .line 47
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method
