.class public Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private final COUNT_LIST_PER_ITEM:I

.field private _Context:Landroid/content/Context;

.field private mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

.field private mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;)V
    .locals 2

    .prologue
    const/16 v1, 0xf

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->COUNT_LIST_PER_ITEM:I

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->_Context:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    return-object v0
.end method

.method private requestPurchasedList()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;)V

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/list/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 62
    return-void
.end method


# virtual methods
.method public getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    return-object v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->_Context:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->clear()V

    .line 83
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    .line 84
    return-void
.end method

.method public requestMore()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADINGMORE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->requestPurchasedList()V

    .line 68
    return-void
.end method

.method public setIListViewStateManager(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 26
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->clear()V

    .line 35
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListManager;->requestPurchasedList()V

    .line 36
    return-void
.end method
