.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

.field odcVersion:Ljava/lang/String;

.field openAPIVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->load()V

    .line 12
    return-void
.end method

.method private load()V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getOpenApiVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->openAPIVersion:Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->loadODCVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->odcVersion:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public getODCVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->odcVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenAPIVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->openAPIVersion:Ljava/lang/String;

    return-object v0
.end method
