.class final Lcom/sec/android/app/samsungapps/tobelog/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/b;->a:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/b;->a:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    invoke-static {p2}, Lcom/sec/spp/push/dlc/api/IDlcService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/dlc/api/IDlcService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 116
    const-string v0, "LogCollectorService is connected"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/b;->a:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;)Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;->onServiceConnected()V

    .line 120
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/b;->a:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 127
    const-string v0, "LogCollectorService is disconnected"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/b;->a:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;)Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;->onServiceDisconnected()V

    .line 131
    return-void
.end method
