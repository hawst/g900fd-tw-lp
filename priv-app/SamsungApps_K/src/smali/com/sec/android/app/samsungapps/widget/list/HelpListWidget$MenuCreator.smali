.class public Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/content/Intent;

.field final synthetic c:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->c:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->a:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->b:Landroid/content/Intent;

    .line 78
    return-void
.end method


# virtual methods
.method public createContactNumbers()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 118
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->c:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800f3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/m;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/m;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public createContactUs()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 105
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->c:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/l;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/l;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public createFaq()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->c:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/k;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/k;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public createQuestions()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 4

    .prologue
    .line 81
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItem;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->c:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08010f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/list/j;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/list/j;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/MenuItem;-><init>(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method
