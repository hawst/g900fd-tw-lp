.class public Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    .line 13
    return-void
.end method

.method private getBooleanFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    .line 36
    if-eqz v0, :cond_0

    .line 38
    const-string v0, "1"

    .line 40
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method private getDoubleFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getDouble(Ljava/lang/Object;)D

    move-result-wide v0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getIntFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 17
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLongFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 23
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStringFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    return-object v0
.end method

.method private putToMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    if-eqz p3, :cond_0

    .line 53
    invoke-virtual {p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 55
    :cond_0
    return-void
.end method


# virtual methods
.method public toMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 7

    .prologue
    .line 59
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->mObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 64
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_5

    aget-object v4, v2, v0

    .line 66
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    .line 68
    :try_start_0
    const-string v6, "int"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 70
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->getIntFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v5, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->putToMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    const-string v6, "long"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 75
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->getLongFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v5, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->putToMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 94
    :catch_0
    move-exception v4

    goto :goto_1

    .line 78
    :cond_2
    const-string v6, "double"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_3

    .line 80
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->getDoubleFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v5, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->putToMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_1

    .line 82
    :cond_3
    const-string v6, "String"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    .line 84
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->getStringFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v5, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->putToMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 86
    :cond_4
    const-string v6, "boolean"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 88
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->getBooleanFieldValue(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v5, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/fakemaputil/MapGenerator;->putToMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 96
    :cond_5
    return-object v1
.end method
