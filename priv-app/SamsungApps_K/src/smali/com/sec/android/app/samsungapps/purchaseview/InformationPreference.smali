.class public Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;
.super Landroid/preference/Preference;
.source "ProGuard"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field protected mExpanded:Z

.field protected mPayMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 30
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mExpanded:Z

    .line 31
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mView:Landroid/view/View;

    .line 32
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mPayMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 34
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mPayMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    const v1, 0x7f0400ea

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mView:Landroid/view/View;

    .line 47
    invoke-virtual {p0, p0}, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 48
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 9

    .prologue
    const v8, 0x7f08024b

    const v7, 0x7f080136

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 64
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mPayMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-nez v0, :cond_2

    .line 68
    :cond_0
    const-string v0, "InformationPreference::onBindView::Not Already Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 137
    :cond_1
    :goto_0
    return-void

    .line 72
    :cond_2
    const-string v0, ""

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 75
    sget-object v2, Lcom/sec/android/app/samsungapps/purchaseview/a;->a:[I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mPayMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 112
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-ne v1, v5, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPaymentPrice()D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_8

    .line 117
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f080134

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 129
    :goto_1
    const v0, 0x7f0c0304

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 130
    if-eqz v0, :cond_1

    .line 132
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 133
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 79
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    :goto_2
    const-string v1, "%s\n\n%s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    const v3, 0x7f080303

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 86
    goto :goto_1

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 89
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f080298

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getCount()I

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    :goto_3
    move-object v1, v0

    .line 90
    goto :goto_1

    .line 89
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mDetailContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getURL()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eq v2, v5, :cond_4

    const-string v2, "%s %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 94
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 97
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 99
    goto/16 :goto_1

    .line 102
    :pswitch_3
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isTurkey()Z

    move-result v1

    if-ne v1, v5, :cond_8

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f0802ff

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 120
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f080245

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mView:Landroid/view/View;

    const v1, 0x7f0c0304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 54
    if-eqz v0, :cond_0

    .line 56
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mView:Landroid/view/View;

    return-object v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public setExpanable()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x64

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mView:Landroid/view/View;

    const v1, 0x7f0c0304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-eq v1, v2, :cond_0

    .line 189
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mExpanded:Z

    .line 190
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 191
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 207
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mExpanded:Z

    if-ne v1, v2, :cond_1

    .line 197
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mExpanded:Z

    .line 198
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 199
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 203
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mExpanded:Z

    .line 204
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 205
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0
.end method

.method public setExpanable(Z)V
    .locals 1

    .prologue
    .line 174
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->mExpanded:Z

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/purchaseview/InformationPreference;->setExpanable()V

    .line 177
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
