.class final Lcom/sec/android/app/samsungapps/widget/list/ac;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;)V
    .locals 0

    .prologue
    .line 601
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ac;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ac;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 607
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showUnifiedBillingCreditCard()V

    .line 616
    :goto_0
    return-void

    .line 613
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ac;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/CreditCardDisclaimerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ac;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
