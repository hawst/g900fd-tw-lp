.class final Lcom/sec/android/app/samsungapps/commands/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/b;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 220
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_0032(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    const-class v0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountValidationActivity;

    invoke-static {p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 225
    :cond_0
    return-void
.end method
