.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 11
    const-string v2, "screenShot"

    const-string v3, "2220"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    const-string v0, "productID"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void
.end method
