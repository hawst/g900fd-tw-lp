.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/w;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353
    const-string v0, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->unregisterReceiver()V

    .line 357
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;-><init>(Landroid/content/Intent;)V

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onResultToGetToken(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)V

    .line 360
    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->resultCode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 362
    const-string v0, "error_message"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 365
    const-string v0, "client_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 369
    const-string v0, "check_list"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->getValidationViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    .line 395
    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    .line 397
    const-string v0, "REQUIRED_PROCESS_ACTION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    invoke-virtual {p2, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 401
    invoke-virtual {p2, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/w;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_Context:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Landroid/content/Context;

    move-result-object v0

    invoke-interface {v1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 410
    :cond_0
    return-void
.end method
