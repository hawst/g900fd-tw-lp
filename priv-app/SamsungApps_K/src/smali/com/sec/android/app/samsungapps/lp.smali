.class final Lcom/sec/android/app/samsungapps/lp;
.super Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheck$Stub;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheck$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final showUpdateList(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->c:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/ls;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ls;-><init>(Lcom/sec/android/app/samsungapps/lp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method

.method public final showUpdateListGear2(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    invoke-static {v0, p1, p2}, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->c:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/lv;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lv;-><init>(Lcom/sec/android/app/samsungapps/lp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 132
    return-void
.end method

.method public final updateCheck(Ljava/lang/String;Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->c:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/lq;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/lq;-><init>(Lcom/sec/android/app/samsungapps/lp;Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 71
    return-void
.end method

.method public final updateCheckGear2(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    invoke-static {v0, p1, p2}, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lp;->a:Lcom/sec/android/app/samsungapps/UpdateCheckSVC;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->c:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/lt;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/lt;-><init>(Lcom/sec/android/app/samsungapps/lp;Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 118
    return-void
.end method
