.class public Lcom/sec/android/app/samsungapps/BadgeNotification;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static a:I

.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "com.sec.android.app.samsungapps"

    sput-object v0, Lcom/sec/android/app/samsungapps/BadgeNotification;->b:Ljava/lang/String;

    .line 14
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/BadgeNotification;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getOldBadgeCount()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/sec/android/app/samsungapps/BadgeNotification;->a:I

    return v0
.end method

.method public static setBadgeNotification(Landroid/content/Context;I)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 23
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BadgeNotification : badgeCount: 0,oldBadgeCount:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sec/android/app/samsungapps/BadgeNotification;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 29
    sput v1, Lcom/sec/android/app/samsungapps/BadgeNotification;->a:I

    .line 32
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 34
    const-string v3, "com.sec.android.touchwiz.app.BadgeNotification"

    invoke-virtual {v2, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 37
    invoke-virtual {v2}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v2

    .line 51
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    .line 52
    const/4 v4, 0x0

    const-class v5, Landroid/content/ContentResolver;

    aput-object v5, v3, v4

    .line 53
    const/4 v4, 0x1

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    .line 54
    const/4 v4, 0x2

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    .line 55
    const/4 v4, 0x3

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    .line 57
    const/4 v4, 0x0

    aget-object v4, v2, v4

    const-string v5, "setBadgeCount"

    invoke-virtual {v4, v5, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 58
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    .line 60
    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    aput-object v6, v4, v5

    .line 61
    const/4 v5, 0x1

    new-instance v6, Ljava/lang/String;

    sget-object v7, Lcom/sec/android/app/samsungapps/BadgeNotification;->b:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v6, v4, v5

    .line 62
    const/4 v5, 0x2

    new-instance v6, Ljava/lang/String;

    const-string v7, "com.sec.android.app.samsungapps.Main"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v6, v4, v5

    .line 63
    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 65
    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 76
    :goto_0
    return v0

    .line 69
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 72
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 74
    :catch_2
    move-exception v0

    move v0, v1

    goto :goto_0
.end method
