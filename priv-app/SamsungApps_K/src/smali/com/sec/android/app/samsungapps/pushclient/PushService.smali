.class public Lcom/sec/android/app/samsungapps/pushclient/PushService;
.super Landroid/app/Service;
.source "ProGuard"


# static fields
.field public static final ACTION_PUSHSERVICE_END:Ljava/lang/String; = "intent.action.pushservice.end"

.field public static final ACTION_PUSHSERVICE_START:Ljava/lang/String; = "intent.action.pushservice.start"

.field public static final ACTION_PUSH_SERVICE_CONNECTION_FAILURE:Ljava/lang/String; = "intent.action.push_connFailure"

.field public static final ACTION_PUSH_SERVICE_DEREG:Ljava/lang/String; = "intent.action.push_dereg"

.field public static final ACTION_PUSH_SERVICE_REG:Ljava/lang/String; = "intent.action.push_reg"

.field public static final CMD_DEEPLINK:Ljava/lang/String; = "cmd_deeplink"

.field public static final CMD_DEREGISTRATION:Ljava/lang/String; = "cmd_deregistration"

.field public static final CMD_DOWNLOADING:Ljava/lang/String; = "cmd_downloading"

.field public static final CMD_LOGIN_UNLOCK:Ljava/lang/String; = "cmd_unlock"

.field public static final CMD_REGISTRATION:Ljava/lang/String; = "cmd_registration"

.field public static final DEREG_ERROR_SKIP:Ljava/lang/String; = "SKIP"

.field public static final EXTRA_ACCOUNT_NAME:Ljava/lang/String; = "extra_account_name"

.field public static final EXTRA_COMMAND:Ljava/lang/String; = "extra_command"

.field public static final EXTRA_DEEPLINK_URI:Ljava/lang/String; = "extra_deeplink_uri"

.field public static final EXTRA_GUID:Ljava/lang/String; = "extra_guid"

.field public static final EXTRA_NOTI_MSG:Ljava/lang/String; = "extra_noti_msg"

.field public static final EXTRA_PRODUCT_ID:Ljava/lang/String; = "extra_product_id"

.field public static final EXTRA_PUSH_ERR_CODE:Ljava/lang/String; = "extra.push.err.code"

.field public static final EXTRA_PUSH_STATUS:Ljava/lang/String; = "extra.push.status"

.field public static final EXTRA_VERION:Ljava/lang/String; = "extra_version"

.field public static final KEY_DEFAULT_PUSH_DEEPLINK_NOTIFICATION:I = 0x2328

.field public static final PUSH_STATUS_CONN_FAIL:Ljava/lang/String; = "CONNECTIONFAIL"

.field public static final PUSH_STATUS_FAIL:Ljava/lang/String; = "FAIL"

.field public static final PUSH_STATUS_SIGN_IN_TIME_OUT_FAIL:Ljava/lang/String; = "TIMEOUT"

.field public static final PUSH_STATUS_SUCCESS:Ljava/lang/String; = "SUCCESS"

.field public static final SAMSUNG_APPS_ID:Ljava/lang/String;

.field public static final SENDER_UPPER_STR_SAMSUNGAPPS_ADMIN:Ljava/lang/String;

.field public static final SENDER_UPPER_STR_SAMSUNGAPPS_WEB:Ljava/lang/String;

.field public static final SPP_CLIENT_PACKAGE_NEME:Ljava/lang/String; = "com.sec.spp.push"

.field public static context:Landroid/content/Context;

.field private static g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

.field private b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

.field private c:Ljava/util/LinkedHashSet;

.field private d:Landroid/os/Handler;

.field private e:I

.field private f:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-string v0, "69,38,3d,39,69,66,6a,3c,3a,35,3b,66,3e,66,3c,38,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    .line 83
    const-string v0, "58,46,52,58,5a,53,4c,47,55,55,58,5c,4a,46,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SENDER_UPPER_STR_SAMSUNGAPPS_WEB:Ljava/lang/String;

    .line 84
    const-string v0, "58,46,52,58,5a,53,4c,46,53,55,58,46,49,52,4e,55,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SENDER_UPPER_STR_SAMSUNGAPPS_ADMIN:Ljava/lang/String;

    .line 882
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 136
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    .line 137
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    .line 141
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/pushclient/c;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    .line 324
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->e:I

    .line 356
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/pushclient/e;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f:Landroid/content/BroadcastReceiver;

    .line 903
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Lcom/sec/android/app/samsungapps/pushclient/PushInterface;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    return-object v0
.end method

.method static synthetic a()Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    return-object v0
.end method

.method private a(Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1483
    const-string v1, ""

    .line 1484
    const-string v0, ""

    .line 1485
    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1533
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1534
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1535
    const-string v1, "extra.push.status"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1536
    const-string v0, "extra.push.err.code"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1539
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1541
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->e()V

    .line 1544
    :cond_0
    return-void

    .line 1491
    :pswitch_0
    const-string v1, "intent.action.push_connFailure"

    .line 1492
    const-string v0, "CONNECTIONFAIL"

    goto :goto_0

    .line 1500
    :pswitch_1
    const-string v1, "intent.action.push_reg"

    .line 1501
    const-string v0, "FAIL"

    goto :goto_0

    .line 1506
    :pswitch_2
    const-string v1, "intent.action.push_reg"

    .line 1507
    const-string v0, "TIMEOUT"

    goto :goto_0

    .line 1515
    :pswitch_3
    const-string v1, "intent.action.push_dereg"

    .line 1516
    const-string v0, "FAIL"

    goto :goto_0

    .line 1485
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushService$8;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/pushclient/PushService$8;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    const-string v1, "productDetail()"

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v3

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;->getProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/pushclient/j;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/j;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Z)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/pushclient/PushService;[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 43
    const-string v0, "registerDeeplinkNotification()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    aget-object v0, p1, v0

    aget-object v1, p1, v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eq v2, v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v3, :cond_1

    :cond_0
    const-string v0, "Message or Uri is null string"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v2, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;-><init>(Landroid/content/Context;)V

    iget v3, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->e:I

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1104
    const/4 v1, 0x0

    .line 1106
    monitor-enter p0

    .line 1108
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1110
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p1

    :goto_1
    move-object v1, v0

    .line 1112
    goto :goto_0

    .line 1116
    :cond_0
    if-eqz v1, :cond_1

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 1120
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1122
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c()V

    .line 1123
    return-void

    .line 1120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->writePushSvcOnOffState(Z)V

    .line 60
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->writePushRegID(Ljava/lang/String;)V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 993
    monitor-enter p0

    .line 995
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 997
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1001
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    goto :goto_0

    .line 1003
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1011
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d()Ljava/lang/String;

    move-result-object v0

    .line 1013
    if-eqz v0, :cond_0

    .line 1015
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {v1, v2, v3, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1021
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1028
    :goto_0
    return-void

    .line 1025
    :cond_0
    const-string v0, "### BgDownloadService STOP ###"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1026
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->e()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 7

    .prologue
    .line 43
    const-string v0, "registerWebOTAService()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lcom/sec/android/app/samsungapps/pushclient/g;

    invoke-direct {v6, p0}, Lcom/sec/android/app/samsungapps/pushclient/g;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->registerWebOTAService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/samsungapps/Main;->contentNotificationManager:Lcom/sec/android/app/samsungapps/ContentNotificationManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/ContentNotificationManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/ContentNotificationManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/Main;->contentNotificationManager:Lcom/sec/android/app/samsungapps/ContentNotificationManager;

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushService$4;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/pushclient/PushService$4;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    new-instance v2, Lcom/sec/android/app/samsungapps/pushclient/k;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/k;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;->enqueue(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceLockEventReceiver;)V

    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1035
    monitor-enter p0

    .line 1037
    const/4 v0, 0x0

    .line 1039
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1044
    :cond_0
    check-cast v0, Ljava/lang/String;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1045
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 3

    .prologue
    .line 43
    const-string v0, "registerPushNotiDevice()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/pushclient/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/pushclient/h;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->registerPushNotiDevice(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1561
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1563
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->stopService(Landroid/content/Intent;)Z

    .line 1565
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Z
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->registration(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1638
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    if-eqz v0, :cond_0

    .line 1640
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->getRegId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1643
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getPushRegID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/pushclient/PushService;)Z
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->deregistration(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->e()V

    return-void
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 3

    .prologue
    .line 43
    const-string v0, "deregisterPushNotiDevice()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getPushRegID()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Z)V

    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_DEREG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "SKIP"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    const-string v0, "regID : null"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/pushclient/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/pushclient/i;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->deregisterPushNotiDevice(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method public static isUsablePushService(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 716
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 720
    :try_start_0
    const-string v2, "com.sec.spp.push"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 728
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 724
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c()V

    return-void
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 4

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d()Ljava/lang/String;

    move-result-object v1

    monitor-enter p0

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 665
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 545
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 548
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 549
    const-string v0, "onCreate"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 553
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    .line 554
    monitor-enter p0

    .line 555
    :try_start_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    .line 556
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 573
    new-instance v0, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    .line 578
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 579
    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 580
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->bind()Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    .line 587
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_CONNECTION_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v1, "S"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    .line 590
    :cond_0
    return-void

    .line 556
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 585
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 674
    const-string v0, "onDestroy()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->b:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->unbind()Z

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 679
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 680
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 595
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 596
    sput-object p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->context:Landroid/content/Context;

    .line 597
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    .line 599
    if-eqz p1, :cond_2

    .line 602
    const-string v0, "extra_command"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    monitor-enter p0

    .line 605
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onStartCommand() CMD : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",CNT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->c:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 608
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->isUsablePushService(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 621
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->SPP_CONNECTION_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    const-string v2, "N"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    .line 629
    :cond_0
    const-string v1, "cmd_registration"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v5, :cond_3

    .line 631
    const-string v0, "startPushService()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-static {v0, v5, v3, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 657
    :cond_1
    :goto_0
    return v5

    .line 608
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 612
    :cond_2
    const-string v0, "onStartCommand() aIntent = is null"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 637
    :cond_3
    const-string v1, "cmd_deregistration"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v5, :cond_4

    .line 639
    const-string v0, "endPushService()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-static {v0, v4, v3, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 644
    :cond_4
    const-string v1, "cmd_downloading"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v5, :cond_7

    .line 646
    const-string v0, "startDownload()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extra_product_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "extra_guid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_version"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    if-eqz v1, :cond_6

    if-eqz v2, :cond_6

    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->compareVersion(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->leftlower:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    if-eq v2, v3, :cond_5

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->same:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    if-ne v2, v3, :cond_6

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startDownload Fail>> Exist Package : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/app/samsungapps/pushclient/b;->c(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/pushclient/f;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/f;-><init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto/16 :goto_0

    .line 651
    :cond_7
    const-string v1, "cmd_deeplink"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 653
    const-string v0, "startDeeplinkNoti()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extra_noti_msg"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "extra_deeplink_uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_8

    if-nez v1, :cond_9

    :cond_8
    const-string v0, "notiMsg or uri is null"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v3

    aput-object v1, v2, v5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-static {v0, v1, v3, v3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
