.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 10
    const-string v3, ""

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getCategoryGetter()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 13
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getCategoryGetter()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;->getSelectedCategory()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 14
    const-string v0, "categoryProductList2Notc"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->setReqName(Ljava/lang/String;)V

    .line 15
    const-string v0, "2030"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->setReqID(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0, p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addCategoryParams(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V

    .line 20
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 23
    const-string v3, ""

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 25
    const-string v0, "categoryProductList2Notc"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->setReqName(Ljava/lang/String;)V

    .line 26
    const-string v0, "2030"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->setReqID(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0, p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addCategoryParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected addCategoryParams(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 34
    const-string v0, "categoryID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getCategoryGetter()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;->getSelectedCategory()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v0, "categoryName"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getCategoryGetter()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;->getSelectedCategory()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v0, "alignOrder"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 38
    const-string v0, "status"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getPaidTypeFilter()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method protected addCategoryParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 44
    const-string v0, "categoryID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;->getCategoryID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v0, "categoryName"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;->getCategoryName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v0, "alignOrder"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;->getAlignOrderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 48
    const-string v0, "status"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ICategoryProductListParam;->getPaidTypeFilter()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method
