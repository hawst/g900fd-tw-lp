.class public Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 72
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    :pswitch_0
    return-void

    .line 77
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)Z
    .locals 2

    .prologue
    .line 45
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 48
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 51
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 59
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 48
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 56
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
