.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 10
    const-string v2, "joinRegister"

    const-string v3, "5050"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    const-string v0, "emailID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 13
    const-string v0, "password"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 14
    const-string v0, "rePassword"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 15
    const-string v0, "birthYear"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getBirthYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 16
    const-string v0, "birthMonth"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getBirthMonth()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 17
    const-string v0, "birthDay"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getBirthDay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 18
    const-string v0, "cardType"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 19
    const-string v0, "cardNum"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 20
    const-string v0, "expirationYear"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 21
    const-string v0, "expirationMonth"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 22
    const-string v0, "cvs"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 23
    const-string v1, "emailCheck"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getNewsLetterReceive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 24
    const-string v1, "userID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getUserID()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 25
    const-string v0, "authNID"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 26
    const-string v0, "authPIN"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 27
    return-void

    .line 23
    :cond_0
    const-string v0, "N"

    goto :goto_0

    .line 24
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getUserID()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 30
    const-string v2, "joinRegister"

    const-string v3, "5050"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 32
    const-string v0, "emailID"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->emailID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 33
    const-string v0, "password"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->password:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 34
    const-string v0, "rePassword"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->password:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 35
    const-string v0, "birthYear"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 36
    const-string v0, "birthMonth"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 37
    const-string v0, "birthDay"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 38
    const-string v0, "cardType"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 39
    const-string v0, "cardNum"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 40
    const-string v0, "expirationYear"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 41
    const-string v0, "expirationMonth"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 42
    const-string v0, "cvs"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 43
    const-string v1, "emailCheck"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->newsLetterReceive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 44
    const-string v1, "userID"

    iget-object v0, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 45
    const-string v0, "authNID"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 46
    const-string v0, "authPIN"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 47
    return-void

    .line 43
    :cond_0
    const-string v0, "N"

    goto :goto_0

    .line 44
    :cond_1
    iget-object v0, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    goto :goto_1
.end method
