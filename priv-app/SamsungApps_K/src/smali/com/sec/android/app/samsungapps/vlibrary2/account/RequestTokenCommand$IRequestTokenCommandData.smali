.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract createCancellableLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;
.end method

.method public abstract getNewInterfaceViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public abstract getToken()Ljava/lang/String;
.end method

.method public abstract getValidationViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public abstract isAutoLogin()Z
.end method

.method public abstract isHideNotificationSamsungAccountInstalled()Z
.end method

.method public abstract isNewInterfaceSamsungAccountInstalled()Z
.end method

.method public abstract isTokenExpired()Z
.end method

.method public abstract setTokenResult(Ljava/lang/String;Ljava/lang/String;)V
.end method
