.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 15
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;

    .line 23
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 104
    const-string v0, "AlipayPaymentStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 134
    :goto_0
    :pswitch_0
    return-void

    .line 107
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 111
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->SEND_FAIL_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 115
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 119
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 126
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 129
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)Z
    .locals 3

    .prologue
    .line 30
    const-string v0, "AlipayPaymentStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 31
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 99
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 33
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 36
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 47
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 55
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 58
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 69
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 80
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    .line 91
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 94
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->COMPLETING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch

    .line 33
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 44
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 55
    :pswitch_data_3
    .packed-switch 0x5
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 66
    :pswitch_data_4
    .packed-switch 0x7
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 77
    :pswitch_data_5
    .packed-switch 0x9
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 88
    :pswitch_data_6
    .packed-switch 0xb
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 138
    const-string v0, "AlipayPaymentStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 139
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 156
    :goto_0
    :pswitch_0
    return-void

    .line 143
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 150
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 155
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
