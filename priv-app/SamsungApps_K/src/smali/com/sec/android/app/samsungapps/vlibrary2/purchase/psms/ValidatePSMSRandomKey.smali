.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IConfirmPasswordUI:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;

.field private _IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

.field mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->mHandler:Landroid/os/Handler;

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;Z)V
    .locals 0

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->onFinalResult(Z)V

    return-void
.end method

.method private invokeUI()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;->getPSMSRandomkeyViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 43
    return-void
.end method


# virtual methods
.method public checkRandomKey(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;->getRandomKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 49
    if-ne v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IConfirmPasswordUI:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;->onConfirmPassworCompleted(Z)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/p;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/p;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IConfirmPasswordUI:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;->onEnteredWrongPassword()V

    goto :goto_0
.end method

.method protected existRandomkey()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;->existRandomKey()Z

    move-result v0

    return v0
.end method

.method public getFormattedSellingPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;->getFormattedSellingPrice()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;->getProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRandomKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IValidatePSMSRandomKey:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;->getRandomKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->existRandomkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->invokeUI()V

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->onFinalResult(Z)V

    goto :goto_0
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->_IConfirmPasswordUI:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;

    .line 38
    return-void
.end method
