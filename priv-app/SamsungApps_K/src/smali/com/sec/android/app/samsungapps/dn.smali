.class final Lcom/sec/android/app/samsungapps/dn;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/dn;->a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 83
    if-eqz p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/dn;->a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/dn;->a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->c:Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;->getGiftCardList(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/dn;->a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->updateData()V

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/dn;->a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->setVisibleRetry(I)Z

    goto :goto_0
.end method
