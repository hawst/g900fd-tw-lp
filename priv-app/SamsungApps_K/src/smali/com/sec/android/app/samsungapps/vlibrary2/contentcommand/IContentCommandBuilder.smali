.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
.end method

.method public abstract getContentSize()I
.end method

.method public abstract getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getGUID()Ljava/lang/String;
.end method

.method public abstract getProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getWishListID()Ljava/lang/String;
.end method

.method public abstract hasLoadType()Z
.end method

.method public abstract hasOrderID()Z
.end method

.method public abstract hasProductID()Z
.end method

.method public abstract isAddedWishItem()Z
.end method

.method public abstract isInstalledItem()Z
.end method

.method public abstract isPrePostType()Z
.end method

.method public abstract isPurchasedContentType()Z
.end method

.method public abstract isPurchasedDetailType()Z
.end method

.method public abstract isStoreType()Z
.end method

.method public abstract requestGUIDProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestGUIDProductDetailExCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestGUIDProductDetailMainCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestProductDetailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract setAddedWishItem()V
.end method

.method public abstract setDeletedWishItem()V
.end method

.method public abstract setLikeState(ZI)V
.end method

.method public abstract updateDetailAndAskAlreadyPurchased()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract updateDetailViewAfterGetDetail()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract validateCompatibleOS()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract validateDetail()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
