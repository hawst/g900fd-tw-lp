.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;


# instance fields
.field mResponseRequiredEventQueue:Ljava/util/ArrayList;

.field mSystemEventHandlers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

.field private mSystemEventQueue:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventQueue:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventHandlers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    .line 25
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    return-object v0
.end method


# virtual methods
.method public addSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventQueue:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->add(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 180
    return-void
.end method

.method public addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventHandlers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V
    .locals 1

    .prologue
    .line 219
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 220
    return-void
.end method

.method protected callQueueHandlerIfQueueNotEmpty()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getFirstResponseRequiredEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v3

    .line 81
    if-nez v3, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventHandlers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;

    .line 87
    invoke-interface {v0, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 93
    :goto_1
    if-nez v0, :cond_0

    .line 96
    const-string v0, "no one handle the event"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->failed()Z

    .line 98
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->processed()Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public checkAppUpgradeUpdated()V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;->CheckAppUpgradeUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 124
    return-void
.end method

.method public exitSamsungApps()V
    .locals 2

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 129
    return-void
.end method

.method public exitSamsungApps(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 133
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 134
    return-void
.end method

.method public findSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventQueue:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->findEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v0

    return-object v0
.end method

.method public flexibleTabChanged()V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;->FlexibleTabChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 120
    return-void
.end method

.method public getAccountEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;-><init>()V

    return-object v0
.end method

.method public getFirstResponseRequiredEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    .line 108
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInicisEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisEventManager;
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisEventManager;-><init>()V

    return-object v0
.end method

.method protected getSystemEventQueue()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventQueue:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    return-object v0
.end method

.method public getYesOrNoEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEventManager;
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEventManager;-><init>()V

    return-object v0
.end method

.method public inicisPayInitSuccess(Z)V
    .locals 2

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 142
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->INITSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->INITFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    goto :goto_0
.end method

.method public migrationCompleted()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->MigrationCompleted:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 52
    return-void
.end method

.method public notifyAlreadyInstalledAppForBatchDownload(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 210
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AlreadyInstalledBatchApp:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 211
    return-void
.end method

.method protected notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventHandlers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;

    .line 58
    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    :cond_1
    return-void
.end method

.method public notifyCommentChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 229
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CommentChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 230
    return-void
.end method

.method public notifyContentDetailUpdated(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 2

    .prologue
    .line 234
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ContentDetailUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 235
    return-void
.end method

.method public notifyCountryDecided()V
    .locals 2

    .prologue
    .line 239
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CountryDecided:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 240
    return-void
.end method

.method public notifyDownloaderStateChanged()V
    .locals 2

    .prologue
    .line 206
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DownloaderStateChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 207
    return-void
.end method

.method public notifyEnterPreCheckingForDownload(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 197
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotifyEnterPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 198
    return-void
.end method

.method public notifyFailedPreCheckingForDownload(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 202
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NotifyFailedPreCheckingForDownload:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 203
    return-void
.end method

.method public notifyGetUpdatableAppCount(I)V
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;->UpdatableAppListCountUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;I)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 138
    return-void
.end method

.method public notifyLimitedStore()V
    .locals 2

    .prologue
    .line 244
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->DispLimitedStore:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 245
    return-void
.end method

.method public notifyNameAgeVerified()V
    .locals 2

    .prologue
    .line 258
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->REAL_AGE_NAME_VERIFIED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 259
    return-void
.end method

.method public notifyNoAvailablePurchaseMethod()V
    .locals 2

    .prologue
    .line 214
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->NoAvailablePurchaseEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 215
    return-void
.end method

.method protected notifyResponseRequiredEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 68
    const/4 v0, 0x1

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->callQueueHandlerIfQueueNotEmpty()V

    .line 74
    :cond_1
    return-void
.end method

.method public notifySearchGesturePad(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 254
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SearchGesturePad:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 255
    return-void
.end method

.method public notifyStoreTypeChanged()V
    .locals 2

    .prologue
    .line 249
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->StoreTypeChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 250
    return-void
.end method

.method public notifySuggestAllShareContent()V
    .locals 2

    .prologue
    .line 224
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->SuggestAllShareContent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 225
    return-void
.end method

.method public openScreenShot(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;)V
    .locals 2

    .prologue
    .line 174
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 175
    return-void
.end method

.method public openWishList()V
    .locals 2

    .prologue
    .line 193
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->OpenWishList:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 194
    return-void
.end method

.method public removeSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventQueue:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->remove(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z

    move-result v0

    return v0
.end method

.method public removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V
    .locals 1

    .prologue
    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mSystemEventHandlers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    return-void
.end method

.method protected requestProcessed(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->mResponseRequiredEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->callQueueHandlerIfQueueNotEmpty()V

    .line 115
    return v0
.end method

.method public startLoadingDialog()V
    .locals 2

    .prologue
    .line 164
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->StartLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 165
    return-void
.end method

.method public stopLoadingDialog()V
    .locals 2

    .prologue
    .line 169
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->EndLoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 170
    return-void
.end method
