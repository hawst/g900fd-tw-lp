.class final Lcom/sec/android/app/samsungapps/initializer/i;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/app/samsungapps/initializer/Global;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/initializer/Global;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/i;->b:Lcom/sec/android/app/samsungapps/initializer/Global;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/initializer/i;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final setForeground()V
    .locals 3

    .prologue
    .line 279
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/i;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/AppsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 281
    return-void
.end method

.method public final unsetForeground()V
    .locals 3

    .prologue
    .line 273
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/i;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/AppsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 275
    return-void
.end method
