.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 102
    const-string v0, ""

    .line 104
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;->getCommentText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRatingValue()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;->getRatingValue()I

    move-result v0

    goto :goto_0
.end method
