.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CEHCK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_18AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_COMPATIBLE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_ENOUGH_SPACE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_MULTI_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_NETWORK_SIZE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_PERMISSIN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum CHECK_REALNAME_VERIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field public static final enum TURKEY_3GCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "LOGINCHECK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "TURKEY_3GCHECK"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->TURKEY_3GCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CEHCK_DETAIL"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CEHCK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_MULTI_DOWNLOAD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_MULTI_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_ENOUGH_SPACE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_ENOUGH_SPACE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_NETWORK_SIZE_LIMIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_NETWORK_SIZE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_18AGE_LIMIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_18AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_COMPATIBLE_OS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_COMPATIBLE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_REALNAME_VERIFICATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_REALNAME_VERIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "CHECK_PERMISSIN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_PERMISSIN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    const-string v1, "SUCCESS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 4
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->LOGINCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->TURKEY_3GCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CEHCK_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_MULTI_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_ENOUGH_SPACE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_NETWORK_SIZE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_18AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_COMPATIBLE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_REALNAME_VERIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->CHECK_PERMISSIN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    return-object v0
.end method
