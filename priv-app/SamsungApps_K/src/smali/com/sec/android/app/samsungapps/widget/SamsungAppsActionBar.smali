.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;
.super Landroid/widget/LinearLayout;
.source "ProGuard"


# static fields
.field public static final ACTION_BAR_APPS_FOR_GALAXY_TYPE:I = 0xb0004

.field public static final ACTION_BAR_NOAPPS_TYPE:I = 0xb0003

.field public static final ACTION_BAR_NORMAL_TYPE:I = 0xb0001

.field public static final ACTION_BAR_SELECTION_TYPE:I = 0xb0002

.field public static final ACTION_ITEM_CANCELALL_TYPE:I = 0xa0011

.field public static final ACTION_ITEM_CANCEL_TYPE:I = 0xa000c

.field public static final ACTION_ITEM_CATEGORY_TYPE:I = 0xa0007

.field public static final ACTION_ITEM_CLEAR:I = 0xa0012

.field public static final ACTION_ITEM_DELETE_TYPE:I = 0xa000e

.field public static final ACTION_ITEM_DOWNLOAD_TYPE:I = 0xa0006

.field public static final ACTION_ITEM_INSTALLALL_TYPE:I = 0xa0017

.field public static final ACTION_ITEM_LONG_DELETE_TYPE:I = 0xa0018

.field public static final ACTION_ITEM_NEGATIVE_TYPE:I = 0xa000a

.field public static final ACTION_ITEM_OPTION_TYPE:I = 0xa0008

.field public static final ACTION_ITEM_POSITIVE_TYPE:I = 0xa0009

.field public static final ACTION_ITEM_REMOVE_TYPE:I = 0xa0010

.field public static final ACTION_ITEM_SAVE_TYPE:I = 0xa000d

.field public static final ACTION_ITEM_SEARCHBAR_TYPE:I = 0xa000b

.field public static final ACTION_ITEM_SEARCH_TYPE:I = 0xa0005

.field public static final ACTION_ITEM_SEND_TYPE:I = 0xa0014

.field public static final ACTION_ITEM_SHARE_TYPE:I = 0xa0013

.field public static final ACTION_ITEM_SUBTITLE_TYPE:I = 0xa0004

.field public static final ACTION_ITEM_UPDATEALL_TYPE:I = 0xa000f

.field public static final ACTION_ITEM_WISH_TYPE:I = 0xa0015

.field public static final ACTION_ITEM_WISH_TYPE_ON:I = 0xa0016

.field public static final NAVI_ITEM_BACK_TYPE:I = 0xa0000

.field public static final NAVI_ITEM_ICON_TYPE:I = 0xa0001

.field public static final NAVI_ITEM_TITLE_TYPE:I = 0xa0002


# instance fields
.field private final a:I

.field private b:Landroid/widget/Toast;

.field private c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/view/ViewGroup;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/LinearLayout;

.field private j:Ljava/util/HashMap;

.field private k:Ljava/util/HashMap;

.field private l:Ljava/util/HashMap;

.field private m:Landroid/widget/TextView;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 186
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a:I

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b:Landroid/widget/Toast;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->c:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->n:Z

    .line 188
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/content/Context;)V

    .line 189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a:I

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b:Landroid/widget/Toast;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->c:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->n:Z

    .line 176
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/content/Context;)V

    .line 177
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    return-object v0
.end method

.method private a()Ljava/lang/Boolean;
    .locals 10

    .prologue
    const v9, 0xa0001

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 673
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 676
    if-nez v0, :cond_8

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    const/high16 v2, 0xa0000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v2, v0

    .line 681
    :goto_0
    if-nez v2, :cond_0

    move-object v0, v1

    .line 714
    :goto_1
    return-object v0

    .line 686
    :cond_0
    if-eqz v2, :cond_7

    .line 688
    const v0, 0x7f0c0298

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 689
    const v0, 0x7f0c029a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 690
    const v0, 0x7f0c0299

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 691
    if-eqz v3, :cond_1

    if-eqz v4, :cond_1

    if-nez v5, :cond_2

    .line 693
    :cond_1
    const-string v0, "_resizePadding::Can\'t getting view"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move-object v0, v1

    .line 694
    goto :goto_1

    .line 697
    :cond_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v7, :cond_4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 699
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_5

    .line 700
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 701
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 702
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 709
    :goto_3
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa000c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa000d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa0014

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 711
    :cond_3
    :goto_4
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 697
    :cond_4
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    .line 704
    :cond_5
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 705
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 706
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 709
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v7, :cond_3

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v3, -0x1

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_4

    :cond_7
    move-object v0, v1

    .line 714
    goto/16 :goto_1

    :cond_8
    move-object v2, v0

    goto/16 :goto_0
.end method

.method private a(I)Ljava/lang/Boolean;
    .locals 6

    .prologue
    const v5, 0xa0001

    const/high16 v4, 0xa0000

    const/4 v3, 0x1

    .line 1030
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1032
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1034
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAppsActionBar::_addAction::Already contained type, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 1071
    :goto_0
    return-object v0

    .line 1038
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAllViews()V

    .line 1039
    const v0, 0xa000b

    if-ne p1, v0, :cond_1

    .line 1040
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400c2

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b()V

    .line 1060
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 1062
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 1063
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;

    .line 1071
    :cond_2
    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1065
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 1067
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 1068
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private a(III)Ljava/lang/Boolean;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 1084
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-nez v0, :cond_2

    .line 1089
    :cond_0
    const-string v0, "SamsungAppsActionBar::_addAction::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 1090
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1232
    :cond_1
    :goto_0
    return-object v0

    .line 1093
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v9, :cond_3

    .line 1095
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAppsActionBar::_addAction::Already contained type, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move-object v0, v4

    .line 1096
    goto :goto_0

    .line 1099
    :cond_3
    const v0, 0xa000c

    if-eq p3, v0, :cond_4

    const v0, 0xa000d

    if-eq p3, v0, :cond_4

    const v0, 0xa0014

    if-ne p3, v0, :cond_6

    .line 1103
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v7, v7, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400bf

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1105
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v7, v7, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v3, v0

    .line 1113
    :goto_1
    const v0, 0x7f0c0055

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1115
    const v1, 0x7f0c029e

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1116
    if-eq p1, v7, :cond_7

    .line 1118
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1119
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1120
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1131
    :goto_2
    const v2, 0x7f0c0099

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1132
    if-eq p2, v7, :cond_9

    .line 1134
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1135
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v5, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1136
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1137
    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1142
    const v5, 0xa000c

    if-eq p3, v5, :cond_5

    const v5, 0xa000d

    if-eq p3, v5, :cond_5

    const v5, 0xa0014

    if-ne p3, v5, :cond_8

    .line 1143
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const/16 v6, 0x13

    invoke-static {v5, v2, v6}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->textSizeChangedLimit(Landroid/content/Context;Landroid/widget/TextView;I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v9, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1153
    :goto_3
    const v2, 0x7f0c004f

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1158
    if-eqz v2, :cond_b

    .line 1160
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1161
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-lez v5, :cond_a

    .line 1162
    const v5, 0xa000e

    if-eq p3, v5, :cond_a

    .line 1163
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1170
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1173
    invoke-direct {p0, v0, p3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(Landroid/view/View;I)V

    .line 1174
    invoke-direct {p0, v2, p3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/view/View;I)V

    .line 1176
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1177
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/x;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/x;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1205
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/y;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/y;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    move-object v0, v4

    .line 1228
    :goto_5
    if-ne p1, v7, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-le v1, v9, :cond_1

    .line 1229
    invoke-virtual {p0, v11, v11}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1107
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400be

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_1

    .line 1124
    :cond_7
    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1125
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1145
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->textSizeChanged(Landroid/content/Context;Landroid/widget/TextView;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v9, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_3

    .line 1150
    :cond_9
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 1168
    :cond_a
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 1225
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAppsActionBar::_addAction::Can\'t get view, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    move-object v0, v4

    goto :goto_5
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    .line 201
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400bd

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    .line 203
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    .line 204
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    .line 205
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c029b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b()V

    .line 211
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 213
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 215
    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ":Constructor::Can\'t found root view on View, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;I)V
    .locals 4

    .prologue
    const v1, 0x7f0802a1

    const/4 v3, 0x1

    .line 1394
    const-string v0, ""

    .line 1396
    packed-switch p2, :pswitch_data_0

    .line 1467
    :goto_0
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0800d6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1468
    return-void

    .line 1399
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0802f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1403
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f080141

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1408
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0802f7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1412
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/BadgeNotification;->getOldBadgeCount()I

    move-result v0

    .line 1414
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1416
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0801ea

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1420
    :cond_1
    if-le v0, v3, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f08029e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f080299

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1426
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0802f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1431
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f08023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1435
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0802f0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1439
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0800d4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1443
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1447
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1451
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0802e9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1455
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0801e0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1459
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f0802f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1463
    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v1, 0x7f08011a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1396
    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_e
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_a
        :pswitch_c
        :pswitch_0
        :pswitch_2
        :pswitch_d
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 3

    .prologue
    const v2, 0x7f080219

    .line 1853
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1858
    :goto_0
    return-void

    .line 1856
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method private b(I)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1707
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1709
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 1711
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1712
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1714
    const v2, 0xa0002

    if-ne p1, v2, :cond_0

    .line 1716
    const v2, 0x7f0c0038

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1717
    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1724
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 1731
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a()Ljava/lang/Boolean;

    .line 1733
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1735
    return-object v0

    .line 1721
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1728
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "_removeNavi::Not contained type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2007
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2008
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2009
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 2010
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    .line 2011
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 2012
    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 2025
    :cond_0
    :goto_0
    return-void

    .line 2015
    :pswitch_0
    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2016
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2020
    :pswitch_1
    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2021
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2012
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Landroid/view/View;I)V
    .locals 6

    .prologue
    const v5, 0x7f0802a1

    const/4 v4, 0x1

    .line 1477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 1479
    const-string v0, ""

    .line 1480
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const/4 v3, -0x1

    invoke-direct {v1, v2, v4, v3}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;-><init>(Landroid/content/Context;II)V

    .line 1481
    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->setViewToBeHovered(Landroid/view/View;)V

    .line 1483
    packed-switch p2, :pswitch_data_0

    .line 1553
    :goto_0
    :pswitch_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->showToolTip(Ljava/lang/String;)V

    .line 1555
    :cond_0
    return-void

    .line 1486
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0802f1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1490
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f080141

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1495
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0802f7

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1499
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/BadgeNotification;->getOldBadgeCount()I

    move-result v0

    .line 1501
    if-lez v0, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1503
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0801ea

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1507
    :cond_2
    if-le v0, v4, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v3, 0x7f08029e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f080299

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1513
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0802f5

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1518
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1522
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0802f0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1526
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0800d4

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1530
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1534
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1538
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0802e9

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1542
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0801e0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1546
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f0802f2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1550
    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f08011a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1483
    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_e
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_a
        :pswitch_c
        :pswitch_0
        :pswitch_2
        :pswitch_d
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    return-object v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 1984
    const/4 v0, 0x0

    .line 1985
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 1986
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0038

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1988
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1996
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1999
    :goto_0
    return-void

    .line 1990
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 1993
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const v2, 0x7f090135

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 1988
    :pswitch_data_0
    .packed-switch 0xb0003
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    return-object v0
.end method


# virtual methods
.method public _setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;
    .locals 8

    .prologue
    const v7, 0xa0001

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v0, 0xa0000

    .line 437
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 439
    const/4 v2, -0x1

    if-ne p3, v2, :cond_0

    move p3, v0

    .line 447
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 449
    invoke-direct {p0, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(I)Ljava/lang/Boolean;

    .line 450
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(I)Ljava/lang/Boolean;

    .line 451
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 537
    :goto_0
    return-object v0

    .line 454
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    if-nez v2, :cond_3

    .line 456
    :cond_2
    const-string v0, "SamsungAppsActionBar::_setIconVisible::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 457
    goto :goto_0

    .line 463
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v3, 0x7f0c0090

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 465
    if-eqz v2, :cond_8

    .line 470
    if-nez p2, :cond_4

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 510
    :goto_1
    if-ne p3, v0, :cond_7

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0038

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 512
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 513
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 525
    :goto_2
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 526
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a()Ljava/lang/Boolean;

    goto :goto_0

    .line 478
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 481
    if-ne p3, v0, :cond_5

    .line 482
    invoke-virtual {v2, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 483
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/w;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/w;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 505
    :cond_5
    invoke-virtual {v2, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 506
    invoke-virtual {v2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 515
    :cond_6
    invoke-direct {p0, v2, v0, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_2

    .line 519
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 534
    :cond_8
    const-string v0, "Constructor::Can\'t get view, 655362"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0
.end method

.method public addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 759
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    if-nez v1, :cond_1

    .line 762
    const-string v1, "SamsungAppsActionBar::addAction::Document.getInstanc().getConfig is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 925
    :cond_0
    :goto_0
    return-object v0

    .line 771
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v1

    if-ne v1, v7, :cond_5

    .line 774
    sparse-switch p1, :sswitch_data_0

    .line 800
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addAction::Not supported type, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 913
    :cond_2
    :goto_1
    :pswitch_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v7, :cond_0

    .line 915
    if-nez p2, :cond_11

    .line 917
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 777
    :sswitch_0
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 778
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v2, v3, p2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 779
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setParentView(Landroid/view/View;)V

    .line 782
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 783
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setHardKey(Z)V

    move-object v0, v1

    goto :goto_1

    .line 785
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setHardKey(Z)V

    .line 786
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->n:Z

    if-eqz v0, :cond_4

    const v0, 0x7f020060

    :goto_2
    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_4
    const v0, 0x7f02005e

    goto :goto_2

    .line 790
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 791
    const v0, 0x7f020095

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 795
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 796
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 806
    :cond_5
    packed-switch p1, :pswitch_data_0

    .line 905
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addAction::Not supported type, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 809
    :pswitch_2
    const v0, 0x7f020095

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 813
    :pswitch_3
    const v0, 0x7f020068

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 817
    :pswitch_4
    const v0, 0x7f020096

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 820
    :pswitch_5
    const v0, 0x7f02006e

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 843
    :pswitch_6
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 844
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v2, v3, p2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setParentView(Landroid/view/View;)V

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setHardKey(Z)V

    move-object v0, v1

    goto/16 :goto_1

    .line 850
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->setHardKey(Z)V

    .line 851
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->n:Z

    if-eqz v0, :cond_7

    const v0, 0x7f020060

    :goto_3
    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    const v0, 0x7f02005e

    goto :goto_3

    .line 856
    :pswitch_7
    const v0, 0x7f0802f0

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 860
    :pswitch_8
    const v0, 0x7f08023d

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 864
    :pswitch_9
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 868
    :pswitch_a
    const v0, 0x7f08023d

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 872
    :pswitch_b
    const v0, 0x7f0802f0

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 876
    :pswitch_c
    const v0, 0x7f0200a9

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 880
    :pswitch_d
    const v0, 0x7f0800d4

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 885
    :pswitch_e
    const v0, 0x7f0200a9

    invoke-direct {p0, v0, v6, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 889
    :pswitch_f
    const v0, 0x7f0802e9

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 893
    :pswitch_10
    const-string v5, "Install all"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-nez v0, :cond_9

    :cond_8
    const-string v0, "SamsungAppsActionBar::_addAction::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v7, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAppsActionBar::_addAction::Already contained type, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move-object v0, v4

    goto/16 :goto_1

    :cond_a
    const v0, 0xa000c

    if-eq p1, v0, :cond_b

    const v0, 0xa000d

    if-ne p1, v0, :cond_d

    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v6, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400bf

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v6, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v3, v0

    :goto_4
    const v0, 0x7f0c0055

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0c029e

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f0c0099

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    const v5, 0xa000c

    if-eq p1, v5, :cond_c

    const v5, 0xa000d

    if-eq p1, v5, :cond_c

    const v5, 0xa0014

    if-ne p1, v5, :cond_e

    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    const/16 v6, 0x13

    invoke-static {v5, v2, v6}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->textSizeChangedLimit(Landroid/content/Context;Landroid/widget/TextView;I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    :goto_5
    const v2, 0x7f0c004f

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_10

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_f

    const v4, 0xa000e

    if-eq p1, v4, :cond_f

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(Landroid/view/View;I)V

    invoke-direct {p0, v5, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/view/View;I)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/z;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/z;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/aa;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/aa;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    move-object v0, v2

    :goto_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-le v1, v7, :cond_2

    invoke-virtual {p0, v10, v10}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400be

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_4

    :cond_e
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->textSizeChanged(Landroid/content/Context;Landroid/widget/TextView;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_5

    :cond_f
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_6

    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAppsActionBar::_addAction::Can\'t get view, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    move-object v0, v4

    goto :goto_7

    .line 897
    :pswitch_11
    const v0, 0x7f0801e0

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 901
    :pswitch_12
    const v0, 0x7f0802f2

    invoke-direct {p0, v6, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 921
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 774
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0005 -> :sswitch_1
        0xa0008 -> :sswitch_0
        0xa000b -> :sswitch_2
    .end sparse-switch

    .line 806
    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_d
        :pswitch_f
        :pswitch_c
        :pswitch_11
        :pswitch_1
        :pswitch_3
        :pswitch_12
        :pswitch_4
        :pswitch_5
        :pswitch_10
        :pswitch_e
    .end packed-switch
.end method

.method public addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 938
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 940
    packed-switch p1, :pswitch_data_0

    .line 951
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addAction::Not supported type, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 958
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 960
    if-nez p2, :cond_1

    .line 962
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    :cond_0
    :goto_1
    return-object v0

    .line 943
    :pswitch_0
    invoke-direct {p0, v1, p3, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 947
    :pswitch_1
    invoke-direct {p0, v1, p3, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(III)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 966
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 940
    :pswitch_data_0
    .packed-switch 0xa0009
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public changeActionBarBG(I)V
    .locals 4

    .prologue
    const v3, 0x7f020058

    .line 1945
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 1976
    :cond_0
    :goto_0
    return-void

    .line 1948
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1949
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f0c0298

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1951
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1952
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1954
    :pswitch_0
    const v2, 0x7f0200fd

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1955
    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 1958
    :pswitch_1
    const v2, 0x7f020028

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1959
    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 1962
    :pswitch_2
    const v2, 0x7f020029

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1963
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->c(I)V

    .line 1964
    const v0, 0x7f020059

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 1967
    :pswitch_3
    const v1, 0x7f020027

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1968
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->c(I)V

    .line 1969
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0299

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1970
    if-eqz v0, :cond_0

    .line 1971
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1952
    :pswitch_data_0
    .packed-switch 0xb0001
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public destroy()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 229
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 231
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    .line 232
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    .line 233
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->f:Landroid/view/LayoutInflater;

    .line 234
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    .line 235
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->c:Ljava/lang/String;

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 242
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    if-eqz v1, :cond_2

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 252
    :cond_2
    return-object v0
.end method

.method public doHomeKeyOptionMenu()V
    .locals 2

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1670
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->dismiss()Z

    .line 1672
    :cond_0
    return-void
.end method

.method public doNonHardKeyOptionMenu(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1632
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;

    .line 1633
    if-nez v0, :cond_0

    .line 1635
    const-string v0, "doNonHardKeyOptionMenu::Can\'t getting listener"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 1665
    :goto_0
    return-void

    .line 1639
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->isShowing()Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1641
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->dismiss()Z

    goto :goto_0

    .line 1645
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasMenu()Z

    move-result v1

    if-ne v1, v2, :cond_3

    .line 1647
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 1649
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->show()Z

    .line 1664
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->notifyDataChanged()V

    goto :goto_0

    .line 1654
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 1656
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->create()Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 1659
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->show()Z

    goto :goto_1
.end method

.method public enableActionItem(IZ)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    const v4, 0xa0010

    .line 1613
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 1628
    :cond_0
    :goto_0
    return-void

    .line 1617
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1618
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1619
    if-ne p1, v4, :cond_4

    .line 1620
    if-ne p1, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v1, 0xa0008

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    if-ne p1, v4, :cond_0

    const v1, 0x7f0c029e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    .line 1622
    :cond_4
    if-eqz v0, :cond_0

    .line 1623
    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 1624
    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method public getActionItemsCounts()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNaviItemsCounts()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getOptionMenu()Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;
    .locals 1

    .prologue
    .line 2032
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    return-object v0
.end method

.method public getToolTip()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b:Landroid/widget/Toast;

    return-object v0
.end method

.method public getUpdateCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->c:Ljava/lang/String;

    return-object v0
.end method

.method public hideActionBar()V
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 560
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1779
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1780
    const v0, 0x7f0c0036

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1781
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1782
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1784
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 1797
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    if-eqz v0, :cond_0

    .line 1798
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->onOptionConfiguration(I)V

    .line 1800
    :cond_0
    return-void

    .line 1787
    :pswitch_0
    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1788
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1792
    :pswitch_1
    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1793
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1784
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onWindowStatusChangedListener()V
    .locals 1

    .prologue
    .line 1769
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    if-eqz v0, :cond_0

    .line 1770
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1771
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->dismiss()Z

    .line 1774
    :cond_0
    return-void
.end method

.method public removeAction(I)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 980
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 982
    packed-switch p1, :pswitch_data_0

    .line 1007
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeAction::Not supported type, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 1014
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1016
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1019
    :cond_0
    return-object v0

    .line 1003
    :pswitch_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "_removeAction::Not contained type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 982
    nop

    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public removeAll()Ljava/lang/Boolean;
    .locals 5

    .prologue
    .line 723
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 726
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 727
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 728
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v1

    .line 729
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 731
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 732
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAction(I)Ljava/lang/Boolean;

    move-result-object v1

    .line 733
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 735
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "removeAll::Can\'t remove item, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    .line 737
    goto :goto_0

    .line 739
    :cond_1
    return-object v0
.end method

.method public setIconNextFocusDownid(I)V
    .locals 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0090

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {v0, p1}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 547
    :cond_0
    return-void
.end method

.method public setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 425
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->_setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->_setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public setListener(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 308
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    return-object v0
.end method

.method public setMenIconWhite(Z)V
    .locals 0

    .prologue
    .line 748
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->n:Z

    .line 749
    return-void
.end method

.method public setNegativeText(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const v2, 0xa000a

    .line 1589
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1593
    :cond_0
    const-string v0, "SamsungAppsActionBar::setNegativeText::Not existed negative button"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 1594
    const/4 v0, 0x0

    .line 1604
    :goto_0
    return v0

    .line 1597
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1598
    const v1, 0x7f0c0099

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1599
    if-eqz v0, :cond_2

    .line 1601
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1604
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setNextFocusItems(I)V
    .locals 2

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 1804
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1805
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1807
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1808
    invoke-virtual {v0, p1}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_0

    .line 1811
    :cond_0
    const v0, 0x7f0c0090

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1812
    if-eqz v0, :cond_1

    .line 1813
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    .line 1816
    :cond_1
    const v0, 0x7f0c008e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1817
    if-eqz v0, :cond_2

    .line 1818
    invoke-virtual {v0, p1}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 1820
    :cond_2
    return-void
.end method

.method public setPositiveText(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const v2, 0xa0009

    .line 1564
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1568
    :cond_0
    const-string v0, "SamsungAppsActionBar::setPositiveText::Not existed positive button"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 1569
    const/4 v0, 0x0

    .line 1579
    :goto_0
    return v0

    .line 1572
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1573
    const v1, 0x7f0c0099

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1574
    if-eqz v0, :cond_2

    .line 1576
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1579
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSelectedText(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0038

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1833
    if-eqz v0, :cond_0

    .line 1834
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1836
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f0c0090

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1837
    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1838
    invoke-direct {p0, v1, v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1841
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;
    .locals 8

    .prologue
    const v7, 0xa0002

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 323
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 324
    const/4 v0, 0x0

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_8

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f0c004c

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 332
    :goto_0
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v6, :cond_2

    .line 334
    :cond_0
    invoke-direct {p0, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b(I)Ljava/lang/Boolean;

    .line 335
    if-eqz v2, :cond_1

    .line 337
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 339
    :cond_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 403
    :goto_1
    return-object v0

    .line 342
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    if-nez v0, :cond_4

    .line 344
    :cond_3
    const-string v0, "SamsungAppsActionBar::setTitle::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 345
    goto :goto_1

    .line 351
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v3, 0x7f0c0038

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 352
    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    if-eqz p1, :cond_7

    .line 355
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 360
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 361
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 363
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    if-nez p2, :cond_6

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    invoke-virtual {v2, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 376
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v6, :cond_5

    .line 377
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->m:Landroid/widget/TextView;

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->m:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/v;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/v;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 396
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a()Ljava/lang/Boolean;

    move-object v0, v1

    goto :goto_1

    .line 373
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->l:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 400
    :cond_7
    const-string v0, "Constructor::Can\'t get view, 655362"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_8
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public setToolTip(Landroid/widget/Toast;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->b:Landroid/widget/Toast;

    .line 262
    return-void
.end method

.method public showActionBar()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 568
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->g:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 572
    :cond_0
    const-string v1, "showActionBar::Not already obejcts"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 602
    :goto_0
    return-object v0

    .line 576
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->a()Ljava/lang/Boolean;

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 582
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 583
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 586
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 592
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 593
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 594
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 596
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 597
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 600
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 602
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public showNavi(Z)V
    .locals 2

    .prologue
    .line 1927
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 1937
    :cond_0
    :goto_0
    return-void

    .line 1931
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0297

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1933
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->i:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 1936
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public showOptionItem(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1870
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa0010

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1872
    if-nez v0, :cond_0

    .line 1873
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1876
    :cond_0
    if-nez v0, :cond_5

    .line 1877
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa0018

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v3, v0

    .line 1880
    :goto_0
    if-eqz v3, :cond_1

    .line 1881
    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1884
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1885
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->j:Ljava/util/HashMap;

    const v3, 0xa0008

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1887
    if-eqz v0, :cond_2

    .line 1888
    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1891
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 1881
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1888
    goto :goto_2

    :cond_5
    move-object v3, v0

    goto :goto_0
.end method
