.class public Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMapWithEndField;
.super Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;
.source "ProGuard"


# instance fields
.field private _bEnd:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;-><init>(Ljava/lang/Class;)V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMapWithEndField;->_bEnd:Z

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMap;-><init>(Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectContainer;)V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMapWithEndField;->_bEnd:Z

    .line 13
    return-void
.end method


# virtual methods
.method public isComplete()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMapWithEndField;->_bEnd:Z

    return v0
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 17
    const-string v0, "endOfList"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ObjectGenByMapWithEndField;->_bEnd:Z

    .line 21
    :cond_0
    return-void
.end method
