.class public Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Object;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field private h:I

.field private i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field private j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field private k:Landroid/widget/GridView;

.field private l:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

.field private m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

.field private n:Landroid/widget/LinearLayout;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->c:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->d:Ljava/lang/String;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->l:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    .line 142
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/c;-><init>(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    .line 62
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 63
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->c:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->d:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    .line 66
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->f:Ljava/lang/Object;

    .line 67
    iput p6, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    .line 68
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 432
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    sparse-switch v0, :sswitch_data_0

    .line 454
    :goto_0
    return-object p1

    .line 434
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setPaidTypeFilter(I)V

    .line 435
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    .line 436
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_ALL:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_0

    .line 439
    :sswitch_1
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setPaidTypeFilter(I)V

    .line 440
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    .line 441
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_PAID:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_0

    .line 444
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setPaidTypeFilter(I)V

    .line 445
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    .line 446
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_FREE:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_0

    .line 449
    :sswitch_3
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setPaidTypeFilter(I)V

    .line 450
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->recent:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    .line 451
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_NEW:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_0

    .line 432
    :sswitch_data_0
    .sparse-switch
        0x7f08029b -> :sswitch_2
        0x7f08029c -> :sswitch_3
        0x7f08029d -> :sswitch_1
        0x7f0802b3 -> :sswitch_0
    .end sparse-switch
.end method

.method private a()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->l:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    if-eqz v0, :cond_4

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->f:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->f:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->f:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const-string v0, ""

    .line 207
    :goto_0
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->checkActionbarTitleEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 214
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->l:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;->setOnActionBarConfiguration(Ljava/lang/String;)V

    .line 219
    :cond_4
    return-void

    .line 205
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;)V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "descriptionText"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "titleString"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/d;-><init>(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 291
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    const v1, 0x7f0802b3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    const v1, 0x7f0802f5

    if-eq v0, v1, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    const v0, 0x7f0c02fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    const v1, 0x7f0c02fb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setOnTextSingleLineChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 305
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v0

    .line 307
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 311
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLines(I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLineCount(I)V

    goto :goto_0

    .line 317
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLines(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLineCount(I)V

    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static newInstance(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;
    .locals 7

    .prologue
    .line 72
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object v0
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 349
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/e;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 356
    :cond_0
    :goto_0
    return v2

    .line 351
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 5

    .prologue
    .line 327
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 328
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 331
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 339
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 115
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 117
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_2

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 122
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eq v0, v1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getIndexOfFirstItem()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->b()V

    .line 129
    :cond_1
    return-void

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    .line 80
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const v2, 0x7f0400e6

    const/4 v3, 0x2

    .line 85
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setMainViewAndEmptyView(I)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createProductSetList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->createBannerProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->a()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->showDescription()V

    .line 88
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 89
    return-object v1

    .line 87
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setMainView(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->CURATED_PRODUCTLIST:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createCuratedProductList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->createBannerProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createContentCategoryList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_3

    :cond_7
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_3

    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_ALL:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_4

    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_PAID:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_4

    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_FREE:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_4

    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->BUTTON_NEW:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setButtonType(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->k:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x7f08029b -> :sswitch_2
        0x7f08029c -> :sswitch_3
        0x7f08029d -> :sswitch_1
        0x7f0802b3 -> :sswitch_0
    .end sparse-switch
.end method

.method public onDataLoadCompleted()V
    .locals 2

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->showDescription()V

    .line 362
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->a()V

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setOnToBeLog(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 369
    :cond_0
    return-void
.end method

.method public onDataLoadingMore()V
    .locals 0

    .prologue
    .line 392
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->i:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    .line 108
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->clearSortOrder()V

    .line 110
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->onDestroy()V

    .line 111
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 96
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/ContentListFragment;->onDestroyView()V

    .line 97
    return-void
.end method

.method public setOnActionBarConfigurationListener(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->l:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    .line 396
    return-void
.end method

.method public showDescription()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 223
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    const v1, 0x7f0802b3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->h:I

    const v1, 0x7f0802f5

    if-eq v0, v1, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->j:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getContentListQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    .line 229
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 236
    :cond_2
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 238
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryDescription:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 239
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryDescription:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    .line 248
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->b()V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 256
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->m:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 241
    :cond_5
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listDescription:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 242
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listDescription:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->e:Ljava/lang/String;

    goto :goto_1
.end method
