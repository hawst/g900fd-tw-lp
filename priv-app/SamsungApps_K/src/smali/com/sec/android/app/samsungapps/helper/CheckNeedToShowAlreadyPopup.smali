.class public Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 18
    return-void
.end method


# virtual methods
.method protected matchCondition()Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    return v0
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->_Context:Landroid/content/Context;

    const v2, 0x7f08032f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->_Context:Landroid/content/Context;

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->_Context:Landroid/content/Context;

    const v4, 0x7f08023d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/f;-><init>(Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/CheckNeedToShowAlreadyPopup;->invokeCompleted()V

    .line 31
    return-void
.end method
