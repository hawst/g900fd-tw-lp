.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mResultList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListParser;->mResultList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getResult()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListParser;->mResultList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BundleProduct;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BundleProduct;-><init>()V

    .line 19
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/BundleProduct;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 20
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method
