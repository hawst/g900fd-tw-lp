.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract hideProgressBar()V
.end method

.method public abstract notifyProgress(JJ)V
.end method

.method public abstract notifyProgressIndeterminated()V
.end method

.method public abstract notifyProgressText(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;)V
.end method

.method public abstract setDeleteButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;)V
.end method

.method public abstract setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V
.end method

.method public abstract showProgressBar()V
.end method
