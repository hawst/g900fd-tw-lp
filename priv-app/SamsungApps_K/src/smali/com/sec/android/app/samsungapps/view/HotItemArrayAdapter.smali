.class public Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    .line 17
    return-void
.end method


# virtual methods
.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 9

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/HotListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/HotItemArrayAdapter;->mListType:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewholder/HotListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    return-object v0
.end method
