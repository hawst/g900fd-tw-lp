.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field final ID_REQUEST_ACCESSTOKEN:I

.field private mAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

.field private mBroadCastResult:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

.field private mISaService:Lcom/msc/sa/aidl/ISAService;

.field private mRegistrationCode:Ljava/lang/String;

.field private mSaCallback:Lcom/msc/sa/aidl/ISACallback$Stub;

.field private mServiceManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mServiceManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    .line 31
    const v0, 0x8698

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->ID_REQUEST_ACCESSTOKEN:I

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mBroadCastResult:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    .line 135
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mSaCallback:Lcom/msc/sa/aidl/ISACallback$Stub;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    .line 36
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mBroadCastResult:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    return-object p1
.end method


# virtual methods
.method public getResultData()Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mBroadCastResult:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/x;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->_Context:Landroid/content/Context;

    const-string v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/x;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mServiceManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mServiceManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/y;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/y;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->checkServiceConnection(Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager$IServiceBinderResult;)V

    .line 62
    return-void
.end method

.method onFail()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mServiceManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->release()Z

    .line 123
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFinalResult(Z)V

    .line 124
    return-void
.end method

.method onSuccess()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mServiceManager:Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/servicebindmanager/ServiceConnectionManager;->release()Z

    .line 128
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFinalResult(Z)V

    .line 129
    return-void
.end method

.method requestToken()V
    .locals 5

    .prologue
    .line 67
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 68
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getMyPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 70
    if-nez v0, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFail()V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mISaService:Lcom/msc/sa/aidl/ISAService;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mSaCallback:Lcom/msc/sa/aidl/ISACallback$Stub;

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mRegistrationCode:Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->_Context:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 84
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 86
    array-length v0, v0

    if-lez v0, :cond_3

    .line 88
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "api_server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "auth_server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "cc"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "user_id"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "birthday"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "email_id"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "mcc"

    aput-object v3, v1, v2

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->isTokenExpired()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;->getToken()Ljava/lang/String;

    move-result-object v2

    .line 94
    if-eqz v2, :cond_2

    .line 95
    const-string v3, "expired_access_token"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_2
    const-string v2, "additional"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mISaService:Lcom/msc/sa/aidl/ISAService;

    const v2, 0x8698

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    .line 107
    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFail()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFail()V

    goto/16 :goto_0

    .line 111
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFail()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
