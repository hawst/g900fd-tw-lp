.class public Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

.field b:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

.field private f:Landroid/content/Context;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;)V
    .locals 10

    .prologue
    .line 41
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 43
    check-cast p9, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    .line 44
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    .line 45
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->f:Landroid/content/Context;

    .line 46
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    .line 47
    return-void
.end method

.method static synthetic A(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Position:I

    return v0
.end method

.method private a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-nez v1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->findDownloadingContent(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 361
    const/4 v0, 0x1

    .line 364
    :cond_0
    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 371
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isKNOXApp()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isExecutable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    .line 398
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 386
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 387
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isExecutable(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 389
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 394
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Position:I

    return v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Position:I

    return v0
.end method

.method static synthetic u(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic v(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic z(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected cancelDl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->cancelDl(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->isDeleteMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;->updateList(Z)V

    .line 99
    :cond_0
    return-void
.end method

.method protected createDownloadHelperFactory(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createNoAccountBasedDownloadHelperFactoryForDemo(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method protected handleNotDownloading()V
    .locals 2

    .prologue
    .line 527
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleNotDownloading()V

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 532
    :cond_0
    return-void
.end method

.method public onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 73
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/view/downloadable/f;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 82
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    goto :goto_0

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;->updateList(Z)V

    goto :goto_1

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected prepareView()V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showProductType()V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showPurchaseStatus()V

    .line 53
    return-void
.end method

.method public processDateFormat(Landroid/content/Context;Ljava/lang/String;ZZZ)Ljava/lang/String;
    .locals 12

    .prologue
    .line 402
    const-string v1, ""

    .line 403
    const-string v3, ""

    .line 407
    const-string v2, ""

    .line 409
    const/4 v0, 0x0

    .line 413
    const/4 v4, 0x1

    .line 416
    if-nez p2, :cond_1

    move-object v0, v1

    .line 522
    :cond_0
    :goto_0
    return-object v0

    .line 425
    :cond_1
    const/16 v1, 0x2d

    :try_start_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 426
    const/16 v1, 0x2d

    add-int/lit8 v5, v6, 0x1

    invoke-virtual {p2, v1, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    .line 427
    const/16 v1, 0x2d

    add-int/lit8 v5, v7, 0x1

    invoke-virtual {p2, v1, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 429
    if-eqz v1, :cond_2

    const/4 v5, -0x1

    if-ne v1, v5, :cond_8

    .line 431
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 439
    :cond_3
    const/4 v4, 0x0

    move v5, v1

    move v1, v0

    move v0, v4

    .line 443
    :goto_1
    if-eqz v0, :cond_c

    .line 445
    const/16 v0, 0x2d

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 447
    if-eqz v0, :cond_4

    const/4 v2, -0x1

    if-ne v0, v2, :cond_5

    .line 449
    :cond_4
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 452
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {p2, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 453
    add-int/lit8 v2, v6, 0x1

    invoke-virtual {p2, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 454
    add-int/lit8 v2, v7, 0x1

    invoke-virtual {p2, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 455
    add-int/lit8 v2, v5, 0x1

    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 456
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 458
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 459
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 460
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 461
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 462
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 464
    new-instance v0, Ljava/util/Date;

    add-int/lit16 v1, v1, -0x76c

    add-int/lit8 v2, v2, -0x1

    invoke-direct/range {v0 .. v5}, Ljava/util/Date;-><init>(IIIII)V

    .line 466
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 467
    if-eqz p5, :cond_9

    .line 469
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 470
    invoke-virtual {v0, v1, v2}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v3, v0

    add-long/2addr v1, v3

    .line 472
    invoke-static {p1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 473
    if-eqz p4, :cond_6

    .line 475
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    move-object v1, v6

    move-object v2, v7

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    .line 511
    :goto_2
    if-eqz v0, :cond_7

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 513
    :cond_7
    const-string v0, "%s/%s/%s%s%s"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v3, v6, v4

    const/4 v3, 0x2

    aput-object v5, v6, v3

    const/4 v3, 0x3

    aput-object v2, v6, v3

    const/4 v2, 0x4

    aput-object v1, v6, v2

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 436
    :cond_8
    const/16 v0, 0x2d

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p2, v0, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 437
    if-eqz v0, :cond_3

    const/4 v5, -0x1

    if-eq v0, v5, :cond_3

    move v5, v1

    move v1, v0

    move v0, v4

    goto/16 :goto_1

    .line 480
    :cond_9
    invoke-static {p1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 481
    if-eqz p4, :cond_a

    .line 483
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 486
    :cond_a
    if-eqz p3, :cond_b

    .line 488
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GMT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_b
    move-object v1, v6

    move-object v2, v7

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    .line 491
    goto :goto_2

    .line 494
    :cond_c
    const/4 v0, 0x0

    invoke-virtual {p2, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 495
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {p2, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 496
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {p2, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 498
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 499
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 500
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 502
    new-instance v8, Ljava/util/Date;

    add-int/lit16 v5, v5, -0x76c

    add-int/lit8 v6, v6, -0x1

    invoke-direct {v8, v5, v6, v7}, Ljava/util/Date;-><init>(III)V

    .line 503
    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    .line 504
    invoke-static {p1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 505
    if-eqz p3, :cond_d

    .line 507
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " GMT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    move-object v11, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v5

    move-object v5, v4

    move-object v4, v1

    move-object v1, v11

    goto/16 :goto_2

    .line 519
    :catch_0
    move-exception v0

    move-object v0, p2

    goto/16 :goto_0

    :cond_d
    move-object v11, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v5

    move-object v5, v4

    move-object v4, v1

    move-object v1, v11

    goto/16 :goto_2
.end method

.method public showCommonStatus()V
    .locals 6

    .prologue
    const v2, 0x7f0c0056

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->installedTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Position:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->purchasedDate:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->isDeleteMode()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->installedTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_7

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->progressLayout:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 130
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "1"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v4, :cond_6

    .line 134
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :goto_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_0

    .line 136
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->installedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 144
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly:Landroid/view/View;

    const v1, 0x7f0c0044

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusLeftId(I)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusRightId(I)V

    goto/16 :goto_0

    .line 154
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusable(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly:Landroid/view/View;

    const v1, 0x7f0c0154

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusRightId(I)V

    goto/16 :goto_0
.end method

.method public showPurchaseStatus()V
    .locals 10

    .prologue
    const v9, 0x7f0801c3

    const/4 v8, 0x1

    const v7, 0x7f08015a

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 163
    const-string v0, ""

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_View:Landroid/view/View;

    if-nez v1, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showCommonStatus()V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showProductName()V

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showProductImg()V

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showAdultIcon()V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->showCategory()V

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appSize:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appDescription:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getCuratedDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly:Landroid/view/View;

    new-instance v2, Lcom/sec/android/app/samsungapps/view/downloadable/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/downloadable/d;-><init>(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->isDeleteMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v1, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 208
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v1

    .line 209
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v1

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appSize:Landroid/widget/TextView;

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->a()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appSize:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->progressLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->progresstext:Landroid/widget/TextView;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1, v5}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->makeDownloadProgressText(Ljava/lang/Long;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/app/samsungapps/view/downloadable/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/downloadable/e;-><init>(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getCuratedDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->categoryName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getCuratedDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appSize:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 336
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v8, :cond_5

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_13

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 184
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->sellerName:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->sellerName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_8

    .line 185
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 187
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->categoryName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->sellerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 220
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v1

    if-nez v1, :cond_a

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->appSize:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->progressLayout:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v1, :cond_3

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    :cond_b
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->hasOrderID()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 235
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    const v1, 0x7f0801e4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 238
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 241
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->c()V

    goto/16 :goto_2

    .line 243
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 249
    :cond_f
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 250
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    const v1, 0x7f0801e4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 253
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->c()V

    goto/16 :goto_2

    .line 258
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 345
    :cond_13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method
