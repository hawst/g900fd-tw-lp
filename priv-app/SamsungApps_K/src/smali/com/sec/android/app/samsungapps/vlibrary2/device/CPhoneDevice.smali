.class public Lcom/sec/android/app/samsungapps/vlibrary2/device/CPhoneDevice;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/device/IPhoneDevice;


# instance fields
.field private _Context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/device/CPhoneDevice;->_Context:Landroid/content/Context;

    .line 12
    return-void
.end method

.method private isPhoneNumberReadable()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/device/CPhoneDevice;->_Context:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 47
    if-nez v0, :cond_0

    move v0, v1

    .line 57
    :goto_0
    return v0

    .line 52
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getPhoneNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/device/CPhoneDevice;->_Context:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 32
    if-nez v0, :cond_1

    .line 33
    const-string v0, ""

    .line 41
    :cond_0
    :goto_0
    return-object v0

    .line 36
    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 37
    if-nez v0, :cond_0

    .line 41
    const-string v0, ""

    goto :goto_0
.end method

.method public isPhoneAvailable()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/device/CPhoneDevice;->_Context:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 16
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 24
    :goto_0
    return v0

    .line 20
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/device/CPhoneDevice;->isPhoneNumberReadable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 24
    goto :goto_0
.end method
