.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mConfirmedTime:J

.field private mPaswordConfirmed:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mConfirmedTime:J

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mPaswordConfirmed:Z

    return-void
.end method


# virtual methods
.method public clearPasswordConfirmationTime()V
    .locals 2

    .prologue
    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mConfirmedTime:J

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mPaswordConfirmed:Z

    .line 21
    return-void
.end method

.method public getLastPasswordConfirmationTime()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mConfirmedTime:J

    return-wide v0
.end method

.method public isPasswordConfirmed()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mPaswordConfirmed:Z

    return v0
.end method

.method public setPasswordConfirmationTimeNow()V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mConfirmedTime:J

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->mPaswordConfirmed:Z

    .line 48
    return-void
.end method
