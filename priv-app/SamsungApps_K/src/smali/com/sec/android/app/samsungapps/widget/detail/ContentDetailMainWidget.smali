.class public Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

.field private d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

.field private e:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ProgressBar;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->n:Z

    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->n:Z

    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Landroid/content/Context;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->n:Z

    .line 90
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Landroid/content/Context;)V

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(DLjava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0c00b5

    const/4 v2, 0x0

    .line 281
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 282
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 283
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 284
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 286
    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 304
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 305
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 308
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    :goto_0
    if-eqz p4, :cond_0

    .line 314
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 317
    :cond_0
    const v1, 0x7f0c00c6

    if-ne p1, v1, :cond_1

    .line 318
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 319
    if-eqz v0, :cond_1

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 321
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/d;

    invoke-direct {v1, p0, v0, p3}, Lcom/sec/android/app/samsungapps/widget/detail/d;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 338
    :cond_1
    return-void

    .line 310
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    .line 95
    const v0, 0x7f04002e

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->initView(Landroid/content/Context;I)V

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const v0, 0x7f0c00ac

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    const v0, 0x7f0c0042

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    const v0, 0x7f0c0040

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    const v0, 0x7f0c0041

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    const v0, 0x7f0c0043

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setProgressStateNormal()V

    .line 100
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08015b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080276

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0, v5}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/detail/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/detail/h;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 684
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 685
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/detail/e;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;)V

    .line 721
    const v1, 0x7f0c00b1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 722
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->c()V

    .line 723
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->delete()V

    .line 724
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const v2, 0x7f0c00b4

    const v1, 0x7f0c00b2

    .line 382
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->n:Z

    if-eqz v0, :cond_0

    .line 383
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 384
    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 389
    :goto_0
    return-void

    .line 386
    :cond_0
    invoke-virtual {p0, v2, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 387
    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    goto :goto_0
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 598
    const/4 v0, 0x0

    .line 599
    const v2, 0x7f0c00ad

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const v2, 0x7f0c003d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 603
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 606
    :cond_3
    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 877
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 880
    return-void
.end method

.method private b(DLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const v3, 0x7f0c00b6

    .line 289
    cmpl-double v0, p1, v4

    if-nez v0, :cond_1

    .line 290
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f08029b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v0, v1, v6}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v0, v1, v6}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v0

    if-eqz v0, :cond_0

    cmpl-double v0, p1, v4

    if-lez v0, :cond_0

    .line 297
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    double-to-int v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "WON"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const v5, 0x7f0c00b4

    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 610
    if-nez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v3

    if-nez v3, :cond_3

    .line 612
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 641
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 610
    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isAlreadyPurchased()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 615
    :cond_5
    if-eqz p1, :cond_6

    .line 616
    const v0, 0x7f0c00ad

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 617
    const v0, 0x7f0c003d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 623
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 624
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->c()V

    goto :goto_2

    .line 626
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 630
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v1, :cond_8

    .line 633
    :cond_7
    const v0, 0x7f0c00ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 638
    :goto_3
    const v0, 0x7f0c003d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 639
    const v0, 0x7f0c00b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 636
    :cond_8
    const v0, 0x7f0c00ad

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1104
    const v0, 0x7f0c00b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1105
    const v1, 0x7f0c00b4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1106
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1111
    :cond_0
    :goto_0
    return-void

    .line 1109
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1110
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public buttonListEnable(Z)V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Z)V

    .line 350
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->likeButtonEnable(Z)V

    .line 353
    return-void
.end method

.method public buttonSetEnable(IZ)V
    .locals 1

    .prologue
    .line 341
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 345
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method public hideProgressBar()V
    .locals 2

    .prologue
    .line 829
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    :goto_0
    return-void

    .line 832
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setProgressStateNormal()V

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public isOpenGearManager()Z
    .locals 1

    .prologue
    .line 1162
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->o:Z

    return v0
.end method

.method public likeButtonEnable(Z)V
    .locals 5

    .prologue
    const v4, 0x7f0800e1

    const v1, 0x7f0c00ae

    .line 401
    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 402
    const v0, 0x7f0c00af

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 404
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 405
    if-nez v0, :cond_0

    .line 413
    :goto_0
    return-void

    .line 408
    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 411
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public likeToast()V
    .locals 3

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-nez v0, :cond_0

    .line 465
    :goto_0
    return-void

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->hasLike()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080224

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080201

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0
.end method

.method public likebuttonUpdate()V
    .locals 8

    .prologue
    const/16 v5, 0x270f

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    const v0, 0x7f0c00ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 420
    const v1, 0x7f0c00b0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 422
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVLikeCount()I

    move-result v2

    if-le v2, v5, :cond_2

    .line 429
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v1

    if-ne v1, v6, :cond_4

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->hasLike()Z

    move-result v1

    if-ne v1, v6, :cond_3

    .line 445
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    .line 434
    :cond_2
    const-string v2, "%d"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVLikeCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 447
    :cond_3
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto/16 :goto_0

    .line 450
    :cond_4
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto/16 :goto_0
.end method

.method public loadWidget()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;->onClickRetryBtn(I)V

    .line 144
    :cond_0
    return-void
.end method

.method public notifyProgress(JJ)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 838
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 842
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 843
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(Z)V

    .line 844
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 846
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    const-string v1, " / %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 851
    const-wide/16 v0, 0x64

    mul-long/2addr v0, p1

    :try_start_0
    div-long/2addr v0, p3

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 857
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 860
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 866
    :goto_2
    cmp-long v0, p1, p3

    if-nez v0, :cond_0

    .line 868
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->notifyProgressIndeterminated()V

    .line 869
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->notifyProgressText(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;)V

    .line 870
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    .line 871
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b()V

    goto :goto_0

    .line 852
    :catch_0
    move-exception v0

    .line 854
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailMainWidget::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 861
    :catch_1
    move-exception v0

    .line 863
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailMainWidget::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public notifyProgressIndeterminated()V
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 884
    return-void
.end method

.method public notifyProgressText(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1146
    sget-object v0, Lcom/sec/android/app/samsungapps/widget/detail/b;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1157
    :goto_0
    return-void

    .line 1149
    :pswitch_0
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 1150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f08027f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1154
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b()V

    goto :goto_0

    .line 1146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    if-nez v0, :cond_1

    .line 808
    :cond_0
    :goto_0
    return-void

    .line 775
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 807
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;->onClickCancelBtn()V

    goto :goto_0

    .line 779
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 784
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v1

    if-ne v1, v6, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->doIHaveDeletePackagePermission()Z

    move-result v0

    if-ne v0, v6, :cond_2

    .line 786
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVProductName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVGUID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08016a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080169

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v6}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v3, 0x7f0802ef

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/f;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/f;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v1, 0x7f08023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/detail/g;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto/16 :goto_0

    .line 791
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVGUID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 796
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;->onClickLikeBtn()V

    goto/16 :goto_0

    .line 801
    :sswitch_3
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonSetEnable(IZ)V

    .line 802
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;->onClickGetBtn()V

    goto/16 :goto_0

    .line 775
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c0044 -> :sswitch_0
        0x7f0c00ae -> :sswitch_2
        0x7f0c00b2 -> :sswitch_1
        0x7f0c00b4 -> :sswitch_3
    .end sparse-switch
.end method

.method public onProgresBarStateChanged(IJJ)V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v4, 0x0

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 675
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 649
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 651
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080296

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 657
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 658
    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    const-string v1, " / %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 664
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 673
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080273

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0

    .line 649
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public ratingUpdate()V
    .locals 8

    .prologue
    const/16 v3, 0x8

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    const v0, 0x7f0c00ca

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    .line 475
    if-eqz v0, :cond_0

    .line 479
    const v1, 0x7f0c00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 480
    const v2, 0x7f0c00cb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 482
    if-eqz v1, :cond_0

    .line 486
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 488
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVAverageRating()F

    move-result v2

    .line 489
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v7}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v7

    invoke-interface {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVRatingParticipants()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    invoke-virtual {v0, v2}, Landroid/widget/RatingBar;->setRating(F)V

    .line 491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "point"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVRatingParticipants()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 495
    :cond_2
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 496
    invoke-virtual {v0, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->likebuttonUpdate()V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->ratingUpdate()V

    .line 165
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 121
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    .line 122
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    .line 123
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    .line 124
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 125
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    .line 126
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    .line 127
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    .line 128
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    .line 129
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    .line 130
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 131
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    .line 132
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 133
    return-void
.end method

.method public setButtonTextSize()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x32

    const/16 v9, 0x8

    const/high16 v3, 0x40800000    # 4.0f

    .line 1032
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1033
    mul-float v1, v3, v0

    float-to-int v1, v1

    const/high16 v2, 0x41800000    # 16.0f

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int v5, v1, v2

    .line 1034
    mul-float/2addr v0, v3

    float-to-int v6, v0

    .line 1036
    const/4 v0, 0x2

    new-array v4, v0, [I

    .line 1037
    const v0, 0x7f0c00b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1038
    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1039
    const v0, 0x7f0c00b6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 1040
    const v0, 0x7f0c00b3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1042
    if-eqz v8, :cond_0

    if-eqz v7, :cond_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 1101
    :cond_0
    :goto_0
    return-void

    .line 1046
    :cond_1
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v9, :cond_0

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v9, :cond_0

    .line 1050
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v9, 0x7f08015a

    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v9, 0x7f0802f3

    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1055
    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/i;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/widget/detail/i;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;[IIILandroid/widget/TextView;)V

    invoke-virtual {v7, v0, v10, v11}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1078
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/j;

    move-object v1, p0

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/widget/detail/j;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;[IIILandroid/widget/TextView;)V

    invoke-virtual {v8, v0, v10, v11}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public setDeleteButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1125
    const v0, 0x7f0c00b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1127
    sget-object v1, Lcom/sec/android/app/samsungapps/widget/detail/b;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1143
    :goto_0
    return-void

    .line 1130
    :pswitch_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1131
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 1135
    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1136
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 1140
    :pswitch_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V
    .locals 9

    .prologue
    const v8, 0x7f0c00b7

    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x7f0c00b5

    const/16 v4, 0x8

    .line 887
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->o:Z

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return-void

    .line 891
    :cond_1
    const v0, 0x7f0c00b6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 892
    const v1, 0x7f0c00b1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 893
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isLinkApp()Z

    move-result v1

    .line 894
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->n:Z

    .line 896
    sget-object v2, Lcom/sec/android/app/samsungapps/widget/detail/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1024
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isAlreadyPurchased()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1025
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1027
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setButtonTextSize()V

    goto :goto_0

    .line 899
    :pswitch_1
    invoke-direct {p0, v7}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Z)V

    .line 900
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f0802e5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 903
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isDiscount()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 904
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    .line 905
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(DLjava/lang/String;)V

    .line 909
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 916
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVReducePrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto :goto_1

    .line 912
    :cond_3
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 920
    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isAlreadyPurchased()Z

    move-result v0

    if-ne v0, v7, :cond_5

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVReducePrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_1

    .line 928
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_1

    .line 936
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->showProgressBar()V

    goto/16 :goto_1

    .line 942
    :pswitch_4
    if-eqz v1, :cond_7

    .line 943
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 944
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 945
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 946
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isDiscount()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVReducePrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_1

    .line 951
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_1

    .line 956
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f0802e5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 961
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isDiscount()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 962
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_8

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(DLjava/lang/String;)V

    .line 967
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 974
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVReducePrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_1

    .line 970
    :cond_8
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 978
    :cond_9
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 980
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f08029b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 987
    :pswitch_6
    if-eqz v1, :cond_0

    .line 988
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 989
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isDiscount()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVReducePrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_0

    .line 995
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellingPrice()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(DLjava/lang/String;)V

    goto/16 :goto_0

    .line 1002
    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1003
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1004
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1007
    :pswitch_8
    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->n:Z

    .line 1008
    invoke-direct {p0, v7}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Z)V

    .line 1009
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1010
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1011
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1014
    :pswitch_9
    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->o:Z

    .line 1015
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080156

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1016
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1017
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1020
    :pswitch_a
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f0802f3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 896
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public setDetailButtonManager(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    .line 117
    return-void
.end method

.method public setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 104
    return-void
.end method

.method public setListener()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const v0, 0x7f0c00ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    :cond_0
    const v0, 0x7f0c00b2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    return-void
.end method

.method public setMainWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    .line 113
    return-void
.end method

.method public setProgressStateNormal()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 580
    :cond_0
    :goto_0
    return-void

    .line 573
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Z)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 579
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(Z)V

    goto :goto_0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 108
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    .line 109
    return-void
.end method

.method public showProgressBar()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 819
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    :goto_0
    return-void

    .line 822
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(Z)V

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 824
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080296

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateProgressBarState(JJ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(Z)V

    .line 540
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    if-lez v0, :cond_3

    .line 541
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->j:Landroid/widget/TextView;

    const-string v1, " / %s"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080274

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 545
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 546
    iput v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    .line 555
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 549
    :cond_2
    const-wide/16 v0, 0x64

    mul-long/2addr v0, p1

    :try_start_0
    div-long/2addr v0, p3

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->l:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 550
    :catch_0
    move-exception v0

    .line 552
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailMainWidget::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 558
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const v2, 0x7f080296

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto/16 :goto_0
.end method

.method public updateWidget()V
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    if-nez v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 152
    :cond_0
    const v0, 0x7f0c00c6

    :try_start_0
    const-string v3, ""

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVProductName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    const v0, 0x7f0c00c6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/a;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/a;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Landroid/widget/TextView;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isPrePostApp()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f0c00c8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->ratingUpdate()V

    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellerName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eq v4, v6, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->hasSellerID()Z

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v6, :cond_2

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/c;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/c;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Landroid/widget/TextView;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    :cond_2
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const-string v4, "isa_samsungapps_default"

    const-string v5, "drawable"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVProductImgUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    const-string v3, "widget"

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVContentType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    const v0, 0x7f0c00cc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0c00b9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->isLinkApp()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v2

    if-eqz v2, :cond_9

    const v2, 0x7f0800fb

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020086

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->likebuttonUpdate()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v6, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v6, :cond_b

    :cond_5
    const v0, 0x7f0c00ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c00c9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :cond_6
    :goto_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setListener()V

    .line 154
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setNeedLoad(Z)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailMainWidget::Error::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 152
    :cond_7
    const v0, 0x7f0c00c8

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c00c8

    const-string v3, ""

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVCategoryName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_8
    move v1, v2

    goto/16 :goto_2

    :cond_9
    const v2, 0x7f080164

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    :cond_a
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    const-string v3, "isa_detail_samsung_bg"

    const-string v4, "drawable"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-ne v0, v6, :cond_6

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->likeButtonEnable(Z)V
    :try_end_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_5
.end method
