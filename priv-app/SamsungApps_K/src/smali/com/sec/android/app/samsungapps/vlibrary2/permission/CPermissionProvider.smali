.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _HasPermissionExist:Z

.field private _PermissionInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;

.field private _PermissionString:Ljava/lang/String;

.field _Result:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_PermissionInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;

    .line 22
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 38
    const-string v0, "permission"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_PermissionString:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_PermissionInfoProvider:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_PermissionString:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfoProvider;->getGroupedPermissionInfoArray(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_Result:Ljava/util/HashMap;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_HasPermissionExist:Z

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_HasPermissionExist:Z

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "permission"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_PermissionString:Ljava/lang/String;

    .line 65
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPermissionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_PermissionString:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_Result:Ljava/util/HashMap;

    return-object v0
.end method

.method public hasPermissionExist()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionProvider;->_HasPermissionExist:Z

    return v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method
