.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;


# instance fields
.field private a:J

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->d:Landroid/content/Context;

    .line 29
    iput-wide p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->a:J

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-static {p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->b:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    .line 31
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onCancelCompleted()V

    .line 129
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onProgress(J)V

    .line 121
    :cond_0
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onWriteFailed()V

    .line 145
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->f:Z

    .line 151
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->close()V

    .line 50
    :cond_0
    return-void
.end method

.method public deleteFile()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->b:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->deleteFile()Z

    .line 42
    return-void
.end method

.method public download(Ljava/io/InputStream;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 66
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->f:Z

    .line 68
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    const/16 v1, 0x2000

    invoke-direct {v4, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 72
    const/16 v1, 0x2000

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 74
    const-wide/16 v1, 0x0

    move v3, v0

    .line 77
    :cond_0
    const/4 v6, 0x0

    const/16 v7, 0x2000

    invoke-virtual {v4, v5, v6, v7}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    invoke-virtual {v3, v6, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->writeBuffer(I[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    const-wide/16 v6, 0x1

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 84
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->getDownloadedSize()J

    move-result-wide v6

    long-to-int v3, v6

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-double v6, v6

    sub-double/2addr v6, v1

    const-wide v8, 0x408f400000000000L    # 1000.0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_1

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    long-to-double v1, v1

    .line 87
    int-to-long v6, v3

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->a(J)V

    .line 89
    :cond_1
    iget-boolean v6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->f:Z

    if-eqz v6, :cond_0

    .line 90
    const-string v1, "FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->a()V

    .line 111
    :goto_1
    return v0

    .line 96
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->f:Z

    if-eqz v1, :cond_3

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 110
    :catch_0
    move-exception v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->b()V

    goto :goto_1

    .line 101
    :cond_3
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->isDone()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 102
    int-to-long v1, v3

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->a(J)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onWriteCompleted()V

    .line 104
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 106
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->b()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->getDownloadedSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public open()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
    .locals 5

    .prologue
    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->b:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->a:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;J)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->open()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    move-result-object v0

    return-object v0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    .line 37
    return-void
.end method
