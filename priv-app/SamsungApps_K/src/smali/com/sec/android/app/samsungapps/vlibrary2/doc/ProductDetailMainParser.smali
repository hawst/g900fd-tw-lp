.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 16
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 64
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-string v2, "cyberCash"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 29
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    invoke-direct {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;-><init>(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->setCyberCashInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;)V

    .line 37
    :cond_0
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->prepaidCardList:Ljava/lang/String;

    .line 38
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 40
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    invoke-direct {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->setChinaPrepaidCardInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;)V

    .line 46
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->optBillList:Ljava/lang/String;

    .line 47
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 49
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;

    invoke-direct {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;->getCount()I

    move-result v1

    if-eqz v1, :cond_2

    .line 52
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->setOptBillInfoList(Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;)V

    .line 55
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v1, :cond_3

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setDetailMain(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V

    .line 59
    :cond_3
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method
