.class public Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final PURCHASED_MODE_DELETE:I = 0x13

.field public static final PURCHASED_MODE_LONG_DELETE:I = 0x14

.field public static final PURCHASED_MODE_NORMAL:I = 0x11

.field public static final PURCHASED_MODE_REMOVE:I = 0x12

.field public static final PURCHASED_TYPE_ALL:I = 0x2

.field public static final PURCHASED_TYPE_UPDATE:I = 0x1


# instance fields
.field a:I

.field b:Landroid/content/Context;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->a:I

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->b:Landroid/content/Context;

    .line 26
    const-string v0, "PurchasedWidgetHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->c:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->a:I

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->b:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->a:I

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->b:Landroid/content/Context;

    .line 41
    return-void
.end method

.method public getCommandType()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->a:I

    return v0
.end method

.method public setCommandType(I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->a:I

    .line 51
    return-void
.end method
