.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;


# instance fields
.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private _ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field private activeGiftCard:Ljava/util/ArrayList;

.field private expiredGiftCard:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 20
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V
    .locals 2

    .prologue
    .line 30
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->giftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-ne v0, v1, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 26
    return-void
.end method

.method public complete()V
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_1

    .line 69
    :cond_1
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->_ISelectableItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 48
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->expiredGiftCard:Ljava/util/ArrayList;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 53
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->activeGiftCard:Ljava/util/ArrayList;

    .line 54
    return-void
.end method
