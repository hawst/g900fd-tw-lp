.class final Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

.field final synthetic b:Z

.field final synthetic c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;Z)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    iput-boolean p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 3

    .prologue
    .line 383
    if-eqz p2, :cond_4

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->b:Z

    if-eqz v0, :cond_3

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->clearData()V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    # setter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$402(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    .line 396
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->getStartNumber()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->createFile()Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getResultXml()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->save(Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$600(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Ljava/io/File;Ljava/lang/String;)V

    .line 400
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->onReceiveSuccess()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$700(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V

    .line 406
    :goto_1
    return-void

    .line 390
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->getEndNumber()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->a:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)V

    goto :goto_0

    .line 404
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;->c:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->onReceiveFailed()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->access$800(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V

    goto :goto_1
.end method
