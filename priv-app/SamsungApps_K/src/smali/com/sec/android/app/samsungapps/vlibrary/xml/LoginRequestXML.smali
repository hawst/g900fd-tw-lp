.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 9
    const-string v2, "login"

    const-string v3, "5010"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, "emailID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 11
    const-string v0, "password"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 12
    const-string v0, "reqCardInfoYn"

    const-string v1, "N"

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 13
    const-string v0, "cardType"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "cardNum"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v0, "expirationYear"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const-string v0, "expirationMonth"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v0, "cvs"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v0, "authNID"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const-string v0, "authPIN"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 24
    const-string v2, "login"

    const-string v3, "5010"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 25
    const-string v0, "emailID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 26
    const-string v0, "password"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 27
    const-string v0, "reqCardInfoYn"

    const-string v1, "N"

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 28
    const-string v0, "cardType"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v0, "cardNum"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v0, "expirationYear"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v0, "expirationMonth"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v0, "cvs"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v0, "authNID"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v0, "authPIN"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-void
.end method
