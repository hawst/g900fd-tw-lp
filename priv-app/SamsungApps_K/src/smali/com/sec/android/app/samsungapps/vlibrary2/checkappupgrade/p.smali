.class final Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onForegroundInstalling()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public final onInstallFailed()V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public final onInstallFailed(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyInstallFailed(Ljava/lang/String;)V

    .line 177
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    .line 179
    return-void
.end method

.method public final onInstallSuccess()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->notifyInstallCompleted()V

    .line 162
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFinished(Ljava/lang/String;)Z

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;->exitSamsungApps()V

    .line 165
    return-void
.end method
