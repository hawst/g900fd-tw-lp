.class public Lcom/sec/android/app/samsungapps/view/CouponListView;
.super Lcom/sec/android/app/samsungapps/view/MyListView;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

.field private f:Lcom/sec/android/app/samsungapps/view/r;

.field private g:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->c:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponListView;->a(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->c:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponListView;->a(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->c:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponListView;->a(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setContext(Landroid/content/Context;)V

    .line 77
    new-instance v0, Lcom/sec/android/app/samsungapps/view/r;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/view/r;-><init>(Lcom/sec/android/app/samsungapps/view/CouponListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/CouponListView;)[Z
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->g:[Z

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/CouponListView;)Lcom/sec/android/app/samsungapps/view/r;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/CouponListView;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/r;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->g:[Z

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isCoupon()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/r;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->g:[Z

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;->showGiftCardNotiArea()V

    :cond_2
    :goto_1
    new-instance v0, Lcom/sec/android/app/samsungapps/view/o;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/o;-><init>(Lcom/sec/android/app/samsungapps/view/CouponListView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setNoDataEmptyView()V

    goto :goto_1
.end method


# virtual methods
.method public clearList()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/r;->clear()V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/r;->notifyDataSetChanged()V

    .line 221
    :cond_0
    return-void
.end method

.method protected empty()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 204
    .line 206
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->f:Lcom/sec/android/app/samsungapps/view/r;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/r;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 210
    :cond_0
    return v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    .line 242
    sget-object v1, Lcom/sec/android/app/samsungapps/view/q;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 249
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 245
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->refresh()V

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::mContext is empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;->release()V

    .line 166
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 168
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    .line 169
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListReceiver;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/CouponListRequestor;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupon/ICouponListReceiver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->c:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/view/p;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/p;-><init>(Lcom/sec/android/app/samsungapps/view/CouponListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setLoadingEmptyView()V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 228
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 229
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->release()V

    .line 230
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->c:Landroid/content/Context;

    .line 48
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CouponListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 53
    return-void
.end method
