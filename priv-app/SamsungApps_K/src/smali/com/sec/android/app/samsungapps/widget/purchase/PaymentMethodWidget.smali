.class public Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidget;


# instance fields
.field a:Landroid/widget/AdapterView$OnItemClickListener;

.field private b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/q;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    .line 41
    const v0, 0x7f040089

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->initView(Landroid/content/Context;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/q;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    .line 47
    const v0, 0x7f040089

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->initView(Landroid/content/Context;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/q;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    .line 53
    const v0, 0x7f040089

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->initView(Landroid/content/Context;I)V

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/widget/purchase/s;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->updateWidget()V

    .line 68
    return-void
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->updateWidget()V

    .line 63
    return-void
.end method

.method public refreshWidget()V
    .locals 1

    .prologue
    .line 125
    const-string v0, "PaymentMethodWidget::refreshWidget"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/s;->clear()V

    .line 77
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 84
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    .line 87
    return-void
.end method

.method public set(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;)V
    .locals 0

    .prologue
    .line 57
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    .line 59
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 130
    const-string v0, "PaymentMethodWidget::setWidgetData"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public updateWidget()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 91
    const v0, 0x7f0c01fd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    if-nez v0, :cond_1

    .line 94
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->setVisibility(I)V

    .line 121
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->size()I

    move-result v2

    move v0, v1

    .line 100
    :goto_1
    if-ge v0, v2, :cond_3

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;

    invoke-interface {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentMethodWidgetData;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    move-result-object v3

    .line 102
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 104
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 107
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setFocusableInTouchMode(Z)V

    .line 108
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/s;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/s;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->b:Lcom/sec/android/app/samsungapps/widget/purchase/s;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/p;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/p;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->post(Ljava/lang/Runnable;)Z

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f:Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsPurchaseListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method
