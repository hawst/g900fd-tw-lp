.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/FILERequestorCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createForOneTimeURL(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;

    invoke-direct {v0, p1, p3, p4, p5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 16
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    invoke-direct {v1, p1, p2, v0, p6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)V

    .line 17
    return-object v1
.end method

.method public createForStaticURL(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;
    .locals 3

    .prologue
    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;

    invoke-direct {v0, p1, p3, p4, p5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 9
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)V

    .line 10
    return-object v1
.end method

.method public createWithoutExpectedSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFILERequestor;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;

    invoke-direct {v0, p1, p3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 23
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IURLGetterForResumeDownload;)V

    .line 24
    return-object v1
.end method
