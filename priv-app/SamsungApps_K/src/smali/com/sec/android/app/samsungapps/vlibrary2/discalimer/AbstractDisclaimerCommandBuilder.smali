.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;


# static fields
.field private static _Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;


# instance fields
.field private _DataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

.field private _ExistNewDisclaimerVersion:Z

.field private _NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

.field private _NewDisclaimerVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_DataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;-><init>()V

    .line 26
    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->load(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V

    .line 28
    :cond_0
    return-void
.end method


# virtual methods
.method public askUserToAcceptDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;)V

    return-object v0
.end method

.method public clearDisclaimer()V
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 58
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->setNeedClearDisclaimer(Z)V

    .line 60
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_DataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->clear(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V

    .line 61
    return-void
.end method

.method public existNewDisclaimer()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_ExistNewDisclaimerVersion:Z

    return v0
.end method

.method public existNewDisclaimerVersion()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_ExistNewDisclaimerVersion:Z

    return v0
.end method

.method public getDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    return-object v0
.end method

.method public abstract getDisclaimerViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getNewDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    return-object v0
.end method

.method public getNewDisclaimerVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    .line 99
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimerVersion:Ljava/lang/String;

    goto :goto_0
.end method

.method public getOldDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    return-object v0
.end method

.method public isDiscalimerNeverAgreedByUser()Z
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->isAgreed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needDisclaimerClear()Z
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 66
    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->needClearDisclaimer()Z

    move-result v0

    goto :goto_0
.end method

.method public requestDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;)V

    return-object v0
.end method

.method public setNewDisclaimer(Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    .line 86
    return-void
.end method

.method public setNewDisclaimerVersion(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimerVersion:Ljava/lang/String;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_ExistNewDisclaimerVersion:Z

    .line 106
    return-void
.end method

.method public setUserAgreeToDisclaimer()V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->setAgree(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;Z)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_NewDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AbstractDisclaimerCommandBuilder;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    .line 117
    return-void
.end method

.method public validateDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;)V

    return-object v0
.end method
