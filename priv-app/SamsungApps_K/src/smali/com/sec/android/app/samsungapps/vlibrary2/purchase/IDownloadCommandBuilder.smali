.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadInfo;


# virtual methods
.method public abstract checkLoadTypeForDownloadEx()Z
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract doesContentHasOrderID()Z
.end method

.method public abstract getContentSize()J
.end method

.method public abstract getGUID()Ljava/lang/String;
.end method

.method public abstract getLoadType()Ljava/lang/String;
.end method

.method public abstract getNeedRetry()Z
.end method

.method public abstract getProductName()Ljava/lang/String;
.end method

.method public abstract getURLContainer()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;
.end method

.method public abstract isFreeContent()Z
.end method

.method public abstract paymentCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract setNeedRetry(Z)V
.end method
