.class public Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->a:Landroid/content/Context;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 21
    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 52
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 53
    const v1, 0x7357dcc5

    const-string v2, "com.sec.android.app.billing"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->checkHashcodeOfSignature(ILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 56
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isUPApkCondition()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPBillingCondition()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isUsingApkVersionUnifiedBilling()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingAPKVersionBilling()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPDisabledInPackageManager()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_2

    .line 74
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 63
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 67
    goto :goto_1

    .line 69
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    move v0, v1

    .line 74
    goto :goto_1

    .line 71
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_2
.end method

.method public isUPBillingCondition()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 25
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isUnifiedBillingSupported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 28
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 31
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 33
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method

.method public isUPDisabledInPackageManager()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 80
    const-string v2, "com.sec.android.app.billing"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 83
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 85
    :cond_0
    const/4 v0, 0x1

    .line 94
    :cond_1
    :goto_0
    return v0

    .line 89
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 91
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method
