.class final Lcom/sec/android/app/samsungapps/commands/bo;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/bo;->a:Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/bo;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/bo;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/bo;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/bo;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected final impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bo;->_Context:Landroid/content/Context;

    const v1, 0x7f0800d2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/bo;->_Context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bo;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0802ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/bp;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/bp;-><init>(Lcom/sec/android/app/samsungapps/commands/bo;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bo;->_Context:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/bq;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/bq;-><init>(Lcom/sec/android/app/samsungapps/commands/bo;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 107
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 108
    return-void
.end method
