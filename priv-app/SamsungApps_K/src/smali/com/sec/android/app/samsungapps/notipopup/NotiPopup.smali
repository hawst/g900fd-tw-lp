.class public Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    .line 59
    return-void
.end method

.method private a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 64
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CHINA_AUTO_UPD_3G_WARN_AGREE_CHECKED"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CHINA_AUTO_UPD_WIFI_WARN_AGREE_CHECKED"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 164
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 166
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_0

    const-string v1, "Y"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const/4 v0, 0x1

    .line 171
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 178
    const-string v1, "Y"

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 179
    return-void
.end method


# virtual methods
.method public askChinaDataWarningForBackgrounUpdate(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v0

    :goto_0
    if-nez v1, :cond_2

    .line 266
    :goto_1
    return-void

    .line 228
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 233
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v0, "CHINA_AUTO_UPD_3G_WARN_AGREE_CHECKED"

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    :cond_3
    :goto_2
    if-eqz v0, :cond_5

    .line 235
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;->onAgree()V

    goto :goto_1

    .line 233
    :cond_4
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "CHINA_AUTO_UPD_WIFI_WARN_AGREE_CHECKED"

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_2

    .line 239
    :cond_5
    const-string v0, ""

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 242
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080103

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 244
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    const v2, 0x7f080104

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08019b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/notipopup/i;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/i;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    invoke-static {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->showDlg(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupObserver;)V

    goto :goto_1
.end method

.method public askDeleteCreditCard(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
    .locals 6

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    new-instance v3, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0801ba

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/notipopup/g;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/g;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 127
    return-void
.end method

.method public askEmergencyUpdatePopup(Ljava/util/ArrayList;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V
    .locals 4

    .prologue
    .line 365
    const-string v1, "["

    .line 366
    const/4 v0, 0x1

    .line 367
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 369
    if-nez v2, :cond_0

    .line 371
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 374
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 376
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " needs emergency updates for security and fundamental error correction. Will you update?(Unless your device is connected to WIFI, the updates may result in additional charges according to your data plan.) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 381
    const-string v1, "Emergency Update"

    .line 383
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    new-instance v3, Lcom/sec/android/app/samsungapps/notipopup/m;

    invoke-direct {v3, p0, p2}, Lcom/sec/android/app/samsungapps/notipopup/m;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V

    invoke-static {v2, v1, v0, v3}, Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg;->showDlgWithoutCheckbox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/notipopup/ActivityPopupDlg$IActivityPopupOkCancelObserver;)V

    .line 395
    return-void
.end method

.method public askRegistrationOfCreditCardIsRequiredToPayTax(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
    .locals 5

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0802ae

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 138
    new-instance v4, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    invoke-direct {v4, v3, v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/notipopup/h;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/h;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 152
    return-void
.end method

.method public registerEmergencyUpdateNotification(I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    const v1, 0x7f0801ab

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 402
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/samsungapps/preloadupdate/EUpdateService;

    invoke-direct {v0, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-static {v2, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 405
    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;-><init>(Landroid/content/Context;)V

    .line 406
    const/4 v2, 0x0

    const-string v4, "Emergency Update"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 407
    return-void
.end method

.method public showAccountDisabled(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V
    .locals 3

    .prologue
    .line 271
    const v0, 0x7f080349

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 272
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 273
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;->onConfirm()V

    .line 274
    return-void
.end method

.method public showAlreadyPurchasedContent(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
    .locals 5

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08032f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08023d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/notipopup/e;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/e;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 83
    return-void
.end method

.method public showGeoIPFailed(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V
    .locals 3

    .prologue
    .line 279
    new-instance v1, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const v2, 0x7f0400d7

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;I)V

    .line 280
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0801da

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/notipopup/j;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/j;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 290
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v2, 0x7f0c0038

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 292
    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/notipopup/k;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/samsungapps/notipopup/k;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnConfigurationChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onConfigurationChangedListener;)V

    .line 321
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/notipopup/l;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/l;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 358
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 360
    return-void
.end method

.method public showIncompatibleOS(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V
    .locals 5

    .prologue
    .line 87
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1f41

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08023d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;->a()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/notipopup/f;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/notipopup/f;-><init>(Lcom/sec/android/app/samsungapps/notipopup/NotiPopup;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 98
    return-void
.end method
