.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOESNT_MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field public static final enum INVOKE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field public static final enum MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field public static final enum TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field public static final enum USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field public static final enum USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "MATCH_CONDITION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "DOESNT_MATCH_CONDITION"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->DOESNT_MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "INVOKE_COMPLETE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->INVOKE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "USER_AGREE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    const-string v1, "USER_DISAGREE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    .line 9
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->DOESNT_MATCH_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->INVOKE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Event;

    return-object v0
.end method
