.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# static fields
.field public static final SAC_INIT_VERSION_CODE:I = 0x2ee1

.field public static final SAC_PKGNAME:Ljava/lang/String; = "com.osp.app.signin"

.field public static final SAC_VERSION_1_30032:I = 0x1fbf0

.field public static final SAC_VERSION_1_3044:I = 0x32f4

.field public static final SAC_VERSION_1_40366:I = 0x2244e

.field public static final SAC_VERSION_1_50200:I = 0x24ab8


# instance fields
.field _SamsungAccountHandlerActivity:Ljava/lang/Class;

.field private _SamsungAccountNewHandler:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_SamsungAccountHandlerActivity:Ljava/lang/Class;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_SamsungAccountNewHandler:Ljava/lang/Class;

    .line 51
    return-void
.end method

.method public static isDisabledSamsungAccount(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 208
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 213
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 214
    :cond_0
    const/4 v0, 0x1

    .line 217
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExistSamsungAccount(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 56
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungAccountUsed()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v0

    .line 62
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 68
    :cond_1
    :try_start_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 69
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x2ee1

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 71
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isTestStore()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    if-nez v1, :cond_0

    .line 77
    const/4 v0, 0x1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static isRegisteredSamsungAccount(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 229
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 230
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 231
    const-string v1, ""

    .line 232
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    .line 234
    aget-object v1, v2, v0

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 237
    :cond_0
    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAccountValidator::isRegisteredSamsungAccount:: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x1

    .line 243
    :cond_1
    return v0
.end method

.method public static isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungAccountUsed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    :try_start_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 108
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    const-wide/16 v3, 0x32f4

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 110
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isSAccountVersionHigherThan1_3_0032(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 129
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungAccountUsed()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    .line 140
    :cond_1
    :try_start_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 141
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v1

    const-wide/32 v3, 0x1fbf0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 143
    const/4 v0, 0x1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static isSAccountVersionHigherThan1_4_0366(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungAccountUsed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    :try_start_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 167
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    const-wide/32 v3, 0x2244e

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 169
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isSAccountVersionHigherThan1_5_0200(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 186
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungAccountUsed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v0

    .line 192
    :cond_1
    :try_start_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 193
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    const-wide/32 v3, 0x24ab8

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 195
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static isTestStore()Z
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isTestStore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 254
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->onSamsungAccountDoesnotExist()V

    .line 269
    :goto_0
    return-void

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_SamsungAccountNewHandler:Ljava/lang/Class;

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->_SamsungAccountHandlerActivity:Ljava/lang/Class;

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onFailGetAccessToken()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->onFinalResult(Z)V

    .line 295
    return-void
.end method

.method public onSamsungAccountDisabled()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->onFinalResult(Z)V

    .line 279
    return-void
.end method

.method public onSamsungAccountDoesnotExist()V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->onFinalResult(Z)V

    .line 274
    return-void
.end method

.method public onSamsungAccountLoginFailed()V
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->onFinalResult(Z)V

    .line 289
    return-void
.end method

.method public onSamsungAccountLoginSuccess()V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->onFinalResult(Z)V

    .line 284
    return-void
.end method
