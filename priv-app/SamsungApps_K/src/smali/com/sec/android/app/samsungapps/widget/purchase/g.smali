.class final Lcom/sec/android/app/samsungapps/widget/purchase/g;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 223
    const v0, 0x7f0c0244

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 224
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 225
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    move-result-object v1

    invoke-interface {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;->onSelItem(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;)Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->h(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/m;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/m;->a(Z)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Z)Z

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/widget/purchase/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/n;->notifyDataSetChanged()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->g(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;->clearSel()Z

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->j(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->b(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Z)Z

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/g;->a:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->i(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Lcom/sec/android/app/samsungapps/widget/purchase/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/n;->notifyDataSetChanged()V

    goto :goto_0
.end method
