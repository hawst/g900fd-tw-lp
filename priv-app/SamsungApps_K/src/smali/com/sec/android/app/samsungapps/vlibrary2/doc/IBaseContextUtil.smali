.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFakeModel(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;

    if-eqz v0, :cond_0

    .line 10
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;

    .line 11
    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;->getFakeModel()Ljava/lang/String;

    move-result-object v0

    .line 15
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getGearOSVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 21
    if-eqz p0, :cond_0

    :try_start_0
    instance-of v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;

    if-eqz v1, :cond_0

    .line 22
    check-cast p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;

    .line 23
    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;->getGearPlatformVersion()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 32
    :cond_0
    :goto_0
    return-object v0

    .line 29
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isGear2(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 49
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getGearOSVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 56
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 55
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static isGearMode(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 37
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 44
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 43
    :catch_1
    move-exception v1

    goto :goto_0
.end method
