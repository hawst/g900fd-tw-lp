.class public Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 25
    const v0, 0x7f0400de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->setContentView(I)V

    .line 27
    const v0, 0x7f0c02f3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 28
    new-instance v1, Lcom/sec/android/app/samsungapps/eu;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/eu;-><init>(Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v0, 0x7f0c009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 37
    const v0, 0x7f0c009f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 42
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 43
    const-string v1, "knox_welcome_page_check"

    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-ne v4, v6, :cond_1

    .line 46
    :cond_0
    const-string v1, "0"

    .line 47
    const-string v4, "knox_welcome_page_check"

    const-string v5, "0"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 50
    :cond_1
    const-string v4, "1"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v6, :cond_2

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->b()V

    .line 55
    :cond_2
    new-instance v1, Lcom/sec/android/app/samsungapps/ev;

    invoke-direct {v1, p0, v0, v3}, Lcom/sec/android/app/samsungapps/ev;-><init>(Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;Landroid/widget/CheckBox;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/Main;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 88
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->finish()V

    .line 90
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->a()V

    .line 81
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/KnoxAppsMainActivity;->a()V

    .line 21
    return-void
.end method
