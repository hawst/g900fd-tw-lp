.class public Lcom/sec/android/app/samsungapps/CouponListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/CouponListView;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CouponListActivity;)Lcom/sec/android/app/samsungapps/view/CouponListView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    return-object v0
.end method


# virtual methods
.method protected displayData()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33
    const v0, 0x7f040093

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->setMainView(I)V

    .line 37
    new-instance v1, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    .line 38
    new-instance v2, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    .line 39
    new-instance v3, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    .line 41
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    .line 42
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    .line 43
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    .line 47
    const v0, 0x7f0c020b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CouponListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setFocusableInTouchMode(Z)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setItemsCanFocus(Z)V

    .line 50
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 51
    new-instance v4, Lcom/sec/android/app/samsungapps/cd;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/cd;-><init>(Lcom/sec/android/app/samsungapps/CouponListActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setContext(Landroid/content/Context;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/view/CouponListView;->setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->refresh()V

    .line 69
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080188

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0003

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CouponListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 78
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->mContext:Landroid/content/Context;

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->displayData()V

    .line 80
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->a:Lcom/sec/android/app/samsungapps/view/CouponListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CouponListView;->release()V

    .line 103
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 104
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->checkLoginState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    .line 93
    :cond_0
    return-void
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public showGiftCardNotiArea()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CouponListActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isNotUsingGooglePlayCountry()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    const v0, 0x7f0c020a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CouponListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 125
    if-eqz v0, :cond_0

    .line 127
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
