.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getVcontentGradeImgUrl()Ljava/lang/String;
.end method

.method public abstract getVcreateDate()Ljava/lang/String;
.end method

.method public abstract getVlastUpdateDate()Ljava/lang/String;
.end method

.method public abstract getVnameAuth()Ljava/lang/String;
.end method

.method public abstract getVproductDescription()Ljava/lang/String;
.end method

.method public abstract getVrealContentsSize()Ljava/lang/String;
.end method

.method public abstract getVreportNum()Ljava/lang/String;
.end method

.method public abstract getVrepresentation()Ljava/lang/String;
.end method

.method public abstract getVrestrictedAge()Ljava/lang/String;
.end method

.method public abstract getVscreenShotCount()I
.end method

.method public abstract getVscreenShotImgURL(I)Ljava/lang/String;
.end method

.method public abstract getVscreenShotOrientation(I)I
.end method

.method public abstract getVsellerDescription()Ljava/lang/String;
.end method

.method public abstract getVsellerEmail()Ljava/lang/String;
.end method

.method public abstract getVsellerLocation()Ljava/lang/String;
.end method

.method public abstract getVsellerName()Ljava/lang/String;
.end method

.method public abstract getVsellerNum()Ljava/lang/String;
.end method

.method public abstract getVsellerRegisterNum()Ljava/lang/String;
.end method

.method public abstract getVsellerTradeName()Ljava/lang/String;
.end method

.method public abstract getVsellerUrl()Ljava/lang/String;
.end method

.method public abstract getVsupportEmail()Ljava/lang/String;
.end method

.method public abstract getVupdateDescription()Ljava/lang/String;
.end method

.method public abstract getVversion()Ljava/lang/String;
.end method

.method public abstract getVyoutubeRtspUrl()Ljava/lang/String;
.end method

.method public abstract getVyoutubeScreenShoutUrl()Ljava/lang/String;
.end method

.method public abstract getVyoutubeUrl()Ljava/lang/String;
.end method

.method public abstract hasGradeImg()Z
.end method

.method public abstract hasScreenshotImage()Z
.end method

.method public abstract isAllAge()Z
.end method

.method public abstract isAlreadyPurchased()Z
.end method

.method public abstract isDisplaySellerInfo()Z
.end method

.method public abstract isDisplaySellerLocation()Z
.end method

.method public abstract isIAPSupported()Z
.end method

.method public abstract isLinkApp()Z
.end method

.method public abstract isRevision()Z
.end method

.method public abstract isSINA()Z
.end method

.method public abstract openScreenShot(I)V
.end method
