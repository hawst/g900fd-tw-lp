.class public Lcom/sec/android/app/samsungapps/CommonDialogInterface;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# static fields
.field private static f:Ljava/util/ArrayList;


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Landroid/app/Activity;

.field private e:Lcom/sec/android/app/samsungapps/NotiDialogObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a:I

    .line 15
    iput v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->b:I

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->c:Z

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->d:Landroid/app/Activity;

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->e:Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    .line 25
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 116
    new-instance v1, Ljava/util/ArrayList;

    sget-object v0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->f:Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 117
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 119
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    .line 123
    invoke-interface {v0, p0, p1}, Lcom/sec/android/app/samsungapps/NotiDialogObserver;->onNotiDialogReceive(Lcom/sec/android/app/samsungapps/CommonDialogInterface;I)I

    goto :goto_0

    .line 126
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 127
    return-void
.end method

.method private static a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 107
    if-nez p0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static registerObserver(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)V
    .locals 2

    .prologue
    .line 88
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 89
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    return-void
.end method

.method public static registerObserver(Lcom/sec/android/app/samsungapps/NotiDialogObserver;I)V
    .locals 2

    .prologue
    .line 95
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 96
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 98
    :cond_0
    return-void
.end method

.method public static unRegisterObserver(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)V
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 103
    return-void
.end method


# virtual methods
.method public getContext()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->d:Landroid/app/Activity;

    return-object v0
.end method

.method public getFinish()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->c:Z

    return v0
.end method

.method public getNotiDialogObserver()Lcom/sec/android/app/samsungapps/NotiDialogObserver;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->e:Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    return-object v0
.end method

.method public getNotiType()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a:I

    return v0
.end method

.method public getTid()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->b:I

    return v0
.end method

.method public onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 133
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 145
    :sswitch_0
    if-ne p2, v1, :cond_0

    .line 147
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->c:Z

    if-ne v0, v2, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 155
    :sswitch_1
    if-ne p2, v1, :cond_0

    .line 157
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->c:Z

    if-ne v0, v2, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 167
    :sswitch_2
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a(I)V

    goto :goto_0

    .line 174
    :sswitch_3
    if-ne p2, v1, :cond_0

    .line 176
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a(I)V

    goto :goto_0

    .line 210
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->e:Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    invoke-interface {v0, p0, p2}, Lcom/sec/android/app/samsungapps/NotiDialogObserver;->onNotiDialogReceive(Lcom/sec/android/app/samsungapps/CommonDialogInterface;I)I

    goto :goto_0

    .line 218
    :sswitch_5
    if-ne p2, v1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->e:Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    invoke-interface {v0, p0, p2}, Lcom/sec/android/app/samsungapps/NotiDialogObserver;->onNotiDialogReceive(Lcom/sec/android/app/samsungapps/CommonDialogInterface;I)I

    goto :goto_0

    .line 133
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_4
        0x7d0 -> :sswitch_4
        0xbb9 -> :sswitch_0
        0x13ed -> :sswitch_4
        0x1450 -> :sswitch_4
        0x15e1 -> :sswitch_3
        0x15e2 -> :sswitch_4
        0xff01 -> :sswitch_4
        0xff02 -> :sswitch_2
        0xff03 -> :sswitch_2
        0xff04 -> :sswitch_2
        0xff06 -> :sswitch_4
        0xff07 -> :sswitch_0
        0xff0a -> :sswitch_0
        0xff0c -> :sswitch_0
        0xff0f -> :sswitch_0
        0xff10 -> :sswitch_1
        0xff11 -> :sswitch_0
        0xff13 -> :sswitch_3
        0xff17 -> :sswitch_4
        0xff18 -> :sswitch_0
        0xff1b -> :sswitch_3
        0xff1c -> :sswitch_4
        0xff1d -> :sswitch_4
        0xff1e -> :sswitch_4
        0xff1f -> :sswitch_3
        0xff20 -> :sswitch_4
        0xff21 -> :sswitch_4
        0xff23 -> :sswitch_4
        0xff28 -> :sswitch_0
        0xff29 -> :sswitch_5
        0xff2a -> :sswitch_4
        0xff2b -> :sswitch_5
        0xff2c -> :sswitch_4
        0xff2d -> :sswitch_4
        0xff2f -> :sswitch_4
        0xff34 -> :sswitch_4
        0xff35 -> :sswitch_4
        0xff36 -> :sswitch_4
        0xff37 -> :sswitch_4
        0xff38 -> :sswitch_4
        0xff39 -> :sswitch_4
        0xff3a -> :sswitch_5
        0xff3b -> :sswitch_4
        0xff3c -> :sswitch_0
        0xff3d -> :sswitch_0
        0xff3e -> :sswitch_5
        0xff3f -> :sswitch_4
        0xff40 -> :sswitch_4
        0xff41 -> :sswitch_4
        0xff43 -> :sswitch_4
        0xff45 -> :sswitch_4
        0xff46 -> :sswitch_5
    .end sparse-switch
.end method

.method public setContext(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->d:Landroid/app/Activity;

    .line 59
    return-void
.end method

.method public setFinish(Z)V
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->c:Z

    .line 49
    return-void
.end method

.method public setNotiDialogObserver(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->e:Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    .line 54
    return-void
.end method

.method public setNotiType(I)V
    .locals 1

    .prologue
    .line 38
    const v0, 0x1869f

    if-le p1, v0, :cond_0

    .line 40
    const p1, 0xff3f

    .line 43
    :cond_0
    iput p1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->a:I

    .line 44
    return-void
.end method

.method public setTid(I)V
    .locals 0

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->b:I

    .line 30
    return-void
.end method
