.class public Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mIContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;->mContext:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;->mIContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    .line 21
    return-void
.end method


# virtual methods
.method public IsLikeYn()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;->mIContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasLike()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 49
    :catch_0
    move-exception v1

    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IsLikeYn:: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;->mIContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public registerContentLikeUnlike()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeUnlikeRegisterCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeUnlikeRegisterCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;)V

    return-object v0
.end method

.method public setNewContentLikeUnlike(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;)V
    .locals 3

    .prologue
    .line 37
    if-eqz p1, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;->mIContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;->getUserLikeYn()Z

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;->getLikeCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->setLikeState(ZI)V

    .line 42
    :cond_0
    return-void
.end method
