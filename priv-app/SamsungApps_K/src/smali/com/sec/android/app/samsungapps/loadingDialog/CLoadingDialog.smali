.class public Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/LoadingDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method


# virtual methods
.method public endLoading()V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 19
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 20
    return-void
.end method

.method public startLoading()V
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 11
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CLoadingDialog;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 12
    return-void
.end method
