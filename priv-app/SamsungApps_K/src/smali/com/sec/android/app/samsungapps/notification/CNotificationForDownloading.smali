.class public Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/downloadnotification/INotificationForDownloading;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

.field private e:Landroid/content/Context;

.field private f:Landroid/app/NotificationManager;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;

.field private n:Landroid/app/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const v3, -0x19191a

    const v2, -0x777778

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const v0, -0x282829

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a:I

    .line 35
    const v0, -0x646465

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    .line 43
    sget-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->IDLE:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->f:Landroid/app/NotificationManager;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const-string v1, "com.android.settings"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->c:I

    .line 57
    iget v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->c:I

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ThemeInfo;->isBlackTheme(I)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 58
    iput v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a:I

    .line 59
    iput v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    .line 62
    :cond_0
    const-string v0, "GT-I9008"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 63
    iput v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a:I

    .line 64
    iput v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    .line 66
    :cond_1
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->g:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->h:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->i:Ljava/lang/String;

    .line 69
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->j:Z

    .line 70
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->k:Z

    .line 71
    iput p7, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->l:I

    .line 72
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->m:Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;

    .line 73
    return-void
.end method

.method private static a()I
    .locals 4

    .prologue
    .line 78
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "status_bar_latest_event_content"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 79
    return v0
.end method

.method private static a(Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 338
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v1

    const-wide/16 v3, 0x64

    mul-long/2addr v1, v3

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v3

    div-long v0, v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    long-to-int v0, v0

    .line 344
    :cond_0
    :goto_0
    return v0

    .line 342
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 181
    if-nez p0, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/app/Notification;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->j:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 119
    :goto_0
    if-eqz v0, :cond_0

    .line 120
    iput-object v0, p1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 122
    :cond_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->m:Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->getDetailPageIntent()Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/widget/RemoteViews;)V
    .locals 2

    .prologue
    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 319
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 322
    :goto_0
    return-object v0

    .line 321
    :catch_0
    move-exception v0

    const-string v0, "CNotificationManager::getRemoteViewBitmmapIcon::There is no package which name is same with paramater pkgName"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 322
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 108
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 109
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 110
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    goto :goto_0
.end method

.method private static b(Landroid/app/Notification;)V
    .locals 1

    .prologue
    .line 209
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const v0, 0x7f0201bd

    iput v0, p0, Landroid/app/Notification;->icon:I

    .line 214
    :goto_0
    return-void

    .line 212
    :cond_0
    const v0, 0x7f0200d2

    iput v0, p0, Landroid/app/Notification;->icon:I

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 133
    const-string v0, "CNotificationManager : setDownloadProgress"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->f:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->l:I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    const-string v0, "notification doesn\'t have contentIntent"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040080

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 267
    :goto_0
    return-object v0

    .line 264
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f04007e

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public addInstallItem()V
    .locals 7

    .prologue
    const v6, 0x7f0c01d0

    const v5, 0x7f0c01b0

    const/4 v4, -0x1

    .line 242
    sget-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->INSTALLING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b()V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const-string v2, "isa_quick_panel_logo"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Landroid/app/Notification;->icon:I

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    .line 246
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    const v2, 0x7f0c0088

    iget v1, v1, Landroid/app/Notification;->icon:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v1, 0x7f0c01cf

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->h:Ljava/lang/String;

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a:I

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    const v1, 0x7f0c01b3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v4, v4, v2}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const v2, 0x7f08027f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    invoke-virtual {v0, v5, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 248
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Landroid/widget/RemoteViews;)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    const/4 v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Landroid/app/Notification;)V

    .line 251
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->c()V

    .line 252
    return-void
.end method

.method public addWaitingItem(Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;)V
    .locals 8

    .prologue
    const v7, 0x7f0c01b0

    const v3, 0x7f0c0088

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->WAITING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    if-eq v0, v1, :cond_1

    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b()V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b(Landroid/app/Notification;)V

    .line 195
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 196
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0201bd

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 197
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Landroid/widget/RemoteViews;)V

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iput v4, v1, Landroid/app/Notification;->flags:I

    .line 202
    :goto_1
    invoke-static {p1, p2}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;)I

    move-result v1

    const v2, 0x7f0c01d0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    invoke-virtual {v0, v7, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    const v2, 0x7f0c01b3

    const/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v1, v5}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    const v2, 0x7f0c01cf

    const-string v3, "%s%%"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f0c01cf

    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Landroid/app/Notification;)V

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->c()V

    .line 205
    sget-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->WAITING:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    .line 206
    return-void

    .line 196
    :cond_0
    const v1, 0x7f020191

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto :goto_1
.end method

.method public removeItem()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->f:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->l:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 334
    return-void
.end method

.method public setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;)V
    .locals 8

    .prologue
    const v7, 0x7f0c01b0

    const v3, 0x7f0c0088

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->DOWNLOADPROGRESS:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    if-eq v0, v1, :cond_3

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b(Landroid/app/Notification;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040080

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0201bd

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Landroid/widget/RemoteViews;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iput v4, v1, Landroid/app/Notification;->flags:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Landroid/app/Notification;)V

    .line 90
    :goto_2
    invoke-static {p1, p2}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;)I

    move-result v1

    const v2, 0x7f0c01d0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    invoke-virtual {v0, v7, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    const v2, 0x7f0c01b3

    const/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v1, v5}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    const v2, 0x7f0c01cf

    const-string v3, "%s%%"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f0c01cf

    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->c()V

    .line 92
    sget-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->DOWNLOADPROGRESS:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    .line 93
    return-void

    .line 86
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f04007f

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f04007e

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_2
    const v1, 0x7f020191

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_1

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->n:Landroid/app/Notification;

    iget-object v0, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto/16 :goto_2
.end method

.method public setItemInstalled()V
    .locals 6

    .prologue
    .line 282
    sget-object v0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;->INSTALLED:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->d:Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading$State;

    .line 283
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const-string v2, "isa_quick_panel_logo"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 297
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->m:Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/notification/NotificationIntentCreator;->getExecuteItent()Landroid/app/PendingIntent;

    move-result-object v1

    .line 299
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const-string v4, "isa_quick_panel_logo"

    const-string v5, "drawable"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08027e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 306
    const-string v0, "CNotificationManager : setItemInstalled"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->e:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 311
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    :goto_1
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationForDownloading;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 312
    :catch_0
    move-exception v0

    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CNotificationForDownloading::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method
