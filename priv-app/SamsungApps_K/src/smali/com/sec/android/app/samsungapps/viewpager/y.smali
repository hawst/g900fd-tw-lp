.class final Lcom/sec/android/app/samsungapps/viewpager/y;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;B)V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/y;-><init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)V

    return-void
.end method


# virtual methods
.method public final onPageScrollStateChanged(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 253
    if-nez p1, :cond_3

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 255
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 256
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/widget/LinearLayout;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v3, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;I)Landroid/widget/TextView;

    move-result-object v3

    .line 259
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/support/v4/view/ViewPager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 260
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->f(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 261
    if-eqz v3, :cond_0

    .line 262
    const/4 v2, 0x1

    invoke-static {v3, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    .line 255
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->g(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 266
    if-eqz v3, :cond_0

    .line 267
    invoke-static {v3, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 274
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->invalidate()V

    .line 277
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_4

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 280
    :cond_4
    return-void
.end method

.method public final onPageScrolled(IFI)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x437f0000    # 255.0f

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 219
    const/4 v0, 0x1

    .line 224
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->c(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/widget/LinearLayout;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 227
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 229
    mul-float v4, v6, p2

    float-to-int v4, v4

    shl-int/lit8 v4, v4, 0x18

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I

    move-result v5

    add-int/2addr v4, v5

    .line 230
    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    mul-float/2addr v5, v6

    float-to-int v5, v5

    shl-int/lit8 v5, v5, 0x18

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->d(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I

    move-result v6

    add-int/2addr v5, v6

    .line 232
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 234
    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 235
    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 237
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->e(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v2, v1, p1, p2, v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->a(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;IIFZ)V

    .line 237
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 221
    goto :goto_0

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->invalidate()V

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_3

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 249
    :cond_3
    return-void
.end method

.method public final onPageSelected(I)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->delegatePageListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setCurrentTabSelect(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->h(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/y;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->b(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;I)V

    .line 291
    return-void
.end method
