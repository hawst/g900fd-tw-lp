.class public Lcom/sec/android/app/samsungapps/view/WebImageView;
.super Landroid/widget/ImageView;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

.field c:Z

.field d:I

.field e:Z

.field f:Z

.field g:Landroid/os/Handler;

.field h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

.field private i:Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

.field private j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

.field protected mOverlay:Z

.field protected mResourceID:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->c:Z

    .line 30
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/view/be;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/be;-><init>(Lcom/sec/android/app/samsungapps/view/WebImageView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->g:Landroid/os/Handler;

    .line 137
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 89
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->e:Z

    .line 90
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->f:Z

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 29
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->c:Z

    .line 30
    iput v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/view/be;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/be;-><init>(Lcom/sec/android/app/samsungapps/view/WebImageView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->g:Landroid/os/Handler;

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/R$styleable;->WebImageView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->e:Z

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/R$styleable;->WebImageView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->f:Z

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 76
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 29
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->c:Z

    .line 30
    iput v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/view/be;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/be;-><init>(Lcom/sec/android/app/samsungapps/view/WebImageView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->g:Landroid/os/Handler;

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/R$styleable;->WebImageView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->e:Z

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/R$styleable;->WebImageView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->f:Z

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/WebImageView;)Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->i:Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/WebImageView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    if-eqz v0, :cond_0

    .line 166
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 167
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->d:I

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/bf;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/bf;-><init>(Lcom/sec/android/app/samsungapps/view/WebImageView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method protected applyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 150
    if-nez p1, :cond_0

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    if-eqz v1, :cond_1

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;->postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 159
    :cond_1
    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    move-object p1, v0

    goto :goto_1
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    return-object v0
.end method

.method public isSucceesfullyLoaded()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->c:Z

    return v0
.end method

.method protected overlayBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 204
    if-nez p1, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-object v0

    .line 216
    :cond_1
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 219
    if-eqz v1, :cond_0

    .line 224
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 225
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 227
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 228
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 230
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, p1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 231
    int-to-float v2, v2

    int-to-float v3, v3

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 232
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 234
    :catch_0
    move-exception v1

    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WebImageView::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :catch_1
    move-exception v1

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WebImageView::OutOfMemoryError::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected requestImage(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 115
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 116
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->exist()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 118
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mOverlay:Z

    if-eqz v1, :cond_1

    .line 120
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mResourceID:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->overlayBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->applyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->i:Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->i:Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;->downloadedDone(Z)V

    .line 131
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->c:Z

    .line 135
    :goto_1
    return-void

    .line 125
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->applyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setConverter(Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 143
    return-void
.end method

.method protected setImage(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->e:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 258
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected setImageLoadedUsingAnimation(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->e:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 268
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 273
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->f:Z

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    const v1, 0x7f050001

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 275
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 280
    :goto_1
    return-void

    .line 270
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mContext:Landroid/content/Context;

    const v1, 0x7f050002

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 278
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/WebImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->b:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 248
    return-void
.end method

.method public setNotifier(Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->i:Lcom/sec/android/app/samsungapps/view/WebImageView$WebImageNotifier;

    .line 46
    return-void
.end method

.method public setOverlayResource(I)V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mOverlay:Z

    .line 199
    iput p1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->mResourceID:I

    .line 200
    return-void
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    if-nez p1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    .line 104
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/WebImageView;->a:Ljava/lang/String;

    .line 106
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->requestImage(Ljava/lang/String;)V

    goto :goto_0
.end method
