.class final Lcom/sec/android/app/samsungapps/pushclient/g;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/pushclient/PushService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V
    .locals 0

    .prologue
    .line 1169
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/g;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 4

    .prologue
    .line 1174
    if-nez p2, :cond_0

    const-string v0, "8003"

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1176
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "doRegisterWebOTAService OK("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/g;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Z)V

    .line 1185
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.action.push_reg"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1186
    const-string v1, "extra.push.status"

    const-string v2, "SUCCESS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1189
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/g;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/g;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->j(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    .line 1223
    :goto_0
    return-void

    .line 1211
    :cond_1
    const-string v0, "5000"

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/g;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_REG_SIGN_ERROR:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "W"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto :goto_0

    .line 1220
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/g;->a:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;->WEB_REG_FAIL:Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "W"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->a(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/pushclient/PushErrorType;Ljava/lang/String;)V

    goto :goto_0
.end method
