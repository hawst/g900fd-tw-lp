.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mList:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;->mList:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    .line 11
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;-><init>()V

    .line 22
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;->mList:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->add(Ljava/lang/Object;)Z

    .line 24
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method
