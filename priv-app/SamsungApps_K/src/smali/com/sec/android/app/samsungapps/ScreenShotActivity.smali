.class public Lcom/sec/android/app/samsungapps/ScreenShotActivity;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

.field c:Landroid/widget/RelativeLayout;

.field d:Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;

.field private e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    .line 169
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ScreenShotActivity;)Lcom/sec/android/app/samsungapps/ScreenShotGallery;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onBackPressed()V

    .line 116
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 133
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getScreenShot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getSel()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getOrientation(I)I

    move-result v1

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->getSelectedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 144
    if-eqz v0, :cond_0

    .line 149
    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 152
    :pswitch_0
    if-eq v1, v3, :cond_2

    .line 153
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 160
    :pswitch_1
    if-ne v1, v3, :cond_2

    .line 161
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 163
    :cond_2
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/high16 v4, 0x1000000

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 65
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->RequestScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->findSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->finish()V

    .line 110
    :goto_1
    return-void

    .line 67
    :cond_1
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    const/4 v0, 0x1

    goto :goto_0

    .line 73
    :cond_2
    const v0, 0x7f040041

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->setContentView(I)V

    .line 75
    const v0, 0x7f0c0138

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->c:Landroid/widget/RelativeLayout;

    .line 76
    new-instance v0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;-><init>(Lcom/sec/android/app/samsungapps/ScreenShotActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->d:Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;

    .line 77
    new-instance v0, Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    new-instance v2, Landroid/widget/Gallery$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setFocusableInTouchMode(Z)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setSpacing(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setUnselectedAlpha(F)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->d:Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setScreenShotInterface(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    new-instance v1, Lcom/sec/android/app/samsungapps/hu;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/hu;-><init>(Lcom/sec/android/app/samsungapps/ScreenShotActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getSel()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ScreenShotGallery;->setSelection(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->c:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->e:Lcom/sec/android/app/samsungapps/ScreenShotGallery;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->d:Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->d:Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->c:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 127
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onDestroy()V

    .line 128
    return-void
.end method
