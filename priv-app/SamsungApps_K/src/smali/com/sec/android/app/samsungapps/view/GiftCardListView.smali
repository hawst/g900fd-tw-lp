.class public Lcom/sec/android/app/samsungapps/view/GiftCardListView;
.super Lcom/sec/android/app/samsungapps/view/MyListView;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

.field private f:Lcom/sec/android/app/samsungapps/view/w;

.field private g:Landroid/widget/ScrollView;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->g:Landroid/widget/ScrollView;

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->g:Landroid/widget/ScrollView;

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->g:Landroid/widget/ScrollView;

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/GiftCardListView;)Lcom/sec/android/app/samsungapps/view/w;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setContext(Landroid/content/Context;)V

    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/view/w;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/view/w;-><init>(Lcom/sec/android/app/samsungapps/view/GiftCardListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/GiftCardListView;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V
    .locals 3

    .prologue
    .line 39
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x24

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/GiftCardListView;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/w;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isGiftCard()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/w;->add(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;->showGiftCardNotiArea()V

    :cond_2
    :goto_1
    new-instance v0, Lcom/sec/android/app/samsungapps/view/t;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/t;-><init>(Lcom/sec/android/app/samsungapps/view/GiftCardListView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setNoDataEmptyView()V

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/GiftCardListView;)Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    return-object v0
.end method


# virtual methods
.method public clearList()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/w;->clear()V

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/w;->notifyDataSetChanged()V

    .line 234
    :cond_0
    return-void
.end method

.method protected empty()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 217
    .line 219
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/w;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/w;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 223
    :cond_0
    return v0
.end method

.method public getParentScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->g:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 254
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    .line 255
    sget-object v1, Lcom/sec/android/app/samsungapps/view/v;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 262
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 258
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->refresh()V

    goto :goto_0

    .line 255
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 369
    const v0, 0x1fffffff

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 370
    invoke-super {p0, p1, v0}, Lcom/sec/android/app/samsungapps/view/MyListView;->onMeasure(II)V

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 373
    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::mContext is empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 208
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;->release()V

    .line 179
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 181
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    .line 182
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListReceiver;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/view/u;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/u;-><init>(Lcom/sec/android/app/samsungapps/view/GiftCardListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setLoadingEmptyView()V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 241
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 242
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->release()V

    .line 243
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->c:Landroid/content/Context;

    .line 53
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 58
    return-void
.end method

.method public setParentScrollView(Landroid/widget/ScrollView;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->g:Landroid/widget/ScrollView;

    .line 90
    return-void
.end method
