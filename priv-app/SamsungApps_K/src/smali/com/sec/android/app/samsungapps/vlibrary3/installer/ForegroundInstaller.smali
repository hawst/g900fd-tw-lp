.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;

.field private e:Ljava/lang/String;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->b:Landroid/content/Context;

    .line 24
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 26
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->e:Ljava/lang/String;

    .line 28
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 78
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->INSTALLCOMPLETED_AND_GUIDMATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;)V

    goto :goto_0
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    move-result-object v0

    return-object v0
.end method

.method public install()V
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;)V

    .line 110
    return-void
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;)V
    .locals 2

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/o;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 68
    :goto_0
    return-void

    .line 50
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->deleteFile()Z

    goto :goto_0

    .line 53
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->f:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->getAbsoluteFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->install(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;->onForegroundInstalling()V

    goto :goto_0

    .line 59
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->d:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller$IForegroundInstallerObserver;->onForegroundInstallCompleted()V

    goto :goto_0

    .line 62
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;)V

    goto :goto_0

    .line 65
    :pswitch_5
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;)V

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;)V

    return-void
.end method

.method public onPackageAdded(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->a(Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public onPackageInstalled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->a(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public onPackageReplaced(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->a(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public sendEvent(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;)V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;)Z

    .line 33
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    .line 38
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstaller;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;)V

    return-void
.end method
