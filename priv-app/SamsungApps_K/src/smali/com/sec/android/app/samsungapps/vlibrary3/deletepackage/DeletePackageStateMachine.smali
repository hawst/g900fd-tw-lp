.class public Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 21
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;

    .line 35
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 94
    const-string v0, "DeletePackageStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 118
    :goto_0
    :pswitch_0
    return-void

    .line 98
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->CHECK_PACKAGE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->CHECK_AM_I_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->DELETE_AS_NORMAL_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 112
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->DELETE_AS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 41
    const-string v0, "DeletePackageStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 45
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 48
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->CHECK_PACKAGE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 56
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->CHECK_AM_I_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 67
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->DELETE_AS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->DELETE_AS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 81
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_9
        :pswitch_9
    .end packed-switch

    .line 45
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 53
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 64
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 78
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method
