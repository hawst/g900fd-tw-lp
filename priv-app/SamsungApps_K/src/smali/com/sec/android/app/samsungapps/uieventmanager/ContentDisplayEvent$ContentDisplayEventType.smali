.class public final enum Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field public static final enum DisplayContentDetail:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field public static final enum DisplayContentDetailActivity:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field public static final enum DisplayHotKeywordSearchList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field public static final enum DisplaySeries:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field public static final enum OnClickContentDownloadButton:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    const-string v1, "DisplayContentDetail"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetail:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    const-string v1, "DisplayBannerContentList"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    const-string v1, "DisplayContentDetailActivity"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetailActivity:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    const-string v1, "DisplayHotKeywordSearchList"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayHotKeywordSearchList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    const-string v1, "DisplaySeries"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplaySeries:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    const-string v1, "OnClickContentDownloadButton"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->OnClickContentDownloadButton:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetail:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetailActivity:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayHotKeywordSearchList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplaySeries:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->OnClickContentDownloadButton:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    return-object v0
.end method
