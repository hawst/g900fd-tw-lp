.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _IRegisterIranCreditCardCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->_IRegisterIranCreditCardCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;

    .line 14
    return-void
.end method


# virtual methods
.method public getIranDebitURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->_IRegisterIranCreditCardCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;->getIranDebitURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->_IRegisterIranCreditCardCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;->getIranCreditCardViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 19
    return-void
.end method

.method public setDebitCardResult(Z)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->_IRegisterIranCreditCardCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand$IRegisterIranCreditCardCommandData;->setCardRegisterResult(Z)V

    .line 29
    if-eqz p1, :cond_0

    .line 31
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->onFinalResult(Z)V

    .line 33
    :cond_0
    return-void
.end method
