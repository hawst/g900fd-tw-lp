.class public Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
.super Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x390236635e9faf50L


# instance fields
.field public listBGImgUrl:Ljava/lang/String;

.field public listBGLandscapeImgUrl:Ljava/lang/String;

.field public listContentSetID:Ljava/lang/String;

.field public listDescription:Ljava/lang/String;

.field public listTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    .line 13
    return-void
.end method


# virtual methods
.method public clearData()V
    .locals 1

    .prologue
    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listTitle:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listDescription:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGImgUrl:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listContentSetID:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->listBGLandscapeImgUrl:Ljava/lang/String;

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->clear()V

    .line 27
    return-void
.end method
