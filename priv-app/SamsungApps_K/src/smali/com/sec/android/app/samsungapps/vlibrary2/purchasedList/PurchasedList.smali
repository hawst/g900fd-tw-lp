.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;
.super Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList;


# static fields
.field private static final SERIALVERSIONUID:J = 0x5c00eec69dd8dbc6L


# instance fields
.field private _Listeners:Ljava/util/ArrayList;

.field private mEndNum:I

.field private mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;


# direct methods
.method public constructor <init>(IILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(II)V

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->_Listeners:Ljava/util/ArrayList;

    .line 35
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mEndNum:I

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    .line 37
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->_Listeners:Ljava/util/ArrayList;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    .line 30
    return-void
.end method

.method private isDownloading(Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    .line 177
    if-nez v1, :cond_0

    .line 196
    :goto_0
    :pswitch_0
    return v0

    .line 181
    :cond_0
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/b;->a:[I

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 192
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isDownloading true:: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    .line 193
    const/4 v0, 0x1

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private notifyUpdate()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList$IAllSelectableItemListListener;

    .line 238
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList$IAllSelectableItemListListener;->onListStateChanged()V

    goto :goto_0

    .line 240
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
    .locals 1

    .prologue
    .line 288
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addIAllSelectableItemListListener(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList$IAllSelectableItemListListener;)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method public append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 294
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isIntersect(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getEndNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->setEndNumber(I)V

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)V

    .line 302
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 303
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 305
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 307
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->_Listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 231
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->clear()V

    .line 232
    return-void
.end method

.method public clearSel()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 336
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    goto :goto_0

    .line 338
    :cond_0
    return v2
.end method

.method public deSelectAll()Z
    .locals 3

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 204
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    goto :goto_0

    .line 206
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public deSelectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 315
    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 318
    if-ne v0, p1, :cond_0

    .line 320
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    .line 321
    const/4 v0, 0x1

    .line 325
    :goto_0
    if-eqz v0, :cond_1

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->notifyUpdate()V

    .line 329
    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    return-object v0
.end method

.method public getDeleteItemList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;
    .locals 5

    .prologue
    .line 65
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->getEndNumber()I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;-><init>(ILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 69
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 71
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_1
    return-object v1
.end method

.method public getFirstEndNum()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mEndNum:I

    return v0
.end method

.method public getUpdatableItemList(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;
    .locals 4

    .prologue
    .line 46
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    const/16 v0, 0xf

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mEndNum:I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;-><init>(IILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 52
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->mInstallChecker:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    invoke-interface {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 54
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    :cond_1
    return-object v1
.end method

.method public isAllDeselected()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 214
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 219
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public isAllSelected()Z
    .locals 2

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 252
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isAllSelected(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 268
    if-nez p1, :cond_0

    move v0, v1

    .line 283
    :goto_0
    return v0

    .line 273
    :cond_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 276
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isSelected()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isDownloading(Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getLoadType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 280
    goto :goto_0

    .line 283
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public selectAll()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 139
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    goto :goto_0

    .line 141
    :cond_0
    return v2
.end method

.method public selectAll(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 152
    if-nez p1, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    .line 157
    :cond_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 160
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isDownloading(Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getLoadType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 163
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 166
    goto :goto_0
.end method

.method public selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 83
    const/4 v2, 0x0

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 86
    if-ne v0, p1, :cond_0

    .line 88
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    move v0, v1

    .line 94
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isAllSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->notifyUpdate()V

    .line 98
    :cond_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 108
    const/4 v2, 0x0

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    .line 111
    if-ne v0, p1, :cond_0

    .line 113
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v3, p2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 114
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getGUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isDownloading(Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getLoadType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 117
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->setSelect(Z)V

    move v0, v1

    .line 124
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->isAllSelected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->notifyUpdate()V

    .line 128
    :cond_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method
