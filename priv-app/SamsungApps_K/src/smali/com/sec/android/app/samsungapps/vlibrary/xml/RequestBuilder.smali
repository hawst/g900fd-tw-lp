.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _RequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

.field private mContext:Landroid/content/Context;

.field private mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

.field protected mErrorPreprocessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

.field mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

.field private mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 98
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mErrorPreprocessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 108
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mContext:Landroid/content/Context;

    .line 109
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->_RequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

    .line 110
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mErrorPreprocessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 111
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 112
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    .line 113
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    return-object v0
.end method

.method private getImei()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    .line 1061
    if-eqz v0, :cond_0

    .line 1062
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 1064
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "000000000"

    goto :goto_0
.end method

.method private getSignIDFromGUID(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1068
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1078
    :cond_0
    :goto_0
    return-object v0

    .line 1072
    :cond_1
    const-string v1, "com.sec.android.app.samsungapps"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v2, :cond_2

    const-string v1, "com.sec.android.app.samsungapps.una"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v2, :cond_2

    const-string v1, "com.sec.android.app.samsungapps.una2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1075
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInitialContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private isValidString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1032
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1033
    const/4 v0, 0x1

    .line 1035
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public InitiatePSMSPurchaseNS(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1743
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->init(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    move-result-object v0

    .line 1745
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1747
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1748
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1749
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1750
    return-object v0
.end method

.method public PrepaidCardPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2004
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "PrepaidCardPurchase"

    const-string v3, "3130"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2006
    const-string v1, "productID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2007
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2008
    const-string v1, "couponIssuedSEQ"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2014
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2015
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2019
    :goto_1
    const-string v1, "guid"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2020
    const-string v1, "paymentAmountPrice"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;->getPaymentPrice()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2022
    const-string v1, "paymentMethodID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;->getPaymentMethodID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2024
    const-string v1, "rentalTerm"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2025
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2026
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2027
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 2028
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 2029
    return-object v0

    .line 2011
    :cond_0
    const-string v1, "couponIssuedSEQ"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2017
    :cond_1
    const-string v1, "imei"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public activateMigration(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2092
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "activateMigration"

    const-string v3, "5200"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2094
    const-string v2, "activate"

    if-eqz p1, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2096
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2097
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2098
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 2099
    return-object v0

    .line 2094
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method

.method public allProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1286
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "allProductList2Notc"

    const-string v4, "2003"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1288
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1289
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1291
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public androidManifest(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2171
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "androidManifest"

    const-string v3, "2081"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2173
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2174
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2175
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2176
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 2177
    return-object v0
.end method

.method public appInstallLog(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2409
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "appInstallLog"

    const-string v3, "2905"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2411
    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2414
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public autoCompleteSearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 858
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;I)V

    .line 861
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;)V

    .line 863
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public availableCountryList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 578
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AvailableCountryListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AvailableCountryListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 580
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 581
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public bannerClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 2438
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "bannerClickLog"

    const-string v3, "2440"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2440
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2446
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isInteractionBanner()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2447
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needProductDetail()Z

    move-result v1

    if-ne v1, v5, :cond_1

    .line 2448
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v1

    move-object v2, v7

    move-object v3, v1

    move-object v1, v7

    .line 2455
    :goto_0
    const-string v4, "interactionBannerYn"

    const-string v6, "0"

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461
    :goto_1
    const-string v4, "productID"

    invoke-virtual {v0, v4, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462
    const-string v3, "productSetID"

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2463
    const-string v2, "contentCategoryID"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2465
    instance-of v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/BigBanner;

    if-nez v1, :cond_5

    .line 2466
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isFeaturedBanner()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2468
    const-string v1, "bannerPos"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476
    :goto_2
    const-string v1, "bannerIndex"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerIndex()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v1

    if-ne v1, v5, :cond_0

    .line 2480
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v7

    .line 2482
    :cond_0
    const-string v1, "emailID"

    invoke-virtual {v0, v1, v7, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2484
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2485
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 2449
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needGetProductSetList()Z

    move-result v1

    if-ne v1, v5, :cond_2

    .line 2450
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    move-object v3, v7

    move-object v1, v7

    goto :goto_0

    .line 2451
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isURLBanner()Z

    move-result v1

    if-ne v1, v5, :cond_6

    .line 2452
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerLinkURL()Ljava/lang/String;

    move-result-object v1

    move-object v2, v7

    move-object v3, v7

    goto :goto_0

    .line 2457
    :cond_3
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v3

    .line 2458
    const-string v1, "interactionBannerYn"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v7

    move-object v2, v7

    goto :goto_1

    .line 2469
    :cond_4
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isNewBanner()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2470
    const-string v1, "bannerPos"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2473
    :cond_5
    const-string v1, "bannerPos"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v1, v7

    move-object v2, v7

    move-object v3, v7

    goto/16 :goto_0
.end method

.method public bannerClickLog(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 634
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;I)V

    .line 636
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 637
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 639
    return-object v0
.end method

.method public bigBannerList(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 653
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 654
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BigBannerParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;)V

    .line 655
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public bundleList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 837
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)V

    .line 840
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BundleListParser;-><init>()V

    .line 841
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public categoryList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 498
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "categoryList"

    const-string v3, "2010"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 501
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 511
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public categoryListSearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 453
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;I)V

    .line 455
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 457
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public categoryListSearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 444
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 446
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 448
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public categoryProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 212
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryProductListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;I)V

    .line 214
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 216
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public checkAppUpgrade(Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 898
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;I)V

    .line 901
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V

    .line 902
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public checkAppUpgrade(Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 910
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckAppUpgradeRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/util/UNAManager;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;I)V

    .line 913
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 914
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public checkDuplication(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1494
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;I)V

    .line 1496
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CheckDuplicationParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;)V

    .line 1497
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public checkPrepaidCard(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1942
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "checkPrepaidCard"

    const-string v3, "3140"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1944
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1945
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1946
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1947
    return-object v0
.end method

.method public checkUpdateCycle(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2603
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "checkUpdateCycle"

    const-string v3, "2307"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2605
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2606
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2608
    return-object v0
.end method

.method public commentDelete(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->toDel(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    move-result-object v0

    .line 293
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 294
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 296
    return-object v0
.end method

.method public commentDetail(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 381
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;I)V

    .line 383
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailParser;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;)V

    .line 384
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public commentList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 346
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;I)V

    .line 348
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 349
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public commentListExpert(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 362
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListExpertRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListExpertRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;I)V

    .line 364
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 365
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public commentModify(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->toModify(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    move-result-object v0

    .line 320
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 321
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 323
    return-object v0
.end method

.method public commentModify(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->toModify(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    move-result-object v0

    .line 330
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 331
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 333
    return-object v0
.end method

.method public commentRegister(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->toAdd(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    move-result-object v0

    .line 265
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 266
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 268
    return-object v0
.end method

.method public commentRegister(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;->toAdd(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML;

    move-result-object v0

    .line 276
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 277
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 279
    return-object v0
.end method

.method public completeAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 2142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->completeAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;

    move-result-object v0

    .line 2144
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2145
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2146
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 2147
    return-object v0
.end method

.method public completeInicisMobilePurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1639
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "completeInicisMobilePurchase"

    const-string v3, "3190"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1642
    const-string v1, "orderID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;->getOrderID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1643
    const-string v1, "paymentID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;->getPaymentID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1644
    const-string v1, "v3dToken"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;->getV3DToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1645
    const-string v1, "confirmUrl"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;->getConfirmURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1646
    const-string v2, "lastReqYn"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;->getLastRequest()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Y"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    const-string v2, "upointYn"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisCompleteParam;->isUpoint()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Y"

    :goto_1
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1651
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1652
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1653
    return-object v0

    .line 1646
    :cond_0
    const-string v1, "N"

    goto :goto_0

    .line 1648
    :cond_1
    const-string v1, "N"

    goto :goto_1
.end method

.method public completeOrder(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2546
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "completeOrder"

    const-string v3, "3102"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2549
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2550
    invoke-static {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeStringFieldsFromObject(Ljava/util/HashMap;Ljava/lang/Object;)Z

    .line 2552
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/util/Map$Entry;

    .line 2553
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2556
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2557
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2558
    return-object v0
.end method

.method public confirmOptBillingPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->confirm(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;

    move-result-object v0

    .line 1730
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1732
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1733
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1734
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1735
    return-object v0
.end method

.method public confirmSMSPurchaseNS(Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1756
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p2, p3, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->confirm(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;ZLcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    move-result-object v0

    .line 1758
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;)V

    .line 1761
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1763
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1764
    return-object v0
.end method

.method public confirmSMSPurchaseNS(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1770
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->confirm(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    move-result-object v0

    .line 1772
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1774
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1776
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1777
    return-object v0
.end method

.method public contentCategoryListSearch(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 462
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "categoryList"

    const-string v3, "2010"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 465
    const-string v1, "upLevelCategoryID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v1, "upLevelCategoryType"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v1, "categoryLevel"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v1, "startNum"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v1, "endNum"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    const-string v1, "categoryRepImgYn"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    .line 492
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 493
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 486
    :cond_0
    const-string v1, "categoryRepImgYn"

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public contentCategoryProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1410
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "contentCategoryProductList2Notc"

    const-string v4, "2031"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1412
    const-string v1, "contentCategoryID"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1414
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 1415
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1416
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1417
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setStatus(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1419
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1421
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public contentCategoryProductList2Notc(Ljava/lang/String;Ljava/lang/String;IIILcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1427
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "contentCategoryProductList2Notc"

    const-string v3, "2031"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1433
    const-string v1, "contentCategoryID"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1435
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Ljava/lang/String;)V

    .line 1436
    invoke-static {v0, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;II)V

    .line 1437
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1438
    invoke-static {v0, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setStatus(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;I)V

    .line 1440
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1441
    invoke-virtual {p0, v0, v1, p7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public countrySearch(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 126
    if-eqz p1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->countrySearchEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;

    move-result-object v0

    .line 133
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryParser;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V

    .line 134
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;->countrySearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CountryRequestXML;

    move-result-object v0

    goto :goto_0
.end method

.method public countrySearchEx(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/a;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "countrySearchEx"

    const-string v3, "2300"

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v1, "latestCountryCode"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v1, "whoAmI"

    const-string v2, "odc"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 150
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public createWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;->createWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;

    move-result-object v0

    .line 1448
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1449
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1450
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1451
    return-object v0
.end method

.method public createWishList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2104
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "createWishList"

    const-string v3, "4100"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2106
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2107
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2108
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public creditCardRegister(Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1522
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/CreditCard;I)V

    .line 1524
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;)V

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;)V

    .line 1533
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1534
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1535
    return-object v0
.end method

.method public creditCardRegister(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1541
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;I)V

    .line 1543
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1544
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1545
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1546
    return-object v0
.end method

.method public creditCardRegisterForUSA(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1552
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IRegisterCardParam;I)V

    .line 1555
    const-string v1, "state"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;->getState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    const-string v1, "city"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    const-string v1, "street"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;->getStreet()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1558
    const-string v1, "zip"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;->getZip()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    const-string v1, "firstName"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;->getFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    const-string v1, "lastName"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/IRegisterCardParamForUSA;->getLastName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CreditCardRegisterRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1562
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1563
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1564
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1565
    return-object v0
.end method

.method public curatedInterimProductSummary(IILcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1346
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "curatedInterimProductSummary"

    const-string v3, "2993"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1349
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1350
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;II)V

    .line 1351
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1353
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;)V

    .line 1355
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public curatedProductList(IILjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1383
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "curatedProductList"

    const-string v3, "2935"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1386
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1387
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;II)V

    .line 1388
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1389
    const-string v1, "index"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1393
    invoke-virtual {p0, v0, v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public curatedProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1398
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "curatedProductList"

    const-string v4, "2935"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1400
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1401
    const-string v1, "index"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1404
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public curatedProductSetList2Notc(IILjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2668
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "curatedProductSetList2Notc"

    const-string v3, "2247"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2671
    const-string v1, "productSetID"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2673
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 2674
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;II)V

    .line 2675
    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Ljava/lang/String;)V

    .line 2676
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 2678
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2683
    invoke-virtual {p0, v0, v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public curatedProductSummary(IILcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1368
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "curatedProductSummary"

    const-string v3, "2999"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1371
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1372
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;II)V

    .line 1373
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/ListXmlHelper;->setContentType(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;)V

    .line 1375
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;)V

    .line 1377
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public customerBoughtProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 798
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerAlsoBoughtRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)V

    .line 801
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 803
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public customerCouponDetail(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 682
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;I)V

    .line 684
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V

    .line 686
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public customerCouponList(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 595
    if-nez p1, :cond_0

    .line 596
    const/4 v0, 0x0

    .line 603
    :goto_0
    return-object v0

    .line 598
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->clear()V

    .line 599
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 600
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;)V

    .line 601
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    goto :goto_0
.end method

.method public customerCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 608
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 609
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 611
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 612
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 613
    return-object v0
.end method

.method public customerCouponRegister(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponRegisterParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1484
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponRegisterParam;->getCouponCode()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;->register(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;

    move-result-object v0

    .line 1486
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1487
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1488
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1489
    return-object v0
.end method

.method public customerCouponRegister(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;->register(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDCouponRequestXML;

    move-result-object v0

    .line 1475
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1476
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1477
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1478
    return-object v0
.end method

.method public cyberCashPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1658
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CyberCachPurchaseRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;I)V

    .line 1660
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1662
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1663
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1664
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1665
    return-object v0
.end method

.method public deleteGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2297
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "deleteGiftCard"

    const-string v3, "3302"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2299
    const-string v1, "giftCardCode"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2300
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2301
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1455
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;->deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;

    move-result-object v0

    .line 1457
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1458
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1459
    return-object v0
.end method

.method public deleteWishList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;->deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/CRUDWishListRequestXML;

    move-result-object v0

    .line 1466
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1467
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1468
    return-object v0
.end method

.method public deliveryUrlOptBillingPurchase(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;
    .locals 2

    .prologue
    .line 1707
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;

    invoke-direct {v0, p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 1708
    invoke-virtual {v0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 1709
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestRestUrl;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1710
    return-object v0
.end method

.method public deregisterPushNotiDevice(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2162
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "deregisterPushNotiDevice"

    const-string v3, "6016"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2164
    const-string v1, "regId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2166
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public deregisterWebOTAService(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1815
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->deregister(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;

    move-result-object v0

    .line 1816
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1817
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1041
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "download"

    const-string v3, "3040"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1043
    const-string v1, "orderID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    const-string v1, "orderItemSEQ"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    const-string v1, "productID"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    const-string v1, "trialClsf"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    invoke-direct {p0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1050
    const-string v1, "versionCode"

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1054
    invoke-virtual {p0, v0, v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1056
    return-object v0
.end method

.method public downloadClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2490
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "downloadClickLog"

    const-string v3, "2322"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2492
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2493
    const-string v1, "linkProductStore"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->linkProductStore:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2494
    const-string v1, "categoryID"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495
    const-string v1, "categoryName"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496
    const-string v1, "categoryID2"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryID2:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2497
    const-string v1, "categoryName2"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->categoryName2:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498
    const-string v1, "productID"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2499
    const-string v1, "productName"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->productName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2500
    const-string v1, "sellingPrice"

    iget-wide v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->sellingPrice:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->isDiscount()Z

    move-result v1

    if-ne v1, v5, :cond_0

    .line 2502
    const-string v1, "reducePrice"

    iget-wide v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;->reducePrice:D

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2508
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2509
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 2505
    :cond_0
    const-string v1, "reducePrice"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public downloadEx(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1084
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "downloadEx"

    const-string v3, "2303"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1086
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1088
    const-string v2, "dowloadType"

    if-eqz p3, :cond_2

    const-string v1, "update"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getSignIDFromGUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1091
    if-eqz v1, :cond_0

    .line 1092
    const-string v2, "signID"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    :cond_0
    invoke-direct {p0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1096
    const-string v1, "versionCode"

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1100
    invoke-virtual {p0, v0, v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1102
    return-object v0

    .line 1088
    :cond_2
    const-string v1, "new"

    goto :goto_0
.end method

.method public downloadEx2(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1874
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "downloadEx2"

    const-string v3, "2311"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1876
    const-string v1, "GUID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1878
    const-string v1, "dowloadType"

    const-string v2, "new"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getSignIDFromGUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1881
    if-eqz v1, :cond_0

    .line 1882
    const-string v2, "signID"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1885
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    const-string v2, "0"

    invoke-direct {v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1887
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1889
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCodeString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1894
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1895
    const-string v2, "versionCode"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1901
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1902
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1903
    return-object v0

    .line 1891
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public downloadForRestore(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2627
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "downloadForRestore"

    const-string v3, "2316"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2630
    const-string v2, "new"

    .line 2632
    const-string v1, "0"

    .line 2634
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2635
    const-string v1, "1"

    .line 2637
    :cond_0
    const-string v3, "GUID"

    invoke-virtual {v0, v3, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2638
    const-string v3, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2639
    const-string v3, "downloadType"

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2640
    const-string v2, "predeployed"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getSignIDFromGUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2643
    if-eqz v1, :cond_1

    .line 2644
    const-string v2, "signID"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2647
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    const-string v2, "0"

    invoke-direct {v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2649
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 2651
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCodeString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2656
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2657
    const-string v2, "versionCode"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2660
    :cond_2
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2661
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2662
    return-object v0

    .line 2653
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public easybuyPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 966
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-direct {v1, v0, p1, v2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;ZI)V

    .line 969
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    const-string v2, "0"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 973
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCodeString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 978
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 979
    const-string v2, "versionCode"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 983
    invoke-virtual {p0, v1, v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 985
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 986
    return-object v0

    .line 975
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public easybuyPurchase(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 991
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "EasybuyPurchase"

    const-string v3, "3010"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 993
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    const-string v1, "couponIssuedSEQ"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const-string v1, "guid"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v1, "paymentAmountPrice"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    const-string v1, "testPurchaseYn"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1006
    const-string v1, "userID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    const-string v2, "0"

    invoke-direct {v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1014
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCodeString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1019
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->isValidString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1020
    const-string v2, "versionCode"

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1025
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1027
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1028
    return-object v0

    .line 1016
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public featuredProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1276
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "featuredProductList2Notc"

    const-string v4, "2235"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1278
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1279
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1281
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public flexibleCategoryList(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2052
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    .line 2053
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "flexibleCategoryList"

    const-string v3, "2011"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2055
    const-string v1, "imgWidth"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2057
    const-string v1, "imgHeight"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2061
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public freeProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1316
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "freeProductList2Notc"

    const-string v4, "2034"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1318
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1319
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1321
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public getCreditCardInfo(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2392
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getCreditCardInfo"

    const-string v3, "3011"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2394
    const-string v1, "userID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2396
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadList(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 936
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getDownloadList"

    const-string v3, "2309"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 938
    const-string v1, "startNum"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    const-string v1, "endNum"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 949
    const-string v1, "contentType"

    const-string v2, "all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    const-string v1, "preloadCount"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    const-string v1, "postloadCount"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    const-string v1, "preloadApp"

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const-string v1, "postloadApp"

    invoke-virtual {v0, v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 959
    invoke-virtual {p0, v0, v1, p7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 960
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 961
    return-object v0
.end method

.method public getDownloadList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 923
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetDownloadListRequestXML;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;I)V

    .line 925
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 928
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 929
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 930
    return-object v0
.end method

.method public getEmergencyDownloadList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2586
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getEmergencyDownloadList"

    const-string v3, "2315"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2588
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2589
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2590
    const-string v2, "predeployed"

    const-string v1, "1"

    move-object v3, v0

    .line 2592
    :goto_0
    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2596
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2598
    return-object v0

    .line 2592
    :cond_0
    const-string v2, "predeployed"

    if-eqz p2, :cond_1

    const-string v1, "1"

    move-object v3, v0

    goto :goto_0

    :cond_1
    const-string v1, "0"

    move-object v3, v0

    goto :goto_0
.end method

.method public getKnoxMainHome(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2617
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getKnoxMainHome"

    const-string v3, "2345"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2619
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2620
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2622
    return-object v0
.end method

.method public getNotification(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1172
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getNotification"

    const-string v3, "2340"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1174
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;)V

    .line 1175
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1176
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 1177
    return-object v0
.end method

.method public getPriceWithTax(DLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2523
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getPriceWithTax"

    const-string v3, "2083"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2525
    const-string v1, "sellingPrice"

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2526
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2527
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2528
    return-object v0
.end method

.method public getProductSetList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 816
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/GetProductSetListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;I)V

    .line 819
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->getLinkedContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 822
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public getProductSetList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1334
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "getProductSetList2Notc"

    const-string v4, "2238"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1336
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1337
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 1338
    const-string v1, "productSetID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getProductSetListID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1339
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1341
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public getStateList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2514
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getStateList"

    const-string v3, "3012"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2516
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2517
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2518
    return-object v0
.end method

.method public getType6Notification(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1190
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "getNotification"

    const-string v3, "2340"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1192
    const-string v1, "notificationType"

    const-string v2, "06"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NotificationParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;)V

    .line 1194
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1195
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 1196
    return-object v0
.end method

.method public giftCardDetail(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2317
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "giftCardDetail"

    const-string v3, "3304"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2319
    const-string v1, "giftCardCode"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2320
    const-string v1, "imageType"

    const-string v2, "fHD"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2321
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2322
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public giftCardList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2306
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "giftCardList"

    const-string v3, "3303"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2308
    const-string v1, "giftCardStatusCode"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2309
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2310
    const-string v1, "imageType"

    const-string v2, "fHD"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2311
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2312
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public giftCardPurchase(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2327
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "giftCardPurchase"

    const-string v3, "3305"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2329
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2331
    const-string v1, "giftCardCode"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2332
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2333
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public guidProductDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 731
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->guidProductDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    move-result-object v0

    .line 733
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 734
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public guidProductDetailEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->guidProductDetailEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    move-result-object v0

    .line 750
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 751
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public guidProductDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 2351
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->guidProductDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    move-result-object v0

    .line 2353
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2354
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2355
    return-object v0
.end method

.method public guidProductDetailOverview(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2226
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "guidProductDetailExOverview"

    const-string v3, "2291"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2228
    const-string v1, "GUID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2229
    const-string v1, "screenImgWidth"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2233
    const-string v1, "screenImgHeight"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2237
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2238
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public guidProductDetailRelated(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2261
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "guidProductDetailExRelated"

    const-string v3, "2292"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2263
    const-string v1, "GUID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2264
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2265
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public initAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 2132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->initAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;

    move-result-object v0

    .line 2134
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2135
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2136
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 2137
    return-object v0
.end method

.method public initInicisMobilePurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1590
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/InitInicisRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;I)V

    .line 1592
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1593
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public initPayment(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2533
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "initPayment"

    const-string v3, "3101"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2535
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2536
    const-string v1, "userID"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2538
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2539
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2540
    return-object v0
.end method

.method public initiateOptBillingPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1683
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->init(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;

    move-result-object v0

    .line 1685
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1687
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1688
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1689
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1690
    return-object v0
.end method

.method public joinRegister(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1248
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;I)V

    .line 1249
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1250
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1251
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1252
    return-object v0
.end method

.method public joinRegisterOld(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1257
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/JoinRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;I)V

    .line 1258
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1259
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1260
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1261
    return-object v0
.end method

.method public linkShopFreeOrder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2564
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "LinkShopFreeOrder"

    const-string v3, "3013"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2566
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2567
    const-string v1, "guid"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2568
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569
    const-string v1, "userID"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570
    const-string v1, "linkShopType"

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2578
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2579
    invoke-virtual {p0, v0, v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2581
    return-object v0
.end method

.method public logOut(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 237
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 238
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutResponseParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutResponseParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    .line 239
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 241
    return-object v0
.end method

.method public login(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 180
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;I)V

    .line 181
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginResponseParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginResponseParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    .line 183
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getNetError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorChecker()Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

    move-result-object v1

    const-string v2, "3010"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;->addSuccessErrorCode(Ljava/lang/String;)V

    .line 186
    if-nez p2, :cond_0

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 190
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->suppressReceivedLog()V

    .line 191
    return-object v0
.end method

.method public login(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LoginRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;I)V

    .line 197
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 199
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getNetError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorChecker()Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

    move-result-object v1

    const-string v2, "3010"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;->addSuccessErrorCode(Ljava/lang/String;)V

    .line 202
    if-nez p2, :cond_0

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 206
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->suppressReceivedLog()V

    .line 207
    return-object v0
.end method

.method public loginEx(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginExParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2183
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "loginEx"

    const-string v3, "5011"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2185
    const-string v1, "token"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginExParam;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2186
    const-string v1, "accountURL"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginExParam;->getAccountURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2187
    const-string v1, "reqCardInfoYn"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2188
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2189
    const-string v2, "unifiedPaymentYn"

    if-eqz p3, :cond_0

    const-string v1, "Y"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2190
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2191
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 2189
    :cond_0
    const-string v1, "N"

    goto :goto_0
.end method

.method public mODeliveryResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->modelivery(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;ZLcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    move-result-object v0

    .line 1784
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1786
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1788
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1789
    return-object v0
.end method

.method public mODeliveryResult(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1794
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;->modelivery(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSMoParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSPurchaseXML;

    move-result-object v0

    .line 1796
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1798
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1800
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1801
    return-object v0
.end method

.method public microPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1570
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/MicroPaymentRequest;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/danal/IDanalRequestParam;I)V

    .line 1572
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1573
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1574
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1575
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1576
    return-object v0
.end method

.method public nameAuthSetting(Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1221
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/RealAgeVerificationInfo;I)V

    .line 1223
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationParser;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RealAgeVerificationParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V

    .line 1225
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1226
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1227
    return-object v0
.end method

.method public newProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1296
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "newProductList2Notc"

    const-string v4, "2033"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1298
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1299
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1301
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public noticeDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 2218
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;I)V

    .line 2220
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V

    .line 2221
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public noticeList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 2203
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;I)V

    .line 2205
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V

    .line 2206
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public paidProductList2Notc(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1306
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "paidProductList2Notc"

    const-string v4, "2035"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1308
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1309
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1311
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method protected post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->_RequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mErrorPreprocessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;->createRequest(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1234
    return-object v0
.end method

.method public productDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 764
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->productDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    move-result-object v0

    .line 766
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 767
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public productDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 783
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->productDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    move-result-object v0

    .line 785
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 786
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public productDetailOverview(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2243
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "productDetailOverview"

    const-string v3, "2281"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2245
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2246
    const-string v1, "orderID"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2247
    const-string v1, "screenImgWidth"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251
    const-string v1, "screenImgHeight"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2256
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public productDetailRelated(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2270
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "productDetailRelated"

    const-string v3, "2282"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2272
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2273
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2274
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public productLikeUnlikeRegister(Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2339
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "productLikeUnlikeRegister"

    const-string v3, "4010"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2341
    const-string v1, "productID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2342
    const-string v2, "likeYn"

    if-eqz p2, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2343
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2344
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2345
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 2346
    return-object v0

    .line 2342
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method

.method public promotionBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 668
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 669
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;)V

    .line 670
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public purchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 713
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;)V

    .line 715
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 718
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public purchaseHistHide(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 1

    .prologue
    .line 2360
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2361
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 2362
    return-object v0
.end method

.method public purchaseList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 1326
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)V

    .line 1327
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1329
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public purchaseList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, p2, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->purchaseList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;

    move-result-object v0

    .line 407
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V

    .line 409
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public purchaseListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p1

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->purchaseListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;ILjava/lang/String;ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;

    move-result-object v0

    .line 423
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V

    .line 425
    invoke-virtual {p0, v0, v1, p7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public ratingAuthority(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 164
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RatingAuthorityRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RatingAuthorityRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)V

    .line 166
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 167
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public registerGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2279
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "registerGiftCard"

    const-string v3, "3301"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2281
    const-string v1, "giftCardCode"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    .line 2285
    if-eqz v1, :cond_0

    .line 2286
    const-string v1, "upBillingYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2292
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 2288
    :cond_0
    const-string v1, "upBillingYN"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerPrepaidCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1984
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "registerPrepaidCard"

    const-string v3, "3120"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1986
    const-string v1, "cardNumber"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;->getCardNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1987
    const-string v1, "cardPassword"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;->getCardPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1989
    const-string v1, "cardValue"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;->getCardValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1990
    const-string v1, "cardCorpSEQ"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;->getCardCorpSeq()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1992
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1993
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1994
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1995
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setDisableBackgroundRetry()V

    .line 1996
    return-object v0
.end method

.method public registerPushNotiDevice(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2152
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "registerPushNotiDevice"

    const-string v3, "6015"

    const/4 v6, 0x1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2154
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155
    const-string v1, "regId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2156
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2157
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public registerWebOTAService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    .line 1807
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->register(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;

    move-result-object v0

    .line 1809
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1810
    invoke-virtual {p0, v0, v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public reportAppDefect(Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1502
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;I)V

    .line 1504
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1505
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1506
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1507
    return-object v0
.end method

.method public reportAppDefect(Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML$IReportProblemInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1512
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML$IReportProblemInfo;I)V

    .line 1514
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 1515
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1516
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1517
    return-object v0
.end method

.method public request(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 1

    .prologue
    .line 430
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 431
    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public resetEasybuySetting(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1598
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "resetEasybuySetting"

    const-string v3, "5092"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1600
    const-string v1, "userID"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1601
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;)V

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser$SingleResponseSuccessJob;)V

    .line 1611
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1612
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 1613
    return-object v0
.end method

.method public screenShot(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 877
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V

    .line 880
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 882
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public search(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 220
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)V

    .line 222
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;

    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;->getSearchContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 225
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public searchCard(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 618
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CardSortRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CardSortRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 619
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 620
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 621
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V

    .line 622
    return-object v0
.end method

.method public sellerDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 3

    .prologue
    .line 1205
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SellerDetailRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SellerDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;I)V

    .line 1207
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SellerDetailParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SellerDetailParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;)V

    .line 1208
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public sellerProductList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 1139
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SellerProductListRequestXML;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SellerProductListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;I)V

    .line 1141
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Seller;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1143
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public sellerProductList(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1148
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "sellerProductList2Notc"

    const-string v3, "2310"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1150
    const-string v1, "imgWidth"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const-string v1, "imgHeight"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    const-string v1, "startNum"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getNextStartNumber()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    const-string v1, "endNum"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getNextEndNumber()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    const-string v1, "contentType"

    const-string v2, "all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const-string v1, "sellerId"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1159
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public shareClickLog(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2428
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "shareClickLog"

    const-string v3, "2904"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2430
    const-string v1, "productId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2432
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SingleResponseParser;-><init>()V

    .line 2433
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public subCategoryList(Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 517
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "categoryList"

    const-string v3, "2010"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 520
    const-string v1, "upLevelCategoryID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v1, "categoryLevel"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const-string v1, "categorySortString"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const-string v1, "startNum"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const-string v1, "endNum"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 536
    const-string v1, "categoryRepImgYn"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    .line 547
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 548
    invoke-virtual {p0, v0, v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0

    .line 541
    :cond_0
    const-string v1, "categoryRepImgYn"

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public termInformation(Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 2

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;->help(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationRequestXML;

    move-result-object v0

    .line 1127
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;)V

    .line 1128
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public termInformation_TNC(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1115
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "termInformation"

    const-string v3, "5060"

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1117
    const-string v1, "flag"

    const-string v2, "4"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    const-string v1, "fileFlag"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1120
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public updateCheck(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1822
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "updateCheck"

    const-string v3, "2346"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1824
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1825
    const-string v1, "applist"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->getRequestString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1826
    if-eqz p2, :cond_0

    .line 1827
    const-string v1, "versionCodeList"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/d;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;)V

    .line 1850
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1851
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 1852
    return-object v0
.end method

.method public updateCheck(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1857
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "updateCheck"

    const-string v3, "2346"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1859
    const-string v1, "imei"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1860
    const-string v1, "applist"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1861
    if-eqz p2, :cond_0

    .line 1862
    const-string v1, "versionCodeList"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1865
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 1867
    invoke-virtual {p0, v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 1868
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V

    .line 1869
    return-object v0
.end method

.method public upgradeListEx(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;IILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 2368
    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;

    invoke-direct {v7, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;-><init>(Landroid/content/Context;)V

    .line 2369
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "upgradeListEx"

    const-string v3, "2302"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2371
    const-string v1, "startNum"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    .line 2372
    const-string v1, "endNum"

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    .line 2373
    const-string v1, "imgWidth"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2375
    const-string v1, "imgHeight"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->toStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2377
    const-string v1, "imei"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    const-string v1, "contentType"

    const-string v2, "all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    const-string v1, "alignOrder"

    const-string v2, "update"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2381
    const-string v1, "preloadCount"

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->getPreLoadCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    .line 2382
    const-string v1, "postloadCount"

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->getPostLoadCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;I)V

    .line 2383
    const-string v1, "preloadApp"

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->getPreLoadApp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384
    const-string v1, "postloadApp"

    invoke-virtual {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/util/PrePostUtil;->getPostLoadApp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2386
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2387
    invoke-virtual {p0, v0, v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public upgradeListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    .line 2067
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p1

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->upgradeListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;ILjava/lang/String;ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;

    move-result-object v0

    .line 2070
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V

    .line 2072
    invoke-virtual {p0, v0, v1, p7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public verificationAuthority(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2077
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v2, "verificationAuthority"

    const-string v3, "2233"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2079
    const-string v1, "password"

    invoke-virtual {v0, v1, p1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2082
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isCountryListSearchMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2083
    const-string v1, "type"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 2086
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 2087
    invoke-virtual {p0, v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method

.method public wishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1266
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    const-string v3, "wishList"

    const-string v4, "4090"

    move-object v2, p1

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 1268
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;->addImageSizeContentTypeListCountAsParams(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 1269
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/WishListParser;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->mDocument:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WishListParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;)V

    .line 1271
    invoke-virtual {p0, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->post(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    return-object v0
.end method
