.class final Lcom/sec/android/app/samsungapps/widget/banner/q;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/q;->a:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    if-eqz p2, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/q;->a:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;Z)Z

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/q;->a:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->updateWidget()V

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SmallBannerWidget :: load() = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 121
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/q;->a:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->a(Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;Z)Z

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/q;->a:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setVisibleRetry(I)Z

    .line 118
    const-string v0, "SmallBannerWidget :: load() Fail!!!! "

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
