.class public final Lcom/sec/android/app/samsungapps/R$styleable;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final BigBannerAttribute:[I

.field public static final BigBannerAttribute_is_full_banner:I = 0x0

.field public static final CheckBoxImage:[I

.field public static final CheckBoxImage_is_checkbox_image:I = 0x0

.field public static final CheckBoxImage_is_checkbox_image_focus:I = 0x1

.field public static final WebImageView:[I

.field public static final WebImageView_is_webimage_background:I = 0x0

.field public static final WebImageView_is_webimage_big_banner_item:I = 0x1

.field public static final isa_ChartBannerCustomTextStyle:[I

.field public static final isa_ChartBannerCustomTextStyle_textStroke:I = 0x0

.field public static final isa_ChartBannerCustomTextStyle_textStrokeColor:I = 0x2

.field public static final isa_ChartBannerCustomTextStyle_textStrokeWidth:I = 0x1

.field public static final isa_ViewpagerTabStyle:[I

.field public static final isa_ViewpagerTabStyle_indicatorColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11222
    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->BigBannerAttribute:[I

    .line 11251
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->CheckBoxImage:[I

    .line 11294
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->WebImageView:[I

    .line 11339
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->isa_ChartBannerCustomTextStyle:[I

    .line 11395
    new-array v0, v3, [I

    const v1, 0x7f010005

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->isa_ViewpagerTabStyle:[I

    return-void

    .line 11251
    :array_0
    .array-data 4
        0x7f010003
        0x7f010004
    .end array-data

    .line 11294
    :array_1
    .array-data 4
        0x7f010000
        0x7f010001
    .end array-data

    .line 11339
    :array_2
    .array-data 4
        0x7f010006
        0x7f010007
        0x7f010008
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
