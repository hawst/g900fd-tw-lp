.class public Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ExcludeProductID:Ljava/lang/String;

.field protected _IGetSellerProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;

.field _SellerProductList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_SellerProductList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_IGetSellerProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_ExcludeProductID:Ljava/lang/String;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_ExcludeProductID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 5

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_IGetSellerProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->onFinalResult(Z)V

    .line 72
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_IGetSellerProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVSellerId()Ljava/lang/String;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;-><init>()V

    .line 41
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_SellerProductList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;)V

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->sellerProductList(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 69
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method
