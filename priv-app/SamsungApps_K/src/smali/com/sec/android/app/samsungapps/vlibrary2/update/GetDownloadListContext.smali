.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;


# instance fields
.field private _Context:Landroid/content/Context;

.field protected _GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

.field _Receivers:Ljava/util/ArrayList;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Receivers:Ljava/util/ArrayList;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Context:Landroid/content/Context;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    .line 35
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->addSubscriber(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V

    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;

    .line 44
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;

    return-object v0
.end method

.method private notifySigFailed()V
    .locals 3

    .prologue
    .line 98
    monitor-enter p0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Receivers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 102
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 104
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Receivers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 105
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private notifySigSuccess()V
    .locals 3

    .prologue
    .line 87
    monitor-enter p0

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Receivers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 91
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 93
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Receivers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 94
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private onCheckLoginState()V
    .locals 1

    .prologue
    .line 141
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->RESULT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->RESULT_NOTLOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V

    goto :goto_0
.end method

.method private requestLogin()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)Z

    .line 176
    return-void
.end method

.method private updateUpateCountAndSigSuccess()V
    .locals 2

    .prologue
    .line 136
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyGetUpdatableAppCount(I)V

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->notifySigSuccess()V

    .line 138
    return-void
.end method


# virtual methods
.method public check(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 154
    monitor-enter p0

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Receivers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V

    .line 159
    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMinCountToRequestPurchaseItemList()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->getMinCountToRequestPurchaseItemList()I

    move-result v0

    return v0
.end method

.method protected getParams(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;-><init>()V

    .line 198
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->create(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;)V
    .locals 2

    .prologue
    .line 163
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/b;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 167
    :goto_0
    return-void

    .line 166
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;)V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    :goto_0
    return-void

    .line 62
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->updateUpateCountAndSigSuccess()V

    goto :goto_0

    .line 65
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->notifySigFailed()V

    goto :goto_0

    .line 68
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->notifySigSuccess()V

    goto :goto_0

    .line 71
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->sendRequestGetDownloadList()V

    goto :goto_0

    .line 74
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->onCheckLoginState()V

    goto :goto_0

    .line 77
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->requestLogin()V

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;)V

    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 180
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->removeSubscriber(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;)Z

    .line 181
    return-void
.end method

.method protected sendRequestGetDownloadList()V
    .locals 8

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Context:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->getParams(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    move-result-object v5

    .line 111
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Context:Landroid/content/Context;

    iget v2, v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadCount:I

    iget v3, v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadCount:I

    iget-object v4, v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    iget-object v5, v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_GetDownloadListResult:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/a;

    invoke-direct {v7, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;)V

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getDownloadList(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 132
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 133
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    .line 50
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;)V

    return-void
.end method
