.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    return-object v0
.end method


# virtual methods
.method public getCardBalanceInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getCardBalance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    move-result-object v0

    return-object v0
.end method

.method public getExtraObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getExtraObject()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getConfirmViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public onAgree(Z)V
    .locals 1

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->requestPurchase(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method protected requestPurchase(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getFinalMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->PrepaidCardPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 75
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 76
    return-void
.end method
