.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract autoLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract backgroundLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract clearLoginSession()V
.end method

.method public abstract clearPasswordConfirmedTime()V
.end method

.method public abstract createChangeCreditCardCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createForceLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createRegisterCreditCard(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createValidateConfirmPassword()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getAccountTokenURL()Ljava/lang/String;
.end method

.method public abstract getToken()Ljava/lang/String;
.end method

.method public abstract hasToken()Z
.end method

.method public abstract isLogedIn()Z
.end method

.method public abstract isPasswordConfirmed()Z
.end method

.method public abstract isPopupConfirmPWSamsungAccountInstalled()Z
.end method

.method public abstract isSamsungAccountDisabled()Z
.end method

.method public abstract isSamsungAccountInstalled()Z
.end method

.method public abstract isTokenBasedSamsungAccountInstalled()Z
.end method

.method public abstract isTokenExpired()Z
.end method

.method public abstract loginODCWithOldAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract loginODCWithToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract loginSamsungAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract logout()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract normalLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract setPasswordConfirmedTime()V
.end method

.method public abstract setTokenExpired()V
.end method

.method public abstract setTokenResult(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract tokenBasedLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
