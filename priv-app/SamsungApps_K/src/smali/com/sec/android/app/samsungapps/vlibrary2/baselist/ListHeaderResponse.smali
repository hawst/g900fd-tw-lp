.class public Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x7098aa1aaa50a6edL


# instance fields
.field private _EndNumber:I

.field private _EndOfList:Z

.field private _EndOfListValid:Z

.field private _StartNumber:I

.field private _TotalCount:I

.field private _TotalCount2:I

.field private _TotalCount2Valid:Z

.field private _TotalCountValid:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->setHeaderResponse(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 24
    return-void
.end method


# virtual methods
.method public append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)V
    .locals 1

    .prologue
    .line 103
    iget v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    .line 104
    iget v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    .line 105
    iget-boolean v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfList:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfList:Z

    .line 106
    iget-boolean v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCountValid:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCountValid:Z

    .line 107
    iget-boolean v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfListValid:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfListValid:Z

    .line 108
    iget v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2:I

    .line 109
    iget-boolean v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2Valid:Z

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2Valid:Z

    .line 111
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 130
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_StartNumber:I

    .line 131
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    .line 132
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    .line 133
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2:I

    .line 134
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfList:Z

    .line 135
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCountValid:Z

    .line 136
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfListValid:Z

    .line 137
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2Valid:Z

    .line 138
    return-void
.end method

.method public getEndNumber()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    return v0
.end method

.method public getStartNumber()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_StartNumber:I

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    return v0
.end method

.method public getTotalCount2()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2:I

    return v0
.end method

.method public isEof()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 83
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfListValid:Z

    if-eqz v1, :cond_1

    .line 84
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfList:Z

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 86
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCountValid:Z

    if-eqz v1, :cond_0

    .line 87
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isIntersect(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getStartNumber()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getEndNumber()I

    move-result v2

    if-nez v2, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getStartNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getStartNumber()I

    move-result v3

    if-gt v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getEndNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getStartNumber()I

    move-result v3

    if-lt v2, v3, :cond_2

    move v0, v1

    .line 120
    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getStartNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getEndNumber()I

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getEndNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getEndNumber()I

    move-result v3

    if-lt v2, v3, :cond_0

    move v0, v1

    .line 124
    goto :goto_0
.end method

.method public isTotalCount2Valid()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2Valid:Z

    return v0
.end method

.method public isTotalCountValid()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCountValid:Z

    return v0
.end method

.method public setEndNumber(I)V
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    .line 94
    return-void
.end method

.method public setHeaderResponse(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 33
    const-string v0, "startNum"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_StartNumber:I

    .line 34
    const-string v0, "totalCount"

    invoke-virtual {p1, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    .line 35
    const-string v0, "endNum"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndNumber:I

    .line 36
    const-string v0, "endOfList"

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfListValid:Z

    .line 40
    :cond_0
    const-string v0, "endOfList"

    invoke-virtual {p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_EndOfList:Z

    .line 41
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount:I

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCountValid:Z

    .line 42
    const-string v0, "totalCount2"

    invoke-virtual {p1, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2:I

    .line 43
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2:I

    if-eq v0, v3, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->_TotalCount2Valid:Z

    .line 44
    return-void

    :cond_1
    move v0, v2

    .line 41
    goto :goto_0

    :cond_2
    move v1, v2

    .line 43
    goto :goto_1
.end method

.method public union(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method
