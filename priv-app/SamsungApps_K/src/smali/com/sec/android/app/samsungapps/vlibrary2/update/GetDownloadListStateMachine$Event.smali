.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum CACHE_EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum REQUEST_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum RESULT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

.field public static final enum RESULT_NOTLOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "LOGEDIN"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "CHECK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "REQUEST_FAILURE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->REQUEST_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "CACHE_EXPIRED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->CACHE_EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "RESULT_LOGEDIN"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->RESULT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "RESULT_NOTLOGEDIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->RESULT_NOTLOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "LOGIN_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "LOGIN_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    const-string v1, "REQUEST_SUCCESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    .line 19
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->REQUEST_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->CACHE_EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->RESULT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->RESULT_NOTLOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    return-object v0
.end method
