.class public Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

.field b:Ljava/util/ArrayList;

.field c:Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

.field d:Landroid/widget/ListView;

.field e:Lcom/sec/android/app/samsungapps/du;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->c:Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    .line 375
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V
    .locals 2

    .prologue
    .line 32
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x24

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected deleteAllExpiredGiftCards()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/du;->notifyDataSetChanged()V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->loadData()V

    .line 250
    :goto_0
    return-void

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->c:Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;->deleteGiftCard(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 233
    new-instance v1, Lcom/sec/android/app/samsungapps/ds;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ds;-><init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method protected displayData()V
    .locals 1

    .prologue
    .line 94
    const v0, 0x7f040083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->setMainView(I)V

    .line 95
    const v0, 0x7f0c01e0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->d:Landroid/widget/ListView;

    .line 96
    return-void
.end method

.method public displaySoftkey(Z)V
    .locals 3

    .prologue
    const v1, 0x7f0c0169

    const/4 v2, 0x0

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 180
    :cond_0
    if-eqz p1, :cond_2

    .line 181
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 183
    const v0, 0x7f0c0096

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 184
    const v1, 0x7f08020d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 187
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 194
    :goto_1
    new-instance v1, Lcom/sec/android/app/samsungapps/dp;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/dp;-><init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 191
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 220
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected loadData()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->c:Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;->getExpiredGiftCardList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/sec/android/app/samsungapps/dn;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/dn;-><init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 91
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f0801d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0003

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->c:Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    .line 48
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    .line 58
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 59
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->displayData()V

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->loadData()V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->checkLoginState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    .line 75
    :cond_0
    return-void
.end method

.method protected setVisibleEmpty(I)Z
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 305
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 306
    if-nez v0, :cond_0

    .line 308
    const/4 v0, 0x0

    .line 318
    :goto_0
    return v0

    .line 311
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 312
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 313
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 314
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 316
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 317
    const v1, 0x7f0801d7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setVisibleLoading(I)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 323
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 324
    if-nez v0, :cond_0

    .line 334
    :goto_0
    return v1

    .line 329
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 331
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 332
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 333
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setVisibleRetry(I)Z
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 276
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 277
    if-nez v0, :cond_0

    .line 299
    :goto_0
    return v2

    .line 282
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 285
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 286
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 287
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 290
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 291
    new-instance v1, Lcom/sec/android/app/samsungapps/dt;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/dt;-><init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected updateData()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    if-nez v0, :cond_1

    .line 100
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->setVisibleEmpty(I)Z

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/du;->notifyDataSetChanged()V

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;->size()I

    move-result v2

    .line 116
    if-lez v2, :cond_4

    move v0, v1

    .line 117
    :goto_1
    if-ge v0, v2, :cond_3

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    invoke-interface {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 144
    :cond_3
    new-instance v0, Lcom/sec/android/app/samsungapps/du;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->b:Ljava/util/ArrayList;

    invoke-direct {v0, p0, p0, v2}, Lcom/sec/android/app/samsungapps/du;-><init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->d:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->d:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->e:Lcom/sec/android/app/samsungapps/du;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 148
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->setVisibleLoading(I)Z

    .line 150
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->displaySoftkey(Z)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/do;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/do;-><init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 162
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;->setVisibleEmpty(I)Z

    goto :goto_0
.end method
