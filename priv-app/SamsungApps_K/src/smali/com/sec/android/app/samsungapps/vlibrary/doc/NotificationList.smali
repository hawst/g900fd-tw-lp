.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x69093eb6f0417ba7L


# instance fields
.field mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 61
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList$NotificationListObserver;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method protected notifyLoadCompleted(Z)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList$NotificationListObserver;

    .line 47
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList$NotificationListObserver;->loadCompleted(Z)V

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList$NotificationListObserver;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public requestNotifications()V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ac;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ac;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getNotification(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 41
    return-void
.end method

.method public requestType6Notifications()V
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ab;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ab;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getType6Notification(Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 29
    return-void
.end method
