.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum AUSTRIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum BRAZIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum BULGARIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum CROATIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum CZECH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum DENMARK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum FINLAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum FRANCE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum GERMAN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum GREECE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum INDIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum IRAN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum IRELAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum ITALY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum LITHUANIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum NKOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum NORWAY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum POLAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum ROMANIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum RUSSIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum SINGAPORE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum SLOVAKIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum SWEDEN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum TEST_STORE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum TURKEY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum UK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum UKRAINE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

.field public static final enum USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;


# instance fields
.field _Code:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "KOREA"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "450"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 5
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "CHINA"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "460"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "CHINA2"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "461"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "USA"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "310"

    aput-object v3, v2, v5

    const-string v3, "311"

    aput-object v3, v2, v6

    const-string v3, "312"

    aput-object v3, v2, v7

    const-string v3, "313"

    aput-object v3, v2, v8

    const-string v3, "314"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "315"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "316"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "TURKEY"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "286"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v9, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->TURKEY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "UKRAINE"

    const/4 v2, 0x5

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "255"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->UKRAINE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "INDIA"

    const/4 v2, 0x6

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "404"

    aput-object v4, v3, v5

    const-string v4, "405"

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->INDIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "NKOREA"

    const/4 v2, 0x7

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "1000"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->NKOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "BRAZIL"

    const/16 v2, 0x8

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "724"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->BRAZIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "FRANCE"

    const/16 v2, 0x9

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "208"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->FRANCE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "GERMAN"

    const/16 v2, 0xa

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "262"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->GERMAN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "ITALY"

    const/16 v2, 0xb

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "222"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->ITALY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "SINGAPORE"

    const/16 v2, 0xc

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "525"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->SINGAPORE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "RUSSIA"

    const/16 v2, 0xd

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "250"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->RUSSIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "SWEDEN"

    const/16 v2, 0xe

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "240"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->SWEDEN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "AUSTRIA"

    const/16 v2, 0xf

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "232"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->AUSTRIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "POLAND"

    const/16 v2, 0x10

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "260"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->POLAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "CZECH"

    const/16 v2, 0x11

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "230"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CZECH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "SLOVAKIA"

    const/16 v2, 0x12

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "231"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->SLOVAKIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "UK"

    const/16 v2, 0x13

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "234"

    aput-object v4, v3, v5

    const-string v4, "235"

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->UK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "LITHUANIA"

    const/16 v2, 0x14

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "246"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->LITHUANIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "NORWAY"

    const/16 v2, 0x15

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "242"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->NORWAY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "FINLAND"

    const/16 v2, 0x16

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "244"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->FINLAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "DENMARK"

    const/16 v2, 0x17

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "238"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->DENMARK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "GREECE"

    const/16 v2, 0x18

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "202"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->GREECE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "IRELAND"

    const/16 v2, 0x19

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "272"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->IRELAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "ROMANIA"

    const/16 v2, 0x1a

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "226"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->ROMANIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "CROATIA"

    const/16 v2, 0x1b

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "219"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CROATIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "BULGARIA"

    const/16 v2, 0x1c

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "284"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->BULGARIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "IRAN"

    const/16 v2, 0x1d

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "432"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->IRAN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    const-string v1, "TEST_STORE"

    const/16 v2, 0x1e

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "1000"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->TEST_STORE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    .line 3
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->TURKEY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->UKRAINE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->INDIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->NKOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->BRAZIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->FRANCE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->GERMAN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->ITALY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->SINGAPORE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->RUSSIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->SWEDEN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->AUSTRIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->POLAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CZECH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->SLOVAKIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->UK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->LITHUANIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->NORWAY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->FINLAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->DENMARK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->GREECE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->IRELAND:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->ROMANIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CROATIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->BULGARIA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->IRAN:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->TEST_STORE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->_Code:[Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    return-object v0
.end method


# virtual methods
.method public final equalMCC(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 42
    if-nez p1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->_Code:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 47
    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 48
    const/4 v0, 0x1

    goto :goto_0

    .line 45
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final getCode()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->_Code:[Ljava/lang/String;

    return-object v0
.end method
