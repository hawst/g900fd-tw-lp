.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/f;
.super Lcom/samsung/android/aidl/ICheckAppInstallStateCallback$Stub;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/f;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;

    invoke-direct {p0}, Lcom/samsung/android/aidl/ICheckAppInstallStateCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final packageInstalled(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/f;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->f(Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>> CheckAppInstallStateCallback  packageInstalled() packageName:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   returnCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/g;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/f;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 355
    return-void
.end method
