.class public Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    .line 20
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    return-object v0
.end method


# virtual methods
.method public createFormatter(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;
    .locals 2

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;

    invoke-direct {v1, v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/ICurrencyFormatInfo;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
