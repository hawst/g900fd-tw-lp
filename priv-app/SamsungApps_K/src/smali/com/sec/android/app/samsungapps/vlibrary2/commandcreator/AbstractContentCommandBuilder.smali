.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected _Context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;->_Context:Landroid/content/Context;

    .line 16
    return-void
.end method


# virtual methods
.method public createReportProblem(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;-><init>()V

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;->loginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 22
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;->getReportProblemActivity()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 23
    return-object v0
.end method

.method public createReportProblem(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;-><init>()V

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;->loginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 30
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;

    invoke-direct {v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReportProblemCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 31
    return-object v0
.end method

.method protected abstract getReportProblemActivity()Ljava/lang/Class;
.end method

.method protected abstract loginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
