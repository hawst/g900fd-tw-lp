.class final Lcom/sec/android/app/samsungapps/widget/r;
.super Landroid/widget/FrameLayout;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1471
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    .line 1472
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1473
    return-void
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1498
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 1499
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 1501
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/r;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1502
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/r;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 1530
    :cond_0
    :goto_0
    return v0

    .line 1505
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/r;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/r;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1508
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    goto :goto_0

    .line 1511
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1513
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x52

    if-ne v1, v2, :cond_5

    .line 1518
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->f(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Z

    move-result v1

    if-ne v1, v0, :cond_4

    .line 1520
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    goto :goto_0

    .line 1524
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->g(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Z

    goto :goto_0

    .line 1530
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1536
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->h(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View$OnTouchListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->h(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View$OnTouchListener;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1537
    const/4 v0, 0x1

    .line 1549
    :goto_0
    return v0

    .line 1541
    :cond_0
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    .line 1544
    :catch_0
    move-exception v1

    const-string v1, "Android Framework adapter occured exception"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1547
    :catch_1
    move-exception v1

    .line 1548
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OptionMenuPopupWindow::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 1486
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->e(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1488
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1489
    invoke-static {}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->a()[I

    move-result-object v1

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    .line 1492
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1555
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 1556
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 1558
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/r;->getWidth()I

    move-result v3

    if-ge v1, v3, :cond_0

    if-ltz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/r;->getHeight()I

    move-result v1

    if-lt v2, v1, :cond_1

    .line 1560
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    .line 1566
    :goto_0
    return v0

    .line 1562
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 1563
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    goto :goto_0

    .line 1566
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 1477
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 1479
    if-nez p1, :cond_0

    .line 1480
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->dismiss()V

    .line 1482
    :cond_0
    return-void
.end method

.method public final sendAccessibilityEvent(I)V
    .locals 1

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1574
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/r;->a:Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;->i(Lcom/sec/android/app/samsungapps/widget/OptionMenuPopupWindow;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1578
    :goto_0
    return-void

    .line 1576
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method
