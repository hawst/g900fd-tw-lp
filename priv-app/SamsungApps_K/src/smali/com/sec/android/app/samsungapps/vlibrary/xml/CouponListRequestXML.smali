.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 9
    const-string v2, "customerCouponList2Notc"

    const-string v3, "2170"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->addCountParam()V

    .line 11
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 14
    const-string v0, "upBillingYN"

    const-string v1, "Y"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    :goto_0
    return-void

    .line 18
    :cond_0
    const-string v0, "upBillingYN"

    const-string v1, "N"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected addCountParam()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "startNum"

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const-string v0, "endNum"

    const/16 v1, 0x32

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-void
.end method
