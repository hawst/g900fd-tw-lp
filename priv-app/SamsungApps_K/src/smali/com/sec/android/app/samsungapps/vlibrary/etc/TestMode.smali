.class public Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final STR_COUNTRY_LIST_SEARCH_TEST_FILE:Ljava/lang/String;

.field private static final STR_PRE_DEPLOY_TEST_DIR:Ljava/lang/String; = "/sdcard"

.field private static final STR_PRE_DEPLOY_TEST_FILE:Ljava/lang/String;


# instance fields
.field private final TEST_MODE_COUNTRY_LIST:I

.field private final TEST_MODE_QA:I

.field private mTestMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "6c,74,64,79,74,64,66,73,69,77,79,72,6a,69,66,33,79,6a,78,74,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->STR_PRE_DEPLOY_TEST_FILE:Ljava/lang/String;

    .line 15
    const-string v0, "6c,74,64,79,74,64,67,6a,77,79,7a,69,66,33,79,6a,78,72,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->STR_COUNTRY_LIST_SEARCH_TEST_FILE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->TEST_MODE_QA:I

    .line 17
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->TEST_MODE_COUNTRY_LIST:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->mTestMode:I

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->checkTestMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->mTestMode:I

    .line 23
    return-void
.end method

.method private checkTestMode()I
    .locals 4

    .prologue
    .line 27
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->STR_PRE_DEPLOY_TEST_FILE:Ljava/lang/String;

    .line 29
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->STR_COUNTRY_LIST_SEARCH_TEST_FILE:Ljava/lang/String;

    .line 35
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 44
    :goto_0
    :try_start_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const/4 v0, 0x1

    .line 58
    :goto_1
    return v0

    .line 39
    :catch_0
    move-exception v0

    const-string v0, "/sdcard"

    goto :goto_0

    .line 49
    :cond_0
    :try_start_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    goto :goto_1

    :catch_1
    move-exception v0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public isCountryListSearchMode()Z
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->mTestMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTestMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 63
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->mTestMode:I

    if-ne v1, v0, :cond_0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
