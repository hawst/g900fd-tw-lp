.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->e:Z

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;

    .line 18
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;

    .line 19
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;

    .line 20
    return-void
.end method

.method private a()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->getInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 2

    .prologue
    .line 131
    const-string v0, "AutoUpdateTriggerManager"

    const-string v1, "setAlarmWithNewInterval"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->saveAndSetInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->registerAlarm(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 134
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;->onUpdateTime()V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->writeLastUpdateCheckedTimeNow()V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->e:Z

    .line 156
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    return-void
.end method

.method private b(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 2

    .prologue
    .line 143
    const-string v0, "AutoUpdateTriggerManager"

    const-string v1, "setAlarmWithChangedInterval"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->saveAndSetInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->registerAlarm(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 146
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)Z
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->existAlarm()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V
    .locals 2

    .prologue
    .line 7
    const-string v0, "AutoUpdateTriggerManager"

    const-string v1, "setAlarmWithDefaultInterval"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->getDefaultInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->saveAndSetInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->getDefaultInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->registerAlarm(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    return-void
.end method


# virtual methods
.method public check()V
    .locals 5

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->e:Z

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->e:Z

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->loadInterval()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;->isTimedOutByCompareLoadedIntervalAndLastUpdTime()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;->hasFakeInterval()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;->getFakeInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getSeconds()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getSeconds()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b()V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b()V

    goto :goto_0

    :cond_2
    const-string v0, "AutoUpdateTriggerManager"

    const-string v1, "onRequestIntervalForCheckChange"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;->requestInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V

    goto :goto_0

    .line 38
    :cond_3
    const-string v0, "AutoUpdateTriggerManager"

    const-string v1, "notifyNextTime"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;->onNoUpdateTime()V

    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->e:Z

    goto :goto_0

    .line 41
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;->hasFakeInterval()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;->getFakeInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b()V

    goto :goto_0

    :cond_6
    const-string v0, "AutoUpdateTriggerManager"

    const-string v1, "onRequestIntervalForNew"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;->requestInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V

    goto :goto_0
.end method
