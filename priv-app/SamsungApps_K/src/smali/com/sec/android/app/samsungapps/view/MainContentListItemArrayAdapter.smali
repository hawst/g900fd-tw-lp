.class public Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.source "ProGuard"


# instance fields
.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;II)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    .line 18
    iput p5, p0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->g:I

    .line 19
    return-void
.end method


# virtual methods
.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 10

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->mListType:I

    iget v9, p0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;->g:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;I)V

    return-object v0
.end method
