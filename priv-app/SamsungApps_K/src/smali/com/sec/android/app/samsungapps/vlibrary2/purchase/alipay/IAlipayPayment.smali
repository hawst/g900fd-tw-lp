.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private _IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field private bAlipayInstalled:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->isAlipayInstalled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private endLoading()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 39
    :cond_0
    return-void
.end method

.method private isAlipayInstalled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 58
    const-string v1, "com.alipay.android.app"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private needAlipayUpdate(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->needAlipayUpdate()Z

    move-result v1

    .line 47
    if-ne v1, v0, :cond_0

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAskAlipayInstall()V
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->bAlipayInstalled:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/m;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 215
    return-void
.end method

.method private onStartLoading()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->startLoading()V

    .line 180
    return-void
.end method

.method private onStopLoading()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->endLoading()V

    .line 176
    return-void
.end method

.method private requestComplete()V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 138
    return-void
.end method

.method private requestInit()V
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/k;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 172
    return-void
.end method

.method private requestInstallAlipay()V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->installAlipay()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 198
    return-void
.end method

.method private requestProcess()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->process()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 155
    return-void
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayPaymentStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)Z

    .line 64
    return-void
.end method

.method private startLoading()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 31
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    move-result-object v0

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->isAlipayInstalled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->bAlipayInstalled:Z

    .line 69
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->bAlipayInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_Context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->needAlipayUpdate(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->EXECUTE_ALIPAY_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)V

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->EXECUTE_ALIPAY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;)V

    goto :goto_0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;)V
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/n;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 121
    :goto_0
    return-void

    .line 94
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->onAskAlipayInstall()V

    goto :goto_0

    .line 97
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->requestComplete()V

    goto :goto_0

    .line 100
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->requestInit()V

    goto :goto_0

    .line 103
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->requestInstallAlipay()V

    goto :goto_0

    .line 106
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->requestProcess()V

    goto :goto_0

    .line 109
    :pswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->onFinalResult(Z)V

    goto :goto_0

    .line 112
    :pswitch_6
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->onFinalResult(Z)V

    goto :goto_0

    .line 115
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->onStartLoading()V

    goto :goto_0

    .line 118
    :pswitch_8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->onStopLoading()V

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 82
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;)V

    return-void
.end method
