.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field _DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

.field _Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private _screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

.field private bValid:Z

.field private contentGradeImgUrl:Ljava/lang/String;

.field private createDate:Ljava/lang/String;

.field private lastUpdateDate:Ljava/lang/String;

.field private nameAuthYN:Ljava/lang/String;

.field private productDescription:Ljava/lang/String;

.field private realContentsSize:J

.field private reportNum:Ljava/lang/String;

.field private representation:Ljava/lang/String;

.field private restrictedAge:Ljava/lang/String;

.field private screenShotCount:I

.field private screenShotImgURL:Ljava/lang/String;

.field private screenShotResolution:Ljava/lang/String;

.field private sellerDescription:Ljava/lang/String;

.field private sellerEmail:Ljava/lang/String;

.field private sellerLocation:Ljava/lang/String;

.field private sellerName:Ljava/lang/String;

.field private sellerNum:Ljava/lang/String;

.field private sellerRegisterNum:Ljava/lang/String;

.field private sellerTradeName:Ljava/lang/String;

.field private sellerUrl:Ljava/lang/String;

.field private supportEmail:Ljava/lang/String;

.field private updateDescription:Ljava/lang/String;

.field private version:Ljava/lang/String;

.field private youtubeRtspUrl:Ljava/lang/String;

.field private youtubeScreenShoutUrl:Ljava/lang/String;

.field private youtubeUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->bValid:Z

    .line 436
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 59
    return-void
.end method

.method private createScreenShot()V
    .locals 4

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->screenShotImgURL:Ljava/lang/String;

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->screenShotCount:I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->screenShotResolution:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 239
    :cond_0
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 454
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 458
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->bValid:Z

    .line 447
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVcontentGradeImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->contentGradeImgUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 169
    const-string v0, ""

    .line 171
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->contentGradeImgUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVcreateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->createDate:Ljava/lang/String;

    return-object v0
.end method

.method public getVlastUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->lastUpdateDate:Ljava/lang/String;

    return-object v0
.end method

.method public getVnameAuth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->nameAuthYN:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 206
    const-string v0, ""

    .line 208
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->nameAuthYN:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVproductDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->productDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 88
    const-string v0, ""

    .line 90
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->productDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVrealContentsSize()Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->realContentsSize:J

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    .line 97
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v0

    .line 99
    return-object v0
.end method

.method public getVreportNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->reportNum:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 336
    const-string v0, ""

    .line 338
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->reportNum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVrepresentation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->representation:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 320
    const-string v0, ""

    .line 322
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->representation:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVrestrictedAge()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->restrictedAge:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 152
    const-string v0, ""

    .line 154
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->restrictedAge:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVscreenShotCount()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->screenShotCount:I

    return v0
.end method

.method public getVscreenShotImgURL(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->createScreenShot()V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getImageURL(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVscreenShotOrientation(I)I
    .locals 1

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->createScreenShot()V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getOrientation(I)I

    move-result v0

    return v0
.end method

.method public getVsellerDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 288
    const-string v0, ""

    .line 290
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerEmail:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 360
    const-string v0, ""

    .line 362
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerEmail:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerLocation:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 352
    const-string v0, ""

    .line 354
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerLocation:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 279
    const-string v0, ""

    .line 281
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerNum:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 344
    const-string v0, ""

    .line 346
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerNum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerRegisterNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerRegisterNum:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 328
    const-string v0, ""

    .line 330
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerRegisterNum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerTradeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerTradeName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 312
    const-string v0, ""

    .line 314
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerTradeName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 304
    const-string v0, ""

    .line 306
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsupportEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->supportEmail:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 296
    const-string v0, ""

    .line 298
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->supportEmail:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVupdateDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->updateDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 144
    const-string v0, ""

    .line 146
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->updateDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVversion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->version:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->version:Ljava/lang/String;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getVyoutubeRtspUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->youtubeRtspUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 110
    const-string v0, ""

    .line 112
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->youtubeRtspUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVyoutubeScreenShoutUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->youtubeScreenShoutUrl:Ljava/lang/String;

    .line 129
    if-nez v0, :cond_1

    .line 130
    const-string v0, ""

    .line 138
    :cond_0
    :goto_0
    return-object v0

    .line 133
    :cond_1
    const-string v1, "_P"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 134
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getVyoutubeUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->youtubeUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 119
    const-string v0, ""

    .line 121
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->youtubeUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getYouTubeScreenShotImgURL()Ljava/lang/String;
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->youtubeScreenShoutUrl:Ljava/lang/String;

    .line 251
    if-nez v0, :cond_1

    .line 252
    const/4 v0, 0x0

    .line 260
    :cond_0
    :goto_0
    return-object v0

    .line 255
    :cond_1
    const-string v1, "_P"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 256
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasGradeImg()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 177
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    .line 178
    if-nez v2, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isBrazill()Z

    move-result v3

    if-ne v3, v1, :cond_2

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->contentGradeImgUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->contentGradeImgUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 189
    goto :goto_0

    .line 191
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->nameAuthYN:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->nameAuthYN:Ljava/lang/String;

    const-string v3, "N"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 197
    goto :goto_0
.end method

.method public hasScreenshotImage()Z
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->screenShotImgURL:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->screenShotImgURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 368
    :cond_0
    const/4 v0, 0x0

    .line 370
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isAllAge()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 159
    const-string v1, "0+"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->restrictedAge:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    const-string v1, "4+"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->restrictedAge:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAlreadyPurchased()Z
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v0, :cond_0

    .line 379
    const/4 v0, 0x0

    .line 382
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->alreadyPurchased:Z

    goto :goto_0
.end method

.method public isDisplaySellerInfo()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 407
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 409
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 413
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDisplaySellerLocation()Z
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerRegisterNum:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->sellerRegisterNum:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isFreeContent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 400
    :cond_1
    const/4 v0, 0x1

    .line 402
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIAPSupported()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v1, :cond_1

    .line 432
    :cond_0
    :goto_0
    return v0

    .line 424
    :cond_1
    :try_start_0
    const-string v1, "Y"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->IAPSupportYn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "1"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->IAPSupportYn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 432
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public isLinkApp()Z
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v0, :cond_0

    .line 388
    const/4 v0, 0x0

    .line 391
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    goto :goto_0
.end method

.method public isRevision()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->updateDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSINA()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isSINAContent()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->bValid:Z

    return v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 439
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 440
    return-void
.end method

.method public openScreenShot(I)V
    .locals 3

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->createScreenShot()V

    .line 245
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->_screenshot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    invoke-direct {v1, v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->openScreenShot(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;)V

    .line 246
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 472
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x0

    return v0
.end method
