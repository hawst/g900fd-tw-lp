.class public Lcom/sec/android/app/samsungapps/AboutActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetClickListener;


# instance fields
.field protected final TAG:Ljava/lang/String;

.field a:Landroid/content/Context;

.field b:Landroid/os/Handler;

.field private c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

.field private d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->a:Landroid/content/Context;

    .line 30
    const-string v0, "AboutActivity"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->TAG:Ljava/lang/String;

    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/a;-><init>(Lcom/sec/android/app/samsungapps/AboutActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/AboutActivity;)Lcom/sec/android/app/samsungapps/widget/AboutWidget;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->a:Landroid/content/Context;

    .line 38
    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/AboutActivity;->setMainView(I)V

    .line 39
    const v1, 0x7f080116

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/AboutActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    const-string v1, "com.sec.android.app.samsungapps"

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    const v0, 0x7f0c0027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->setWidgetClickListener(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->loadWidget()V

    .line 43
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidget;->release()V

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->c:Lcom/sec/android/app/samsungapps/widget/AboutWidget;

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->clear()V

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->d:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    .line 60
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 62
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 63
    return-void
.end method

.method public onOpenSourceLicenseClick()Z
    .locals 4

    .prologue
    .line 154
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/AboutActivity;->commonStartActivity(Landroid/content/Intent;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 160
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onPrivacyPolicyClick()Z
    .locals 4

    .prologue
    .line 138
    new-instance v0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;-><init>(Landroid/content/Context;)V

    .line 139
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->showPrivacyPolicy()V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 145
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onTermsAndConditionClick()Z
    .locals 4

    .prologue
    .line 122
    new-instance v0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;-><init>(Landroid/content/Context;)V

    .line 123
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->showTermsAndConditions()V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 129
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onUpdateClick()Z
    .locals 4

    .prologue
    .line 94
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;-><init>()V

    .line 95
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->askUserUpdateODC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    if-nez v2, :cond_1

    .line 100
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 107
    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->setNextSuccessCommand(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 108
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 113
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 104
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->odcUpdateCommand(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_0
.end method

.method public onYouthPrivacyPolicyClick()Z
    .locals 4

    .prologue
    .line 168
    new-instance v0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;-><init>(Landroid/content/Context;)V

    .line 169
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->showYouthPrivacyPolicy()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/AboutActivity;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 175
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
