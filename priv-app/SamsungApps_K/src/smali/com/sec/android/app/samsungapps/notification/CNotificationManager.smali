.class public Lcom/sec/android/app/samsungapps/notification/CNotificationManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;


# instance fields
.field a:I

.field b:I

.field c:I

.field final d:Ljava/lang/String;

.field e:Ljava/util/HashMap;

.field f:Ljava/util/HashMap;

.field g:I

.field h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

.field private i:Landroid/content/Context;

.field private j:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const v0, -0x282829

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    .line 56
    const v0, -0x646465

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    .line 58
    const-string v0, "odc9820938409234.apk"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->d:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    .line 191
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 62
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v1, "com.android.settings"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->c:I

    .line 66
    iget v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->c:I

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ThemeInfo;->isBlackTheme(I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 68
    const v0, -0x19191a

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    .line 69
    const v0, -0x777778

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    .line 72
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 567
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->dontOpenDetailPage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    const/4 v0, 0x0

    .line 573
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;Z)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;Z)Landroid/app/PendingIntent;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 579
    .line 580
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v4

    .line 582
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getGUID()Ljava/lang/String;

    move-result-object v5

    .line 583
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getLoadType()Ljava/lang/String;

    move-result-object v2

    .line 584
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->getActionType(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;Z)I

    move-result v6

    .line 586
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v7

    .line 588
    const-string v1, "odc9820938409234.apk"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-object v0

    .line 593
    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->getActionType(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;Z)I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    if-eq v6, v1, :cond_2

    const/4 v1, 0x3

    if-ne v6, v1, :cond_5

    :cond_2
    const/4 v1, -0x1

    if-eq v7, v1, :cond_5

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 596
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/Main;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 616
    :goto_1
    if-eqz v1, :cond_0

    .line 620
    const-string v0, "GUID"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    const-string v0, "productid"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getLoadType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 623
    const-string v0, "loadtype"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628
    :cond_4
    const-string v0, "action"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 629
    const-string v0, "notificationid"

    invoke-virtual {v1, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 630
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 631
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "actionstring"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 633
    invoke-static {p1, v3, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 600
    :cond_5
    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 601
    new-instance v1, Landroid/content/Intent;

    const-class v8, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v1, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 602
    new-instance v8, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v8}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 603
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "0"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "1"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "3"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_6
    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_7

    .line 604
    const-string v2, "productID"

    invoke-virtual {v8, v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 606
    :cond_7
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getFakeModel()Ljava/lang/String;

    move-result-object v2

    .line 607
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    .line 609
    const-string v2, "fakeModel"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getFakeModel()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 611
    :cond_8
    const-string v2, "GUID"

    invoke-virtual {v8, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 612
    const-string v2, "cdcontainer"

    new-instance v9, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v9, v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {v1, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_9
    move v2, v3

    .line 603
    goto :goto_2

    :cond_a
    move-object v1, v0

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 687
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 689
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v3, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 700
    :goto_0
    return-object v0

    .line 692
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/Main;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 693
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 696
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 697
    const-string v1, "action"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 698
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    if-nez p0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/PendingIntent;)V
    .locals 8

    .prologue
    const/16 v4, 0x10

    const/4 v7, 0x4

    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 437
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_4

    .line 439
    if-eqz p6, :cond_2

    .line 447
    :goto_0
    if-eqz p3, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 448
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p3

    .line 451
    :cond_1
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v3, "isa_tab_quick_panel_logo"

    const-string v4, "drawable"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 461
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 473
    :goto_1
    :pswitch_0
    if-eqz p1, :cond_3

    .line 474
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;-><init>(Landroid/support/v4/app/NotificationCompat$Builder;)V

    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->build()Landroid/app/Notification;

    move-result-object v0

    move-object v1, v0

    .line 531
    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 533
    iget-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_9

    .line 555
    :goto_3
    return-void

    .line 444
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p6

    goto :goto_0

    .line 466
    :pswitch_1
    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 469
    :pswitch_2
    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 480
    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    move-object v1, v0

    .line 482
    goto :goto_2

    .line 483
    :cond_4
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 485
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 486
    const v2, 0x7f0201bd

    iput v2, v1, Landroid/app/Notification;->icon:I

    .line 490
    :goto_4
    iput-object p4, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 491
    iput v6, v1, Landroid/app/Notification;->defaults:I

    .line 492
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/app/Notification;->when:J

    .line 493
    iput v4, v1, Landroid/app/Notification;->flags:I

    .line 495
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 507
    :goto_5
    :pswitch_3
    if-eqz p6, :cond_8

    .line 514
    :goto_6
    iput-object p6, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 516
    if-eqz p3, :cond_5

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 517
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p3

    .line 520
    :cond_6
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f04007b

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 521
    const v2, 0x7f0c01c7

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v4, "isa_tab_quick_panel_logo"

    const-string v5, "drawable"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 522
    const v2, 0x7f0c01c8

    invoke-virtual {v0, v2, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 523
    const v2, 0x7f0c0066

    invoke-virtual {v0, v2, p4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 524
    const v2, 0x7f0c01c9

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 526
    iput-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto/16 :goto_2

    .line 488
    :cond_7
    const v2, 0x7f0201bc

    iput v2, v1, Landroid/app/Notification;->icon:I

    goto :goto_4

    .line 500
    :pswitch_4
    iput v5, v1, Landroid/app/Notification;->defaults:I

    goto :goto_5

    .line 503
    :pswitch_5
    iput v7, v1, Landroid/app/Notification;->defaults:I

    goto :goto_5

    .line 512
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p6

    goto :goto_6

    .line 538
    :cond_9
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 540
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/samsungapps/view/TransparentActivity;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 541
    const/high16 v4, 0x10800000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 542
    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 544
    invoke-virtual {v0, p5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 545
    invoke-virtual {v0, p5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_3

    .line 549
    :catch_0
    move-exception v0

    const-string v0, "notification doesn\'t have contentIntent"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 553
    :catch_1
    move-exception v0

    const-string v0, "SecurityException is occured."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 461
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 495
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/notification/CNotificationManager;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/PendingIntent;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 315
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    .line 319
    :catch_0
    move-exception v0

    const-string v0, "CNotificationManager::getRemoteViewBitmmapIcon::There is no package which name is same with paramater pkgName"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 320
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 705
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addInstallItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
    .locals 10

    .prologue
    const v9, 0x7f0c01b0

    const v8, 0x7f08027f

    const/4 v5, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 196
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v2, "isa_tab_quick_panel_logo"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 201
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v3, "isa_tab_quick_panel_logo"

    const-string v4, "drawable"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v5, v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 231
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 232
    if-eqz v1, :cond_0

    .line 234
    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 239
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 240
    const-string v0, "CNotificationManager : addInstallItem"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_1
    return v6

    .line 212
    :cond_1
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v2, "isa_tab_quick_panel_logo"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Landroid/app/Notification;->icon:I

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    .line 217
    const/4 v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 219
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f04007e

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 220
    const v2, 0x7f0c0088

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v4, "isa_tab_quick_panel_logo"

    const-string v5, "drawable"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 221
    const v2, 0x7f0c01cf

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 222
    const v2, 0x7f0c01d0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 223
    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 224
    const v2, 0x7f0c01b3

    invoke-virtual {v1, v2, v7, v7, v6}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 226
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v1, v9, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 228
    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto/16 :goto_0

    .line 244
    :catch_0
    move-exception v0

    const-string v0, "notification doesn\'t have contentIntent"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public addWaitingItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
    .locals 9

    .prologue
    const v8, 0x7f0c01b0

    const/16 v5, 0x64

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Notification;

    .line 713
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/NotificationCompat$Builder;

    .line 715
    if-eqz v0, :cond_0

    if-nez v1, :cond_5

    .line 717
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 718
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    const v0, 0x7f0201bd

    .line 724
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 726
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v2, -0x2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v2

    invoke-virtual {v0, v5, v2, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 736
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 768
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 769
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    .line 772
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CNotificationManager : addWaitingItem "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 806
    :cond_1
    :goto_2
    return v7

    .line 722
    :cond_2
    const v0, 0x7f0200d2

    goto/16 :goto_0

    .line 738
    :cond_3
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 739
    const v0, 0x7f0200d2

    iput v0, v1, Landroid/app/Notification;->icon:I

    .line 740
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/app/Notification;->when:J

    .line 741
    iput v4, v1, Landroid/app/Notification;->flags:I

    .line 743
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f04007e

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 745
    const v2, 0x7f0c0088

    const v3, 0x7f020191

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 746
    const v2, 0x7f0c01d0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 747
    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 748
    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 749
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v0, v8, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 750
    const v2, 0x7f0c01b3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v3

    invoke-virtual {v0, v2, v5, v3, v6}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 751
    const v2, 0x7f0c01cf

    const-string v3, "%s%%"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 752
    const v2, 0x7f0c01cf

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 754
    iput-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 758
    if-eqz v0, :cond_4

    .line 760
    iput-object v0, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 763
    :cond_4
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 776
    :catch_0
    move-exception v0

    const-string v0, "notification doesn\'t have contentIntent"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 781
    :cond_5
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 786
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    .line 788
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_6

    .line 789
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v0

    invoke-virtual {v1, v5, v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-string v1, "%s/%s"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 802
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 803
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CNotificationManager : addWaitingItem "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 793
    :cond_6
    iget-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 794
    const v2, 0x7f0c01d0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 795
    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 796
    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 797
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v1, v8, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 798
    const v2, 0x7f0c01b3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v3

    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 799
    const v2, 0x7f0c01cf

    const-string v3, "%s%%"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 800
    const v2, 0x7f0c01cf

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_3
.end method

.method protected getActionType(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;Z)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 661
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v2

    .line 662
    const-string v3, "UNAService"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v0, :cond_0

    const-string v3, "Samsung Apps"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v0, :cond_0

    const-string v3, "Samsung Installer Utility"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v0, :cond_0

    const-string v3, "Samsung Ad Utility"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v0, :cond_0

    const-string v3, "Samsung Push Service"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    :cond_0
    move v0, v1

    .line 682
    :cond_1
    :goto_0
    return v0

    .line 671
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->isAD()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->isSPP()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    .line 672
    goto :goto_0

    .line 674
    :cond_4
    if-eqz p2, :cond_1

    .line 675
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KNOX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 677
    const/4 v0, 0x3

    goto :goto_0

    .line 679
    :cond_5
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 333
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 334
    return-void
.end method

.method public registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/app/PendingIntent;)V
    .locals 13

    .prologue
    .line 339
    const/4 v0, -0x1

    .line 341
    :try_start_0
    const-string v1, "||"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 351
    const-string v2, "||"

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 353
    if-eqz v0, :cond_0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 355
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 358
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 359
    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move v1, v0

    .line 370
    :goto_0
    :try_start_1
    const-string v0, "||"

    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 372
    if-eqz v0, :cond_2

    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    .line 374
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 377
    :cond_3
    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 379
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    if-nez v1, :cond_4

    .line 381
    const/4 v0, 0x0

    :cond_4
    move-object v8, v0

    .line 390
    :goto_1
    if-nez v8, :cond_6

    .line 391
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p2

    move/from16 v5, p3

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/PendingIntent;)V

    .line 395
    :cond_5
    :goto_2
    return-void

    .line 363
    :catch_0
    move-exception v1

    move-object v4, p1

    move v1, v0

    move-object/from16 v3, p4

    .line 364
    goto :goto_0

    .line 386
    :catch_1
    move-exception v0

    const-string v0, "image Url is not received."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 387
    const/4 v8, 0x0

    goto :goto_1

    .line 393
    :cond_6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v0, v8, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->exist()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ImageFile;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move/from16 v5, p3

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/PendingIntent;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v0, v8, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    new-instance v5, Lcom/sec/android/app/samsungapps/notification/a;

    move-object v6, p0

    move-object v7, p2

    move-object v9, v3

    move-object v10, v4

    move/from16 v11, p3

    move-object/from16 v12, p5

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/samsungapps/notification/a;-><init>(Lcom/sec/android/app/samsungapps/notification/CNotificationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestImage;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->h:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_2
.end method

.method public removeItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
    .locals 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    const/4 v0, 0x1

    return v0
.end method

.method public setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
    .locals 9

    .prologue
    const v8, 0x7f0c01b0

    const/16 v5, 0x64

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Notification;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/NotificationCompat$Builder;

    .line 90
    const-string v2, "GT-I9008"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v7, :cond_0

    .line 92
    const v2, -0x19191a

    iput v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    .line 93
    const v2, -0x777778

    iput v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    .line 96
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_6

    .line 98
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 99
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 101
    const v0, 0x7f0201bd

    .line 106
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 108
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v2, -0x2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v2

    invoke-virtual {v0, v5, v2, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 149
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CNotificationManager : setDownloadProgress "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :cond_2
    :goto_2
    return v7

    .line 103
    :cond_3
    const v0, 0x7f0200d2

    goto/16 :goto_0

    .line 120
    :cond_4
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 121
    const v0, 0x7f0200d2

    iput v0, v1, Landroid/app/Notification;->icon:I

    .line 122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/app/Notification;->when:J

    .line 123
    iput v4, v1, Landroid/app/Notification;->flags:I

    .line 125
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f04007f

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 126
    const v2, 0x7f0c0088

    const v3, 0x7f020191

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 127
    const v2, 0x7f0c01d0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 128
    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 129
    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 130
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v0, v8, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 131
    const v2, 0x7f0c01b3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v3

    invoke-virtual {v0, v2, v5, v3, v6}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 132
    const v2, 0x7f0c01cf

    const-string v3, "%s%%"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 133
    const v2, 0x7f0c01cf

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 135
    iput-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_5

    .line 141
    iput-object v0, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 144
    :cond_5
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 158
    :catch_0
    move-exception v0

    const-string v0, "notification doesn\'t have contentIntent"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 163
    :cond_6
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 168
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    .line 170
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_7

    .line 171
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v0

    invoke-virtual {v1, v5, v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-string v1, "%s/%s"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 185
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->j:Landroid/app/NotificationManager;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getNotificationID()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CNotificationManager : setDownloadProgress "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 175
    :cond_7
    iget-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 176
    const v2, 0x7f0c01d0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 177
    const v2, 0x7f0c01d0

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 178
    const-string v2, "%s/%s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getShortFormatString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 179
    iget v2, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v1, v8, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 180
    const v2, 0x7f0c01b3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v3

    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 181
    const v2, 0x7f0c01cf

    const-string v3, "%s%%"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getDownloadProgress()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 182
    const v2, 0x7f0c01cf

    iget v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_3
.end method

.method public setItemInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->e:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->f:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v2, "isa_tab_quick_panel_logo"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 271
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {p0, v1, p1, v6}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;Z)Landroid/app/PendingIntent;

    move-result-object v1

    .line 273
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v4, "isa_tab_quick_panel_logo"

    const-string v5, "drawable"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08027e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 283
    iget-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    if-nez v0, :cond_3

    .line 284
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->removeItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 298
    :goto_1
    return v6

    .line 268
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 288
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->i:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 291
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 292
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 293
    const-string v0, "CNotificationManager : setItemInstalled"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 294
    :catch_0
    move-exception v0

    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CNotificationManager::setItenInstalled::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setItemInstalledFromGoogleInstaller(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
    .locals 1

    .prologue
    .line 253
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->setItemInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z

    .line 254
    const/4 v0, 0x1

    return v0
.end method
