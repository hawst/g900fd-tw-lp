.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mComment:Ljava/lang/String;

.field mProductID:Ljava/lang/String;

.field mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->mProductID:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->mComment:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->mComment:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->mProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->mType:Ljava/lang/String;

    .line 38
    return-void
.end method
