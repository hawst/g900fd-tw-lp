.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ASK_3G_CONNECTIONOK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum HANDOVER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum NORMAL_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum RESUME_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum RESUME_TO_NORMAL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field public static final enum WAIT_NETWORK_ACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "CHECK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "RESUME_DOWNLOAD"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RESUME_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "NORMAL_DOWNLOAD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->NORMAL_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "RESUME_TO_NORMAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RESUME_TO_NORMAL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "DOWNLOADING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "CANCELLING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "CANCELED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "SUCCESS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "RETRYCHECK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "RETRY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "REQUEST_URL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "WAIT_NETWORK_ACTIVATE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->WAIT_NETWORK_ACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "HANDOVER_CHECK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->HANDOVER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    const-string v1, "ASK_3G_CONNECTIONOK"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->ASK_3G_CONNECTIONOK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    .line 10
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RESUME_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->NORMAL_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RESUME_TO_NORMAL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->WAIT_NETWORK_ACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->HANDOVER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->ASK_3G_CONNECTIONOK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    return-object v0
.end method
