.class public Lcom/sec/android/app/samsungapps/view/ViewList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ViewList;->a:Landroid/view/View;

    .line 45
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 46
    return-void
.end method

.method public getTopView()Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ViewList;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ViewList;->a:Landroid/view/View;

    .line 38
    :goto_0
    return-object v0

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ViewList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 36
    const/4 v0, 0x0

    goto :goto_0

    .line 38
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ViewList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public setTopView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ViewList;->a:Landroid/view/View;

    .line 28
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ViewList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 19
    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 23
    :cond_1
    return-void
.end method
