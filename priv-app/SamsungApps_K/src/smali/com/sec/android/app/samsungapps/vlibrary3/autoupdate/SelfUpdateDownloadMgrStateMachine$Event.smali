.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum ON_FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum SILENCE_INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field public static final enum WAIT_LOCK_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "WAIT_LOCK_RECEIVED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->WAIT_LOCK_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "DOWNLOAD_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "DOWNLOAD_DONE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "SILENCE_INSTALL_FAILED_WITH_CODE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->SILENCE_INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "ON_FOREGROUND_INSTALLING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->ON_FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "INSTALL_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "INSTALL_SUCCESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    .line 14
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->WAIT_LOCK_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->SILENCE_INSTALL_FAILED_WITH_CODE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->ON_FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Event;

    return-object v0
.end method
