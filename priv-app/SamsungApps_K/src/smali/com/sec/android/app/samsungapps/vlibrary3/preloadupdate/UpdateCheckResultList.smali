.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->b:Ljava/util/ArrayList;

    .line 13
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 38
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 44
    return-void
.end method

.method public closeMap()V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;-><init>()V

    .line 27
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-class v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 30
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public get(I)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->a:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 22
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
