.class public Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

.field b:Ljava/util/ArrayList;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

.field private f:Landroid/os/Handler;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->f:Landroid/os/Handler;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->b:Ljava/util/ArrayList;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->d:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->f:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 30
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    return-object v0
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    .line 73
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 82
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/f;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 85
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    goto :goto_0

    .line 91
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->execute()V

    goto :goto_0

    .line 98
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->d:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/d;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getDownloadList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 101
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;->onSellerAutoUpdateFailed()V

    goto :goto_0

    .line 104
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;->onSellerAutoUpdateSuccess()V

    goto :goto_0

    .line 107
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd$ISellerSingleAppAutoUpdObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->execute()V

    goto/16 :goto_0

    .line 110
    :pswitch_6
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->j:I

    goto/16 :goto_0

    .line 113
    :pswitch_7
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->j:I

    goto/16 :goto_0

    .line 116
    :pswitch_8
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->j:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->j:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;->onDisplayRemainCount(I)V

    goto/16 :goto_0

    .line 119
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->userCancel()V

    goto/16 :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager$ISellerAppAutoUpdateObserver;

    .line 52
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    .line 68
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$State;)V

    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 283
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    .line 284
    return-void
.end method
