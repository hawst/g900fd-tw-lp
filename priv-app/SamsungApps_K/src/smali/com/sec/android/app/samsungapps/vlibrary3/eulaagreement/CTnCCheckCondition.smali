.class public Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;->ATNT:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;->createCarrierTable(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;)Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;->a:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;->VERIZON:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;->createCarrierTable(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;)Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;->b:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;->SPRINT:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;->createCarrierTable(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;)Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;->c:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    .line 20
    return-void
.end method


# virtual methods
.method public needToSeeEULATnc()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;->a:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-virtual {v1, v2, v2, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->matchCondition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;->b:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-virtual {v1, v2, v2, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->matchCondition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;->c:Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-virtual {v1, v2, v2, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->matchCondition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public needToSeeFOTATnC()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 25
    const-string v2, "DCM"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->readCSC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 33
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 25
    goto :goto_0

    .line 29
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 33
    goto :goto_1
.end method
