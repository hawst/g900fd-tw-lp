.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

.field mAppMgr:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->mAppMgr:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 21
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 51
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->clear()V

    .line 56
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_1

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->isKNOXApp()Z

    move-result v1

    if-nez v1, :cond_1

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 43
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 26
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 71
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->size()I

    move-result v0

    return v0
.end method
