.class public Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 149
    const-string v0, "AbuseContentBlockerStm"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 191
    :goto_0
    :pswitch_0
    return-void

    .line 155
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQ_CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 158
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQ_LOGIN_AND_AGE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 164
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 167
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->SHOW_NEED_TO_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 170
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 173
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 176
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->ASK_POPUP_FOR_VALIDATION_USER_AGE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 179
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->VERIFY_REAL_AGE_NAME:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 182
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 185
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WINDOW_CAUSE_LOGEDOUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_CLOSE_WITHOUT_NOTICE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_2
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)Z
    .locals 3

    .prologue
    .line 41
    const-string v0, "AbuseContentBlockerStm"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 144
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 45
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 48
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 56
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->NOT_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->DONE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->FAILED_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 73
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->ASK_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->ASK_VALIDATION_FOR_USER_AGE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 81
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 84
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->TRY_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 95
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    .line 106
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 111
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 114
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;->NOTIFY_SHOW_DETAIL:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 117
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->FAIL_CAUSED_LOGOUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 122
    :pswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    .line 125
    :pswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->VERIFY_REAL_AGE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 128
    :pswitch_15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->FAILED_WITHOUT_NOTICE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 133
    :pswitch_16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_8

    goto/16 :goto_0

    .line 136
    :pswitch_17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 139
    :pswitch_18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;->LOGIN_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_a
        :pswitch_d
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_16
    .end packed-switch

    .line 45
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 53
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 64
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 81
    :pswitch_data_4
    .packed-switch 0x8
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 92
    :pswitch_data_5
    .packed-switch 0xa
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 103
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_11
    .end packed-switch

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xc -> :sswitch_1
    .end sparse-switch

    .line 122
    :pswitch_data_7
    .packed-switch 0x8
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 133
    :pswitch_data_8
    .packed-switch 0xd
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method
