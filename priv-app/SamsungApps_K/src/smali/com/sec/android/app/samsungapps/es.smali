.class final Lcom/sec/android/app/samsungapps/es;
.super Landroid/webkit/WebViewClient;
.source "ProGuard"


# instance fields
.field a:Z

.field b:Z

.field final synthetic c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 191
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 193
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/es;->a:Z

    .line 194
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/es;->b:Z

    return-void
.end method

.method private a(I)Z
    .locals 3

    .prologue
    .line 225
    const/4 v0, 0x0

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    const v2, 0x7f0c007e

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 228
    if-eqz v1, :cond_0

    .line 230
    const/4 v0, 0x1

    .line 231
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 234
    :cond_0
    return v0
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x1

    .line 240
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/es;->a(I)Z

    .line 242
    const-string v0, "https://samsung.samanepay.com/SamsungAppsRedirection.aspx"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 245
    invoke-virtual {p1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->a(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->a(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "Status"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 251
    const-string v1, "FAIL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 254
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    const v1, 0xff3c

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->b(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080276

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 256
    new-instance v2, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->b(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->b(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 258
    new-instance v1, Lcom/sec/android/app/samsungapps/et;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/et;-><init>(Lcom/sec/android/app/samsungapps/es;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 264
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :cond_1
    :goto_0
    return-void

    .line 273
    :cond_2
    const-string v1, "AUTHORIZED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iput v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    .line 277
    invoke-virtual {p1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    const v2, 0x7f08032a

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->toastMessage(Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->setDebitCardResult(Z)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->finish()V

    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 204
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/es;->a(I)Z

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setVisibleLoading(I)Z

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setVisibleNodata(I)Z

    .line 208
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    .line 209
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v3}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->a(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 213
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    .line 214
    :goto_0
    if-ge v1, v3, :cond_0

    .line 215
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 216
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v4

    .line 217
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/es;->c:Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->a(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 221
    :cond_0
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IranShetabCardWebViewClient::onReceivedError::URL="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 199
    const/4 v0, 0x1

    return v0
.end method
