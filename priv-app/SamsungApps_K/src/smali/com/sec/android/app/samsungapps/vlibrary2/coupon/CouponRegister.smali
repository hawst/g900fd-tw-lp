.class public Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _CouponRegisterComm:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponComm;

.field listeners:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponComm;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;->listeners:Ljava/util/ArrayList;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;->_CouponRegisterComm:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponComm;

    .line 16
    return-void
.end method


# virtual methods
.method public addCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponRegisterParam;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;->_CouponRegisterComm:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponComm;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;)V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponComm;->requestRegisterCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponRegisterParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 37
    return-void
.end method

.method public addListener(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister$ICouponRegisterListener;)V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method protected onRegisterCoupon(Z)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister$ICouponRegisterListener;

    .line 43
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister$ICouponRegisterListener;->onCouponRegistered(Z)V

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister$ICouponRegisterListener;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponRegister;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method
