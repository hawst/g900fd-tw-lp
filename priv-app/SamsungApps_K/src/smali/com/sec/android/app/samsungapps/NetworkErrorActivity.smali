.class public Lcom/sec/android/app/samsungapps/NetworkErrorActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/NetworkErrorActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    return-object v0
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 93
    const v0, 0x7f0c0036

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 94
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 97
    if-eqz v2, :cond_0

    .line 98
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 100
    :pswitch_0
    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 105
    :pswitch_1
    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v4, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 106
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/NetworkErrorActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;->notifiedAndEndApp()V

    .line 79
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a(Landroid/content/res/Configuration;)V

    .line 85
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    if-nez v0, :cond_0

    .line 32
    const-string v0, "NetworkErrorActivity::onCreate::Command is empty"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->finish()V

    .line 42
    :goto_0
    return-void

    .line 37
    :cond_0
    const v0, 0x7f040077

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->a(Landroid/content/res/Configuration;)V

    const v0, 0x7f0c0051

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c015d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0801da

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    new-instance v1, Lcom/sec/android/app/samsungapps/fg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/fg;-><init>(Lcom/sec/android/app/samsungapps/NetworkErrorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NetworkErrorActivity::ClassCastException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/NetworkErrorActivity;->finish()V

    goto :goto_0
.end method
