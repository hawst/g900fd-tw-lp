.class public Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

.field private final d:Ljava/lang/String;

.field private e:I

.field private f:Z

.field private g:Landroid/app/Activity;

.field private h:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

.field public mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

.field public mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

.field public mSellerInfoWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 20
    const-string v0, "ContentDetailOverviewWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->d:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->f:Z

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-string v0, "ContentDetailOverviewWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->d:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->f:Z

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const-string v0, "ContentDetailOverviewWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->d:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->f:Z

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->g:Landroid/app/Activity;

    .line 50
    const v0, 0x7f040034

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->e:I

    .line 51
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->e:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->initView(Landroid/content/Context;I)V

    .line 52
    const v0, 0x7f0c00e1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    .line 53
    const v0, 0x7f0c00e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    .line 54
    const v0, 0x7f0c00e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mSellerInfoWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;

    .line 55
    return-void
.end method


# virtual methods
.method public isScreenshotLoaded()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->f:Z

    return v0
.end method

.method public loadWidget()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;->onClickRetryBtn(I)V

    .line 116
    :cond_0
    return-void
.end method

.method public onResumeOverView()V
    .locals 1

    .prologue
    .line 80
    const v0, 0x7f0c00e1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->setScreenShotUIOnDetailPage()V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->refreshAutoUpdate()V

    .line 83
    return-void
.end method

.method public onStopOverView()V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->f:Z

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setScreenshotNeedLoad()V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->removeScreenshot()V

    .line 89
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContext:Landroid/content/Context;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->g:Landroid/app/Activity;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->release()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->release()V

    .line 102
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->b:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mSellerInfoWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;

    .line 106
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 107
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->g:Landroid/app/Activity;

    .line 77
    return-void
.end method

.method public setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 59
    return-void
.end method

.method public setScreenshotListner(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;)V

    .line 69
    return-void
.end method

.method public setScreenshotNeedLoad()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->setRefresh()V

    .line 135
    :cond_0
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 64
    return-void
.end method

.method public updateWidget()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mSellerInfoWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-nez v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->setWidgetData(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mSellerInfoWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailDescriptionWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->loadWidget()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->f:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->g:Landroid/app/Activity;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->setAllAttributes(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mContentDetailScreenshotWidget:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->setScreenShotUIOnDetailPage()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->mSellerInfoWidget:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->loadWidget()V

    .line 128
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setNeedLoad(Z)V

    goto :goto_0
.end method
