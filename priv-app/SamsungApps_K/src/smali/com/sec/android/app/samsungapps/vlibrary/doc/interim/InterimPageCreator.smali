.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;


# instance fields
.field private mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

.field private mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

.field private mListItemCount:I

.field private map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mListItemCount:I

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

    .line 16
    return-void
.end method

.method private setInterimPageInfoData(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    const-string v1, "listTitle"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->setListTitle(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    const-string v1, "listDescription"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->setListDescription(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    const-string v1, "InterimSymbolImgUrl"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->setInterimSymbolImgUrl(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    const-string v1, "seeMoreBtnTargetType"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->setSeeMoreBtnTargetType(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    const-string v1, "seeMoreBtnTargetData"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->setSeeMoreBtnTargetData(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    const-string v1, "listContentSetID"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->setListContentSetID(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

    const-string v1, "InterimBGImgUrl"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->setInterimBGImgUrl(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

    const-string v1, "InterimTopTitle"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->setInterimTopTitle(Ljava/lang/String;)V

    .line 47
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 63
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public closeList()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimPageInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageInfo;->add(Ljava/lang/Object;)Z

    .line 33
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mListItemCount:I

    .line 35
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;->add(Ljava/lang/Object;)Z

    .line 71
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mListItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mListItemCount:I

    .line 72
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mListItemCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->setInterimPageInfoData(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 77
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 78
    return-void
.end method

.method public openList()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mInterimContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimContentList;

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->mListItemCount:I

    .line 53
    return-void
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/interim/InterimPageCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 83
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method
