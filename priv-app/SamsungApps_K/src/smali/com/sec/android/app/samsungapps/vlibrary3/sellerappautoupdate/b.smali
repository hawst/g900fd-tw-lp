.class final Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd$ISellerSingleAppAutoUpdObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onUpdateFailed()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_FAILED_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_FAILED_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    goto :goto_0
.end method

.method public final onUpdateSuccess()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_SUCCESS_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_SUCCESS_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;)V

    goto :goto_0
.end method
