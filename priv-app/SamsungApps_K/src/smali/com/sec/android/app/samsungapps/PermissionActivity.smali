.class public Lcom/sec/android/app/samsungapps/PermissionActivity;
.super Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/preferences/IContentSummary;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

.field private b:Z

.field private c:Z

.field private d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

.field private e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

.field private f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

.field private g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->c:Z

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    .line 210
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->j:Z

    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 232
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->c:Z

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->finish()V

    .line 234
    return v2
.end method

.method public getDiscountPrice()Ljava/lang/String;
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->i:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getBuyPrice()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFocus()V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNormalPrice()Ljava/lang/String;
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->i:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getNormalPrice()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 207
    const v0, 0x7f0c021d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method protected getTailView()Landroid/view/View;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    return-object v0
.end method

.method protected getaddingResourceId()I
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 184
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 185
    const v0, 0x7f0c0029

    .line 189
    :goto_0
    return v0

    .line 187
    :cond_0
    const v0, 0x7f0c0148

    goto :goto_0
.end method

.method protected getremovingResourceId()I
    .locals 2

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 196
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 197
    const v0, 0x7f0c0148

    .line 201
    :goto_0
    return v0

    .line 199
    :cond_0
    const v0, 0x7f0c0029

    goto :goto_0
.end method

.method public isDiscounted()Z
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->i:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->isDiscounted()Z

    move-result v0

    return v0
.end method

.method public isFreeProduct()Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    return v0
.end method

.method public isKorea()Z
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    return v0
.end method

.method public isPaymentAbleCondition()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method public onAgree()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 162
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->b:Z

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 165
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->b:Z

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->finish()V

    .line 167
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onBackPressed()V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->c:Z

    .line 158
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    if-nez v0, :cond_0

    .line 45
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->finish()V

    .line 63
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v1, 0x7f0802e6

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v4

    :goto_1
    if-eqz v0, :cond_2

    .line 53
    const v1, 0x7f0801c3

    .line 56
    :cond_2
    new-array v4, v4, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PermissionActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    const v3, 0x7f08029b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/PermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->createFormatter(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculatorCreator;->createFreePrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->i:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    .line 60
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->setMainView(I)V

    .line 61
    const v0, 0x7f0c021e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculatorCreator;->createFreePrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    move-result-object v3

    invoke-direct {v0, p0, v1, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PurchaseItemInfoWidgetData;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->set(Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->refresh()V

    const v0, 0x7f0c021f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetDataForFreeProduct;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetDataForFreeProduct;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->loadWidget()V

    const v0, 0x7f0c021b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->getPermissionMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->setPermissionInfoList(Ljava/util/HashMap;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->setPermissionData(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->loadWidget()V

    const v0, 0x7f0c021c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->loadWidget()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->setConfChangedCompleteListener(Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->changeHeaderTailView()V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->invokeCompleted()V

    goto/16 :goto_0

    .line 51
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isAlreadyPurchased()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    goto/16 :goto_1

    :cond_4
    move v0, v4

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onDestroy()V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;->release()V

    .line 124
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->d:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseItemInfoWidget;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->release()V

    .line 129
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->e:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    if-eqz v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->release()V

    .line 134
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->f:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    if-eqz v0, :cond_3

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;->release()V

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->g:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonWidget;

    .line 142
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->b:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->j:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->c:Z

    if-nez v0, :cond_4

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    if-eqz v0, :cond_4

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 148
    :cond_4
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    .line 150
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->i:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    .line 151
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->j:Z

    .line 216
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onResume()V

    .line 217
    return-void
.end method

.method public payment()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 221
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->j:Z

    if-nez v0, :cond_0

    .line 222
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->j:Z

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PermissionActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->userAgree(Z)V

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PermissionActivity;->finish()V

    .line 226
    :cond_0
    return v1
.end method

.method public release()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method protected requestFocus()V
    .locals 0

    .prologue
    .line 285
    return-void
.end method
