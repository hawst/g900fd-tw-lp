.class public Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;

    .line 37
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;)Z
    .locals 2

    .prologue
    .line 43
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/b;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 73
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 46
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/b;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 49
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/b;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 60
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/b;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 68
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_5
    .end packed-switch

    .line 46
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 57
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
    .end packed-switch

    .line 65
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 84
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/b;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 93
    :goto_0
    return-void

    .line 87
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;->CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;->CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
