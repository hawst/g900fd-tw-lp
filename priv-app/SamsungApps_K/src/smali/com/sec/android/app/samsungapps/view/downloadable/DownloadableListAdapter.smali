.class public Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;
.super Lcom/sec/android/app/samsungapps/view/CommonAdapter;
.source "ProGuard"


# instance fields
.field e:Landroid/content/Context;

.field f:Landroid/view/View$OnFocusChangeListener;

.field private g:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;)V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x63

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;-><init>(Landroid/content/Context;II)V

    .line 204
    new-instance v0, Lcom/sec/android/app/samsungapps/view/downloadable/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/downloadable/c;-><init>(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->f:Landroid/view/View$OnFocusChangeListener;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->g:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->e:Landroid/content/Context;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;)Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->g:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    return-object v0
.end method


# virtual methods
.method public cancelAll()V
    .locals 4

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getCount()I

    move-result v0

    .line 72
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 73
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 74
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 72
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 78
    :cond_1
    return-void
.end method

.method public checkUpdateCount()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 99
    :goto_0
    if-ge v2, v3, :cond_0

    .line 100
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 101
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 102
    add-int/lit8 v0, v1, 0x1

    .line 99
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getUpdatableListCount()I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->g:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;->hideUpdateAll()V

    .line 111
    :goto_2
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->g:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;->showUpdateAll()V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 11

    .prologue
    .line 146
    new-instance v0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->e:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->mListType:I

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->g:Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;

    iget-object v10, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    move-object v3, p1

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;)V

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 161
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 162
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->mAppMgr:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "1"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    :cond_0
    move v1, v3

    .line 165
    :goto_0
    if-eqz v4, :cond_1

    .line 166
    const v5, 0x7f0c0056

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 168
    if-eqz v5, :cond_1

    .line 169
    if-eqz v1, :cond_3

    .line 170
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 182
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->f:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 183
    invoke-virtual {v4, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->isDeleteMode()Z

    move-result v6

    if-nez v6, :cond_4

    .line 185
    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 186
    invoke-virtual {v5, v2}, Landroid/view/View;->setSelected(Z)V

    .line 201
    :cond_1
    :goto_2
    return-object v4

    :cond_2
    move v1, v2

    .line 163
    goto :goto_0

    .line 172
    :cond_3
    new-instance v6, Lcom/sec/android/app/samsungapps/view/downloadable/b;

    invoke-direct {v6, p0, p1}, Lcom/sec/android/app/samsungapps/view/downloadable/b;-><init>(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_1

    .line 188
    :cond_4
    if-eqz v0, :cond_1

    .line 189
    if-eqz v1, :cond_5

    .line 190
    invoke-virtual {v4, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 191
    invoke-virtual {v5, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 192
    invoke-virtual {v5, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2

    .line 194
    :cond_5
    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2
.end method

.method public isAllCancellable()Z
    .locals 4

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getCount()I

    move-result v0

    .line 58
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 59
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isCancellable(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 67
    :goto_1
    return v0

    .line 58
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isEnableSelected()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getCount()I

    move-result v3

    move v2, v1

    .line 116
    :goto_0
    if-ge v2, v3, :cond_1

    .line 117
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    const/4 v0, 0x1

    .line 124
    :goto_1
    return v0

    .line 116
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 124
    goto :goto_1
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method public onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->checkUpdateCount()V

    .line 131
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 132
    return-void
.end method

.method public onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->checkUpdateCount()V

    .line 138
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 139
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->release()V

    .line 40
    return-void
.end method

.method public updateAll()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getCount()I

    move-result v3

    move v1, v2

    .line 44
    :goto_0
    if-ge v1, v3, :cond_1

    .line 45
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 46
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->h:Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->e:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;->e:Landroid/content/Context;

    invoke-interface {v4, v5, v0, v2}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createUpdateCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    new-instance v4, Lcom/sec/android/app/samsungapps/view/downloadable/a;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/view/downloadable/a;-><init>(Lcom/sec/android/app/samsungapps/view/downloadable/DownloadableListAdapter;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager$IDownloadCmdHelperObserver;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->execute()V

    .line 44
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 53
    :cond_1
    return-void
.end method
