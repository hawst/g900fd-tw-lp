.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;
.super Landroid/widget/ListView;
.source "ProGuard"


# instance fields
.field a:I

.field b:Z

.field c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 12
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->a:I

    .line 13
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->c:Landroid/content/Context;

    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    .line 20
    return-void

    :cond_0
    move v0, v1

    .line 19
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->a:I

    .line 13
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->c:Landroid/content/Context;

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    .line 27
    return-void

    :cond_0
    move v0, v1

    .line 26
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->a:I

    .line 13
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->c:Landroid/content/Context;

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    .line 34
    return-void

    :cond_0
    move v0, v1

    .line 33
    goto :goto_0
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/widget/ListView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 59
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    return-void

    .line 62
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    goto :goto_0

    .line 66
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->b:Z

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 47
    const v0, 0x1fffffff

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 48
    invoke-super {p0, p1, v0}, Landroid/widget/ListView;->onMeasure(II)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->a:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 52
    return-void
.end method

.method public setHeightSize(I)V
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsListView;->a:I

    .line 39
    return-void
.end method
