.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# instance fields
.field private bRequestTop:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 11
    const-string v2, "categoryListSearch"

    const-string v3, "2010"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 9
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->bRequestTop:Z

    .line 13
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->bRequestTop:Z

    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 19
    const-string v2, "categoryListSearch"

    const-string v3, "2010"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 9
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->bRequestTop:Z

    .line 21
    const-string v0, "upLevelCategoryID"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v0, "categoryLevel"

    iget v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->subLevelCategory:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const-string v0, "categorySortString"

    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categorySortString:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const-string v0, "startNum"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v0, "endNum"

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const-string v0, "categoryRepImgYn"

    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;I)V

    .line 39
    :goto_0
    return-void

    .line 36
    :cond_0
    const-string v0, "categoryRepImgYn"

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CategoryListRequestXML;->addParam(Ljava/lang/String;I)V

    goto :goto_0
.end method
