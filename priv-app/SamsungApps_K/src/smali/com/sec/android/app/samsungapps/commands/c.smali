.class final Lcom/sec/android/app/samsungapps/commands/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/c;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 235
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_5_0200(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 239
    const-string v1, "isAutoLogin"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/c;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->isAutoLogin()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 241
    const-class v1, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewInterfaceHandlerActivity;

    invoke-static {p1, v1, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 243
    :cond_0
    return-void
.end method
