.class final Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getFakeInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getUpdateInterval()I

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getUpdateInterval()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->fromMinutes(J)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFakeInterval()Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getUpdateInterval()I

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    const/4 v0, 0x1

    .line 29
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
