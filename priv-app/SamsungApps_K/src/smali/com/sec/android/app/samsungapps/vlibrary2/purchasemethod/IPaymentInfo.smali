.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract addIGiftCardCouponInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;)V
.end method

.method public abstract addIPriceChangeListener(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;)V
.end method

.method public abstract createRealPurchaseInfo(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
.end method

.method public abstract getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public abstract getSelCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
.end method

.method public abstract getSelGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
.end method

.method public abstract getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
.end method

.method public abstract getSelPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
.end method

.method public abstract isAllCoupon()Z
.end method

.method public abstract load()V
.end method

.method public abstract release()V
.end method

.method public abstract save()V
.end method

.method public abstract selCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)Z
.end method

.method public abstract selGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)Z
.end method

.method public abstract selPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z
.end method
