.class public final enum Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum GOOGLE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

.field public static final enum SAMSUNG:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

.field public static final enum TSTORE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    const-string v1, "SAMSUNG"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->SAMSUNG:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    const-string v1, "GOOGLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->GOOGLE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    const-string v1, "TSTORE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->TSTORE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->SAMSUNG:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->GOOGLE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->TSTORE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    return-object v0
.end method
