.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum AVAILABLE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum GEOIP_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum SHOWING_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

.field public static final enum TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "TESTMODE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "MCC_COUNTRY_SEARCH"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "GEOIP_COUNTRY_SEARCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "DISCLAIMER_CHECK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "GEOIP_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "TESTMODE_COUNTRY_LIST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "AVAILABLE_COUNTRY_LIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->AVAILABLE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    const-string v1, "SHOWING_CHINA_DATA_POPUP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SHOWING_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    .line 24
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->GEOIP_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->AVAILABLE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->SHOWING_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$State;

    return-object v0
.end method
