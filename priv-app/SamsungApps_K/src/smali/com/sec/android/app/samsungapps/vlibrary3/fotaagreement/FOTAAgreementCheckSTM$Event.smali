.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_AGREEMENT:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

.field public static final enum TnCAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

.field public static final enum TnCNotAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    const-string v1, "CHECK_AGREEMENT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->CHECK_AGREEMENT:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    const-string v1, "TnCAgreed"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    const-string v1, "TnCNotAgreed"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCNotAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->CHECK_AGREEMENT:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCNotAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    return-object v0
.end method
