.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;
.source "ProGuard"


# instance fields
.field bCanceled:Z

.field bResumeDownload:Z

.field private bSuccess:Z

.field handler:Landroid/os/Handler;

.field is:Ljava/io/InputStream;

.field mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

.field mDownloadInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

.field mHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field retryCount:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;-><init>()V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 26
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bResumeDownload:Z

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bCanceled:Z

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->handler:Landroid/os/Handler;

    .line 104
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->is:Ljava/io/InputStream;

    .line 195
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->retryCount:I

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z

    return v0
.end method

.method private download()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 134
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSending:Z

    .line 135
    const-string v0, "RequestGET bSending=true"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 136
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->is:Ljava/io/InputStream;

    const/16 v2, 0x800

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 139
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->download(Ljava/io/BufferedInputStream;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z

    .line 147
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->notifyDownloadComplete()V

    .line 155
    :goto_1
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 153
    :catchall_0
    move-exception v0

    throw v0
.end method

.method private notifyDownloadComplete()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 160
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 162
    return-void
.end method

.method private validateHeader(Lorg/apache/http/HttpResponse;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 172
    const-string v1, "Content-Type"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    .line 174
    array-length v2, v1

    if-eqz v2, :cond_0

    .line 176
    aget-object v2, v1, v0

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    aget-object v1, v1, v0

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, "application/octet-stream"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private validateResumeDownload(Lorg/apache/http/HttpResponse;)Z
    .locals 1

    .prologue
    .line 95
    const-string v0, "Content-Range"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    .line 97
    array-length v0, v0

    if-nez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancel()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 215
    const-string v0, "RequestFIle cancel"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->isSending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const-string v0, "RequestFIle cancel 2"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    .line 221
    const-string v0, "RequestFIle cancel 3"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 223
    const-string v0, "RequestFIle cancel 4"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->cancel()V

    .line 225
    const-string v0, "RequestFIle cancel 5"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 228
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bCanceled:Z

    .line 229
    const-string v0, "RequestFIle cancel 6"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 230
    return v1
.end method

.method public getDownloadFile()Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    return-object v0
.end method

.method public getHttpGet()Lorg/apache/http/client/methods/HttpGet;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;->getDownloadURL()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-object v0

    .line 54
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;->getDownloadURL()Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStagingAppHostUrl()Ljava/lang/String;

    move-result-object v2

    .line 59
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadInfo:Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;->getDownloadURL()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getNewURL()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getOldHost()Ljava/lang/String;

    move-result-object v0

    .line 67
    :cond_1
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 68
    if-eqz v0, :cond_2

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v2, "Host"

    invoke-virtual {v1, v2, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->existDownloadingFile()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bResumeDownload:Z

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v1, "Range"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytes="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->getSize()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    goto :goto_0

    .line 83
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->createNewFile()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 84
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public getThreadNo()I
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x2

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bCanceled:Z

    return v0
.end method

.method protected isCompletelyDownloaded()Z
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return v0
.end method

.method public isSucceed()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z

    return v0
.end method

.method public needRetry()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method protected onFail()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z

    .line 267
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->notifyDownloadComplete()V

    .line 268
    return-void
.end method

.method public onResult(Lorg/apache/http/HttpResponse;)V
    .locals 1

    .prologue
    .line 108
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 111
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->is:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 119
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->validateHeader(Lorg/apache/http/HttpResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z

    .line 130
    :goto_1
    return-void

    .line 112
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 124
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bResumeDownload:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->validateResumeDownload(Lorg/apache/http/HttpResponse;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bResumeDownload:Z

    .line 129
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->download()V

    goto :goto_1
.end method

.method public send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
    .locals 1

    .prologue
    .line 251
    const-string v0, "RequestFILE send(NetAPI net)"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->mDownloadFile:Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->isCompletelyDownloaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->bSuccess:Z

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestFile;->notifyDownloadComplete()V

    .line 261
    :goto_0
    return-void

    .line 259
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;->send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V

    goto :goto_0
.end method
