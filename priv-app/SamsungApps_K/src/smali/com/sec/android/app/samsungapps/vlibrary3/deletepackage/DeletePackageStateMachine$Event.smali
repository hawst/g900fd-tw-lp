.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DELETE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field public static final enum DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field public static final enum DELETE_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field public static final enum DELPACKAGE_IS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field public static final enum DELPACKAGE_IS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field public static final enum NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field public static final enum SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "DELPACKAGE_IS_SYSTEM_APP"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELPACKAGE_IS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "DELPACKAGE_IS_NOT_SYSTEM_APP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELPACKAGE_IS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "SYSTEM_APP"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "NOT_SYSTEM_APP"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "DELETE_SUCCEED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    const-string v1, "DELETE_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    .line 21
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELPACKAGE_IS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELPACKAGE_IS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    return-object v0
.end method
