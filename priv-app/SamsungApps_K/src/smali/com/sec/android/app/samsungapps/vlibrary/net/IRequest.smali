.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract cancel()Z
.end method

.method public abstract getThreadNo()I
.end method

.method public abstract isCanceled()Z
.end method

.method public abstract isPOST()Z
.end method

.method public abstract isSucceed()Z
.end method

.method public abstract needRetry()Z
.end method

.method public abstract send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
.end method
