.class final Lcom/sec/android/app/samsungapps/jk;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/sec/android/app/samsungapps/jj;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/jj;I)V
    .locals 0

    .prologue
    .line 1766
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iput p2, p0, Lcom/sec/android/app/samsungapps/jk;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfirmResult(Z)V
    .locals 2

    .prologue
    .line 1770
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1772
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/jk;->a:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(I)V

    .line 1773
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/jj;->a:Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;->notifyDataSetChanged()V

    .line 1774
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    .line 1778
    :cond_0
    return-void
.end method

.method public final onInvalidPassword()V
    .locals 3

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jk;->b:Lcom/sec/android/app/samsungapps/jj;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802c7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 1783
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 1784
    return-void
.end method
