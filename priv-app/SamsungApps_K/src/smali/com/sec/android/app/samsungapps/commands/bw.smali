.class final Lcom/sec/android/app/samsungapps/commands/bw;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/SMS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->b:Landroid/content/Context;

    .line 133
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->c:Lcom/sec/android/app/samsungapps/SMS;

    .line 137
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/bw;->b:Landroid/content/Context;

    .line 138
    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 167
    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->b:Landroid/content/Context;

    const v1, 0x7f0802c1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/bw;->b:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/by;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/android/app/samsungapps/commands/by;-><init>(Lcom/sec/android/app/samsungapps/commands/bw;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 184
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    if-eqz p1, :cond_0

    .line 190
    invoke-interface {p1, v1, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0
.end method

.method public final sendSMS(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 144
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 146
    new-instance v0, Lcom/sec/android/app/samsungapps/SMS;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/bw;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/android/app/samsungapps/SMS;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->c:Lcom/sec/android/app/samsungapps/SMS;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->c:Lcom/sec/android/app/samsungapps/SMS;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/bx;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/samsungapps/commands/bx;-><init>(Lcom/sec/android/app/samsungapps/commands/bw;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/SMS;->addObserver(Lcom/sec/android/app/samsungapps/SMS$SMSObserver;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bw;->c:Lcom/sec/android/app/samsungapps/SMS;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SMS;->send()V

    .line 158
    return-void
.end method
