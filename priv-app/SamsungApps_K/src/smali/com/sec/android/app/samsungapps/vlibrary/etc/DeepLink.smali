.class public Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final DEEPLINK_SAMSUNG_SERVICE_GAMEHUB:Ljava/lang/String; = "GameHub"

.field public static final DEEPLINK_SAMSUNG_SERVICE_KIDSAPPS:Ljava/lang/String; = "KidsApps"

.field public static final EXTRA_DEEPLINK_AM_I_S2:Ljava/lang/String; = "amIS2I"

.field public static final EXTRA_DEEPLINK_APP_NAME:Ljava/lang/String; = "deepLinkAppName"

.field public static final EXTRA_DEEPLINK_CATEGORY_TITLE:Ljava/lang/String; = "categoryTitle"

.field public static final EXTRA_DEEPLINK_FAKE_MODEL:Ljava/lang/String; = "fakeModel"

.field public static final EXTRA_DEEPLINK_NOACCOUNT:Ljava/lang/String; = "NOACCOUNT"

.field public static final EXTRA_DEEPLINK_SEARCH_KEYWORD:Ljava/lang/String; = "sKeyword"


# instance fields
.field private mCategoryTitle:Ljava/lang/String;

.field private mDeeplinkAppName:Ljava/lang/String;

.field private mFakeModel:Ljava/lang/String;

.field private mID:Ljava/lang/String;

.field private mIsInternal:Z

.field private mIsNoAccount:Ljava/lang/String;

.field private mIsS2I:Ljava/lang/String;

.field private mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

.field private mSearchKeyword:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mFakeModel:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mDeeplinkAppName:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsS2I:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mCategoryTitle:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mSearchKeyword:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsInternal:Z

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsNoAccount:Ljava/lang/String;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    .line 60
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    .line 72
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->initialize(Landroid/os/Bundle;)V

    .line 73
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    .line 64
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    .line 65
    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    .line 80
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    .line 81
    invoke-direct {p0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->initialize(Landroid/os/Bundle;)V

    .line 82
    return-void
.end method

.method public static createDeepLink(Landroid/content/Intent;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 153
    :try_start_0
    const-string v0, "directcall"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 155
    const-string v2, "GUID"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 157
    if-ne v0, v4, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createProductDetailDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    .line 409
    :goto_0
    return-object v0

    .line 162
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 164
    if-nez v2, :cond_1

    move-object v0, v1

    .line 166
    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 171
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_e

    .line 175
    :cond_2
    const-string v5, "SearchResult"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 177
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createSearchResultDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto :goto_0

    .line 180
    :cond_3
    const-string v5, "MainCategoryList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 182
    const/4 v0, 0x0

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createMainCategoryListDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto :goto_0

    .line 185
    :cond_4
    const-string v5, "MainActivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_6

    .line 187
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_5
    move-object v0, v1

    .line 409
    goto :goto_0

    .line 190
    :cond_6
    :try_start_1
    const-string v5, "PaidProductList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_7

    .line 192
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createPaidProductListDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_7
    const-string v5, "FreeProductList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_8

    .line 197
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createFreeProductListDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto :goto_0

    .line 200
    :cond_8
    const-string v5, "NewProductList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_9

    .line 202
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createNewProductListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto :goto_0

    .line 205
    :cond_9
    const-string v5, "GoUpdates"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_a

    .line 207
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createGoUpdates(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_a
    const-string v5, "GoDownloads"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_b

    .line 212
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createGoDownloads(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 215
    :cond_b
    const-string v5, "PaymentMethodsList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_c

    .line 217
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createPaymentMethodsListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 220
    :cond_c
    const-string v5, "GiftCardList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_d

    .line 222
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createGiftCardListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 225
    :cond_d
    const-string v5, "InterimPage"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_e

    .line 227
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createInterimPageDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 231
    :cond_e
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 232
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_10

    :cond_f
    move-object v0, v1

    .line 234
    goto/16 :goto_0

    .line 237
    :cond_10
    const-string v5, "ProductDetail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_11

    .line 239
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createProductDetailDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 242
    :cond_11
    const-string v5, "ProductSetList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_12

    .line 244
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToBannerList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 247
    :cond_12
    const-string v5, "CategoryList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_13

    .line 249
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 252
    :cond_13
    const-string v5, "SubCategoryList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_14

    .line 254
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToSubCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 257
    :cond_14
    const-string v5, "MainCategoryList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_15

    .line 259
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createMainCategoryListDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 262
    :cond_15
    const-string v5, "KNOXCategoryList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_17

    .line 264
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bkNOXmode:Z

    .line 265
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bNeedClearCache:Z

    .line 266
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    if-eqz v2, :cond_16

    .line 268
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->clear()V

    .line 270
    :cond_16
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 273
    :cond_17
    const-string v5, "SellerDetail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_18

    .line 275
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createSellerDeepLink(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 279
    :cond_18
    const-string v0, "www.samsungapps.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "apps.samsung.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 281
    :cond_19
    const-string v0, "/main/main.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 283
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    goto/16 :goto_0

    .line 286
    :cond_1a
    const-string v0, "/main/SearchResult.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 288
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createSearchResultDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 291
    :cond_1b
    const-string v0, "/main/MainCategoryList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 293
    const/4 v0, 0x0

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createMainCategoryListDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 296
    :cond_1c
    const-string v0, "/main/PaidProductList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 298
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createPaidProductListDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 301
    :cond_1d
    const-string v0, "/main/FreeProductList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 303
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createFreeProductListDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 306
    :cond_1e
    const-string v0, "/main/NewProductList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 308
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createNewProductListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 313
    :cond_1f
    const-string v0, "/appquery/categoryProductList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 315
    const-string v0, "categoryID"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_20

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_20

    .line 318
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 323
    :cond_20
    const-string v0, "/appquery/sellerProductList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 325
    const-string v0, "SellerID"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    if-eqz v0, :cond_21

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_21

    .line 328
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createSellerDeepLink(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 333
    :cond_21
    const-string v0, "/appquery/productSetList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 335
    const-string v0, "ProductsetID"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_22

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_22

    .line 338
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToBannerList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 344
    :cond_22
    const-string v0, "/appquery/categoryProductList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 346
    const-string v0, "subCategoryID"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 347
    if-eqz v0, :cond_23

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_23

    .line 349
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToSubCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 354
    :cond_23
    const-string v0, "/appquery/appDetail.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 356
    const-string v0, "appId"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 357
    if-eqz v0, :cond_24

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_24

    .line 359
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createProductDetailDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 365
    :cond_24
    const-string v0, "/mercury/topApps/topAppsList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 367
    const-string v0, "categoryID"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_25

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_25

    .line 370
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 375
    :cond_25
    const-string v0, "/mercury/topApps/getSellerList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 377
    const-string v0, "sellerId"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    if-eqz v0, :cond_26

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_26

    .line 380
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createSellerDeepLink(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 385
    :cond_26
    const-string v0, "/mercury/promotion/specialPromotion.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 387
    const-string v0, "sItemSeq"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 388
    if-eqz v0, :cond_27

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_27

    .line 390
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToBannerList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    goto/16 :goto_0

    .line 396
    :cond_27
    const-string v0, "/mercury/topApps/topAppsList.as"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 398
    const-string v0, "subCategoryID"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    .line 401
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLinkToSubCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static createDeepLinkToBannerList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 427
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->c:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createDeepLinkToCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 434
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->d:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createDeepLinkToSubCategoryList(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 485
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->q:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createFreeProductListDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 465
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->i:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createGiftCardListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 477
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->o:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    return-object v0
.end method

.method private static createGoDownloads(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 423
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->m:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createGoUpdates(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 416
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->l:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createInterimPageDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 481
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->p:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    return-object v0
.end method

.method private static createMainCategoryListDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 457
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->g:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createNewProductListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 469
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->j:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    return-object v0
.end method

.method private static createPaidProductListDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 461
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->h:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createPaymentMethodsListDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 473
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->n:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;)V

    return-object v0
.end method

.method private static createProductDetailDeepLink(Ljava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 441
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->e:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createSearchResultDeepLink(Landroid/os/Bundle;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 450
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->f:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static createSellerDeepLink(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 2

    .prologue
    .line 446
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->k:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/etc/a;Ljava/lang/String;)V

    return-object v0
.end method

.method private getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-object p3

    .line 109
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-le v0, v1, :cond_2

    .line 111
    invoke-virtual {p1, p2, p3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 113
    :cond_2
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    move-object p3, v0

    .line 118
    goto :goto_0
.end method

.method private initialize(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 91
    const-string v0, "bd is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->w(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 95
    :cond_0
    const-string v0, "fakeModel"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mFakeModel:Ljava/lang/String;

    .line 96
    const-string v0, "deepLinkAppName"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mDeeplinkAppName:Ljava/lang/String;

    .line 97
    const-string v0, "amIS2I"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsS2I:Ljava/lang/String;

    .line 98
    const-string v0, "categoryTitle"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mCategoryTitle:Ljava/lang/String;

    .line 99
    const-string v0, "sKeyword"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mSearchKeyword:Ljava/lang/String;

    .line 100
    const-string v0, "NOACCOUNT"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBundleString(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsNoAccount:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mDeeplinkAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getBannerListID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mCategoryTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public getFakeModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mFakeModel:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenType()Lcom/sec/android/app/samsungapps/vlibrary/etc/a;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    return-object v0
.end method

.method public getSearchKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mSearchKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSellerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public isBannerList()Z
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->c:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCategoryList()Z
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->d:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDetailPage()Z
    .locals 2

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->e:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFreeProductList()Z
    .locals 2

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->i:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGiftCardList()Z
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->o:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGoToDownloads()Z
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->m:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGoToUpdates()Z
    .locals 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->l:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInterimPage()Z
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->p:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInternal()Z
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsInternal:Z

    return v0
.end method

.method public isMainCategoryList()Z
    .locals 2

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->g:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMainView()Z
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNewProductList()Z
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->j:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNoAccount()Z
    .locals 2

    .prologue
    .line 608
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsNoAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaidProductList()Z
    .locals 2

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->h:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaymentMehtodsList()Z
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->n:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isS2I()Z
    .locals 2

    .prologue
    .line 603
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsS2I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchResult()Z
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->f:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSellerPage()Z
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->k:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubCategoryList()Z
    .locals 2

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mOpenType:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/etc/a;->q:Lcom/sec/android/app/samsungapps/vlibrary/etc/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsInternal(Z)V
    .locals 0

    .prologue
    .line 518
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->mIsInternal:Z

    .line 519
    return-void
.end method
