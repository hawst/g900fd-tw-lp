.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 8
    const-string v2, "commentList"

    const-string v3, "2100"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    const-string v0, "startNum"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;->getNextStartNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "endNum"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;->getNextEndNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    return-void
.end method
