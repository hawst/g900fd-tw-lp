.class final Lcom/sec/android/app/samsungapps/du;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    const v1, 0x7f040084

    .line 342
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/du;->a:Lcom/sec/android/app/samsungapps/ExpiredGiftCardListActivity;

    .line 343
    invoke-direct {p0, p2, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 345
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/du;->b:Landroid/view/LayoutInflater;

    .line 346
    iput v1, p0, Lcom/sec/android/app/samsungapps/du;->c:I

    .line 347
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/du;->d:Landroid/content/Context;

    .line 348
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/du;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 357
    if-nez p2, :cond_0

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/du;->b:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/sec/android/app/samsungapps/du;->c:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 359
    new-instance v1, Lcom/sec/android/app/samsungapps/dv;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/du;->d:Landroid/content/Context;

    invoke-direct {v1, v2, p2}, Lcom/sec/android/app/samsungapps/dv;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 361
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 365
    :goto_0
    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/dv;->a(I)V

    .line 366
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/dv;->a(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V

    .line 367
    return-object p2

    .line 363
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/dv;

    goto :goto_0
.end method
