.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/ISamsungWalletCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;


# instance fields
.field private _FinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->_FinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 19
    return-void
.end method


# virtual methods
.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public getBuyPrice()D
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getFinalMapContainer()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->_FinalMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    return-object v0
.end method

.method public getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public samsungWalletCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;)V

    return-object v0
.end method
