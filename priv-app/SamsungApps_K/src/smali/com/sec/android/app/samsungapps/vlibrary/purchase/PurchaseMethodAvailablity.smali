.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/purchase/PurchaseMethodAvailablity;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getDiscountPSMSPrice()D
.end method

.method public abstract getDiscountPrice()D
.end method

.method public abstract getNormalPSMSPrice()D
.end method

.method public abstract getNormalPrice()D
.end method

.method public abstract isAlipayPurchaseSupported()Z
.end method

.method public abstract isCreditCardPurchaseAvailable()Z
.end method

.method public abstract isDirectBillingPurchaseAvailable()Z
.end method

.method public abstract isPSMSPurchaseAvailable()Z
.end method

.method public abstract isPhoneBillPurchaseAvailable()Z
.end method

.method public abstract isTestPSMSPurchaseSupported()Z
.end method
