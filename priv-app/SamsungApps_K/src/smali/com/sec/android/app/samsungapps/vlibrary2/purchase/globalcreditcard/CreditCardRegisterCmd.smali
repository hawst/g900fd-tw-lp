.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;->_CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;->_CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->requestCardRegister(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 34
    return-void
.end method
