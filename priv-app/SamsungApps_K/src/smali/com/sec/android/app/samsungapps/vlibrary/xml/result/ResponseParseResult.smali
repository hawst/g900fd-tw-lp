.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;


# instance fields
.field private bodyMap:Ljava/util/ArrayList;

.field private headerMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private listlistbodyMap:Ljava/util/ArrayList;

.field private mError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->headerMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->bodyMap:Ljava/util/ArrayList;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->listlistbodyMap:Ljava/util/ArrayList;

    .line 17
    return-void
.end method


# virtual methods
.method public addBodyListListMap(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->listlistbodyMap:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public addBodyListMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->bodyMap:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public getBodyListListMap()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->listlistbodyMap:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getBodyListMap()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->bodyMap:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHeaderMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->headerMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-object v0
.end method

.method public getServerErrorInfo()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    return-object v0
.end method

.method public setHeaderMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->headerMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 27
    return-void
.end method

.method public setServerErrorCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    if-eqz p1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->setErrorCode(Ljava/lang/String;)V

    .line 58
    :cond_0
    return-void
.end method

.method public setServerErrorInfo(Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    .line 33
    return-void
.end method

.method public setServerErrorMsg(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->setErrorMsg(Ljava/lang/String;)V

    .line 63
    return-void
.end method
