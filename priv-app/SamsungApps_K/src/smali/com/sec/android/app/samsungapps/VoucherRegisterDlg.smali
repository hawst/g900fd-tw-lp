.class public Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field b:Landroid/app/Dialog;

.field private c:Landroid/content/Context;

.field private d:Landroid/view/View;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->b:Landroid/app/Dialog;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->c:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->e:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->e:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;)V

    .line 33
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->b:Landroid/app/Dialog;

    .line 49
    :cond_0
    return-void
.end method

.method public endLoading()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 156
    return-void
.end method

.method public onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->d:Landroid/view/View;

    const v1, 0x7f0c0303

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 98
    const/4 v1, -0x2

    if-ne v1, p2, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 105
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->e:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->registerCoupon(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public show()V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400e9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->d:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0802af

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    const v2, 0x7f0802ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    const v2, 0x7f08023d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->d:Landroid/view/View;

    const v2, 0x7f0c0303

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/lx;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/lx;-><init>(Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/ly;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/samsungapps/ly;-><init>(Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->b:Landroid/app/Dialog;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 40
    return-void
.end method

.method public startLoading()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/VoucherRegisterDlg;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 151
    return-void
.end method
