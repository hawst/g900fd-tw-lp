.class final Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 211
    if-eqz p2, :cond_2

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResultList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;

    move-result-object v0

    .line 214
    const-string v1, "1"

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/UpdateCheckResult;->upgrade:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    .line 227
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NO_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    goto :goto_0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NO_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/n;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;)V

    goto :goto_0
.end method
