.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum END_LOADING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field public static final enum SEND_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field public static final enum SEND_INIT_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field public static final enum SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field public static final enum SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field public static final enum START_ACTIVIY:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field public static final enum START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "SEND_INIT_REQUEST"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SEND_INIT_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "START_LOADING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "END_LOADING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->END_LOADING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "START_ACTIVIY"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->START_ACTIVIY:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "SEND_COMPLETE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SEND_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "SIG_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    const-string v1, "SIG_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SEND_INIT_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->END_LOADING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->START_ACTIVIY:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SEND_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    return-object v0
.end method
