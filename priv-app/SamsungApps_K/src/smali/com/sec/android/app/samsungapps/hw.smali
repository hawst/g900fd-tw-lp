.class final Lcom/sec/android/app/samsungapps/hw;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SearchResultActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 191
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 194
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Lcom/sec/android/app/samsungapps/SearchResultActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    goto :goto_0

    .line 202
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->cancel()V

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 208
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/hw;->removeMessages(I)V

    .line 209
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/hw;->removeMessages(I)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->search(Ljava/lang/String;)V

    goto :goto_0

    .line 214
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Z

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->displaySearchResultCount()V

    goto :goto_0

    .line 218
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Z

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    const/4 v1, 0x2

    iput v1, v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    .line 221
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/hw;->removeMessages(I)V

    .line 222
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/hw;->removeMessages(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->displaySearchResultCount()V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->displayHotKeywordList()V

    goto :goto_0

    .line 228
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    goto/16 :goto_0

    .line 232
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hw;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    goto/16 :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
