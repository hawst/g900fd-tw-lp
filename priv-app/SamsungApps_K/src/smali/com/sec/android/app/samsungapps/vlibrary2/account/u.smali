.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/u;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 277
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;->end()V

    .line 283
    :cond_0
    if-eqz v0, :cond_1

    .line 286
    :try_start_0
    iget v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->resultCode:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->accessToken:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->api_server_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->getAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onGetAccessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 309
    return-void

    .line 292
    :cond_2
    :try_start_1
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->errorMessage:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->errorMessage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 294
    const-string v1, "SA"

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->errorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_3
    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->errorMessage:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 299
    const-string v1, ""

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->errorMessage:Ljava/lang/String;

    .line 301
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/u;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    iget v2, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->resultCode:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\r\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;->errorMessage:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFailGetAccessToken(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
