.class public Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->a:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->getODCUpdateViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->a:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 32
    return-void
.end method


# virtual methods
.method public askUserUpdateODC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bo;-><init>(Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;)V

    return-object v0
.end method

.method protected getODCUpdateViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bn;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bn;-><init>(Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;)V

    return-object v0
.end method

.method public odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->a:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    return-object v0
.end method

.method public odcUpdateCommand(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/ODCManualUpdateCommandBuilder;->a:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    return-object v0
.end method
