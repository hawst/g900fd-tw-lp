.class public Lcom/sec/android/app/samsungapps/SignInActivity;
.super Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;


# static fields
.field public static final EXTRA_TITLETEXT:Ljava/lang/String; = "_titleText"


# instance fields
.field a:Landroid/widget/EditText;

.field b:Landroid/widget/EditText;

.field c:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

.field d:Landroid/content/Context;

.field e:Z

.field f:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->e:Z

    .line 314
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method

.method private a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SignInActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getEmailID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    return-object v0
.end method

.method protected getFocus()V
    .locals 0

    .prologue
    .line 382
    return-void
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 299
    return-object v0
.end method

.method protected getScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 337
    const v0, 0x7f0c021d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method protected getTailView()Landroid/view/View;
    .locals 1

    .prologue
    .line 342
    const v0, 0x7f0c0095

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getaddingResourceId()I
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 369
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 370
    const v0, 0x7f0c0029

    .line 375
    :goto_0
    return v0

    .line 372
    :cond_0
    const v0, 0x7f0c0148

    goto :goto_0
.end method

.method protected getremovingResourceId()I
    .locals 2

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 355
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 356
    const v0, 0x7f0c0148

    .line 361
    :goto_0
    return v0

    .line 358
    :cond_0
    const v0, 0x7f0c0029

    goto :goto_0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_1

    .line 204
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedIn:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_1

    .line 206
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->e:Z

    if-nez v0, :cond_0

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->e:Z

    .line 211
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->notifySuccess()V

    .line 214
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->finish()V

    .line 217
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    return v0
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 0

    .prologue
    .line 274
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->processCancel()V

    .line 259
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onBackPressed()V

    .line 260
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->d:Landroid/content/Context;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->finish()V

    .line 198
    :goto_0
    return-void

    .line 74
    :cond_0
    const v0, 0x7f0400d2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->setMainView(I)V

    .line 75
    const v0, 0x7f08033e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0xb0001

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/SignInActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->invokeComplete(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;)V

    .line 82
    :cond_1
    const v0, 0x7f0802ef

    const v1, 0x7f08023d

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    .line 84
    const v0, 0x7f0c01f1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->a:Landroid/widget/EditText;

    .line 85
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 88
    const v0, 0x7f0c02d3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->b:Landroid/widget/EditText;

    .line 90
    sget-object v0, Lcom/sec/android/app/samsungapps/SignInActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocusFromTouch()Z

    .line 99
    :cond_2
    :goto_1
    const v0, 0x7f0c02d5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0802fb

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0802e8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    const v1, 0x7f0c02d4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<u>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f080205

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</u>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v2

    .line 117
    new-instance v3, Lcom/sec/android/app/samsungapps/km;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/km;-><init>(Lcom/sec/android/app/samsungapps/SignInActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v2

    .line 129
    new-instance v3, Lcom/sec/android/app/samsungapps/kn;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/kn;-><init>(Lcom/sec/android/app/samsungapps/SignInActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    new-instance v2, Lcom/sec/android/app/samsungapps/ko;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ko;-><init>(Lcom/sec/android/app/samsungapps/SignInActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    new-instance v0, Lcom/sec/android/app/samsungapps/kq;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/kq;-><init>(Lcom/sec/android/app/samsungapps/SignInActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    const v0, 0x7f0c009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 164
    const v0, 0x7f0c009f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 166
    new-instance v2, Lcom/sec/android/app/samsungapps/kr;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/kr;-><init>(Lcom/sec/android/app/samsungapps/SignInActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    new-instance v1, Lcom/sec/android/app/samsungapps/ks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ks;-><init>(Lcom/sec/android/app/samsungapps/SignInActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->changeHeaderTailView()V

    .line 197
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    goto/16 :goto_0

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocusFromTouch()Z

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 234
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 235
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onDestroy()V

    .line 236
    return-void
.end method

.method public onInvalidEmailID()V
    .locals 1

    .prologue
    .line 304
    const v0, 0x7f0802c6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 306
    return-void
.end method

.method public onInvalidPassword()V
    .locals 1

    .prologue
    .line 310
    const v0, 0x7f0802c7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 312
    return-void
.end method

.method public onLoadingEnd()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 323
    return-void
.end method

.method public onLoadingStart()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->f:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 318
    return-void
.end method

.method public onLoginCompleted(Z)V
    .locals 1

    .prologue
    .line 327
    if-eqz p1, :cond_0

    .line 329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->e:Z

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->finish()V

    .line 333
    :cond_0
    return-void
.end method

.method public onPasswordConfirmed(Z)V
    .locals 3

    .prologue
    .line 278
    if-eqz p1, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->finish()V

    .line 288
    :goto_0
    return-void

    .line 284
    :cond_0
    const v0, 0x7f0802c7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 285
    const v1, 0x7f0802ef

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 286
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 228
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 229
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onPause()V

    .line 230
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 222
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onResume()V

    .line 223
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 224
    return-void
.end method

.method protected processCancel()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignInActivity;->e:Z

    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignInActivity;->a()Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->cancelLogin()V

    .line 269
    :cond_0
    return-void
.end method

.method protected requestFocus()V
    .locals 0

    .prologue
    .line 388
    return-void
.end method
