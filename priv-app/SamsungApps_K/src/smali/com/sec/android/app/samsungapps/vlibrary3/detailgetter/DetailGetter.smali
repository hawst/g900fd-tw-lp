.class public Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    .line 25
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->e:Landroid/os/Handler;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 31
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->d:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;

    .line 32
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;)Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->d:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 70
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 71
    const-string v1, "DetailGetterStateMachine"

    const-string v2, "onRequestForceDetail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const-string v1, "2"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 112
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 113
    const-string v0, "DetailGetterStateMachine"

    const-string v1, "onRequestForceDetail send"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return-void

    .line 93
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/b;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->guidProductDetailEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)Z

    .line 37
    return-void
.end method


# virtual methods
.method public forceLoad()V
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;->FORCERELOAD:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)V

    .line 159
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;)V
    .locals 3

    .prologue
    .line 51
    const-string v0, "DetailGetterStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    return-void

    .line 55
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;->RECEIVE_DETAIL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a()V

    goto :goto_0

    .line 58
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->e:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 61
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->e:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 64
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a()V

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->c:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    .line 42
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$State;)V

    return-void
.end method

.method public validate()V
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->a(Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetterState$Event;)V

    .line 154
    return-void
.end method
