.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCard$IRegisterGiftCardData;


# instance fields
.field protected _IActiveGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

.field protected _IExpiredGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IActiveGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    .line 11
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IExpiredGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    return-void
.end method


# virtual methods
.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public deleteGiftCard(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/DeleteGiftCard$IDeleteGiftCardData;Ljava/lang/String;)V

    return-object v0
.end method

.method public getActivieGiftCardList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;)V

    return-object v0
.end method

.method public getExpiredGiftCardList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GetGiftCardList$IGetGiftCardListData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;)V

    return-object v0
.end method

.method public getGiftCardList(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 44
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 40
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IActiveGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    goto :goto_0

    .line 42
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IExpiredGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public registerGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCard;

    invoke-direct {v0, p0, p2, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCard;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCard$IRegisterGiftCardData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;Ljava/lang/String;)V

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IActiveGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IExpiredGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    .line 61
    return-void
.end method

.method public setGiftCardList(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;)V
    .locals 2

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 33
    :goto_0
    return-void

    .line 27
    :pswitch_0
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IActiveGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    goto :goto_0

    .line 30
    :pswitch_1
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/AbstractGiftCardCommandBuilder;->_IExpiredGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;

    goto :goto_0

    .line 24
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
