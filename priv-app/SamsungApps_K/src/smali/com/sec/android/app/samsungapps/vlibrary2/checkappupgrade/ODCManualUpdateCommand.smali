.class public Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;
.source "ProGuard"


# instance fields
.field private final TAG:Ljava/lang/String;

.field protected mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

.field protected mDownloadState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

.field protected mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

.field protected mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, v0, v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    .line 39
    const-string v0, "ODCManualUpdateCommand"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->TAG:Ljava/lang/String;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 54
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, v0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    .line 39
    const-string v0, "ODCManualUpdateCommand"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->TAG:Ljava/lang/String;

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    const-string v1, "com.sec.android.app.samsungapps"

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 47
    return-void
.end method


# virtual methods
.method protected getNotifyStateObserver()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;)V

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    if-nez v0, :cond_0

    .line 62
    const-string v0, "ODCManualUpdateCommand::impExecute::View Invoker is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    invoke-interface {v0, p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    .line 75
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->getProductName()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setCancellable(Z)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->getNotifyStateObserver()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    .line 79
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 80
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    if-nez v0, :cond_0

    .line 84
    const-string v0, "ODCManualUpdateCommand::invokeCompleted::Update Command is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->setNeedAutoDownload(Z)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mUpdateCommand:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
