.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum ASKCLISTP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum ASKQP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum REQUEST_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum REQUEST_DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum REQUEST_GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum REQUEST_MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum SEND_GEOIP_SEARCH_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum SEND_SIGNAL_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

.field public static final enum SHOW_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "ASKQP2"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ASKQP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "CLEAR_CACHE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "REQUEST_MCC_COUNTRY_SEARCH"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "SEND_SUCCESS_SIGNAL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "REQUEST_GEOIP_COUNTRY_SEARCH"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "REQUEST_DISCLAIMER_CHECK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "SEND_SIGNAL_FAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_SIGNAL_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "SEND_GEOIP_SEARCH_FAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_GEOIP_SEARCH_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "ASKCLISTP2"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ASKCLISTP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "REQUEST_COUNTRY_LIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    const-string v1, "SHOW_CHINA_DATA_POPUP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SHOW_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    .line 39
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ASKQP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->CLEAR_CACHE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_MCC_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_GEOIP_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_DISCLAIMER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_SIGNAL_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SEND_GEOIP_SEARCH_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->ASKCLISTP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->REQUEST_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->SHOW_CHINA_DATA_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Action;

    return-object v0
.end method
