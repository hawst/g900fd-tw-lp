.class final Lcom/sec/android/app/samsungapps/vlibrary2/update/j;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onForegroundInstalling()V
    .locals 2

    .prologue
    .line 267
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFinished(Ljava/lang/String;)Z

    .line 269
    return-void
.end method

.method public final onInstallFailed()V
    .locals 2

    .prologue
    .line 252
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    .line 254
    return-void
.end method

.method public final onInstallFailed(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->setTag(Ljava/lang/Object;)V

    .line 261
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    .line 263
    return-void
.end method

.method public final onInstallSuccess()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->amISystemApp()Z
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->_DLState:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addStateItem(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    .line 246
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->getGUID()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFinished(Ljava/lang/String;)Z

    .line 248
    return-void
.end method
