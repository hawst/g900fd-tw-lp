.class final Lcom/sec/android/app/samsungapps/commands/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/k;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 124
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    .line 127
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->Create()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 129
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    const-class v0, Lcom/sec/android/app/samsungapps/SignInActivity;

    invoke-static {p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
