.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ASK_UPDATE_BY_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field public static final enum FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field public static final enum REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field public static final enum UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    const-string v1, "REQUEST"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    const-string v1, "FINISH"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    const-string v1, "ASK_UPDATE_BY_POPUP"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->ASK_UPDATE_BY_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    const-string v1, "UPDATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 9
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->ASK_UPDATE_BY_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    return-object v0
.end method
