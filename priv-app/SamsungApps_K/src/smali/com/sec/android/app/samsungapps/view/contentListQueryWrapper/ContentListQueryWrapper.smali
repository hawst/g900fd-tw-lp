.class public Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field private b:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->c:Z

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->b:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)V

    .line 18
    return-void
.end method


# virtual methods
.method public changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    .line 56
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 118
    return-void
.end method

.method public contentListGetCompleted(Z)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->c:Z

    .line 92
    return-void
.end method

.method public contentListLoading()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->c:Z

    .line 98
    return-void
.end method

.method public doesSupportSortOption()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->doesSupportSortOption()Z

    move-result v0

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finishGettingMoreContent(Z)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public getContentListSize()I
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->isLoading()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-nez v0, :cond_1

    .line 122
    :cond_0
    const/4 v0, -0x1

    .line 125
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getIndexOfFirstItem()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 39
    :cond_0
    const/4 v0, 0x0

    .line 41
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getIndexOfFirstItem()I

    move-result v0

    goto :goto_0
.end method

.method public getPaidTypeFilter()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getPaidTypeFilter()I

    move-result v0

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSortOrderToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    :cond_0
    return-object v0
.end method

.method public isCompleteEmptyList()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 26
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isCompleted()Z

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->c:Z

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)Z

    .line 114
    :cond_0
    return-void
.end method

.method public requestDataGet()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->b:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestDataGet(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->c:Z

    .line 23
    return-void
.end method

.method public requestMoreData()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->b:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestMoreData(Landroid/content/Context;)V

    .line 86
    return-void
.end method

.method public setIndexOfFirstItem(I)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setIndexOfFirstItem(I)V

    .line 34
    :cond_0
    return-void
.end method

.method public setPaidTypeFilter(I)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/contentListQueryWrapper/ContentListQueryWrapper;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setPaidTypeFilter(I)V

    .line 82
    :cond_0
    return-void
.end method

.method public startGettingMoreContent()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
