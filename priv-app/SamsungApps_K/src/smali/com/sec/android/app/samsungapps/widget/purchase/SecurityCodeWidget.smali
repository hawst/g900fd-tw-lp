.class public Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

.field b:Landroid/view/View$OnClickListener;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field private f:Landroid/view/View;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->f:Landroid/view/View;

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/aj;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 134
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/ak;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ak;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->b:Landroid/view/View$OnClickListener;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    const v1, 0x7f0400b3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->initView(Landroid/content/Context;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->f:Landroid/view/View;

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/aj;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/aj;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 134
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/ak;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ak;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->b:Landroid/view/View$OnClickListener;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    const v1, 0x7f0400b3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->initView(Landroid/content/Context;I)V

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Landroid/view/View;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;)V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->f:Landroid/view/View;

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f08028f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    return-void
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->updateWidget()V

    .line 44
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->updateWidget()V

    .line 70
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 63
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    const v0, 0x7f0c0280

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    :cond_0
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    .line 77
    return-void
.end method

.method public updateWidget()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;->isSecurityCodeRequired()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->setVisibility(I)V

    .line 58
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/ISecurityCodeWidgetData;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->setSummary(Ljava/lang/String;)V

    .line 56
    const v0, 0x7f0c027e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/SecurityCodeWidget;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
