.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field private static a:Landroid/os/Handler;


# instance fields
.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->a:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)V
    .locals 2

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 23
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;->CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->a(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)V

    .line 73
    return-void
.end method

.method public execute()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->a(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)V

    .line 38
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;)V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 54
    :goto_0
    return-void

    .line 45
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->onRequest()V

    goto :goto_0

    .line 48
    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->onNotifyResult(Z)V

    goto :goto_0

    .line 51
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->onNotifyResult(Z)V

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Action;)V

    return-void
.end method

.method protected abstract onNotifyResult(Z)V
.end method

.method protected onReceiveFailed()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;->RECEIVE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->a(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)V

    .line 68
    return-void
.end method

.method protected onReceiveSuccess()V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;->RECEIVE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->a(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$Event;)V

    .line 63
    return-void
.end method

.method protected abstract onRequest()V
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->b:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    .line 28
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;)V

    return-void
.end method
