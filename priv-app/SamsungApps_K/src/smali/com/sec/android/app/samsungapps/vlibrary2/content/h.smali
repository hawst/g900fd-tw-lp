.class final Lcom/sec/android/app/samsungapps/vlibrary2/content/h;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    if-nez p2, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;Z)V

    .line 73
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->isValid()Z

    move-result v0

    if-nez v0, :cond_2

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;Z)V

    goto :goto_0

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_IGetDetailRelatedCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setDetailRelated(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;Z)V

    goto :goto_0
.end method
