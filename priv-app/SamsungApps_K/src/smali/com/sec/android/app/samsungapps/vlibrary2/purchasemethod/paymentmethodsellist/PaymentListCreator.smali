.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentListCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createList(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;
    .locals 4

    .prologue
    .line 11
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;-><init>()V

    .line 12
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 14
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V

    invoke-interface {v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    goto :goto_0

    .line 16
    :cond_0
    return-object v1
.end method
