.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
.source "ProGuard"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private _bChooseTobeDeleted:Z

.field public categoryID:Ljava/lang/String;

.field public categoryName:Ljava/lang/String;

.field public forSaleYn:Ljava/lang/String;

.field private mPosition:I

.field public shopID:Ljava/lang/String;

.field public wishListID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ai;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ai;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Landroid/os/Parcel;)V

    .line 31
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->mPosition:I

    .line 33
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->_bChooseTobeDeleted:Z

    .line 17
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 31
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->mPosition:I

    .line 33
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->_bChooseTobeDeleted:Z

    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 12
    return-void
.end method


# virtual methods
.method public getPosition()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->mPosition:I

    return v0
.end method

.method public isSelectedToBeDeleted()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->_bChooseTobeDeleted:Z

    return v0
.end method

.method public resetDeleteSelection()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->_bChooseTobeDeleted:Z

    .line 42
    return-void
.end method

.method public selDeleteSelection(Z)V
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->_bChooseTobeDeleted:Z

    .line 48
    return-void
.end method

.method public setPosition(I)V
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->mPosition:I

    .line 52
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->writeToParcel(Landroid/os/Parcel;I)V

    .line 23
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 24
    return-void
.end method
