.class public Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;


# static fields
.field private static c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->a:Landroid/content/Context;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->a:Landroid/content/Context;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->a:Landroid/content/Context;

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->b:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;

    return-object v0
.end method


# virtual methods
.method public end()V
    .locals 1

    .prologue
    .line 80
    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_0
    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    .line 88
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public start(Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;)V
    .locals 2

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->b:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialogResult;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->end()V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 46
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->a:Landroid/content/Context;

    .line 51
    :cond_0
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;-><init>(Landroid/content/Context;)V

    .line 52
    sput-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->setCancelable(Z)V

    .line 53
    sget-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-static {}, Lcom/sec/android/app/samsungapps/NotiDialog;->getSearchKeyDisabled()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 54
    sget-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/loadingDialog/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/loadingDialog/a;-><init>(Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 63
    sget-object v0, Lcom/sec/android/app/samsungapps/loadingDialog/CancellableLoadingDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsLoadingDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
