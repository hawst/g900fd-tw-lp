.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final transient BASE_EQUAL_VER:I = 0x0

.field public static final transient BASE_HIGH_VER:I = 0x1

.field public static final transient BASE_LOW_VER:I = 0x2

.field public static final transient CREATOR:Landroid/os/Parcelable$Creator;

.field private static final serialVersionUID:J = 0x2e589504fc5ebb3cL


# instance fields
.field public GUID:Ljava/lang/String;

.field private _SINAContent:Z

.field private _isKNOXApp:Z

.field private _isTerminatedContent:Z

.field private _mIsSSuggestContent:Z

.field private transient _mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

.field public averageRating:I

.field public categoryDescription:Ljava/lang/String;

.field public categoryName:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public correctedKeyword:Ljava/lang/String;

.field public curatedDescription:Ljava/lang/String;

.field public currencyUnit:Ljava/lang/String;

.field public date:Ljava/lang/String;

.field public discountFlag:Z

.field public discountPrice:D

.field public idx:Ljava/lang/String;

.field public linkProductYn:Ljava/lang/String;

.field public listDescription:Ljava/lang/String;

.field public listLinkUrl:Ljava/lang/String;

.field public listTitle:Ljava/lang/String;

.field public loadType:Ljava/lang/String;

.field public newProductYn:Ljava/lang/String;

.field public price:D

.field public productID:Ljava/lang/String;

.field public productImgUrl:Ljava/lang/String;

.field public productName:Ljava/lang/String;

.field public promotionDescription:Ljava/lang/String;

.field public realContentSize:I

.field public sellerName:Ljava/lang/String;

.field public sourcename:Ljava/lang/String;

.field public version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 557
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/m;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/m;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>()V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isTerminatedContent:Z

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 152
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>(Landroid/os/Parcel;)V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isTerminatedContent:Z

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 153
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 154
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isTerminatedContent:Z

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 127
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {p1, v0, p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    const-string v1, "[S_I_N_A]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    const-string v1, "[S_I_N_A]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 131
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 132
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    const-string v1, "_terminated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    const-string v1, "_terminated"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 137
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 138
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isTerminatedContent:Z

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    const-string v1, "Samsung KNOX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    const-string v1, "for KNOX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 148
    :cond_3
    return-void
.end method

.method public constructor <init>(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>()V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isTerminatedContent:Z

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 117
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Ljava/io/ObjectInputStream;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;-><init>()V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isTerminatedContent:Z

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 111
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 113
    return-void
.end method

.method private getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 480
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 483
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 484
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 490
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    .line 492
    const-string v0, ""

    move-object v3, p1

    move-object p1, v0

    move-object v0, v3

    .line 498
    :goto_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 506
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 494
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 495
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 502
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 511
    :cond_1
    return-object v1
.end method

.method private getLong(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 300
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 305
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private isKnox2Mode()Z
    .locals 1

    .prologue
    .line 449
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 455
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 454
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private isPostLoad()Z
    .locals 2

    .prologue
    .line 320
    const-string v0, "1"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isVercodeValid()Z
    .locals 1

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getVersionCode()Ljava/lang/String;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0

    .prologue
    .line 569
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 570
    return-void
.end method


# virtual methods
.method public compareID(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 191
    const/4 v0, 0x1

    .line 193
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public compareVersion(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    .prologue
    .line 515
    const/4 v1, 0x0

    .line 517
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 518
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 521
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v0, v4, :cond_0

    .line 522
    const/4 v0, -0x1

    .line 542
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 549
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 551
    return v0

    .line 526
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 527
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 529
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 530
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 531
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 533
    if-eq v6, v0, :cond_1

    .line 534
    if-le v6, v0, :cond_2

    .line 538
    const/4 v0, 0x1

    .line 539
    goto :goto_0

    .line 541
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getCuratedDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->curatedDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    .line 211
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    goto :goto_0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVGUID()Ljava/lang/String;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getListDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getListLinkUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getListTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->loadType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    .line 174
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getProductID()Ljava/lang/String;

    move-result-object v0

    .line 186
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductImageURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductImgUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductImgUrl()Ljava/lang/String;

    move-result-object v0

    .line 361
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productImgUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductName()Ljava/lang/String;

    move-result-object v0

    .line 232
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 271
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRealContentSize()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    .line 273
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->realContentSize:I

    int-to-long v1, v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    goto :goto_0
.end method

.method public getScreenShot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    return-object v0
.end method

.method public getVGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getVaverageRating()F
    .locals 4

    .prologue
    .line 423
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->averageRating:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public getVcategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getVcategoryID2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryID2:Ljava/lang/String;

    return-object v0
.end method

.method public getVcategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getVcategoryName2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName2:Ljava/lang/String;

    return-object v0
.end method

.method public getVcontentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getVcurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getVdiscountFlag()Z
    .locals 1

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountFlag:Z

    return v0
.end method

.method public getVdiscountPrice()D
    .locals 2

    .prologue
    .line 413
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountPrice:D

    return-wide v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->version:Ljava/lang/String;

    .line 288
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->version:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->versionCode:Ljava/lang/String;

    .line 281
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->versionCode:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVprice()D
    .locals 2

    .prologue
    .line 408
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    return-wide v0
.end method

.method public getVproductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getVproductImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getVproductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    return-object v0
.end method

.method public isFreeContent()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    if-nez v2, :cond_3

    .line 217
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountFlag:Z

    if-eqz v2, :cond_2

    .line 218
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountPrice:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 218
    goto :goto_0

    .line 220
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 223
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isFreeContent()Z

    move-result v0

    goto :goto_0
.end method

.method public isKNOXApp()Z
    .locals 1

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isKnoxApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    const/4 v0, 0x1

    .line 468
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_isKNOXApp:Z

    goto :goto_0
.end method

.method public isLinkApp()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 439
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->linkProductYn:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->linkProductYn:Ljava/lang/String;

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 442
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isVercodeValid()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 329
    invoke-virtual {p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v2

    .line 331
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 348
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getVersionCode()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-lez v2, :cond_0

    move v0, v1

    .line 336
    goto :goto_0

    .line 341
    :cond_2
    invoke-virtual {p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPacakgeVersionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->compareVersion(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 345
    if-ne v2, v1, :cond_0

    move v0, v1

    .line 346
    goto :goto_0
.end method

.method public isOtherStoreContent()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSINAContent()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_SINAContent:Z

    return v0
.end method

.method public isSSuggestContent()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    return v0
.end method

.method protected isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->hasOrderID()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isPostLoad()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "3"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    const/4 v0, 0x0

    .line 314
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v0

    goto :goto_0
.end method

.method public setContentSize(J)V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setContentSize(J)V

    .line 238
    long-to-int v0, p1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->realContentSize:I

    .line 239
    return-void
.end method

.method public setListDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listDescription:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public setListTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listTitle:Ljava/lang/String;

    .line 247
    return-void
.end method

.method public setSSuggestContent(Z)V
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mIsSSuggestContent:Z

    .line 72
    return-void
.end method

.method public setScreenShot(Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->_mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 198
    return-void
.end method

.method public writeObjectOutStream(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 122
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Ljava/io/ObjectOutputStream;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 123
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->writeToParcel(Landroid/os/Parcel;I)V

    .line 161
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 162
    return-void
.end method
