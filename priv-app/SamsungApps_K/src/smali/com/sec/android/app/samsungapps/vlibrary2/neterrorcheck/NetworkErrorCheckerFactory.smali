.class public Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;


# instance fields
.field mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

.field mTurkeyPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;->mTurkeyPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 12
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    .line 13
    return-void
.end method


# virtual methods
.method public create()Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorChecker;
    .locals 3

    .prologue
    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;->mTurkeyPopup:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V

    return-object v0
.end method
