.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILogoutCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;

.field private loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->_ILogoutCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->notifyLogoutSuccess(Z)V

    return-void
.end method

.method private getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    return-object v0
.end method

.method private notifyLogoutSuccess(Z)V
    .locals 1

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 52
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getAccountEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->logoutSuccess()V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getAccountEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->logoutFailed()V

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->_ILogoutCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;->getLoadingDialogCreator()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;->loading:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/r;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->logOut(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 48
    return-void
.end method
