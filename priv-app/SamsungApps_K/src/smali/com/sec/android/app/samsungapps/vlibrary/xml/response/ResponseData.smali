.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mErrorCode:Ljava/lang/String;

.field private mErrorString:Ljava/lang/String;

.field private mObjectMapList:Ljava/util/ArrayList;

.field private mResponseHeaderMap:Ljava/util/HashMap;

.field private mSamsungProtocolMap:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mSamsungProtocolMap:Ljava/util/HashMap;

    .line 8
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mResponseHeaderMap:Ljava/util/HashMap;

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mObjectMapList:Ljava/util/ArrayList;

    .line 10
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mErrorCode:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mErrorString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addObjectMap(Ljava/util/HashMap;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mObjectMapList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public addResponseHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mResponseHeaderMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    return-void
.end method

.method public addSamsungProtocol(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mSamsungProtocolMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    return-void
.end method

.method public getErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mErrorCode:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mErrorString:Ljava/lang/String;

    return-object v0
.end method

.method public getObjectMapList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mObjectMapList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResponseHeaderMap()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mResponseHeaderMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getSamsungProtocolMap()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mSamsungProtocolMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public setErrorCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mErrorCode:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setErrorString(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->mErrorString:Ljava/lang/String;

    .line 45
    return-void
.end method
