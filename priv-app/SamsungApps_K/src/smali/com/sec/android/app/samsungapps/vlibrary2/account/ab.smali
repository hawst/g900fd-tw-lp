.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->endLoading()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)V

    .line 60
    if-eqz p2, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;->setPasswordConfirmedTime()V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;->onPasswordConfirmed(Z)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;Z)V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;->onPasswordConfirmed(Z)V

    goto :goto_0
.end method
