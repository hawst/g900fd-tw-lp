.class final Lcom/sec/android/app/billing/helper/m;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/helper/UPHelper;

.field private final synthetic b:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/helper/UPHelper;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/billing/helper/m;->a:Lcom/sec/android/app/billing/helper/UPHelper;

    iput-object p2, p0, Lcom/sec/android/app/billing/helper/m;->b:Landroid/os/Handler;

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 177
    const-string v0, "UPHelper"

    const-string v1, "[UP_API_VERSION : 20016] startSetup() : onServiceConnected() : [UP_CLIENT_OK] Billing service connected."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-static {p2}, Lcom/sec/android/app/billing/service/IBillingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/billing/service/IBillingService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Lcom/sec/android/app/billing/service/IBillingService;)V

    .line 180
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Z)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/m;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 183
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 184
    iput v2, v0, Landroid/os/Message;->what:I

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/billing/helper/m;->a:Lcom/sec/android/app/billing/helper/UPHelper;

    const-string v1, "Billing service connected."

    invoke-static {v2, v1}, Lcom/sec/android/app/billing/helper/UPHelper;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/billing/helper/m;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 188
    :cond_0
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 169
    const-string v0, "UPHelper"

    const-string v1, "[UP_API_VERSION : 20016] startSetup() : onServiceDisconnected() : Billing service disconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Lcom/sec/android/app/billing/service/IBillingService;)V

    .line 172
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Z)V

    .line 173
    return-void
.end method
