.class public Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final JELLY_BEAN:I = 0x10


# instance fields
.field private errorType:I

.field private mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

.field private mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->notifyFailedNeedRetry()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->notifyFailed()V

    return-void
.end method

.method private getNetworkErrorType()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->errorType:I

    return v0
.end method

.method private isOccurredNetworkError(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 88
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isWIFIConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/CheckNetworkError;->getNetworkErrorType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->errorType:I

    .line 93
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->errorType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 95
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPhoneTypeNone()Z
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getExtraPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPlatformVersionJellyBeanOrAbove()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 62
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    .line 63
    if-nez v1, :cond_1

    .line 65
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getOpenApiVersion()Ljava/lang/String;

    move-result-object v1

    .line 69
    if-nez v1, :cond_2

    .line 71
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    const-string v1, "Main::isConnectedNetwork::NumberFormatException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_2
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 79
    :catch_1
    move-exception v1

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Main::isConnectedNetwork::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private notifyFailed()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;->onFailed()V

    .line 110
    return-void
.end method

.method private notifyFailedNeedRetry()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;->onFailedNeedRetry()V

    .line 115
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->isPlatformVersionJellyBeanOrAbove()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->isPhoneTypeNone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->isOccurredNetworkError(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->mNetworkErrorPopup:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->getNetworkErrorType()I

    move-result v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;)V

    invoke-interface {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;->showJellyBeanNetworkPopup(Landroid/content/Context;ILcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;)V

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/JellyBeanNetworkErrorChecker$IJellyBeanNetworkErrCheckObserver;->onSuccess()V

    goto :goto_0
.end method
