.class final Lcom/sec/android/app/samsungapps/vlibrary2/update/m;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onResult(ZLcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 66
    if-ne p1, v7, :cond_3

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->getUpdateAppCount()I

    move-result v1

    .line 69
    if-eqz v1, :cond_1

    .line 71
    :goto_0
    if-ge v0, v1, :cond_2

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mSystemAppList:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->getUpdateApp(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;

    move-result-object v2

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->removeUpdateList(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;)Z

    move-result v3

    if-eq v3, v7, :cond_0

    .line 76
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->_Context:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/update/n;

    invoke-direct {v5, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/update/m;)V

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->mInstallerFactory:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
    invoke-static {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v6

    invoke-direct {v3, v4, v2, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader$SAUtilDownloadResultListener;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 85
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/SAUtilDownloader;->downloadAndInstall()V

    .line 71
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onFinalResult(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Z)V

    .line 100
    :cond_2
    :goto_1
    return-void

    .line 97
    :cond_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->stopLoadingDialog()V

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/m;->a:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->onFinalResult(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;Z)V

    goto :goto_1
.end method
