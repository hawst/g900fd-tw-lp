.class public Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;
.super Landroid/app/Activity;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c0306

    if-ne v0, v1, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->finish()V

    .line 30
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->requestWindowFeature(I)Z

    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f0400eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->setContentView(I)V

    .line 22
    const v0, 0x7f0c02f5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x7f0c02f6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f08027d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f080319

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0c0305

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c0306

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushInfoAcitivty;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    :cond_0
    return-void
.end method
