.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;
.source "ProGuard"


# instance fields
.field private OnReceiveProductDetail:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field private _ProductDetailParser:Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_ProductDetailParser:Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/s;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/s;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->OnReceiveProductDetail:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_ProductDetailParser:Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected createRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UncModeChecker;->isUncMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_ProductDetailParser:Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;

    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_ProductDetailParser:Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->OnReceiveProductDetail:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 32
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/content/r;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/r;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqProductDetailCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    goto :goto_0
.end method
