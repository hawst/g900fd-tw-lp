.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/Request;
.source "ProGuard"


# instance fields
.field private _200OK:Z

.field private _ResponsePOST:Lcom/sec/android/app/samsungapps/vlibrary/net/ResponsePOST;

.field private _bDontDisplayErrorPoupu:Z

.field private bBackgroundRetryBlocked:Z

.field bCanceled:Z

.field private bSuppressReceivedLog:Z

.field errorcodesNotToDisplay:Ljava/util/ArrayList;

.field handler:Landroid/os/Handler;

.field mHttpPost:Lorg/apache/http/client/methods/HttpPost;

.field mHttpPostClient:Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;

.field protected mIXMLResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;

.field private mNetStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

.field mPostProcessor:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;

.field private mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

.field private mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

.field private mResultXml:Ljava/lang/String;

.field netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

.field retryCount:I

.field stageNo:I

.field xmlgenerator:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 156
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->xmlgenerator:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mPostProcessor:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    .line 32
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->stageNo:I

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bSuppressReceivedLog:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_200OK:Z

    .line 72
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handler:Landroid/os/Handler;

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bBackgroundRetryBlocked:Z

    .line 370
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mResultXml:Ljava/lang/String;

    .line 434
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->retryCount:I

    .line 481
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bCanceled:Z

    .line 498
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->errorcodesNotToDisplay:Ljava/util/ArrayList;

    .line 526
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_bDontDisplayErrorPoupu:Z

    .line 157
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->xmlgenerator:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;

    .line 158
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mPostProcessor:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;

    .line 159
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mIXMLResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;

    .line 160
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyResultToUI()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    return-object v0
.end method

.method private increaseRetryCount()V
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->retryCount:I

    .line 438
    return-void
.end method

.method private isRetryCountExpired()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 442
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bBackgroundRetryBlocked:Z

    if-eqz v1, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->retryCount:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyFinishSending()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 276
    return-void
.end method

.method private notifyResultToUI()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "notifyResultToUI pre"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mNetStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/b;->b()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->stageNo:I

    if-eq v0, v1, :cond_0

    .line 135
    :goto_0
    return-void

    .line 115
    :cond_0
    const-string v0, "notifyResultToUI"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isSucceed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->hasErrorPreProcessor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->releaseErrorPreProcessor()V

    .line 122
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isSucceed()Z

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getNetError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v1

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->hasErrorPreProcessor()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getNetError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;->processError(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isSucceed()Z

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getNetError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v1

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0
.end method

.method private notifyStartSending()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 271
    return-void
.end method

.method private onReceiveXMLText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->printLogIfLogisEnabled(Ljava/lang/String;)V

    .line 199
    if-eqz p1, :cond_2

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mPostProcessor:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;

    if-nez v0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->parseXML(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;

    move-result-object v0

    .line 208
    if-nez v0, :cond_3

    .line 210
    const-string v1, "parseError"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    if-eqz v1, :cond_1

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setParseError()V

    .line 221
    :cond_1
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setErrorFromResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->printErrorCodeIfLogIsSuppressed()V

    .line 225
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V

    goto :goto_0

    .line 218
    :cond_3
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->onParsingDone(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V

    goto :goto_1
.end method

.method private parseXML(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mPostProcessor:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IPostProcessor;->parseXML(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;

    move-result-object v0

    .line 187
    return-object v0
.end method

.method private printErrorCodeIfLogIsSuppressed()V
    .locals 3

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bSuppressReceivedLog:Z

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    .line 241
    if-nez v0, :cond_0

    .line 243
    const-string v0, ""

    .line 245
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->isLoggingEnabled()Lcom/sec/android/app/samsungapps/vlibrary/util/LogMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/LogMode;->isLogMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "errorcode=="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 251
    :cond_1
    return-void
.end method

.method private printLogIfLogisEnabled(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->isLoggingEnabled()Lcom/sec/android/app/samsungapps/vlibrary/util/LogMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/LogMode;->isLogMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bSuppressReceivedLog:Z

    if-nez v0, :cond_0

    .line 179
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 182
    :cond_0
    return-void
.end method

.method private releaseErrorPreProcessor()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;->release()V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 148
    :cond_0
    return-void
.end method


# virtual methods
.method public addDontDisplayErrorCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->errorcodesNotToDisplay:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    return-void
.end method

.method public applyFakeModelIfNeeds(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 538
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 539
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 541
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setFakeModel(Ljava/lang/String;)V

    .line 543
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getGearOSVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 546
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->setGearOSVersion(Ljava/lang/String;)V

    .line 548
    :cond_1
    return-void
.end method

.method public cancel()Z
    .locals 1

    .prologue
    .line 477
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bCanceled:Z

    .line 478
    const/4 v0, 0x0

    return v0
.end method

.method protected concrete_sendpacket(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/net/g;
    .locals 3

    .prologue
    .line 379
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_200OK:Z

    .line 380
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;

    invoke-direct {v0, p3, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mHttpPostClient:Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mHttpPostClient:Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->execute(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)Z

    move-result v0

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mHttpPostClient:Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->getResultString()Ljava/lang/String;

    move-result-object v1

    .line 388
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mHttpPostClient:Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/HTTPPostClient;->is200OK()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_200OK:Z

    .line 390
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/net/g;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/g;-><init>()V

    .line 391
    iput-boolean v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->b:Z

    .line 392
    iput-object v1, v2, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->a:Ljava/lang/String;

    .line 394
    return-object v2
.end method

.method public dontDisplayErrorPopup()V
    .locals 1

    .prologue
    .line 528
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_bDontDisplayErrorPoupu:Z

    .line 529
    return-void
.end method

.method public getNetError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    return-object v0
.end method

.method public getReqID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;->getReqID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReqName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;->getReqName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponse()Lcom/sec/android/app/samsungapps/vlibrary/net/ResponsePOST;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_ResponsePOST:Lcom/sec/android/app/samsungapps/vlibrary/net/ResponsePOST;

    return-object v0
.end method

.method public getResultXml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mResultXml:Ljava/lang/String;

    return-object v0
.end method

.method public getStageNo()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->stageNo:I

    return v0
.end method

.method public final getThreadNo()I
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;->getURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXML()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->xmlgenerator:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;->getXML()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleReceiveResult(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 399
    if-eqz p1, :cond_0

    .line 401
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->onReceiveXMLText(Ljava/lang/String;)V

    .line 413
    :goto_0
    return-void

    .line 405
    :cond_0
    if-eqz p2, :cond_1

    .line 407
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setHttpError()V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V

    goto :goto_0
.end method

.method protected handleResultInUIThread()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 261
    return-void
.end method

.method public is200OK()Z
    .locals 1

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_200OK:Z

    return v0
.end method

.method public isBackgroundRetryBlocked()Z
    .locals 1

    .prologue
    .line 422
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bBackgroundRetryBlocked:Z

    return v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 484
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bCanceled:Z

    return v0
.end method

.method public isDisplayingErrorPopupSuppressed()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_bDontDisplayErrorPoupu:Z

    return v0
.end method

.method public final isPOST()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x1

    return v0
.end method

.method public isSucceed()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    if-eqz v1, :cond_0

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->isError()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 431
    :cond_0
    return v0
.end method

.method public isSuppressedErrorCode(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->errorcodesNotToDisplay:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 508
    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 510
    const/4 v0, 0x1

    .line 513
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needRetry()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isSucceed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 459
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bBackgroundRetryBlocked:Z

    if-nez v1, :cond_0

    .line 463
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isRetryCountExpired()Z

    move-result v1

    if-nez v1, :cond_0

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->EXCEPTION:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    if-ne v1, v2, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->increaseRetryCount()V

    .line 470
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onParsingDone(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mIXMLResultReceiver:Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;->onReceiveParsingResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V

    .line 193
    return-void
.end method

.method public send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
    .locals 3

    .prologue
    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net)"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->clearErrorCondition()V

    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyStartSending()V

    .line 282
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->sendPostPacket(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 338
    :goto_0
    return-void

    .line 286
    :catch_0
    move-exception v0

    .line 287
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) ClientProtocolException"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 288
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setException(Ljava/lang/Exception;)V

    .line 291
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isRetryCountExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 295
    :catch_1
    move-exception v0

    .line 296
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) SocketTimeoutException"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setTimeoutException(Ljava/lang/Exception;)V

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 336
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 301
    :catch_2
    move-exception v0

    .line 302
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) ConnectTimeoutException"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setConnectionTimeoutException(Lorg/apache/http/conn/ConnectTimeoutException;)V

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 336
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308
    :catch_3
    move-exception v0

    .line 310
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) IllegalArgumentException"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setException(Ljava/lang/Exception;)V

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 336
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 317
    :catch_4
    move-exception v0

    .line 319
    :try_start_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) IllegalStateException"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setException(Ljava/lang/Exception;)V

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 336
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    :catch_5
    move-exception v0

    .line 327
    :try_start_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) IOException"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 328
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->setException(Ljava/lang/Exception;)V

    .line 331
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->isRetryCountExpired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleResultInUIThread()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 336
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "send(NetAPI net) complete"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 336
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->notifyFinishSending()V

    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send(NetAPI net) complete"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    throw v0
.end method

.method protected sendPostPacket(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->xmlgenerator:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;->getXML()Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getURL()Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 348
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->isLoggingEnabled()Lcom/sec/android/app/samsungapps/vlibrary/util/LogMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/LogMode;->isLogMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->xmlgenerator:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;->getLogXML()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 354
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->concrete_sendpacket(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/net/g;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 363
    :goto_0
    iget-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->b:Z

    .line 364
    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->a:Ljava/lang/String;

    .line 365
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mResultXml:Ljava/lang/String;

    .line 367
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->handleReceiveResult(ZLjava/lang/String;)V

    .line 368
    return-void

    .line 355
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 358
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/g;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/g;-><init>()V

    .line 359
    const-string v1, ""

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->a:Ljava/lang/String;

    .line 360
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/net/g;->b:Z

    goto :goto_0
.end method

.method public setDisableBackgroundRetry()V
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bBackgroundRetryBlocked:Z

    .line 418
    return-void
.end method

.method protected setErrorFromResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getServerErrorInfo()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->netError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getServerErrorInfo()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->checkServerError(Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;)V

    .line 233
    :cond_0
    return-void
.end method

.method protected setFakeModel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;->setFakeModel(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method protected setGearOSVersion(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;->setGearOSVersion(Ljava/lang/String;)V

    .line 553
    return-void
.end method

.method public setNetStageCounter(Lcom/sec/android/app/samsungapps/vlibrary/net/b;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mNetStageCounter:Lcom/sec/android/app/samsungapps/vlibrary/net/b;

    .line 70
    return-void
.end method

.method public setReqSendingProgressObserver(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->mProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    .line 106
    return-void
.end method

.method public setResponse(Lcom/sec/android/app/samsungapps/vlibrary/net/ResponsePOST;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->_ResponsePOST:Lcom/sec/android/app/samsungapps/vlibrary/net/ResponsePOST;

    .line 524
    return-void
.end method

.method public setStageNo(I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->stageNo:I

    .line 60
    return-void
.end method

.method public suppressReceivedLog()V
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->bSuppressReceivedLog:Z

    .line 256
    return-void
.end method
