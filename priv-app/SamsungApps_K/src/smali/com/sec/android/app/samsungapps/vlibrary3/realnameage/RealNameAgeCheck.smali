.class public Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheck;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

.field private b:Landroid/os/Handler;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->b:Landroid/os/Handler;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->c:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    .line 43
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 44
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 36
    return-void
.end method


# virtual methods
.method public check()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    .line 49
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Action;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Action;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    if-eqz v2, :cond_5

    .line 66
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_2
    if-eqz v0, :cond_4

    .line 67
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_ALREADY_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    goto :goto_0

    .line 65
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isNameAuthRequired(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)Z

    move-result v2

    goto :goto_1

    :cond_3
    move v0, v1

    .line 66
    goto :goto_2

    .line 69
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_REQUIRED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    goto :goto_0

    .line 72
    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_CHECK_IS_NOT_NEEDED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    goto :goto_0

    .line 76
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getRealAge()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRestrictedAge()I

    move-result v1

    if-ge v0, v1, :cond_6

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->AGE_RESTRICTED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CHECK_CONTENT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    goto :goto_0

    .line 79
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;->onRealAgeCheckResult(Z)V

    goto/16 :goto_0

    .line 82
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm$IRealNameAgeConfirmObserver;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->execute()V

    goto/16 :goto_0

    .line 86
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;->onRealAgeCheckResult(Z)V

    goto/16 :goto_0

    .line 89
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->c:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Action;)V

    return-void
.end method

.method public onConfirmNameAgeResult(Z)V
    .locals 1

    .prologue
    .line 156
    if-eqz p1, :cond_0

    .line 157
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CONFRIM_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    goto :goto_0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->f:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;

    .line 132
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

    .line 54
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$State;)V

    return-void
.end method

.method public userOk()V
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;->USER_OK:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck;->a(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheckStateMachine$Event;)V

    .line 100
    return-void
.end method
