.class public Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;


# static fields
.field private static final serialVersionUID:J = -0x649f640aa4e336d6L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public clearSel()Z
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    .line 62
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;->setSelect(Z)V

    goto :goto_0

    .line 64
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    return-object v0
.end method

.method public selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v2

    .line 20
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;->isSelected()Z

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    .line 28
    if-ne v0, p1, :cond_5

    move v0, v3

    :goto_2
    move v1, v0

    .line 30
    goto :goto_1

    .line 33
    :cond_2
    if-eqz v1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    .line 40
    if-ne v0, p1, :cond_3

    .line 42
    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;->setSelect(Z)V

    goto :goto_3

    .line 46
    :cond_3
    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;->setSelect(Z)V

    goto :goto_3

    :cond_4
    move v2, v3

    .line 49
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method
