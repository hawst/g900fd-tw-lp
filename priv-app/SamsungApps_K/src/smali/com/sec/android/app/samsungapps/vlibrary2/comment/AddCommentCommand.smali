.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

.field private mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

.field private mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->onFinalResult(Z)V

    return-void
.end method

.method private checkRatingAuthority()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;->onRequestRatingAuthority()V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->checkRatingAuthorityCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/comment/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 71
    return-void
.end method

.method private getRegisterCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)V

    return-object v0
.end method


# virtual methods
.method public addComment()V
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;->onCommentRegistering()V

    .line 123
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->getRegisterCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentRegister(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 156
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 157
    return-void
.end method

.method public getProhibitWords()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRatingValue()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->getRatingValue()I

    move-result v0

    return v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->getAddCommentViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mICommentAddView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$ICommentAddView;

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->checkRatingAuthority()V

    .line 53
    return-void
.end method

.method public isMyReviewDuplicated()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->mIAddCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand$IAddCommentCommandData;->isMyReviewDuplicated()Z

    move-result v0

    return v0
.end method
