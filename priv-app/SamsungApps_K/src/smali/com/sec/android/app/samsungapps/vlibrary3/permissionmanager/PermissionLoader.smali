.class public Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;
.super Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;

.field private d:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandler;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->a:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->d:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->onReceiveSuccess()V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->onReceiveFailed()V

    return-void
.end method


# virtual methods
.method protected onNotifyResult(Z)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->c:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->c:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;->onResult(Z)V

    .line 37
    :cond_0
    return-void
.end method

.method protected onRequest()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/a;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;Landroid/content/Context;Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/a;->request()V

    .line 43
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->c:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;

    .line 29
    return-void
.end method
