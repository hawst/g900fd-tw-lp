.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract add(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
.end method

.method public abstract clear()V
.end method

.method public abstract clearSel()Z
.end method

.method public abstract get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;
.end method

.method public abstract selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
.end method

.method public abstract selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;Landroid/content/Context;)Z
.end method

.method public abstract size()I
.end method
