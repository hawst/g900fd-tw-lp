.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;
.source "ProGuard"


# instance fields
.field private _CheckRestrictedAge:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _VerifyRealAge:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field justFail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ISwitch;-><init>()V

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/x;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/x;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->justFail:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_VerifyRealAge:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 18
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_CheckRestrictedAge:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;Z)V
    .locals 0

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public nameAgeAlreadyVerified()Z
    .locals 2

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nameAgeVerificationRequired()Z
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isNameAuthRequired(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)Z

    move-result v0

    goto :goto_0
.end method

.method protected selectCommandToExecute()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->nameAgeVerificationRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->nameAgeAlreadyVerified()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_VerifyRealAge:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 31
    :goto_0
    return-object v0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->_CheckRestrictedAge:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    goto :goto_0

    .line 31
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/content/VerifyRealAgeIfNeeded;->dummyCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_0
.end method
