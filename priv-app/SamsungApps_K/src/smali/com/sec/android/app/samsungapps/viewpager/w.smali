.class final Lcom/sec/android/app/samsungapps/viewpager/w;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/w;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 122
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/w;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/w;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
