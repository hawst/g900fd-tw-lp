.class final Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 5

    .prologue
    .line 45
    if-eqz p2, :cond_1

    .line 47
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->getResult()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;

    .line 50
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;->getVproductID()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_ExcludeProductID:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    move-object v1, v0

    .line 52
    goto :goto_0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->getResult()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;->remove(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_SellerProductList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->getResult()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_IGetSellerProductListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->_SellerProductList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->setSellerProductList(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;Z)V

    .line 64
    :goto_2
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/c;->b:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;Z)V

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
