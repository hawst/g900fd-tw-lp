.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->d:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 25
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)Z

    .line 51
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;)V
    .locals 3

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    :goto_0
    return-void

    .line 43
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;->LOGIN_CHECK_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Event;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->d:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    .line 31
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/DownloadPreConditionCheckManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecondition/IDownloadPreConditionCheckState$State;)V

    return-void
.end method
