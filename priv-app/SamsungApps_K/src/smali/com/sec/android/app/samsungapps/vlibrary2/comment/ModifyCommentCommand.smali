.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

.field private mCommentID:Ljava/lang/String;

.field private mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

.field private mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentID:Ljava/lang/String;

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentID:Ljava/lang/String;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    return-object v0
.end method

.method private checkRatingAuthority()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;->onRequestRatingAuthority()V

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->checkRatingAuthorityCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/comment/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected getModifyCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V

    return-object v0
.end method

.method public getOldCommentText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentID:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->getCommentText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOldRating()D
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentID:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->getRatingValue(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getProhibitWords()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->validateLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 43
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->checkRatingAuthority()V

    .line 66
    return-void
.end method

.method public modifyComment()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;->onCommentModifying()V

    .line 124
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->getModifyCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentModify(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 141
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 142
    return-void
.end method
