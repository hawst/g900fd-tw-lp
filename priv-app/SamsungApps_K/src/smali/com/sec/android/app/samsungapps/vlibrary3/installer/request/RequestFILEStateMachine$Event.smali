.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CANCEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum DOWNLOADING_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum DOWNLOADING_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum DOWNLOAD_READY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum HANDOVER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum HANDOVER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum HEADER_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum NETWORK_ACTIVATED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum NORMAL_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum NOT_SUPPORT_RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum OPEN_COMPLETED_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum OPEN_EXISTING_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum OPEN_FILE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum OPEN_NEW_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum RESUME_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum SEND:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum URL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field public static final enum WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "SEND"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->SEND:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "OPEN_COMPLETED_FILE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_COMPLETED_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "OPEN_EXISTING_FILE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_EXISTING_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "OPEN_FILE_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_FILE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "OPEN_NEW_FILE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_NEW_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "HEADER_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HEADER_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "RESUME_FAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RESUME_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "DOWNLOAD_READY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOAD_READY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "REQUEST_FAIL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "CANCEL_COMPLETED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->CANCEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "DOWNLOADING_SUCCESS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOADING_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "DOWNLOADING_FAILED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOADING_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "TIMEOUT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "NOT_SUPPORT_RETRY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NOT_SUPPORT_RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "RETRY_COUNT_OVER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "RETRY_CONDITION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "URL_SUCCESS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->URL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "URL_FAILED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "NETWORK_NOT_AVAILABLE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "NETWORK_ACTIVATED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NETWORK_ACTIVATED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "NORMAL_HANDOVER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NORMAL_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "WIFI_TO_3G_HANDOVER"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "HANDOVER_AGREE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HANDOVER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    const-string v1, "HANDOVER_DISAGREE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HANDOVER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    .line 22
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->SEND:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_COMPLETED_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_EXISTING_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_FILE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->OPEN_NEW_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HEADER_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RESUME_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOAD_READY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->CANCEL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOADING_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->DOWNLOADING_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NOT_SUPPORT_RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->URL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NETWORK_ACTIVATED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->NORMAL_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HANDOVER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->HANDOVER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    return-object v0
.end method
