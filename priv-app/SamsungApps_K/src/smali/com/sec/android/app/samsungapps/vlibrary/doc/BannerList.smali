.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private bInProgress:Z

.field observers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->bInProgress:Z

    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->observers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 105
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Z)Z
    .locals 0

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->bInProgress:Z

    return p1
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->observers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 98
    return-void
.end method

.method notifyLoadCompleted(Z)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->observers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    .line 89
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;->loadCompleted(Z)V

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method notifyLoading()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->observers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;

    .line 81
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;->loading()V

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList$BannerListObserver;)Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->observers:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public request()V
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->bInProgress:Z

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->bInProgress:Z

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->notifyLoading()V

    .line 66
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getBannerList()Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->promotionBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 74
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method public request(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/f;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->bInProgress:Z

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->bInProgress:Z

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getBannerList()Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->promotionBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 39
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method
