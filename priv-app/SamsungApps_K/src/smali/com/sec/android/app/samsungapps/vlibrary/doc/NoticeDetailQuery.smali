.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static NOTICE_ID:Ljava/lang/String;


# instance fields
.field private mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

.field private mNoticeDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

.field private mNoticeID:Ljava/lang/String;

.field mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "noticeid"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->NOTICE_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    .line 81
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 29
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method public getNoticeContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    return-object v0
.end method

.method public getNoticeDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    return-object v0
.end method

.method public getNoticeID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeID:Ljava/lang/String;

    return-object v0
.end method

.method public loadNoticeDetail()V
    .locals 3

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/y;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/y;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->noticeDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 63
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 64
    return-void
.end method

.method protected notifyLoadCompleted(Z)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;

    .line 86
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;->noticeDetailLoadingCompleted(Z)V

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery$NoticeDetailObserver;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public setNoticeDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    .line 34
    return-void
.end method

.method public setNoticeID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->mNoticeID:Ljava/lang/String;

    .line 52
    return-void
.end method
