.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract deleteGiftCard(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getActivieGiftCardList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getExpiredGiftCardList()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getGiftCardList(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardList;
.end method

.method public abstract registerGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract release()V
.end method
