.class public Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;
.super Landroid/app/Service;
.source "ProGuard"


# static fields
.field public static final MAGAZINEHOME_FILE_PREFIX:Ljava/lang/String; = "MAGAZINEHOME_"

.field public static final MAGAZINEHOME_STORY_COUNT:I = 0xa


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/util/List;

.field private j:Ljava/util/List;

.field private k:Ljava/util/HashMap;

.field private l:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b:Z

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 52
    new-instance v0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService$MediaHomeImageResolution;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService$MediaHomeImageResolution;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->d:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    .line 53
    iput v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    .line 54
    iput v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->f:I

    .line 55
    iput v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->k:Ljava/util/HashMap;

    .line 63
    new-instance v0, Lcom/sec/android/app/samsungapps/magazinehome/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/magazinehome/a;-><init>(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->l:Landroid/os/Handler;

    .line 419
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    return p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->f:I

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->l:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected createStoryItem(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)Lsstream/lib/objs/StoryItem;
    .locals 13

    .prologue
    const-wide v5, 0x406a800000000000L    # 212.0

    const/4 v12, 0x0

    .line 251
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v4

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 257
    new-instance v7, Lsstream/lib/objs/Author;

    invoke-direct {v7, v0, v12}, Lsstream/lib/objs/Author;-><init>(Ljava/lang/String;Lsstream/lib/objs/Image;)V

    .line 259
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getWidth()I

    move-result v0

    const/16 v1, 0x200

    if-ge v0, v1, :cond_0

    .line 263
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getWidth()I

    move-result v1

    .line 264
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getHeight()I

    move-result v0

    .line 270
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Image size w: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", h: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 272
    new-instance v8, Lsstream/lib/objs/Image;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v2, v1, v0, v12}, Lsstream/lib/objs/Image;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Image File Path: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->generateStoryId(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Ljava/lang/String;

    move-result-object v1

    .line 277
    new-instance v0, Lsstream/lib/objs/StoryItem;

    const-string v2, "samsung.media"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    sget-object v6, Lsstream/lib/objs/StoryItem$StoryType;->ARTICLE:Lsstream/lib/objs/StoryItem$StoryType;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v12}, Lsstream/lib/objs/StoryItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsstream/lib/objs/StoryItem$StoryType;Lsstream/lib/objs/Author;Lsstream/lib/objs/Image;JILjava/lang/String;)V

    .line 280
    return-object v0

    .line 266
    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    mul-double/2addr v0, v5

    double-to-int v0, v0

    add-int/lit8 v1, v0, 0x64

    .line 267
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    mul-double/2addr v2, v5

    double-to-int v0, v2

    add-int/lit8 v0, v0, 0x64

    goto/16 :goto_0
.end method

.method protected deleteAllStoryItems()V
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "samsung.media"

    invoke-static {p0, v0, v1}, Lsstream/lib/SStreamContentManager;->deleteAllStoryItemsFromStream(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method protected deleteUnusedImageFiles()V
    .locals 5

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->fileList()[Ljava/lang/String;

    move-result-object v1

    .line 211
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 212
    aget-object v2, v1, v0

    const-string v3, "MAGAZINEHOME_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->k:Ljava/util/HashMap;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 213
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v2

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Deleteing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 211
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_1
    return-void
.end method

.method protected generateStoryId(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 285
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 286
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Content or GUID is NULL."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v0

    .line 290
    return-object v0
.end method

.method protected getImageFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MAGAZINEHOME_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 294
    iget v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    .line 295
    if-eqz p1, :cond_3

    iget v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    if-gt v1, v2, :cond_3

    .line 296
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 298
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v1

    .line 299
    const-string v2, " ITEM =========================="

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 300
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GUID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ProductName: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 302
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Image URL: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 303
    const-string v1, "================================"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 305
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->createStoryItem(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)Lsstream/lib/objs/StoryItem;

    move-result-object v1

    .line 306
    if-eqz v1, :cond_0

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->k:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current mFinishedRequestCount is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 317
    iget v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    if-ne v1, v2, :cond_2

    move v1, v0

    .line 320
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;

    .line 322
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->cancel()Z

    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called cancel of : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 320
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 311
    :catch_0
    move-exception v1

    .line 312
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MagazineHomeService::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 326
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->deleteAllStoryItems()V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->deleteUnusedImageFiles()V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->l:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 330
    iput v4, v0, Landroid/os/Message;->what:I

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 352
    :cond_2
    :goto_2
    return-void

    .line 333
    :cond_3
    iget v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 334
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    goto :goto_2

    :cond_4
    move v1, v0

    .line 338
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;

    .line 340
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->cancel()Z

    .line 341
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called cancel of : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 338
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 344
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->deleteAllStoryItems()V

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->deleteUnusedImageFiles()V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->l:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 348
    iput v4, v0, Landroid/os/Message;->what:I

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 123
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 129
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 180
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 181
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 134
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b:Z

    if-ne v0, v3, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    .line 174
    :goto_0
    return v3

    .line 141
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b:Z

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 147
    iput v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    .line 148
    iput v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    .line 149
    iput v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->f:I

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->h:Ljava/lang/String;

    .line 153
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createFeaturedHot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->d:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setImageResolution(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 158
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V

    .line 159
    new-instance v0, Lcom/sec/android/app/samsungapps/magazinehome/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/magazinehome/c;-><init>(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;)V

    goto :goto_0
.end method

.method protected requestFromWeb(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 378
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->isCanceled()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 379
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    .line 417
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v0

    .line 384
    if-nez v0, :cond_1

    .line 385
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    goto :goto_0

    .line 389
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/magazinehome/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/magazinehome/d;-><init>(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)V

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 414
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added on list : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method protected requestImages(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V
    .locals 6

    .prologue
    .line 356
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->f:I

    if-ge v1, v0, :cond_2

    .line 358
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 359
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->getImageFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a:Landroid/content/Context;

    invoke-direct {v2, v0, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Ljava/lang/String;Landroid/content/Context;)V

    .line 362
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 363
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". File "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is existing for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    .line 356
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 366
    :cond_0
    iget v3, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->g:I

    iget v4, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->e:I

    if-ge v3, v4, :cond_1

    .line 367
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->requestFromWeb(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    .line 368
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Request log for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 370
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->imageDownloadCallback(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;)V

    goto :goto_1

    .line 374
    :cond_2
    return-void
.end method

.method protected sendStoryItems()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    invoke-static {p0, v0}, Lsstream/lib/SStreamContentManager;->sendStoryItems(Landroid/content/Context;Ljava/util/List;)V

    .line 227
    const-string v0, "Sent story Items!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Lsstream/lib/InvalidStoryItemException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 237
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MagazineHomeService::InvalidStoryItemException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsstream/lib/InvalidStoryItemException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :catch_1
    move-exception v0

    .line 240
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MagazineHomeService::ArrayIndexOutOfBoundsException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected stopService()V
    .locals 2

    .prologue
    .line 191
    const-string v0, "Service for Magazine Home will stop"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b:Z

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopSelf()V

    .line 197
    return-void
.end method
