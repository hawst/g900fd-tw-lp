.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum China:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum France:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum Germany:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum Iran:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum Korea:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum Other:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

.field public static final enum Ukraine:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "Korea"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Korea:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 5
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "China"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->China:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "Ukraine"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Ukraine:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "Iran"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Iran:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "France"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->France:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "Germany"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Germany:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    const-string v1, "Other"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Other:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 3
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Korea:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->China:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Ukraine:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Iran:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->France:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Germany:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Other:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    return-object v0
.end method
