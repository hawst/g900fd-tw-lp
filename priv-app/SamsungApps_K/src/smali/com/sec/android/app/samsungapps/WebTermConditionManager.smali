.class public Lcom/sec/android/app/samsungapps/WebTermConditionManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static c:Lcom/sec/android/app/samsungapps/LoadingDialog;


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    .line 21
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 163
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/SamsungAppsWebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 164
    const-string v1, "webViewUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/content/Context;)V

    .line 26
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->isExistSamsungAccount()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onBasicAccount()V
    .locals 4

    .prologue
    .line 56
    const-string v0, "%s%s.txt"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "http://static.bada.com/contents/deviceterms/"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openMandatoryWeb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onBasicPolicy()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    const-string v1, "450"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 129
    const-string v0, "http://static.bada.com/contents/legal/kor/kor/pp.txt"

    .line 137
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openMandatoryWeb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_1
    return-void

    .line 133
    :cond_0
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "http://www.samsungapps.com/common/privacy.as?mcc="

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onNewSamsungAccount()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    const-string v1, "onNewSamsungAccount() TODO!!"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method protected onNewSamsungAccountPolicy()V
    .locals 3

    .prologue
    .line 147
    const-string v0, "http://static.bada.com/contents/legal/mcc=/all/pp.txt"

    .line 149
    const-string v1, "mcc="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    const-string v1, "mcc="

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 154
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openMandatoryWeb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showAccountTNC()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->onNewSamsungAccount()V

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->onBasicAccount()V

    goto :goto_0
.end method

.method public showPrivacyPolicy()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->onNewSamsungAccountPolicy()V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->onBasicPolicy()V

    goto :goto_0
.end method

.method public showSamsungAppsTNC()V
    .locals 4

    .prologue
    .line 44
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "http://www.samsungapps.com/common/term.as?mcc="

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 47
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openMandatoryWeb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showTermsAndConditions()V
    .locals 4

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    const-string v0, "http://static.bada.com/contents/legal/mcc=/all/allinone.txt"

    .line 80
    const-string v1, "mcc="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const-string v1, "mcc="

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openMandatoryWeb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_1
    return-void

    .line 85
    :cond_1
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "http://www.samsungapps.com/common/term.as?mcc="

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public showYouthPrivacyPolicy()V
    .locals 2

    .prologue
    .line 112
    const-string v0, "http://apps.samsung.com/mercury/common/youthPolicy.as"

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openMandatoryWeb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
