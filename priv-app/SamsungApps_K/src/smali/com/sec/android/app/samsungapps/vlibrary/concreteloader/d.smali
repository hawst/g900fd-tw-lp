.class final Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final packageInstalled(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 357
    if-eq p2, v1, :cond_0

    .line 360
    const-string v0, "[ERROR] Package install return code : %d"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iput p2, v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$100(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$100(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 368
    :cond_1
    return-void
.end method
