.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->a:Ljava/util/ArrayList;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->b:Ljava/util/ArrayList;

    .line 64
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    .line 14
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    return-object v0
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;

    .line 41
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;->onItemAdded(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method

.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22
    if-nez p1, :cond_0

    move-object v0, v1

    .line 33
    :goto_0
    return-object v0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    .line 28
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 33
    goto :goto_0
.end method

.method public remove(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;

    .line 50
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;->onItemRemoved(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method
