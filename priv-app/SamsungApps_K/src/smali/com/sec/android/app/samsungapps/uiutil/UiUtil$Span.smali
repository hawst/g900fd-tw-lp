.class public Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput p1, p0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->a:I

    .line 159
    iput p2, p0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->b:I

    .line 160
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 172
    check-cast p1, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;

    .line 174
    iget v0, p1, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->a:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->a:I

    if-le v0, v1, :cond_0

    .line 175
    const/4 v0, -0x1

    .line 179
    :goto_0
    return v0

    .line 176
    :cond_0
    iget v0, p1, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->a:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->a:I

    if-ne v0, v1, :cond_1

    .line 177
    const/4 v0, 0x0

    goto :goto_0

    .line 179
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->b:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/sec/android/app/samsungapps/uiutil/UiUtil$Span;->a:I

    return v0
.end method
