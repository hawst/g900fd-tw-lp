.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public eventType:I

.field public moreDetailBtnLink:Ljava/lang/String;

.field public moreDetailBtnVal:Ljava/lang/String;

.field public noEvent:Ljava/lang/String;

.field public noUrl:Ljava/lang/String;

.field public notificationHeaderVal:Ljava/lang/String;

.field public notificationId:Ljava/lang/String;

.field public notificationType:I

.field public notificationVal:Ljava/lang/String;

.field public productSetId:Ljava/lang/String;

.field public promotionImgUrl:Ljava/lang/String;

.field public promotionType:I

.field public shortcutBtnVal:Ljava/lang/String;

.field public systemCheckImgUrl:Ljava/lang/String;

.field public yesUrl:Ljava/lang/String;

.field public yesokEvent:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkUserAlreadyCheck()Z
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isDialogCheckedDontDisplay(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getNotificationType()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationType:I

    return v0
.end method

.method public isProductSetType()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 28
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isProductType()Z
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDontDisplayAgain()V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeDialogDontDisplayCheck(Ljava/lang/String;)V

    .line 49
    return-void
.end method
