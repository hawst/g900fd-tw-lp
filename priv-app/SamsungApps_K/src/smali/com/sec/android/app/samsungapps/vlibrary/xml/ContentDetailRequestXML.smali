.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# instance fields
.field private additionalInfo:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 18
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public static guidProductDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
    .locals 4

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    const-string v1, "guidProductDetail"

    const-string v2, "2232"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 70
    const-string v1, "GUID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 72
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 73
    const-string v1, "productImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v1, "productImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v1, "screenImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v1, "screenImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    const-string v1, "unifiedPaymentYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_0
    return-object v0
.end method

.method public static guidProductDetailEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    const-string v1, "guidProductDetailEx"

    const-string v2, "2239"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    const-string v1, "GUID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 91
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 92
    const-string v1, "productImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v1, "productImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v1, "screenImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v1, "screenImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addPredeployed()V

    .line 102
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    const-string v1, "unifiedPaymentYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_1
    return-object v0
.end method

.method public static guidProductDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
    .locals 4

    .prologue
    .line 134
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    const-string v1, "guidProductDetailExMain"

    const-string v2, "2290"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 136
    const-string v1, "GUID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 138
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 139
    const-string v1, "productImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v1, "productImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addPredeployed()V

    .line 145
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    const-string v1, "unifiedPaymentYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_1
    return-object v0
.end method

.method public static productDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    const-string v1, "productDetail"

    const-string v2, "2080"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    const-string v1, "productID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->getProductID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 52
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 53
    const-string v1, "productImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v1, "productImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v1, "screenImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v1, "screenImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v1, "source"

    const-string v2, "banner"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    const-string v1, "unifiedPaymentYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    return-object v0
.end method

.method public static productDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    const-string v1, "productDetail"

    const-string v2, "2080"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 25
    const-string v1, "productID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 27
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 28
    const-string v1, "productImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v1, "productImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v1, "screenImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v1, "screenImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ScreenShot:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v1, "source"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->srchClickURL:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 37
    const-string v1, "srchClickURL"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->srchClickURL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    const-string v1, "unifiedPaymentYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :cond_1
    return-object v0
.end method

.method public static productDetailMain(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;

    const-string v1, "productDetailMain"

    const-string v2, "2280"

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 113
    const-string v1, "productID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->additionalInfo:Ljava/lang/String;

    .line 115
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 116
    const-string v1, "productImgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v1, "productImgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v1, "source"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v1, "orderID"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->srchClickURL:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 123
    const-string v1, "srchClickURL"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->srchClickURL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingCondition()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    const-string v1, "unifiedPaymentYN"

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ContentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_1
    return-object v0
.end method
