.class public Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;


# static fields
.field public static sActivityCount:I


# instance fields
.field private a:Z

.field protected mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

.field protected mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field protected mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

.field protected mIsS2I:Z

.field protected mOptionMenu:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

.field protected mTheme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 50
    iput v1, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mTheme:I

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mOptionMenu:Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mIsS2I:Z

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->a:Z

    return-void
.end method

.method private static a([F)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 386
    .line 387
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 388
    float-to-int v3, v3

    add-int/2addr v1, v3

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390
    :cond_0
    return v1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;)Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->a:Z

    return v0
.end method

.method public static checkIntentSafe(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 868
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 869
    invoke-virtual {v2, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 870
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 872
    :goto_0
    if-eqz v2, :cond_1

    .line 875
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 870
    goto :goto_0

    :cond_1
    move v0, v1

    .line 875
    goto :goto_1
.end method


# virtual methods
.method public getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v0

    .line 914
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    goto :goto_0
.end method

.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 905
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    .line 907
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGearPlatformVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 939
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    .line 921
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto :goto_0
.end method

.method public getTotalActivityCount()I
    .locals 2

    .prologue
    .line 149
    sget v0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    sget v1, Lcom/sec/android/app/samsungapps/CommonActivity;->sActivityCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected initialized()V
    .locals 3

    .prologue
    .line 155
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 157
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->initialize(Landroid/content/Context;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getTotalActivityCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogUtils;->findEntryPoint(Landroid/app/Activity;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setEntryPoint(Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V

    .line 167
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_START:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendAppStartAndFinishLog(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V

    .line 170
    :cond_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CommonPreferenceActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    packed-switch p1, :pswitch_data_0

    .line 277
    :goto_0
    :pswitch_0
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 278
    return-void

    .line 267
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 268
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 272
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/CategoryListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 881
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 883
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/high16 v2, 0x1000000

    .line 66
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mTheme:I

    if-nez v0, :cond_0

    .line 68
    invoke-static {}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getInstance()Lcom/sec/android/app/samsungapps/ThemeInfo;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mTheme:I

    .line 69
    iget v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mTheme:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->setTheme(I)V

    .line 75
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 82
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->requestWindowFeature(I)Z

    .line 83
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 86
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "amIS2I"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mIsS2I:Z

    .line 90
    new-instance v0, Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 92
    sget v0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonPreferenceActivity : onCreate : Current Activity count is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getTotalActivityCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->destroy()Ljava/lang/Boolean;

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->recursiveRecycle(Landroid/view/View;)V

    .line 105
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 107
    sget v0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->sActivityCount:I

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonPreferenceActivity : onDestroy : Current Activity count is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getTotalActivityCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getTotalActivityCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_1

    .line 112
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_FINISH:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendAppStartAndFinishLog(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V

    .line 114
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->destroy()V

    .line 117
    :cond_2
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 118
    return-void
.end method

.method public onLongClickActionItemActionBar(ILandroid/view/View;)Z
    .locals 12

    .prologue
    const/16 v11, 0x33

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-nez v0, :cond_0

    .line 291
    const-string v0, "CommonPreferenceActivity::onLongClickActionItemActionBar::Action Bar is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v2

    .line 382
    :goto_0
    return v0

    .line 295
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 338
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommonPreferenceActivity::onLongClickActionItemActionBar::Invalid action type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v2

    .line 339
    goto :goto_0

    .line 298
    :pswitch_1
    const v0, 0x7f0802f1

    move v1, v0

    .line 342
    :goto_1
    new-array v4, v5, [I

    .line 343
    new-array v5, v5, [I

    .line 345
    invoke-virtual {p2, v5}, Landroid/view/View;->getLocationInWindow([I)V

    .line 346
    invoke-virtual {p2, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v6, 0x7f040025

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 349
    const v0, 0x7f0c0099

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 350
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 352
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getToolTip()Landroid/widget/Toast;

    move-result-object v1

    .line 354
    invoke-virtual {v6}, Landroid/view/View;->getPaddingRight()I

    move-result v7

    invoke-virtual {v6}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    .line 356
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    new-array v8, v8, [F

    .line 357
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    .line 358
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 359
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v8}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 361
    invoke-static {v8}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->a([F)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 362
    add-int/2addr v0, v7

    .line 364
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v7, v0

    .line 366
    if-nez v1, :cond_1

    .line 368
    new-instance v0, Landroid/widget/Toast;

    invoke-direct {v0, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 369
    invoke-virtual {v0, v6}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 370
    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 371
    aget v1, v4, v2

    add-int/2addr v1, v7

    aget v2, v4, v3

    aget v4, v5, v3

    sub-int/2addr v2, v4

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v0, v11, v1, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setToolTip(Landroid/widget/Toast;)V

    .line 380
    :goto_2
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v3

    .line 382
    goto/16 :goto_0

    .line 302
    :pswitch_2
    const v0, 0x7f080141

    move v1, v0

    .line 303
    goto/16 :goto_1

    .line 306
    :pswitch_3
    const v0, 0x7f0802f5

    move v1, v0

    .line 307
    goto/16 :goto_1

    .line 310
    :pswitch_4
    const v0, 0x7f08023d

    move v1, v0

    .line 311
    goto/16 :goto_1

    .line 314
    :pswitch_5
    const v0, 0x7f0802f0

    move v1, v0

    .line 315
    goto/16 :goto_1

    .line 318
    :pswitch_6
    const v0, 0x7f0800d4

    move v1, v0

    .line 319
    goto/16 :goto_1

    .line 322
    :pswitch_7
    const v0, 0x7f0802a1

    move v1, v0

    .line 323
    goto/16 :goto_1

    .line 326
    :pswitch_8
    const v0, 0x7f0802e9

    move v1, v0

    .line 327
    goto/16 :goto_1

    .line 330
    :pswitch_9
    const v0, 0x7f08011a

    move v1, v0

    .line 331
    goto/16 :goto_1

    .line 334
    :pswitch_a
    const v0, 0x7f0802f2

    move v1, v0

    .line 335
    goto/16 :goto_1

    .line 376
    :cond_1
    invoke-virtual {v1, v6}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 377
    aget v0, v4, v2

    add-int/2addr v0, v7

    aget v2, v4, v3

    aget v4, v5, v3

    sub-int/2addr v2, v4

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v1, v11, v0, v2}, Landroid/widget/Toast;->setGravity(III)V

    move-object v0, v1

    goto :goto_2

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0xa0005
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 180
    packed-switch p1, :pswitch_data_0

    .line 250
    :goto_0
    return-void

    .line 227
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 229
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->isGearMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onBackPressed()V

    .line 245
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onBackPressed()V

    goto :goto_0

    .line 237
    :cond_2
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_1

    .line 240
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mIsS2I:Z

    if-ne v0, v3, :cond_1

    .line 242
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_1

    .line 249
    :pswitch_1
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0xa0000
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->doHomeKeyOptionMenu()V

    .line 135
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 136
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 125
    sput-object p0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 126
    return-void
.end method

.method public onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 689
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 837
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 693
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 696
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->a:Z

    .line 699
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/aa;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/aa;-><init>(Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 716
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/SettingsListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 717
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 718
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 720
    :cond_2
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 721
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 723
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 729
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SETTINGS_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 737
    :pswitch_3
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SendLogFileTo(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 739
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 742
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/WishListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 743
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_4

    .line 744
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 746
    :cond_4
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 747
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 755
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 761
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 762
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_6

    .line 763
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 765
    :cond_6
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 766
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 774
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 781
    :pswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->startTencentPurchasedActivity()V

    goto/16 :goto_0

    .line 785
    :pswitch_7
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 786
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_8

    .line 787
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 789
    :cond_8
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 790
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 798
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PAYMENT_METHOD_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 804
    :pswitch_8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 805
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v3, :cond_a

    .line 806
    const-string v1, "isDeepLink"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 808
    :cond_a
    const-string v1, "moremenu"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 809
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 811
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 817
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_ANNOUNCEMENT_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 823
    :pswitch_9
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/HelpListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 824
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CHART:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    .line 832
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_HELP_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    goto/16 :goto_0

    .line 689
    :pswitch_data_0
    .packed-switch 0x7f0c0000
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 8

    .prologue
    const v7, 0x7f0200c6

    const v6, 0x7f0801c2

    const v5, 0x7f0200c7

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 411
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 413
    const v0, 0x7f0c000b

    const v1, 0x7f0802b5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c9

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 414
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    const v0, 0x7f0c0003

    const v1, 0x7f08021a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v7, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 453
    :cond_0
    :goto_0
    return v4

    .line 421
    :cond_1
    const v0, 0x7f0c000b

    const v1, 0x7f0802b5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c9

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 422
    const/high16 v0, 0x7f0c0000

    const v1, 0x7f0802f7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200ca

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 429
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 431
    const v0, 0x7f0c0001

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v5, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 435
    const v0, 0x7f0c0002

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f08014f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v5, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 444
    :goto_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_2

    .line 446
    const v0, 0x7f0c0003

    const v1, 0x7f08021a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v7, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 449
    :cond_2
    const v0, 0x7f0c0004

    const v1, 0x7f08018c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c4

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 450
    const v0, 0x7f0c0006

    const v1, 0x7f0802b4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c8

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 451
    const v0, 0x7f0c0005

    const v1, 0x7f08021b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c5

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_0

    .line 441
    :cond_3
    const v0, 0x7f0c0001

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v5, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto :goto_1
.end method

.method public onSamsungAppsCreateSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z
    .locals 1

    .prologue
    .line 668
    const/4 v0, 0x1

    return v0
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 10

    .prologue
    const v8, 0x7f0c0001

    const v7, 0x7f0c000f

    const v6, 0x7f0c0003

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 464
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-ne v0, v9, :cond_10

    .line 469
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 471
    const/high16 v0, 0x7f0c0000

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_19

    move v2, v9

    .line 477
    :goto_0
    const/high16 v1, 0x7f0c0000

    const v0, 0x7f0802f7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200ca

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 480
    :cond_0
    invoke-virtual {p1, v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 486
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v9, :cond_9

    .line 489
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 490
    const/4 v2, 0x2

    .line 492
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0801c2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c7

    move-object v0, p1

    move v1, v8

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 520
    :cond_1
    :goto_2
    const v0, 0x7f0c0002

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 522
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v9, :cond_3

    .line 524
    const/4 v2, 0x2

    .line 525
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 526
    const/4 v2, 0x3

    .line 528
    :cond_2
    const v1, 0x7f0c0002

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0801c2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f08014f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c7

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 542
    :cond_3
    :goto_3
    invoke-virtual {p1, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_e

    .line 544
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_5

    .line 546
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 548
    const/4 v2, 0x2

    .line 549
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 550
    const/4 v2, 0x3

    .line 552
    :cond_4
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c6

    move-object v0, p1

    move v1, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 583
    :cond_5
    :goto_4
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    .line 584
    const-string v1, "1"

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->writeHistory:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0c0010

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 586
    const v0, 0x7f0c0010

    const-string v1, "Send Log By Email"

    const v2, 0x7f02015b

    invoke-virtual {p1, v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(ILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 633
    :cond_6
    :goto_5
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->isAutoLoginInProgress()Z

    move-result v0

    if-eq v0, v9, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v9, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-ne v0, v9, :cond_8

    .line 637
    :cond_7
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 640
    :cond_8
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem()Z

    move-result v0

    if-nez v0, :cond_15

    .line 645
    :goto_6
    return v5

    .line 500
    :cond_9
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 501
    const/4 v2, 0x2

    .line 503
    :goto_7
    const v0, 0x7f0801c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c7

    move-object v0, p1

    move v1, v8

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_2

    .line 508
    :cond_a
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 511
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 512
    const/4 v2, 0x2

    .line 514
    :goto_8
    invoke-virtual {p1, v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 515
    const v0, 0x7f0801c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c7

    move-object v0, p1

    move v1, v8

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_2

    .line 536
    :cond_b
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 538
    const v0, 0x7f0c0002

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    goto/16 :goto_3

    .line 558
    :cond_c
    const/4 v2, 0x3

    .line 559
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 560
    const/4 v2, 0x4

    .line 562
    :cond_d
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c6

    move-object v0, p1

    move v1, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_4

    .line 570
    :cond_e
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 572
    const/4 v2, 0x2

    .line 573
    invoke-virtual {p1, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 574
    const/4 v2, 0x3

    .line 576
    :cond_f
    invoke-virtual {p1, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 577
    const v0, 0x7f08021a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c6

    move-object v0, p1

    move v1, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_4

    .line 596
    :cond_10
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v9, :cond_13

    .line 598
    invoke-virtual {p1, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-ne v0, v9, :cond_11

    .line 600
    invoke-virtual {p1, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 617
    :cond_11
    :goto_9
    const v0, 0x7f0c0010

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-ne v0, v9, :cond_12

    .line 619
    const v0, 0x7f0c0010

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 620
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->removeLogFile()V

    .line 623
    :cond_12
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 625
    const v4, 0x7f0c000b

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200c9

    move-object v3, p1

    move v8, v5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto/16 :goto_5

    .line 605
    :cond_13
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-nez v0, :cond_14

    .line 607
    invoke-virtual {p1, v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->hasItem(I)Z

    move-result v0

    if-ne v0, v9, :cond_14

    .line 609
    invoke-virtual {p1, v8}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 612
    :cond_14
    const/high16 v0, 0x7f0c0000

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 613
    const v0, 0x7f0c0002

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 614
    invoke-virtual {p1, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    goto :goto_9

    :cond_15
    move v5, v9

    .line 645
    goto/16 :goto_6

    :cond_16
    move v2, v9

    goto/16 :goto_8

    :cond_17
    move v2, v9

    goto/16 :goto_7

    :cond_18
    move v2, v9

    goto/16 :goto_1

    :cond_19
    move v2, v5

    goto/16 :goto_0
.end method

.method public onSamsungAppsOpenSubOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)Z
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x1

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 935
    return-void
.end method

.method public refreshOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)V
    .locals 0

    .prologue
    .line 655
    if-eqz p1, :cond_0

    .line 656
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 657
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->refreshOptionmenu()V

    .line 659
    :cond_0
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 890
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 891
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->copyToIntent(Landroid/content/Intent;)V

    .line 899
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 900
    return-void
.end method

.method protected startTencentPurchasedActivity()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 841
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 842
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "open://updatelist?id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->ODC_PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 843
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 845
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->checkIntentSafe(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 847
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 854
    :goto_0
    return-void

    .line 851
    :cond_0
    const v0, 0x7f080148

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x7f08014f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 852
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0
.end method
