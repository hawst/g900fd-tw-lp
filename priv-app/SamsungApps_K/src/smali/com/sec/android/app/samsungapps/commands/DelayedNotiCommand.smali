.class public Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;->a:Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;

    .line 17
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;)V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;)V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;->a:Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;->a:Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;->getPositiveLabel()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/ay;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/ay;-><init>(Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 31
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;->a:Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;->getNegativeLabel()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/az;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/az;-><init>(Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 40
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 41
    return-void
.end method
