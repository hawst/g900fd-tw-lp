.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider;


# instance fields
.field private _IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _InicisType:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 14
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_InicisType:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 16
    return-void
.end method


# virtual methods
.method public getMerchantID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "merchantID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotiURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "notiURL"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPayPrice()I
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getPaymentID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "paymentID"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductImageURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    return-object v0
.end method

.method public isUpoint()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->_InicisType:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->getMerchantID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->getPaymentID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/CInicisURLInfoProvider;->getNotiURL()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x1

    .line 58
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
