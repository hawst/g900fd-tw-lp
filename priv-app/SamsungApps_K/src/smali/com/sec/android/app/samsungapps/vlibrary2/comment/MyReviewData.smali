.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private authorityRating:I

.field private commentId:Ljava/lang/String;

.field private commentYn:Z

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private productComment:Ljava/lang/String;

.field private ratingValue:I

.field private ratingYn:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method

.method private hasAuthorityRating()Z
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->authorityRating:I

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 38
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 29
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 30
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->commentId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 82
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->commentId:Ljava/lang/String;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->commentId:Ljava/lang/String;

    return-object v0
.end method

.method public getMyComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->productComment:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->productComment:Ljava/lang/String;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->productComment:Ljava/lang/String;

    return-object v0
.end method

.method public getMyRatingValue()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->ratingValue:I

    return v0
.end method

.method public hasMyComment()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->commentYn:Z

    return v0
.end method

.method public hasMyRating()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->ratingYn:Z

    return v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewData;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 21
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method
