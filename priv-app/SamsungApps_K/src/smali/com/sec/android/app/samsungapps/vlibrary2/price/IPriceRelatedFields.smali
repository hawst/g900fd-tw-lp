.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getDiscountPhoneBillPrice()D
.end method

.method public abstract getPhoneBillPrice()D
.end method

.method public abstract getReducePrice()D
.end method

.method public abstract getSellingPrice()D
.end method

.method public abstract isDiscounted()Z
.end method

.method public abstract isPhoneBillDiscounted()Z
.end method
