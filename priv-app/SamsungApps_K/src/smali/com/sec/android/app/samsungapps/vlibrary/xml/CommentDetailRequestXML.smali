.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 10
    const-string v2, "commentDetail"

    const-string v3, "2110"

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    const-string v0, "productID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "commentID"

    iget-object v1, p3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Comment;->commentID:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    return-void
.end method
