.class public Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractContentCommandBuilder;-><init>(Landroid/content/Context;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected getReportProblemActivity()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/samsungapps/ReportAppPageActivity;

    return-object v0
.end method

.method protected loginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ContentCommandBuilder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method
