.class public Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 28
    const-string v0, "CategoryViewFragment"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->b:Ljava/lang/String;

    .line 251
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;)Lcom/sec/android/app/samsungapps/view/CategoryItemListView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 41
    const v0, 0x7f040050

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setMainView(I)V

    .line 43
    new-instance v2, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v0, 0x7f0c0161

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->setData(Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->load()V

    .line 47
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->release()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 59
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onDestroy()V

    .line 60
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->release()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 71
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onDestroyView()V

    .line 72
    return-void
.end method

.method public retry()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 77
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-nez v5, :cond_1

    :cond_0
    const-string v0, "CategoryViewFragment::setVisibleRetry::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_1
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_2

    const-string v0, "CategoryViewFragment::setVisibleRetry::Retry Button is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/f;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/viewpager/f;-><init>(Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setResizeInMultiWindow()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method protected setVisibleEmpty(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 172
    .line 174
    if-ne p1, v0, :cond_0

    .line 181
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleLoading(I)Z

    .line 182
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleNodata(I)Z

    .line 191
    :goto_0
    return v0

    .line 186
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleLoading(I)Z

    .line 187
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleNodata(I)Z

    .line 188
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleEmptyLayout(I)Z

    move v0, v1

    goto :goto_0
.end method

.method protected setVisibleEmptyLayout(I)Z
    .locals 1

    .prologue
    .line 202
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 205
    if-nez v0, :cond_0

    .line 207
    const-string v0, "CategoryViewFragment::setVisibleEmptyLayout::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 208
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0

    .line 211
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 214
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setVisibleLoading(I)Z
    .locals 1

    .prologue
    .line 144
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 147
    if-nez v0, :cond_0

    .line 149
    const-string v0, "CategoryViewFragment::setVisibleLoading::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    .line 153
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    if-nez p1, :cond_1

    .line 158
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleEmptyLayout(I)Z

    .line 161
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setVisibleNodata(I)Z
    .locals 1

    .prologue
    .line 225
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 228
    if-nez v0, :cond_0

    .line 230
    const-string v0, "CategoryViewFragment::setVisibleNodata::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 231
    const/4 v0, 0x0

    .line 242
    :goto_0
    return v0

    .line 234
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 237
    if-nez p1, :cond_1

    .line 239
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleEmptyLayout(I)Z

    .line 242
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategoryViewFragment;->setVisibleEmpty(Z)Z

    .line 249
    return-void
.end method
