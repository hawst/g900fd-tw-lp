.class public Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected createAlipay(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/CAlipayCommandBuilder;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/commands/CAlipayCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 129
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CAlipayCommandBuilder;->createPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method protected createIranDebit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 135
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;->payment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/be;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/be;-><init>(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)V

    return-object v0
.end method

.method protected createPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 11

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;-><init>()V

    .line 58
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;-><init>()V

    .line 62
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/CSamsungWalletCommandBuilder;

    invoke-direct {v1, p1, p4}, Lcom/sec/android/app/samsungapps/commands/CSamsungWalletCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 63
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_SAMSUNG_WALLET:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletCommandBuilder;->samsungWalletCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 65
    const-class v1, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-virtual {v0, p0, v1, p1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createInicisPhoneBill(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 66
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_DANAL:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 68
    const-class v1, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-virtual {v0, p0, v1, p1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createUPoint(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 69
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 71
    const-class v1, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;

    invoke-virtual {v0, p0, v1, p1, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createInicis(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 72
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 74
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;

    new-instance v2, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;

    invoke-direct {v2, p3}, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-direct {v1, p1, p4, v2}, Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Ljava/lang/Object;)V

    .line 75
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;->createPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 76
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_PPC:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 78
    invoke-virtual {p0, p4, p1}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->globalCreditCard(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 79
    const-class v2, Lcom/sec/android/app/samsungapps/CyberCashActivity;

    invoke-virtual {v0, p1, v2, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createChinaCyberCash(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v2

    .line 81
    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 82
    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 83
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_CYBERCASH:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 84
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_PSMS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-direct {v2, v4, v5, v6, p1}, Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/commands/CPSMSCommandBuilder;->psmsPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 85
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_DIRECT_BILLING:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-direct {v5, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    invoke-direct {v2, p0, v4, v5, p1}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->directBillingPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 86
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_ALIPAY:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {p0, p1, p4}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->createAlipay(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->createEasyBuy(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    .line 88
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_IRAN_DEBIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->createIranDebit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 90
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    invoke-direct {v1, v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Landroid/content/Context;)V

    .line 93
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPBillingCondition()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    new-instance v3, Lcom/sec/android/app/samsungapps/commands/bc;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/app/samsungapps/commands/bc;-><init>(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;)V

    invoke-direct {v2, p2, p3, v3, p4}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 109
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createUnifiedBillingPaymentCommand(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 119
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getPermissionData()Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V

    .line 120
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;-><init>()V

    .line 121
    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 122
    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 123
    return-object v2

    .line 113
    :cond_0
    const-class v4, Lcom/sec/android/app/samsungapps/PurchaseActivity;

    new-instance v5, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;

    invoke-direct {v5, p3}, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {p0, p1, p4}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->createEasyBuy(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v6

    new-instance v8, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;

    new-instance v1, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    invoke-direct {v8, p1, p4, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V

    new-instance v9, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v9}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    new-instance v10, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-static {p2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v2

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    invoke-direct {v10, v1, v2, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Landroid/content/Context;)V

    move-object v1, p2

    move-object v2, p1

    move-object v7, p0

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createPurchaseCommand(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditionUSAContext;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_0
.end method

.method protected globalCreditCard(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 6

    .prologue
    .line 140
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;-><init>()V

    .line 141
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    :goto_0
    sget-object v3, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->viewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    new-instance v4, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/loadingDialog/LoadingDialogCreator;-><init>()V

    const/4 v5, 0x0

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Z)V

    new-instance v3, Lcom/sec/android/app/samsungapps/commands/bd;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/commands/bd;-><init>(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)V

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createGlobalCreditPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 159
    return-object v0

    .line 141
    :cond_0
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->context:Landroid/content/Context;

    goto :goto_0
.end method

.method public paymentCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 5

    .prologue
    .line 255
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 256
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    const v4, 0x7f08029b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->createPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 260
    return-object v0
.end method

.method public showNoAvailablePaymentMethod()V
    .locals 3

    .prologue
    .line 243
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    const v2, 0x7f080276

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    const v2, 0x7f0802e0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 247
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->_Context:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 250
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 251
    return-void
.end method
