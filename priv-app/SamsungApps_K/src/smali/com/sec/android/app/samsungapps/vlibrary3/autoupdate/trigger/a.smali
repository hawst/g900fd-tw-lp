.class final Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceivedInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getSeconds()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getSeconds()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->c(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V

    goto :goto_0
.end method

.method public final onRecieveFailed()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/a;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;)V

    .line 73
    return-void
.end method
