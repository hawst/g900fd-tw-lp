.class public Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 27
    return-void
.end method


# virtual methods
.method protected matchCondition()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->TURKEY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    .line 105
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isWIFIConnected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 107
    :cond_0
    return v0
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 8

    .prologue
    const v7, 0x7f0802ef

    const v6, 0x7f08023d

    const/4 v5, 0x1

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    const v4, 0x7f0802bc

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/r;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/r;-><init>(Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/s;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/s;-><init>(Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->invokeCompleted()V

    .line 39
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->invokeCompleted()V

    .line 40
    return-void

    .line 37
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    const v3, 0x7f08027d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    const v4, 0xff2c

    invoke-static {v3, v4}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/p;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/p;-><init>(Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/q;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/q;-><init>(Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;->invokeCompleted()V

    goto :goto_0
.end method
