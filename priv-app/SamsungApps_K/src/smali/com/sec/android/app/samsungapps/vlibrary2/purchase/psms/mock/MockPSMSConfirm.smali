.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockPSMSConfirm;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;
.source "ProGuard"


# instance fields
.field public _Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected createTimeOutRetrier(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;-><init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V

    .line 33
    return-object v0
.end method

.method public getMockTimeoutRetrier()Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockPSMSConfirm;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;

    return-object v0
.end method

.method public onNetResult(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockPSMSConfirm;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-interface {v0, v1, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 28
    return-void
.end method

.method protected sendRequest(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 0

    .prologue
    .line 21
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockPSMSConfirm;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 23
    return-void
.end method
