.class public Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;
.super Landroid/widget/TextView;
.source "ProGuard"


# instance fields
.field a:Landroid/content/Context;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field e:I

.field f:I

.field private g:Landroid/text/SpannableString;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 48
    const-string v0, "www.samsungapps.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->b:Ljava/lang/String;

    .line 49
    const-string v0, "apps.samsung.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->c:Ljava/lang/String;

    .line 50
    const-string v0, "help.content@samsung.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->d:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->a:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-string v0, "www.samsungapps.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->b:Ljava/lang/String;

    .line 49
    const-string v0, "apps.samsung.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->c:Ljava/lang/String;

    .line 50
    const-string v0, "help.content@samsung.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->d:Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->a:Landroid/content/Context;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const-string v0, "www.samsungapps.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->b:Ljava/lang/String;

    .line 49
    const-string v0, "apps.samsung.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->c:Ljava/lang/String;

    .line 50
    const-string v0, "help.content@samsung.com"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->d:Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->a:Landroid/content/Context;

    .line 32
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 25
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ActivityNotFoundException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 25
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openWebBrowser(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAppsClickableTextView::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public changText(Ljava/lang/CharSequence;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 45
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->g:Landroid/text/SpannableString;

    const-string v0, "www.samsungapps.com|apps.samsung.com|help.content@samsung.com|\uc0ac\uc5c5\uc790\uc815\ubcf4\ud655\uc778"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "www.samsungapps.com"

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    new-instance v3, Lcom/sec/android/app/samsungapps/view/aw;

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, v4, v0}, Lcom/sec/android/app/samsungapps/view/aw;-><init>(Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->g:Landroid/text/SpannableString;

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    const-string v0, "apps.samsung.com"

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    const-string v0, "help.content@samsung.com"

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    const-string v0, "\uc0ac\uc5c5\uc790\uc815\ubcf4\ud655\uc778"

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->g:Landroid/text/SpannableString;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-void

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public detectClickableArea(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 89
    const-string v0, "kimss"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "action "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    if-eq v3, v2, :cond_0

    if-nez v3, :cond_3

    .line 93
    :cond_0
    :try_start_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 94
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 96
    iput v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->e:I

    .line 97
    iput v4, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->f:I

    .line 99
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v5

    sub-int/2addr v0, v5

    .line 100
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    .line 102
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v5

    add-int/2addr v0, v5

    .line 103
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v5

    add-int/2addr v4, v5

    .line 105
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 106
    invoke-virtual {v5, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v4

    .line 107
    int-to-float v0, v0

    invoke-virtual {v5, v4, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v0

    .line 109
    const-class v4, Lcom/sec/android/app/samsungapps/view/aw;

    invoke-interface {p2, v0, v0, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/view/aw;

    .line 112
    array-length v4, v0

    if-eqz v4, :cond_2

    .line 114
    if-ne v3, v2, :cond_1

    .line 115
    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/aw;->onClick(Landroid/view/View;)V

    :goto_0
    move v0, v2

    .line 156
    :goto_1
    return v0

    .line 117
    :cond_1
    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v3}, Lcom/sec/android/app/samsungapps/view/aw;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1

    .line 121
    :cond_2
    if-ne v3, v2, :cond_6

    .line 122
    const/4 v0, 0x0

    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v3, Lcom/sec/android/app/samsungapps/view/aw;

    invoke-interface {p2, v0, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/view/aw;

    .line 124
    array-length v2, v0

    if-eqz v2, :cond_6

    .line 125
    array-length v3, v0

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_6

    aget-object v4, v0, v2

    .line 126
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/sec/android/app/samsungapps/view/aw;->a(Landroid/view/View;Z)V

    .line 125
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 131
    :cond_3
    const/4 v0, 0x2

    if-ne v3, v0, :cond_5

    .line 132
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 133
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 134
    iget v3, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->e:I

    sub-int v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v3, 0x1e

    if-gt v0, v3, :cond_4

    iget v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->f:I

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v2, 0xa

    if-le v0, v2, :cond_6

    .line 135
    :cond_4
    const/4 v0, 0x0

    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v3, Lcom/sec/android/app/samsungapps/view/aw;

    invoke-interface {p2, v0, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/view/aw;

    .line 137
    array-length v2, v0

    if-eqz v2, :cond_6

    .line 138
    array-length v3, v0

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_6

    aget-object v4, v0, v2

    .line 139
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/sec/android/app/samsungapps/view/aw;->a(Landroid/view/View;Z)V

    .line 138
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 143
    :cond_5
    const/4 v0, 0x3

    if-ne v3, v0, :cond_6

    .line 144
    const/4 v0, 0x0

    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v3, Lcom/sec/android/app/samsungapps/view/aw;

    invoke-interface {p2, v0, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/view/aw;

    .line 146
    array-length v2, v0

    if-eqz v2, :cond_6

    .line 147
    array-length v3, v0

    move v2, v1

    :goto_4
    if-ge v2, v3, :cond_6

    aget-object v4, v0, v2

    .line 148
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/sec/android/app/samsungapps/view/aw;->a(Landroid/view/View;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    move v0, v1

    .line 156
    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->g:Landroid/text/SpannableString;

    invoke-virtual {p0, p0, v0, p1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->detectClickableArea(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
