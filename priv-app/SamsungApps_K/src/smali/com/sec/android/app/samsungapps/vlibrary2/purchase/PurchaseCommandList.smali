.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field map:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->map:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    return-object v0
.end method

.method public putMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommandList;->map:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    return-void
.end method
